<?php
error_reporting(0);
$pageurl=  explode('/', $_SERVER['PHP_SELF']);

if((basename($_SERVER['PHP_SELF'])=='prospects') || (basename($_SERVER['PHP_SELF'])=='dashboard') || (basename($_SERVER['PHP_SELF'])=='calendar') || ($pageurl[3] == 'case_details') || (basename($_SERVER['PHP_SELF'])=='rolodex') || (basename($_SERVER['PHP_SELF'])=='eams_lookup') || (basename($_SERVER['PHP_SELF'])=='chat') || (basename($_SERVER['PHP_SELF'])=='reports') || (basename($_SERVER['PHP_SELF'])=='tasks'))

{
function compress() {
    $CI = & get_instance();
    $buffer = $CI->output->get_output();

    $search = array(
        '/\>[^\S ]+/s',
        '/[^\S ]+\</s',
        '/(\s)+/s',
        '#(?://)?<!\[CDATA\[(.*?)(?://)?\]\]>#s'
    );
    $replace = array(
        '>',
        '<',
        '\\1',
        "//&lt;![CDATA[\n" . '\1' . "\n//]]>"
    );

    $buffer = preg_replace($search, $replace, $buffer);

    $CI->output->set_output($buffer);
    $CI->output->_display();
}
}