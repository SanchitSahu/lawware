<?php

include APPPATH . 'libraries/sendgrid/vendor/autoload.php';
include APPPATH . 'libraries/mailparser/PlancakeEmailParser.php';
include APPPATH . 'libraries/mailparser/MimeMailParser.class.php';

class Common_Functions {
    /*
     * Function Name : error_redirection
     * @param $error_type , $msg, $redirect
     * @description :  function is used for set error or success message
     */

    public $ci;

    public function __construct() {
        $this->ci = & get_instance();
        $this->no_cache();
    }

    /** @Description : used for manage window back management
     * @Param :none
     * @Return : none;
     */
    function get_timeago($ptime, $returnAgo = false) {
        //echo $ptime; exit;
        /*$estimate_time = time() - $ptime;

        if ($estimate_time < 1) {
            if ($returnAgo) {
                return 'less than 1 second ago';
            } else {
                return 'less than 1 second';
            }
        }

        $condition = array(
            12 * 30 * 24 * 60 * 60 => 'year',
                /* 30 * 24 * 60 * 60 => 'month',
                  24 * 60 * 60 => 'day',
                  60 * 60 => 'hour',
                  60 => 'minute',
                  1 => 'second' */
        /*);

        foreach ($condition as $secs => $str) {
            $d = $estimate_time / $secs;

            if ($d >= 1) {
                $r = floor($d);
                if ($returnAgo) {
                    return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
                } else {
                    return $r . ' ' . $str . ( $r > 1 ? 's' : '' );
                }
            } else {
                return '0 years';
            }
        }*/
        $ptime = (date("Y-m-d", $ptime));
        $from = new DateTime($ptime);
        $to   = new DateTime('today');
        return $from->diff($to)->y;
    }

    /*
     * @Description : used for manage window back management
     * @Param :none
     * @Return : none;
     */

    public function no_cache() {
        $this->ci->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->ci->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->ci->output->set_header('Pragma: no-cache');
        $this->ci->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }

    public function get_month_names($month = '') {
        $month_names = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June',
            '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
        return ($month != "" && $month_names[$month]) ? $month_names[$month] : $month_names;
    }

    public function checkDateIsNull($date) {

        if ($date !== '1970-00-00 00:00:00' && $date !== '0000-00-00 00:00:00' && $date !== '1899-12-30 00:00:00' && $date != '') {
            return date('m/d/Y H:m:i', strtotime($date));
        } else {
            return '';
        }
    }

    /* This fucntion to generate random number
     * @params : N/A
     * @return : reuturn string of random number */

    public function generateRandomNum() {
        $randomCodlogine = '';
        $ramdomKey = array_merge(range('a', 'z'), range(0, 9), range('A', 'Z'));
        for ($i = 0; $i < 10; $i++) {
            $randomCode .= $ramdomKey[array_rand($ramdomKey)];
        }
        return $randomCode;
    }

    /*
     * @Description: check record exists or not
     * @Param : $table_name,$condition, $msg,$redirect
     * @Return : error or none
     */

    public function check_record_exists($table_name, $condition, $msg = '', $redirect = '') {

        $this->ci->db->select('count(*) as count ');
        $this->ci->db->from($table_name);
        $this->ci->db->where($condition);

        $result = $this->ci->db->get();

        $result = $result->result();
        //echo $this->ci->db->last_query(); exit;

        $result = $result[0];
        if ($result->count > 0) {
            if ($redirect != "") {
                $this->error_redirection("error", $msg, $redirect);
            }
            return "1";
        } else {//echo "new";exit;
            return "0";
        }
    }

    /*
     * @Description: insert Record in Database
     * @Param : $table,$data
     * @Return : Inserted Id
     */

    public function insertRecord($table, $data) {

        $this->ci->db->insert($table, $data);
//        echo $this->ci->db->last_query();exit;
        return $this->ci->db->insert_id();
    }

    public function getRecordById($table, $select, $cond = '', $count = 0) {

        if ($cond != '')
            $this->ci->db->where($cond);
        $this->ci->db->select($select);
        $q = $this->ci->db->get($table);
        if ($count > 0) {
            return $q->num_rows();
        } else {
            return $q->row();
        }
    }

    /*
     * @Description: Edit Record in Database
     * @Param : $table,$data,$where
     * @Return : Afftected Row
     */

    public function editRecord($table, $data, $where) {

        $this->ci->db->where($where);
        $this->ci->db->update($table, $data);
//        echo $this->ci->db->last_query(); exit;
        return $this->ci->db->affected_rows();
    }

    public function checkRecord($table) {
        $this->ci->db->select('*');
        $this->ci->db->from($table);
        $result = $this->ci->db->get();
        //echo $result->num_rows; exit;
        if ($result->num_rows > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function editchklist($table, $data) {
        //echo $table;  print_r($data); print_r($where); exit;
        $this->ci->db->where('id', '1');
        $this->ci->db->update($table, $data);
        //echo $this->ci->db->last_query();exit;
        return $this->ci->db->affected_rows();
    }

    /*
     * @Description: last Record in Database
     * @Param : $table,$field
     * @Return : Get Last Record
     */

    public function last_record($table, $field) {
        $last_row = $this->ci->db->select($field)->order_by($field, "desc")->limit(1)->get($table)->row()->{$field};

        return $last_row;
    }

    /*
     * @Description: Delete Record in Database
     * @Param : $table,$cond
     */

    public function deleteRec($table, $cond) {

        $this->ci->db->where($cond);
        $q = $this->ci->db->delete($table);
        //echo $this->ci->db->last_query();exit;
        return $this->ci->db->affected_rows();
    }

    /*
     * @Description: return password in encrypted form
     * @Param : $password
     * @Return : encrypted password
     */

    public function get_encrypt_password($password) {

        return isset($password) ? md5(config_item("pwd_hash_salt") . $password) : "";
    }

    /**
     * To create directory of log if not exists
     *
     */
    public function create_dir($path, $folder_name) {
        try {
            if (!mkdir($path . $folder_name, 0777))
                throw new Exception('Folder could not be created');
        } catch (Exception $ex) {
            $this->write_log('ERROR', $ex->getMessage());
        }
    }

    /**
     * To create directory of log if not exists
     *
     */
    public function check_dir_exists($path) {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        } else {
            return TRUE;
        }
    }

    /*
     *
     * @Description: Function is used for uploading image type data
     * @param : $file_name, $redirect
     * @Return : file name/error
     */

    public function upload_image_data($file_name = '', $setting, $redirect = '') {
        $dir = explode('uploads/', $setting['upload_path']);
        $dir = explode('/', $dir['1']);
        if (!file_exists($setting['upload_path'])) {
            $path = "././uploads/";
            $tmp_path = "";
            foreach ($dir as $dir) {
                $tmp_path .= $dir . '/';
                $this->check_dir_exists($path . $tmp_path);
            }
        }

        $this->ci->load->library('upload', $setting);

        $_FILES[$file_name]['name'] = preg_replace("/[^a-zA-Z0-9. ]/", "", $_FILES[$file_name]['name']);
        $_FILES[$file_name]['name'] = str_replace(" ", "_", rtrim($_FILES[$file_name]['name'], " "));

        if ($this->ci->upload->do_upload($file_name)) {
            $data = array('upload_data' => $this->ci->upload->data());
            $file_info = $this->ci->upload->data();
            return $file_info['file_name'];
        } else {
            $error = array('error' => $this->ci->upload->display_errors());
            if (!empty($redirect)) {
                $this->error_redirection("error", $this->ci->upload->display_errors(), $redirect);
            } else {
                return $error;
            }
        }
    }

    public function get_email_config() {

        $this->ci->db->select('*');
        $this->ci->db->from('email_config');
        $query = $this->ci->db->get();
        $result = $query->row();
        $configEmail = array();
        $configEmail['mailConfig']['protocol'] = $result->protocol;
        $configEmail['mailConfig']['smtp_host'] = $result->smtp_host;
        $configEmail['mailConfig']['smtp_user'] = $result->smtp_user;
        $configEmail['mailConfig']['smtp_pass'] = $result->smtp_pass;
        $configEmail['mailConfig']['smtp_port'] = $result->smtp_port;
        $configEmail['mailConfig']['mailtype'] = 'html';
        $configEmail['from_name'] = $result->from_name;
        $configEmail['from_email'] = $result->from_email;
        return $configEmail;
    }

    /*
     * @Description : Send email
     * @Param: $to,$subject,$message
     * @Return :nothing
     */

    public function send_mail($to, $mail_from, $subject, $message) {
        $emil_configArr = $this->ci->common_functions->get_email_config();
        $this->ci->load->library('email', $emil_configArr['mailConfig']);

        $this->ci->email->clear(TRUE);
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($mail_from, "AskIndus");
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);


        // $this->ci->email->attach($attachment);
        // send your email. if it produce an error it will print 'Fail to send your message!' for you
        if ($this->ci->email->send()) {
            return true;
            //echo "Message sent successfully!";die;
        } else {
            return false;
            //echo "Fail to send your message!";die;
        }
    }

    /* this function is used to get any table data
     * @Param:$table_name
     * @return : $data
     */

    public function get_table_data($table_name = '', $condition = '') {

        if ($table_name != "" && $condition != "") {
            $this->ci->db->select("*");
            $this->ci->db->from($table_name);
            $this->ci->db->where($condition);
            $result = $this->ci->db->get('');
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }
            return "";
        }
    }

    /*
     * this function is used for send sms
     *
     */

    function send_sms($to = '', $message = '') {

        require_once(APPPATH . 'third_party/Twilio.php');
        // set your AccountSid and AuthToken from www.twilio.com/user/account
        $AccountSid = "ACbb30337f9ed64b7e073370955b2c6d8e";
        $AuthToken = "e05de506c954e35a1f892109f782da68";
        $client = new Services_Twilio($AccountSid, $AuthToken);
        $code = $code = (rand(1000, 10000));
        try {
            /* $message = $client->account->messages->sendMessage(
              '+19125742443', // From a Twilio number in your account
              '+918000338613', // Text any number
              "Your Mobile verification Code is ".$code." Thank You Askindus"
              ); */
            $message = $client->account->messages->create(array(
                "From" => "+14696153050",
                "To" => $to,
                "Body" => $message
            ));
        } catch (Services_Twilio_RestException $e) {
            echo $e->getMessage();
        }
    }

    /*
     * this function is used for get Multiple Record
     */

    public function getAllRecord($table, $fields, $cond = '', $orderby = '') {
        if ($orderby != '') {
            $this->ci->db->order_by($orderby);
        }
        if ($cond != '') {
            $this->ci->db->where($cond);
        }
        $this->ci->db->select($fields);
        $this->ci->db->from($table);
        $q = $this->ci->db->get();
        //echo $this->ci->db->last_query();
        return $q->result();
    }

    /*
     * this function is used for get Single Record
     */

    public function getAllEventRecord($cond = '', $orderby = '', $where1 = '', $where_and = '') {

        if ($orderby != '') {
            $this->ci->db->order_by($orderby);
        }
        if ($cond != '') {
            $this->ci->db->where($cond);
        }
        if ($where1 != '') {
            $this->ci->db->where($where1);
        }
        if($where_and != '') {
            $this->ci->db->where($where_and);
        }
        $this->ci->db->select('c1.*,c2.initials as intl');
        $this->ci->db->from('cal1 c1');
        $this->ci->db->join('cal2 c2', 'c1.eventno = c2.eventno', 'left');
        $this->ci->db->group_by('c1.eventno');
        $q = $this->ci->db->get();
        // echo $this->ci->db->last_query();exit;
        return $q->result();
    }

    public function getSingleRecordById($table, $fields, $cond = '', $orderby = '') {
        if ($orderby != '') {
            $this->ci->db->order_by($orderby);
        }

        $this->ci->db->where($cond);
        $this->ci->db->select($fields, FALSE);
        $this->ci->db->from($table);
        $q = $this->ci->db->get();

        return $q->row();
    }

    // get Single Record Array
    public function getSingleRecordArray($table, $fields, $cond = '', $orderby = '') {
        if ($orderby != '') {
            $this->ci->db->order_by($orderby);
        }
        if ($cond != '') {
            $this->ci->db->where($cond);
        }
        $this->ci->db->select($fields);
        $this->ci->db->from($table);
        $q = $this->ci->db->get();

        return $q->row_array();
    }

    /*
     * check User is login or Not Session
     */

    public function checkLogin() {
        if ($this->ci->uri->segment(2) != 'logout') {
            if ($this->ci->input->cookie('loginUser') != '' || $this->ci->session->userdata('user_data') != '') {

                // $check_login_id = $this->getRecordById('staff','loggedin_id', array('username' => $this->ci->session->userdata('user_data')['username']));
                if ($this->ci->input->cookie('loginUser') != '') {
                    $username = $this->ci->input->cookie('loginUser');
                }

                if ($this->ci->session->userdata('user_data') != '') {
                    $username = $this->ci->session->userdata('user_data')['username'];
                }


                $res = $this->getRecordById('staff', 'initials,username,password,fname,lname,title,profilepic,loggedin_id', array('username' => $username));

                $this->ci->session->set_userdata('user_data', (array) $res);
                if ($this->ci->uri->segment(1) == 'login' && count($res) > 0) {
                    redirect('dashboard');
                }
                if (count($res) < 1) {
                    redirect('login/logout');
                }
            } else {
                if ($this->ci->uri->segment(1) != 'login') {
                    redirect('login');
                }
            }
        }
    }

    public function getCaseSpecificDetails($caseno) {
        //   $result = $this->ci->db->get_where('case', array('caseno' => $caseno));
        $query = $this->ci->db->query("
            SELECT c.*,c22.venue AS mainVanue,c2.last FROM `case` c
            LEFT JOIN card c2 ON c2.cardcode= c.venue
            LEFT JOIN card2 c22 ON c22.firmcode= c2.firmcode
            WHERE c.caseno = $caseno
        ");
        //  echo $this->db->last_query();exit;
        return $query->result();
    }

    public function get_staff_listing($current_user) {
        $this->ci->db->select("initials, CONCAT(initials, ' - ', fname,' ', lname) AS name", false);
        /*$this->ci->db->where('initials !=', $current_user);*/
        $result = $this->ci->db->get('staff');
        return $result->result();
    }

    public function get_staff_listing_Case() {
        $this->ci->db->select("initials, CONCAT(initials, ' - ', fname,' ', lname) AS name", false);
        $result = $this->ci->db->get('staff');
        return $result->result();
    }

    public function getlatestTask() {

        $this->ci->db->limit(5);
        $this->ci->db->order_by('datereq', 'DESC');
        $this->ci->db->where("whoto = '" . $this->ci->session->userdata('user_data')['username'] . "' AND (completed = '0000-00-00 00:00:00' OR completed = '1899-12-30 00:00:00')");
        $this->ci->db->select("DATE_FORMAT(datereq,'%m/%d/%Y') as finishby,priority,event,typetask,mainkey,whofrom,completed,caseno", false);
        $result = $this->ci->db->get('tasks');
//echo $this->ci->db->last_query();exit;
        return $result->result();
    }

    public function last_caseact_record($cond) {
        $this->ci->db->limit(1);
        $this->ci->db->order_by('actno', "desc");
        $this->ci->db->where($cond);
        $this->ci->db->select('actno');
        $q = $this->ci->db->get('caseact');
        //$last_row = $this->ci->db->select($field)->order_by($field, "desc")->limit(1)->get($table)->row()->{$field};
//        echo $this->ci->db->last_query();exit;
        if (count($q->row()) > 0) {
            return $q->row()->{'actno'};
        } else {
            return 0;
        }
    }

    public function getCardSpecific($cardcode, $caseno) {
        $res = $this->ci->db->get_where('casecard', array('cardcode' => $cardcode, 'caseno !=' => $caseno));
        return $res->num_rows();
    }

    public function getCaseFullDetails($case_id, $cardcode) {
        $query = $this->ci->db->query("
            SELECT *,c.venue AS mainVenue FROM casecard cc
            LEFT JOIN `case` c ON cc.caseno = c.caseno
            LEFT JOIN card cd ON cc.cardcode = cd.cardcode
            LEFT JOIN card2 c2 ON c2.firmcode= cd.firmcode
            WHERE c.caseno = $case_id and cc.cardcode=$cardcode
        ");
        if ($query->num_rows() > 0) {
            //echo $this->db->last_query();
            //exit;
            return $query->row();
        } else {
            return false;
        }
    }

    public function getCardSpecificdata($cardcode, $caseno) {
        $res = $this->ci->db->get_where('casecard', array('cardcode' => $cardcode, 'caseno !=' => $caseno));
        return $res->row();
    }

    public function insertCaseActRecord($data) {
        // echo "<pre>";
        //print_r($data);exit;
        if (isset($data['caseno']) && $data['caseno'] > 0) {
            $actno = $this->last_caseact_record(array('caseno' => $data['caseno']));
            $data['actno'] = $actno + 1;
            $res = $this->ci->db->insert('caseact', $data);
            if ($res) {
                return $data['actno'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function insertMetadata($metadata) {
        $res = $this->getRecordById('meta_data', 'id', array('staff' => $metadata['staff'], 'caseno' => $metadata['caseno']));

        if (!isset($res->id)) {
            $recordId = $this->insertRecord('meta_data', $metadata);
        } else {
            $record = $this->editRecord('meta_data', $metadata, array('id' => $res->id));

            if ($record >= 0) {
                $recordId = $res->id;
            } else {
                $recordId = 0;
            }
        }

        return $recordId;
    }

    /*
      placing the sendgrid functions here */

    function ext_send($recipients, $subject, $whofrom, $mailbody, $att_files, $case_no, $email_con_id, $urgent, $whofrom_e_name = '', $filename) {
        $sendate = time();
        $subject = !empty($subject) ? $subject : 'Regarding '.APPLICATION_NAME;
        $subject = !empty($urgent) ? 'Urgent - ' . $subject : $subject;
        if ($case_no != '') {
            $from_set_email = $email_con_id . '-' . $case_no . '-' . strtolower($whofrom) . '@' . LAW_PUBLIC_DOMAIN;
        } else {
            $from_set_email = $email_con_id . '-' . strtolower($whofrom) . '@' . LAW_PUBLIC_DOMAIN;
        }
        $from = new SendGrid\Email($whofrom_e_name, 'notification@' . LAW_PUBLIC_DOMAIN);
        $to = new SendGrid\Email("Admin User", "kairavi@solulab.com");
        $content = new SendGrid\Content("text/html", $mailbody);
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $mail->addHeader("MIME-Version", "1.0");
        $mail->addHeader("X-Priority", "1");
        $mail->addHeader("Importance", "High");
        $personalization = array();
        $tos = array();

        foreach ($recipients as $key => $recipient) {

            $username = explode('@', $recipient);
            $personalization[$key] = new SendGrid\Personalization();
            $tos[$key] = new SendGrid\Email($username[0], trim($recipient));
            $personalization[$key]->addTo($tos[$key]);
            // $personalization[$key]->addCC($tos[$key]);
            $mail->addPersonalization($personalization[$key]);
        }
        $mail->setSendAt($sendate);
        $mail->addCategory('lawware'); /*In case this needs to be Application name, then use constant APPLICATION_NAME*/
        if ($urgent != '') {
            $mail->addHeader("Priority", "urgent");
        }
        if ($case_no != '') {
            $mail->addCustomArg("Case No", $case_no);
        }
        $reply_to = new SendGrid\ReplyTo($from_set_email, APPLICATION_NAME); // dont remove these 2 line
        $mail->setReplyTo($reply_to);
        if (!empty($att_files)) {
            $attachment = array();
            $file_data = '';
            $tmp_att_files['file0'] = $att_files;
            foreach ($tmp_att_files as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $attachment[$key1] = new SendGrid\Attachment();
                    if(strpos($value['tmp_name'], '\\')) {
                        $file_data = file_get_contents($value['tmp_name']);
                    } else {
                        $file_data = file_get_contents(APPPATH."../assets/attachment/".$filename."/".$value['tmp_name']);
                    }
                    $attachment[$key1]->setContent(base64_encode($file_data));
                    //$fileoftype = $this->get_mime_type(basename($value));
                    $attachment[$key1]->setType($value['type']);
                    $attachment[$key1]->setFilename($value['name']);
                    $attachment[$key1]->setDisposition("attachment");
                    $attachment[$key1]->setContentId($whofrom . uniqid());
                    $mail->addAttachment($attachment[$key1]);
                }
                
            }
        }
        $apiKey = $this->ci->config->item('sendgrid_apikey');
        $sg = new SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        $status = $response->statusCode();
        return $status;
    }

    public function mailparsing($body) {
        $emailParser = new \PlancakeEmailParser(json_decode($body));
        // You can use some predefined methods to retrieve headers...
        $emailTo = $emailParser->getTo();
        $emailSubject = $emailParser->getSubject();
        $emailCc = $emailParser->getCc();
        // ... or you can use the 'general purpose' method getHeader()
        $emailDeliveredToHeader = $emailParser->getHeader('Delivered-To');

        $emailBodyplain = $emailParser->getPlainBody();
        $emailBodyhtml = $emailParser->getHTMLBody();

        return $emailBodyplain;
    }

    public function mimemailparse($body, $folder) {
        error_reporting(E_ALL);
        ini_set("display_errors","ON");
        $fileLocation = APPPATH . "mymail.txt";
        //echo 'file location => ' . $fileLocation; exit();
        $file = fopen($fileLocation, "w");
        fwrite($file, json_decode($body));
        fclose($file);
        $parser = new \MimeMailParser();
        $parser->setText(file_get_contents($fileLocation));
        $to = $parser->getHeader('to');
        $delivered_to = $parser->getHeader('delivered-to');
        $from = $parser->getHeader('from');
        $subject = $parser->getHeader('subject');
        $text = $parser->getMessageBody('text');
        $html = $parser->getMessageBody('html');
        $attachments = $parser->getAttachments();
        if (!empty($attachments)) {
            $path = FCPATH . 'assets/attachment/' . $folder . '/';
            // Write attachments to disk
            foreach ($attachments as $attachment) {
                $attachment->saveAttachment($path);
            }
        }
        if ($folder != '') {
            $parse_data = array(
                'to' => $to,
                'from' => $from,
                'subject' => $subject,
                'text' => $text,
                'html' => $html,
                'delivered_to' => $delivered_to,
            );
        } else {
            $parse_data = $text;
        }
        @ftruncate($file, 0);
        // if (!empty($html)){ return $html;} else{ return $text;}
        return $parse_data;
    }

    //get Next Cal Event For Case
    public function getNextCalendarEvent($caseno) {

        $this->ci->db->limit(1);
        $this->ci->db->order_by('date', 'desc');
//        $this->ci->db->where('date >=', date('Y-m-d H:i:s'));
        $this->ci->db->where('caseno', $caseno);
        $this->ci->db->select('date,event');
        $query = $this->ci->db->get('cal1');

        return $query->row();
    }

    public function getQuickNote($caseno) {

        $this->ci->db->limit(1);
        $this->ci->db->where('category', '1000001');
        $this->ci->db->where('caseno', $caseno);
        $this->ci->db->select('event');
        $query = $this->ci->db->get('caseact');

        return $query->row();
    }

    /* check Admin login  */

    public function checkAdminLogin() {
        if ($this->ci->uri->segment(2) != 'logout') {
            if ($this->ci->input->cookie('login_admin_user') != '' || $this->ci->session->userdata('admin_user_data') != '') {
                if ($this->ci->input->cookie('login_admin_user') != '') {
                    $username = $this->ci->input->cookie('login_admin_user');
                    $password = $this->ci->input->cookie('login_admin_password');
                }

                if ($this->ci->session->userdata('admin_user_data') != '') {
                    $username = $this->ci->session->userdata('admin_user_data')['sa_user_name'];
                    $password = $this->ci->session->userdata('admin_user_data')['sa_password'];
                }

                $res = $this->getRecordById('system_admin', 'sa_user_name,sa_password,sa_email,sa_role,sa_status', array('sa_user_name' => $username, 'sa_password' => $password));
                $this->ci->session->set_userdata('admin_user_data', (array) $res);
                if ($this->ci->uri->segment(1) == 'login' && count($res) > 0) {
                    redirect('admin/dashboard');
                }
                if (count($res) < 1) {
                    redirect('admin/logout');
                }
            }
        }
    }

    public function get_mime_type($filename) {
        $idx = explode('.', $filename);
        $count_explode = count($idx);
        $idx = strtolower($idx[$count_explode - 1]);

        $mimet = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (isset($mimet[$idx])) {
            return $mimet[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    /* send mail function on registration time */

    public function reg_send($recipient, $subject, $whofrom, $mailbody) {
        $sendate = time();
        $subject = !empty($subject) ? $subject : 'Regarding '.APPLICATION_NAME;
        $from_set_email = strtolower($whofrom) . '@' . LAW_PUBLIC_DOMAIN;
        $from = new SendGrid\Email('notification@' . LAW_PUBLIC_DOMAIN, $from_set_email);
        $to = new SendGrid\Email($whofrom, $recipient);
        $content = new SendGrid\Content("text/html", $mailbody);
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $mail->setSendAt($sendate);
        $mail->addCategory('lawware');

        //$mail->addHeader("Content-Transfer-Encoding:", "base64");
        $reply_to = new SendGrid\ReplyTo($from_set_email, APPLICATION_NAME); // dont remove these 2 line
        $mail->setReplyTo($reply_to);
        $apiKey = $this->ci->config->item('sendgrid_apikey');
        $sg = new SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        $status = $response->statusCode();
        return $status;
    }

    // class common function ends here

    public function getCaseno($pro_caseno) {
        $this->ci->db->where('caseno', $pro_caseno);
        $result = $this->ci->db->get('case');
        //echo $this->db->last_query();exit;
        return $result->num_rows();
    }

// class common function ends here

    public function checkSessionId() {
        /* echo '<pre>'; print_r($this->ci->session->userdata('user_data'));
          echo 'session id =>' . $this->ci->session->userdata('user_data')['loggedin_id'];
          echo '<br/>matched id =>' . $this->ci->session->userdata('match_id');
          exit; */
        if ($this->ci->session->userdata('user_data')['loggedin_id'] != $this->ci->session->userdata('match_id')) {
            redirect('login/logout');
        }
    }

    public function checkAdminSession() {
        if (empty($this->ci->session->userdata('admin_user_data'))) {
            redirect('admin/login');
        }
    }

    public function getProfileImage($initials) {
        $this->ci->db->select('profilepic');
        $this->ci->db->from('staff');
        $this->ci->db->where('initials', $initials);
        $result = $this->ci->db->get();
        $row = $result->row();
        if (!empty($row->profilepic))
            return $row->profilepic;
        else
            return "";
    }

    public function getCaseDefendantdetails($case_id, $prepatry) {
        // $case_search = explode('-', $case_id);
        // print_r($prepatry);exit;
        $query = $this->ci->db->query("
            SELECT `first`,`last` FROM casecard cc
            LEFT JOIN `case` c ON cc.caseno = c.caseno
            LEFT JOIN card cd ON cc.cardcode = cd.cardcode
            WHERE c.caseno = $case_id and (cd.type LIKE '$prepatry' ) order by orderno ASC limit 1
        ");
        //echo $this->ci->db->last_query();exit;
        if ($query->num_rows() > 0) {
            // echo $this->ci->db->last_query();exit;
            return $query->result();
        } else {
            return false;
        }
    }

    public function check_case_clone($caseno, $clonecaseno) {

        $this->ci->db->where('caseno', $caseno);
        //$this->db->where('cardcode', $cardcode);
        $this->ci->db->select('related');
        $result = $this->ci->db->get('casekey');
        //echo $this->ci->db->last_query();
        $related = $result->result_array();
        $relatedkey = $related[0]['related'];

        $this->ci->db->where('related', $relatedkey);
        //$this->db->where('cardcode', $cardcode);
        $this->ci->db->select('caseno');
        $resultcases = $this->ci->db->get('casekey');
        $cases = $resultcases->result_array();
        foreach ($cases as $key => $value) {
            $existCases[] = $value['caseno'];
        }
        //echo $this->ci->db->last_query(); exit;
        if (in_array($clonecaseno, $existCases)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getEventCaseNo($eventno) {
        $query = $this->ci->db->query("
            SELECT * from cal1 where eventno = " . $eventno . "
        ");
        if ($query->num_rows() > 0) {
            //echo $this->ci->db->last_query(); exit;
            $row = $query->result();
            return $row[0]->caseno;
        } else {
            return 'N/A';
        }
    }

    public function getEventDate($caseno) {
        //echo "SELECT * from cal1 where caseno = ".$caseno." order by date asc LIMIT 1" ; exit;
        $query1 = $this->ci->db->query("
                SELECT * from cal1 where caseno = " . $caseno . " order by date asc LIMIT 1
            ");
        if ($query1->num_rows() > 0) {
            //echo $this->ci->db->last_query(); exit;
            $row1 = $query1->result();
            return $row1[0]->date;
        } else {
            return 'N/A';
        }
    }

    public function getsystemData() {
        $query = $this->ci->db->query("SELECT * from system");
        //echo $this->ci->db->last_query(); exit;
        return $query->result();
    }

    public function getlatestTaskadmin() {

        $this->ci->db->limit(5);
        $this->ci->db->order_by('datereq', 'DESC');
        $this->ci->db->where("completed = '0000-00-00 00:00:00' OR completed = '1899-12-30 00:00:00'");
        $this->ci->db->select("DATE_FORMAT(datereq,'%m/%d/%Y') as finishby,priority,event,typetask,mainkey,whofrom,completed,caseno", false);
        $result = $this->ci->db->get('tasks');
//echo $this->ci->db->last_query();exit;
        return $result->result();
    }

    /*Function to get all groups*/
    public function getGroups() {
        $this->ci->db->select('grp.group_id, grp.group_name, grp.module_name');
        $this->ci->db->from('`group` grp');
        $this->ci->db->group_by('grp.group_id');
        $q = $this->ci->db->get();
        return $q->result();
    }

    /*Function to get all members of a group*/
    public function getGroupMembers($group_id) {
        $this->ci->db->select('grpmem.member_id, grpmem.initials');
        $this->ci->db->from('`group_member` grpmem');
        $this->ci->db->where('grpmem.group_id = '.$group_id.' AND grpmem.role LIKE "member"');
        $q = $this->ci->db->get();
        return $q->result();
    }
    
    function ext_send_new($recipients, $subject, $whofrom, $mailbody, $att_files, $case_no, $email_con_id, $urgent, $whofrom_e_name = '', $filename) {
        $sendate = time();
        $email_con_id = $filename;
        $subject = !empty($subject) ? $subject : 'Regarding '.APPLICATION_NAME;
        $subject = !empty($urgent) ? 'Urgent - ' . $subject : $subject;
        if ($case_no != '') {
            $from_set_email = $email_con_id . '-' . $case_no . '-' . strtolower($whofrom) . '@' . LAW_PUBLIC_DOMAIN;
        } else {
            $from_set_email = $email_con_id . '-' . strtolower($whofrom) . '@' . LAW_PUBLIC_DOMAIN;
        }
        $from = new SendGrid\Email($whofrom_e_name, 'notification@' . LAW_PUBLIC_DOMAIN);
        $to = new SendGrid\Email("Admin User", "dinky@solulab.com");
        $content = new SendGrid\Content("text/html", $mailbody);
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $mail->addHeader("MIME-Version", "1.0");
        $mail->addHeader("X-Priority", "1");
        $mail->addHeader("Importance", "High");
        $personalization = array();
        $tos = array();

        foreach ($recipients as $key => $recipient) {

            $username = explode('@', $recipient);
            $personalization[$key] = new SendGrid\Personalization();
            $tos[$key] = new SendGrid\Email($username[0], trim($recipient));
            $personalization[$key]->addTo($tos[$key]);
            // $personalization[$key]->addCC($tos[$key]);
            $mail->addPersonalization($personalization[$key]);
        }
        $mail->setSendAt($sendate);
        $mail->addCategory('lawware'); /*In case this needs to be Application name, then use constant APPLICATION_NAME*/
        if ($urgent != '') {
            $mail->addHeader("Priority", "urgent");
        }
        if ($case_no != '') {
            $mail->addCustomArg("Case No", $case_no);
        }
        $reply_to = new SendGrid\ReplyTo($from_set_email, APPLICATION_NAME); // dont remove these 2 line
        $mail->setReplyTo($reply_to);
        if (!empty($att_files)) {
            $attachment = array();
            $file_data = '';
            $tmp_att_files['file0'] = $att_files;
            foreach ($tmp_att_files as $key => $val) {
                foreach ($val as $key1 => $value) {
                    $attachment[$key1] = new SendGrid\Attachment();
                    if(strpos($value['tmp_name'], '\\')) {
                        $file_data = file_get_contents($value['tmp_name']);
                    } else {
                        $file_data = file_get_contents(APPPATH."../assets/attachment/".$filename."/".$value['tmp_name']);
                    }
                    $attachment[$key1]->setContent(base64_encode($file_data));
                    //$fileoftype = $this->get_mime_type(basename($value));
                    $attachment[$key1]->setType($value['type']);
                    $attachment[$key1]->setFilename($value['name']);
                    $attachment[$key1]->setDisposition("attachment");
                    $attachment[$key1]->setContentId($whofrom . uniqid());
                    $mail->addAttachment($attachment[$key1]);
                }
                
            }
        }
        $apiKey = $this->ci->config->item('sendgrid_apikey');
        $sg = new SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        $status = $response->statusCode();
        return $status;
    }


    // function to get signature details
    public function getSignaturedetails($initials) {
        $this->ci->db->select('onoffsignature , signature');
        $this->ci->db->where('initials', $initials);
        $q = $this->ci->db->get('staff');
        return $q->result()[0];
       
    }
}

?>
