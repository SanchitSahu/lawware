<?php

// Include classes
include_once('tbs_class.php'); // Load the TinyButStrong template engine
include_once('tbs_plugin_opentbs.php'); // Load the OpenTBS plugin

class Tbs_Functions {

    public $ci;
    public $name;
    public $address1;
    public $address2;
    public $city;
    public $state;
    public $zip;
    public $caption;
    public $fileno;
    public $date;
    public $time;
    public $place;

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function setDocs($data) {
        
        //echo $this->session->userdata('user_data')['username']; exit;
        $result = array();

        $result['docPath'] = DOCUMENTPATH.'clients/doc_forms/'.$data['case_data']['fileName'];

        $result['enum'] = $data['case_data']['enum'];
        $result['today'] = $data['case_data']['today'];
        $result['filenumber'] = $data['case_data']['fileNumber'];
        $result['depo_date'] = $data['case_data']['depo_date'];
        $result['depo_time'] = $data['case_data']['depo_time'];
        $result['fee_amt'] = $data['case_data']['fee_amt'];
        $result['pl_title'] = $data['case_data']['pl_title'];
        $result['my_pl_title'] = $data['case_data']['my_pl_title'];
        $result['date_App'] = $data['case_data']['date_App'];
        $result['date_Tri'] = $data['case_data']['date_Tri'];
        $result['time_Tri'] = $data['case_data']['time_Tri'];
        $result['boardCity'] = $data['case_data']['boardCity'];
        $result['dr_splt'] = $data['case_data']['dr_splt'];
        $result['e_type'] = $data['case_data']['e_type'];
        $result['e_day'] = $data['case_data']['e_day'];
        $result['e_date'] = $data['case_data']['e_date'];
        $result['e_time'] = $data['case_data']['e_time'];
        $result['attyres'] = $data['case_data']['attorneyresname'];
        $result['social_sec']='';
		$result['pro_unit'] = $data['case_data']['pro_unit'];
        $result['pro_doc_typ'] = $data['case_data']['pro_doc_typ'];
        $result['doc_title'] = $data['case_data']['doc_title'];
        $result['total_mileage'] = $data['case_data']['total_mileage'];
        $result['total_mileage_amount'] = $data['case_data']['total_mileage_amount'];
        $result['travel_time'] = $data['case_data']['travel_time'];
        $result['depo_prep_time'] = $data['case_data']['depo_prep_time'];
        $result['total_time'] = $data['case_data']['total_time'];
        $result['total_request'] = $data['case_data']['total_request'];
        $result['dr_speciality'] = $data['case_data']['dr_speciality'];
        $result['body_partexamined'] = $data['case_data']['body_partexamined'];
       // $result['client_first'] = $data['case_data']['result']->first;

        $result['client_full'] = $data['case_data']['result']->first ." ". $data['case_data']['result']->last;
		$result['clientfullnames'] = $data['case_data']['result']->first ." ". $data['users']['client']['middle']." ". ucfirst($data['case_data']['result']->last);
		$result['clientlawfullnames'] = lcfirst($data['case_data']['result']->first) ." ". $data['users']['client']['middle']." ". lcfirst($data['case_data']['result']->last);
		
        $result['client_phome'] = (isset($data['case_data']['result']->home)) ? $data['case_data']['result']->home : "";
        $result['clientaddress1sperate'] = $data['case_data']['result']->address1;
		$result['clientaddress2sperate'] = $data['case_data']['result']->address2;
        $result['city'] = $data['case_data']['result']->city;
        $result['state'] = $data['case_data']['result']->state;
        $result['zip'] = $data['case_data']['result']->zip;
        $result['judgeNM'] = $data['case_data']['judgeNM'];
        $result['depo_time_hours'] = $data['case_data']['depo_time_hours'];
        $result['depo_time_minutes'] = $data['case_data']['depo_time_minutes'];
        $result['attorney_fees'] = $data['case_data']['attorney_fees'];
        $result['trip_mileage'] = $data['case_data']['trip_mileage'];
        $result['daily_wage'] = $data['case_data']['daily_wage'];
		
        $clientInfo = $this->getClientInformation($data);
        $DefendantInfo = $this->getDefendantInformation($data['users']);
        $attorneyInfo = $this->getattorneyInfo($data['users']);
        $result['defendantName'] = $DefendantInfo['defendantName'];
        $InsInfo = $this->getInsInformation($data['users']);
		
        $result['insName'] = $InsInfo['insName'];
		$result['insurance_name'] = $InsInfo['ins_name'];
		$result['insuranceAddress1'] = $InsInfo['insAddress1'];
		$result['insuranceAddress2'] = $InsInfo['insAddress2'];
		$result['insuranceCity'] = $InsInfo['insCity'];
		$result['insuranceState'] = $InsInfo['insState'];
		$result['insuranceZip'] = $InsInfo['insZip'];
		$result['insurancephone1'] = $InsInfo['phone1'];
        $varLawLeft = '';
        $varLawRight = '';
        
		$lawyersLeft = json_decode(lawyersLeft);
        $lawyersRight = json_decode(lawyersRight);
        foreach($lawyersLeft as $key=>$lawyersLeft)
        {
            $varLawLeft .= "{$lawyersLeft[0]}\n";
        }
        $result['lawyersLeft'] = trim($varLawLeft);

        foreach($lawyersRight as $key=>$lawyersRight)
        {
            $varLawRight .= "{$lawyersRight[0]}\n";
        }
        $result['lawyersRight'] = trim($varLawRight);

       // echo '<pre>'; print_r($data['users']); exit;

        if($data['users']['applicant'])
        {
            $result['my_middle'] = $data['users']['applicant']['middle'];
            $result['my_email'] = $data['users']['applicant']['email'];
            $result['my_occupation'] = $data['users']['applicant']['occupation'];
            $result['my_title'] = $data['users']['applicant']['title'];
            $result['licenseno'] = $data['users']['applicant']['licenseno'];
            $result['client_suffix'] = $data['users']['applicant']['suffix'];
            $result['client_fname'] = $data['users']['applicant']['first'];
            $result['client_lname'] = $data['users']['applicant']['last'];
            $result['client_sal'] = $data['users']['applicant']['salutation'];
            $result['social_sec'] = $data['users']['applicant']['social_sec'];
            $result['my_social'] = isset($result['social_sec'])?substr($result['social_sec'], 7):'';
            $result['my_MI'] = isset($result['my_middle'])?substr($result['my_middle'], 0, 1).'.':'';
            $result['birthdate'] = isset($data['users']['applicant']['birth_date'])?date('m/d/Y',strtotime($data['users']['applicant']['birth_date'])):'';
            $result['my_age'] = isset($result['birthdate'])?date_diff(date_create($result['birthdate']), date_create(date('m/d/Y')))->y.' Yrs':'';
            $result['venue'] = $data['users']['applicant']['venue'];
            $result['client_first'] = $data['users']['applicant']['first'];
            $result['firmname'] = isset($data['users']['applicant']['firm'])?$data['users']['applicant']['firm']:'';
        }
        else if($data['users']['client'])
        {
            $result['my_middle'] = $data['users']['client']['middle'];
            $result['my_email'] = $data['users']['client']['email'];
            $result['my_occupation'] = $data['users']['client']['occupation'];
            $result['my_title'] = $data['users']['client']['title'];
            $result['licenseno'] = $data['users']['client']['licenseno'];
            $result['client_suffix'] = $data['users']['client']['suffix'];
            $result['client_fname'] = $data['users']['client']['first'];
            $result['client_lname'] = $data['users']['client']['last'];
            $result['client_sal'] = $data['users']['client']['salutation'];
            $result['social_sec'] = $data['users']['client']['social_sec'];
            $result['my_social'] = isset($result['social_sec'])?substr($result['social_sec'], 7):'';
            $result['my_MI'] = isset($result['my_middle'])?substr($result['my_middle'], 0, 1).'.':'';
            $result['venue'] = $data['users']['client']['venue'];
            $result['birthdate'] = isset($data['users']['client']['birth_date'])?date('m/d/Y',strtotime($data['users']['client']['birth_date'])):'';
            $result['my_age'] = isset($result['birthdate'])?date_diff(date_create($result['birthdate']), date_create(date('m/d/Y')))->y.' Yrs':'';
            $result['client_first'] = $data['users']['client']['first'];
            $result['firmname'] = isset($data['users']['client']['firm'])?$data['users']['client']['firm']:'';
        }


        //echo '<pre>'; print_r($result); exit;
        if(!empty($data['party_location'])){
        $clientlocation = $this->getLocationInformation($data);

        $all_parties = $this->getLocationInformation($data);
        $last_party = $this->getLocationInformation2($data);
        //echo '<pre>'; print_r($clientInfo); exit;
        $partyData = '';
        $partyData1 = '';
        if(count($all_parties) > 0)
        {
            for($p=0; $p<count($all_parties); $p++)
            {
                if($p <= 3)
                {
                    if($all_parties[$p]['firm'] != '')
                    $partyData .= "{$all_parties[$p]['firm']}\n";
                    if($all_parties[$p]['name'] != '' && $all_parties[$p]['firm'] == ''){
                         if($all_parties[$p]['suffix'] != '')
                            $partyData .= "{$all_parties[$p]['name']} , {$all_parties[$p]['suffix']}\n";
                        else
                            $partyData .= "{$all_parties[$p]['name']}\n";
                    }
                    if($all_parties[$p]['address'] != ''){
                        $partyData .= "{$all_parties[$p]['address']}\n";
                    }if($all_parties[$p]['address1'] != ''){
                        $partyData .= "{$all_parties[$p]['address1']}\n";
                    }if($all_parties[$p]['address2'] != ''){
                        $partyData .= "{$all_parties[$p]['address2']}\n";
                    if($all_parties[$p]['name'] != '' && $all_parties[$p]['firm'] != ''){
                        if($all_parties[$p]['suffix'] != '')
                            $partyData .= "Attn: {$all_parties[$p]['name']} , {$all_parties[$p]['suffix']}\n";
                        else
                            $partyData .= "Attn: {$all_parties[$p]['name']}\n";
                    }
                    $partyData .= "\r\n";}
                }
                else
                {
                    //$partyData1 .= "\r\n";
                    if($all_parties[$p]['firm'] != '')
                    $partyData1 .= "{$all_parties[$p]['firm']}\n";
                    if($all_parties[$p]['name'] != '' && $all_parties[$p]['firm'] == ''){
                         if($all_parties[$p]['suffix'] != '')
                            $partyData1 .= "{$all_parties[$p]['name']} , {$all_parties[$p]['suffix']}\n";
                        else
                            $partyData1 .= "{$all_parties[$p]['name']}\n";
                    }
                    if($all_parties[$p]['address'] != '')
                        $partyData1 .= "{$all_parties[$p]['address']}\n";
                    if($all_parties[$p]['address1'] != '')
                        $partyData1 .= "{$all_parties[$p]['address1']}\n";
                    if($all_parties[$p]['address2'] != '')
                        $partyData1 .= "{$all_parties[$p]['address2']}\n";
                    if($all_parties[$p]['name'] != '' && $all_parties[$p]['firm'] != ''){
                        if($all_parties[$p]['suffix'] != '')
                            $partyData1 .= "Attn: {$all_parties[$p]['name']} , {$all_parties[$p]['suffix']}\n";
                        else
                            $partyData1 .= "Attn: {$all_parties[$p]['name']}\n";
                    }
                    $partyData1 .= "\r\n";
                }

            }
            //echo '<pre>'; print_r($partyData1); exit;
            $result['partyData']= trim($partyData);
            $result['partyData1']= trim($partyData1);

        }

        if(count($last_party) > 0)
        {
            for($p=0; $p<count($last_party); $p++)
            {
                if($p == count($last_party)-1)
                {
                    if($last_party[$p]['firm'] != ''){
                        $lastParty .= "{$last_party[$p]['firm']}\n";
                        $lastParty_frname = $last_party[$p]['firm'];
                    }
                    else{
                        if(!empty($last_party[$p]['suffix']))
                            $lastParty .= "{$last_party[$p]['name']}, {$last_party[$p]['suffix']}\n";
                        else
                            $lastParty .= "{$last_party[$p]['name']}\n";
                    }
                    if($last_party[$p]['address'] != ''){
                        $lastParty .= "{$last_party[$p]['address']}\n";
                        $lastParty_add1 = $last_party[$p]['address'];
                    }if($last_party[$p]['address1'] != ''){
                        $lastParty .= "{$last_party[$p]['address1']}\n";
                        $lastParty_add2 .= $last_party[$p]['address1'].' ';
                    }if($last_party[$p]['address2'] != ''){
                        $lastParty .= "{$last_party[$p]['address2']}\n";
                        $lastParty_add2 .= $last_party[$p]['address2'];
                    }
                    if($last_party[$p]['name'] != ''){
                        if($last_party[$p]['firm'] != ''){
                            if(!empty($last_party[$p]['suffix']))
                                $lastParty .= "Attn: {$last_party[$p]['name']}, {$last_party[$p]['suffix']}\n";
                            else
                                $lastParty .= "Attn: {$last_party[$p]['name']}\n";
                        }

                    }
                    else
                    {
                        if($last_party[$p]['suffix'] != '')
                            $partyFullNm = $last_party[$p]['name'].', '.$last_party[$p]['suffix'];
                        else
                            $partyFullNm = $last_party[$p]['name'];
                    }
                    $lastParty .= "\r\n";

                    if($last_party[$p]['phone1'] != ''){
                        $lastParty_phone1 = $last_party[$p]['phone1'];
                    }
                    if($last_party[$p]['salutation'] != '')
                    {
                        $lastParty_sal = $last_party[$p]['salutation'];
                    }
                    if($last_party[$p]['lastname'] != ''){

                        $lastParty_lname = $last_party[$p]['lastname'];
                    }else{
                        $lastParty_lname = '';
                    }


                    if($last_party[$p]['l_fname'] != '')
                        $lastParty_fname = $last_party[$p]['l_fname'];
                    else
                        $lastParty_fname = '';

                    if($last_party[$p]['suffix'] != '')
                        $lastParty_suffix = $last_party[$p]['suffix'];
                    else
                        $lastParty_suffix = '';

                    if(!empty($last_party[$p]['firm']))
                    {
                        $partyFullNm = '';
                        $partyFullNm = $last_party[$p]['firm'];
                    }else{
                        if($last_party[$p]['suffix'] != '')
                            $partyFullNm = $last_party[$p]['name'].', '.$last_party[$p]['suffix'];
                        else
                            $partyFullNm = $last_party[$p]['name'];
                    }



                }
            }



            $result['last_Party']= trim($lastParty);
            $result['last_Party_lname']= trim($lastParty_lname);
            $result['last_Party_sal']= trim($lastParty_sal);
            $result['last_Party_suffix']= trim($lastParty_suffix);
            $result['last_Party_frstname']= trim($lastParty_fname);
            $result['last_Party_frname']= trim($lastParty_frname);
            $result['last_Party_full']= trim($partyFullNm);
            $result['last_Party_add1']= trim($lastParty_add1);
            $result['last_Party_add2']= trim($lastParty_add2);
            $result['last_Party_phone1']= $lastParty_phone1;

        }
        else
        {
            $result['last_Party']='';
        }
       //echo '<pre>'; print_r($result); exit;
        //$result['parties'] = $parties;
        for($i=0;$i<count($clientlocation);$i++){
                //echo '<pre>'; print_r($clientlocation); exit;
                $result['loc_fnm'] = '';
                $result['loc_lnm'] = '';
                $result['loc_suffix'] = '';
                $result['loc_sal'] = '';
                /*if($clientlocation[$i]['firm']){
                    $result['loc_name'.$i]=$clientlocation[$i]['firm'];
                }else{
                $result['loc_name'.$i]=$clientlocation[$i]['name'];
                }*/
                
                if($clientlocation[$i]['firm']){
                    $result['loc_name'.$i]=$clientlocation[$i]['firm'];
                }
                if($clientlocation[$i]['name'])
                {
                $result['loc_name'.$i] .= ' '.$clientlocation[$i]['name'];
                }
                
                $result['loc_address'.$i]=trim($clientlocation[$i]['address']);
                $result['loc_address1'.$i]=trim($clientlocation[$i]['address1']);
                $result['loc_address2'.$i]=trim($clientlocation[$i]['address2']);
                $result['loc_phone'.$i]=$clientlocation[$i]['phone1'];

                if($clientlocation[$i]['firm'] != ''){
                    $result['loc_name']=$clientlocation[$i]['firm'];
                }else{
                    if($clientlocation[$i]['suffix'])
                        $result['loc_name']=$clientlocation[$i]['name'].', '.$clientlocation[$i]['suffix'];
                    else
                        $result['loc_name']=$clientlocation[$i]['name'];
                }
                $result['loc_party']=$clientlocation[$i]['party'];
                $result['loc_suffix']=$clientlocation[$i]['suffix'];
                $result['loc_sal']=$clientlocation[$i]['salutation'];
                if($clientlocation[$i]['lastname'] != '' || $clientlocation[$i]['firstname'] != '')
                {
                    $result['loc_last_name']=$clientlocation[$i]['lastname'];
                    $result['loc_first_name']=$clientlocation[$i]['firstname'];
                    $result['loc_fnm']=$clientlocation[$i]['firstname'];
                    $result['loc_lnm']=$clientlocation[$i]['lastname'];
                }
                else
                {
                    $result['loc_first_name']=$clientlocation[$i]['firm'];
                }


                if($clientlocation[$i]['firm'] != '')
                {

                    $result['locWFirmName']=$clientlocation[$i]['firm'];
                    $result['my_locWFirmName']=$clientlocation[$i]['firm'];


                }
                else
                {
                    $result['locWFirmName']=$clientlocation[$i]['lastname']." ".$clientlocation[$i]['firstname'];
                    $result['my_locWFirmName']='';
                }

                $result['loc_firmname']=$clientlocation[$i]['firm'];
                
                $loc_full = '';
                if($clientlocation[$i]['firstname'] != '' || $clientlocation[$i]['lastname'] != '')
                {
                    $loc_full .= trim($clientlocation[$i]['firstname']);
                    $loc_full .= ' '.trim($clientlocation[$i]['lastname']);
                    if(!empty($clientlocation[$i]['suffix'])){
                       $loc_full .=  ', '.$clientlocation[$i]['suffix'];
                    }
                }
                if(!empty($result['loc_firmname']))
                {
                    $loc_full .= "\n{$result['loc_firmname']}";
                }
                if(!empty($clientlocation[$i]['address'])){
                    $loc_full .= "\n{$clientlocation[$i]['address']}";
                }
                if(!empty($clientlocation[$i]['address1'])){
                    $loc_full .= "\n{$clientlocation[$i]['address1']}";
                }
                if(!empty($clientlocation[$i]['address2'])){
                    $loc_full .= "\n{$clientlocation[$i]['address2']}";
                }
                
                $result['loc_full'] = $loc_full;
                $result['loc_address']=$clientlocation[$i]['address'];
                $addr = '';
                if(!empty($clientlocation[$i]['address1']))
                {
                    $result['loc_address'] = $result['loc_address'].', '.$clientlocation[$i]['address1'];
                    $result['loc_address1']=$clientlocation[$i]['address1'];
                
                }
                if(!empty($clientlocation[$i]['address2']))
                {
                    $addr .= trim($clientlocation[$i]['address2']);
                    $result['loc_address2']=$addr;
                
                }
                $result['loc_phone1']=$clientlocation[$i]['phone1'];
				$result['loc_bussiness']=$clientlocation[$i]['business'];
                $result['loc_fax']=$clientlocation[$i]['fax'];
            }
        }
        //echo '<pre>'; print_r($result); exit;
        if(!empty($data['cc_party'])){
            $CC_party = $this->getPartyInformation($data);
            $cc_pname = '';

            for($i=0;$i<count($CC_party);$i++){
              if($CC_party[$i]['name'] != '' && $CC_party[$i]['firm'] == '')
              {
                  $cc_pname .= "{$CC_party[$i]['name']}\n";
              }
              if($CC_party[$i]['firm'] != '' && $CC_party[$i]['name'] != '')
              {
                   $fullname = "Attn : " . $CC_party[$i]['name'].", ".$CC_party[$i]['firm'];
                   $cc_pname .= "{$fullname}\n";
              }
              if($CC_party[$i]['firm'] != '' && $CC_party[$i]['name'] == '')
              {
                  $cc_pname .= "{$CC_party[$i]['firm']}\n";
              }
            }
            $result['cc_party_name'] = $cc_pname;
        }
        $result['clientName'] = $clientInfo['clientName'];
        $result['clientFNM'] = $clientInfo['clientFNM'];
        $result['clientLNM'] = $clientInfo['clientLNM'];
        $result['clientWFirmName'] = $clientInfo['clientWFirmName'];
        $result['clientFirm'] = $clientInfo['clientFirm'];
        $result['clientIName'] = $clientInfo['clientIName'];
        $result['clientfull'] = $clientInfo['clientfull'];
        $result['clientIAddress1'] = $clientInfo['clientIAddress1'];
        $result['clientIAddress2'] = $clientInfo['clientIAddress2'];
        $result['clientAddress1'] = $clientInfo['clientAddress1'];
        $result['clientAddress2'] = $clientInfo['clientAddress2'];
        $result['clientCity'] = $clientInfo['clientCity'];
        $result['clientState'] = $clientInfo['clientState'];
        $result['clientZip'] = $clientInfo['clientZip'];
        $result['clientboardName'] = $clientInfo['clientboardName'];
        $result['clientboardOnly'] = $clientInfo['clientboardOnly'];
        $result['clientboardAddress1'] = $clientInfo['clientboardAddress1'];
        $result['clientboardAddress2'] = $clientInfo['clientboardAddress2'];
        $result['board_City'] = $clientInfo['board_city'];
        $result['home'] =$clientInfo['home'];
        $result['business'] =$clientInfo['business'];
        $result['car'] =$clientInfo['car'];
        $result['fax'] =$clientInfo['fax'];

        $result['attnm'] = $attorneyInfo['attnm'];
        $result['att_suff'] = $attorneyInfo['att_suff'];
        $result['att_nm'] = $attorneyInfo['att_nm'];
        $result['att_lnm'] = $attorneyInfo['att_lnm'];
        $result['att_sal'] = $attorneyInfo['att_sal'];
        $result['att_firm'] = $attorneyInfo['att_firm'];
        $result['attAddress1'] = $attorneyInfo['attAddress1'];
        $result['attAddress2'] = $attorneyInfo['attAddress2'];
        $result['attCity'] = $attorneyInfo['attCity'];
        $result['attState'] = $attorneyInfo['attState'];
        $result['attZip'] = $attorneyInfo['attZip'];
        $result['att_phone'] = $attorneyInfo['att_phone'];
		$result['defAddress1'] = $DefendantInfo['defAddress1'];
        $result['defCity'] = $DefendantInfo['defCity'];
        $result['defState'] = $DefendantInfo['defState'];
        $result['defState'] = $DefendantInfo['defState'];
        $result['defZip'] = $DefendantInfo['defZip'];

        $result['ins_name'] = $InsInfo['ins_name'];
        $result['insAddress1'] = $InsInfo['insAddress1'];
        $result['insCity'] = $InsInfo['insCity'];
        $result['insState'] = $InsInfo['insState'];
        $result['insZip'] = $InsInfo['insZip'];


        $result['attorneyname'] = $clientInfo['attorneyname'];
        $result['atty_first'] = $clientInfo['atty_first'];
        $result['atty_last'] = $clientInfo['atty_last'];
        $result['atty_hand'] = $clientInfo['atty_hand'];
        $result['lusername'] = $clientInfo['lusername'];

//        $result['firmname'] = isset($data['case_data']['result']->firm)?$data['case_data']['result']->firm:'';
        $result['address1'] = isset($data['case_data']['result']->address1)?$data['case_data']['result']->address1:'';
        $result['firstname'] = $data['case_data']['result']->first;
        $result['lastname'] = $data['case_data']['result']->last;
        $result['address2'] = '';
        //$result['caption'] = $data['case_data']['result']->caption1;

        $result['fileno'] = $data['case_data']['result']->yourfileno;

//        $result['birthdate'] = isset($data['case_data']['result']->birth_date)?date('m/d/Y',strtotime($data['case_data']['result']->birth_date)):'';
        $result['staff'] = $this->ci->session->userdata('user_data')['username'];
//        if($data['case_data']['result']->social_sec!='')
//        {
//             $result['social_sec'] =$data['case_data']['result']->social_sec;
//        }
        //$result['home'] =$data['case_data']['result']->home;
        //$result['business'] =$data['case_data']['result']->business;
       // $result['car'] =$data['case_data']['result']->car;
       // $result['fax'] =$data['case_data']['result']->fax;

        if ($data['case_data']['result']->city != '') {
            $result['address2'] .= $data['case_data']['result']->city;
             $result['city'] = $data['case_data']['result']->city;
        }
        if ($data['case_data']['result']->state != '') {
            $result['address2'] .= ", " . $data['case_data']['result']->state;
            //$result['state'] = ", " . $data['case_data']['result']->state;
			$result['state'] = $data['case_data']['result']->state;
        }if ($data['case_data']['result']->zip != '') {
            $result['address2'] .= " " . $data['case_data']['result']->zip;
            $result['zip'] = $data['case_data']['result']->zip;
        }
        $result['phone1'] = $data['case_data']['result']->phone1;
        $result['caseno'] = $data['case_data']['caseno'];
        $result['dateenter'] = date('m/d/Y',strtotime($data['case_data']['result']->dateenter));
        //echo '<pre>'; print_r($result);exit;

	if(isset($data['injury_details']))
        {
			//$bodyParts = (array)bodyParts;
			$bodyParts = json_decode(bodyPartsshort);
                        $bodypadrtsArray = [];
                        if(!empty($bodyParts)) {
                            foreach($bodyParts as $key => $value) :
                                $bodypadrtsArray[$key] = $key . " " . $value;
                            endforeach;
                        }
            $injuryArray = $this->getInjury($data['injury_details'],$data['case_data']['addinj']);
			if($data['case_data']['fileNumber'] == '10861' || $data['case_data']['fileNumber'] == '10856' || $data['case_data']['fileNumber'] == '10874' || $data['case_data']['fileNumber'] == '10031' || $data['case_data']['fileNumber'] == '10048' || $data['case_data']['fileNumber'] == '10864' || $data['case_data']['fileNumber'] == '10422' || $data['case_data']['fileNumber'] == '10864' || $data['case_data']['fileNumber'] == '10863' || $data['case_data']['fileNumber'] == '10886'||$data['case_data']['fileNumber'] == '10890' || $data['case_data']['fileNumber'] == '434' || $data['case_data']['fileNumber'] == '470' || $data['case_data']['fileNumber'] == '10074' || $data['case_data']['fileNumber'] == '10076' || $data['case_data']['fileNumber'] == '10859' || $data['case_data']['fileNumber'] == '10891' || $data['case_data']['fileNumber'] == '11216' || $data['case_data']['fileNumber'] == '10875' || $data['case_data']['fileNumber'] == '10089' || $data['case_data']['fileNumber'] == '10091' || $data['case_data']['fileNumber'] == '10886' || $data['case_data']['fileNumber'] == '10876' || $data['case_data']['fileNumber'] == '1CalPERSApp2018'){
               	$pob1 = '';
			$pob2 = '';
			$pob3 = '';
			$pob4 = '';
			$pob5 = '';


               for($i=0;$i<count($data['injury_details']);$i++)
               {
                    
                    foreach ($bodyParts as $key => $value) {
                        if (!empty($data['injury_details'][$i]['pob1']) && $key == $data['injury_details'][$i]['pob1']) {
                                $pob1 = $key . ' ' . $value . ",";
                        }
                        if (!empty($data['injury_details'][$i]['pob2']) && $key == $data['injury_details'][$i]['pob2']) {
                                $pob2 = $key . ' ' . $value . ",";
                        }
                        if (!empty($data['injury_details'][$i]['pob3']) && $key == $data['injury_details'][$i]['pob3']) {
                                $pob3 = $key . ' ' . $value . ",";
                        }
                        if (!empty($data['injury_details'][$i]['pob4']) && $key == $data['injury_details'][$i]['pob4']) {
                                $pob4 = $key . ' ' . $value . ",";
                        }
                        if (!empty($data['injury_details'][$i]['pob5']) && $key == $data['injury_details'][$i]['pob5']) {
                                        $pob5 = $key . ' ' . $value . ",";
                                }
                    }
                    $result['fadj1d'.$i]=isset($injuryArray['fieldAdj1d'][$i])?$injuryArray['fieldAdj1d'][$i]:'';
                    $result['fpob1'.$i]=isset($bodypadrtsArray[$injuryArray['pob1'][$i]])? $bodypadrtsArray[$injuryArray['pob1'][$i]]:'';
                    $result['fpob2'.$i]=isset($bodypadrtsArray[$injuryArray['pob2'][$i]])? $bodypadrtsArray[$injuryArray['pob2'][$i]]:'';
                    $result['fpob3'.$i]=isset($bodypadrtsArray[$injuryArray['pob3'][$i]])? $bodypadrtsArray[$injuryArray['pob3'][$i]]:'';
                    $result['fpob4'.$i]=isset($bodypadrtsArray[$injuryArray['pob4'][$i]])? $bodypadrtsArray[$injuryArray['pob4'][$i]]:'';
                    $result['fpob5'.$i]=isset($bodypadrtsArray[$injuryArray['pob5'][$i]])? $bodypadrtsArray[$injuryArray['pob5'][$i]]:'';

                    $result['npob1'.$i]=isset($injuryArray['pob1'][$i])? $injuryArray['pob1'][$i]:'';
                    $result['npob2'.$i]=isset($injuryArray['pob2'][$i])? $injuryArray['pob2'][$i]:'';
                    $result['npob3'.$i]=isset($injuryArray['pob3'][$i])? $injuryArray['pob3'][$i]:'';
                    $result['npob4'.$i]=isset($injuryArray['pob4'][$i])? $injuryArray['pob4'][$i]:'';
                    $result['npob5'.$i]=isset($injuryArray['pob5'][$i])? $injuryArray['pob5'][$i]:'';

                    if (!empty($injuryArray['pob1']) && $key == $injuryArray['pob1']) {
                            $singlepob1 = $key . ' ' . $value . ",";
                    }
                    if (!empty($injuryArray['pob2']) && $key == $injuryArray['pob2']) {
                            $singlepob2 = $key . ' ' . $value . ",";
                    }
                    if (!empty($injuryArray['pob3']) && $key == $injuryArray['pob3']) {
                            $singlepob3 = $key . ' ' . $value . ",";
                    }
                    if (!empty($injuryArray['pob4']) && $key == $injuryArray['pob4']) {
                            $singlepob4 = $key . ' ' . $value . ",";
                    }
                    if (!empty($injuryArray['pob5']) && $key == $injuryArray['pob5']) {
                            $singlepob5 = $key . ' ' . $value . ",";
                    }

                    $result['feamsno'.$i]=isset($injuryArray['wcabCaseNo'][$i])?$injuryArray['wcabCaseNo'][$i]:'';
                    $result['fadj1e'.$i]=isset($injuryArray['adj1e'][$i])?$injuryArray['adj1e'][$i]:'';
                    $result['i_type'.$i]=isset($injuryArray['i_type'][$i])?$injuryArray['i_type'][$i]:'0';
                    $result['single_i'.$i]=isset($injuryArray['single_i'][$i])?$injuryArray['single_i'][$i]:'';
                    $result['inj_dt'.$i]=isset($injuryArray['inj_dt'][$i])?$injuryArray['inj_dt'][$i]:'';
                }  
            }
            else if($data['case_data']['fileNumber'] == '10871'){
               // echo '<pre>'; print_r($data['injury_details']); exit;
               for($i=0;$i<count($data['injury_details']);$i++)
               {
                   if(strtolower($data['injury_details'][$i]['app_status']) != 'closed'){
                   if(isset($data['injury_details'][$i]['case_no']) && $data['injury_details'][$i]['case_no'] != ''){
                        $wcabCaseNo = $data['injury_details'][$i]['case_no'];
                    }
                    if(isset($data['injury_details'][$i]['eamsno']) && $data['injury_details'][$i]['eamsno'] != ''){
                        $wcabCaseNo = 'ADJ'.$data['injury_details'][$i]['eamsno'];
                    }
                    
                        $result['feamsno'.$i]=$wcabCaseNo; 
                        $sbstr = substr($data['injury_details'][$i]['adj1c'],0,3);
                        /*if($sbstr == 'CT:')
                        {
                            $newdt = explode("CT: ",$data['injury_details'][$i]['adj1c']);
                            $result['inj_dt'.$i] = $newdt[1];
                        }
                        else
                        {*/
                            $result['inj_dt'.$i] = $data['injury_details'][$i]['adj1c'];
                        //}
                        if(!empty($data['injury_details'][$i]['i_claimno'])){
                           $result['r_i_claimno'][] = $data['injury_details'][$i]['i_claimno'];
                        }
                        if(!empty($data['injury_details'][$i]['i2_claimno'])){
                           $result['r_i_claimno'][] = $data['injury_details'][$i]['i2_claimno'];
                        }
                        
                   }
                   
               }
            }
            
           //echo '<pre>'; print_r($result); exit; 
        }
        if(isset($data['client_defendant']))
        {
           $result['client_defendant'] = $data['client_defendant'][0]['first']."".$data['client_defendant'][0]['last'];
        }

                $result['claimNo'] = !empty($injuryArray['claimNo']) ? implode(', ', $injuryArray['claimNo']) : '';
                $result['r_i_claimno'] = !empty($result['r_i_claimno']) ? implode(', ', $result['r_i_claimno']) : '';
		$result['pob1'] = !empty($pob1) ? $pob1 : '';
		$result['pob2'] = !empty($pob2) ? $pob2 : '';
		$result['pob3'] = !empty($pob3) ? $pob3 : '';
		$result['pob4'] = !empty($pob4) ? $pob4 : '';
                $result['pob5'] = !empty($pob5) ? $pob5 : '';
                
                $result['singlepob1'] = !empty($singlepob1) ? $singlepob1 : '';
		$result['singlepob2'] = !empty($singlepob2) ? $singlepob2 : '';
		$result['singlepob3'] = !empty($singlepob3) ? $singlepob3 : '';
		$result['singlepob4'] = !empty($singlepob4) ? $singlepob4 : '';
                $result['singlepob5'] = !empty($singlepob5) ? $singlepob5 : '';

		$result['DOI'] = !empty($injuryArray['DOI']) ? implode(', ', $injuryArray['DOI']) : '';
                $result['DOI_all'] = !empty($injuryArray['DOI_all']) ? implode(', ', $injuryArray['DOI_all']) : '';
		$result['DOI_start'] = !empty($injuryArray['DOI_start']) ? implode(', ', $injuryArray['DOI_start']) : '';
		$result['DOI_end'] = !empty($injuryArray['DOI_end']) ? implode(', ', $injuryArray['DOI_end']) : '';
        $result['DOI_first'] = !empty($injuryArray['DOI_first']) ? $injuryArray['DOI_first']: '';
        $result['Ename_last'] = !empty($injuryArray['Ename_last']) ? $injuryArray['Ename_last']: '';
        $result['Iname_last'] = !empty($injuryArray['Iname_last']) ? $injuryArray['Iname_last']: '';
		$result['Iaddress_last'] = !empty($injuryArray['Iaddress_last']) ? $injuryArray['Iaddress_last']: '';
		$result['Icity_last'] = !empty($injuryArray['Icity_last']) ? $injuryArray['Icity_last']: '';
		$result['Istate_last'] = !empty($injuryArray['Istate_last']) ? $injuryArray['Istate_last']: '';
		$result['Izip_last'] = !empty($injuryArray['Izip_last']) ? $injuryArray['Izip_last']: '';
		$result['Iphone_last'] = !empty($injuryArray['Iphone_last']) ? $injuryArray['Iphone_last']: '';
        $result['wcabCaseNo'] = !empty($injuryArray['wcabCaseNo']) ? implode(', ', array_filter($injuryArray['wcabCaseNo'])) : '';
        //echo '<pre>'; print_r($injuryArray); exit;
        $result['WcabNoEamsNO'] = !empty($injuryArray['WcabNoEamsNO']) ? implode(', ', $injuryArray['WcabNoEamsNO']) : '';
        $result['fieldEname'] = !empty($injuryArray['fieldEname']) ? $injuryArray['fieldEname'][0]: '';
        $result['injurycity'] = !empty($injuryArray['injurycity']) ? implode(', ', $injuryArray['injurycity']) : '';
        $result['adj1e'] = !empty($injuryArray['adj1e']) ? implode(', ', $injuryArray['adj1e']) : '';
        $result['adj2a'] =!empty($injuryArray['adj2a']) ? implode(', ', $injuryArray['adj2a']) : '';
        $result['adj3a'] =!empty($injuryArray['adj3a']) ? implode(', ', $injuryArray['adj3a']) : '';
        $result['adj2a_last'] =!empty($injuryArray['adj2a_last']) ? $injuryArray['adj2a_last'] : '';
        $result['adj1e_last'] =!empty($injuryArray['adj1e_last']) ? $injuryArray['adj1e_last'] : '';
        $result['adj1a_birthday'] = isset($injuryArray['adj1a_birthday']) ? implode(', ', $injuryArray['adj1a_birthday']) : '';
        $result['fieldEaddress'] = !empty($injuryArray['fieldEaddress']) ?  $injuryArray['fieldEaddress'][0] : '';
        $result['fieldEaddress2'] = !empty($injuryArray['fieldEaddress2']) ? $injuryArray['fieldEaddress2'][0] : '';
        $result['fieldEcity'] = !empty($injuryArray['fieldEcity']) ? $injuryArray['fieldEcity'][0] : '';
        $result['fieldEstate'] = !empty($injuryArray['fieldEstate']) ?  $injuryArray['fieldEstate'][0] : '';
        $result['fieldEzip'] = !empty($injuryArray['fieldEzip']) ?    $injuryArray['fieldEzip'][0] : '';
        $result['fieldEphone'] = !empty($injuryArray['fieldEphone']) ?    $injuryArray['fieldEphone'][0] : '';
        $result['fieldEfax'] = !empty($injuryArray['fieldEfax']) ?    $injuryArray['fieldEfax'][0] : '';


        $result['fieldIadjuster'] = !empty($injuryArray['fieldIadjuster']) ?$injuryArray['fieldIadjuster'][0] : '';
        $result['fieldIname'] = !empty($injuryArray['fieldIname']) ?$injuryArray['fieldIname'][0] : '';
        $result['fieldIaddress'] = !empty($injuryArray['fieldIaddress']) ? $injuryArray['fieldIaddress'][0] : '';

        $result['fieldIcity'] = !empty($injuryArray['fieldIcity']) ?$injuryArray['fieldIcity'][0] : '';
        $result['fieldIstate'] =!empty($injuryArray['fieldIstate']) ?$injuryArray['fieldIstate'][0] : '';
        $result['fieldIzip'] = !empty($injuryArray['fieldIzip']) ?$injuryArray['fieldIzip'][0] : '';
        $result['fieldIphone'] = !empty($injuryArray['fieldIphone']) ?  $injuryArray['fieldIphone'][0] : '';
        //print_r($injuryArray['fieldd1phone'][0]); exit;
        $result['fieldd1firm'] = !empty($injuryArray['fieldd1firm']) ? $injuryArray['fieldd1firm'][0] : '';
        $result['fieldd1firstlast'] = !empty($injuryArray['fieldd1firstlast']) ? $injuryArray['fieldd1firstlast'][0] : '';
        $result['fieldd1address'] = !empty($injuryArray['fieldd1address']) ? $injuryArray['fieldd1address'][0] : '';
        $result['fieldd1address2'] = !empty($injuryArray['fieldd1address2']) ? $injuryArray['fieldd1address2'][0] : '';
        $result['fieldAdj1b'] = !empty($injuryArray['fieldAdj1b']) ? $injuryArray['fieldAdj1b'][0] : '';
		$result['fieldAdj1a'] = !empty($injuryArray['fieldAdj1a']) ? $injuryArray['fieldAdj1a'][0] : '';
		$result['fieldAdj1d'] = !empty($injuryArray['fieldAdj1d']) ? $injuryArray['fieldAdj1d'][0] : '';

		$result['sfieldAdj1a'] = !empty($injuryArray['fieldAdj1a']) ? implode(', ', $injuryArray['fieldAdj1a']) : '';

        $result['fieldAdj10city'] = !empty($injuryArray['fieldAdj10city']) ? implode(', ', $injuryArray['fieldAdj10city']) : '';
        $result['fieldd1phone'] = !empty($injuryArray['fieldd1phone']) ? $injuryArray['fieldd1phone'][0] : '';

        //tab 5 no 5

        $result['fieldadj5b'] = !empty($injuryArray['fieldadj5b']) ? $injuryArray['fieldadj5b'][0] : '';
        $result['fieldadj5c'] = !empty($injuryArray['fieldadj5c']) ? implode(', ', $injuryArray['fieldadj5c']) : '';
        $result['fieldadj5d'] = !empty($injuryArray['fieldadj5d']) ? $injuryArray['fieldadj5d'][0] : '';
        //tab 6 no 6

        $result['fielddoctors'] = !empty($injuryArray['fielddoctors']) ? $injuryArray['fielddoctors'][0] : '';
        $result['fieldadj7d'] = !empty($injuryArray['fieldadj7d']) ? $injuryArray['fieldadj7d'][0] : '';
		$result['fieldadj9g'] = !empty($injuryArray['fieldadj9g']) ? $injuryArray['fieldadj9g'][0] : '';
        $result['fieldadj7b'] = !empty($injuryArray['fieldadj7b']) ? $injuryArray['fieldadj7b'][0] : '';
        $result['fieldadj7a'] = !empty($injuryArray['fieldadj7a']) ? $injuryArray['fieldadj7a'][0] : '';
        $result['fieldadj7c'] = !empty($injuryArray['fieldadj7c']) ? $injuryArray['fieldadj7c'][0] : '';
        $result['fieldadj5a'] = !empty($injuryArray['fieldadj5a']) ? $injuryArray['fieldadj5a'][0] : '';
        $result['fieldadj6a'] = !empty($injuryArray['fieldadj6a']) ? $injuryArray['fieldadj6a'][0] : '';
        $result['fieldadj7e'] = !empty($injuryArray['fieldadj7e']) ? $injuryArray['fieldadj7e'][0] : '';
        $result['fieldadj9a'] = !empty($injuryArray['fieldadj9a']) ? $injuryArray['fieldadj9a'][0] : '';
        $result['fieldadj9b'] = !empty($injuryArray['fieldadj9b']) ? $injuryArray['fieldadj9b'][0] : '';
        $result['fieldadj9c'] = !empty($injuryArray['fieldadj9c']) ? $injuryArray['fieldadj9c'][0] : '';
        $result['fieldadj9d'] = !empty($injuryArray['fieldadj9d']) ? $injuryArray['fieldadj9d'][0] : '';
        $result['fieldadj9e'] = !empty($injuryArray['fieldadj9e']) ? $injuryArray['fieldadj9e'][0] : '';
        $result['fieldadj9f'] = !empty($injuryArray['fieldadj9f']) ? $injuryArray['fieldadj9f'][0] : '';


        //echo $injuryArray['DOI'][0];exit;
        //more then 5 inury
        $result['DOI1'] = isset($injuryArray['DOI'][0]) ?$injuryArray['DOI'][0] : '';
        $result['DOI2'] = isset($injuryArray['DOI'][1]) ?$injuryArray['DOI'][1] : '';
        $result['DOI3'] = isset($injuryArray['DOI'][2]) ?$injuryArray['DOI'][2] : '';
        $result['DOI4'] = isset($injuryArray['DOI'][3]) ?$injuryArray['DOI'][3] : '';
        $result['DOI5'] = isset($injuryArray['DOI'][4]) ?$injuryArray['DOI'][4] : '';
        $result['DOI_end1'] = isset($injuryArray['DOI_end'][0])&& !empty($injuryArray['DOI_end'][0]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][0])) : '';
        $result['DOI_end2'] = isset($injuryArray['DOI_end'][1])&& !empty($injuryArray['DOI_end'][1]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][1])) : '';
        $result['DOI_end3'] = isset($injuryArray['DOI_end'][2])&& !empty($injuryArray['DOI_end'][2]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][2])) : '';
        $result['DOI_end4'] = isset($injuryArray['DOI_end'][3])&& !empty($injuryArray['DOI_end'][3]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][3])) : '';
        $result['DOI_end5'] = isset($injuryArray['DOI_end'][4])&& !empty($injuryArray['DOI_end'][4]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][4])) : '';
        $result['DOI_end6'] = isset($injuryArray['DOI_end'][5])&& !empty($injuryArray['DOI_end'][5]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][5])) : '';
        $result['DOI_end7'] = isset($injuryArray['DOI_end'][6])&& !empty($injuryArray['DOI_end'][6]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][6])) : '';
        $result['DOI_end8'] = isset($injuryArray['DOI_end'][7])&& !empty($injuryArray['DOI_end'][7]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][7])) : '';
        $result['DOI_end9'] = isset($injuryArray['DOI_end'][8])&& !empty($injuryArray['DOI_end'][8]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][8])) : '';
        $result['DOI_end10'] = isset($injuryArray['DOI_end'][9])&& !empty($injuryArray['DOI_end'][9]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][9])) : '';
        $result['DOI_end11'] = isset($injuryArray['DOI_end'][10])&& !empty($injuryArray['DOI_end'][10]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][10])) : '';
        $result['DOI_end12'] = isset($injuryArray['DOI_end'][11])&& !empty($injuryArray['DOI_end'][11]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][11])) : '';
        $result['DOI_end13'] = isset($injuryArray['DOI_end'][12])&& !empty($injuryArray['DOI_end'][12]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][12])) : '';
        $result['DOI_end14'] = isset($injuryArray['DOI_end'][13])&& !empty($injuryArray['DOI_end'][13]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][13])) : '';
        $result['DOI_end15'] = isset($injuryArray['DOI_end'][14])&& !empty($injuryArray['DOI_end'][14]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][14])) : '';
        $result['DOI_end16'] = isset($injuryArray['DOI_end'][15])&& !empty($injuryArray['DOI_end'][15]) ? date('m/d/Y', strtotime($injuryArray['DOI_end'][15])) : '';
        

        $result['DOI_start1'] = isset($injuryArray['DOI_start'][0]) && !empty($injuryArray['DOI_start'][0]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][0])) : '';
        $result['DOI_start2'] = isset($injuryArray['DOI_start'][1])&& !empty($injuryArray['DOI_start'][1]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][1])) : '';
        $result['DOI_start3'] = isset($injuryArray['DOI_start'][2])&& !empty($injuryArray['DOI_start'][2]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][2])) : '';
        $result['DOI_start4'] = isset($injuryArray['DOI_start'][3])&& !empty($injuryArray['DOI_start'][3]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][3])) : '';
        $result['DOI_start5'] = isset($injuryArray['DOI_start'][4])&& !empty($injuryArray['DOI_start'][4]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][4])) : '';
        $result['DOI_start6'] = isset($injuryArray['DOI_start'][5])&& !empty($injuryArray['DOI_start'][5]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][5])) : '';
        $result['DOI_start7'] = isset($injuryArray['DOI_start'][6])&& !empty($injuryArray['DOI_start'][6]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][6])) : '';
        $result['DOI_start8'] = isset($injuryArray['DOI_start'][7])&& !empty($injuryArray['DOI_start'][7]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][7])) : '';
        $result['DOI_start9'] = isset($injuryArray['DOI_start'][8])&& !empty($injuryArray['DOI_start'][8]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][8])) : '';
        $result['DOI_start10'] = isset($injuryArray['DOI_start'][9])&& !empty($injuryArray['DOI_start'][9]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][9])) : '';
        $result['DOI_start11'] = isset($injuryArray['DOI_start'][10])&& !empty($injuryArray['DOI_start'][10]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][10])) : '';
        $result['DOI_start12'] = isset($injuryArray['DOI_start'][11])&& !empty($injuryArray['DOI_start'][11]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][11])) : '';
        $result['DOI_start13'] = isset($injuryArray['DOI_start'][12])&& !empty($injuryArray['DOI_start'][12]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][12])) : '';
        $result['DOI_start14'] = isset($injuryArray['DOI_start'][13])&& !empty($injuryArray['DOI_start'][13]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][13])) : '';
        $result['DOI_start15'] = isset($injuryArray['DOI_start'][14])&& !empty($injuryArray['DOI_start'][14]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][14])) : '';
        $result['DOI_start16'] = isset($injuryArray['DOI_start'][15])&& !empty($injuryArray['DOI_start'][15]) ? date('m/d/Y', strtotime($injuryArray['DOI_start'][15])) : '';

        //wcabCaseNo 5
        $result['wcab1'] = isset($injuryArray['wcabCaseNo'][0]) ?$injuryArray['wcabCaseNo'][0] : '';
        $result['wcab2'] = isset($injuryArray['wcabCaseNo'][1]) ?$injuryArray['wcabCaseNo'][1] : '';
        $result['wcab3'] = isset($injuryArray['wcabCaseNo'][2]) ?$injuryArray['wcabCaseNo'][2] : '';
        $result['wcab4'] = isset($injuryArray['wcabCaseNo'][3]) ?$injuryArray['wcabCaseNo'][3] : '';
        $result['wcab5'] = isset($injuryArray['wcabCaseNo'][4]) ?$injuryArray['wcabCaseNo'][4] : '';
        $result['wcab6'] = isset($injuryArray['wcabCaseNo'][5]) ?$injuryArray['wcabCaseNo'][5] : '';
        $result['wcab7'] = isset($injuryArray['wcabCaseNo'][6]) ?$injuryArray['wcabCaseNo'][6] : '';
        $result['wcab8'] = isset($injuryArray['wcabCaseNo'][7]) ?$injuryArray['wcabCaseNo'][7] : '';
        $result['wcab9'] = isset($injuryArray['wcabCaseNo'][8]) ?$injuryArray['wcabCaseNo'][8] : '';
        $result['wcab10'] = isset($injuryArray['wcabCaseNo'][9]) ?$injuryArray['wcabCaseNo'][9] : '';
        $result['wcab11'] = isset($injuryArray['wcabCaseNo'][10]) ?$injuryArray['wcabCaseNo'][10] : '';
        $result['wcab12'] = isset($injuryArray['wcabCaseNo'][11]) ?$injuryArray['wcabCaseNo'][11] : '';
        $result['wcab13'] = isset($injuryArray['wcabCaseNo'][12]) ?$injuryArray['wcabCaseNo'][12] : '';
        $result['wcab14'] = isset($injuryArray['wcabCaseNo'][13]) ?$injuryArray['wcabCaseNo'][13] : '';
        $result['wcab15'] = isset($injuryArray['wcabCaseNo'][14]) ?$injuryArray['wcabCaseNo'][14] : '';
        $result['wcab16'] = isset($injuryArray['wcabCaseNo'][15]) ?$injuryArray['wcabCaseNo'][15] : '';
        
        //echo '<pre>'; print_r($result); exit;
        if(!empty($result['client_suffix']))
        {
            $caption = ucfirst($result['client_fname']).' '.ucfirst($result['client_lname']).", ".$result['client_suffix'].' vs '.ucfirst($result['defendantName']);
        }
        else
        {
            $caption = ucfirst($result['client_fname']).' '.ucfirst($result['client_lname']).' vs. '.ucfirst($result['defendantName']);
        }
        $result['caption'] = $caption;
        //echo '<pre>'; print_r($result); exit;
        //system data

        //*Code For form starts (Namrata)*//
        $result['doi1_startdate'] = (isset($data['injury_details'][0]['doi']) && $data['injury_details'][0]['doi'] != "") ? date('m/d/Y', strtotime($data['injury_details'][0]['doi'])) : "";
        $result['doi1_enddate'] = (isset($data['injury_details'][0]['doi2']) && $data['injury_details'][0]['doi2'] != "") ? date('m/d/Y', strtotime($data['injury_details'][0]['doi2'])) : "";
        $result['caseuserBdate'] = (isset($data['users']['attorney']['birth_date']) && $data['users']['attorney']['birth_date'] != "") ? date('m/d/Y', strtotime($data['users']['attorney']['birth_date'])) : "";
        //*Code For form ends (Namrata)*//

        $main1='';
        $result['fcity']=isset($data['system_data'][0]->firmcity)?$data['system_data'][0]->firmcity:'';
        $result['firmaddr1']=isset($data['system_data'][0]->firmaddr1)?$data['system_data'][0]->firmaddr1:'';
        $result['firmstatezip']=isset($data['system_data'][0]->firmstate)?$data['system_data'][0]->firmstate:''."".isset($data['system_data'][0]->firmzip)?$data['system_data'][0]->firmzip:'';
        $result['firmphone']=isset($data['system_data'][0]->firmphone)?$data['system_data'][0]->firmphone:'';
        $result['firmzip']=isset($data['system_data'][0]->firmzip)?$data['system_data'][0]->firmzip:'';
        $result['firmstate']=isset($data['system_data'][0]->firmstate)?$data['system_data'][0]->firmstate:'';
        if(isset($data['system_data'][0])){
         $main1=json_decode($data['system_data'][0]->main1);
         $result['firmcountry']=isset($main1->country)?$main1->country:'';
         $result['EAMSRefNo']=isset($main1->EAMSRefNo)?$main1->EAMSRefNo:'';
        }
        $result['firmfax']=isset($data['system_data'][0]->firmfax)?$data['system_data'][0]->firmfax:'';
        $result['sfirmname']=isset($data['system_data'][0]->firmname)?$data['system_data'][0]->firmname:'';
        $result['firmfletter']=isset($data['system_data'][0]->fletter1)?$data['system_data'][0]->fletter1:'';
        //login user full name
        $result['logindetails']=$data['case_data']['logindetail'];
        $result['SeectedInjuryDetails']= (isset($data['SeectedInjuryDetails']) && !empty($data['SeectedInjuryDetails'])) ? $data['SeectedInjuryDetails'] : [];
        $result['selectedtinsparty']= (isset($data['selectedtinsparty']) && !empty($data['selectedtinsparty'])) ? $data['selectedtinsparty'] : [];
        $result['selectedempparty']= (isset($data['selectedempparty']) && !empty($data['selectedempparty'])) ? $data['selectedempparty'] : [];
        $result['selecteddefatt']= (isset($data['selecteddefatt']) && !empty($data['selecteddefatt'])) ? $data['selecteddefatt'] : [];
	$result['selectedphy']= (isset($data['selectedphy']) && !empty($data['selectedphy'])) ? $data['selectedphy'] : [];	
        $result['injury_details'] = $data['injury_details'];
        $data=$this->create_doc_file($result);

        if(!empty($data)){  return $data;}// exit;
    }

    public function setEnvelope($data){
        // $GLOBALS['address'] = $data['address'];
        $caseNo = $data['caseNo'];
        unset($data['caseNo']);
        $datetime = date('Y-m-d-H-i-s');
        $TBS = new clsTinyButStrong; // new instance of TBS
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load the OpenTBS plugin
        $template = DOCUMENTPATH.'clients/doc_forms/Envelope.docx';

        $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8);

        $TBS->MergeBlock('BlockName', $data);
        
        if(!is_dir(DOCUMENTPATH.'clients/'.$caseNo)){
            $oldmask = umask(0);
            mkdir(DOCUMENTPATH.'clients/'.$caseNo,0777);
            umask($oldmask);
        }
       $extwe = '.docx';
        $output_file_name = DOCUMENTPATH.'clients/'.$caseNo."/".'Envelope-'.$datetime .$extwe;
          $TBS->Show(OPENTBS_FILE , $output_file_name);
          $output_file_name_to_return = ''.$caseNo. "/".'Envelope-'.$datetime .$extwe;
          return $output_file_name_to_return;
    }

    public function getClientInformation($data) {

       //echo "<pre>";
        //print_r($data);exit;

        $clientInfo = array();

        /*$clientInfo['clientName'] = $data['case_data']['clientName'];
        $clientInfo['clientAddress1'] = $data['case_data']['clientAddress1'];
        $clientInfo['clientAddress2'] = $data['case_data']['clientAddress2'];*/
        $clientInfo['attorneyname'] = $data['case_data']['attorneyname'];
        $clientInfo['atty_first'] = $data['case_data']['atty_first'];
        $clientInfo['atty_last'] = $data['case_data']['atty_last'];
        $clientInfo['atty_hand'] = $data['case_data']['atty_hand'];
        $clientInfo['lusername'] = $data['case_data']['lusername'];
        $clientInfo['clientCity'] = $data['case_data']['city'];
        $clientInfo['clientState'] = $data['case_data']['state'];
        $clientInfo['clientZip'] = $data['case_data']['zip'];
        $clientInfo['clientFirm'] = $data['case_data']['firm'];
        // echo $data['case_data']['fileNumber'];exit;
        if($data['case_data']['fileNumber'] == '405'){
            if(isset($data['users']['board'])){

                $clientInfo['clientName'] = $this->getName($data['users']['board']);
                $address = $this->getAddress($data['users']['board']);
                $clientInfo['clientAddress1'] = $address['address1'];
                $clientInfo['clientAddress2'] = $address['address2'];
                $clientInfo['clientCity'] = $address['city'];
                $clientInfo['clientState'] = $address['state'];
                $clientInfo['clientZip'] = $address['zip'];
            }
        }

        if($data['case_data']['fileNumber'] == '486' || $data['case_data']['fileNumber'] == '10857' || $data['case_data']['fileNumber'] == '10864'){
            if(isset($data['users']['insurance'])){
                    $clientInfo['clientIName'] = $this->getName($data['users']['insurance']);
                    if(isset($data['users']['insurance']['firm']))
                    {
                        $clientInfo['clientfull'] = $data['users']['insurance']['firm'];
                    }
                    else{
                        if($data['users']['insurance']['suffix'])
                            $clientInfo['clientfull'] = $data['users']['insurance']['first'].' '.$data['users']['insurance']['last'].', '.$data['users']['insurance']['suffix'];
                        else
                          $clientInfo['clientfull'] = $data['users']['insurance']['first'].' '.$data['users']['insurance']['last'];
                    }
                    $address = $this->getAddress($data['users']['insurance']);
                    $clientInfo['clientIAddress1'] = $address['address1'];
                    $clientInfo['clientIAddress2'] = $address['address2'];
                    $clientInfo['clientCity'] = $address['city'];
                    $clientInfo['clientState'] = $address['state'];
                    $clientInfo['clientZip'] = $address['zip'];
            }

        }

        if($data['case_data']['fileNumber'] == '10857' || $data['case_data']['fileNumber'] == '10864' ||  $data['case_data']['fileNumber'] == '407' || $data['case_data']['fileNumber'] == '469'  ){
            if(isset($data['users']['insurance'])){
                    $clientInfo['clientIName'] = $this->getName($data['users']['insurance']);
                    if(isset($data['users']['insurance']['firm']))
                    {
                        $clientInfo['clientfull'] = $data['users']['insurance']['firm'];
                    }
                    else{
                        if($data['users']['insurance']['suffix'])
                            $clientInfo['clientfull'] = $data['users']['insurance']['first'].' '.$data['users']['insurance']['last'].', '.$data['users']['insurance']['suffix'];
                        else
                          $clientInfo['clientfull'] = $data['users']['insurance']['first'].' '.$data['users']['insurance']['last'];
                    }
                    $address = $this->getAddress($data['users']['insurance']);
                    $clientInfo['lusername'] = $data['users']['insurance']['last'];
                    $clientInfo['clientIAddress1'] = $address['address1'];
                    $clientInfo['clientIAddress2'] = $address['address2'];
                    $clientInfo['clientCity'] = $address['city'];
                    $clientInfo['clientState'] = $address['state'];
                    $clientInfo['clientZip'] = $address['zip'];
                    $clientInfo['clienteamsref'] = $data['users']['insurance']['eamsref'];
                    //echo '<pre>';print_r($clientInfo);exit;
            }
         }

         if($data['case_data']['fileNumber'] == '469'  ){
            if(isset($data['users']['insurance'])){
                    if(isset($data['users']['insurance']['firm']))
                    {
                        $applicant_name = $data['users']['insurance']['firm'];

                        if (isset($data['users']['insurance']['first']) && $data['users']['insurance'] != '') {
                            $fullname .= ', Attn : '.$data['users']['insurance']['first'].' '.$data['users']['insurance']['last'];
                        }
                        $applicant_name .= "{$fullname}";
                        if (isset($data['users']['insurance']['suffix']) && $data['users']['insurance']['suffix'] != '') {
                            $applicant_name .= ' , ' . $data['users']['insurance']['suffix'];
                        }
                    }
                    else{
                        $applicant_name = isset($data['users']['insurance']['salutation']) ? $data['users']['insurance']['salutation'] : '';
                        if (isset($data['users']['insurance']['first']) && $data['users']['insurance']['first'] != '') {
                            $applicant_name .= ' ' . $data['users']['insurance']['first'];
                        }
                        if (isset($data['users']['insurance']['middle']) && $data['users']['insurance']['middle'] != '') {
                            $applicant_name .= ' ' . $data['users']['insurance']['middle'];
                        }
                        if (isset($data['users']['insurance']['last']) && $data['users']['insurance']['last'] != '') {
                            $applicant_name .= ' ' . $data['users']['insurance']['last'];
                        }
                        if (isset($data['users']['insurance']['suffix']) && $data['users']['insurance']['suffix'] != '') {
                            $applicant_name .= ' , ' . $data['users']['insurance']['suffix'];
                        }
                    }
                    $clientInfo['clientIName'] = $applicant_name;
                    $address = $this->getAddress($data['users']['insurance']);
                    $clientInfo['lusername'] = $data['users']['insurance']['last'];
                    $clientInfo['clientIAddress1'] = $address['address1'];
                    $clientInfo['clientIAddress2'] = $address['address2'];
                    $clientInfo['clientCity'] = $address['city'];
                    $clientInfo['clientState'] = $address['state'];
                    $clientInfo['clientZip'] = $address['zip'];
					$clientInfo['eamsref'] = $data['users']['insurance']['eamsref'];
            }
         }


        //print_r($clientInfo); exit;
        if(isset($data['users']['board'])){

            $clientInfo['clientboardName'] = $this->getBName($data['users']['board']);
            $address = $this->getAddress($data['users']['board']);
            $clientInfo['clientboardAddress1'] = $address['address1'];
            $clientInfo['clientboardAddress2'] = $address['address2'];
            $clientInfo['board_city'] = $data['users']['board']['city'];
            $clientInfo['clientboardOnly'] = $this->getOnlyBName($data['users']['board']);
        }
        else
        {
            $clientInfo['clientboardName'] = '';
            $clientInfo['clientboardOnly'] = '';
            $clientInfo['clientboardAddress1'] = '';
            $clientInfo['clientboardAddress2'] = '';
        }

        if(isset($data['users']['client'])){
                $clientNM = '';
                //$clientNM = $this->getName($data['users']['client']);
                if(isset($data['users']['client']['first']))
                {
                    $clientNM .= $data['users']['client']['first'];
                    $clientFNM = $data['users']['client']['first'];
                }
                if(isset($data['users']['client']['last']))
                {
                    $clientNM .= " ".trim($data['users']['client']['last']);
                    $clientLNM = $data['users']['client']['last'];
                }
                if(isset($data['users']['client']['suffix']))
                {
                    $clientNM .= ', '.trim($data['users']['client']['suffix']);
                }
//                if(isset($data['users']['client']['firm']))
//                {
//                    $clientNM .= "\n{$data['users']['client']['firm']}";
//                }
                $clientInfo['clientName'] = $clientNM;
                if($data['users']['applicant']['firm'] != ''){
                    $clientInfo['clientWFirmName'] = "{$clientNM}\n{$data['users']['applicant']['firm']}";
                }
                else
                {
                    $clientInfo['clientWFirmName'] = $clientNM;
                }
                $clientInfo['clientFirm'] = $data['users']['client']['firm'];
                $address = $this->getAddress($data['users']['client']);
                $clientInfo['clientAddress1'] = $address['address1'];
                $clientInfo['clientAddress2'] = $address['address2'];
                $clientInfo['clientCity'] = $address['city'];
                $clientInfo['clientState'] = $address['state'];
                $clientInfo['clientZip'] = $address['zip'];
                $clientInfo['home'] =$data['users']['client']['home'];
                $clientInfo['business'] =$data['users']['client']['business'];
                $clientInfo['car'] =$data['users']['client']['car'];
                $clientInfo['fax'] =$data['users']['client']['fax'];
            }

        if(isset($data['users']['applicant'])){

                //$clientNM = $this->getName($data['users']['client']);
                $clientNM = '';
                if(isset($data['users']['applicant']['first']))
                {
                    $clientNM .= $data['users']['applicant']['first'];
                    $clientFNM = $data['users']['applicant']['first'];
                }
                if(isset($data['users']['applicant']['last']))
                {
                    $clientNM .= " ".trim($data['users']['applicant']['last']);
                    $clientLNM = $data['users']['applicant']['last'];
                }
                /*if(isset($data['users']['applicant']['suffix']))
                {
                    $clientNM .= ', '.trim($data['users']['applicant']['suffix']);
                }*/
//                if(isset($data['users']['client']['firm']))
//                {
//                    $clientNM .= "\n{$data['users']['client']['firm']}";
//                }
                $clientInfo['clientName'] = $clientNM;
                if($data['users']['applicant']['firm'] != ''){
                    $clientInfo['clientWFirmName'] = "{$clientNM}\n{$data['users']['applicant']['firm']}";
                }
                else
                {
                    $clientInfo['clientWFirmName'] = $clientNM;
                }
                $clientInfo['clientFirm'] = $data['users']['applicant']['firm'];
                $address = $this->getAddress($data['users']['applicant']);
                $clientInfo['clientAddress1'] = $address['address1'];
                $clientInfo['clientAddress2'] = $address['address2'];
                $clientInfo['clientCity'] = $address['city'];
                $clientInfo['clientState'] = $address['state'];
                $clientInfo['clientZip'] = $address['zip'];
                $clientInfo['home'] =$data['users']['applicant']['home'];
                $clientInfo['business'] =$data['users']['applicant']['business'];
                $clientInfo['car'] =$data['users']['applicant']['car'];
                $clientInfo['fax'] =$data['users']['applicant']['fax'];
            }
        return $clientInfo;
            //print_r($clientInfo); exit;

    }
    
    public function getDefendantInformation($data) {
        //echo '<pre>'; print_r($data); exit;
        $clientInfo = array();
        if(count($data)>0){
            if(isset($data['defendant'])){

                $clientInfo['defendantName'] = $this->getDefName($data['defendant']);
                $address = $this->getAddress($data['defendant']);
            }
            else if(isset($data['employer']))
            {
                $clientInfo['defendantName'] = $this->getDefName($data['employer']);
                $address = $this->getAddress($data['employer']);
            }
            else
            {
                $clientInfo['defendantName'] = '';
            }
            
            $clientInfo['defAddress1'] = $address['address1'];
            $clientInfo['defAddress2'] = $address['address2'];
            $clientInfo['defCity'] = $address['city'];
            $clientInfo['defState'] = $address['state'];
            $clientInfo['defZip'] = $address['zip'];
        }
        return $clientInfo;
    }

    public function getattorneyInfo($data) {
        //echo '<pre>'; print_r($data['attorney']); exit;
        $clientInfo = array();
        if(count($data)>0){
            if(isset($data['attorney'])){

                $clientInfo['attnm'] = $this->getattnm($data['attorney']);
            }
            else
            {
                $clientInfo['attnm'] = '';
            }
            $clientInfo['att_lnm'] = $data['attorney']['last'];
            $clientInfo['att_sal'] = $data['attorney']['salutation'];
            $clientInfo['att_nm'] = $data['attorney']['first'].' '.$data['attorney']['last'];
            $clientInfo['att_suff'] = $data['attorney']['suffix'];
            $clientInfo['att_firm'] = $data['attorney']['firm'];
            $clientInfo['att_phone'] = $data['attorney']['business'];
            $address = $this->getAddress($data['attorney']);
            $clientInfo['attAddress1'] = $address['address1'];
            $clientInfo['attAddress2'] = $address['address2'];
            $clientInfo['attCity'] = $address['city'];
            $clientInfo['attState'] = $address['state'];
            $clientInfo['attZip'] = $address['zip'];
        }
        //echo '<pre>'; print_r($clientInfo); exit;
        return $clientInfo;
    }
     public function getInsInformation($data) {

        $clientInfo = array();
		
        if(count($data)>0){
            if(isset($data['insurance'])){

                $clientInfo['insName'] = $this->getInsName($data['insurance']);
            }
            else
            {
                $clientInfo['insName'] = '';
            }
			
            $clientInfo['ins_name'] = $data['insurance']['first'].' '.$data['insurance']['last'];
            $address = $this->getAddress($data['insurance']);
	    $clientInfo['insAddress1'] = $address['address1'];
            $clientInfo['insAddress2'] = $address['address2'];
            $clientInfo['insCity'] = $address['city'];
            $clientInfo['insState'] = $address['state'];
            $clientInfo['insZip'] = $address['zip'];
			$clientInfo['phone1'] = $data['insurance']['phone1'];
        }
        return $clientInfo;
    }
    public function getLocationInformation($data){
        //echo '<pre>'; print_r($data);
        //echo 'count => ' . count($data['party_location']);
        //exit;
         //if($data['case_data']['fileNumber'] == '10006'){
            $location1=array(); //
            foreach($data['display_all_details'] as $key=>$value)
            {
                for($i=0;$i<count($data['party_location']);$i++){
                $address2 = '';
                $fullname = '';

                if(in_array($data['party_location'][$i], $value)){
                    //$location1[$i]['name']=isset($value['first'])?$value['first']:''.' '.isset($value['last'])?$value['last']:'';
                    if($data['party_location'][$i] == $value['cardcode']){
                        if(isset($value['first']))
                        {
                            $fullname .= $value['first'].' ';
                        }
                        if(isset($value['last']))
                        {
                            $fullname .= $value['last'];
                        }
                        $location1[$i]['name'] = $fullname;
                        $fullname = '';
                        $location1[$i]['lastname']=isset($value['last'])?$value['last']:'';
                        $location1[$i]['firstname']=isset($value['first'])?$value['first']:'';
                        $location1[$i]['suffix']=isset($value['suffix'])?$value['suffix']:'';
                        $location1[$i]['salutation']=isset($value['salutation'])?$value['salutation']:'';
                        $location1[$i]['address']=isset($value['address1'])?trim($value['address1']):'';
                        $location1[$i]['address1']=isset($value['address2'])?trim($value['address2']):'';
                        if(isset($value['city']))
                        {
                            $address2 .= trim($value['city']).', ';
                        }
                        if(isset($value['state']))
                        {
                            $address2 .= trim($value['state'])." ";
                        }
                        if(isset($value['zip']))
                        {
                            $address2 .= trim($value['zip']);
                        }
                        $location1[$i]['address2'] = trim($address2);
                        $address2 = '';
                        $party = '';
                        $location1[$i]['fname']= $value['zip'];
                        $location1[$i]['party']= $party;
                       // $location1[$i]['address2']=isset($value['city'])?$value['city']:''." , ".isset($value['state'])?$value['state']:''." , ".isset($value['zip'])?$value['zip']:'';
                        $location1[$i]['phone1']=isset($value['phone1'])?$value['phone1']:'';
                        $location1[$i]['business']=isset($value['business'])?$value['business']:'';
                        $location1[$i]['fax']=isset($value['fax'])?$value['fax']:'';
                        $location1[$i]['firm']=isset($value['firm'])?$value['firm']:'';
                    }
                    }
                }
            }
        //echo '<pre>'; print_r($location1); exit;
        return $location1;


         //}
    }

    public function getLocationInformation2($data){
        //echo '<pre>'; print_r($data['lastParty']); exit;
         //if($data['case_data']['fileNumber'] == '10006'){
            $location1=array(); //print_r($data['display_all_details']);
            foreach($data['display_all_details'] as $key=>$value)
            {
                for($i=0;$i<count($data['lastParty']);$i++){
                $address2 = '';
                $fullname = '';

                if(in_array($data['lastParty'][$i], $value)){
                    //$location1[$i]['name']=isset($value['first'])?$value['first']:''.' '.isset($value['last'])?$value['last']:'';
                 if($data['lastParty'][$i] == $value['cardcode']){
                    if(isset($value['first']))
                    {
                        $fullname .= $value['first'].' ';
                    }
                    if(isset($value['last']))
                    {
                        $fullname .= $value['last'];
                    }
                    $location1[$i]['name'] = $fullname;
                    $fullname = '';
                    $location1[$i]['lastname']=isset($value['last'])?$value['last']:'';
                    $location1[$i]['l_fname']=isset($value['first'])?$value['first']:'';
                    $location1[$i]['address']=isset($value['address1'])?$value['address1']:'';
                    $location1[$i]['address1']=isset($value['address2'])?$value['address2']:'';
                    if(isset($value['city']))
                    {
                        $address2 .= $value['city'].', ';
                    }
                    if(isset($value['state']))
                    {
                        $address2 .= $value['state']." ";
                    }
                    if(isset($value['zip']))
                    {
                        $address2 .= $value['zip'];
                    }
                    $location1[$i]['address2'] = $address2;
                    $address2 = '';
                    $party = '';
                    $location1[$i]['fname']= $value['zip'];
                    $location1[$i]['party']= $party;
                   // $location1[$i]['address2']=isset($value['city'])?$value['city']:''." , ".isset($value['state'])?$value['state']:''." , ".isset($value['zip'])?$value['zip']:'';
                    $location1[$i]['phone1']=isset($value['phone1'])?$value['phone1']:'';
                    $location1[$i]['business']=isset($value['business'])?$value['business']:'';
                    $location1[$i]['firm']=isset($value['firm'])?$value['firm']:'';
                    $location1[$i]['suffix']=isset($value['suffix'])?$value['suffix']:'';
                    $location1[$i]['salutation']=isset($value['salutation'])?$value['salutation']:'';
                 }
                }
                }

            }

        return $location1;


         //}
    }

    public function getPartyInformation($data){
        //echo '<pre>'; print_r($data['display_all_details']); exit;
         //if($data['case_data']['fileNumber'] == '10006'){
            $location1=array(); //print_r($data['display_all_details']);
            foreach($data['display_all_details'] as $key=>$value)
            {
                for($i=0;$i<count($data['cc_party']);$i++){

                $fullname = '';
                $address2 = '';
                $party = '';
                if(in_array($data['cc_party'][$i], $value)){
                    //$location1[$i]['name']=isset($value['first'])?$value['first']:''.' '.isset($value['last'])?$value['last']:'';
                    //echo '<pre>'; print_r($value);
                    //echo '<pre>'; print_r($data['cc_party'][$i]);
                   if($data['cc_party'][$i] == $value['cardcode']){
                        if(isset($value['first']))
                        {
                            $fullname .= $value['first'].' ';
                        }
                        if(isset($value['last']))
                        {
                            $fullname .= $value['last'];
                        }
                        if(isset($value['suffix'])&&!empty($value['suffix']))
                        {
                            $fullname .= ", ".$value['suffix'];
                        }

                        $location1[$i]['name'] = $fullname;
                        $fullname = '';
                        $location1[$i]['lastname']=isset($value['last'])?$value['last']:'';
                        $location1[$i]['address']=isset($value['address1'])?$value['address1']:'';
                        $location1[$i]['address1']=isset($value['address2'])?$value['address2']:'';
                        if(isset($value['city']))
                        {
                            $address2 .= $value['city'].', ';
                        }
                        if(isset($value['state']))
                        {
                            $address2 .= $value['state']." ";
                        }
                        if(isset($value['zip']))
                        {
                            $address2 .= $value['zip'];
                        }
                        $location1[$i]['address2'] = $address2;
                        $address2 = '';

                        $location1[$i]['fname']= $value['zip'];
                        $location1[$i]['party']= $party;
                        $party = '';
                       // $location1[$i]['address2']=isset($value['city'])?$value['city']:''." , ".isset($value['state'])?$value['state']:''." , ".isset($value['zip'])?$value['zip']:'';
                        $location1[$i]['phone1']=isset($value['phone1'])?$value['phone1']:'';
                        $location1[$i]['firm']=isset($value['firm'])?$value['firm']:'';
                    }
                }
                }
            }
        //echo '<pre>'; print_r($location1); exit;
        return $location1;


         //}
    }

    public function getName($display_details){

        if(isset($display_details['firm']))
        {
            $applicant_name = trim($display_details['firm']);

            if (isset($display_details['first']) && $display_details['first'] != '') {
                $applicant_name= trim($applicant_name);
                $fullname .= trim($display_details['first']).' '.trim($display_details['last']);
                $applicant_name = "{$applicant_name}\n".trim("Attn: {$fullname}");
                if (isset($display_details['suffix']) && $display_details['suffix'] != '') {
                    $applicant_name .= ', ' . $display_details['suffix'];
                }
            }
            
            //echo $applicant_name; exit;
        }
        else{
            $applicant_name = isset($display_details['salutation']) ? $display_details['salutation'] : '';
            if (isset($display_details['first']) && $display_details['first'] != '') {
                $applicant_name .= ' ' . $display_details['first'];
            }
            if (isset($display_details['middle']) && $display_details['middle'] != '') {
                $applicant_name .= ' ' . $display_details['middle'];
            }
            if (isset($display_details['last']) && $display_details['last'] != '') {
                $applicant_name .= ' ' . $display_details['last'];
            }
            if (isset($display_details['suffix']) && $display_details['suffix'] != '') {
                $applicant_name .= ' , ' . $display_details['suffix'];
            }
        }

        return trim($applicant_name);
    }

    public function getBName($display_details){
        $applicant_name = isset($display_details['salutation']) ? $display_details['salutation'] : '';
        if (isset($display_details['first']) && $display_details['first'] != '') {
            $applicant_name .= ' ' . $display_details['first'];
        }
        if (isset($display_details['last']) && $display_details['last'] != '') {
            $applicant_name .= ' ' . $display_details['last'];
        }
        if (isset($display_details['firm']) && $display_details['firm'] != '') {
                $applicant_name .= "\n{$display_details['firm']}";
        }

        return trim($applicant_name);
    }
    
    public function getOnlyBName($display_details){
        if (isset($display_details['firm']) && $display_details['firm'] != '') {
                $applicant_name = $display_details['firm'];
        }

        return trim($applicant_name);
    }

    public function getDefName($display_details){
        if (isset($display_details['firm']) && $display_details['firm'] != '') {
                $applicant_name = $display_details['firm'];
            }
        return $applicant_name;
    }

    public function getattnm($display_details){
        if (isset($display_details['firm']) && $display_details['firm'] != '') {
            $applicant_name = $display_details['firm'];
        }
        else{
            $applicant_name = $display_details['first'].' '.$display_details['last'];
        }
        return $applicant_name;
    }


    public function getInsName($display_details){
        if (isset($display_details['firm']) && $display_details['firm'] != '') {
                $applicant_name = $display_details['firm'];
            }
        return $applicant_name;
    }


    public function getAddress($address){
        $address_array = array();
        $address_array['address1'] = '';
        $address_array['address2'] = '';

        if(!empty($address['address1'])){
            $address_array['address1'] .= $address['address1'];
        }
        if(!empty($address['address2'])){
            $address_array['address1'] .= ', '.$address['address2'];
        }
        if(isset($address['city'])){
            $address_array['address2'] = trim($address['city']);
            $address_array['city'] = trim($address['city']);
        }
        if(isset($address['state'])){
           // $address_array['address2']  = isset($address_array['address2']) ? ", ".$address_array['state'] : $address_array['state'];
            $address_array['address2'] .= trim(', '.$address['state']);
            $address_array['state'] = trim($address['state']);
        }
        if(isset($address['zip'])){
            $address_array['address2'] .= ' '.trim($address['zip']);
            $address_array['zip'] = $address['zip'];
           // $address_array['address2']  = isset($address_array['address2']) ? ", ".$address_array['zip'] : $address_array['zip'];
        }
        //print_r($address_array); exit;
        return $address_array;
    }

    public function getInjury($injuryDetails,$addInj){

        $addInj = explode(",",$addInj);
        //echo '<pre>'; print_r($injuryDetails); exit;
        $injuryArray = array();
        $injuryArray['claimNo'] = array();
        $injuryArray['DOI'] = array();
        $injuryArray['DOI_start'] = array();
        $injuryArray['DOI_end'] = array();
        $injuryArray['wcabCaseNo'] = array();
        $injuryArray['adj1e'] = array();//body part
        $injuryArray['adj2a'] = array();//. The Injury occured as follows
        //employer
        $injuryArray['adj3a'] = array();
        $injuryArray['fieldEname'] = array();
        $injuryArray['fieldIphone']=array();
        $injuryArray['fieldEaddress'] = array();
        $injuryArray['fieldEaddress2']=array();
        $injuryArray['fieldEcity']=array();
        $injuryArray['fieldEstate']=array();
        $injuryArray['fieldEzip']=array();
        $injuryArray['fieldEphone']=array();
        $injuryArray['fieldEfax']=array();


        //carriers
        $injuryArray['fieldIadjuster']=array();
        $injuryArray['fieldIname']=array();
        $injuryArray['fieldIaddress']=array();
        $injuryArray['fieldIphone']=array();
        $injuryArray['fieldIcity']=array();
        $injuryArray['fieldIstate']=array();
        $injuryArray['fieldIzip']=array();
        $injuryArray['fieldIphone']=array();
        $injuryArray['fieldIfax']=array();
        //
        $injuryArray['injurycity']=array();
        $injuryArray['adj1a_birthday']=array();//injured emp birthday
        $injuryArray['fieldAdj10city']=array();//city
        $injuryArray['fieldAdj1b']=array();//injured emp work
		$injuryArray['fieldAdj1a']=array();//injured emp work
		$injuryArray['sfieldAdj1d']=array();//injured emp work
		$injuryArray['fieldAdj1d']=array();//injured emp work
        $injuryArray['fieldFirmatty1']=array();//Applicant Attorney
        //Attorneys
        $injuryArray['fieldd1firm']=array();// firm
        $injuryArray['fieldd1firstlast']=array();//first last name
        $injuryArray['fieldd1address']= array();// attarny address
        $injuryArray['fieldd1address2']= array();//attorneys city state,zip
        $injuryArray['fieldd1phone']=array();// d1 phone
        //no 5 tab 6
        $injuryArray['fieldadj5b']=array();
        $injuryArray['fieldadj5c']=array();
        $injuryArray['fieldadj5d']=array();
        //no 7 tab 7
         $injuryArray['fielddoctors']=array();
         $injuryArray['fieldadj7d']=array();
		 $injuryArray['fieldadj9g']=array();
         $injuryArray['fieldadj7b']=array();
         $injuryArray['fieldadj7a']=array();
         $injuryArray['fieldadj7c']=array();
         $injuryArray['fieldadj5a']=array();
         $injuryArray['fieldadj6a']=array();
         $injuryArray['fieldadj7e']=array();
         $injuryArray['fieldadj9a']=array();
         $injuryArray['fieldadj9b']=array();
         $injuryArray['fieldadj9c']=array();
         $injuryArray['fieldadj9d']=array();
         $injuryArray['fieldadj9e']=array();
         $injuryArray['fieldadj9f']=array();
         $injuryArray['WcabNoEamsNO']=array();
         
        if(count($injuryDetails) > 0){
            foreach($injuryDetails as $key=>$result){
                $WcabNoEamsNO = "";
                $injuryArray['Ename_last'] = '';
                $injuryArray['Ename_first'] = '';
                $injuryArray['Iname_last'] = '';
				$injuryArray['Iaddress_last'] = '';
				$injuryArray['Icity_last'] = '';
				$injuryArray['Istate_last'] = '';
				$injuryArray['Izip_last'] = '';
				$injuryArray['Iphone_last'] = '';
                                
                                /*if(empty($addInj)){
                                    echo 'empty';
                                }
                                else{
                                    echo 'not empty';
                                }
                                echo 'variable=>'; print_r($addInj); exit;*/
                foreach ($addInj as $key => $value) {
                    if (empty($value)) {
                       unset($addInj[$key]);
                    }
                }
	
                if(strtolower($result['app_status']) != 'closed'){ 
                if(in_array($result['injury_id'],$addInj) || empty($addInj)){ 
                    //echo 'in loop';
                    /*body parts of selected injury Dinky*/
                    if((isset($result['pob1'])&& $result['pob1'] != ''))
                    {
                       $injuryArray['pob1'][] = isset($result['pob1'])?$result['pob1']:'';
                    }
                    if((isset($result['pob2'])&& $result['pob2'] != ''))
                    {
                       $injuryArray['pob2'][] = isset($result['pob2'])?$result['pob2']:'';
                    }
                    if((isset($result['pob3'])&& $result['pob3'] != ''))
                    {
                       $injuryArray['pob3'][] = isset($result['pob3'])?$result['pob3']:'';
                    }
                    if((isset($result['pob4'])&& $result['pob4'] != ''))
                    {
                       $injuryArray['pob4'][] = isset($result['pob4'])?$result['pob4']:'';
                    }
                    if((isset($result['pob5'])&& $result['pob5'] != ''))
                    {
                       $injuryArray['pob5'][] = isset($result['pob5'])?$result['pob5']:'';
                    }
                    if((isset($result['case_no']) && $result['case_no'] != '')){
                        $WcabNoEamsNO = $result['case_no'] . ", " . 'ADJ'. $result['eamsno'];
                    } elseif((isset($result['case_no']) && $result['case_no'] != '') && (isset($result['eamsno']) && $result['eamsno'] == '')) {
                         $WcabNoEamsNO = $result['case_no'];
                    } elseif((isset($result['eamsno']) && $result['eamsno'] != '')) {
                        $WcabNoEamsNO = 'ADJ'.$result['eamsno'];
                    } else {
                        $WcabNoEamsNO = "";
                    }
                    
                    //echo '<pre> before'; print_r($WcabNoEamsNO);
                    //var_dump($WcabNoEamsNO);
                    //&& !in_array($WcabNoEamsNO, $injuryArray['WcabNoEamsNO'])
                    if(isset($WcabNoEamsNO)) { 
                        $injuryArray['WcabNoEamsNO'][] = $WcabNoEamsNO; 
                        
                    }
                    if(isset($result['eamsno']) && $result['eamsno'] != ''){
                        $wcabCaseNo = 'ADJ'.$result['eamsno'];
                    }else if(isset($result['case_no']) && $result['case_no'] != ''){
                        $wcabCaseNo = $result['case_no'];
                    }
                    else{
                        $wcabCaseNo = '';
                    }

					 //&& !in_array($wcabCaseNo, $injuryArray['wcabCaseNo'])
                    if(isset($wcabCaseNo)) { $injuryArray['wcabCaseNo'][] = $wcabCaseNo; }
                    if(isset($result['i_claimno']) && $result['i_claimno'] != '' && !in_array($result['i_claimno'], $injuryArray['claimNo'])){
                        $injuryArray['claimNo'][] = $result['i_claimno'];
                    }
                    if(isset($result['i2_claimno']) && $result['i2_claimno'] != '' && !in_array($result['i2_claimno'], $injuryArray['claimNo'])){
                        $injuryArray['claimNo'][] = $result['i2_claimno'];
                    }
                    if(isset($result['adj1e']) && $result['adj1e'] != ''){
                        $injuryArray['adj1e'][] = $result['adj1e'];
                        $injuryArray['adj1e_last'] = $result['adj1e'];
                    }
                    if(isset($result['adj1c']) && $result['adj1c'] != ''){
                        $IDate = explode(' ',$result['adj1c']);
                         $injuryArray['DOI'][] = $IDate[1];
                    }
                    
                    if(isset($result['adj1c']) && $result['adj1c'] != ''){
                        $injuryArray['DOI_all'][] = $result['adj1c'];
                    }
                    /*if(isset($result['doi']) && $result['doi'] != ''){
                         $injuryArray['DOI_start'][] = $result['doi'];
                    }*/
                    /*if(isset($result['doi2']) && $result['doi2'] != ''){
                         $injuryArray['DOI_end'][] = $result['doi2'];
                    }*/

                    if(isset($result['adj10city']) && $result['adj10city'] != '' && !in_array($result['adj10city'], $injuryArray['injurycity'])){
                        $injuryArray['injurycity'][] = $result['adj10city'];
                    }
                    if(isset($result['adj1a']) && $result['adj1a'] != '' && $result['adj1a'] != '0000-00-00 00:00:00'&& !in_array($result['adj1a'], $injuryArray['injurycity'])){
                        $injuryArray['adj1a_birthday'][] = $result['adj1a'];
                    }
                    /*if(isset($result['i_claimno']) && $result['i_claimno'] != '') {
                        $wcabCaseNo = $result['i_claimno'];
                    }*/
                    if(isset($result['adj1a']) && $result['adj1a'] != ''){
                        $injuryArray['adj1a'][] = $result['adj1a'];
                    }
                    if(isset($result['e_name']) && $result['e_name'] != ''){
                        $injuryArray['fieldEname'][] = $result['e_name'];
                    }
                    if(isset($result['e_address']) && $result['e_address'] != ''){
                       $injuryArray['fieldEaddress'][] = $result['e_address'];
                    }
                    else{
                        $injuryArray['fieldEaddress'][] = '';
                    }
                    if(((isset($result['e_city'])&&$result['e_city'] != '') )|| ((isset($result['e_state'])&&$result['e_state'] != '') )|| ((isset($result['e_zip'])&&$result['e_zip'] != '') ))
                    {
                        $injuryArray['fieldEaddress2'][] = $result['e_city'].' ,'.$result['e_state'].' '.$result['e_zip'].'';
                   }
                   else if(((isset($result['e_city'])&&$result['e_city'] == '') )|| ((isset($result['e_state'])&&$result['e_state'] == '') )|| ((isset($result['e_zip'])&&$result['e_zip'] == '') )) {
                       $injuryArray['fieldEaddress2'][] = '';
                   }
                    if(((isset($result['e_city'])&&$result['e_city'] != '') ))
                    {
                        $injuryArray['fieldEcity'][] = $result['e_city'];
                    }
                    else{
                        $injuryArray['fieldEcity'][] = '';
                    }
                    if((isset($result['e_state'])&&$result['e_state'] != ''))
                    {
                        $injuryArray['fieldEstate'][] = $result['e_state'];
                    }else{
                        $injuryArray['fieldEstate'][] = '';
                    }
                    if ((isset($result['e_zip'])&&$result['e_zip'] != ''))
                    {
                        $injuryArray['fieldEzip'][] = $result['e_zip'];
                    }else{
                        $injuryArray['fieldEzip'][] = '';
                    }
                    if ((isset($result['e_fax'])&&$result['e_fax'] != ''))
                    {
                        $injuryArray['fieldEfax'][] = $result['e_fax'];
                    }
                    if ((isset($result['e_phone'])&&$result['e_phone'] != ''))
                    {
                        $injuryArray['fieldEphone'][] = $result['e_phone'];
                    }

                  //Carrier
                    if(((isset($result['i_adjfst'])&&$result['i_adjfst'] != '')) ||((isset($result['i_adjuster'])&&$result['i_adjuster'] != '')))
                    {
                        $injuryArray['fieldIadjuster'][] = isset($result['i_adjfst'])?$result['i_adjfst']:''."".isset($result['i_adjuster'])?$result['i_adjuster']:'';
                   }
                   if((isset($result['i_name'])&&$result['i_name'] != ''))
                    {
                        $injuryArray['fieldIname'][] = $result['i_name'];
                    }

                   if((isset($result['i_address'])&&$result['i_address'] != ''))
                    {
                        $injuryArray['fieldIaddress'][] = $result['i_address'];
                   }
                    if((isset($result['i_city'])&&$result['i_city'] != ''))
                    {
                        $injuryArray['fieldIcity'][] = $result['i_city'];
                   }
                    if((isset($result['i_state'])&&$result['i_state'] != ''))
                    {
                        $injuryArray['fieldIstate'][] = $result['i_state'];
                    }
                    if((isset($result['i_zip'])&&$result['i_zip'] != ''))
                    {
                        $injuryArray['fieldIzip'][] = $result['i_zip'];
                    }

                    if((isset($result['i_phone'])&&$result['i_phone'] != ''))
                    {
                        $injuryArray['fieldIphone'][] = $result['i_phone'];
                    }
                   if((isset($result['adj10city'])&&$result['adj10city'] != ''))
                    {
                        $injuryArray['fieldAdj10city'][] = $result['adj10city'];
                    }
                   if((isset($result['adj1b'])&&$result['adj1b'] != ''))
                    {
                        $injuryArray['fieldAdj1b'][] = $result['adj1b'];
                   }
				   if((isset($result['adj1a'])&&$result['adj1a'] != ''))
                    {
                        $injuryArray['fieldAdj1a'][] = $result['adj1a'];
                   }

				   if((isset($result['adj1d'])&&$result['adj1d'] != ''))
                    {
                        $injuryArray['fieldAdj1d'][] = $result['adj1d'];
                   }
                    if((isset($result['adj2a'])&&$result['adj2a'] != ''))
                    {
                        $injuryArray['adj2a'][] = $result['adj2a'];
                        $injuryArray['adj2a_last'] = $result['adj2a'];
                    }

                    if((isset($result['adj3a'])&&$result['adj3a'] != ''))
                    {
                        $injuryArray['adj3a'][] = $result['adj3a'];
                    }

                   if((isset($result['firmatty1'])&&$result['firmatty1'] != ''))
                    {
                        $injuryArray['fieldFirmatty1'][] = $result['firmatty1'];
                    }
                   if((isset($result['i_phone'])&&$result['i_phone'] != ''))
                    {
                        $injuryArray['fieldIphone'][] = $result['i_phone'];
                    }
                    if((isset($result['d1_firm'])&&$result['d1_firm'] != ''))
                    {
                        $injuryArray['fieldd1firm'][] = $result['d1_firm'];
                    }
					if((isset($result['firmatty1'])&&$result['firmatty1'] != ''))
                    {
                        $injuryArray['fieldfirmatty1'][] = $result['firmatty1'];
                    }


                    if((isset($result['d1_first'])&& $result['d1_first'] != '') || (isset($result['d1_last'])&& $result['d1_last'] != '') )
                    {
						$fullnamed1 = $result['d1_first'].' '.$result['d1_last'];
                        $injuryArray['fieldd1firstlast'][] = isset($fullnamed1)?$fullnamed1:'';
                    }
                    if((isset($result['d1_address'])&& $result['d1_address'] != ''))
                    {
                       $injuryArray['fieldd1address'][] = isset($result['d1_address'])?$result['d1_address']:'';
                    }
                     if((isset($result['d1_city'])&& $result['d1_city'] != '')||(isset($result['d1_state'])&& $result['d1_state'] != '') ||(isset($result['d1_zip'])&& $result['d1_zip'] != ''))
                    {
						$fulladressd1 = $result['d1_city'].','.$result['d1_state'] .' '. $result['d1_zip'];
                       $injuryArray['fieldd1address2'][] = isset($fulladressd1)?$fulladressd1:'';
                    }
                    if((isset($result['d1_phone'])&& $result['d1_phone'] != ''))
                    {
                       $injuryArray['fieldd1phone'][] = isset($result['d1_phone'])?$result['d1_phone']:'';
                    }
                    //tav 5 no 5

                    if((isset($result['adj5b'])&& $result['adj5b'] != ''))
                    {
                       $injuryArray['fieldadj5b'][] = isset($result['adj5b'])?$result['adj5b']:'';
                    }
                    if((isset($result['adj5b'])&& $result['adj5b'] != ''))
                    {
                       $injuryArray['fieldadj5b'][] = isset($result['adj5b'])?$result['adj5b']:'';
                    }
                    if((isset($result['adj5c'])&& $result['adj5c'] != ''))
                    {
                       $injuryArray['fieldadj5c'][] = isset($result['adj5c'])?$result['adj5c']:'';
                    }
                    if((isset($result['adj5d'])&& $result['adj5d'] != ''))
                    {
                       $injuryArray['fieldadj5d'][] = isset($result['adj5d'])?$result['adj5d']:'';
                    }
                     if((isset($result['doctors'])&& $result['doctors'] != ''))
                    {
                       $injuryArray['fielddoctors'][] = isset($result['doctors'])?$result['doctors']:'';
                    }
                    if((isset($result['adj7d'])&& $result['adj7d'] != ''))
                    {
                       $injuryArray['fieldadj7d'][] = isset($result['adj7d'])?$result['adj7d']:'';
                    }
					if((isset($result['adj9g'])&& $result['adj9g'] != ''))
                    {
                       $injuryArray['fieldadj9g'][] = isset($result['adj9g'])?$result['adj9g']:'';
                    }
					if((isset($result['adj7b'])&& $result['adj7b'] != ''))
                    {
                       $injuryArray['fieldadj7b'][] = isset($result['adj7b'])?$result['adj7b']:'';
                    }
                    if((isset($result['adj7a'])&& $result['adj7a'] != ''))
                    {
                       $injuryArray['fieldadj7a'][] = isset($result['adj7a'])?$result['adj7a']:'';
                    }
                    if((isset($result['adj7c'])&& $result['adj7c'] != ''))
                    {
                       $injuryArray['fieldadj7c'][] = isset($result['adj7c'])?$result['adj7c']:'';
                    }
                    if((isset($result['adj5a'])&& $result['adj5a'] != ''))
                    {
                       $injuryArray['fieldadj5a'][] = isset($result['adj5a'])?$result['adj5a']:'';
                    }
                    if((isset($result['adj6a'])&& $result['adj6a'] != ''))
                    {
                       $injuryArray['fieldadj6a'][] = isset($result['adj6a'])?$result['adj6a']:'';
                    }
                    if((isset($result['adj7e'])&& $result['adj7e'] != ''))
                    {
                       $injuryArray['fieldadj7e'][] = isset($result['adj7e'])?$result['adj7e']:'';
                    }
                    if((isset($result['adj9a'])&& $result['adj9a'] != ''))
                    {
                       $injuryArray['fieldadj9a'][] = isset($result['adj9a'])?$result['adj9a']:'';
                    }
                    if((isset($result['adj9b'])&& $result['adj9b'] != ''))
                    {
                       $injuryArray['fieldadj9b'][] = isset($result['adj9b'])?$result['adj9b']:'';
                    }
                    if((isset($result['adj9c'])&& $result['adj9c'] != ''))
                    {
                       $injuryArray['fieldadj9c'][] = isset($result['adj9c'])?$result['adj9c']:'';
                    }
                    if((isset($result['adj9d'])&& $result['adj9d'] != ''))
                    {
                       $injuryArray['fieldadj9d'][] = isset($result['adj9d'])?$result['adj9d']:'';
                    }
                    if((isset($result['adj9e'])&& $result['adj9e'] != ''))
                    {
                       $injuryArray['fieldadj9e'][] = isset($result['adj9e'])?$result['adj9e']:'';
                    }
                    if((isset($result['adj9f'])&& $result['adj9f'] != ''))
                    {
                       $injuryArray['fieldadj9f'][] = isset($result['adj9f'])?$result['adj9f']:'';
                    }
                    $injuryArray['i_type'][] = isset($result['ct1'])?$result['ct1']:'0';
                    //echo 'ct => ' . $result['ct1'];
                    if((isset($result['ct1'])))
                    {
                       $injuryArray['single_i'][] = '';
                       $injuryArray['DOI_start'][] = $result['doi'];
                       $injuryArray['DOI_end'][] = $result['doi2'];
                       $injuryArray['inj_dt'][] = date('m/d/Y',strtotime($result['doi'])).'-'.date('m/d/Y',strtotime($result['doi2']));
                    }
                    else{
                        $injuryArray['single_i'][] = date('m/d/Y',strtotime($result['doi']));
                        $injuryArray['DOI_start'][] = '';
                       $injuryArray['DOI_end'][] = '';
                       $injuryArray['inj_dt'][] = '';
                    }
                    
                }
                else if($addInj == ''){ 
                    if((isset($result['case_no']) && $result['case_no'] != '') && (isset($result['eamsno']) && $result['eamsno'] != '')){
                        $WcabNoEamsNO = $result['case_no'] . ", " . 'ADJ'. $result['eamsno'];
                    } elseif((isset($result['case_no']) && $result['case_no'] != '') && (isset($result['eamsno']) && $result['eamsno'] == '')) {
                        $WcabNoEamsNO = $result['case_no'];
                    } elseif((isset($result['case_no']) && $result['case_no'] == '') && (isset($result['eamsno']) && $result['eamsno'] != '')) {
                        $WcabNoEamsNO = 'ADJ'.$result['eamsno'];
                    } else {
                        $WcabNoEamsNO = "";
                    }
                    if(isset($WcabNoEamsNO) && !in_array($WcabNoEamsNO, $injuryArray['WcabNoEamsNO'])) { $injuryArray['WcabNoEamsNO'][] = $WcabNoEamsNO; }

                    /*if(isset($result['case_no']) && $result['case_no'] != ''){
                        $wcabCaseNo = $result['case_no'];
                    }
                    if(isset($result['eamsno']) && $result['eamsno'] != ''){
                        $wcabCaseNo = 'ADJ'.$result['eamsno'];
                    }*/
                    
                    if(isset($result['eamsno']) && $result['eamsno'] != ''){
                        $wcabCaseNo = 'ADJ'.$result['eamsno'];
                    }else if(isset($result['case_no']) && $result['case_no'] != ''){
                        $wcabCaseNo = $result['case_no'];
                    }
                    else{
                        $wcabCaseNo = '';
                    }
		
                    if(isset($wcabCaseNo) && !in_array($wcabCaseNo, $injuryArray['wcabCaseNo'])) { $injuryArray['wcabCaseNo'][] = $wcabCaseNo; }
                    if(isset($result['i_claimno']) && $result['i_claimno'] != '' && !in_array($result['i_claimno'], $injuryArray['claimNo'])){
                        $injuryArray['claimNo'][] = $result['i_claimno'];
                    }
                    if(isset($result['i2_claimno']) && $result['i2_claimno'] != '' && !in_array($result['i2_claimno'], $injuryArray['claimNo'])){
                        $injuryArray['claimNo'][] = $result['i2_claimno'];
                    }
                    if(isset($result['adj1c']) && $result['adj1c'] != ''){
                         $injuryArray['DOI'][] = $result['adj1c'];
                    }
                }

                }
            }
			
	        $injuryArray['Ename_last'] = $result['e_name'];
            $injuryArray['Iname_last'] = $result['i_name'];
			$injuryArray['Iaddress_last'] = $result['i_address'];
			$injuryArray['Icity_last'] = $result['i_city'];
			$injuryArray['Istate_last'] = $result['i_state'];
			$injuryArray['Izip_last'] = $result['i_zip'];
			$injuryArray['Iphone_last'] = $result['i_phone'];
        }
        if(isset($injuryArray['DOI'])){
                $injuryArray['DOI_first'] = $injuryArray['DOI'][0];
            }
        if(isset($injuryArray['fieldEname'])){
                $injuryArray['Ename_first'] = $injuryArray['fieldEname'][0];
            }
       //echo '<pre>'; print_r($injuryArray); exit;
       // exit;
        return $injuryArray;
    }
    


    public function setData($name, $var) {
        return $this->{$var} = $name;
    }

    public function create_doc_file($data) {
	
        if (version_compare(PHP_VERSION, '5.1.0') >= 0) {
            if (ini_get('date.timezone') == '') {
                date_default_timezone_set('UTC');
            }
        }

        if($data['enum'] != ''){
            //  $GLOBALS['cc'] = ($data['enum'] == 'y') ? 'cc:    '.$data['cc_party_name'] : '';
            if($data['enum'] == 'y')
            {
                $GLOBALS['cc'] = $data['cc_party_name'];
                $GLOBALS['tmc'] = 'cc :';
            }else{
                $GLOBALS['cc'] = '';
                $GLOBALS['tmc'] = '';
            }

        }else{
            $GLOBALS['depo_date'] = $data['depo_date'];
            $GLOBALS['depo_time'] = $data['depo_time'];
            $GLOBALS['fee_amt'] = $data['fee_amt'];
            $GLOBALS['cc'] = '';
            $GLOBALS['tmc'] = '';
        }
        // cc party for forms without yes/no popup
         if(!empty($data['cc_party_name']))
            {
                $GLOBALS['cc'] = $data['cc_party_name'];
                $GLOBALS['tmc'] = 'cc :';
            }else{
                $GLOBALS['cc'] = '';
                $GLOBALS['tmc'] = '';
            }
		$GLOBALS['clientaddress1sperate'] = strtoupper($data['clientaddress1sperate']);
		$GLOBALS['clientaddress2sperate'] = strtoupper($data['clientaddress2sperate']);
		$GLOBALS['sfieldAdj1a'] = $data['sfieldAdj1a'];
		$GLOBALS['usfieldAdj1a'] = strtoupper($data['sfieldAdj1a']);
		//echo '<pre>';print_r($GLOBALS);exit;
		$GLOBALS['pob1']= $data['pob1'];
		$GLOBALS['pob2']= $data['pob2'];
		$GLOBALS['pob3']= $data['pob3'];
		$GLOBALS['pob4']= $data['pob4'];
		$GLOBALS['pob5']= $data['pob5'];
                $GLOBALS['singlepob1']= $data['singlepob1'];
		$GLOBALS['singlepob2']= $data['singlepob2'];
		$GLOBALS['singlepob3']= $data['singlepob3'];
		$GLOBALS['singlepob4']= $data['singlepob4'];
		$GLOBALS['singlepob5']= $data['singlepob5'];
         $GLOBALS['depo_date'] = $data['depo_date'];
        if($data['depo_date'] != '' ){
            $date = $data['depo_date'];
            $weekday = date('l', strtotime($date));
            $GLOBALS['depo_day'] = $weekday;
            $GLOBALS['depo_date_f'] = date("F d, Y",strtotime($GLOBALS['depo_date']));
        }
        else
        {
            $GLOBALS['depo_day'] = '';
            $GLOBALS['depo_date_f'] = '';
        }


        //$GLOBALS['depo_time'] = $data['depo_time'];
        $depoTime = explode(" ",$data['depo_time']);
        if(strlen($depoTime[1]) == 2){
            $depoTime[1] = strtolower($depoTime[1]);
            $price_text = (string)$depoTime[1]; // convert into a string
            $arr = str_split($price_text, "1"); // break string in 3 character sets

            $price_new_text = implode(".", $arr);
            $GLOBALS['depo_time'] = $depoTime[0].' '.$price_new_text.'.';
        }else{
            $GLOBALS['depo_time'] = $data['depo_time'];
        }
        //echo  $GLOBALS['depo_time']; exit;
        $GLOBALS['fee_amt'] = $data['fee_amt'];

        $GLOBALS['pl_title'] = $data['pl_title'];
        $GLOBALS['pl_title_trim'] = trim($data['pl_title']);
        $GLOBALS['my_pl_title'] = $data['my_pl_title'];
        $GLOBALS['date_App'] = $data['date_App'];
        $GLOBALS['date_App_f'] = date("F d, Y",strtotime($GLOBALS['date_App']));
        $GLOBALS['date_Tri'] = $data['date_Tri'];
        $GLOBALS['date_Tri_f'] = date("F d, Y",strtotime($GLOBALS['date_Tri']));
        $GLOBALS['time_Tri'] = $data['time_Tri'];
        $GLOBALS['boardCity'] = $data['boardCity'];
        if($data['boardCity'] == ''){
            $GLOBALS['boardCity'] = $data['board_City'];
        }
        $GLOBALS['board_City'] = $data['board_City'];
        $GLOBALS['dr_splt'] = $data['dr_splt'];
        $GLOBALS['e_type'] = $data['e_type'];
        $GLOBALS['e_day'] = $data['e_day'];
        $GLOBALS['e_date'] = $data['e_date'];
        $GLOBALS['e_date_f'] =  date("F d, Y",strtotime($GLOBALS['e_date']));
        $GLOBALS['e_time'] = $data['e_time'];
        $GLOBALS['attyres'] = $data['attyres'];
        $GLOBALS['pro_unit'] = $data['pro_unit'];
        $GLOBALS['pro_doc_typ'] = $data['pro_doc_typ'];
        $GLOBALS['doc_title'] = $data['doc_title'];
        $GLOBALS['judgeNM'] = $data['judgeNM'];
        $GLOBALS['defendantName'] = ucfirst($data['defendantName']);
        $GLOBALS['defendantNameC'] = strtoupper($data['defendantName']);
        $GLOBALS['defendantNameC'] = strtoupper($data['defendantName']);
        $GLOBALS['insName'] = $data['insName'];
		$GLOBALS['insurance_name'] = $data['insurance_name'];
		$GLOBALS['insuranceAddress1'] = $data['insuranceAddress1'];
		$GLOBALS['insuranceAddress2'] = $data['insuranceAddress2'];
		$GLOBALS['insuranceCity'] = $data['insuranceCity'];
		$GLOBALS['insuranceState'] = $data['insuranceState'];
		$GLOBALS['insuranceZip'] = $data['insuranceZip'];
		$GLOBALS['insurancephone1'] = $data['insurancephone1'];
		
        $GLOBALS['lawyersLeft'] = $data['lawyersLeft'];
        $GLOBALS['lawyersRight'] = $data['lawyersRight'];
        $GLOBALS['insNameC'] = strtoupper($data['insName']);

        $GLOBALS['todayDate'] = $data['today'];
        $GLOBALS['today'] = date('m/d/Y');
        $GLOBALS['t'] = date('m/d/Y');
		//$clinetnames = explode('.',$data['clienntName']);
        $GLOBALS['caseno'] = $data['caseno'];
        $GLOBALS['clientname'] = isset($data['clientName'])?trim(ucfirst($data['clientName'])):'';
        $GLOBALS['total_mileage'] = $data['total_mileage'];
        $GLOBALS['total_mileage_amount'] = $data['total_mileage_amount'];
        $GLOBALS['travel_time'] = $data['travel_time'];
        $GLOBALS['depo_prep_time'] = $data['depo_prep_time'];
        $GLOBALS['total_time'] = $data['total_time'];
        $GLOBALS['total_request'] = $data['total_request'];
        $GLOBALS['dr_speciality'] = $data['dr_speciality'];
        $GLOBALS['body_partexamined'] = $data['body_partexamined'];
		
		$GLOBALS['clientnames'] = isset($data['clientName'])?trim($data['clientName']):'';
		
        $GLOBALS['clientWFirmName'] = isset($data['clientWFirmName'])?trim($data['clientWFirmName']):'';
        $GLOBALS['clientFNM'] = isset($data['clientFNM'])?trim($data['clientFNM']):'';
        $GLOBALS['clientLNM'] = isset($data['clientLNM'])?trim($data['clientLNM']):'';
        $GLOBALS['clientFirm'] = isset($data['clientFirm'])?trim($data['clientFirm']):'';
        $GLOBALS['CLIENTNAME'] = isset($data['clientName'])?strtoupper($data['clientName']):'';
        $GLOBALS['firstname'] = isset($data['firstname'])?$data['firstname']:'';
        $GLOBALS['lastname'] = isset($data['lastname'])?$data['lastname']:'';
		
		$GLOBALS['ufirstname'] = isset($data['firstname'])?strtoupper($data['firstname']):'';
        $GLOBALS['ulastname'] = isset($data['lastname'])?strtoupper($data['lastname']):'';

        $GLOBALS['client'] = isset($data['clientName'])?$data['clientName']:'';
        $GLOBALS['clientAddress1'] = isset($data['clientAddress1'])?$data['clientAddress1']:'';
        $GLOBALS['clientAddress2'] = isset($data['clientAddress2'])?$data['clientAddress2']:'';
        $GLOBALS['clientIAddress1'] = isset($data['clientIAddress1'])?$data['clientIAddress1']:'';
        $GLOBALS['clientIAddress2'] = isset($data['clientIAddress2'])?$data['clientIAddress2']:'';
        $GLOBALS['clientIName'] = isset($data['clientIName'])?$data['clientIName']:'';
        $GLOBALS['clientfull'] = isset($data['clientfull'])?$data['clientfull']:'';
        $GLOBALS['clientCity'] = isset($data['clientCity'])?$data['clientCity']:'';
        $GLOBALS['clientState'] = isset($data['clientState'])?$data['clientState']:'';
        $GLOBALS['clientZip'] = isset($data['clientZip'])?$data['clientZip']:'';
        $GLOBALS['clientboard'] = isset($data['clientboardName'])?$data['clientboardName']:'';
        $GLOBALS['clientboardOnly'] = isset($data['clientboardOnly'])?$data['clientboardOnly']:'';
        $GLOBALS['clientboardAddress1'] = isset($data['clientboardAddress1'])?$data['clientboardAddress1']:'';
        $GLOBALS['clientboardAddress2'] = isset($data['clientboardAddress2'])?$data['clientboardAddress2']:'';
        //echo '<pre>'; print_r($GLOBALS); exit;
        $GLOBALS['socialsec'] = isset($data['social_sec'])?$data['social_sec']:'';
        $GLOBALS['dateenter'] = isset($data['dateenter'])?$data['dateenter']:'';
        $GLOBALS['home'] = isset($data['home'])?$data['home']:'';
        $GLOBALS['business'] = isset($data['business'])?$data['business']:'';
        $GLOBALS['car'] = isset($data['car'])?$data['car']:'';
        $GLOBALS['fax'] = isset($data['fax'])?$data['fax']:'';
        $GLOBALS['birthdate'] = isset($data['birthdate'])?$data['birthdate']:'';
        $GLOBALS['bdate'] = isset($data['birthdate'])?$data['birthdate']:'';
        $GLOBALS['firmname'] = isset($data['firmname'])?$data['firmname']:'';
        $GLOBALS['venue'] = isset($data['venue'])?$data['venue']:'';
        $GLOBALS['client_first'] = isset($data['client_first'])?$data['client_first']:'';
        $GLOBALS['client_suffix'] = isset($data['client_suffix'])?$data['client_suffix']:'';
        $GLOBALS['my_middle'] = isset($data['my_middle'])?$data['my_middle']:'';
        $GLOBALS['my_email'] = isset($data['my_email'])?$data['my_email']:'';
        $GLOBALS['my_age'] = isset($data['my_age'])?$data['my_age']:'';
        $GLOBALS['my_social'] = isset($data['my_social'])?$data['my_social']:'';
        $GLOBALS['my_MI'] = isset($data['my_MI'])?$data['my_MI']:'';
        $GLOBALS['my_occupation'] = isset($data['my_occupation'])?$data['my_occupation']:'';
        $GLOBALS['my_title'] = isset($data['my_title'])?$data['my_title']:'';
        $GLOBALS['licenceNo'] = isset($data['licenseno'])?$data['licenseno']:'';

        $GLOBALS['client_fname'] = isset($data['client_fname'])?$data['client_fname']:'';
		$GLOBALS['client_fnameupper'] = isset($data['client_fname'])? strtoupper($data['client_fname']):'';
		
        $GLOBALS['client_sal'] = isset($data['client_sal'])?$data['client_sal']:'';
        $GLOBALS['client_lname'] = isset($data['client_lname'])?$data['client_lname']:'';
		$GLOBALS['client_lnameupper'] = isset($data['client_lname'])? strtoupper($data['client_lname']):'';
        $GLOBALS['client_full'] = isset($data['client_full'])?$data['client_full']:'';
		$GLOBALS['usclient_full'] = isset($data['client_full'])? strtoupper($data['client_full']):'';
		$GLOBALS['client_fullname'] = isset($data['client_full'])? $data['client_full']:'';
		$GLOBALS['clientfullnames'] = isset($data['clientfullnames'])? ucfirst($data['clientfullnames']):'';
        $GLOBALS['clientlawfullnames'] = isset($data['clientlawfullnames'])? $data['clientlawfullnames']:'';
		$GLOBALS['uclientlawfullnames'] = isset($data['clientlawfullnames'])? strtoupper($data['clientlawfullnames']):'';		
	    $GLOBALS['address1'] = isset($data['address1'])?$data['address1']:'';
		$GLOBALS['uaddress1'] = isset($data['address1'])?strtoupper($data['address1']):'';

		$GLOBALS['eaddress'] = isset($data['fieldEaddress'])?$data['fieldEaddress']:'';

		$GLOBALS['address2'] = isset($data['address2'])?$data['address2']:'';
		$GLOBALS['uaddress2'] = isset($data['address2'])?strtoupper($data['address2']):'';
        $GLOBALS['city'] = isset($data['city'])? trim($data['city']):'';
		$GLOBALS['ucity'] = isset($data['city'])? strtoupper($data['city']):'';
        $GLOBALS['zip'] = isset($data['zip'])? trim($data['zip']):'';
        $GLOBALS['state'] = isset($data['state'])? trim($data['state']):'';
		$GLOBALS['ustate'] = isset($data['state'])? strtoupper($data['state']):'';
        $GLOBALS['st'] = isset($data['state'])?$data['state']:'';
        $GLOBALS['phone1'] = isset($data['phone1'])?$data['phone1']:'';
        $GLOBALS['fileno'] = isset($data['fileno'])? $data['fileno'] : '';
        $GLOBALS['caption'] = isset($data['caption'])?$data['caption']:'';
        $GLOBALS['CAPTION'] = isset($data['caption'])?strtoupper($data['caption']):'';
        $GLOBALS['staff'] = isset($data['staff'])?strtolower($data['staff']):'';

        $GLOBALS['attnm'] = isset($data['attnm'])?$data['attnm']:'';
        $GLOBALS['att_nm'] = isset($data['att_nm'])?$data['att_nm']:'';
        $GLOBALS['att_nm_upper'] = isset($data['att_nm'])? strtoupper($data['att_nm']):'';
        $GLOBALS['att_suff_upper'] = isset($data['att_suff'])? strtoupper($data['att_suff']):'';
        $GLOBALS['att_lnm'] = isset($data['att_lnm'])?$data['att_lnm']:'';
        $GLOBALS['att_sal'] = isset($data['att_sal'])?$data['att_sal']:'';
        $GLOBALS['att_phone'] = isset($data['att_phone'])?$data['att_phone']:'';
        $GLOBALS['att_salnm'] = isset($data['att_sal']) && !empty($data['att_sal'])?$data['att_sal']." ".$data['att_lnm'] :$GLOBALS['att_lnm'];;     $GLOBALS['att_firm'] = isset($data['att_firm'])?$data['att_firm']:'';
        $GLOBALS['attAddress1'] = isset($data['attAddress1'])?$data['attAddress1']:'';
        $GLOBALS['attAddress2'] = isset($data['attAddress2'])?$data['attAddress2']:'';
        $GLOBALS['attorney_fees'] = isset($data['attorney_fees'])?$data['attorney_fees']:'';

        $GLOBALS['daily_wage'] = isset($data['daily_wage'])?$data['daily_wage']:'';
        $GLOBALS['depo_time_hours'] = isset($data['depo_time_hours'])?$data['depo_time_hours']:'';
        $GLOBALS['depo_time_minutes'] = isset($data['depo_time_minutes'])?$data['depo_time_minutes']:'';
        $GLOBALS['attorney_fees'] = isset($data['attorney_fees'])?$data['attorney_fees']:'';
        $GLOBALS['trip_mileage'] = isset($data['trip_mileage'])?$data['trip_mileage']:'';

		
        $attorney_details = '';
        if(!empty($GLOBALS['att_firm'])){
            $attorney_details .= "{$GLOBALS['att_firm']}\n";
        }
        else{
            if(!empty($GLOBALS['att_nm']))
                $attorney_details .= "{$GLOBALS['att_nm']}\n";
        }
        if(!empty($GLOBALS['attAddress1'])){
            $attorney_details .= "{$GLOBALS['attAddress1']}\n";
        }
        if(!empty($GLOBALS['attAddress2'])){
            $attorney_details .= "{$GLOBALS['attAddress2']}\n";
        }
        if(!empty(trim($data['att_firm'])) && !empty(trim($data['att_nm']))){
            if(!empty(trim($data['att_suff']))){
                $GLOBALS['att_nm'] = $GLOBALS['att_nm'].', '.$data['att_suff'];
                $attorney_details .= "Attn: {$GLOBALS['att_nm']}\n";
            }
            else{
               $attorney_details .= "Attn: {$GLOBALS['att_nm']}\n";
            }
        }
	if(!empty($GLOBALS['att_firm'])){
            $GLOBALS['att_firmqme'] = $GLOBALS['att_firm'];
        }
        else{
            $GLOBALS['att_firmqme'] = $GLOBALS['att_nm'];
        }
	$GLOBALS['attorney_details'] = trim($attorney_details);
        $GLOBALS['attCity'] = isset($data['attCity'])?$data['attCity']:'';
        $GLOBALS['attState'] = isset($data['attState'])?$data['attState']:'';
        $GLOBALS['attZip'] = isset($data['attZip'])?$data['attZip']:'';

        $GLOBALS['defAddress1'] = isset($data['defAddress1'])?$data['defAddress1']:'';
        $GLOBALS['defAddress2'] = isset($data['defAddress2'])?$data['defAddress2']:'';
        $GLOBALS['defCity'] = isset($data['defCity'])?$data['defCity']:'';
        $GLOBALS['defState'] = isset($data['defState'])?$data['defState']:'';
        $GLOBALS['defZip'] = isset($data['defZip'])?$data['defZip']:'';

        $GLOBALS['ins_name'] = isset($data['ins_name'])?$data['ins_name']:'';
		$GLOBALS['ins_nameupper'] = isset($data['ins_name'])? strtoupper($data['ins_name']):'';
        $GLOBALS['insAddress1'] = isset($data['insAddress1'])?$data['insAddress1']:'';
		$GLOBALS['insCity'] = isset($data['insCity'])?$data['insCity']:'';
        $GLOBALS['insState'] = isset($data['insState'])?$data['insState']:'';
		
		
		
        $GLOBALS['insZip'] = isset($data['insZip'])?$data['insZip']:'';
        $GLOBALS['parties'] = isset($data['parties'])?$data['parties']:'';

        //Injury Details
        $GLOBALS['wcabcaseno'] = isset($data['wcabCaseNo'])?$data['wcabCaseNo']:'';
        $GLOBALS['WcabNoEamsNO'] = isset($data['WcabNoEamsNO'])?$data['WcabNoEamsNO']:'';
        $GLOBALS['DOI_first'] = isset($data['DOI_first'])?$data['DOI_first']:'';
        $GLOBALS['Ename_last'] = isset($data['Ename_last'])?$data['Ename_last']:'';
        $GLOBALS['Ename_first'] = isset($data['Ename_first'])?$data['Ename_first']:'';

        $GLOBALS['Iname_last'] = isset($data['Iname_last'])?$data['Iname_last']:'';
		$GLOBALS['Iaddress_last'] = isset($data['fieldIaddress'])?$data['fieldIaddress']:'';
		$GLOBALS['Icity_last'] = isset($data['fieldIcity'])?$data['fieldIcity']:'';
		$GLOBALS['Istate_last'] = isset($data['fieldIstate'])?$data['fieldIstate']:'';
		$GLOBALS['Iphone_last'] = isset($data['fieldIphone'])?$data['fieldIphone']:'';
		$GLOBALS['Izip_last'] = isset($data['fieldIzip'])?$data['fieldIzip']:'';

        $GLOBALS['claimno'] = isset($data['claimNo'])?$data['claimNo']:'';
        $GLOBALS['r_i_claimno'] = isset($data['r_i_claimno'])?$data['r_i_claimno']:'';
        $GLOBALS['dateofinjury'] = isset($data['DOI_all'])?$data['DOI_all']:'';
        $GLOBALS['injuryd'] = isset($data['DOI'])?$data['DOI']:'';
        $GLOBALS['adj1e'] =isset($data['adj1e'])?$data['adj1e']:'';
        $GLOBALS['adj2a'] =isset($data['adj2a'])?$data['adj2a']:'';
		$GLOBALS['adj2aupper'] =isset($data['adj2a'])? strtoupper($data['adj2a']):'';
        $GLOBALS['adj2a_last'] =isset($data['adj2a_last'])?$data['adj2a_last']:'';
        $GLOBALS['adj1e_last'] =isset($data['adj1e_last'])?$data['adj1e_last']:'';
        $GLOBALS['adj3a'] =isset($data['adj3a'])?$data['adj3a']:'';

        $GLOBALS['fieldename'] = isset($data['fieldEname'])?$data['fieldEname']:'';
		$GLOBALS['ufieldename'] = isset($data['fieldEname'])?strtoupper($data['fieldEname']):'';
        $GLOBALS['fieldeaddress'] = isset($data['fieldEaddress'])?$data['fieldEaddress']:'';
		
        $GLOBALS['injurycity']=isset($data['injurycity'])?$data['injurycity']:'';
        $GLOBALS['adj1a_birthday']=isset($data['adj1a_birthday'])?$data['adj1a_birthday']:'';//injured emp birthday
        $GLOBALS['fieldEaddress']=isset($data['fieldEaddress'])?$data['fieldEaddress']:'';
        $GLOBALS['fieldEaddress2']=isset($data['fieldEaddress2'])?$data['fieldEaddress2']:'';
        $GLOBALS['Ecity']=isset($data['fieldEcity'])?$data['fieldEcity']:'';
        $GLOBALS['Estate']=isset($data['fieldEstate'])?$data['fieldEstate']:'';
		$GLOBALS['Ecityupper']=isset($data['fieldEcity'])? strtoupper($data['fieldEcity']):'';
        $GLOBALS['Estateupper']=isset($data['fieldEstate'])? strtoupper($data['fieldEstate']):'';
        $GLOBALS['Ezip']=isset($data['fieldEzip'])?$data['fieldEzip']:'';
        $GLOBALS['Efax']=isset($data['fieldEfax'])?$data['fieldEfax']:'';
        $GLOBALS['Ephone']=isset($data['fieldEphone'])?$data['fieldEphone']:'';
        $GLOBALS['Ep']=isset($data['fieldEphone'])?$data['fieldEphone']:'';
        $GLOBALS['fieldeaddressupper'] = isset($data['fieldEaddress'])? strtoupper($data['fieldEaddress']):'';
        //  echo '<pre>'; print_r($GLOBALS); exit;

        //carriers

        $GLOBALS['Iadjuster']=isset($data['fieldIadjuster'])?$data['fieldIadjuster']:'';
        $GLOBALS['fieldIname']=isset($data['fieldIname'])?$data['fieldIname']:'';
        $GLOBALS['fieldIaddress']=isset($data['fieldIaddress'])?$data['fieldIaddress']:'';
        $GLOBALS['ufieldIaddress']=isset($data['fieldIaddress'])? strtoupper($data['fieldIaddress']):'';
		$GLOBALS['fieldIphone']=isset($data['fieldIphone'])?$data['fieldIphone']:'';
        $GLOBALS['Icity']=isset($data['fieldIcity'])?$data['fieldIcity']:'';
        $GLOBALS['Istate']=isset($data['fieldIstate'])?$data['fieldIstate']:'';
        $GLOBALS['Izip']=isset($data['fieldIzip'])?$data['fieldIzip']:'';
        $GLOBALS['fieldIaddress2']=isset($data['fieldIcity'])?$data['fieldIcity']:''." ".isset($data['fieldIstate'])?$data['fieldIstate']:''.''.isset($data['fieldIzip'])?$data['fieldIzip']:'';

        //injury more then 5

        $GLOBALS['DOI1']=isset($data['DOI1'])?$data['DOI1']:'';
        $GLOBALS['DOI2']=isset($data['DOI2'])?$data['DOI2']:'';
        $GLOBALS['DOI3']=isset($data['DOI3'])?$data['DOI3']:'';
        $GLOBALS['DOI4']=isset($data['DOI4'])?$data['DOI4']:'';
        $GLOBALS['DOI5']=isset($data['DOI5'])?$data['DOI5']:'';

        $GLOBALS['DOI_start1']=isset($data['DOI_start1'])?$data['DOI_start1']:'';
        $GLOBALS['DOI_start2']=isset($data['DOI_start2'])?$data['DOI_start2']:'';
        $GLOBALS['DOI_start3']=isset($data['DOI_start3'])?$data['DOI_start3']:'';
        $GLOBALS['DOI_start4']=isset($data['DOI_start4'])?$data['DOI_start4']:'';
        $GLOBALS['DOI_start5']=isset($data['DOI_start5'])?$data['DOI_start5']:'';
        $GLOBALS['DOI_start6']=isset($data['DOI_start6'])?$data['DOI_start6']:'';
        $GLOBALS['DOI_start7']=isset($data['DOI_start7'])?$data['DOI_start7']:'';
        $GLOBALS['DOI_start8']=isset($data['DOI_start8'])?$data['DOI_start8']:'';
        $GLOBALS['DOI_start9']=isset($data['DOI_start9'])?$data['DOI_start9']:'';
        $GLOBALS['DOI_start10']=isset($data['DOI_start10'])?$data['DOI_start10']:'';
        $GLOBALS['DOI_start11']=isset($data['DOI_start11'])?$data['DOI_start11']:'';
        $GLOBALS['DOI_start12']=isset($data['DOI_start12'])?$data['DOI_start12']:'';
        $GLOBALS['DOI_start13']=isset($data['DOI_start13'])?$data['DOI_start13']:'';
        $GLOBALS['DOI_start14']=isset($data['DOI_start14'])?$data['DOI_start14']:'';
        $GLOBALS['DOI_start15']=isset($data['DOI_start15'])?$data['DOI_start15']:'';
        $GLOBALS['DOI_start16']=isset($data['DOI_start16'])?$data['DOI_start16']:'';

        $GLOBALS['DOI_end1']=isset($data['DOI_end1'])?$data['DOI_end1']:'';
        $GLOBALS['DOI_end2']=isset($data['DOI_end2'])?$data['DOI_end2']:'';
        $GLOBALS['DOI_end3']=isset($data['DOI_end3'])?$data['DOI_end3']:'';
        $GLOBALS['DOI_end4']=isset($data['DOI_end4'])?$data['DOI_end4']:'';
        $GLOBALS['DOI_end5']=isset($data['DOI_end5'])?$data['DOI_end5']:'';
        $GLOBALS['DOI_end6']=isset($data['DOI_end6'])?$data['DOI_end6']:'';
        $GLOBALS['DOI_end7']=isset($data['DOI_end7'])?$data['DOI_end7']:'';
        $GLOBALS['DOI_end8']=isset($data['DOI_end8'])?$data['DOI_end8']:'';
        $GLOBALS['DOI_end9']=isset($data['DOI_end9'])?$data['DOI_end9']:'';
        $GLOBALS['DOI_end10']=isset($data['DOI_end10'])?$data['DOI_end10']:'';
        $GLOBALS['DOI_end11']=isset($data['DOI_end11'])?$data['DOI_end11']:'';
        $GLOBALS['DOI_end12']=isset($data['DOI_end12'])?$data['DOI_end12']:'';
        $GLOBALS['DOI_end13']=isset($data['DOI_end13'])?$data['DOI_end13']:'';
        $GLOBALS['DOI_end14']=isset($data['DOI_end14'])?$data['DOI_end14']:'';
        $GLOBALS['DOI_end15']=isset($data['DOI_end15'])?$data['DOI_end15']:'';
        $GLOBALS['DOI_end16']=isset($data['DOI_end16'])?$data['DOI_end16']:'';
      //wcabCaseno

        $GLOBALS['wc1']=isset($data['wcab1'])?$data['wcab1']:'';
        $GLOBALS['wc2']=isset($data['wcab2'])?$data['wcab2']:'';
        $GLOBALS['wc3']=isset($data['wcab3'])?$data['wcab3']:'';
        $GLOBALS['wc4']=isset($data['wcab4'])?$data['wcab4']:'';
        $GLOBALS['wc5']=isset($data['wcab5'])?$data['wcab5']:'';
        $GLOBALS['wc6']=isset($data['wcab6'])?$data['wcab6']:'';
        $GLOBALS['wc7']=isset($data['wcab7'])?$data['wcab7']:'';
        $GLOBALS['wc8']=isset($data['wcab8'])?$data['wcab8']:'';
        $GLOBALS['wc9']=isset($data['wcab9'])?$data['wcab9']:'';
        $GLOBALS['wc10']=isset($data['wcab10'])?$data['wcab10']:'';
        $GLOBALS['wc11']=isset($data['wcab11'])?$data['wcab11']:'';
        $GLOBALS['wc12']=isset($data['wcab12'])?$data['wcab12']:'';
        $GLOBALS['wc13']=isset($data['wcab13'])?$data['wcab13']:'';
        $GLOBALS['wc14']=isset($data['wcab14'])?$data['wcab14']:'';
        $GLOBALS['wc15']=isset($data['wcab15'])?$data['wcab15']:'';
        $GLOBALS['wc16']=isset($data['wcab16'])?$data['wcab16']:'';
       //city
        $GLOBALS['Adj10city']=isset($data['fieldAdj10city'])?$data['fieldAdj10city']:'';
       //injared emp work deatials
       $GLOBALS['fieldAdj1b']=isset($data['fieldAdj1b'])?$data['fieldAdj1b']:'';
	   $GLOBALS['fieldAdj1a']=isset($data['fieldAdj1a'])?$data['fieldAdj1a']:'';
	   $GLOBALS['sfieldAdj1a']=isset($data['fieldAdj1a'])?$data['fieldAdj1a']:'';
	   $GLOBALS['usfieldAdj1a']=isset($data['fieldAdj1a'])?strtoupper($data['fieldAdj1a']):'';
	   $GLOBALS['fieldAdj1d']=isset($data['fieldAdj1d'])?$data['fieldAdj1d']:'';
	   $GLOBALS['fieldAdj1dupper']=isset($data['fieldAdj1d'])? strtoupper($data['fieldAdj1d']):'';
       //Applicant Attorney
       $GLOBALS['fieldFirmatty1']=isset($data['fieldFirmatty1'])?$data['fieldFirmatty1']:'';
        //login user details
        $GLOBALS['logindetails']=isset($data['logindetails'])? $data['logindetails']:'';
        //echo '<pre>'; print_r($GLOBALS); exit;
        $GLOBALS['defendant']=isset($data['client_defendant'])?$data['client_defendant']:'';
        //Attorny tab 5
        $GLOBALS['d1firm']=isset($data['fieldd1firm'])?$data['fieldd1firm']:'';
        $GLOBALS['d1firstl']= isset($data['fieldd1firstlast'])?$data['fieldd1firstlast']:'';//first last name
        $GLOBALS['d1address']= isset($data['fieldd1address'])?$data['fieldd1address']:'';//attorny address
        $GLOBALS['d1address2']= isset($data['fieldd1address2'])?$data['fieldd1address2']:"";//Attorny city state zip
        $GLOBALS['d1p']= isset($data['fieldd1phone'])?$data['fieldd1phone']:"";//Attorny phone


      //location details
        $GLOBALS['lo0_name']= isset($data['loc_name0'])?$data['loc_name0']:'';
        $GLOBALS['lo1_name']= isset($data['loc_name1'])?$data['loc_name1']:'';
        $GLOBALS['lo2_name']= isset($data['loc_name2'])?$data['loc_name2']:'';
        $GLOBALS['lo3_name']= isset($data['loc_name3'])?$data['loc_name3']:'';
        $GLOBALS['lo4_name']= isset($data['loc_name4'])?$data['loc_name4']:'';
        $GLOBALS['lo5_name']= isset($data['loc_name5'])?$data['loc_name5']:'';
        $GLOBALS['lo6_name']= isset($data['loc_name6'])?$data['loc_name6']:'';
        $GLOBALS['lo7_name']= isset($data['loc_name7'])?$data['loc_name7']:'';
        $GLOBALS['lo8_name']= isset($data['loc_name8'])?$data['loc_name8']:'';
        $GLOBALS['lo9_name']= isset($data['loc_name9'])?$data['loc_name9']:'';
        $GLOBALS['lo10_name']= isset($data['loc_name10'])?$data['loc_name10']:'';
        $GLOBALS['lo11_name']= isset($data['loc_name11'])?$data['loc_name11']:'';
        $GLOBALS['lo12_name']= isset($data['loc_name12'])?$data['loc_name12']:'';
        $GLOBALS['lo13_name']= isset($data['loc_name13'])?$data['loc_name13']:'';
        $GLOBALS['lo14_name']= isset($data['loc_name14'])?$data['loc_name14']:'';
        $GLOBALS['lo15_name']= isset($data['loc_name15'])?$data['loc_name15']:'';


        $GLOBALS['lo0_firmname']= isset($data['loc_firmname0'])?$data['loc_firmname0']:'';
        $GLOBALS['lo1_firmname']= isset($data['loc_firmname1'])?$data['loc_firmname1']:'';
        $GLOBALS['lo2_firmname']= isset($data['loc_firmname2'])?$data['loc_firmname2']:'';
        $GLOBALS['lo3_firmname']= isset($data['loc_firmname3'])?$data['loc_firmname3']:'';
        $GLOBALS['lo4_firmname']= isset($data['loc_firmname4'])?$data['loc_firmname4']:'';
        $GLOBALS['lo5_firmname']= isset($data['loc_firmname5'])?$data['loc_firmname5']:'';
        $GLOBALS['lo6_firmname']= isset($data['loc_firmname6'])?$data['loc_firmname6']:'';
        $GLOBALS['lo7_firmname']= isset($data['loc_firmname7'])?$data['loc_firmname7']:'';
        $GLOBALS['lo8_firmname']= isset($data['loc_firmname8'])?$data['loc_firmname8']:'';
        $GLOBALS['lo9_firmname']= isset($data['loc_firmname9'])?$data['loc_firmname9']:'';
        $GLOBALS['lo10_firmname']= isset($data['loc_firmname10'])?$data['loc_firmname10']:'';
        $GLOBALS['lo11_firmname']= isset($data['loc_firmname11'])?$data['loc_firmname11']:'';
        $GLOBALS['lo12_firmname']= isset($data['loc_firmname12'])?$data['loc_firmname12']:'';
        $GLOBALS['lo13_firmname']= isset($data['loc_firmname13'])?$data['loc_firmname13']:'';
        $GLOBALS['lo14_firmname']= isset($data['loc_firmname14'])?$data['loc_firmname14']:'';
        $GLOBALS['lo15_firmname']= isset($data['loc_firmname15'])?$data['loc_firmname15']:'';

        $GLOBALS['lo0_address']= isset($data['loc_address0'])?$data['loc_address0']:'';
        $GLOBALS['lo1_address']= isset($data['loc_address1'])?$data['loc_address1']:'';
        $GLOBALS['lo2_address']= isset($data['loc_address2'])?$data['loc_address2']:'';
        $GLOBALS['lo3_address']= isset($data['loc_address3'])?$data['loc_address3']:'';
        $GLOBALS['lo4_address']= isset($data['loc_address4'])?$data['loc_address4']:'';
        $GLOBALS['lo5_address']= isset($data['loc_address5'])?$data['loc_address5']:'';
        $GLOBALS['lo6_address']= isset($data['loc_address6'])?$data['loc_address6']:'';
        $GLOBALS['lo7_address']= isset($data['loc_address7'])?$data['loc_address7']:'';
        $GLOBALS['lo8_address']= isset($data['loc_address8'])?$data['loc_address8']:'';
        $GLOBALS['lo9_address']= isset($data['loc_address9'])?$data['loc_address9']:'';
        $GLOBALS['lo10_address']= isset($data['loc_address10'])?$data['loc_address10']:'';
        $GLOBALS['lo11_address']= isset($data['loc_address11'])?$data['loc_address11']:'';
        $GLOBALS['lo12_address']= isset($data['loc_address12'])?$data['loc_address12']:'';
        $GLOBALS['lo13_address']= isset($data['loc_address13'])?$data['loc_address13']:'';
        $GLOBALS['lo14_address']= isset($data['loc_address14'])?$data['loc_address14']:'';
        $GLOBALS['lo15_address']= isset($data['loc_address15'])?$data['loc_address15']:'';

        $GLOBALS['lo0_address1']= isset($data['loc_address10'])?$data['loc_address10']:'';
        $GLOBALS['lo1_address1']= isset($data['loc_address11'])?$data['loc_address11']:'';
        $GLOBALS['lo2_address1']= isset($data['loc_address12'])?$data['loc_address12']:'';
        $GLOBALS['lo3_address1']= isset($data['loc_address13'])?$data['loc_address13']:'';
        $GLOBALS['lo4_address1']= isset($data['loc_address14'])?$data['loc_address14']:'';
        $GLOBALS['lo5_address1']= isset($data['loc_address15'])?$data['loc_address15']:'';
        $GLOBALS['lo6_address1']= isset($data['loc_address16'])?$data['loc_address16']:'';
        $GLOBALS['lo7_address1']= isset($data['loc_address17'])?$data['loc_address17']:'';
        $GLOBALS['lo8_address1']= isset($data['loc_address18'])?$data['loc_address18']:'';
        $GLOBALS['lo9_address1']= isset($data['loc_address19'])?$data['loc_address19']:'';
        $GLOBALS['lo10_address1']= isset($data['loc_address110'])?$data['loc_address110']:'';
        $GLOBALS['lo11_address1']= isset($data['loc_address111'])?$data['loc_address111']:'';
        $GLOBALS['lo12_address1']= isset($data['loc_address112'])?$data['loc_address112']:'';
        $GLOBALS['lo13_address1']= isset($data['loc_address113'])?$data['loc_address113']:'';
        $GLOBALS['lo14_address1']= isset($data['loc_address114'])?$data['loc_address114']:'';
        $GLOBALS['lo15_address1']= isset($data['loc_address115'])?$data['loc_address115']:'';

        $GLOBALS['lo0_address2']= isset($data['loc_address20'])?$data['loc_address20']:'';
        $GLOBALS['lo1_address2']= isset($data['loc_address21'])?$data['loc_address21']:'';
        $GLOBALS['lo2_address2']= isset($data['loc_address22'])?$data['loc_address22']:'';
        $GLOBALS['lo3_address2']= isset($data['loc_address23'])?$data['loc_address23']:'';
        $GLOBALS['lo4_address2']= isset($data['loc_address24'])?$data['loc_address24']:'';
        $GLOBALS['lo5_address2']= isset($data['loc_address25'])?$data['loc_address25']:'';
        $GLOBALS['lo6_address2']= isset($data['loc_address26'])?$data['loc_address26']:'';
        $GLOBALS['lo7_address2']= isset($data['loc_address27'])?$data['loc_address27']:'';
        $GLOBALS['lo8_address2']= isset($data['loc_address28'])?$data['loc_address28']:'';
        $GLOBALS['lo9_address2']= isset($data['loc_address29'])?$data['loc_address29']:'';
        $GLOBALS['lo10_address2']= isset($data['loc_address210'])?$data['loc_address210']:'';
        $GLOBALS['lo11_address2']= isset($data['loc_address211'])?$data['loc_address211']:'';
        $GLOBALS['lo12_address2']= isset($data['loc_address212'])?$data['loc_address212']:'';
        $GLOBALS['lo13_address2']= isset($data['loc_address213'])?$data['loc_address213']:'';
        $GLOBALS['lo14_address2']= isset($data['loc_address214'])?$data['loc_address214']:'';
        $GLOBALS['lo15_address2']= isset($data['loc_address215'])?$data['loc_address215']:'';


        $GLOBALS['lo0_party']= isset($data['loc_party0'])?$data['loc_party0']:'';
        $GLOBALS['lo1_party']= isset($data['loc_party1'])?$data['loc_party1']:'';
        $GLOBALS['lo2_party']= isset($data['loc_party2'])?$data['loc_party2']:'';
        $GLOBALS['lo3_party']= isset($data['loc_party3'])?$data['loc_party3']:'';
        $GLOBALS['lo4_party']= isset($data['loc_party4'])?$data['loc_party4']:'';
        $GLOBALS['lo5_party']= isset($data['loc_party5'])?$data['loc_party5']:'';
        $GLOBALS['lo6_party']= isset($data['loc_party6'])?$data['loc_party6']:'';
        $GLOBALS['lo7_party']= isset($data['loc_party7'])?$data['loc_party7']:'';
        $GLOBALS['lo8_party']= isset($data['loc_party8'])?$data['loc_party8']:'';
        $GLOBALS['lo9_party']= isset($data['loc_party9'])?$data['loc_party9']:'';
        $GLOBALS['lo10_party']= isset($data['loc_party10'])?$data['loc_party10']:'';
        $GLOBALS['lo11_party']= isset($data['loc_party11'])?$data['loc_party11']:'';
        $GLOBALS['lo12_party']= isset($data['loc_party12'])?$data['loc_party12']:'';
        $GLOBALS['lo13_party']= isset($data['loc_party13'])?$data['loc_party13']:'';
        $GLOBALS['lo14_party']= isset($data['loc_party14'])?$data['loc_party14']:'';
        $GLOBALS['lo15_party']= isset($data['loc_party15'])?$data['loc_party15']:'';


        $GLOBALS['lo0_phone']= isset($data['loc_name0'])?$data['loc_phone0']:'';
        $GLOBALS['lo1_phone']= isset($data['loc_name1'])?$data['loc_phone1']:'';
        $GLOBALS['lo2_phone']= isset($data['loc_name2'])?$data['loc_phone2']:'';
        $GLOBALS['lo3_phone']= isset($data['loc_name3'])?$data['loc_phone3']:'';

        if(isset($data['loc_name']))
        {
//            if(isset($data['loc_suffix']))
//            {
//                $GLOBALS['loc_name']= $data['loc_name'].",".$data['loc_suffix'];
//            }
//            else
//            {
                $GLOBALS['loc_name']= isset($data['loc_name'])?$data['loc_name']:'';
//            }
        }


        $GLOBALS['loc_suffixwc']= isset($data['loc_suffix'])&&!empty($data['loc_suffix'])?$data['loc_suffix']:'';
        $GLOBALS['loc_sal']= isset($data['loc_sal'])&&!empty($data['loc_sal'])?$data['loc_sal']:'';
        $GLOBALS['loc_suffix']= isset($data['loc_suffix'])&&!empty($data['loc_suffix'])?', '.$data['loc_suffix']:'';
        $GLOBALS['loc_firmname']= isset($data['loc_firmname'])&&!empty($data['loc_firmname'])?$data['loc_firmname']:'';
        $GLOBALS['loc_last_name']= isset($data['loc_last_name'])&&!empty($data['loc_last_name'])?$data['loc_last_name']:'';
        $GLOBALS['loc_first_name']= isset($data['loc_first_name'])&&!empty($data['loc_first_name'])?$data['loc_first_name']:'';
        $GLOBALS['loc_fnm']= isset($data['loc_fnm'])&&!empty($data['loc_fnm'])?$data['loc_fnm']:'';
        $GLOBALS['locWFirmName']= isset($data['locWFirmName'])&&!empty($data['locWFirmName'])?$data['locWFirmName']:'';
        $GLOBALS['loc_lnm']= isset($data['loc_lnm'])&&!empty($data['loc_lnm'])?$data['loc_lnm']:'';
        if(!empty($data['loc_suffix']))
        {
            $GLOBALS['loc_f_name'] = $data['loc_fnm'].' '.$data['lastname'].', '.$data['loc_suffix'];
            if(!empty($GLOBALS['loc_firmname']))
            {

                $GLOBALS['loc_f_name'] .= "\n{$data['loc_firmname']}";
            }
        }
        else
        {
            $GLOBALS['loc_f_name'] = '';
            $GLOBALS['loc_f_name'] = $data['loc_fnm'].' '.$data['lastname'];
            if(!empty($data['loc_firmname']))
            {

                $GLOBALS['loc_f_name'] .= "\n{$data['loc_firmname']}";
            }
        }
        //echo '<br/>'.$GLOBALS['loc_f_name']; exit;
        $GLOBALS['my_locWFirmName']= isset($data['my_locWFirmName'])&&!empty($data['my_locWFirmName'])?$data['my_locWFirmName']:'';
        $GLOBALS['loc_address']= isset($data['loc_address']) && !empty($data['loc_address'])?$data['loc_address']:'';
        $GLOBALS['loc_address1']= isset($data['loc_address1']) && !empty($data['loc_address1']) ?$data['loc_address1']:'';
        $GLOBALS['loc_address2']= isset($data['loc_address2'])&& !empty($data['loc_address2'])?$data['loc_address2']:'';
        $GLOBALS['loc_phone1']= isset($data['loc_phone1'])&& !empty($data['loc_phone1'])?$data['loc_phone1']:'';
        $GLOBALS['loc_bussiness']= isset($data['loc_bussiness'])&& !empty($data['loc_bussiness'])?$data['loc_bussiness']:'';
        $GLOBALS['loc_fax']= isset($data['loc_fax'])&& !empty($data['loc_fax'])?$data['loc_fax']:'';
        $GLOBALS['cc_party_name']= isset($data['cc_party_name'])&& !empty($data['cc_party_name'])?trim($data['cc_party_name']):'';
        $string_add =  $GLOBALS['loc_address'];
        $array = explode(',', $string_add);
        $loc_newadd1= trim($array[0]). ' , '. trim($array[1] , ',');
        $loc_newadd2= trim($array[2]). ', '. trim($array[3] , ',');
        
        $loc_final = '';
        if(!empty($GLOBALS['my_locWFirmName'])){
            $loc_final .= $GLOBALS['my_locWFirmName'];
        }
        if(!empty($GLOBALS['loc_address'])){
            if(!empty($GLOBALS['my_locWFirmName'])){
                $loc_final .= "\n{$GLOBALS['loc_address']}";
            }else{
                $loc_final .= $GLOBALS['loc_address'];
            }
        }
        if(!empty($GLOBALS['loc_address2'])){
            if(!empty($GLOBALS['loc_address'])){
                $loc_final .= "\n{$GLOBALS['loc_address2']}";
            }else{
                $loc_final .= $GLOBALS['loc_address2'];
            }
        }
        if(!empty($GLOBALS['loc_phone1'])){
            if(!empty($GLOBALS['loc_address2'])){
                $loc_final .= "\n{$GLOBALS['loc_phone1']}";
            }else{
                $loc_final .= $GLOBALS['loc_phone1'];
            }
        }
        
        $GLOBALS['loc_final'] = $loc_final;
        $GLOBALS['loc_full'] = $data['loc_full'];
        $GLOBALS['fadj1d1']= isset($data['fadj1d0'])?$data['fadj1d0']:'';
		$GLOBALS['usfadj1d1']= isset($data['fadj1d0'])? strtoupper($data['fadj1d0']):'';
        $GLOBALS['fpob11']= isset($data['fpob10'])?$data['fpob10']:'';
        $GLOBALS['fpob21']= isset($data['fpob20'])?$data['fpob20']:'';
        $GLOBALS['fpob31']= isset($data['fpob30'])?$data['fpob30']:'';
        $GLOBALS['fpob41']= isset($data['fpob40'])?$data['fpob40']:'';
        $GLOBALS['fpob51']= isset($data['fpob50'])?$data['fpob50']:'';
        
        $GLOBALS['npob11']= isset($data['npob10'])?$data['npob10']:'';
        $GLOBALS['npob21']= isset($data['npob20'])?$data['npob20']:'';
        $GLOBALS['npob31']= isset($data['npob30'])?$data['npob30']:'';
        $GLOBALS['npob41']= isset($data['npob40'])?$data['npob40']:'';
        $GLOBALS['npob51']= isset($data['npob50'])?$data['npob50']:'';
        
        $GLOBALS['feamsno1']= isset($data['feamsno0'])?$data['feamsno0']:'';
        $GLOBALS['fadj1e1']= isset($data['fadj1e0'])?$data['fadj1e0']:'';
		$GLOBALS['usfadj1e1']= isset($data['fadj1e0'])? strtoupper($data['fadj1e0']):'';

        $GLOBALS['fpob12']= isset($data['fpob11'])?$data['fpob11']:'';
        $GLOBALS['fpob22']= isset($data['fpob21'])?$data['fpob21']:'';
        $GLOBALS['fpob32']= isset($data['fpob31'])?$data['fpob31']:'';
        $GLOBALS['fpob42']= isset($data['fpob41'])?$data['fpob41']:'';
        $GLOBALS['fpob52']= isset($data['fpob51'])?$data['fpob51']:'';
        
        $GLOBALS['npob12']= isset($data['npob11'])?$data['npob11']:'';
        $GLOBALS['npob22']= isset($data['npob21'])?$data['npob21']:'';
        $GLOBALS['npob32']= isset($data['npob31'])?$data['npob31']:'';
        $GLOBALS['npob42']= isset($data['npob41'])?$data['npob41']:'';
        $GLOBALS['npob52']= isset($data['npob51'])?$data['npob51']:'';
        
        $GLOBALS['feamsno2']= isset($data['feamsno1'])?$data['feamsno1']:'';
        $GLOBALS['fadj1e2']= isset($data['fadj1e1'])?$data['fadj1e1']:'';
		$GLOBALS['usfadj1e2']= isset($data['fadj1e1'])? strtoupper($data['fadj1e1']):'';

//        $GLOBALS['fpob12']= isset($data['fpob12'])?$data['fpob12']:'';
//        $GLOBALS['fpob22']= isset($data['fpob22'])?$data['fpob22']:'';
//        $GLOBALS['fpob32']= isset($data['fpob32'])?$data['fpob32']:'';
//        $GLOBALS['fpob42']= isset($data['fpob42'])?$data['fpob42']:'';
//        $GLOBALS['feamsno2']= isset($data['feamsno3'])?$data['feamsno3']:'';

        $GLOBALS['fadj1e3']= isset($data['fadj1e2'])?$data['fadj1e2']:'';
        $GLOBALS['fpob13']= isset($data['fpob12'])?$data['fpob12']:'';
        $GLOBALS['fpob23']= isset($data['fpob22'])?$data['fpob22']:'';
        $GLOBALS['fpob33']= isset($data['fpob32'])?$data['fpob32']:'';
        $GLOBALS['fpob43']= isset($data['fpob42'])?$data['fpob42']:'';
        $GLOBALS['fpob53']= isset($data['fpob52'])?$data['fpob52']:'';
        
        $GLOBALS['npob13']= isset($data['npob12'])?$data['npob12']:'';
        $GLOBALS['npob23']= isset($data['npob22'])?$data['npob22']:'';
        $GLOBALS['npob33']= isset($data['npob32'])?$data['npob32']:'';
        $GLOBALS['npob43']= isset($data['npob42'])?$data['npob42']:'';
        $GLOBALS['npob53']= isset($data['npob52'])?$data['npob52']:'';
        
        $GLOBALS['feamsno3']= isset($data['feamsno2'])?$data['feamsno2']:'';
        $GLOBALS['fadj1e3']= isset($data['fadj1e2'])?$data['fadj1e2']:'';
		$GLOBALS['usfadj1e3']= isset($data['fadj1e2'])? strtoupper($data['fadj1e2']):'';
        $GLOBALS['fadj1e4']= isset($data['fadj1e3'])?$data['fadj1e3']:'';
        $GLOBALS['fpob14']= isset($data['fpob13'])?$data['fpob13']:'';
        $GLOBALS['fpob24']= isset($data['fpob23'])?$data['fpob23']:'';
        $GLOBALS['fpob34']= isset($data['fpob33'])?$data['fpob33']:'';
        $GLOBALS['fpob44']= isset($data['fpob43'])?$data['fpob43']:'';
        $GLOBALS['fpob54']= isset($data['fpob53'])?$data['fpob53']:'';
        
        $GLOBALS['npob14']= isset($data['npob13'])?$data['npob13']:'';
        $GLOBALS['npob24']= isset($data['npob23'])?$data['npob23']:'';
        $GLOBALS['npob34']= isset($data['npob33'])?$data['npob33']:'';
        $GLOBALS['npob44']= isset($data['npob43'])?$data['npob43']:'';
        $GLOBALS['npob54']= isset($data['npob53'])?$data['npob53']:'';
        
        $GLOBALS['feamsno4']= isset($data['feamsno3'])?$data['feamsno3']:'';
        $GLOBALS['fadj1e4']= isset($data['fadj1e3'])?$data['fadj1e3']:'';
		$GLOBALS['usfadj1e4']= isset($data['fadj1e3'])? strtoupper($data['fadj1e3']):'';

        $GLOBALS['fadj1e5']= isset($data['fadj1e4'])?$data['fadj1e4']:'';
        $GLOBALS['fpob15']= isset($data['fpob14'])?$data['fpob14']:'';
        $GLOBALS['fpob25']= isset($data['fpob24'])?$data['fpob24']:'';
        $GLOBALS['fpob35']= isset($data['fpob34'])?$data['fpob34']:'';
        $GLOBALS['fpob45']= isset($data['fpob44'])?$data['fpob44']:'';
        $GLOBALS['fpob55']= isset($data['fpob54'])?$data['fpob54']:'';
        
        $GLOBALS['npob15']= isset($data['npob14'])?$data['npob14']:'';
        $GLOBALS['npob25']= isset($data['npob24'])?$data['npob24']:'';
        $GLOBALS['npob35']= isset($data['npob34'])?$data['npob34']:'';
        $GLOBALS['npob45']= isset($data['npob44'])?$data['npob44']:'';
        $GLOBALS['npob55']= isset($data['npob54'])?$data['npob54']:'';
        
        $GLOBALS['feamsno5']= isset($data['feamsno4'])?$data['feamsno4']:'';
        $GLOBALS['fadj1e5']= isset($data['fadj1e4'])?$data['fadj1e4']:'';
		$GLOBALS['usfadj1e5']= isset($data['fadj1e4'])? strtoupper($data['fadj1e4']):'';

        $GLOBALS['fadj1e6']= isset($data['fadj1e5'])?$data['fadj1e5']:'';
        $GLOBALS['fpob16']= isset($data['fpob15'])?$data['fpob15']:'';
        $GLOBALS['fpob26']= isset($data['fpob25'])?$data['fpob25']:'';
        $GLOBALS['fpob36']= isset($data['fpob35'])?$data['fpob35']:'';
        $GLOBALS['fpob46']= isset($data['fpob45'])?$data['fpob45']:'';
        
        $GLOBALS['npob16']= isset($data['npob15'])?$data['npob15']:'';
        $GLOBALS['npob26']= isset($data['npob25'])?$data['npob25']:'';
        $GLOBALS['npob36']= isset($data['npob35'])?$data['npob35']:'';
        $GLOBALS['npob46']= isset($data['npob45'])?$data['npob45']:'';
        $GLOBALS['npob56']= isset($data['npob55'])?$data['npob55']:'';
        
        
        $GLOBALS['feamsno6']= isset($data['feamsno5'])?$data['feamsno5']:'';
        $GLOBALS['fadj1e6']= isset($data['fadj1e5'])?$data['fadj1e5']:'';
        $GLOBALS['fadj1e6']= isset($data['fadj1e5'])?$data['fadj1e5']:'';

        $GLOBALS['fpob17']= isset($data['fpob16'])?$data['fpob16']:'';
        $GLOBALS['fpob27']= isset($data['fpob26'])?$data['fpob26']:'';
        $GLOBALS['fpob37']= isset($data['fpob36'])?$data['fpob36']:'';
        $GLOBALS['fpob47']= isset($data['fpob46'])?$data['fpob46']:'';
        
        
        $GLOBALS['npob17']= isset($data['npob16'])?$data['npob16']:'';
        $GLOBALS['npob27']= isset($data['npob26'])?$data['npob26']:'';
        $GLOBALS['npob37']= isset($data['npob36'])?$data['npob36']:'';
        $GLOBALS['npob47']= isset($data['npob46'])?$data['npob46']:'';
        $GLOBALS['npob57']= isset($data['npob56'])?$data['npob56']:'';
        
        $GLOBALS['feamsno7']= isset($data['feamsno6'])?$data['feamsno6']:'';
        $GLOBALS['fadj1e7']= isset($data['fadj1e6'])?$data['fadj1e6']:'';

        $GLOBALS['fpob18']= isset($data['fpob17'])?$data['fpob17']:'';
        $GLOBALS['fpob28']= isset($data['fpob27'])?$data['fpob27']:'';
        $GLOBALS['fpob38']= isset($data['fpob37'])?$data['fpob37']:'';
        $GLOBALS['fpob48']= isset($data['fpob47'])?$data['fpob47']:'';
        
        $GLOBALS['npob18']= isset($data['npob17'])?$data['npob17']:'';
        $GLOBALS['npob28']= isset($data['npob27'])?$data['npob27']:'';
        $GLOBALS['npob38']= isset($data['npob37'])?$data['npob37']:'';
        $GLOBALS['npob48']= isset($data['npob47'])?$data['npob47']:'';
        $GLOBALS['npob58']= isset($data['npob57'])?$data['npob57']:'';
        
        $GLOBALS['feamsno8']= isset($data['feamsno7'])?$data['feamsno7']:'';
        $GLOBALS['fadj1e8']= isset($data['fadj1e7'])?$data['fadj1e7']:'';

        $GLOBALS['fpob19']= isset($data['fpob18'])?$data['fpob18']:'';
        $GLOBALS['fpob29']= isset($data['fpob28'])?$data['fpob28']:'';
        $GLOBALS['fpob39']= isset($data['fpob38'])?$data['fpob38']:'';
        $GLOBALS['fpob49']= isset($data['fpob48'])?$data['fpob48']:'';
        
        $GLOBALS['npob19']= isset($data['npob18'])?$data['npob18']:'';
        $GLOBALS['npob29']= isset($data['npob28'])?$data['npob28']:'';
        $GLOBALS['npob39']= isset($data['npob38'])?$data['npob38']:'';
        $GLOBALS['npob49']= isset($data['npob48'])?$data['npob48']:'';
        $GLOBALS['npob59']= isset($data['npob58'])?$data['npob58']:'';
        
        
        $GLOBALS['feamsno9']= isset($data['feamsno8'])?$data['feamsno8']:'';
        $GLOBALS['fadj1e9']= isset($data['fadj1e8'])?$data['fadj1e8']:'';

        $GLOBALS['fpob110']= isset($data['fpob169'])?$data['fpob19']:'';
        $GLOBALS['fpob210']= isset($data['fpob29'])?$data['fpob29']:'';
        $GLOBALS['fpob310']= isset($data['fpob39'])?$data['fpob39']:'';
        $GLOBALS['fpob410']= isset($data['fpob49'])?$data['fpob49']:'';
        
        $GLOBALS['npob110']= isset($data['npob19'])?$data['npob19']:'';
        $GLOBALS['npob210']= isset($data['npob29'])?$data['npob29']:'';
        $GLOBALS['npob310']= isset($data['npob39'])?$data['npob39']:'';
        $GLOBALS['npob410']= isset($data['npob49'])?$data['npob49']:'';
        $GLOBALS['npob510']= isset($data['npob59'])?$data['npob59']:'';
        
        $GLOBALS['feamsno10']= isset($data['feamsno9'])?$data['feamsno9']:'';
        $GLOBALS['fadj1e10']= isset($data['fadj1e9'])?$data['fadj1e9']:'';

        $GLOBALS['fpob111']= isset($data['fpob110'])?$data['fpob110']:'';
        $GLOBALS['fpob211']= isset($data['fpob210'])?$data['fpob210']:'';
        $GLOBALS['fpob311']= isset($data['fpob310'])?$data['fpob310']:'';
        $GLOBALS['fpob411']= isset($data['fpob410'])?$data['fpob410']:'';
        
        $GLOBALS['npob111']= isset($data['npob110'])?$data['npob110']:'';
        $GLOBALS['npob211']= isset($data['npob210'])?$data['npob210']:'';
        $GLOBALS['npob311']= isset($data['npob310'])?$data['npob310']:'';
        $GLOBALS['npob411']= isset($data['npob410'])?$data['npob410']:'';
        $GLOBALS['npob511']= isset($data['npob510'])?$data['npob510']:'';
        
        $GLOBALS['feamsno11']= isset($data['feamsno10'])?$data['feamsno10']:'';
        $GLOBALS['fadj1e11']= isset($data['fadj1e10'])?$data['fadj1e10']:'';

        $GLOBALS['fpob112']= isset($data['fpob111'])?$data['fpob111']:'';
        $GLOBALS['fpob212']= isset($data['fpob211'])?$data['fpob211']:'';
        $GLOBALS['fpob312']= isset($data['fpob311'])?$data['fpob311']:'';
        $GLOBALS['fpob412']= isset($data['fpob411'])?$data['fpob411']:'';
        
        $GLOBALS['npob112']= isset($data['npob111'])?$data['npob111']:'';
        $GLOBALS['npob212']= isset($data['npob211'])?$data['npob211']:'';
        $GLOBALS['npob312']= isset($data['npob311'])?$data['npob311']:'';
        $GLOBALS['npob412']= isset($data['npob411'])?$data['npob411']:'';
        $GLOBALS['npob512']= isset($data['npob511'])?$data['npob511']:'';
        
        $GLOBALS['feamsno12']= isset($data['feamsno11'])?$data['feamsno11']:'';
        $GLOBALS['fadj1e12']= isset($data['fadj1e11'])?$data['fadj1e11']:'';

        $GLOBALS['fpob113']= isset($data['fpob112'])?$data['fpob112']:'';
        $GLOBALS['fpob213']= isset($data['fpob212'])?$data['fpob212']:'';
        $GLOBALS['fpob213']= isset($data['fpob212'])?$data['fpob212']:'';
        $GLOBALS['fpob313']= isset($data['fpob312'])?$data['fpob312']:'';
        $GLOBALS['fpob413']= isset($data['fpob412'])?$data['fpob412']:'';
        
        $GLOBALS['npob113']= isset($data['npob112'])?$data['npob112']:'';
        $GLOBALS['npob213']= isset($data['npob212'])?$data['npob212']:'';
        $GLOBALS['npob313']= isset($data['npob312'])?$data['npob312']:'';
        $GLOBALS['npob413']= isset($data['npob412'])?$data['npob412']:'';
        $GLOBALS['npob513']= isset($data['npob512'])?$data['npob512']:'';
        
        $GLOBALS['feamsno13']= isset($data['feamsno12'])?$data['feamsno12']:'';
        $GLOBALS['fadj1e13']= isset($data['fadj1e12'])?$data['fadj1e12']:'';

        $GLOBALS['fpob114']= isset($data['fpob113'])?$data['fpob113']:'';
        $GLOBALS['fpob214']= isset($data['fpob213'])?$data['fpob213']:'';
        $GLOBALS['fpob314']= isset($data['fpob313'])?$data['fpob313']:'';
        $GLOBALS['fpob414']= isset($data['fpob413'])?$data['fpob413']:'';
        
        $GLOBALS['npob114']= isset($data['npob113'])?$data['npob113']:'';
        $GLOBALS['npob214']= isset($data['npob213'])?$data['npob213']:'';
        $GLOBALS['npob314']= isset($data['npob313'])?$data['npob313']:'';
        $GLOBALS['npob414']= isset($data['npob413'])?$data['npob413']:'';
        $GLOBALS['npob514']= isset($data['npob513'])?$data['npob513']:'';
        
        
        $GLOBALS['feamsno14']= isset($data['feamsno13'])?$data['feamsno13']:'';
        $GLOBALS['fadj1e14']= isset($data['fadj1e13'])?$data['fadj1e13']:'';

        $GLOBALS['fpob115']= isset($data['fpob114'])?$data['fpob114']:'';
        $GLOBALS['fpob215']= isset($data['fpob214'])?$data['fpob214']:'';
        $GLOBALS['fpob315']= isset($data['fpob314'])?$data['fpob314']:'';
        $GLOBALS['fpob415']= isset($data['fpob414'])?$data['fpob414']:'';
        
        $GLOBALS['npob115']= isset($data['npob114'])?$data['npob114']:'';
        $GLOBALS['npob215']= isset($data['npob214'])?$data['npob214']:'';
        $GLOBALS['npob315']= isset($data['npob314'])?$data['npob314']:'';
        $GLOBALS['npob415']= isset($data['npob414'])?$data['npob414']:'';
        $GLOBALS['npob515']= isset($data['npob514'])?$data['npob514']:'';
        
        $GLOBALS['feamsno15']= isset($data['feamsno14'])?$data['feamsno14']:'';
        $GLOBALS['fadj1e15']= isset($data['fadj1e14'])?$data['fadj1e14']:'';

        $GLOBALS['fpob116']= isset($data['fpob115'])?$data['fpob115']:'';
        $GLOBALS['fpob216']= isset($data['fpob215'])?$data['fpob215']:'';
        $GLOBALS['fpob316']= isset($data['fpob315'])?$data['fpob315']:'';
        $GLOBALS['fpob416']= isset($data['fpob415'])?$data['fpob415']:'';
        
        $GLOBALS['npob116']= isset($data['npob115'])?$data['npob115']:'';
        $GLOBALS['npob216']= isset($data['npob215'])?$data['npob215']:'';
        $GLOBALS['npob316']= isset($data['npob315'])?$data['npob315']:'';
        $GLOBALS['npob416']= isset($data['npob415'])?$data['npob415']:'';
        $GLOBALS['npob516']= isset($data['npob515'])?$data['npob515']:'';
        
        $GLOBALS['feamsno16']= isset($data['feamsno15'])?$data['feamsno15']:'';
        $GLOBALS['fadj1e16']= isset($data['fadj1e15'])?$data['fadj1e15']:'';
        
        $GLOBALS['i_type1']= isset($data['i_type0'])?$data['i_type0']:'';
        $GLOBALS['i_type2']= isset($data['i_type1'])?$data['i_type1']:'';
        $GLOBALS['i_type3']= isset($data['i_type2'])?$data['i_type2']:'';
        $GLOBALS['i_type4']= isset($data['i_type3'])?$data['i_type3']:'';
        $GLOBALS['i_type5']= isset($data['i_type4'])?$data['i_type4']:'';
        
        
        $GLOBALS['i_y1'] = isset($data['i_type0'])&&!empty($data['i_type0'])?'X':'';
        $GLOBALS['i_y2'] = isset($data['i_type1'])&&!empty($data['i_type1'])?'X':'';
        $GLOBALS['i_y3'] = isset($data['i_type2'])&&!empty($data['i_type2'])?'X':'';
        $GLOBALS['i_y4'] = isset($data['i_type3'])&&!empty($data['i_type3'])?'X':'';
        $GLOBALS['i_y5'] = isset($data['i_type4'])&&!empty($data['i_type4'])?'X':'';
        $GLOBALS['i_y6'] = isset($data['i_type5'])&&!empty($data['i_type5'])?'X':'';
        $GLOBALS['i_y7'] = isset($data['i_type6'])&&!empty($data['i_type6'])?'X':'';
        $GLOBALS['i_y8'] = isset($data['i_type7'])&&!empty($data['i_type7'])?'X':'';
        $GLOBALS['i_y9'] = isset($data['i_type8'])&&!empty($data['i_type8'])?'X':'';
        $GLOBALS['i_y10'] = isset($data['i_type9'])&&!empty($data['i_type9'])?'X':'';
        $GLOBALS['i_y11'] = isset($data['i_type10'])&&!empty($data['i_type10'])?'X':'';
        $GLOBALS['i_y12'] = isset($data['i_type11'])&&!empty($data['i_type11'])?'X':'';
        $GLOBALS['i_y13'] = isset($data['i_type12'])&&!empty($data['i_type12'])?'X':'';
        $GLOBALS['i_y14'] = isset($data['i_type13'])&&!empty($data['i_type13'])?'X':'';
        $GLOBALS['i_y15'] = isset($data['i_type14'])&&!empty($data['i_type14'])?'X':'';
        $GLOBALS['i_y16'] = isset($data['i_type15'])&&!empty($data['i_type15'])?'X':'';
        
        $GLOBALS['i_n1'] = isset($data['i_type0'])&&empty($data['i_type0'])&&!empty($data['single_i0'])?'X':'';
        $GLOBALS['i_n2'] = isset($data['i_type1'])&&empty($data['i_type1'])&&!empty($data['single_i1'])?'X':'';
        $GLOBALS['i_n3'] = isset($data['i_type2'])&&empty($data['i_type2'])&&!empty($data['single_i2'])?'X':'';
        $GLOBALS['i_n4'] = isset($data['i_type3'])&&empty($data['i_type3'])&&!empty($data['single_i3'])?'X':'';
        $GLOBALS['i_n5'] = isset($data['i_type4'])&&empty($data['i_type4'])&&!empty($data['single_i4'])?'X':'';
        $GLOBALS['i_n6'] = isset($data['i_type5'])&&empty($data['i_type5'])&&!empty($data['single_i5'])?'X':'';
        $GLOBALS['i_n7'] = isset($data['i_type6'])&&empty($data['i_type6'])&&!empty($data['single_i6'])?'X':'';
        $GLOBALS['i_n8'] = isset($data['i_type7'])&&empty($data['i_type7'])&&!empty($data['single_i7'])?'X':'';
        $GLOBALS['i_n9'] = isset($data['i_type8'])&&empty($data['i_type8'])&&!empty($data['single_i8'])?'X':'';
        $GLOBALS['i_n10'] = isset($data['i_type9'])&&empty($data['i_type9'])&&!empty($data['single_i9'])?'X':'';
        $GLOBALS['i_n11'] = isset($data['i_type10'])&&empty($data['i_type10'])&&!empty($data['single_i10'])?'X':'';
        $GLOBALS['i_n12'] = isset($data['i_type11'])&&empty($data['i_type11'])&&!empty($data['single_i11'])?'X':'';
        $GLOBALS['i_n13'] = isset($data['i_type12'])&&empty($data['i_type12'])&&!empty($data['single_i12'])?'X':'';
        $GLOBALS['i_n14'] = isset($data['i_type13'])&&empty($data['i_type13'])&&!empty($data['single_i13'])?'X':'';
        $GLOBALS['i_n15'] = isset($data['i_type14'])&&empty($data['i_type14'])&&!empty($data['single_i14'])?'X':'';
        $GLOBALS['i_n16'] = isset($data['i_type15'])&&empty($data['i_type15'])&&!empty($data['single_i15'])?'X':'';
        
        $GLOBALS['inj_dt1']= isset($data['inj_dt0'])?$data['inj_dt0']:'';
        $GLOBALS['inj_dt2']= isset($data['inj_dt1'])?$data['inj_dt1']:'';
        $GLOBALS['inj_dt3']= isset($data['inj_dt2'])?$data['inj_dt2']:'';
        $GLOBALS['inj_dt4']= isset($data['inj_dt3'])?$data['inj_dt3']:'';
        $GLOBALS['inj_dt5']= isset($data['inj_dt4'])?$data['inj_dt4']:'';
        //echo '<pre>'; print_r($GLOBALS); exit;
        if($GLOBALS['i_type1'] == 1)
        {
            $GLOBALS['C'] = 1;
        }else{
            $GLOBALS['C'] = 0;
        }
        
        
        $GLOBALS['single_i1']= isset($data['single_i0'])?$data['single_i0']:'';
        $GLOBALS['single_i2']= isset($data['single_i1'])?$data['single_i1']:'';
        $GLOBALS['single_i3']= isset($data['single_i2'])?$data['single_i2']:'';
        $GLOBALS['single_i4']= isset($data['single_i3'])?$data['single_i3']:'';
        $GLOBALS['single_i5']= isset($data['single_i4'])?$data['single_i4']:'';
        $GLOBALS['single_i6']= isset($data['single_i5'])?$data['single_i5']:'';
        $GLOBALS['single_i7']= isset($data['single_i6'])?$data['single_i6']:'';
        $GLOBALS['single_i8']= isset($data['single_i7'])?$data['single_i7']:'';
        $GLOBALS['single_i9']= isset($data['single_i8'])?$data['single_i8']:'';
        $GLOBALS['single_i10']= isset($data['single_i9'])?$data['single_i9']:'';
        $GLOBALS['single_i11']= isset($data['single_i10'])?$data['single_i10']:'';
        $GLOBALS['single_i12']= isset($data['single_i11'])?$data['single_i11']:'';
        $GLOBALS['single_i13']= isset($data['single_i12'])?$data['single_i12']:'';
        $GLOBALS['single_i14']= isset($data['single_i13'])?$data['single_i13']:'';
        $GLOBALS['single_i15']= isset($data['single_i14'])?$data['single_i14']:'';
        $GLOBALS['single_i16']= isset($data['single_i15'])?$data['single_i15']:'';

        //tab 5 no 5  fieldadj5c fieldadj5d
        $GLOBALS['adj5b']= isset($data['fieldadj5b'])?$data['fieldadj5b']:'';
        $GLOBALS['adj5c']= isset($data['fieldadj5c'])?$data['fieldadj5c']:'';

		if(isset($data['fieldadj5d']))
		{
			$adjfd = explode(" ", $data['fieldadj5d']);
			$GLOBALS['adj5d']= isset($adjfd[0])? $adjfd[0]:'';
		}
		else{
			$GLOBALS['adj5d']= '';
		}
        
        //tab 5 no 7

        $GLOBALS['doctors']= isset($data['fielddoctors'])?$data['fielddoctors']:'';
        $GLOBALS['adj7d']= isset($data['fieldadj7d'])?$data['fieldadj7d']:'';
        $GLOBALS['my_doctors']= isset($data['fielddoctors'])?strtoupper($data['fielddoctors']):'';
        $GLOBALS['my_adj7d']= isset($data['fieldadj7d'])?strtoupper($data['fieldadj7d']):'';
	$GLOBALS['adj9g']= isset($data['fieldadj9g'])?$data['fieldadj9g']:'';
        if($GLOBALS['adj9g'] != '')
        {
            $GLOBALS['9o'] =  'Y';
        }
        else{
            $GLOBALS['9o'] =  'N';
        }
		
        $GLOBALS['adj7b']= isset($data['fieldadj7b'])? str_replace(" 00:00:00","",$data['fieldadj7b']):'';
        $GLOBALS['adj7a']= isset($data['fieldadj7a'])?$data['fieldadj7a']:'';
        $GLOBALS['7ay']= isset($data['fieldadj7a']) && $data['fieldadj7a'] == 'Y'? 'X':'';
        $GLOBALS['7an']= isset($data['fieldadj7a']) && ($data['fieldadj7a'] == 'N' || $data['fieldadj7a'] == '')? 'X':'';
        $GLOBALS['adj7c']= isset($data['fieldadj7c'])?$data['fieldadj7c']:'';
        $GLOBALS['7cy']= isset($data['fieldadj7c']) && $data['fieldadj7c'] == 'Y'? 'X':'';
        $GLOBALS['7cn']= isset($data['fieldadj7c']) && ($data['fieldadj7c']  == 'N' || $data['fieldadj7c']  == '' )? 'X':'';
        $GLOBALS['5ay']= isset($data['fieldadj5a']) && $data['fieldadj5a'] == 'Y'? 'X':'';
        $GLOBALS['5an']= isset($data['fieldadj5a']) && ($data['fieldadj5a'] == 'N' || $data['fieldadj5a'] == '')? 'X':'';
        $GLOBALS['6ay']= isset($data['fieldadj6a']) && $data['fieldadj6a'] == 'Y'? 'X':'';
        $GLOBALS['6an']= isset($data['fieldadj6a']) && ($data['fieldadj6a'] == 'N' || $data['fieldadj6a'] == '')? 'X':'';
        $GLOBALS['7ey']= isset($data['fieldadj7e']) && $data['fieldadj7e'] == 'Y'? 'X':'';
        $GLOBALS['7en']= isset($data['fieldadj7e']) && ($data['fieldadj7e'] == 'N' || $data['fieldadj7e'] == '')? 'X':'';
        $GLOBALS['9a']= isset($data['fieldadj9a']) && $data['fieldadj9a'] == 'Y'? 'Y':'N';
        $GLOBALS['9b']= isset($data['fieldadj9b']) && $data['fieldadj9b'] == 'Y'? 'Y':'N';
        $GLOBALS['9c']= isset($data['fieldadj9c']) && $data['fieldadj9c'] == 'Y'? 'Y':'N';
        $GLOBALS['9d']= isset($data['fieldadj9d']) && $data['fieldadj9d'] == 'Y'? 'Y':'N';
        $GLOBALS['9e']= isset($data['fieldadj9e']) && $data['fieldadj9e'] == 'Y'? 'Y':'N';
        $GLOBALS['9f']= isset($data['fieldadj9f']) && $data['fieldadj9f'] == 'Y'? 'Y':'N';
        //echo '<pre>'; print_r($GLOBALS); exit;
        $GLOBALS['testvariable']='N';
        
        //system information
         $GLOBALS['fcity']= isset($data['fcity'])?strtoupper($data['fcity']):'';//firm city
         $GLOBALS['fcityl']= isset($data['fcity'])? $data['fcity'] :'';//firm city
         $GLOBALS['fadd1']= isset($data['firmaddr1'])?$data['firmaddr1']:'';
         $GLOBALS['fadd1c']= isset($data['firmaddr1'])?strtoupper($data['firmaddr1']):'';
         $GLOBALS['fszip']=isset($data['firmstatezip'])?$data['firmstatezip']:'';
         $GLOBALS['fphone']=isset($data['firmphone'])?$data['firmphone']:'';
         $GLOBALS['AppAtt']='Angela Barba';
         $GLOBALS['fs']=isset($data['firmstate'])?$data['firmstate']:'';//firm state
		 $GLOBALS['ufs']=isset($data['firmstate'])? strtoupper($data['firmstate']):'';//firm state
         $GLOBALS['fzip']=isset($data['firmzip'])?$data['firmzip']:'';//firm zip code
         $GLOBALS['fcontry']=isset($data['firmcountry'])?$data['firmcountry']:'';// contry name
         $GLOBALS['FEAMSRefNo']=isset($data['EAMSRefNo'])?$data['EAMSRefNo']:'';// contry name
         $GLOBALS['finame']=isset($data['firmname'])?$data['firmname']:'';//firm name
         $GLOBALS['ffax']=isset($data['firmfax'])?$data['firmfax']:'';
         $GLOBALS['sfirmnameuc'] = isset($data['sfirmname'])?ucfirst($data['sfirmname']):'';
         $newFnm = '';
//         if(empty($data['sfirmname']))
//         {
             $data['sfirmname'] = 'STRAUSSNER ♦ SHERMAN';
         //}
        // Initialize the TBS instance
         
        $data['sfirmname'] = strtoupper($data['sfirmname']);
        /*$firmName[] = 'STRAUSSNER';
        $firmName[] =  '♦';
        $firmName[] =  'SHERMAN';
        foreach($firmName as $key=>$val)
        {
            //$val = strtoupper($val);
            $newFnm .= "{$val}\n";
        }*/
        $newFnm = 'STRAUSSNER ♦ SHERMAN';
         $GLOBALS['attorneynames'] = isset($data['attorneyname']) && !empty($data['attorneyname']) ?$data['attorneyname']:'';
         $GLOBALS['attorneyname'] = isset($data['attorneyname']) && !empty($data['attorneyname']) ?strtoupper($data['attorneyname']):'';
		 $GLOBALS['attorneyfullname'] = isset($data['attorneyname']) && !empty($data['attorneyname']) ?ucfirst($data['attorneyname']):'';
         $GLOBALS['attrnyName'] = isset($data['attorneyname']) && !empty($data['attorneyname']) ? $data['attorneyname'] :'';
		 $GLOBALS['uattrnyName'] = isset($data['attorneyname']) && !empty($data['attorneyname']) ? strtoupper($data['attorneyname']) :'';

		 $attfname = explode(" ",$data['attorneyname']);

		 $GLOBALS['attorneyfname'] = isset($attfname[0]) && !empty($attfname[0]) ?strtoupper($attfname[0]):'';
		 $GLOBALS['attorneylname'] = isset($attfname[1]) && !empty($attfname[1]) ?strtoupper($attfname[1]):'';

         $GLOBALS['attorneynameesq'] = isset($data['attorneyname']) && !empty($data['attorneyname']) ?strtoupper($data['attorneyname'].', ESQ'):'';
         $GLOBALS['client_phome'] = (isset($data['client_phome'])) ? $data['client_phome'] : "";
         
         //echo $GLOBALS['attorneyname']; exit;
         $GLOBALS['atty_first'] = isset($data['atty_first'])?$data['atty_first']:'';
         $GLOBALS['atty_last'] = isset($data['atty_last'])?$data['atty_last']:'';
		 $GLOBALS['atty_firstupper'] = isset($data['atty_first'])? strtoupper($data['atty_first']):'';
         $GLOBALS['atty_lastupper'] = isset($data['atty_last'])? strtoupper($data['atty_last']):'';
         $GLOBALS['atty_hand'] = isset($data['atty_hand'])?$data['atty_hand']:'';
         $GLOBALS['lusername'] = isset($data['lusername'])?$data['lusername']:'';
		 $GLOBALS['eamsref'] = isset($data['eamsref'])?$data['eamsref']:'';
		 $GLOBALS['ueamsref'] = isset($data['eamsref'])? strtoupper($data['eamsref']):'';
        // $GLOBALS['sfirmname'] = isset($data['sfirmname'])?$data['sfirmname']:'';
        // $GLOBALS['SFIRMNAME'] = isset($data['sfirmname'])?strtoupper($data['sfirmname']):'';
         $GLOBALS['sfirmname'] = 'STRAUSSNER ♦ SHERMAN';
         $GLOBALS['SFIRMNAME'] = 'STRAUSSNER ♦ SHERMAN';
         $GLOBALS['firmfletter'] = isset($data['firmfletter'])?strtoupper($data['firmfletter']):'';
		 $GLOBALS['firmfletters'] = isset($data['firmfletter'])?ucfirst($data['firmfletter']):'';
         $GLOBALS['titlefirmname'] = $newFnm;
         $GLOBALS['partyData'] = isset($data['partyData'])?$data['partyData']:'';
         $GLOBALS['partyData1'] = isset($data['partyData1'])?$data['partyData1']:'';

         $GLOBALS['lastparty'] = isset($data['last_Party'])?$data['last_Party']:'';
         $GLOBALS['last_Party_lname'] = isset($data['last_Party_lname'])?$data['last_Party_lname']:'';
         $GLOBALS['last_Party_lnameupper'] = isset($data['last_Party_lname'])? strtoupper($data['last_Party_lname']):'';
         $GLOBALS['last_Party_sal'] = isset($data['last_Party_sal'])?$data['last_Party_sal']:'';
         $GLOBALS['last_Party_suffix'] = isset($data['last_Party_suffix']) && !empty($data['last_Party_suffix'])? ', '.$data['last_Party_suffix']:'';
         $GLOBALS['last_Party_frstname'] = isset($data['last_Party_frstname'])?$data['last_Party_frstname']:'';
         $GLOBALS['last_Party_frstnameupper'] = isset($data['last_Party_frstname'])? strtoupper($data['last_Party_frstname']):'';
         $GLOBALS['last_Party_fname'] = isset($data['last_Party_frname'])?$data['last_Party_frname']:'';
         $GLOBALS['last_Party_full'] = isset($data['last_Party_full'])?$data['last_Party_full']:'';
         $GLOBALS['last_Party_add1'] = isset($data['last_Party_add1'])?$data['last_Party_add1']:'';
         $GLOBALS['last_Party_add2'] = isset($data['last_Party_add2'])?$data['last_Party_add2']:'';
         $GLOBALS['last_Party_phone1'] = isset($data['last_Party_phone1'])?$data['last_Party_phone1']:'';
         $GLOBALS['defendentInj'] = isset($data['fieldEname'])?$data['fieldEname']:'';
          // echo '<pre>'; print_r($GLOBALS); exit;
         //*Set variable for form starts [Namrata]*//
         $GLOBALS['doi1_startdate'] = isset($data['doi1_startdate']) ? $data['doi1_startdate'] : '';
         $GLOBALS['doi1_enddate'] = isset($data['doi1_enddate']) ? $data['doi1_enddate'] : '';
         $GLOBALS['caseuserBdate'] = isset($data['caseuserBdate']) ? $data['caseuserBdate'] : '';
        
         if($data['last_Party_frstname'] != '' && $data['last_Party_lname'] != ''){
            $GLOBALS['last_Party_suffix'] = isset($data['last_Party_suffix']) && !empty($data['last_Party_suffix'])? ', '.$data['last_Party_suffix']:'';
            $GLOBALS['last_Party_frstname'] = isset($data['last_Party_frstname'])?$data['last_Party_frstname']:'';
            $GLOBALS['last_Party_frstnameupper'] = isset($data['last_Party_frstname'])? strtoupper($data['last_Party_frstname']):'';
            $GLOBALS['last_Party_lname'] = isset($data['last_Party_lname'])?$data['last_Party_lname']:'';
            $GLOBALS['last_Party_lnameupper'] = isset($data['last_Party_lname'])? strtoupper($data['last_Party_lname']):'';
         }else{
            $GLOBALS['last_Party_suffix'] = '';
            $GLOBALS['last_Party_frstnameupper'] = isset($data['last_Party_frname'])? strtoupper($data['last_Party_frname']):'';
            $GLOBALS['last_Party_lnameupper'] = '';
            $GLOBALS['last_Party_frstname']= isset($data['last_Party_frname'])? $data['last_Party_frname']:'';
            $GLOBALS['last_Party_lname']='';
         }
         
         
        //for selected injury
         //echo "<pre>"; print_r($GLOBALS); exit;
         // echo "<pre>"; print_r($data['SeectedInjuryDetails']); exit;
         $GLOBALS['S_dateofinjury'] = (isset($data['SeectedInjuryDetails'][0]['adj1c'])) ? $data['SeectedInjuryDetails'][0]['adj1c'] : "";
         $GLOBALS['S_wcabcasno'] = (isset($data['SeectedInjuryDetails'][0]['case_no'])) ? $data['SeectedInjuryDetails'][0]['case_no'] : "";
         $GLOBALS['S_social_sec'] = (isset($data['SeectedInjuryDetails'][0]['social_sec'])) ? $data['SeectedInjuryDetails'][0]['social_sec'] : "";
         $GLOBALS['S_occup'] = (isset($data['SeectedInjuryDetails'][0]['adj1a'])) ? $data['SeectedInjuryDetails'][0]['adj1a'] : "";
		 $GLOBALS['S_occupupper'] = (isset($data['SeectedInjuryDetails'][0]['adj1a'])) ? strtoupper($data['SeectedInjuryDetails'][0]['adj1a']) : "";
         $GLOBALS['S_iadd'] = (isset($data['SeectedInjuryDetails'][0]['adj1d'])) ? $data['SeectedInjuryDetails'][0]['adj1d'] : "";
         $GLOBALS['S_icity'] = (isset($data['SeectedInjuryDetails'][0]['adj1d2'])) ? $data['SeectedInjuryDetails'][0]['adj1d2'] : "";
         $GLOBALS['S_ist'] = (isset($data['SeectedInjuryDetails'][0]['adj1d3'])) ? $data['SeectedInjuryDetails'][0]['adj1d3'] : "";
		
         $GLOBALS['my_S_iadd'] = (isset($data['SeectedInjuryDetails'][0]['adj1d'])) ? strtoupper($data['SeectedInjuryDetails'][0]['adj1d']) : "";
		 $GLOBALS['uS_icity'] = (isset($data['SeectedInjuryDetails'][0]['adj1d2'])) ? strtoupper($data['SeectedInjuryDetails'][0]['adj1d2']) : "";
         $GLOBALS['uS_ist'] = (isset($data['SeectedInjuryDetails'][0]['adj1d3'])) ? strtoupper($data['SeectedInjuryDetails'][0]['adj1d3']) : "";
         $GLOBALS['S_izp'] = (isset($data['SeectedInjuryDetails'][0]['adj1d4'])) ? $data['SeectedInjuryDetails'][0]['adj1d4'] : "";
         $GLOBALS['S_adj1e'] = (isset($data['SeectedInjuryDetails'][0]['adj1e'])) ? $data['SeectedInjuryDetails'][0]['adj1e'] : "";
		 $GLOBALS['uS_adj1e'] = (isset($data['SeectedInjuryDetails'][0]['adj1e'])) ? strtoupper($data['SeectedInjuryDetails'][0]['adj1e']) : "";

         //Employers
         $GLOBALS['S_e_name'] = (isset($data['SeectedInjuryDetails'][0]['e_name'])) ? $data['SeectedInjuryDetails'][0]['e_name'] : "";
		 $GLOBALS['uS_e_name'] = (isset($data['SeectedInjuryDetails'][0]['e_name'])) ? strtoupper($data['SeectedInjuryDetails'][0]['e_name']) : "";
         $GLOBALS['S_e_address'] = (isset($data['SeectedInjuryDetails'][0]['e_address'])) ? $data['SeectedInjuryDetails'][0]['e_address'] : "";
		 $GLOBALS['uS_e_address'] = (isset($data['SeectedInjuryDetails'][0]['e_address'])) ? strtoupper($data['SeectedInjuryDetails'][0]['e_address']) : "";
         $GLOBALS['S_e_city'] = (isset($data['SeectedInjuryDetails'][0]['e_city'])) ? $data['SeectedInjuryDetails'][0]['e_city'] : "";
		 $GLOBALS['uS_e_city'] = (isset($data['SeectedInjuryDetails'][0]['e_city'])) ? strtoupper($data['SeectedInjuryDetails'][0]['e_city']) : "";
         $GLOBALS['S_e_state'] = (isset($data['SeectedInjuryDetails'][0]['e_state'])) ? $data['SeectedInjuryDetails'][0]['e_state'] : "";
		 $GLOBALS['uS_e_state'] = (isset($data['SeectedInjuryDetails'][0]['e_state'])) ? strtoupper($data['SeectedInjuryDetails'][0]['e_state']) : "";
         $GLOBALS['S_e_zip'] = (isset($data['SeectedInjuryDetails'][0]['e_zip'])) ? $data['SeectedInjuryDetails'][0]['e_zip'] : "";
         $GLOBALS['S_e_phone'] = (isset($data['SeectedInjuryDetails'][0]['e_phone'])) ? $data['SeectedInjuryDetails'][0]['e_phone'] : "";
         $GLOBALS['S_e_fax'] = (isset($data['SeectedInjuryDetails'][0]['e_fax'])) ? $data['SeectedInjuryDetails'][0]['e_fax'] : "";

         //Carrers
         $GLOBALS['S_i_name'] = (isset($data['SeectedInjuryDetails'][0]['i_name'])) ? $data['SeectedInjuryDetails'][0]['i_name'] : "";
         $GLOBALS['S_i_address'] = (isset($data['SeectedInjuryDetails'][0]['i_address'])) ? $data['SeectedInjuryDetails'][0]['i_address'] : "";
         $GLOBALS['S_i_city'] = (isset($data['SeectedInjuryDetails'][0]['i_city'])) ? $data['SeectedInjuryDetails'][0]['i_city'] : "";
         $GLOBALS['S_i_state'] = (isset($data['SeectedInjuryDetails'][0]['i_state'])) ? $data['SeectedInjuryDetails'][0]['i_state'] : "";
         $GLOBALS['S_i_zip'] = (isset($data['SeectedInjuryDetails'][0]['i_zip'])) ? $data['SeectedInjuryDetails'][0]['i_zip'] : "";
         $GLOBALS['S_i_phone'] = (isset($data['SeectedInjuryDetails'][0]['i_phone'])) ? $data['SeectedInjuryDetails'][0]['i_phone'] : "";
         $GLOBALS['S_i_fax'] = (isset($data['SeectedInjuryDetails'][0]['i_fax'])) ? $data['SeectedInjuryDetails'][0]['i_fax'] : "";
         $GLOBALS['S_i_adjsal'] = (isset($data['SeectedInjuryDetails'][0]['i_adjsal'])) ? $data['SeectedInjuryDetails'][0]['i_adjsal'] : "";
         $GLOBALS['S_i_adjfst'] = (isset($data['SeectedInjuryDetails'][0]['i_adjfst'])) ? $data['SeectedInjuryDetails'][0]['i_adjfst'] : "";
		 $GLOBALS['S_i_adjfstupper'] = (isset($data['SeectedInjuryDetails'][0]['i_adjfst'])) ? strtoupper($data['SeectedInjuryDetails'][0]['i_adjfst']) : "";
         $GLOBALS['S_i_adjuster'] = (isset($data['SeectedInjuryDetails'][0]['i_adjuster'])) ? $data['SeectedInjuryDetails'][0]['i_adjuster'] : "";
		 $GLOBALS['S_i_adjusterupper'] = (isset($data['SeectedInjuryDetails'][0]['i_adjuster'])) ? strtoupper($data['SeectedInjuryDetails'][0]['i_adjuster']) : "";
         $GLOBALS['S_iclaimno'] = (isset($data['SeectedInjuryDetails'][0]['i_claimno'])) ? $data['SeectedInjuryDetails'][0]['i_claimno'] : "";
         $GLOBALS['S_i2claimno'] = (isset($data['SeectedInjuryDetails'][0]['i2_claimno'])) ? $data['SeectedInjuryDetails'][0]['i2_claimno'] : "";
         
         //Attorney
         $GLOBALS['S_d1_firm'] = (isset($data['SeectedInjuryDetails'][0]['d1_firm'])) ? $data['SeectedInjuryDetails'][0]['d1_firm'] : "";
         $GLOBALS['S_d1_first'] = (isset($data['SeectedInjuryDetails'][0]['d1_first'])) ? $data['SeectedInjuryDetails'][0]['d1_first'] : "";
         $GLOBALS['S_d1_last'] = (isset($data['SeectedInjuryDetails'][0]['d1_last'])) ? $data['SeectedInjuryDetails'][0]['d1_last'] : "";
         $GLOBALS['S_d1_address'] = (isset($data['SeectedInjuryDetails'][0]['d1_address'])) ? $data['SeectedInjuryDetails'][0]['d1_address'] : "";
         $GLOBALS['S_d1_city'] = (isset($data['SeectedInjuryDetails'][0]['d1_city'])) ? $data['SeectedInjuryDetails'][0]['d1_city'] : "";
         $GLOBALS['S_d1_state'] = (isset($data['SeectedInjuryDetails'][0]['d1_state'])) ? $data['SeectedInjuryDetails'][0]['d1_state'] : "";
         $GLOBALS['S_d1_zip'] = (isset($data['SeectedInjuryDetails'][0]['d1_zip'])) ? $data['SeectedInjuryDetails'][0]['d1_zip'] : "";
         $GLOBALS['S_d1_phone'] = (isset($data['SeectedInjuryDetails'][0]['d1_phone'])) ? $data['SeectedInjuryDetails'][0]['d1_phone'] : "";
         $GLOBALS['S_d1_fax'] = (isset($data['SeectedInjuryDetails'][0]['d1_fax'])) ? $data['SeectedInjuryDetails'][0]['d1_fax'] : "";
		 $GLOBALS['attroneyres_name'] = (isset($data['SeectedInjuryDetails'][0]['firmatty1'])) ? $data['SeectedInjuryDetails'][0]['firmatty1'] : "";

        $GLOBALS['IPArty_eams'] = "";
		$GLOBALS['IPArty_eamsupper'] = "";
        $GLOBALS['IPArty_eams_add1'] = "";
        $GLOBALS['IPArty_eams_add2'] = "";
        $GLOBALS['IPArty_eams_c'] = "";
        $GLOBALS['IPArty_eams_s'] = "";
		$GLOBALS['IPArty_eams_add1upper'] = "";
        $GLOBALS['IPArty_eams_add2upper'] = "";
        $GLOBALS['IPArty_eams_cupper'] = "";
        $GLOBALS['IPArty_eams_supper'] = "";
        $GLOBALS['IPArty_eams_z'] = "";
        $GLOBALS['IPArty_eams_p'] = "";
        $GLOBALS['IPArty_fname'] = "";
        $GLOBALS['IPArty_lname'] = "";
		$GLOBALS['IPArty_fnameupper'] = "";
        $GLOBALS['IPArty_lnameupper'] = "";
        $GLOBALS['EPArty_name'] = "";
        $GLOBALS['EPArty_add1'] = "";
        $GLOBALS['EPArty_add2'] = "";
        $GLOBALS['EPArty_c'] = "";
        $GLOBALS['EPArty_s'] = "";
		
		$GLOBALS['uEPArty_name'] = "";
        $GLOBALS['uEPArty_add1'] = "";
        $GLOBALS['uEPArty_add2'] = "";
        $GLOBALS['uEPArty_c'] = "";
        $GLOBALS['uEPArty_s'] = "";
		
        $GLOBALS['EPArty_z'] = "";
        $GLOBALS['EPArty_p'] = "";
        $GLOBALS['DefPArty_firm'] = "";
		$GLOBALS['DefPArty_firmupper'] = "";
        $GLOBALS['DefPArty_fname'] = "";
        $GLOBALS['DefPArty_lname'] = "";
        $GLOBALS['DefPArty_add1'] = "";
        $GLOBALS['DefPArty_add2'] = "";
        $GLOBALS['DefPArty_c'] = "";
        $GLOBALS['DefPArty_s'] = "";
		$GLOBALS['DefPArty_fnameupper'] = "";
        $GLOBALS['DefPArty_lnameupper'] = "";
        $GLOBALS['DefPArty_suffixupper'] = "";
        $GLOBALS['DefPArty_add1upper'] = "";
        $GLOBALS['DefPArty_add2upper'] = "";
        $GLOBALS['DefPArty_cupper'] = "";
        $GLOBALS['DefPArty_supper'] = "";
        $GLOBALS['DefPArty_z'] = "";
        $GLOBALS['DefPArty_p'] = "";
        $GLOBALS['dfirm'] = "";
        $GLOBALS['dphn_new'] = "";
        $GLOBALS['daddress1'] = "";
        $GLOBALS['daddress2'] = "";
        $GLOBALS['dcity'] = "";
        $GLOBALS['dstate'] = "";
        $GLOBALS['dzip'] = "";
        $GLOBALS['ins_nm'] = '';
	$GLOBALS['ins_nmupper'] = '';
        $GLOBALS['DefPArty_firmno'] = '';
        $GLOBALS['ifirm'] = '';
        $GLOBALS['iaddress1'] = '';
        $GLOBALS['iaddress2'] = '';
        $GLOBALS['icity'] = '';
        $GLOBALS['istate'] = '';
        $GLOBALS['izip'] = '';

        if(isset($data['selectedtinsparty']) && !empty($data['selectedtinsparty'])) {
            foreach($data['selectedtinsparty'] as  $key => $value) :
                $GLOBALS['IPArty_eams'] = $value['name'];
				$GLOBALS['IPArty_eamsupper'] = strtoupper($value['name']);
				$GLOBALS['IPArty_fname'] = $value['first'];
                $GLOBALS['IPArty_lname'] = $value['last'];
				$GLOBALS['IPArty_fnameupper'] = strtoupper($value['first']);
                $GLOBALS['IPArty_lnameupper'] = strtoupper($value['last']);
                $GLOBALS['IPArty_eams_add1'] = $value['address1'];
                $GLOBALS['IPArty_eams_add2'] = $value['address2'];
                $GLOBALS['IPArty_eams_c'] = $value['city'];
                $GLOBALS['IPArty_eams_s'] = $value['state'];
				
				$GLOBALS['IPArty_eams_add1upper'] = strtoupper($value['address1']);
                $GLOBALS['IPArty_eams_add2upper'] = strtoupper($value['address2']);
                $GLOBALS['IPArty_eams_cupper'] = strtoupper($value['city']);
                $GLOBALS['IPArty_eams_supper'] = strtoupper($value['state']);
				
                $GLOBALS['IPArty_eams_z'] = isset($value['zip']) ? $value['zip'] : '';
                $GLOBALS['IPArty_eams_p'] = isset($value['phone']) ? $value['phone'] : '';
                $GLOBALS['eamsref'] = $value['name'];
                $GLOBALS['ueamsref'] = strtoupper($value['name']);
                $GLOBALS['insAddress1'] = isset($value['address1']) ? $value['address1'] : '';
                $GLOBALS['insCity'] = isset($value['city']) ? $value['city'] : '';
                $GLOBALS['insState'] = isset($value['state']) ? $value['state'] : '';
                $GLOBALS['insZip'] = isset($value['zip']) ? $value['zip'] : '';
                $GLOBALS['ins_nm'] = $value['insname'];
                $GLOBALS['ins_nmupper'] = strtoupper($value['insname']);
		$GLOBALS['ifirm'] = $value['ifirm'];
                $GLOBALS['iaddress1'] = $value['iaddress1'];
                $GLOBALS['iaddress2'] = $value['iaddress2'];
                $GLOBALS['icity'] = $value['icity'];
                $GLOBALS['istate'] = $value['istate'];
                $GLOBALS['izip'] = $value['izip'];
            endforeach;
        }
        else if(isset($data['selectedtinsparty']) && count($data['selectedtinsparty']) == 0){
                $GLOBALS['insAddress1'] = '';
                $GLOBALS['insCity'] = '';
                $GLOBALS['insState'] = '';
                $GLOBALS['insZip'] = '';
        }
        $GLOBALS['insAddress1upper'] = isset($GLOBALS['insAddress1'])? strtoupper($GLOBALS['insAddress1']):'';
        $GLOBALS['insCityupper'] = isset($GLOBALS['insCity'])? strtoupper($GLOBALS['insCity']):'';
        $GLOBALS['insStateupper'] = isset($GLOBALS['insState'])? strtoupper($GLOBALS['insState']):'';
        //echo '<pre>'; print_r($data['selectedempparty']); exit;
        if(isset($data['selectedempparty']) && !empty($data['selectedempparty'])) {
            foreach($data['selectedempparty'] as  $key => $value) :
                $GLOBALS['EPArty_name'] = $value['firm'];
                $GLOBALS['EPArty_add1'] = $value['address1'];
                $GLOBALS['EPArty_add2'] = $value['address2'];
                $GLOBALS['EPArty_c'] = $value['city'];
                $GLOBALS['EPArty_s'] = $value['state'];
				
				$GLOBALS['uEPArty_name'] = strtoupper($value['firm']);
                $GLOBALS['uEPArty_add1'] = strtoupper($value['address1']);
                $GLOBALS['uEPArty_add2'] = strtoupper($value['address2']);
                $GLOBALS['uEPArty_c'] = strtoupper($value['city']);
                $GLOBALS['uEPArty_s'] = strtoupper($value['state']);
				
                $GLOBALS['EPArty_z'] = $value['zip'];
                $GLOBALS['EPArty_p'] = $value['phone1'];
            endforeach;
        }
        $GLOBALS['phy_name'] = '';
        $GLOBALS['phy_phone1'] = '';
        if(isset($data['selectedphy']) && !empty($data['selectedphy'])) {
            foreach($data['selectedphy'] as  $key => $value) :
                if(empty($value['suffix'])){
                    if(!empty($value['first']) || !empty($value['last']))
                        $GLOBALS['phy_name'] = $value['first'].' '.$value['last'];
                    else
                        $GLOBALS['phy_name'] = $value['firm'];
                }
                else{
                    $GLOBALS['phy_name'] = $value['first'].' '.$value['last'].', '.$value['suffix'];
                }
                if(!empty($value['phone1'])){
                    $GLOBALS['phy_phone1'] = $value['phone1'];
                }
            endforeach;
        }
        //echo '<pre>'; print_r($data['selectedphy']); exit;
        
        
        if(isset($data['selecteddefatt']) && !empty($data['selecteddefatt'])) {
            foreach($data['selecteddefatt'] as  $key => $value) :
                $GLOBALS['DefPArty_firm'] = (isset($value['name'])) ? $value['name'] : "";
				$GLOBALS['DefPArty_firmupper'] = (isset($value['name'])) ? strtoupper($value['name']) : "";
                $GLOBALS['DefPArty_firmno'] = (isset($value['eamsref'])) ? $value['eamsref'] : "";
                $GLOBALS['DefPArty_fname'] = (isset($value['first'])) ? $value['first'] : "";
                $GLOBALS['DefPArty_lname'] = (isset($value['last'])) ? $value['last'] : "";
                $GLOBALS['DefPArty_add1'] = (isset($value['address1'])) ? $value['address1'] : "";
                $GLOBALS['DefPArty_add2'] = (isset($value['address2'])) ? $value['address2'] : "";
                $GLOBALS['DefPArty_c'] = (isset($value['city'])) ? $value['city'] : "";
                $GLOBALS['DefPArty_s'] = (isset($value['state'])) ? $value['state'] : "";
				
				$GLOBALS['DefPArty_fnameupper'] = (isset($value['first'])) ? strtoupper($value['first']) : "";
                $GLOBALS['DefPArty_lnameupper'] = (isset($value['last'])) ? strtoupper($value['last']) : "";
                $GLOBALS['DefPArty_suffixupper'] = (isset($value['suffix'])) ? strtoupper($value['suffix']) : ""; 
                $GLOBALS['DefPArty_add1upper'] = (isset($value['address1'])) ? strtoupper($value['address1']) : "";
                $GLOBALS['DefPArty_add2upper'] = (isset($value['address2'])) ? strtoupper($value['address2']) : "";
                $GLOBALS['DefPArty_cupper'] = (isset($value['city'])) ? strtoupper($value['city']) : "";
                $GLOBALS['DefPArty_supper'] = (isset($value['state'])) ? strtoupper($value['state']) : "";
				
                $GLOBALS['DefPArty_z'] = (isset($value['zip'])) ? $value['zip'] : "";
                $GLOBALS['DefPArty_p'] = (isset($value['phone'])) ? $value['phone'] : "";
                $GLOBALS['dfirm'] = (isset($value['dfirm'])) ? $value['dfirm'] : "";
                $GLOBALS['daddress1'] = (isset($value['daddress1'])) ? $value['daddress1'] : "";
                $GLOBALS['daddress2'] = (isset($value['daddress2'])) ? $value['daddress2'] : "";
                $GLOBALS['dcity'] = (isset($value['dcity'])) ? $value['dcity'] : "";
                $GLOBALS['dstate'] = (isset($value['dstate'])) ? $value['dstate'] : "";
                $GLOBALS['dzip'] = (isset($value['dzip'])) ? $value['dzip'] : "";
                $GLOBALS['dphn_new'] = (isset($value['phn_new'])) ? $value['phn_new'] : "";
            endforeach;
        }
        //echo '<pre>'; print_r($data['selecteddefatt']); exit;
        $InjuryDetail = [];
        //echo '<pre>'; print_r($injuryArray); exit;
        /*if(!empty($data['injury_details'])) {
            foreach($data['injury_details'] as $key => $value) :
                if(strtolower($result['app_status']) != 'closed'){ 
                if(in_array($result['injury_id'],$addInj) || empty($addInj)){
                $wcabCaseNo = "";
                if(isset($value['case_no']) && $value['case_no'] != ''){
                    $wcabCaseNo = $value['case_no'];
                }
                if(isset($value['eamsno']) && $value['eamsno'] != ''){
                    $wcabCaseNo = 'ADJ'.$value['eamsno'];
                }
                
                $InjuryDetail['pob1'][$key] = (isset($value['pob1'])) ? $value['pob1'] : "";
                $InjuryDetail['pob2'][$key] = (isset($value['pob2'])) ? $value['pob2'] : "";
                $InjuryDetail['pob3'][$key] = (isset($value['pob3'])) ? $value['pob3'] : "";
                $InjuryDetail['pob4'][$key] = (isset($value['pob4'])) ? $value['pob4'] : "";
                $InjuryDetail['pob5'][$key] = (isset($value['pob5'])) ? $value['pob5'] : "";

                $InjuryDetail['wceams'][$key] = $wcabCaseNo;
                $InjuryDetail['DOI1'][$key] = $value['doi'];
                $InjuryDetail['DOI2'][$key] = $value['doi2'];
                }
                }
            endforeach;
        }*/

        /*$GLOBALS['iwc1'] = (isset($InjuryDetail['wceams'][0])) ? $InjuryDetail['wceams'][0] : "";
        $GLOBALS['iwc2'] = (isset($InjuryDetail['wceams'][1])) ? $InjuryDetail['wceams'][1] : "";
        $GLOBALS['iwc3'] = (isset($InjuryDetail['wceams'][2])) ? $InjuryDetail['wceams'][2] : "";
        $GLOBALS['iwc4'] = (isset($InjuryDetail['wceams'][3])) ? $InjuryDetail['wceams'][3] : "";
        $GLOBALS['iwc5'] = (isset($InjuryDetail['wceams'][4])) ? $InjuryDetail['wceams'][4] : "";
        $GLOBALS['iwc6'] = (isset($InjuryDetail['wceams'][5])) ? $InjuryDetail['wceams'][5] : "";
        $GLOBALS['iwc7'] = (isset($InjuryDetail['wceams'][6])) ? $InjuryDetail['wceams'][6] : "";
        $GLOBALS['iwc8'] = (isset($InjuryDetail['wceams'][7])) ? $InjuryDetail['wceams'][7] : "";
        $GLOBALS['iwc9'] = (isset($InjuryDetail['wceams'][8])) ? $InjuryDetail['wceams'][8] : "";
        $GLOBALS['iwc10'] = (isset($InjuryDetail['wceams'][9])) ? $InjuryDetail['wceams'][9] : "";
        $GLOBALS['iwc11'] = (isset($InjuryDetail['wceams'][10])) ? $InjuryDetail['wceams'][10] : "";
        $GLOBALS['iwc12'] = (isset($InjuryDetail['wceams'][11])) ? $InjuryDetail['wceams'][11] : "";
        $GLOBALS['iwc13'] = (isset($InjuryDetail['wceams'][12])) ? $InjuryDetail['wceams'][12] : "";
        $GLOBALS['iwc14'] = (isset($InjuryDetail['wceams'][13])) ? $InjuryDetail['wceams'][13] : "";
        $GLOBALS['iwc15'] = (isset($InjuryDetail['wceams'][14])) ? $InjuryDetail['wceams'][14] : "";
        $GLOBALS['iwc16'] = (isset($InjuryDetail['wceams'][15])) ? $InjuryDetail['wceams'][15] : "";
*/
        
       /* for($a = 0; $a<=16; $a++) {
            $n = $a+1;
            $GLOBALS['i'. $n .'pob1'] = (isset($InjuryDetail['pob1'][$a])) ? $InjuryDetail['pob1'][$a] : "";
            $GLOBALS['i' . $n . 'pob2'] = (isset($InjuryDetail['pob2'][$a])) ? $InjuryDetail['pob2'][$a] : "";
            $GLOBALS['i' . $n . 'pob3'] = (isset($InjuryDetail['pob3'][$a])) ? $InjuryDetail['pob3'][$a] : "";
            $GLOBALS['i' . $n . 'pob4'] = (isset($InjuryDetail['pob4'][$a])) ? $InjuryDetail['pob4'][$a] : "";

        }*/
        for($a = 0; $a<=16; $a++) {
            $n = $a+1;
            $GLOBALS['i'. $n .'DOI1'] = (isset($InjuryDetail['DOI1'][$a])) ? date('m/d/Y', strtotime($InjuryDetail['DOI1'][$a])) : "";
            $GLOBALS['i'. $n .'DOI2'] = (isset($InjuryDetail['DOI2'][$a])) ? date('m/d/Y', strtotime($InjuryDetail['DOI2'][$a])) : "";
        }
        
//        echo "<pre>"; print_r($data); exit;
        //echo '<pre>'; print_r($GLOBALS); exit;
         //*Set variable for form ends [Namrata]*//

        //echo '<pre>'; print_r($GLOBALS); exit;
        //$GLOBALS['sanchit'] = 'N';
        //      echo '<pre>'; print_r($GLOBALS); exit;

        $TBS = new clsTinyButStrong; // new instance of TBS
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load the OpenTBS plugin
        $template = $data['docPath'];

        $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).


        $extArray = explode(".", $template);
        $ext = $extArray[count($extArray) - 1];

//        $TBS->MergeBlock('blk',$data);
//         echo '<pre>'; print_r($data); exit;
        if(!is_dir(DOCUMENTPATH.'clients/'.$data['caseno'])){
            $oldmask = umask(0);
            mkdir(DOCUMENTPATH.'clients/'.$data['caseno'],0777);
            umask($oldmask);
        }

        //$result = $this->addCaseAct($data);

        //if($result != ''){
            $actno = $this->ci->common_functions->last_caseact_record(array('caseno'=>$data['caseno']));
            $nwactno = $actno + 1;
            $output_file_name = DOCUMENTPATH.'clients/'.$data['caseno']."/".'F'.$nwactno.".".$ext;
            //echo $output_file_name; exit;
            $TBS->Show(OPENTBS_FILE, $output_file_name); // Also merges all [onshow] automatic fields.

            if (!empty($output_file_name)) {
               //exec('start "" /max ' . $output_file_name);
                $fileNM = 'F'.$nwactno.".".$ext;
                $data['filename'] = $fileNM;
                //if($data['filenumber'] != 407 && $data['filenumber'] != 469)
                $result = $this->addCaseAct($data);
                //echo '<pre>'; print_r($data); exit;
                return $fileNM;


               // echo "true";
            } else {
                echo "false";
            }
        //}else{
           // echo "false";
        //}
        exit;
    }

    public function addCaseAct($data){
         $category = json_decode(category,true);
         $category=array_search("Legal Document",$category);
         //echo 'in add case act'; print_r($data);exit;

        $record = array('caseno'=>$data['caseno'],'initials0'=>$data['staff'],'initials'=>$data['staff'],'date'=>date('Y-m-d H:i:s'),'category'=>$category,'mainnotes'=>0);
        $record['filename'] = $data['filename'];
        if($data['filenumber'] == 400 || $data['filenumber'] == 402){
            $record['event'] = ($data['depo_date'] != '') ? 'Client Ltr RE: Depo ON: '.$data['depo_date'].' AT:'.$data['depo_time'] : 'Client Ltr RE:';
        }else if($data['filenumber'] == 401){
            $record['event'] = ($data['depo_date'] != '') ? 'Client Ltr RE: Depo Reschedule ON: '.$data['depo_date'].' AT:'.$data['depo_time'] : 'Client Ltr RE:';
        }else if($data['filenumber'] == 403){
            $record['event'] = 'Ltr to Defence Attorney RE : settlement demand';
        }
        else if($data['filenumber'] == 404){
            $record['event'] = 'Ltr to '.$data['loc_first_name'].' '.$data['loc_last_name'].', '.$data['loc_suffix'];
        }else if($data['filenumber'] == 405) {
            $record['event'] = 'Ltr to Board RE:';
        }else if($data['filenumber'] == 406){
            $record['event'] = 'Ltr to Judge '.$data['judgeNM'];
        }else if($data['filenumber'] == 407){
            $record['event'] = 'Ltr to Insurance RE:';
        }else if($data['filenumber'] == 408){
            $record['event'] = ($data['depo_date'] != '') ? 'Client Ltr Re: Depo - Second Part ON: '.$data['depo_date'].' AT:'.$data['depo_time'] : 'Client Ltr Re: Depo - Second Part';
        }else if($data['filenumber'] == 409){
            $record['event'] = 'Dr. Ltr Re: Depo of DR ' . $data['loc_first_name'] . ' ' . $data['loc_last_name'] . ' ON: '.$data['depo_date'].' AT:'.$data['depo_time'].' - WITNESS FEES INCLUDING IN THE AMOUNT OF $'.$data['fee_amt'];
        }else if($data['filenumber'] == 410){
            $record['event'] = 'Proof of Service '.$data['pl_title'];
        }else if($data['filenumber'] == 411){
            $record['event'] = 'Pleading w/Proof of Service '.$data['my_pl_title'];
        }else if($data['filenumber'] == 412){
            $record['event'] = ($data['date_Tri'] != '') ? 'Client Ltr RE: Conference to Trial ON: '.$data['date_Tri'].' AT:'.$data['time_Tri'] : 'Client Ltr RE: Conference to Trial ON: ';
        }else if($data['filenumber'] == 414){
            $record['event'] = 'Doctor Depo Ltr to '.$data['last_Party_full'].' RE: SCHEDULED DEPO OF '.$data['loc_first_name'].' '.$data['loc_last_name'].', '.$data['loc_suffix'].' ON: '.$data['depo_date'].' AT: '.$data['depo_time'].' -WITNESS FEE PAID TO DOCTOR  IN THE AMOUNT OF $'.$data['fee_amt'] ;
        }else if($data['filenumber'] == 415){
            $record['event'] = 'Doctor Depo Fee Reimbursement Ltr to '.$data['last_Party_full'].' ON: '.$data['depo_date'].' IN THE AMOUNT OF $'.$data['fee_amt'].' PAID TO AME: '.$data['pl_title'];
        }else if($data['filenumber'] == 419){
            $record['event'] = 'Defense $4600 Ltr to '.$data['last_Party_full'].' RE APPLICANT&#39;S CHOICE OF PHYSICIAN IS '.$data['loc_first_name'].' '.$data['loc_last_name'].', '.$data['loc_suffix'];
        }else if($data['filenumber'] == 425){
            $record['event'] = 'Join AME Ltr to DR.'.$data['loc_last_name'].' SPECIALITY: '.$data['dr_speciality'].' PLEASE EXAMINE THE FOLLOWING BODY PARTS:'.$data['body_partexamined'].'';
        } else if($data['filenumber'] == 430){
            $record['event'] = 'Client Ltr RE: Truth AME Dr. '.$data['pl_title'];
        }else if($data['filenumber'] == 434){
            $record['event'] = 'Disclosure - Attorney Fee & Acknowledgement Form';
        }else if($data['filenumber'] == 436){
            $record['event'] = 'Client Ltr RE: Medical Appointment w/ '.$data['loc_first_name'].' '.$data['loc_last_name'].', '.$data['loc_suffix'].' SPECIALITY: '.$data['dr_splt'].' -EXAM TYPE:'.$data['e_type'].' -ON:'.$data['e_day'].' , '.$data['e_date'].' AT'.$data['e_time'];
        }else if($data['filenumber'] == 437){
            $record['event'] = 'Client Ltr RE: Medical AME Appointment w/ '.$data['loc_first_name'].' '.$data['loc_last_name'].', '.$data['loc_suffix'].' SPECIALITY: '.$data['dr_splt'].' -ON:'.$data['e_day'].' , '.$data['e_date'].' AT'.$data['e_time'];
        }else if($data['filenumber'] == 438){
            $record['event'] = 'Client Ltr RE: Attempt - Unable to Reach Potential Client';
        } else if($data['filenumber'] == 439){
            $record['event'] = 'Authorization Form Medical';
        }else if($data['filenumber'] == 441){
            $record['event'] = 'Defense Ltr RE: Mileage to Applicant For '.$data['total_mileage'].' MILES @ .34 FOR A TOTAL OF $'.$data['total_mileage_amount'].' ';
        }else if($data['filenumber'] == 442){
            $record['event'] = 'Defense Ltr RE LC5710 Fee Reimbursement';
        } else if($data['filenumber'] == 460){
            $record['event'] = 'Orders Allowing Benefits Pursuant to LC 5710';
        }else if($data['filenumber'] == 461){
            $record['event'] = 'Order Allowing Benefits (No Mileage) Pursuant to LC 5710';
        }else if($data['filenumber'] == 463){
            $record['event'] = 'Petition for Order Allowing Attorney\'s Fees LC§ 5710 w/ POS- FOR DEPOSITION ON '.$data['depo_date'].'- ATTENDING ATTORNEY:'.$data['judgeNM'].' - TRAVEL TIME: '.$data['travel_time'].' - DEPO PREP: '.$data['depo_prep_time'].' - DEPO: '.$data['depo_time'].' - TOTAL TIME: '.$data['total_time'].' - AMOUNT REQUESTED: $ '.$data['total_request'].' ' ;
        } else if($data['filenumber'] == 469){
            $record['event'] = 'Dun Ltr to '.$data['last_Party_full'];
        }else if($data['filenumber'] == 470){
            $record['event'] = 'NOTICE OF '.$data['pl_title'].' W/JUDGE '.$data['judgeNM'].' ON '.$data['depo_date'].' AT: '.$data['depo_time'];
        }else if($data['filenumber'] == 471){
            $record['event'] = 'NOTICE OF CONTINUED '.$data['depo_time'].' W/JUDGE '.$data['judgeNM'].' FROM: '.$data['depo_date'].' TO: '.$data['e_date'].' AT:'.$data['depo_time'];
        }else if($data['filenumber'] == 472){
            $record['event'] = 'NOTICE OF DEPOSITION OF '.$data['pl_title'].' ON: '.$data['depo_date'].' AT:'.$data['depo_time'];
        }else if($data['filenumber'] == 473){
            $record['event'] = 'NOTICE OF DEPOSITION OF '.$data['pl_title'].' ON: '.$data['depo_date'].' AT:'.$data['depo_time'];
        }else if($data['filenumber'] == 474){
            $record['event'] = 'Defense Ltr RE: Demand for Docs/Notice of Claim Enclosed/'.$data['loc_name'];
        }else if($data['filenumber'] == 475){
            $record['event'] = 'Proof of Service-New Claims';
        }else if($data['filenumber'] == 486){
            $record['event'] = 'Insurance Letter RE:Claim Form';
        }else if($data['filenumber'] == 488){
            $record['event'] = 'Client Ltr RE: Conf ltr to clt ON '.$data['depo_date'].' AT:'.$data['depo_time'];
        }else if($data['filenumber'] == 491){
            $record['event'] = 'Petition for Reimbursement to Physician Witness Fee w/POS DEPO DATED '.$data['depo_date'].' - ATTENDING ATTORNEY: '.$data['judgeNM'].' -FEE ADVANCE: '.$data['fee_amt'].' -LOCATION: '.$data['pl_title'].' -DEPO TIME: '.$data['depo_time'].' -TOTAL TIME: '.$data['depo_time'].' - FEE REQUESTED: '.$data['e_day'];
        }else if($data['filenumber'] == 492){
            $record['event'] = 'Order Allowing for Reimbursement for Payment to Physician for deposition as Witness Fee '.$data['fee_amt'].' FOR PETITION FILED ON '.$data['depo_date'];
        }else if($data['filenumber'] == 500){
            $record['event'] = 'Intake Sheet';
        }else if($data['filenumber'] == 502){
            $record['event'] = 'FAX Cover to defense - @ COMMENTS: '.$data['e_type'];
        }else if($data['filenumber'] == 504){
            $record['event'] = 'Medical Mileage Reimbursement Sheet';
        }else if($data['filenumber'] == 505){
            $record['event'] = 'Declaration Pursuant to LC Section 4906(h)';
        }else if($data['filenumber'] == 506){
            $record['event'] = 'Proof of Service EAMS '.$data['pl_title'];;
        }else if($data['filenumber'] == 541){
            $record['event'] = 'Client Ltr RE: Opening Letter - Thank You';
        }else if($data['filenumber'] == 542){
            $record['event'] = 'Client Ltr RE: Opening Letter w/Enclosed Forms';
        }else if($data['filenumber'] == 10857){
            $record['event'] = 'WC EAMS Compromise And Release (Dependancy Claim) (form 10214(d)) DWC-CA';
        }else if($data['filenumber'] == 10039){
            $record['event'] = 'OCF Dismissing Party Defendant';
        }else if($data['filenumber'] == 10080){
            $record['event'] = 'OCF Declaration Of Custodian Of Records';
        }else if($data['filenumber'] == 10025){
            $record['event'] = 'OCF Application  Death';
        }
        else if($data['filenumber'] == 10030){
            $record['event'] = 'OCF Change of Address WC001';
        }
        else if($data['filenumber'] == 10031){
            $record['event'] = 'OCF Claim Form WC1S';
        }
        else if($data['filenumber'] == 10032){
            $record['event'] = 'OCF Commutation Petition WC49';
        }
        else if($data['filenumber'] == 10048){
            $record['event'] = 'OCF Guardian Ad Litem  WC8';
        }
        else if($data['filenumber'] == 10051){
            $record['event'] = 'OCF Joining Party Defendant';
        }
         else if($data['filenumber'] == 10091){
            $record['event'] = 'OCF Notice of Dismissal of Attorney';
        }
         else if($data['filenumber'] == 10581){
            $record['event'] = 'OCF Ortega Counseling Center';
        }
         else if($data['filenumber'] == 10210){
            $record['event'] = 'OCF Petition for Penalties WC-100';
        }
         else if($data['filenumber'] == 10540){
            $record['event'] = 'OCF Petition for Reconsideration (Form 45)';
        }
         else if($data['filenumber'] == 10099){
            $record['event'] = 'Petition to Reopen';
        }
         else if($data['filenumber'] == 10089){
            $record['event'] = 'OCF Petition to Reopen for New and Further Disability';
        }
         else if($data['filenumber'] == 10058){
            $record['event'] = 'WC EAMS Pre-Trial Conference Statement (Rev 9/2010)';
        }
         else if($data['filenumber'] == 10081){
            $record['event'] = 'OCF Proof of Service';
        }
        else if($data['filenumber'] == 10503){
            $record['event'] = 'OCF Proof of Service with Verification (Rev 7/84)';
        }
        else if($data['filenumber'] == 10074){
            $record['event'] = 'OCF Stipulations for Award  WC3';
        }
        else if($data['filenumber'] == 10076){
            $record['event'] = 'OCF Substitution of Attorney  WC36';
        }
        else if($data['filenumber'] == 10078){
            $record['event'] = 'OCF Verification';
        }
        else if($data['filenumber'] == 10886){
            $record['event'] = 'WC EAMS  PACKET Application for Adjudication';
        }
        else if($data['filenumber'] == 10885){
            $record['event'] = 'WC EAMS APPSIF - Application for Subsequent Injuries Fund benefits UEF';
        }
        else if($data['filenumber'] == 10856){
            $record['event'] = ' WC EAMS Compromise and Release  (form 10214(c)) DWC-CA';
        }
        else if($data['filenumber'] == 10859){
            $record['event'] = 'WC EAMS Declaration of Readiness - DOR  (form 10250.1) DWC-CA';
        }
        else if($data['filenumber'] == 10889){
            $record['event'] = ' WC EAMS Declaration of Readiness Expedited Trial  (form 10208.3, 4/2014) DWC-CA';
        }
        else if($data['filenumber'] == 10861){
            $record['event'] = 'WC EAMS Document Cover Sheet (form 10232.1) DWC-CA';
        }
        else if($data['filenumber'] == 10862){
            $record['event'] = 'WC EAMS DWC-CA Document Separator Sheet  (form 10232.2)';
        }
        else if($data['filenumber'] == 10863){
            $record['event'] = 'WC EAMS Minutes of Hearing  (form 10245) DWC-CA';
        }
         else if($data['filenumber'] == 10891){
            $record['event'] = 'WC EAMS Notice and Request for Allowance of Lien  (form 6) WCAB 2017';
        }
        else if($data['filenumber'] == 10871){
            $record['event'] = 'WC EAMS Request for Consultative Rating (RCR)  (form 104 DEU) DWC-AD';
        }
        else if($data['filenumber'] == 10873){
            $record['event'] = 'WC EAMS Minutes of Hearing  (form 10245) DWC-CA';
        }
        else if($data['filenumber'] == 10874){
            $record['event'] = 'WC EAMS Request for Summary Rating Determination - Primary Treating Physician  (form 102 DEU) DWC-AD';
        }
        else if($data['filenumber'] == 10890){
            $record['event'] = 'WC EAMS Stipulations with Request for Awards Injuries Post 1/1/2013  (form 10214(a), 4/2014)';
        }
        else if($data['filenumber'] == 10864){
            $record['event'] = 'WC EAMS Stipulations with Request for Awards Pre 1/1/2013 (form 10214(a)) DWC-CA';
        }
        else if($data['filenumber'] == 10882){
            $record['event'] = 'WC EAMS Supplemental job displacement nontransferable training voucher (DWC-AD form 10133.57) RRTW';
        }
         else if($data['filenumber'] == 11220){ //
            $record['event'] = 'QME form 109 - QME Notice of Unavailability';
        }
         else if($data['filenumber'] == 11216){
            $record['event'] = 'QME form 31.5 - Replacement Panel Request-8 Cal. Code of Regulations section 31.5';
        }
         else if($data['filenumber'] == 11217){
            $record['event'] = 'QME form 31.7 - Additional Panel Request-8 Cal. Code of Regulations section 31.7';
        }
        else if($data['filenumber'] == 10422){ //
            $record['event'] = 'OCF DocCentral Form';
        }
         else if($data['filenumber'] == 10406){
            $record['event'] = 'OCF Matrix Order Form';
        }
         else if($data['filenumber'] == 10006){
            $record['event'] = 'OCF Med-Legal Records Order Form';
        }
         else if($data['filenumber'] == 10214){
            $record['event'] = 'OCF Order Of Dismissal';
        }
         else if($data['filenumber'] == 11224){
            $record['event'] = 'OCF IMR Application for Review';
        }
         else if($data['filenumber'] == 10075){
            $record['event'] = 'OCF Stipulations For Award Death WC4A';
        }
         else if($data['filenumber'] == 11209){
            $record['event'] = "DWC - AD 10133.36 Physician's Return To Work and Voucher Report";
        }
         else if($data['filenumber'] == 10865){
            $record['event'] = "WC EAMS Stipulations with Request for Awards (Death Case)  (form 10214(b) ) DWC-CA";
        }
         else if($data['filenumber'] == 10501){
            $record['event'] = "OCF Order - General Order";
        }
         else if($data['filenumber'] == 10876){
            $record['event'] = "WC EAMS Notice of Offer of Regular Work  (form 10118 SJDB) RRTW";
        }
         else if($data['filenumber'] == 10875){
            $record['event'] = "WC EAMS Notice of Offer of Modified or Alternative Work  (DWC-AD form 10133.53 SJDB) RRTW";
        }
        else if($data['filenumber'] == 10565){
            $record['event'] = "OCF Exhibit List";
        }
        else if($data['filenumber'] == 'ocers'){
            $record['event'] = "Letter OCERS To ". $data['clientName'];
        }
        else if($data['filenumber'] == 'sbcera'){
            $record['event'] = "Letter SBCERA To  ". $data['clientName'];
        }
        else if($data['filenumber'] == 'sbcers'){
            $record['event'] = "Letter SBCERS To  ".$data['clientName'];
        }
        else if($data['filenumber'] == 'vcera'){
            $record['event'] = "Letter VCERA COVER To  ".$data['clientName'];
        }
        else if($data['filenumber'] == 'lafpp'){
            $record['event'] = "Letter LAFPP To  ".$data['clientName'];
        }
        else if($data['filenumber'] == 'lacera_old'){
            $record['event'] = "Ltr to LACERA/Board of Ret. RE: Enclosed for Filing";
        }
        else if($data['filenumber'] == 'lacera_new'){
            $record['event'] = "Ltr to LACERA/Board of Ret. RE: Enclosed for Filing";
        }
        else if($data['filenumber'] == 'calpers_942716'){
            $record['event'] = "Letter CALPERS COVER 942716 To  ". $data['clientName'];
        }
        else if($data['filenumber'] == 'calpers_2796'){
            $record['event'] = "Letter CALPERS COVER 2796 To  ". $data['clientName'];
        }
        else if($data['filenumber'] == 'Lacera_amendment'){
            $record['event'] = "Ltr to LACERA/Board of Ret. RE: Enclosed for Filing";
        }
        else if($data['filenumber'] == 'Lacera_client'){
            $record['event'] = "Client Ltr RE:";
        }
        else if($data['filenumber'] == 'calpers_rep'){
            $record['event'] = "Letter CALPERS LETTER OF REP To  ". $data['clientName'];
        }
        else if($data['filenumber'] == 'calpers_942711'){
            $record['event'] = "Letter CALPERS COVER 942711 To  ". $data['clientName'];
        }
		else if($data['filenumber'] == 'calsper_retainer'){
            $record['event'] = "Letter To Retainer CALPERS ";
        }
		else if($data['filenumber'] == 'retainer_LA'){
            $record['event'] = "Letter To Retainer City of LA ";
        }
		else if($data['filenumber'] == 'retainer_kcera'){
            $record['event'] = "Letter To Retainer KCERA ";
        }
		else if($data['filenumber'] == 'retainer_lacera'){
            $record['event'] = "Letter To Retainer LACERA ";
        }
		else if($data['filenumber'] == 'retainer_sbcers'){
            $record['event'] = "Letter To Retainer SBCERS ";
        }
		else if($data['filenumber'] == 'retainer_sbcera'){
            $record['event'] = "Letter To Retainer SBCERA ";
        }
		else if($data['filenumber'] == 'retainer_vcera'){
            //$record['event'] = "Letter To Retainer ".$data['clientboardOnly'];
                $record['event'] = "Letter To Retainer VCERA";
        }
		else if($data['filenumber'] == 'city_waiver'){
            $record['event'] = "Letter To CITY WAIVER ";
        }
        else if($data['filenumber'] == 'SBCERS_forms'){
            $record['event'] = "Letter To SBCERS forms only ";
        }
        else if($data['filenumber'] == 'FORMS_SCDR'){
            $record['event'] = "Letter To FORMS SCDR Packet ";
        }
        else if($data['filenumber'] == '1ATIER36app'){
            $record['event'] = "Letter To 1 A TIER 3-6 app ";
        }
        else if($data['filenumber'] == 'power_attorney'){
            $record['event'] = "Letter To Power-attorney-form ";
        }
        else if($data['filenumber'] == 'Vcera_application'){
            $record['event'] = "Letter To Vcera Application for Disability Retirement";
        }
        else if($data['filenumber'] == 'LACERA_Application_ps'){
            $record['event'] = "Letter To LACERA Application (with PS) 8.22.18 (2)";
        }
        else if($data['filenumber'] == 'FORMS_Physician'){
            $record['event'] = "Letter To FORMS Physician's Report Packet";
        }
        else if($data['filenumber'] == 'rep_letter'){
            $record['event'] = "Letter To Rep";
        }
        else if($data['filenumber'] == 'retainer_ocera'){
            $record['event'] = "Letter To Retainer OCERS ";
        }
        else if($data['filenumber'] == '4DisabilityApplication_rev04-23-2014-DisabilityApplicaton_rev09-2010andProcedures'){
            $record['event'] = "Letter To Disability Application rev 04-23-2014 - Disability Application rev 09-2010 and Procedures ";
        }
        else if($data['filenumber'] == 'Disability_Application_packet'){
            $record['event'] = "Letter To Disability Application Packet";
        }
        else if($data['filenumber'] == '2DisabilityApplicationHandbook(Dec2016)_interactive1'){
            $record['event'] = "Letter To Disability Application Handbook (Dec2016)_interactive1";
        }
        else if($data['filenumber'] == '1CalPERSApp2018'){
            $record['event'] = "Letter To 1 CalPERS App 2018";
        }
        
        

        //echo "<pre>";
       // print_r($record);exit;

        return $this->ci->common_functions->insertCaseActRecord($record);
    }
}
