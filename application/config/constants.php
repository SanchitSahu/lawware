<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

define('DOCUMENT_AUTOSAVE_ENABLED', 'true');

/**
 * Custom Constants Defined
 */
define('DEFAULT_LISTING_LENGTH', 20);
define('XMPP_HOST', 'iodlawware.com');
define('XMPP_BOSH_URL', 'https://iodlawware.com:7443/http-bind/');
define('XMPP_RESOURCE', '');
define('XMPP_USE_SSL', true);
define('XMPP_USE_DEBUG', true);
define('DOCUMENTPATH', APPLICATION_PATH . 'assets/');
define('INJDOCUMENTPATH', APPLICATION_PATH . 'assets/');
define('SAIP', 'http://192.168.10.52');
define('SNSWEB', 'http://18.219.93.117/snsweb/');
define('SAA', 'off');
define('APPLICATION_NAME', 'CompCase');

define('FTP_HOST', 'ftp.compcaseonline.com');
define('FTP_USERNAME', 'CompCaseUpload');
define('FTP_PASSWORD', 'PassIsComp9000');
/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
define('AddNewTaskCase', 'This is the last task of the case. Do you want to add a new task?');

//define('DocPath','http://solulab.ddns.net/lawware/assets/doc_forms/');
//colorname,forgroundcolor,backgroundcolor
define("color_codes", json_encode(
                array(
                    '1' => array('Black', '#000000', '#FFFFFF'),
                    '2' => array('Yellow Highlight', '#000000', '#FFFBCC;'),
                    '3' => array('Black Highlight', '#FFFFFF', '#000000'),
                    '4' => array('Red', '#FF0000', '#FFFFFF'),
                    '5' => array('Red Highlight', '#FFFFFF', '#FF0000'),
                    '6' => array('Green', '#008000', '#FFFFFF'),
                    '7' => array('Green Highlight', '#000000', '#00FF00'),
                    '8' => array('Blue', '#0000FF', '#FFFFFF'),
                    '9' => array('Blue Highlight', '#FFFFFF', '#0000FF'),
                    '10' => array('Pink Highlight', '#000000', '#ff00ff'),
                    '11' => array('Orange Highlight', '#000000', "#FFA500"),
                    '12' => array('Black on Aqua', '#000000', '#00FFFF'),
                    '13' => array('White on Black', '#FFFFFF', '#404040'),
                    '14' => array('Orient on Blue', '#E6E6DC', '#00628B'),
                    '15' => array('Purple Highlight', '#FFFFFF', '#551a8b')
                )
        )
);

define("lawyersRight", json_encode(
    array(
        '1' => array('ISAY KRUPNIK'),
        '2' => array('LAURA ASHLEY HALL'),
        '3' => array('NAREK AGABALYAN'),
        '4' => array('MICHAEL FARR'),
        '5' => array('JOSHUA ROBERTS'),
    )
)
);
define("lawyersLeft", json_encode(
                array(
                    '1' => array('AARON STRAUSSNER'),
                    '2' => array('JULIE LOCKS SHERMAN'),
                    '3' => array('JEAN-PAUL LONNE'),
                    '4' => array('MICHAEL TREGER'),
                    '5' => array('BENJAMIN HELQUIST'),
                )
        )
);

define("security", json_encode(
                array(
                    "1" => "Full Access",
                    "2" => "Read Only",
                    "3" => "Private"
                )
        )
);

define("category", json_encode(
                array(
                    "1" => "Main Case Activity",
                    "25" => "Intake",
                    "26" => "Email",
                    "5" => "Benefit Tracking",
                    "6" => "Legal Document",
                    "7" => "Correspondance",
                    "8" => "Medical",
                    "9" => "Utilization Review",
                    "10" => "Record Tracking",
                    "11" => "Filed And Served",
                    "12" => "Personnel File",
                    "13" => "Depositions",
                    "14" => "Settlement Fees/Orders",
                    "15" => "Liens",
                    "16" => "Calendar",
                    "17" => "Subpoenaed Record",
                    "18" => "Home Care",
                    "2" => "Fax / Correspondence",
                    "3" => "Ame/ Qme Reports",
                    "4" => "Medical Reports",
                    "19" => "Miscellaneous",
                    "20" => "Applicant's Medical",
                    "21" => "Defendant's Medical",
                    "22" => "Rehabilitation",
                    "23" => "Pension",
                    "24" => "Prior Injuries"
                )
        )
);


define("type_of_activity", json_encode(
                array(
                    "1" => "Normal",
                    "2" => "Task Completed"
                )
        )
);

define("activity_title", json_encode(
                array(
                    "1" => "",
                    "2" => "Attorney",
                    "3" => "Paralegal",
                    "4" => "Legal Assistant",
                    "5" => "Secretary"
                )
        )
);

/**
 * FIELDS NAME : WORDSTYLE
 */
define("activity_doc_type", json_encode(
                array(
                    "0" => "None",
                    "1" => "Law",
                    "2" => "Microsoft Word",
                    "3" => "Word Perfect",
                    "4" => "Court Form",
                    "5" => "Other",
                    "6" => "Adobe Acrobat",
                    "7" => "Excel",
                    "8" => "Default",
                    "9" => "Law Library",
                    "10" => "Acrobat law Compression",
                    "11" => "PDF Form",
                    "12" => "User Filename"
                )
        )
);

/**
 * FIELD NAME : OLE3STYLE
 */
define("activity_style", json_encode(
                array(
                    "1" => "",
                    "2" => "Image",
                    "3" => "Picture"
                )
        )
);

/**
 * FIELD NAME : BISTYLE
 */
define("activity_entry_type", json_encode(
                array(
                    '1' => array('Payment', 'payment'),
                    "2" => array('Fee', 'fee'),
                    "3" => array('Cost', 'cost'),
                    "4" => array('Flat Fee', 'flat-fee'),
                    "5" => array('Late Fee', 'late-fee'),
                    "6" => array('Adjustment/Credit Account', 'credit-account'),
                    "7" => array('Adjustment/Debit Account', 'debit-account'),
                    "8" => array('None', 'none')
                )
        )
);


define("file_attachment_path", "F://SSHYLaw/CLIENTS/");
//define("file_doc_path",'/var/www/html/lawware/clients/');
define("file_doc_path", 'E://xampp/htdocs/lawware/clients/');
//define("file_attachment_path","\\192.168.1.250\f\SSHYLaw\CLIENTS\\");

/*
 * Print Type
 */
define("print_type", json_encode(
                array(
                    '1' => 'All Tasks',
                    '2' => 'All Tasks Large Font',
                    '3' => 'Highlighted Task',
                    '4' => 'Page Break Between cases',
                    '5' => 'Detailed'
                )
        )
);
/*
 * Injury Dropdown Objects
 */
define("injuryStatus", json_encode(
                array(
                    '1' => 'Admitted',
                    '2' => 'Denied',
                    '3' => 'Presumed Compensible',
                    '4' => 'Unknown',
                    '5' => 'Delayed'
                )
        )
);

define('amendedApplication', json_encode(
                array(
                    '1' => 'First Amended',
                    '2' => 'Second Amended',
                    '3' => 'Third Amended',
                    '4' => 'Fourth Amended',
                    '5' => 'Fifth Amended',
                    '6' => 'Sixth Amended',
                    '7' => 'Seventh Amended',
                    '8' => 'Eighth Amended',
                    '9' => 'Ninth Amended',
                )
        )
);

define('bodyPartsshort', json_encode(
                array(
                    '100' => 'HEAD',
                    '110' => 'BRAIN',
                    '120' => 'EAR',
                    '121' => 'EAR',
                    '124' => 'EAR',
                    '130' => 'EYE',
                    '140' => 'FACE',
                    '141' => 'JAW',
                    '144' => 'MOUTH',
                    '145' => 'TEETH',
                    '146' => 'NOSE',
                    '148' => 'FACE',
                    '149' => 'FACE',
                    '150' => 'SCALP',
                    '160' => 'SKULL',
                    '198' => 'HEAD',
                    '200' => 'NECK',
                    '300' => 'UPR EXT',
                    '310' => 'ARM',
                    '311' => 'ARM',
                    '313' => 'ARM',
                    '315' => 'ARM',
                    '318' => 'ARM',
                    '319' => 'ARM',
                    '320' => 'WRIST',
                    '330' => 'HAND',
                    '340' => 'FINGERS',
                    '398' => 'UPR EXT',
                    '400' => 'TRUNK',
                    '410' => 'ABDOMEN',
                    '411' => 'HARNIA',
                    '420' => 'BACK',
                    '430' => 'CHEST',
                    '440' => 'HIPS',
                    '450' => 'SHOULDER',
                    '498' => 'TRUNK',
                    '500' => 'LWR EXT',
                    '510' => 'LEGS',
                    '511' => 'THIGH',
                    '513' => 'KENEE',
                    '515' => 'LOWER LEG',
                    '518' => 'LEG',
                    '519' => 'LEG ',
                    '520' => 'ANKLE',
                    '530' => 'FOOT',
                    '540' => 'TOES',
                    '598' => 'LOWER EXT',
                    '700' => 'MULTIPLE',
                    '800' => 'BODY SYS',
                    '801' => 'CIRC SYS',
                    '802' => 'HEART ATTA',
                    '810' => 'DIGESTIVE',
                    '820' => 'EXCRETORY',
                    '830' => 'MUSC SKEL',
                    '840' => 'NERVOUS',
                    '841' => 'STRESS',
                    '842' => 'PSYCH',
                    '850' => 'RESP SYS',
                    '860' => 'SKIN',
                    '870' => 'REPRPO',
                    '880' => 'BODY SMS',
                    '999' => 'UNCLASS',
                )
        )
);

define('bodyParts', json_encode(
                array(
                    '100' => 'Head - not specified',
                    '110' => 'Brain',
                    '120' => 'Ear - not specified',
                    '121' => 'Ear - External',
                    '124' => 'Ear - Internal Including hair',
                    '130' => 'Eye - Including optic nerves and vision',
                    '140' => 'Face - not specified',
                    '141' => 'Jaw - Including chin and mandible',
                    '144' => 'Mouth - Including lips tongue throat and taste',
                    '145' => 'Teeth',
                    '146' => 'Nose - Including nasal passages sinus and smell',
                    '148' => 'Face - multiple parts any combination of above parts',
                    '149' => 'Face - Forehead cheeks eyelids',
                    '150' => 'Scalp',
                    '160' => 'Skull',
                    '198' => 'Head - Multiple injury any combination of above parts',
                    '200' => 'Neck',
                    '300' => 'Upper extremities - not specified',
                    '310' => 'Arm - above wrist not specified',
                    '311' => 'Arm - upper arm humerus',
                    '313' => 'Arm - elbow head of radius',
                    '315' => 'Arm - forearm radius and ulna',
                    '318' => 'Arm - multiple parts any combination of above parts',
                    '319' => 'Arm',
                    '320' => 'Wrist',
                    '330' => 'Hand - not wrist or fingers',
                    '340' => 'Fingers',
                    '398' => 'Upper extremities - multiple part any combination of above parts',
                    '400' => 'Trunk - not specified',
                    '410' => 'Abdomen - including internal organs and groin',
                    '411' => 'Hernia',
                    '420' => 'Back - including back muscles, spine amd spinal cord',
                    '430' => 'chest - including ribs breast bone and internal organs of the chest',
                    '440' => 'Hips - including pelvis pelvic organs tailbone coccyx and buttocks',
                    '450' => 'Shoulders - Scapula and clavicle',
                    '498' => 'Trunk - use for side; multiple parts any combination of above parts',
                    '500' => 'Lower extremities - not specified',
                    '510' => 'Legs - above ankles not specified',
                    '511' => 'Thig femur',
                    '513' => 'Knee Patell',
                    '515' => 'Lower leg tibia and fibula',
                    '518' => 'Leg - multiple parts any combination of above parts',
                    '519' => 'Leg - not specified',
                    '520' => 'Ankle malleolus',
                    '530' => 'Foot not ankle or toe',
                    '540' => 'Toes',
                    '598' => 'Lower extremities - multiple parts any combination of above parts',
                    '700' => 'Multiple parts more than five major parts use only in fifth position of listing of body parts',
                    '800' => 'Body System not specified',
                    '801' => 'circulatory system - heart - other than heart attack blood , arteries veins etc',
                    '802' => 'circulatory system - Heart attack',
                    '810' => 'Digestive system - stomach',
                    '820' => 'Excretory system - kidneys bladder intenstines etc',
                    '830' => 'Musculo-Skeletal System - bones joints tendons muscles etc',
                    '840' => 'Nervous System - not specified',
                    '841' => 'Nervous System - stress',
                    '842' => 'Nervous System - Psychiatric/psych',
                    '850' => 'Respiratory System - lungs trachea etc',
                    '860' => 'Skin dermatitis etc',
                    '870' => 'Reproductive Systems',
                    '880' => 'other body systems',
                    '999' => 'Unclassified - insufficient information to identify body parts',
                )
        )
);

define('side', json_encode(
                array(
                    'Plaintiff' => 'Plaintiff',
                    'Defendant' => 'Defendant',
                    'Neither' => 'Neither',
                    'Unknown' => 'Unknown',
                    'Applicant' => 'Applicant',
                    'Employer' => 'Employer',
                    'Co-Def' => 'Co-Def'
                )
        )
);

define('CATEGORY_ID', json_encode(
                array(
                    '1000001' => 'quickNote',
                    '1000002' => 'message1',
                    '1000003' => '',
                    '1000004' => '',
                    '1000005' => 'message2',
                    '1000006' => 'message3',
                    '1000007' => 'message4',
                    '1000008' => '',
                    '1000009' => '',
                    '1000010' => '',
                    '1000011' => '',
                    '1000012' => '',
                    '1000013' => '',
                    '1000014' => '',
                    '1000015' => '',
                    '1000016' => '',
                    '1000017' => '',
                    '1000018' => '',
                    '1000019' => '',
                    '1000020' => '',
                    '1000021' => '',
                )
        )
);

define("sub_type", json_encode(
                array(
                    "a" => "a",
                    "b" => "b"
                )
        )
);

define('report_name', json_encode(
                array(
                    array('name' => 'Case Count (Work In Progress)', 'field' => 'case_count', 'type' => 'modal'),
                    array('name' => 'Case Report', 'field' => 'CASE_REPORT', 'type' => 'modal'),
//                    array('name' => 'Referral Report', 'field' => 'referral_report', 'type' => 'modal'),
                    array('name' => 'Case Activity Report', 'field' => 'case_activity_report', 'type' => 'modal'),
                    array('name' => 'Case Injury Report', 'field' => 'case_injury_report', 'type' => 'modal'),
//                    array('name'=>'User Defined Field Report'),
                    array('name' => 'Task Report', 'field' => 'task_report', 'type' => 'modal'),
//                    array('name' => 'Markup Report', 'field' => 'markup_report', 'type' => 'modal'),
//                    array('name' => 'Markup Report with User Defined Field', 'field' => 'markup_report_udf', 'type' => 'modal'),
//                    array('name'=>'User Programs'),
//                    array('name'=>'UDF Case Count'),
//                    array('name'=>'File Tracker Report'),
//                    array('name'=>'Billing Report'),
//                    array('name'=>'Awards,Costs and Fee Reports'),
//                    array('name'=>'A1-Law Studio / Custom Reports'),
                )
        )
);



define('letters_form_content', json_encode(
                array(
					'0' => array('number' => '400', 'group' => '*CL W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Client Ltr RE: Depo', 'filename' => '400.docx', 'modalType' => 'formletters1'),
                    '1' => array('number' => '401', 'group' => '*CL W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Client Ltr RE: Depo Reschedule', 'filename' => '401.docx', 'modalType' => 'formletters1'),
                    '2' => array('number' => '402', 'group' => '*CL W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Client Blank Letter', 'filename' => '402.docx', 'modalType' => 'formletters2'),
                    '3' => array('number' => '403', 'group' => '*DEF W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Demand for Settlement', 'filename' => '403.docx', 'modalType' => 'formletters136'),
                    '4' => array('number' => '404', 'group' => '*GEN W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Blank letter to Anyone', 'filename' => '404.docx', 'modalType' => 'formletters19'),
                    '5' => array('number' => '405', 'group' => '*BRD W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Board Blank Letter', 'filename' => '405.docx', 'modalType' => 'formletters2'),
                    '6' => array('number' => '406', 'group' => '*BRD W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Board Blank Letter to Judge', 'filename' => '406.docx', 'modalType' => 'formletters4'),
                    '7' => array('number' => '407', 'group' => '*INS W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Blank Letter to Insurance', 'filename' => '407.docx', 'modalType' => 'formletters2'),
                    '8' => array('number' => '408', 'group' => '*CL W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Client Ltr Re: Depo - Second Part', 'filename' => '408.docx', 'modalType' => 'formletters1'),
					'9' => array('number' => '409', 'group' => '*DR W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Dr. Ltr Re: Depo - Doctor', 'filename' => '409.docx', 'modalType' => 'formletters5'),
                    '10' => array('number' => '410', 'group' => '*PL', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Proof of Service - Blank', 'filename' => '410.docx', 'modalType' => 'formletters9'),
                    '11' => array('number' => '411', 'group' => '*PLD W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Pleading w/Proof of Service - Blank', 'filename' => '411.docx', 'modalType' => 'formletters9_for411'),
					'12' => array('number' => '412', 'group' => '*CL W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Client Ltr RE: Conference to Trial', 'filename' => '412.docx', 'modalType' => 'formletters10'),
					'13' => array('number' => '414', 'group' => '*DEF W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Defense Ltr RE: Depo - Doctor', 'filename' => '414.docx', 'modalType' => 'formletters11'),
                    '14' => array('number' => '415', 'group' => '*DEF W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Defense Ltr RE: Dr. Depo Fee Reimbursement', 'filename' => '415.docx', 'modalType' => 'formletters11'),
                    '15' => array('number' => "419", 'group' => "*DEF W", 'category' => 'workers comp', 'prefix' => '', "title" => "Defense Ltr RE: $4600 - Applicant's Choice of Physician", "filename" => '419.docx', 'category' => 'workers comp', 'modalType' => 'formletters11'),
                    '16' => array('number' => "430", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Client Ltr RE: AME Truth", "filename" => '430.docx', 'modalType' => 'formletters15'),
                    '17' => array('number' => "434", 'group' => "*FRM W", 'category' => 'workers comp', 'prefix' => '', "title" => "Disclosure - Attorney Fee & Acknowledgement Form", "filename" => '434.docx', 'modalType' => 'formletters16'),
                    '18' => array('number' => "436", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Client Ltr RE: Medical Appointment", "filename" => '436.docx', 'modalType' => 'formletters17'),
                    '19' => array('number' => "437", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Client Ltr RE: Medical AME Appointment", "filename" => '437.docx', 'modalType' => 'formletters17'),
                    '20' => array('number' => "438", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Client Ltr RE: Attempt - Unable to Reach Potential Client", "filename" => '438.docx', 'modalType' => ''),
                    '21' => array('number' => "439", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Authorization Form Medical", "filename" => '439.docx', 'modalType' => ''),
                    '22' => array('number' => "469", 'group' => "*GEN W", 'category' => 'workers comp', 'prefix' => '', "title" => "Dun Letter to Biller", "filename" => '469.docx', 'modalType' => 'formletters19'),
					'23' => array('number' => "470", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Notice of Hearing", "filename" => '470.docx', 'modalType' => 'formletters20'),
                    '24' => array('number' => "471", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Notice of Continued Hearing", "filename" => '471.docx', 'modalType' => 'formletters21'),
                    '25' => array('number' => "472", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Notice of Deposition", "filename" => '472.docx', 'modalType' => 'formletters22'),
                    '26' => array('number' => "473", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Notice of Deposition (other location)", "filename" => '473.docx', 'modalType' => 'formletters23'),
                    '27' => array('number' => "474", 'group' => "*DEF W", 'category' => 'workers comp', 'prefix' => '', "title" => "Defense Ltr RE: Demand for Docs/Notice of Claim Enclosed", "filename" => '474.docx', 'modalType' => 'formletters24'),
                    '28' => array('number' => "475", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Proof of Service-New Claims", "filename" => '475.docx', 'modalType' => 'formletters24'),
                    '29' => array('number' => "486", 'group' => "*INS W", 'category' => 'workers comp', 'prefix' => '', "title" => "Insurance Letter RE:Claim Form", "filename" => '486.docx', 'modalType' => 'formletters2'),
                    '30' => array('number' => "488", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Client Ltr RE:Conf ltr to clt", "filename" => '488.docx', 'modalType' => 'formletters25'),
                    '31' => array('number' => "491", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Petition for Reimbursement to Physician Witness Fee w/POS", "filename" => '491.docx', 'modalType' => 'formletters26'),
					'32' => array('number' => "492", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Order Allowing for Reimbursement for Payment to Physician for deposition as Witness Fee", "filename" => '492.docx', 'modalType' => 'formletters27'),
                    '33' => array('number' => "500", 'group' => "*NEW", 'category' => 'workers comp', 'prefix' => '', "title" => "Intake Sheet", "filename" => '500.docx', 'modalType' => ''),
					'34' => array('number' => "502", 'group' => "*FAX P", 'category' => 'workers comp', 'prefix' => '', "title" => "Fax Cover - Party Prompt/Case related from Atty Handling/User", "filename" => '502.docx', 'modalType' => 'formletters28'),
                    '35' => array('number' => "504", 'group' => "*FRM W", 'category' => 'workers comp', 'prefix' => '', "title" => "Medical Mileage Reimbursement Sheet", "filename" => '504.docx', 'modalType' => ''),
                    '36' => array('number' => "505", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Declaration Pursuant to LC Section 4906(h)", "filename" => '505.docx', 'modalType' => ''),
                    '37' => array('number' => '506', 'group' => '*PL', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Proof of Service EAMS', 'filename' => '506.docx', 'modalType' => 'formletters9'),
                    '38' => array('number' => "541", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Client Ltr RE: Opening Letter - Thank You", "filename" => '541.docx', 'modalType' => ''),
                    '39' => array('number' => "542", 'group' => "*CL W", 'category' => 'workers comp', 'prefix' => '', "title" => "Client Ltr RE: Opening Letter w/Enclosed Forms", "filename" => '542.docx', 'modalType' => ''),
                    '40' => array('number' => '10025', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Application Death', 'filename' => '10025.docx', 'modalType' => 'injuryselectionPopup'),
                    '41' => array('number' => '10030', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Change of Address wc001', 'filename' => '10030.docx', 'modalType' => ''),
                    '42' => array('number' => '10031', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'claim form wc1s', 'filename' => '10031.docx', 'modalType' => 'injuryselectionPopup'),
                    '43' => array('number' => '10032', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Commutation Petition WC49', 'filename' => '10032.docx', 'modalType' => 'injuryselectionPopup'),
                    '44' => array('number' => "10080", 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Declaration Of Custodian Of Records', "filename" => '10080.docx', 'modalType' => ''),
                    '45' => array('number' => "10039", 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Dismissing Party Defendant', "filename" => '10039.docx', 'modalType' => 'injuryselectionPopup'),
                    '46' => array('number' => '10422', 'group' => 'WCAB Copy Service Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'DocCentral Form', 'filename' => '10422.docx', 'modalType' => 'injuryselectionPopup'),
                    '47' => array('number' => "11209", 'group' => 'WCAB 2013 Forms', 'category' => '', 'prefix' => '', 'title' => "DWC - <span class='form-list-lower'>AD</span> 10133.36 - Physician's Return to Work and Voucher Report", "filename" => '11209.docx', 'modalType' => 'injuryselectionPopup'),                     
                    '48' => array('number' => "10565", 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Exhibit List', "filename" => '10565.docx', 'modalType' => ''),
                    '49' => array('number' => '10048', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Guardian Ad Litem  WC8', 'filename' => '10048.docx', 'modalType' => 'injuryselectionPopup'),
                    '50' => array('number' => "11224", 'group' => 'WCAB 2013 Forms', 'category' => '', 'prefix' => 'OCF', 'title' => "IMR Application for Review", "filename" => '11224.docx', 'modalType' => 'injuryselectionPopup'),
                    '51' => array('number' => '10051', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Joining Party Defendant', 'filename' => '10051.docx', 'modalType' => 'injuryselectionPopup'),
                    '52' => array('number' => '10406', 'group' => 'WCAB Copy Service Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Matrix Order Form', 'filename' => '10406.docx', 'modalType' => 'injuryselectionPopup'),
                    '53' => array('number' => '10006', 'group' => 'WCAB Copy Service Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Med-Legal Records Order Form', 'filename' => '10006.docx', 'modalType' => 'injuryselectionPopup'),
                    '54' => array('number' => '10091', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Notice of Dismissal of Attorney', 'filename' => '10091.docx', 'modalType' => 'injuryselectionPopup'),
                    '55' => array('number' => '10085', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Notice of Hearing', 'filename' => '10085.docx', 'modalType' => 'formletters3'),
                    '56' => array('number' => '10581', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Ortega Counseling Center', 'filename' => '10581.docx', 'modalType' => 'injuryselectionPopup'),
                    '57' => array('number' => "10214", 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Order Of Dismissal', "filename" => '10214.docx', 'modalType' => 'injuryselectionPopup'),
                    '58' => array('number' => "10501", 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Order - General Order', "filename" => '10501.docx', 'modalType' => 'injuryselectionPopup'),
                    '59' => array('number' => '10210', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Petition for Penalties WC-100', 'filename' => '10210.docx',  'modalType' => 'injuryselectionPopup'),
                    '60' => array('number' => '10540', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Petition for Reconsideration (Form 45)', 'filename' => '10540.docx', 'modalType' => 'injuryselectionPopup'),
                    '61' => array('number' => '10099', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Petition to Reopen', 'filename' => '10099.docx', 'modalType' => 'injuryselectionPopup'),
                    '62' => array('number' => '10089', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Petition to Reopen for New and Further Disability', 'filename' => '10089.docx', 'modalType' => 'injuryselectionPopup'),
                    '63' => array('number' => '10081', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Proof of service', 'filename' => '10081.docx', 'modalType' => ''),
                    '64' => array('number' => '10503', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Proof of Service with Verification (Rev 7/84)', 'filename' => '10503.docx', 'modalType' => ''),

                    //WCAB 2013 Forms
                    '65' => array('number' => '11220', 'group' => 'WCAB 2013 Forms', 'category' => '', 'prefix' => '', 'title' => 'QME Form 109 - QME Notice of Unavailability', 'filename' => '11220.docx', 'modalType' => ''),
                    '66' => array('number' => '11216', 'group' => 'WCAB 2013 Forms', 'category' => '', 'prefix' => '', 'title' => 'Q<span class="form-list-lower">ME</span> form 31.5 - Replacement Panel Request-8 Cal. Code of Regulations section 31.5', 'filename' => '11216.docx', 'modalType' => 'injuryselectionPopup'),
                    '67' => array('number' => '11217', 'group' => 'WCAB 2013 Forms', 'category' => '', 'prefix' => '', 'title' => 'QME form 31.7 - Additional Panel Request-8 Cal. Code of Regulations section 31.7', 'filename' => '11217.docx', 'modalType' => 'injuryselectionPopup'),
                    '68' => array('number' => "10075", 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Stipulations For Award Death WC4A', "filename" => '10075.docx', 'modalType' => 'injuryselectionPopup'),
                    '69' => array('number' => '10074', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Stipulations for Award  WC3', 'filename' => '10074.docx', 'modalType' => 'injuryselectionPopup'),
                    '70' => array('number' => '10076', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Substitution of Attorney  WC36', 'filename' => '10076.docx', 'modalType' => 'injuryselectionPopup'),
                    '71' => array('number' => '10078', 'group' => 'WCAB Forms', 'category' => '', 'prefix' => 'OCF', 'title' => 'Verification', 'filename' => '10078.docx', 'modalType' => 'formletters31'),
					
                    //WCAB EAMS Forms
                    '72' => array('number' => '10886', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS  PACKET Application for Adjudication', 'filename' => '10886.docx', 'modalType' => 'injuryselectionPopup'),
                    '73' => array('number' => '10885', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS APPSIF - Application for Subsequent Injuries Fund Benefits Uef', 'filename' => '10885.docx', 'modalType' => 'injuryselectionPopup'),
                    '74' => array('number' => '10856', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Compromise and Release  (form 10214(c)) <span class="form-list-lower"><span>D</span>WC-CA</span>', 'filename' => '10856.docx', 'modalType' => 'formletters31'),
                    '75' => array('number' => "10857", 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Compromise And Release (<span class="form-list-lower">D</span>ependancy Claim) (form 10214(d)) D<span class="form-list-lower">WC-CA</span>', "filename" => '10857.docx', 'modalType' => 'injuryselectionPopup'),
                    '76' => array('number' => '10859', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Declaration of Readiness - DOR  (form 10250.1) DWC-CA', 'filename' => '10859.docx', 'modalType' => ''),
                    '77' => array('number' => '10889', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Declaration of Readiness Expedited Trial  (form 10208.3, 4/2014) DWC-CA', 'filename' => '10889.docx', 'modalType' => 'injuryselectionPopup'),
                    '78' => array('number' => '10861', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Document Cover Sheet (form 10232.1) DWC-CA', 'filename' => '10861.docx', 'modalType' => 'formletters31'),
                    '79' => array('number' => '10862', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS DWC-CA Document Separator Sheet  (form 10232.2)', 'filename' => '10862.docx', 'modalType' => 'formletters33'),
                    '80' => array('number' => '10863', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Minutes of Hearing  (form 10245) DWC-CA', 'filename' => '10863.docx', 'modalType' => 'injuryselectionPopup'),
                    '81' => array('number' => '10891', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Notice and Request for Allowance of Lien  (form 6) WCAB 2017', 'filename' => '10891.docx', 'modalType' => 'injuryselectionPopup'),
                    '82' => array('number' => "10875", 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Notice of Offer of Modified or Alternate Work (dwc-ad Form 10133.53 SJDB) RRTW', "filename" => '10875.docx', 'modalType' => 'injuryselectionPopup'),
                    '83' => array('number' => "10876", 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Notice Of Offer Of Regular Work  (form 10118 SJDB) RRTW', "filename" => '10876.docx', 'modalType' => 'injuryselectionPopup'),
                    '84' => array('number' => '10058', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Pre-trial Conference Statement (rev 9/2010)', 'filename' => '10058.docx', 'modalType' => 'injuryselectionPopup'),
                    '85' => array('number' => '10871', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Request for Consultative Rating (RCR)  (form 104 DEU) DWC-AD', 'filename' => '10871.docx', 'modalType' => 'injuryselectionPopup'),
                    '86' => array('number' => '10873', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => "WC EAMS Request for Summary Rating Determination - of AME's or QME's Report  (form 101 DEU) DWC-AD", 'filename' => '10873.docx', 'modalType' => 'injuryselectionPopup'),
                    '87' => array('number' => '10874', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'Wc Eams Request for Summary Rating Determination - Primary Treating Physician  (form 102 Deu) Dwc-ad', 'filename' => '10874.docx', 'modalType' => 'injuryselectionPopup'),
                    '88' => array('number' => "10865", 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => "WC EAMS Stipulations With Request For Awards (<span class='form-list-lower'>D</span>eath Case) (form 10214(b)) D<span class='form-list-lower'>WC-CA</span>", "filename" => '10865.docx', 'modalType' => 'injuryselectionPopup'),
                    '89' => array('number' => '10890', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Stipulations with Request for Awards Injuries Post 1/1/2013  (form 10214(a), 4/2014)', 'filename' => '10890.docx', 'modalType' => 'formletters31'),
                    '90' => array('number' => '10864', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Stipulations with Request for Awards Pre 1/1/2013 (form 10214(a)) DWC-CA', 'filename' => '10864.docx', 'modalType' => 'formletters31'),
                    '91' => array('number' => '10882', 'group' => 'WCAB EAMS Forms', 'category' => '', 'prefix' => '', 'title' => 'WC EAMS Supplemental job displacement nontransferable training voucher (DWC-AD form 10133.57) Rrtw', 'filename' => '10882.docx', 'modalType' => 'injuryselectionPopup'),
                    '92' => array('number' => "62", 'group' => '*CL P', 'category' => 'pension', 'prefix' => '', 'title' => 'Ltr to LACERA CLIENT COVER', "filename" => 'Lacera_client.docx', 'modalType' => 'formletters32'),
                    '93' => array('number' => "84", 'group' => '*BRD P', 'category' => 'pension', 'prefix' => '', 'title' => 'Ltr to LACERA COVER OLD', "filename" => 'lacera_old.docx', 'modalType' => ''),
                    '94' => array('number' => "84", 'group' => '*BRD P', 'category' => 'pension', 'prefix' => '', 'title' => 'Ltr to LACERA COVER NEW', "filename" => 'lacera_new.docx', 'modalType' => ''),
                    '95' => array('number' => "84", 'group' => '*BRD P', 'category' => 'pension', 'prefix' => '', 'title' => 'Ltr to LACERA AMENDMENT', "filename" => 'Lacera_amendment.docx', 'modalType' => ''),
                    '96' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to OCERS COVER', "filename" => 'ocers.docx', 'modalType' => ''),
                    '97' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to SBCERA COVER', "filename" => 'sbcera.docx', 'modalType' => 'formletters2'),
                    '98' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to SBCERS COVER', "filename" => 'sbcers.docx', 'modalType' => 'formletters2'),
                    '99' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to VCERA COVER', "filename" => 'vcera.docx', 'modalType' => 'formletters2'),
                    '100' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to LAFPP COVER', "filename" => 'lafpp.docx', 'modalType' => 'formletters2'),
                    '101' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to CALPERS COVER 942716', "filename" => 'calpers_942716.docx', 'modalType' => 'formletters2'),
                    '102' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to CALPERS COVER 2796', "filename" => 'calpers_2796.docx', 'modalType' => 'formletters2'),
                    '103' => array('number' => "97", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to CALPERS COVER 942711', "filename" => 'calpers_942711.docx', 'modalType' => 'formletters2'),
                    '104' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to CALPERS LETTER OF REP', "filename" => 'calpers_rep.docx', 'modalType' => ''),
                    '105' => array('number' => "#", 'group' =>'*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer CALPERS', "filename" => 'calsper_retainer.docx', 'modalType' => 'formletters2'),
					'106' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer City of LA', "filename" => 'retainer_LA.docx', 'modalType' => 'formletters2'),
					'107' => array('number' => "#", 'group' =>'*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer KCERA', "filename" => 'retainer_kcera.docx', 'modalType' => 'formletters2'),
					'108' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer LACERA', "filename" => 'retainer_lacera.docx', 'modalType' => 'formletters2'),
					'109' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer SBCERS', "filename" => 'retainer_sbcers.docx', 'modalType' => 'formletters2'),
					'110' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer SBCERA', "filename" => 'retainer_sbcera.docx', 'modalType' => 'formletters2'),
					'111' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer OCERS', "filename" => 'retainer_ocera.docx', 'modalType' => 'formletters2'),
					'112' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Retainer VCERA', "filename" => 'retainer_vcera.docx', 'modalType' => 'formletters2'),
					'113' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Rep', "filename" => 'rep_letter.docx', 'modalType' => ''),
					'114' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to CITY WAIVER', "filename" => 'city_waiver.docx', 'modalType' => ''),
					'115' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to SBCERS forms only', "filename" => 'SBCERS_forms.docx', 'modalType' => ''),
					'116' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to FORMS SCDR Packet', "filename" => 'FORMS_SCDR.docx', 'modalType' => ''),
					'117' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to 1 A TIER 3-6 app', "filename" => '1ATIER36app.docx', 'modalType' => ''),
					'118' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Power-attorney-form', "filename" => 'power_attorney.docx', 'modalType' => ''),
					'119' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Vcera Application for Disability Retirement', "filename" => 'Vcera_application.docx', 'modalType' => ''),
					'120' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter toLACERA Application (with PS) 8.22.18 (2)', "filename" => 'LACERA_Application_ps.docx', 'modalType' => ''),
                    '121' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to FORMS Physician Report Packet', "filename" => 'FORMS_Physician.docx', 'modalType' => ''),
                    '122' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Disability Application rev 04-23-2014 - Disability Application rev 09-2010 and Procedures', "filename" => '4DisabilityApplication_rev04-23-2014-DisabilityApplicaton_rev09-2010andProcedures.docx', 'modalType' => ''),
                    '123' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Disability Application Packet', "filename" => 'Disability_Application_packet.docx', 'modalType' => ''),
                    '124' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to Disability Application Handbook (Dec2016)_interactive1', "filename" => '2DisabilityApplicationHandbook(Dec2016)_interactive1.docx', 'modalType' => ''),
                    '125' => array('number' => "#", 'group' => '*GEN P', 'category' => 'pension', 'prefix' => '', 'title' => 'Pension Letter to 1 CalPERS App 2018', "filename" => '1CalPERSApp2018.docx', 'modalType' => 'injuryselectionPopup'),
                    '130' => array('number' => "463", 'group' => '*PLD W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Petition for Order Allowing Attorney’s Fees LC 5710 wPOS', "filename" => '463.docx', 'modalType' => 'formletters131'),
					'129' => array('number' => "461", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Order Allowing Benefits (No Mileage) Pursuant to LC 5710", "filename" => '461.docx', 'modalType' => 'formletters130'),
                    '126' => array('number' => "543", 'group' => '*GEN P', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Envelope', "filename" => 'Envelope.docx', 'modalType' => 'envelope'),
                    '127' => array('number' => "442", 'group' => '*DEF W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Defense Ltr RE LC5710 Fee Reimbursement', "filename" => '442.docx', 'modalType' => 'formletters127'),
                    '128' => array('number' => "460", 'group' => "*PLD W", 'category' => 'workers comp', 'prefix' => '', "title" => "Orders Allowing Benefits Pursuant to LC 5710", "filename" => '460.docx', 'modalType' => 'formletters129'),
                    '132' => array('number' => "425", 'group' => '*DR W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Dr. Ltr AME: Joint (List Body Parts) ', "filename" => '425.docx', 'modalType' => 'formletters134'),
                    '131' => array('number' => "441", 'group' => '*DEF W', 'category' => 'workers comp', 'prefix' => '', 'title' => 'Defense Ltr RE: Mileage to Applicant ', "filename" => '441.docx', 'modalType' => 'formletters132')
                )
        )
);

define('form_name', json_encode(
                array(
                    'All Forms' => 'All Forms',
                    'WCAB Forms' => 'WCAB Forms',
                    'WCAB EAMS Forms' => 'WCAB EAMS Forms',
                    'WCAB 2013 Forms' => 'WCAB 2013 Forms',
                    'WCAB Copy Service Forms' => 'WCAB Copy Service Forms'
                )
        )
);

define("Judge", json_encode(
                array(
                    "Zamudio" => "Zamudio",
                    "Davidson-Guerra" => "Davidson-Guerra",
                    "Feldman" => "Feldman",
                    "O'Kane" => "O'Kane",
                    "Shapiro" => "Shapiro",
                    "Cohn" => "Cohn",
                    "Morgan" => "Morgan",
                    "Gutierrez" => "Gutierrez",
                    "Johnston" => "Johnston",
                    "Keyson" => "Keyson",
                    "Kitchens" => "Kitchens",
                    "Brown" => "Brown",
                    "Pollack" => "Pollack",
                    "Cole" => "Cole",
                    "Greenberg" => "Greenberg",
                )
        )
);
define("Venue", json_encode(
                array(
                    "Anaheim" => "AHM",
                    "Bakersfield" => "BAK",
                    "Eureka" => "EUR",
                    "Redding" => "RDG",
                    "San Luis Obispo" => "SLO",
                    "Santa Barbara" => "SBA",
                    "San Bernardino" => "SBR",
                    "Oxnard" => "OXN",
                    "Bakersfield" => "BAK",
                    "Pomona" => "POM",
                    "San Jose" => "SJO",
                    "Riverside" => "RIV",
                    "Stockton" => "STK",
                    "San Diego" => "SDO",
                    "Long Beach" => "LBO",
                    "Fresno" => "FRE",
                    "Van Nuys" => "VNO",
                    "Marina del Rey" => "MDR",
                /* "Phone" => "Phone",
                  "VNO" => "VNO",
                  "MDR" => "MDR",
                  "OXN" => "OXN",
                  "LA" => "LA",
                  "LB" => "LB",
                  "Los Angeles" => "Los Angeles",
                  "Sherman Oaks" => "Sherman Oaks",
                  "Beverly Hills" => "Beverly Hills", */
                )
        )
);

define('fee_cost', json_encode(
                array(
                    'CFORM' => 'CFORM',
                    'OTHER' => 'OTHER'
                )
        )
);

////Phone,VNO,MDR,OXN,LA,LB,Los Angeles,Sherman Oaks,Beverly Hills
//Zamudio,Davidson-Guerra,Feldman,O'Kane,Shapiro,Cohn,Morgan,Gutierrez,Johnston,Keyson,Kitchens,
//Brown,Pollack,Cole,Greenberg
/* End of file constants.php */
/* Location: ./application/config/constants.php */

define('LAW_PUBLIC_DOMAIN', 'compcaseonline.com');
define('LAW_SUPER_ADMIN', 'lawwaresuperadmin.dev');

define("staff_title", json_encode(
                array(
                    "Administrator" => "Administrator",
                    "Attorney" => "Attorney",
                    "Hearing Representative" => "Hearing Representative",
                    "Legal Assistant" => "Legal Assistant",
                    "Paralegal" => "Paralegal",
                    "Paralegal Trainee" => "Paralegal Trainee",
                    "Secratary" => "Secratary",
                    "Secratary Assistant" => "Secratary Assistant"
                )
        )
);

define("product_delivery_unit", json_encode(
                array(
                    "1" => "ADJ",
                    "2" => "DEU",
                    "3" => "INT",
                    "4" => "RSU",
                    "5" => "SAU"
                )
        )
);

define('doc_type', json_encode(
                array(
                    '0' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS'),
                    '1' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS'),
                    '2' => array('category' => 'ADJ', 'title' => 'MEDICAL DOCS'),
                    '3' => array('category' => 'ADJ', 'title' => 'MISC'),
                    '4' => array('category' => 'ADJ', 'title' => 'IBR'),
                    '5' => array('category' => 'ADJ', 'title' => 'IMR'),
                    '6' => array('category' => 'DEU', 'title' => 'DEU FORMS'),
                    '7' => array('category' => 'DEU', 'title' => 'DEU DOCS - OTHER'),
                    '8' => array('category' => 'DEU', 'title' => 'MEDICAL REPORTS'),
                    '9' => array('category' => 'DEU', 'title' => 'MISC'),
                    '10' => array('category' => 'INT', 'title' => 'AD LEGAL'),
                    '11' => array('category' => 'INT', 'title' => 'MISC'),
                    '12' => array('category' => 'INT', 'title' => 'MEDICAL DOCS'),
                    '13' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS'),
                    '14' => array('category' => 'RSU', 'title' => 'NON-FORM CORRESPONDENCE'),
                    '15' => array('category' => 'RSU', 'title' => 'SUPPORTING DOCUMENTS'),
                    '16' => array('category' => 'RSU', 'title' => 'OTHER'),
                    '17' => array('category' => 'SAU', 'title' => 'LEGAL DOCS'),
                    '18' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS'),
                    '19' => array('category' => 'SAU', 'title' => 'MEDICAL DOCS'),
                    '20' => array('category' => 'SAU', 'title' => 'MISC'),
             )
        )
);

define('doc_title', json_encode(
                array(
                    '0' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'132A'),
                    '1' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'1990-1993 WINDOW PERIOD APPLICATION'),
                    '2' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'1990-1993 WINDOW PERIOD APPLICATION - DEATH CLAIM'),
                    '3' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'4906(g) DECLARATION'),
                    '4' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'10770.5 VERIFICATION'),
                    '5' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'10770.6 VERIFICATION'),
                    '6' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'AMENDED APPLICATION FOR ADJUDICATION'),
                    '7' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'AMENDED APPLICATION FOR ADJUDICATION - DEATH'),
                    '8' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'AMENDED COMPROMISE AND RELEASE'),
                    '9' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'AMENDED STIPULATIONS'),
                    '10' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'ANSWER'),
                    '11' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'ANSWER TO 132A'),
                    '12' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'ANSWER TO APPLICATION FOR ADJUDICATION OF CLAIM'),
                    '13' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'ANSWER TO PETITION FOR RECONSIDERATION'),
                    '14' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'ANSWER TO PETITION FOR REMOVAL'),
                    '15' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'ANSWER TO S&W'),
                    '16' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'APPEAL OF DETERMINATION OF A.D. - OTHER'),
                    '17' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'APPEAL OF DETERMINATION OF A.D. - RSU'),
                    '18' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>"APPEAL OF DIRECTOR'S RETURN TO WORK SUPPLEMENT DECISION"),
                    '19' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'APPLICATION FOR ADJUDICATION'),
                    '20' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'APPLICATION FOR ADJUDICATION OF CLAIM - DEATH'),
                    '21' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'APPLICATION FOR SUBSEQUENT INJURIES FUND BENEFITS'),
                    '22' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'ARBRITATION SUBMITTAL FORM'),
                    '23' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'BENEFIT NOTICE'),
                    '24' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'BENEFIT PRINTOUT'),
                    '25' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'BIRTH CERTIFICATE'),
                    '26' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'C&R: RESPONSE TO LETTER REQUESTING INFORMATION'),
                    '27' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'CHANGE OF ADDRESS'),
                    '28' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'CHANGE OF HANDLING LOCATION'),
                    '29' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'COMPROMISE AND RELEASE'),
                    '30' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'COMPROMISE AND RELEASE DEPENDENCY CLAIM'),
                    '31' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'COMPROMISE AND RELEASE - SIGNED'),
                    '32' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'COMFIRMATION OF LIEN ACTIVATION FEE'),
                    '33' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'CONFIRMATION OF PAYMENT 2004-2006'),
                    '34' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'DEATH CERTIFICATE'),
                    '35' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'DECLARATION OF READINESS TO PROCEED'),
                    '36' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'DECLARATION OF READINESS TO PROCEED TO EXPEDITED HEARING'),
                    '37' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'DEPOSITION TRANSCRIPT'),
                    '38' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'DISMISSAL OF ATTORNEY'),
                    '39' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'DWC-1 CLAIM FORM'),
                    '40' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'FEE DISCLOSURE STATEMENT'),
                    '41' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'MARRIAGE LICENSE'),
                    '42' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'MINUTES OF HEARING'),
                    '43' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF CHANGE OF ADMINISTRATOR'),
                    '44' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF CHANGE OF REPRESENTATION'),
                    '45' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF NON - REPRESENTATION'),
                    '46' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF OFFER OF REGULAR WORK'),
                    '47' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF POTENTIAL SJDB'),
                    '48' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF REPRESENTATION'),
                    '49' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF TEMPORARY DISABILITY BENEFITS (DWC 500-B)'),
                    '50' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF PERMANENT DISABILITY BENEFITS'),
                    '51' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'NOTICE REGARDING MPN'),
                    '52' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'OBJECTION-OTHER'),
                    '53' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'OBJECTION TO DECLARATION OF READINESS TO PROCEED'),
                    '54' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'OBJECTION TO PETITION TO TERMINATE LIABILITY FOR TEMPORARY DISABILITY'),
                    '55' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'OBJECTION TO PETITION/REQUEST FOR DISMISSAL'),
                    '56' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'OBJECTION TO VENUE'),
                    '57' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION-OTHER'),
                    '58' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION BY NON-PHYSICIAN LIEAN CLAIMANT FOR MEDICAL INFORMATION'),
                    '59' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR APPOINTMENT OF GUARDIAN AD LITEM'),
                    '60' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR APPOINTMENT OF REPLACEMENT QME'),
                    '61' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR APPOINTMENT OF REPLACEMENT QME PANEL'),
                    '62' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ATTORNEY FEES'),
                    '63' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR AUTOMATIC REASSIGNMENT'),
                    '64' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CERTIFICATION'),
                    '65' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CHANGE OF GUARDIAN AD LITEM'),
                    '66' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CHANGE OF VENUE'),
                    '67' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR COMMUTATION'),
                    '68' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CONSOLIDATION'),
                    '69' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CONTRIBUTION'),
                    '70' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR COSTS'),
                    '71' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CREDIT FOR OVERPAYMENT OF BENEFITS'),
                    '72' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DEPOSITION ATTORNEY FEE (LC 5710) '),
                    '73' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DEPOSITION ATTORNEY FEE (LC 5710) - WALKTHROUGH'),
                    '74' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISCOVERY ORDER'),
                    '75' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISMISSAL'),
                    '76' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISMISSAL OF A PARTY'),
                    '77' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISQUALIFICATION'),
                    '78' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR FINDING OF FACT'),
                    '79' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR INCREASED BENEFITS FOR SERIOUS AND WILLFUL MISCONDUCT'),
                    '80' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR JOINDER'),
                    '81' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER ASSIGNING SECOND PANEL QME'),
                    '82' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER COMPELLING SERVICE OF MEDICAL BOARD'),
                    '83' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER CORRECTING CLERICAL ORDER'),
                    '84' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER OF CONFIDENTIALITY'),
                    '85' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER OF PUBLICATION'),
                    '86' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR PENALTIES'),
                    '87' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR RECONSIDERATION'),
                    '88' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR RECONSIDERATION/REMOVAL'),
                    '89' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR REHAB ATTORNEY FEE'),
                    '90' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR REHAB ATTORNEY FEE- WALKTHROUGH'),
                    '91' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR REMOVAL'),
                    '92' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR RESTITUTION'),
                    '93' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR SANCTIONS/COST'),
                    '94' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR STAY OF PROCEEDINGS'),
                    '95' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR THIRD PARTY CREDIT'),
                    '96' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR WRIT OF ATTACHMENT'),
                    '97' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO BAR RECEIPT OF BENEFITS'),
                    '98' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO BE RELIEVED AS ATTORNEY OF RECORD'),
                    '99' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO CHANGE ADMINISTRATOR OF AWARD'),
                    '100' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO COMPEL ATTENDANCE AT MEDICAL EVALUATION'),
                    '101' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO COMPEL ATTENDANCE AT MEDICAL EVALUATION - WALKTHROUGH'),
                    '102' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO COMPEL DEPOSITION'),
                    '103' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO COMPEL DEPOSITION-WALKTHROUGH'),
                    '104' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO DISMISS APPLICANT'),
                    '105' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO DISMISS ATTORNEY'),
                    '106' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO DISMISS PARTY DEFENDANT'),
                    '107' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO DISQUALIFY PANEL QME'),
                    '108' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO ENFORCE'),
                    '109' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO ENFORCE IBR DETERMINATION'),
                    '110' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO JOIN APPLICANT'),
                    '111' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO JOIN DEFENDANT;// change from PETITION TO JOIN PARTY DEFENDANT on 10/'),
                    '112' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO JOIN PARTY DEFENDANT'),
                    '113' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO JOIN UEBTF'),
                    '114' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO QUASH DEPOSITION'),
                    '115' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO QUASH SUBPOENA'),
                    '116' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO REOPEN'),
                    '117' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO REOPEN TO REDUCE'),
                    '118' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO RESOLVE MED-LEGAL DISPUTE-NON-IBR'),
                    '119' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SEAL THE RECORD'),
                    '120' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SET ASIDE COMPROMISE AND RELEASE'),
                    '121' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SET ASIDE STIPULATIONS'),
                    '122' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO STRIKE RATING INSTRUCTIONS'),
                    '123' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SUSPEND PROCEEDINGS'),
                    '124' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO TERMINATE LIABILITY FOR MEDICAL TREATMENT'),
                    '125' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO TERMINATE LIABILITY FOR TEMPORARY DISABILITY INDEMNITY'),
                    '126' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO TRANSFER CARE TO MEDICAL PROVIDER NETWORK'),
                    '127' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO UNSEAL THE RECORD'),
                    '128' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PETITION TO VACATE SUBMISSION'),
                    '129' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'POINTS AND AUTHORITIES-TRIAL BRIEF-HEARING BRIEF'),
                    '130' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PRE-TRIAL CONFERENCE STATEMENT ( 5 PAGER )'),
                    '131' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PROOF OF SERVICE'),
                    '132' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PROPOSED ORDER/AWARD'),
                    '133' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'PROPOSED STIPULATION AWARD AND/OR ORDER'),
                    '134' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'QME WAIVER'),
                    '135' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'REPORT AND RECOMMENDATION ON PETITION FOR REMOVAL'),
                    '136' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'REQUEST FOR CONTINUANCE'),
                    '137' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'REQUEST FOR ORDER TAKING OFF CALENDAR'),
                    '138' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'REQUEST TO WITHDRAW PETITION FOR RECONSIDERATION'),
                    '139' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'REQUEST TO WITHDRAW PETITION FOR REMOVAL'),
                    '140' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'RESPONSE TO PETITION'),
                    '141' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'RESUBMITTED 1990 APPLICATION'),
                    '142' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'RETURN TO WORK SUPPLEMENT PROGRAM ANSWER TO APPEAL'),
                    '143' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'SPECIAL NOTICE OF LAWSUIT'),
                    '144' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'STIPS: RESPONSE TO LETTER REQUESTING INFORMATION'),
                    '145' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'STIPULATION AND AWARD AND/OR ORDER'),
                    '146' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'STIPULATION WITH AWARD(DEATH)'),
                    '147' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'STIPULATIONS WITH REQUEST FOR AWARD'),
                    '148' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'STIPULATIONS WITH REQUEST FOR AWARD DOI POST 1-1-2013'),
                    '149' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'STIPULATIONS WITH REQUEST FOR AWARD - SIGNED'),
                    '150' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'SUBSTITUTION OF ATTORNEY'),
                    '151' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'SUPPLEMENTAL PETITION FOR RECONSIDERATION'),
                    '152' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'THIRD PARTY COMPROMISE AND RELEASE'),
                    '153' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'TRANSCRIPT OF RECORD'),
                    '154' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'VENUE VERIFICATION'),
                    '155' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'WAGE STATEMENT'),
                    '156' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'WITHDRAWAL OF DECLARATION OF READINESS TO PROCEED'),
                    '157' => array('category' => 'ADJ', 'title' => 'LEGAL DOCS','val'=>'WRIT'),
                    '158' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'4903.8 (A) (B) ASSIGNMENT'),
                    '159' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'4903.8 (D) DECLARATION'),
                    '160' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'BILLS-OTHER'),
                    '161' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'CONFIRMATION OF LIEN FILING FEE'),
                    '162' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'EDD LIEN'),
                    '163' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'ITEMIZED STATEMENT'),
                    '164' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'MEDICAL BILLS'),
                    '165' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'NOTICE AND REQUEST FOR ALLOWANCE OF LIEN'),
                    '166' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'ORIGINAL BILL'),
                    '167' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'REQUEST FOR WITHDRAWAL OF LIEN'),
                    '168' => array('category' => 'ADJ', 'title' => 'LIENS AND BILLS','val'=>'SUPPLEMENTAL LIEN FORM AND SECTION 4903.05 (c) DECLARATION'),
                    '169' => array('category' => 'ADJ', 'title' => 'MEDICAL DOCS','val'=>'ALL MEDICAL REPORTS'),
                    '170' => array('category' => 'ADJ', 'title' => 'MEDICAL DOCS','val'=>'AME REPORTS'),
                    '171' => array('category' => 'ADJ', 'title' => 'MEDICAL DOCS','val'=>'P&S REPORTS'),
                    '172' => array('category' => 'ADJ', 'title' => 'MEDICAL DOCS','val'=>'QME REPORTS'),
                    '173' => array('category' => 'ADJ', 'title' => 'MEDICAL DOCS','val'=>'TREATING PHYSICIAN'),
                    /*'174' => array('category' => 'ADJ', 'title' => 'MEDICAL DOCS','val'=>'ALL MEDICAL REPORTS'),*/
                    '175' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'CONSULTATIVE RATING'),
                    '176' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'CORRESPONDENCE - OTHER'),
                    '177' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'SETTLEMENT OFFER LETTER'),
                    '178' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'SUBPOENA'),
                    '179' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'SUBPOENA DUCES TECUM'),
                    '180' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'SUMMARY RATING'),
                    '181' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'TRANSMITTAL LETTER'),
                    '182' => array('category' => 'ADJ', 'title' => 'MISC','val'=>'TYPED OR WRITTEN LETTER'),
                    '183' => array('category' => 'ADJ', 'title' => 'IBR','val'=>'APPEAL OF DETERMINATION OF A.D.-IBR'),
                    '184' => array('category' => 'ADJ', 'title' => 'IMR','val'=>'APPEAL OF DETERMINATION OF A.D.-IMR'),
                    '185' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'EMPLOYEES PERMANENT DISABILITY QUESTIONAIRE'),
                    '186' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'COMMUTATION REQUEST'),
                    '187' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'REQUEST FOR CONSULTATIVE RATING'),
                    '188' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'REQUEST FOR FACTUAL CORRECTION'),
                    '189' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'REQUEST FOR INFORMAL RATING BY INSURANCE CARRIER OR SELF-INSURER'),
                    '190' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'REQUEST FOR RECONSIDERATION OF SUMMARY RATING BY THE AD'),
                    '191' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'REQUEST FOR SUMMARY RATING DETERMINATION - QME REPORT'),
                    '192' => array('category' => 'DEU', 'title' => 'DEU FORMS','val'=>'REQUEST FOR SUMMARY RATING DETERMINATION - TREATING PHYSICIAN'),
                    '193' => array('category' => 'DEU', 'title' => 'DEU DOCS - OTHER','val'=>'EARNINGS INFORMATION'),
                    '194' => array('category' => 'DEU', 'title' => 'DEU DOCS - OTHER','val'=>'JOB DESCRIPTION'),
                    '195' => array('category' => 'DEU', 'title' => 'DEU DOCS - OTHER','val'=>'PHOTOGRAPHS'),
                    '196' => array('category' => 'DEU', 'title' => 'DEU DOCS - OTHER','val'=>'RESPONSE TO REQUEST FOR FACTUAL CORRECTION'),
                    '197' => array('category' => 'DEU', 'title' => 'MEDICAL REPORTS','val'=>'AME'),
                    '198' => array('category' => 'DEU', 'title' => 'MEDICAL REPORTS','val'=>'DEFAULT QME ( REPRESENTED WITH DOI ON/AFTER 1-1-05)'),
                    '199' => array('category' => 'DEU', 'title' => 'MEDICAL REPORTS','val'=>'PANEL QME (NON-REPRESENTED ALL DOI)'),
                    '200' => array('category' => 'DEU', 'title' => 'MEDICAL REPORTS','val'=>'REPRESENTED QME (REPRESENTED WITH DOI BEFORE 1-1-05)'),
                    '201' => array('category' => 'DEU', 'title' => 'MEDICAL REPORTS','val'=>'TREATING PHYSICIAN'),
                    '202' => array('category' => 'DEU', 'title' => 'MISC','val'=>'PROOF OF SERVICE'),
                    '203' => array('category' => 'DEU', 'title' => 'MISC','val'=>'TYPED OR WRITTEN LETTER'),
                    '204' => array('category' => 'INT', 'title' => 'AD LEGAL','val'=>'MEDICAL REPORTS'),
                    '205' => array('category' => 'INT', 'title' => 'AD LEGAL','val'=>'MISCELLANEOUS'),
                    '206' => array('category' => 'INT', 'title' => 'AD LEGAL','val'=>'PETITION FOR CHANGE OF PRIMARY TREATING PHYSICIAN'),
                    '207' => array('category' => 'INT', 'title' => 'AD LEGAL','val'=>'RESPONSE TO PETITION FOR CHANGE OF PRIMARY TREATING PHYSICIAN'),
                    '208' => array('category' => 'INT', 'title' => 'MISC','val'=>'CORRESPONDENCE'),
                    '209' => array('category' => 'INT', 'title' => 'MISC','val'=>'GENERAL PUBLIC REQUEST FOR INFORMATION'),
                    '210' => array('category' => 'INT', 'title' => 'MISC','val'=>'I&A CONFERENCE APPOINTMENT NOTICE'),
                    '211' => array('category' => 'INT', 'title' => 'MISC','val'=>'REQUEST TO I&A FOR CONFERENCE'),
                    '212' => array('category' => 'INT', 'title' => 'MEDICAL DOCS','val'=>'MEDICAL REPORTS'),
                    '213' => array('category' => 'INT', 'title' => 'MEDICAL DOCS','val'=>'REQUESTS TO I&A FOR REPLACEMENT OR ADDITIONAL QME PANEL'),
                    '214' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10118 NOTICE OF OFFER OF REGULAR WORK'),
                    '215' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10133.32 SUPPLEMENTAL JOB DISPLACEMENT NON-TRANSFERABLE VOUCHER'),
                    '216' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>"DWC-AD  10133.33 DESCRIPTION OF EMPLOYEE'S JOB DUTIES"),
                    '217' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10133.35 NOTICE OF OFFER OF REG MOD OR ALTERNATIVE WORK'),
                    '218' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>"DWC-AD  10133.36 PHYSICIAN’S RETURN-TO-WORK &VOUCHER REPORT"),
                    '219' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10133.53 NOTICE OF OFFER OF MODIFIED OR ALTERNATIVE WORK'),
                    '220' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10133.55 REQUEST FOR DISPUTE RESOLUTION BEFORE THE AD'),
                   /* '221' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10133.55 REQUEST FOR DISPUTE RESOLUTION BEFORE THE AD'),
                    '222' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10133.55 REQUEST FOR DISPUTE RESOLUTION BEFORE THE AD'),*/
                    '223' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'DWC-AD  10133.57 SUPPLEMENTAL JOB DISPLACEMENT VOUCHER'),
                    '224' => array('category' => 'RSU', 'title' => 'DWC - AD FORMS','val'=>'OBJECTION - OTHER'),
                    '225' => array('category' => 'RSU', 'title' => 'NON-FORM CORRESPONDENCE','val'=>'LETTER'),
                    '226' => array('category' => 'RSU', 'title' => 'SUPPORTING DOCUMENTS','val'=>'ERGONOMIC REPORT'),
                    '227' => array('category' => 'RSU', 'title' => 'SUPPORTING DOCUMENTS','val'=>'JOB DESCRIPTION'),
                    '228' => array('category' => 'RSU', 'title' => 'SUPPORTING DOCUMENTS','val'=>'MEDICAL REPORT'),
                    '229' => array('category' => 'RSU', 'title' => 'SUPPORTING DOCUMENTS','val'=>'POSITION STATEMENT'),
                    '230' => array('category' => 'RSU', 'title' => 'SUPPORTING DOCUMENTS','val'=>'SCHOOL & VRTWC INVOICES'),
                    '231' => array('category' => 'RSU', 'title' => 'OTHER','val'=>'OTHER CORRESPONDENCE'),
                    '232' => array('category' => 'RSU', 'title' => 'OTHER','val'=>'PROOF OF SERVICE'),
                    '233' => array('category' => 'RSU', 'title' => 'OTHER','val'=>'VRTWC REPORT'),
                    '234' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'10770.5 VERIFICATION'),
                    '235' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'10770.6 VERIFICATION'),
                    '236' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'4906(h) DECLARATION'),
                    '237' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'ANSWER'),
                    '238' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'ANSWER TO PETITION FOR RECONSIDERATION'),
                    '239' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'CONFIRMATION OF LIEN ACTIVATION FEE'),
                    '240' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'CONFIRMATION OF PAYMENT 2004-2006'),
                    '241' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'DECLARATION OF READINESS TO PROCEED'),
                    '242' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'DISMISSAL OF ATTORNEY'),
                    '243' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF CHANGE OF REPRESENTATION'),
                    '244' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'NOTICE OF REPRESENTATION'),
                    '245' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'OBJECTION - OTHER'),
                    '246' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'OBJECTION TO DECLARATION OF READINESS TO PROCEED'),
                    '247' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'OBJECTION TO PETITION/REQUEST FOR DISMISSAL'),
                    '248' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'OBJECTION TO VENUE'),
                    '249' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION - OTHER'),
                    '250' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR AUTOMATIC REASSIGNMENT'),
                    '251' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CERTIFICATION'),
                    '252' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CHANGE OF VENUE'),
                    '253' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CONSOLIDATION'),
                    '254' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR COSTS'),
                    '255' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR CREDIT FOR OVERPAYMENT OF BENEFITS'),
                    '256' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISCOVERY ORDER'),
                    '257' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISMISSAL'),
                    '258' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISMISSAL OF A PARTY'),
                    '259' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR DISQUALIFICATION'),
                    '260' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR FINDING OF FACT'),
                    '261' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR JOINDER'),
                    '262' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER CORRECTING CLERICAL ERROR'),
                    '263' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER OF CONFIDENTIALITY'),
                    '264' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR ORDER OF PUBLICATION'),
                    '265' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR PENALTIES'),
                    '266' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR RECONSIDERATION'),
                    '267' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR RECONSIDERATION/REMOVAL'),
                    '268' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR REMOVAL'),
                    '269' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR RESTITUTION'),
                    '270' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION FOR SANCTIONS/COST'),
                    '271' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO BAR RECEIPT OF BENEFITS'),
                    '272' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO BE RELIEVED AS ATTORNEY OF RECORD'),
                    '273' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO COMPEL DEPOSITION'),
                    '274' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO COMPEL DEPOSITION-WALKTHROUGH'),
                    '275' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO DISMISS APPLICANT'),
                    '276' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO DISMISS ATTORNEY'),
                    '277' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO DISMISS PARTY DEFENDANT'),
                    '278' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO ENFORCE'),
                    '279' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO JOIN DEFENDANT'),
                    '280' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO JOIN PARTY DEFENDANT'),
                    '281' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO QUASH DEPOSITION'),
                    '282' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO QUASH SUBPOENA'),
                    '283' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SEAL THE RECORD'),
                    '284' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SET ASIDE COMPROMISE AND RELEASE'),
                    '285' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SET ASIDE STIPULATIONS'),
                    '286' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO SUSPEND PROCEEDINGS'),
                    '287' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO UNSEAL THE RECORD'),
                    '288' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PETITION TO VACATE SUBMISSION'),
                    '289' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'POINTS AND AUTHORITIES-TRIAL BRIEF-HEARING BRIEF'),
                    '290' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PROOF OF SERVICE'),
                    '291' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PROPOSED ORDER/AWARD'),
                    '292' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'PROPOSED STIPULATION AWARD AND/OR ORDER'),
                    '293' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'REQUEST FOR CONTINUANCE'),
                    '294' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'REQUEST FOR ORDER TAKING OFF CALENDAR'),
                    '295' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'REQUEST TO WITHDRAW PETITION FOR RECONSIDERATION'),
                    '296' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'REQUEST TO WITHDRAW PETITION FOR REMOVAL'),
                    '297' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'RESPONSE TO PETITION'),
                    '298' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'STIPULATION WITH AWARD (DEATH)'),
                    '299' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'STIPULATION WITH REQUEST FOR AWARD'),
                    '300' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'STIPULATION WITH REQUEST FOR AWARD POST 1-1-2013'),
                    '301' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'SUBSTITUTION OF ATTORNEY'),
                    '302' => array('category' => 'SAU', 'title' => 'LEGAL DOCS','val'=>'WITHDRAWAL OF DECLARATION OF READINESS TO PROCEED'),
                    '303' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'4903.8 (A) (B) ASSIGNMENT'),
                    '304' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'4903.8 (D) DECLARATION'),
                    '305' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'BILLS-OTHER'),
                    '306' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'CONFIRMATION OF LIEN FILING FEE'),
                    '307' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'ITEMIZED STATEMENT'),
                    '308' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'MEDICAL BILLS'),
                    '309' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'NOTICE AND REQUEST FOR ALLOWANCE OF LIEN'),
                    '310' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'ORIGINAL BILL'),
                    '311' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'REQUEST FOR WITHDRAWAL OF LIEN'),
                    '312' => array('category' => 'SAU', 'title' => 'LIENS AND BILLS','val'=>'SUPPLEMENTAL LIEN FORM AND SECTION 4903.05(c) DECLARATION'),
                    '313' => array('category' => 'SAU', 'title' => 'MEDICAL DOCS','val'=>'ALL MEDICAL REPORTS'),
                    '314' => array('category' => 'SAU', 'title' => 'MEDICAL DOCS','val'=>'AME REPORTS'),
                    '315' => array('category' => 'SAU', 'title' => 'MEDICAL DOCS','val'=>'P&S REPORT'),
                    '316' => array('category' => 'SAU', 'title' => 'MEDICAL DOCS','val'=>'QME REPORTS'),
                    '317' => array('category' => 'SAU', 'title' => 'MEDICAL DOCS','val'=>'TREATING PHYSICIAN'),
                    '318' => array('category' => 'SAU', 'title' => 'MISC','val'=>'CORRESPONDENCE-OTHER'),
                    '319' => array('category' => 'SAU', 'title' => 'MISC','val'=>'SUBPOENA'),
                    '320' => array('category' => 'SAU', 'title' => 'MISC','val'=>'SUBPOENA DUCES TECUM'),
                    '321' => array('category' => 'SAU', 'title' => 'MISC','val'=>'TRANSMITTAL LETTER'),
                    '322' => array('category' => 'SAU', 'title' => 'MISC','val'=>'TYPED OR WRITTEN LETTER')
                    
                    

            )
        )
);
