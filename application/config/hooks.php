<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/
error_reporting(0);

$pageurl=  explode('/', $_SERVER['PHP_SELF']);

if((basename($_SERVER['PHP_SELF'])=='prospects') || (basename($_SERVER['PHP_SELF'])=='dashboard') || (basename($_SERVER['PHP_SELF'])=='calendar') || ($pageurl[3] == 'case_details') || (basename($_SERVER['PHP_SELF'])=='rolodex') || (basename($_SERVER['PHP_SELF'])=='eams_lookup') || (basename($_SERVER['PHP_SELF'])=='chat') || (basename($_SERVER['PHP_SELF'])=='reports') || (basename($_SERVER['PHP_SELF'])=='tasks'))

{
    $hook['display_override'][] = array(
            'class' => '',
            'function' => 'compress',
            'filename' => 'compress.php',
            'filepath' => 'hooks'
            );
}

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */