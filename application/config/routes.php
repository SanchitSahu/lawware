<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = 'my404';

$route['admin'] = "admin/admin";
$route['admin/login'] = "admin/admin/login";
$route['admin/timeclock'] = "admin/admin/timeclock";
$route['admin/timeclockdata'] = "admin/admin/timeclockdata";
$route['admin/dashboard'] = "admin/admin/dashboard";
$route['admin/forcelogout'] = "admin/admin/forcelogout";
$route['admin/forceuseroff'] = "admin/admin/forceuseroff";
$route['admin/logout'] = "admin/admin/logout";
$route['admin/forgotpass'] = "admin/admin/forgotpass";
$route['admin/passchange'] = "admin/admin/passchange";
$route['admin/resetpassword'] = "admin/admin/resetpassword";
$route['admin/firm'] = "admin/admin/firm";
$route['admin/updatefirm'] = "admin/admin/updatefirm";
$route['admin/general'] = "admin/admin/general";
$route['admin/drives'] = "admin/admin/drives";
$route['admin/newcases'] = "admin/admin/newcases";
$route['admin/security'] = "admin/admin/security";
$route['admin/function'] = "admin/admin/adminfunction";
$route['admin/image'] = "admin/admin/image";
$route['admin/popup'] = "admin/admin/popup";
$route['admin/caseactivity'] = "admin/admin/caseactivity";
$route['admin/colors'] = "admin/admin/colors";
$route['admin/calendar'] = "admin/admin/calendar";
$route['admin/screen'] = "admin/admin/screen";
$route['admin/clientcard'] = "admin/admin/clientcard";
$route['admin/program'] = "admin/admin/program";
$route['admin/forms'] = "admin/admin/forms";
$route['admin/injuries'] = "admin/admin/injuries";
$route['admin/holidays'] = "admin/admin/holidays";
$route['admin/tasksmacro'] = "admin/admin/tasksmacro";
$route['admin/rolodexudf'] = "admin/admin/rolodexudf";
$route['admin/injuryudf'] = "admin/admin/injuryudf";
$route['admin/clientcardudf'] = "admin/admin/clientcardudf";
$route['admin/injuryudfview'] = "admin/admin/injuryudfview";
$route['admin/prospects'] = "admin/admin/prospects";
$route['admin/reports'] = "admin/admin/reports";
//superadmin
$route['admin/checklist'] = "admin/admin/checklist";
$route['admin/updatechklist'] = "admin/admin/updatechklist";
$route['admin/featureRequest'] = "admin/admin/featureRequest";
$route['admin/saveRequest'] = "admin/admin/saveRequest";
$route['admin/rolodexsecurity'] = "admin/admin/rolodexsecurity";
$route['admin/staff'] = "admin/admin/staff";
$route['admin/getAjaxStaffDetails'] = "admin/admin/getAjaxStaffDetails";
$route['admin/deleteAjaxStaff'] = "admin/admin/deleteAjaxStaff";
$route['admin/addstaff'] = "admin/admin/addstaff";
$route['admin/editstaff'] = "admin/admin/editstaff";
$route['admin/changeStatus'] = "admin/admin/changeStatus";
$route['admin/changeCaptionAccess'] = "admin/admin/changeCaptionAccess";
$route['admin/group_profile'] = "admin/admin/group_profile";
$route['admin/group_general'] = "admin/admin/group_general";
$route['admin/edit_general/(:num)'] = "admin/admin/edit_general";
$route['admin/addGroup'] = "admin/admin/addGroup";
$route['admin/editGroup'] = "admin/admin/editGroup";
$route['admin/global_reassign'] = "admin/admin/global_reassign";
$route['admin/globalReassign/caseReassign'] = "admin/globalReassign/caseReassign";
$route['admin/globalReassign/updatecases'] = "admin/globalReassign/updatecases";
$route['admin/bug_report'] = "admin/admin/bug_report";
$route['admin/bug_report_list'] = "admin/admin/bug_report_list";
$route['admin/groupmanagement'] = "admin/admin/groupmanagement";
$route['admin/groupmembermanagement'] = "admin/admin/groupmembermanagement";
$route['admin/deletegroup'] = "admin/admin/deletegroup";
$route['admin/deletegroupmember'] = "admin/admin/deletegroupmember";
$route['admin/storegroupdata'] = "admin/admin/storegroupdata";
$route['admin/storegroupmemberdata'] = "admin/admin/storegroupmemberdata";
$route['admin/checkduplicatevalue'] = "admin/admin/checkduplicatevalue";
$route['admin/send_bug_report'] = "admin/admin/send_bug_report";
$route['admin/view_bug_report'] = "admin/admin/view_bug_report" ;
$route['admin/taskReassign'] = "admin/admin/taskReassign";
$route['cases/tasks/(:num)'] = "tasks/index/$1";
$route['email/emailattaupload'] = "email/emailattaupload";
$route['email_new/emailattaupload'] = "email_new/emailattaupload";
$route['email/emailattaremove'] = "email/emailattaremove";
$route['email_new/emailattaremove'] = "email_new/emailattaremove";
$route['cases/prospects/(:num)'] = "prospects/index/$1";
$route['injury/(:num)'] = "injury/index/$1";
$route['prospects/(:num)'] = "prospects/index/$1";
$route['calendar/(:num)'] = "calendar/index/$1";
$route['cases/tasks_new/(:num)'] = "tasks_new/index/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */