
<html>
<head>
<style>
.container {
  width:100%;
}
.row {
  width: 100%;
  margin-top: 10px;
}
table{
    width: 100%;
}
td.border_line {
    border-bottom: 1px solid #000;
}
td {
    line-height: 25px;
    white-space: normal;
    vertical-align: baseline;
    padding-left: 5px;
}
td#first {
    width: 120px;
}
td#third{
    width: 300px;
    
}
td#body{
    width: 100%;
    
}
html, body { height: auto; }
a[href]:after { container: none !important; }
</style>
</head>
<body>
<div class="container" id="container">
  <div class="mail-box-header">
             <div class="mail-tools tooltip-demo m-t-md">
                    <h3><span class="font-normal">Subject: </span><span id="emailSubject"><?php echo $emails->subject; ?></span></h3>
                    <h5>
                        <span class="pull-right font-normal" id="emailDatetime"><?php echo date('H:mA d M Y',strtotime($emails->datesent)); ?></span>
                        <span class="font-normal">From: </span><span id="emailfromName"><?php echo $emails->whoto_name.'('.$emails->whofrom.')'; ?></span>
                    </h5>
                </div>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <pre id="emailBody">
                        <?php echo $emails->mail_body; ?>
                    </pre>
                </div>
     </div>  
    
    
</div>
</body>
</html>
