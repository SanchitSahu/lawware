<style>
     .category-list li {
    display: inline-block !important;
}
</style>
<div class="wrapper wrapper-content">

        <div class="emailHeader">
        <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Email</span>
            </div>
        </div>
        <?php //$this->load->view('email/email_menu'); ?>
        <div class="row">
        <div class="col-lg-12">

                <div class="searchFiltr" id="searchFiltr">

                    <div class="col-lg-6"><label>Email Type:</label>
                    <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="interemails" name="emailtypes" id="emailtypes" style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> Internal</label></div>
                    <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="extemails" name="emailtypes" id="emailtypes" style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> External</label></div>
                    </div>

                    <div class="col-lg-6"><label>Status:</label>
                    <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="withoutcase" name="casetypes" id="casetypes" style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> Without Case</label></div>
                    <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="Withcase" name="casetypes" id="casetypes" style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                            </div> <i></i> With Case</label></div>
                    <button class="btn btn-white" title="Filter Email" onclick="get_email_data()"><i class="fa fa-filter"></i></button>
                </div>
            </div>
        </div>
        </div>
        <div class="row">
        <div class="col-lg-12 marg-top10">
            <div class="col-lg-3">
            <button class="btn btn-white btn-sm mark_as_read" data-toggle="tooltip" data-placement="bottom" title="Mark as read"><i class="fa fa-eye fa"></i></h5></button>
            <button class="btn btn-white btn-sm mark_as_old" data-toggle="tooltip" data-placement="bottom" title="Send to Old"><i class="fa fa-angle-double-right"></i></h5></button>
            <button class="btn btn-white btn-sm mark_as_imp" data-toggle="tooltip" data-placement="bottom" title="Mark as urgent"><i class="fa fa-exclamation"></i> </button>
            <button class="btn btn-white btn-sm move_to_trash" data-toggle="tooltip" data-placement="bottom" title="Move to trash"><i class="fa fa-trash-o"></i> </button>
            </div>
            <div class="col-lg-2 pull-right">
                <div class="btn-group">
                    <button class="btn btn-white btn-sm"><span class="start_mail"></span> - <span  class="end_mail"></span> of <span class="total_mail"></span></button>
                    <button class="btn btn-white btn-sm prev_mails" disabled=true><i class="fa fa-arrow-left"></i></button>
                    <button class="btn btn-white btn-sm next_mails" disabled=true><i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
            <div class="col-lg-3">
            <ul class="category-list" style="padding: 0; list-style: none;">
                    <!--li><a href="javascript:;" data-listing="read" title="Inbox"><i class="fa fa-circle text-warning"></i> New </a></li-->
                    <li><a href="javascript:;" data-listing="urgent" title="Urgent"><i class="fa fa-circle text-danger"></i> Urgent </a></li>
                    <li><a href="javascript:;" data-listing="unread" title="Unread"><i class="fa fa-circle text-navy"></i> Unread </a></li>
        </ul>
        </div>

            <div class="col-lg-2 pull-right">
                <div class="file-manager">
                <a class="btn btn-block btn-primary compose-mail" href="javascript:;" title="Compose Mail">Compose Mail</a>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>

       <div class="row">
        <div class="col-lg-12 animated fadeInRight changeContentDiv emailListing">


            <div class="mail-box">
                <ul id="task-navigation" class="nav nav-pills">
                    <li id="new" class="whoto active"><a onclick="getNewEmail()" ><span class="glyphicon glyphicon-home"></span>  New</a></li>
                    <li id="old" class="whofrom"><a onclick="getOldEmail()" ><span class="glyphicon glyphicon-user"></span> Old</a></li>
                    <li id="sent" class="whofrom"><a onclick="getSentEmail()" ><span class="glyphicon glyphicon-user"></span> Sent</a></li>
                </ul>

                <div class="ibox float-e-margins">

                <div class="ibox-content marg-bot15">
                    <div class="table-responsive">
                        <table class="table table-hover table-mail">
                            <div style="margin: 0 0 10px 12px;" id="selectall_div">
                                <input type="checkbox" class="i-checks" class="selector all" id='selectall'>Select all
                            </div>
                            <tbody>
                                <?php foreach ($received_emails as $email) {
                                $ecount = ($email->ethread > 1) ? '&nbsp;('.$email->ethread.')' :''; ?>
                                    <tr class="<?php echo ($email->readyet == 'Y') ? 'read' : 'unread'; ?>">
                                        <td style="width: 10%;" class="check-mail">
<!--                                            <input type="checkbox" class="i-checks" value="<?php echo $email->filename; ?>">-->
                                            <label class="container-checkbox">
                                                <input type="checkbox" value="<?php echo $email->filename; ?>">
                                                <span class="checkmark"  style="padding-right: 5px;"></span>
                                            </label>
                                        </td>
                                        <td style="width: 20%;" class="mail-ontact">
                                        <?php if ($email->urgent == 'Y') { ?>
                                                <i class="fa fa-circle text-danger"></i>
                                            <?php } elseif ($email->readyet != 'Y') { ?>
                                                <i class="fa fa-circle text-navy"></i>
                                            <?php } else {?>
                                                    <i class="fa fa-circle text-warning"></i>
                                            <?php }?>
                                            <a href="javascript:;" class="read-email" data-filename="<?php echo $email->filename; ?>" email_con_id="<?php echo $email->email_con_id; ?>" ethread="<?php echo $email->ethread > 1 ? $email->ethread : 0;?>" iore="<?php echo $email->iore; ?>"><?php echo (isset($email->whofrom_name) ? $email->whofrom_name : ucfirst(strstr($email->whofrom,'@',true))) .$ecount;  ?></a>
                                        </td>
                                        <td style="width: 50%;" class="mail-subject">
                                            <a href="javascript:;" class="read-email" data-filename="<?php echo $email->filename; ?>" email_con_id="<?php echo $email->email_con_id; ?>" ethread="<?php echo $email->ethread > 1 ? $email->ethread : 0;?>" iore="<?php echo $email->iore; ?>"><?php echo $email->subject; ?><span class="pull-right">
                                    <?php   $directory = FCPATH.'/assets/attachment/'.$email->filename;
                                        if (is_dir($directory) && count(glob($directory."/*")) > 0)
                                        { ?>
                                            <i class="fa fa-paperclip"></i>
                                            <?php } ?>
                                            </span></a>
                                        </td>
                                        <td style="width: 20%;" class="text-right mail-date"><?php echo date('M d, Y', strtotime($email->datesent)); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            </div>
        </div>
        <div class="col-lg-12 animated fadeInRight changeContentDiv emailDetailDiv" style="display: none;">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                     <a href="javascript:;" class="btn btn-white btn-sm back_email"><i class="fa fa-arrow-left"></i> Back</a>
                     <a class="btn btn-sm btn-white move_to_old_one" email_con_id="" href="javascript:;"><i class="fa fa-angle-double-right" style="display: none;"></i>Move to old</a>
                    <a class="btn btn-sm btn-white reply_all_compose_email" email_con_id="" href="javascript:;"><i class="fa fa-reply" style="display: none;"></i> Reply All</a>
                    <a href="javascript:;" class="btn btn-white btn-sm reply_compose_email" data-toggle="tooltip" data-placement="bottom" title="Reply"><i class="fa fa-reply"></i> Reply</a>
                    <a href="javascript:;" class="btn btn-sm btn-white forward_email" data-toggle="tooltip" data-placement="bottom" title="Forward"><i class="fa fa-arrow-right"></i> Forward</a>
                    <a href="javascript:;" class="btn btn-white btn-sm print_email" data-toggle="tooltip" data-placement="bottom" title="Print email"><i class="fa fa-print"></i> </a>
                    <a href="javascript:;" class="btn btn-white btn-sm move_to_trash_one" data-toggle="tooltip" data-placement="bottom" title="Move to trash"><i class="fa fa-trash-o"></i> </a>
                    <a href="javascript:;" class="btn btn-danger btn-sm back_email"><i class="fa fa-times"></i> Close</a>
                </div>
                <h2>
                    View Message
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <h3><span class="font-normal">Subject: </span><span id="emailSubject"></span></h3>
                    <h5>
                        <span class="pull-right font-normal" id="emailDatetime"></span>
                        <span class="font-normal" id="sentto"></span>
                        <span class="font-normal">From: </span><span id="emailfromName"></span>
                        <span class="font-normal" id="inboxto"></span>
                    </h5>
                    <span class="pull-right" id="gotocase" style="margin-top:7px !important;"> </span>
                </div>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <pre id="emailBody">

                    </pre>
                    <div id="file_with_attachment">
                    </div>
                </div>
<!--                <div class="mail-attachment">
                    <p>
                        <span><i class="fa fa-paperclip"></i> 1 attachments - </span>
                        <a href="javascript:;">Download all</a> | <a href="javascript:;">View all images</a>
                    </p>
                    <div class="attachment">
                        <div class="file-box">
                            <div class="file">
                                <a href="javascript:;">
                                    <span class="corner"></span>

                                    <div class="image">
                                        <img alt="image" class="img-responsive" src="<?php echo base_url('assets/img/p1.jpg'); ?>">
                                    </div>
                                    <div class="file-name">
                                        Italy street.jpg
                                        <br/>
                                        <small>Added: Jan 6, 2014</small>
                                    </div>
                                </a>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>-->
                <input type="hidden" name="filenameVal" id="filenameVal">
                <input type="hidden" name="reply_iore" id="reply_iore">

                <div class="mail-body text-right tooltip-demo">
                    <a class="btn btn-sm btn-white move_to_old_one" email_con_id="" href="javascript:;"><i class="fa fa-angle-double-right" style="display: none;"></i>Move to old</a>
                    <a class="btn btn-sm btn-white reply_all_compose_email" email_con_id="" href="javascript:;"><i class="fa fa-reply" style="display: none;"></i> Reply All</a>
                    <a class="btn btn-sm btn-white reply_compose_email" href="javascript:;"><i class="fa fa-reply"></i> Reply</a>
                    <a class="btn btn-sm btn-white forward_email" href="javascript:;"><i class="fa fa-arrow-right"></i> Forward</a>
                    <button class="btn btn-sm btn-white print_email" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Print"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-sm btn-white move_to_trash_one" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Trash"><i class="fa fa-trash-o"></i> Remove</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-12 animated fadeInRight changeContentDiv composeEmailDiv no-padding" style="display: none;">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <!--<a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->
<!--                    <a href="javascript:;" class="btn btn-danger btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>-->
                </div>
                <h2 class="compose_title">Compose Email</h2>
            </div>
            <div class="mail-box">
                <form class="form-horizontal" id="send_mail_form" name="send_mail_form" enctype="multipart/form-data">
                    <div class="mail-body">
                        <div class="form-group"><label class="col-sm-2 control-label">To:</label>
                            <div class="col-sm-10">
                                <select name="whoto" id="whoto"   multiple>
                                    <!-- <option value="">-- Select User to Send Email --</option> -->
                                    <?php foreach ($staff_list as $staff) { ?>
                                        <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">External Parties:</label>
                            <div class="col-sm-10">
                                <input type="text" name="emails" id="emails" style="font-size:13px !important; color:#000 !important;" placeholder="Add Multiple External Parties (Comma-Separated)" class="form-control">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Subject:</label>
                            <div class="col-sm-10">
                                <input type="text" name="mail_subject" id="mail_subject" class="form-control">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Urgent:</label>
                            <div class="col-sm-3">
                                <input type="checkbox" name="mail_urgent" id="mail_urgent" value="Y" class="i-checks">
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <label class="col-sm-4 control-label">Case No:</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="case_no" id="case_no" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mail-text h-200">
                        <textarea id="mailbody" class="mail-body" name="mail-body"></textarea>
                        <div class="clearfix"></div>
                    </div>
                    <!-- <div class="col-sm-10">
                        <div id="dragAndDropFiles" class="uploadArea">
                            <h1>Drag &amp; Drop Files Here</h1>
                        </div> </div> -->
                    <div class="mail-body text-right tooltip-demo">
                    <label class="col-sm-3 control-label">Attachment :</label>
                    <div class="col-sm-5"> <input type="file" name="afiles[]" id="afiles" multiple ></div>
                        <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send"><i class="fa fa-reply"></i> Send</button>
                        <a href="javascript:;" class="btn btn-danger btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                        <!--<a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" name="mailType" id="mailType">
                </form>
            </div>
        </div>
        <div class="col-lg-9 animated fadeInRight email_conversation" style="display: none;">

        </div>
    </div>
</div>
<script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>

<script type="text/javascript">


    var start = 0;
    var length = <?php echo DEFAULT_LISTING_LENGTH; ?>;
    var total = <?php echo $unread_count; ?>;
    var allEmail = <?php echo $newemail_cnt; ?>;
    //var current_list = 'inbox';
    var current_list = $("#task-navigation li.active").attr('id');

    function show_hide_navigation_buttons() {
        $('.start_mail').html(start+1);
        if (total > 20) { $('.end_mail').html(start + length); } else {
        $('.end_mail').html(start + total); }
        $('.total_mail').html(allEmail);
        if (start == 0) {
            $('.prev_mails').attr('disabled', true);
            if (total > start + length) {
                $('.next_mails').attr('disabled', false);
            } else {
                $('.next_mails').attr('disabled', true);
            }
        } else {
            $('.prev_mails').attr('disabled', false);
            if (total > start + length) {
                $('.next_mails').attr('disabled', false);
            } else {
                $('.next_mails').attr('disabled', true);
                $('.end_mail').html(total);
            }
        }
    }

    function show_hide_main_window(screen) {
        if (screen == 'email_detail') {
            $('.changeContentDiv').hide();
            $('.email_conversation').hide();
            $('.emailDetailDiv').show();
        } else if (screen == 'email_listing') {
            $('.changeContentDiv').hide();
            $('.email_conversation').hide();
            $('.emailListing').show();
        } else if (screen == 'email_compose') {
            $('.changeContentDiv').hide();
            $('.email_conversation').hide();
            $('.composeEmailDiv').show();
        } else if (screen == 'email_conversation') {
            $('.changeContentDiv').hide();
            $('.email_conversation').show();
        }
    }

    function generate_tr_html(item) {
        var readClass = ""
        var who_or_from = ""
        if (current_list == 'sent') {
            readClass = "read";
            who_or_from = (item.whoto_name !=null) ? item.whoto_name : item.whoto;
        } else {
            readClass = (item.readyet == 'Y') ? 'read' : 'unread';
            who_or_from = (item.whofrom_name !=null) ? item.whofrom_name : item.whofrom;
        }
        var html = "";
        var ecount = (item.ethread > 1) ? '&nbsp;('+item.ethread+')' :'';
        html += '<tr class="' + readClass + '">';
        html += '<td style="width: 10%;" class="check-mail">';
        html += '<input type="checkbox" class="i-checks" value="' + item.filename + '">';
        html += '</td>';
        html += '<td style="width: 20%;" class="mail-ontact">';
        if (item.urgent == 'Y' && current_list != 'sent') {
            html += '<i class="fa fa-circle text-danger"></i>&nbsp;';
        } else if (item.readyet != 'Y' && current_list != 'sent') {
            html += '<i class="fa fa-circle text-navy"></i>&nbsp;';
        } else {
            if(current_list != 'sent'){
            html += '<i class="fa fa-circle text-warning"></i>&nbsp;';
            }
        }
        html += '<a class="read-email" href="javascript:;" data-filename="' + item.filename + '" email_con_id="'+ item.email_con_id +'" ethread="'+((item.ethread > 1) ? item.ethread : 0) +'" iore="'+item.iore+'" >' + ((item.whofrom_name == null) ? item.whofrom.split('@')[0] : who_or_from) +ecount+  '</a>';
        /*if (item.urgent == 'Y') {
            html += ' <i class="fa fa-circle text-danger"></i>';
        }*/
        html += '</td>';
        html += '<td style="width: 50%;" class="mail-subject">';
        html += '<a class="read-email" href="javascript:;" data-filename="' + item.filename + '" email_con_id="'+ item.email_con_id +'" ethread="'+((item.ethread > 1) ? item.ethread : 0) +'" iore="'+item.iore+'">' + item.subject + '<span class="pull-right">';
        if(item.attachment_file !=0){
        html += '<i class="fa fa-paperclip"></i>';
        }
        html += '</span></a>';
        html += '</td>';
        html += '<td style="width: 20%;" class="text-right mail-date">' + moment(item.datesent).format('MMM DD, YYYY') + '</td>';
        html += '</tr>';
       // console.log(html);
        return html;
    }

    $(document).ready(function () {

        $.fn.dataTableExt.errMode = 'ignore';

        $('.i-checks').iCheck({
             radioClass: 'iradio_square-green',
        });
        $('.collapse').collapse('toggle');
        show_hide_navigation_buttons();
        $('select[name="whoto"]').select2({placeholder: 'Select User to Send Email', multiple: true,selectOnClose: true}).on('change', function () {
            $('select[name="whoto"]').valid();
        });

        CKEDITOR.replace('mailbody');
       if($('#emailtypes:checked').val())
            emailType = $('#emailtypes:checked').val();
        else
            emailType = '';
        if($('#casetypes:checked').val())
            caseType = $('#casetypes:checked').val();
        else
            caseType = '';
        $('#send_mail_form').submit(function() {

                $('select[name="whoto"]').parent().find('#wvalid').remove();
                $('input[name="emails"]').parent().find('span').remove();
                $('#mail_subject').parent().find('span').remove();
                var whoto = [];
                whoto = $('select[name="whoto"]').val();
                var subject = $('#mail_subject').val();
                var urgent = $('input[name="mail_urgent"]:checked').val();
                var ext_emails = $('input[name="emails"]').val();
                var mailbody = CKEDITOR.instances.mailbody.getData();
                var filenameVal = $("#filenameVal").val();
                var mailType = $("#mailType").val();
                var case_no = $("#case_no").val();
                var vaild_data = true;
                if(whoto == null && ext_emails == '')
                {
                    $('select[name="whoto"]').parent().append('<span id="wvalid" style="color:red;">Please Select a User to send mail to</span>');
                    $('input[name="emails"]').parent().append('<span style="color:red;">Please add external party to send email </span>');
                    setTimeout(function() {
                    $('select[name="whoto"]').parent().find('#wvalid').remove();
                    $('input[name="emails"]').parent().find('span').remove();
                    }, 5000);
                    vaild_data = false;
                }
                if(ext_emails !='')
                {
                   vaild_data = validate_ext_email($("input[name=emails]").val());
                }
                if (subject == '')
                {
                    $('#mail_subject').parent().append('<span style="color:red;">Please enter a Mail Subject</span>');
                    setTimeout(function() {
                    $('#mail_subject').parent().find('span').remove();
                    }, 5000);
                    vaild_data = false;
                }

                var form_data = new FormData();
                form_data.append("whoto",whoto);
                form_data.append("subject",subject);
                form_data.append("urgent",urgent);
                form_data.append("ext_emails",ext_emails);
                form_data.append("mailbody",mailbody);
                form_data.append("mailType",mailType);
                form_data.append("filenameVal",filenameVal);
                form_data.append("caseNo",case_no);
                var ins = $('input#afiles')[0].files.length;

                for (var x = 0; x < ins; x++) {
                    vaild_data = validateAttachFileExtension($('#afiles')[0].files[x].name);
                    form_data.append("files_"+x, $('#afiles')[0].files[x]);
                }

                if(case_no != '' && case_no !=0){


                    $.ajax({
                        url:'<?php echo base_url(); ?>prospects/CheckCaseno',
                        type: "post",
                        deferRender: true,
                        serverSide: true,
                        data:{  pro_caseno: case_no  },

                        success: function (data) {
                            var resp =  $.parseJSON(data);
                            if(resp == false)
                            {
                                swal({
                                    title: "Alert",
                                    text: "Please enter valid case number.",
                                    type: "warning"
                                });
                                vaild_data = false;
                            }
                            else if(resp == true)
                            {
                                if (vaild_data == true){
                                    $.ajax({
                                        url: '<?php echo base_url(); ?>email/send_email',
                                        method: 'POST',
                                        dataType: 'json',
                                        deferRender: true,
                                        serverSide: true,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data : form_data,

                                        beforeSend: function (xhr) {
                                            $.blockUI();
                                        },
                                        complete: function (jqXHR, textStatus) {
                                            $.unblockUI();
                                        },
                                        success: function (data) {
                                            if (data.result == "true") {
                                                swal({
                                                    title: "Success!",
                                                    text: "Mail is successfully sent.",
                                                    type: "success"
                                                });
                                            }
                                            else if(data.result == "false" && data.ext_party=='0')
                                            {
                                                swal({
                                                    title: "Alert",
                                                    text: "Oops! Something went wrong. Email wasn't send to External Party.",
                                                    type: "warning"
                                                });
                                                return false;
                                            }
                                            else if(data.result == "false" && data.attachment_issue !='')
                                            {
                                                swal({
                                                    title: "Alert",
                                                    text: "Oops! " + data.attachment_issue,
                                                    type: "warning"
                                                });
                                                return false;
                                            }
                                             else {
                                                swal({
                                                    title: "Alert",
                                                    text: "Oops! Some error has occured. Please try again.",
                                                    type: "warning"
                                                });
                                                 return false;
                                            }
                                            $('.inbox').trigger("click");
                                        }
                                    });
                                }
                            }
                        }
                   });
                }
                else
                {   if (vaild_data == true){
                    $.ajax({
                        url: '<?php echo base_url(); ?>email/send_email',
                        method: 'POST',
                        dataType: 'json',
                        cache: false,
                        deferRender: true,
                        serverSide: true,
                        contentType: false,
                        processData: false,
                        data : form_data,

                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data.result == "true") {
                                swal({
                                    title: "Success!",
                                    text: "Mail is successfully sent.",
                                    type: "success"
                                });
                            }
                            else if(data.result == "true" && data.ext_party=='0')
                            {
                                swal({
                                    title: "Alert",
                                    text: "Oops! Something went wrong. Email wasn't send to External Party.",
                                    type: "warning"
                                });
                                 return false;
                            }
                            else if(data.result == "false" && data.attachment_issue !='')
                            {
                                swal({
                                    title: "Alert",
                                    text: "Oops! " + data.attachment_issue,
                                    type: "warning"
                                });
                                return false;
                            }
                             else {
                                swal({
                                    title: "Alert",
                                    text: "Oops! Some error has occured. Please try again.",
                                    type: "warning"
                                });
                                 return false;
                            }
                            $('.inbox').trigger("click");
                        }
                    });
                  }

                }
            return false;
         });

        $('.discard_message').click(function (e) {
            swal({
                title: "Are you sure?",
                text: "The changes you made will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, discard it!",
                closeOnConfirm: true
            }, function () {
                $('.changeContentDiv').hide();
                $('.email_conversation').hide();
                $('.emailListing').show();
            });
        });

        $('.mark_as_read').click(function (e) {
            e.preventDefault();
            var filenames = [];
            var filenames_count = 0;
            $.each($('.icheckbox_square-green.checked').parents('tr.unread').find('input'), function (index, item) {
                filenames.push($(item).val());
            });
            filenames_count = filenames.length;
            filenames = filenames.join();

            if (filenames != '') {
                $.ajax({
                    url: '<?php echo base_url('email/mark_as_read'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    deferRender: true,
                    serverSide: true,
                    data: {
                        filenames: filenames
                    },
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.result == "true") {
                            total = data.count - filenames_count;
                            if ((data.unread_count - filenames_count) > 0) {
                                $('.total_mail_count').html((data.unread_count - filenames_count)).show();
                                $('.nav .email_count').html((data.unread_count - filenames_count)).show();
                                $('.mail-box-header .email_count').html('(' + (data.unread_count - filenames_count) + ')').show();

                            } else {
                                $('.total_mail_count').hide();
                            }
                            show_hide_navigation_buttons();
                            $('.icheckbox_square-green.checked').parents('tr.unread').addClass('read').removeClass('unread');
                            $.each($('tbody tr .checked'), function (index, item) {
                                $(item).iCheck('uncheck')
                            });
                        } else if (data.result == "false") {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again.",
                                type: "warning"
                            });
                        }
                        getNewEmail();
                    }
                });
            } else {
                swal({
                    title: "Alerts",
                    text: "Please select atleast one Unread Email to Mark it as Read",
                    type: "warning"
                });
            }
        });

        $('.mark_as_old').click(function (e) {
            e.preventDefault();
            var filenames = [];
            var filenames_count = 0;
            $.each($('.icheckbox_square-green.checked').parents('tr').find('input'), function (index, item) {
                filenames.push($(item).val());
            });
            filenames_count = filenames.length;
            filenames = filenames.join();

            if (filenames != '') {
                swal({
                    title: "Are you sure?",
                    text: "Do you want to move all selected emails to Old folder?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, move it!",
                    closeOnConfirm: true
                  },
                  function(){
                        $.ajax({
                            url: '<?php echo base_url('email/mark_as_old'); ?>',
                            dataType: 'json',
                            type: 'POST',
                            deferRender: true,
                            serverSide: true,
                            data: {
                                filenames: filenames
                            },
                            beforeSend: function () {
                                $.blockUI();
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            success: function (data) {
                                if (data.result == "true") {
                                    total = data.count - filenames_count;
                                    if ((data.unread_count - filenames_count) > 0) {
                                        $('.total_mail_count').html((data.unread_count - filenames_count)).show();
                                        $('.nav .email_count').html((data.unread_count - filenames_count)).show();
                                        $('.mail-box-header .email_count').html('(' + (data.unread_count - filenames_count) + ')').show();

                                    } else {
                                        $('.total_mail_count').hide();
                                    }
                                    show_hide_navigation_buttons();
                                    $('.icheckbox_square-green.checked').parents('tr.unread').addClass('read').removeClass('unread');
                                    $.each($('tbody tr .checked'), function (index, item) {
                                        $(item).iCheck('uncheck')
                                    });
                                } else if (data.result == "false") {
                                    swal({
                                        title: "Alert",
                                        text: "Oops! Some error has occured. Please try again.",
                                        type: "warning"
                                    });
                                }
                                getNewEmail();
                            }
                        });
                     });
            } else {
                swal({
                    title: "Alerts",
                    text: "Please select atleast one unread Email to Mark it as Old",
                    type: "warning"
                });
            }
        });

        $('.mark_as_imp').click(function (e) {
            e.preventDefault();
            var filenames = [];
            var filenames_count = 0;
            $.each($('.icheckbox_square-green.checked').parents('tr').find('input'), function (index, item) {
                filenames.push($(item).val());
            });
            filenames_count = filenames.length;
            filenames = filenames.join();

            if (filenames_count > 0) {
                $.ajax({
                    url: '<?php echo base_url('email/mark_as_urgent'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    deferRender: true,
                    serverSide: true,
                    data: {
                        filenames: filenames
                    },
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.result == "true") {
                            $.each($('.icheckbox_square-green.checked').parents('tr').find('td.mail-ontact'), function (index, item) {
                                if ($(item).find('i.fa.fa-circle.text-danger').length == 0) {
                                    $(item).append(' <i class="fa fa-circle text-danger"></i>');
                                }
                            });

                            $.each($('tbody tr .checked'), function (index, item) {
                                $(item).iCheck('uncheck');
                            });
                        } else if (data.result == "false") {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again.",
                                type: "warning"
                            });
                        }
                    }
                });
            } else {
                swal({
                    title: "Alerts",
                    text: "Please select atleast one email to Mark it as Important",
                    type: "warning"
                });
            }
        });

        $('.refresh').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    total = data.count;
                    allEmail = data.count;
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    } else {
                        $('.total_mail_count').hide();
                        $('.dropdown .email_count').text(data.unread_count).show();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    var listing = "";
                    $.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                    $('input#selectall').iCheck('uncheck');
                }
            });
        });

        $('.inbox').click(function (e) {
            e.preventDefault();
            $('.category-list').show();
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    show_hide_main_window('email_listing');
                    total = data.count;
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                        $('.dropdown .email_count').text(data.unread_count).show();
                    } else {
                        $('.total_mail_count').hide();
                    }
                    if (current_list == 'sent')
                    {
                        $('.total_mail_count').hide();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    var listing = "";
                    $.each(data.result, function (index, item) {
                       listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            });
        });

        $('.sent').click(function (e) {
            e.preventDefault();
            $('.category-list').hide();
            current_list = 'sent';
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    show_hide_main_window('email_listing');
                    total = data.count;
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    } else {
                        $('.total_mail_count').hide();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    var listing = "";
                    $.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            });
        });

        $('.prev_mails').click(function (e) {
            e.preventDefault();
            if($('.category-list a').hasClass('activeli'))
            {
                list_type = $('.activeli').data('listing');
            }
            else
            {
                list_type = '';
            }
            emailType = $('#emailtypes:checked').val(),
            caseType = $('#casetypes:checked').val()
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: start - length,
                    length: length,
                    list_type : list_type,
                    emailType : emailType,
                    caseType : caseType,
                },
                beforeSend: function () {
                    start -= length;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.unread_count > 0 && current_list =='inbox') {
                        $('.total_mail_count').html(data.unread_count).show();
                        if(emailType == 'interemails')
                        {
                            total = data.count;
                            $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                        }

                    }
                    else if(data.withcno_count > 0 && current_list =='withcase') {
                        total = data.withcno_count;
                        $('.total_mail_count').html(data.withcno_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.withoutcno_count > 0 && current_list =='withoutcase') {
                        total = data.withoutcno_count;
                        $('.total_mail_count').html(data.withoutcno_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.internal_count > 0 && current_list =='interemails'){
                        total = data.internal_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.internal_count + ')').show();
                    }
                    else if(data.external_count > 0 && current_list =='extemails'){
                        total = data.external_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.external_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='urgent'){
                        total = data.count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                     else if(data.count > 0 && current_list =='unread'){
                        total = data.unread_count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='read')
                    {
                        total = data.count-data.unread_count;
                    }
                    else if(data.count > 0 && current_list =='new')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentnew_cnt;
                            }

                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadnew_cnt;
                            }

                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.newemail_cnt;
                            }

                        }

                    }
                    else if(data.count > 0 && current_list =='old')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentold_cnt;
                            }
                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadold_cnt;
                            }
                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.oldemail_cnt;
                            }
                        }
                    }
                    else {
                        $('.total_mail_count').hide();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    $('.total_mail').html(total);
                    var listing = "";
                    $.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            })
        });

        $('.next_mails').click(function (e) {
            e.preventDefault();
            if($('.category-list a').hasClass('activeli'))
            {
                list_type = $('.activeli').data('listing');
            }
            else
            {
                list_type = '';
            }
            //alert(list_type);
            emailType = $('#emailtypes:checked').val(),
            caseType = $('#casetypes:checked').val(),
            current_list = $("#task-navigation li.active").attr('id');
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
            serverSide: true,
            data: {
                    current_list: current_list,
                    start: start + length,
                    length: length,
                    list_type : list_type,
                    emailType : emailType,
                    caseType : caseType,
                },
                beforeSend: function () {
                    start += length;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.unread_count > 0 && current_list =='inbox') {
                        total = data.count;
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    }
                    else if(data.withcno_count > 0 && (current_list =='withcase')) {
                        total = data.withcno_count;
                        $('.total_mail_count').html(data.withcno_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.withoutcno_count > 0 && current_list =='withoutcase') {
                        total = data.withoutcno_count;
                        $('.total_mail_count').html(data.withoutcno_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.internal_count > 0 && current_list =='interemails'){
                        total = data.internal_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.internal_count + ')').show();
                    }
                    else if(data.external_count > 0 && current_list =='extemails'){
                        total = data.external_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.external_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='urgent'){
                        total = data.count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                     else if(data.count > 0 && current_list =='unread'){
                        total = data.unread_count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='read')
                    {
                        total = data.count-data.unread_count;
                    }
                    else if(data.count > 0 && current_list =='new')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentnew_cnt;
                            }

                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadnew_cnt;
                            }

                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.newemail_cnt;
                            }

                        }

                    }
                    else if(data.count > 0 && current_list =='old')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentold_cnt;
                            }
                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadold_cnt;
                            }
                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.oldemail_cnt;
                            }
                        }
                    }
                    else {
                        $('.total_mail_count').hide();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    $('.total_mail').html(total);
                    var listing = "";
                    $.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green'
                    });

                }
            });
        });
        $('.compose-mail').click(function (e) {
            $("#mailType").val("");
            $('#send_mail_form')[0].reset();
            $('#cke_1_contents').css('height','130px');
            CKEDITOR.instances['mailbody'].setData("");
            $('#whoto').val([]).trigger("change");
            show_hide_main_window('email_compose');
            $('.compose_title').html('Compose Email');
        });

        $('.category-list a, .tag-list a').click(function (e) {
            list_type = $(this).data('listing');
            $(this).addClass( "activeli" );
            current_list = $("#task-navigation li.active").attr('id');
            $('.category-list').show();
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length,
                    list_type : list_type
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    show_hide_main_window('email_listing');
                    if (data.unread_count > 0 && current_list =='inbox') {
                        total = data.count;
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    }
                    else if(data.withcno_count > 0 && current_list =='withcase'){
                        total = data.withcno_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.withoutcno_count > 0 && current_list =='withoutcase'){
                        total = data.withoutcno_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.internal_count > 0 && current_list =='interemails'){
                        total = data.internal_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.internal_count + ')').show();
                    }
                    else if(data.external_count > 0 && current_list =='extemails'){
                        total = data.external_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.external_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='urgent'){
                        total = data.count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='unread'){
                        total = data.unread_count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='read')
                    {
                        total = data.count-data.unread_count;
                    }
                    else if(data.count > 0 && current_list =='new')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentnew_cnt;
                            }

                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadnew_cnt;
                            }

                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.newemail_cnt;
                            }

                        }

                    }
                    else if(data.count > 0 && current_list =='old')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentold_cnt;
                            }
                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadold_cnt;
                            }
                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.oldemail_cnt;
                            }
                        }
                    }
                    else {
                        $('.total_mail_count').hide();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    $('.total_mail').html(total);
                    var listing = "";
                    $.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            });
        });
        $(document.body).on("click",".back_email",function(){
            show_hide_main_window('email_listing');
            get_email_data();
        });
        $(document.body).on("click", "a.read-email", function () {
            var filename = $(this).data('filename');
            var email_con_id = $(this).attr('email_con_id');
            var ethread = $(this).attr('ethread');
            var iore = $(this).attr('iore');
            var $this = $(this);
            if (ethread > 1){
                $.ajax({
                    url: '<?php echo base_url('email/emailThreads'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    deferRender: true,
                    serverSide: true,
                    data: {
                        current_list: current_list,
                        filename: filename,
                        email_con_id: email_con_id,
                        vfrom :'email_details'
                    },
                    beforeSend: function () {
                        start = 0;
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {

                        $this.parent().parent().after(data);
                        $this.removeClass('read-email');
                        $this.addClass('read-email-close');
                        $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                        });
                        return false;
                    }
                });
            }
            else{
            $.ajax({
                    url: '<?php echo base_url('email/view'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    deferRender: true,
                    serverSide: true,
                    data: {
                        current_list: current_list,
                        filename: filename,
                        iore: iore
                    },
                    beforeSend: function () {
                        start = 0;
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        show_hide_main_window('email_detail');
                        $("#filenameVal").val(filename);
                        $("#reply_iore").val(iore);
                        if (data.unread_count > 0) {
                            $('.total_mail_count').html(data.unread_count).show();
                            $('.email_count').html(data.unread_count).show();
                            $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                        } else {
                            $('.total_mail_count').hide();
                        }
                        if (current_list == 'sent')
                        {
                            $('.total_mail_count').hide();
                        }
                        show_hide_navigation_buttons();
                        $('#emailSubject').html(data.emails.subject);
                        $('#emailDatetime').html(moment(data.emails.datesent).format('hh:mm A DD MMM YYYY'));
                        $('#emailfromName').html(data.emails.whofrom);
                        if (data.emails.whofrom == data.emails.email_con_people){
                            data.emails.email_con_people = '<?php echo $this->session->userdata('user_data')['initials'] ?>';
                        }
                        if(data.emails.caseno !='0')
                        {
                            $('#gotocase').html('');
                            var gtc = '<?php echo base_url();?>cases/case_details/'+data.emails.caseno;
                            $('#gotocase').html('Go To Case : <a href="'+gtc+'" target="_blank" >'+data.emails.caseno+'</a>');
                        }
                        else{
                            $('#gotocase').html('');
                        }
                        if (current_list == 'sent'){
                            $('#inboxto').html('');
                            $('#sentto').html("<br>To: "+data.emails.email_con_people +"<br>");
                        }else{
                            $('#sentto').html('');
                            var inboxto= (data.emails.email_con_people !=null) ? data.emails.email_con_people : data.emails.whoto;
                            $('#inboxto').html("<br><br>To: "+inboxto);
                        }
                        $('.mail-body #emailBody').html(data.emails.mail_body);
                        $('.mail-body #file_with_attachment').html(data.file_with_attachment);
                        $('.move_to_trash_one').attr('data-id',filename);
                        $('.print_email').attr('data-id',filename);
                        if (data.reply_all == 1)
                        {
                            $('.emailDetailDiv .reply_all_compose_email').show();
                            $('.emailDetailDiv .reply_all_compose_email').attr('email_con_id',data.emails.email_con_id);
                        }
                        else{
                            $('.emailDetailDiv .reply_all_compose_email').hide();
                        }
                    }
                });
            }
        });

        $(document.body).on("click", "a.read-email-close", function () {
            //$(this).parent().parent().find('tr.rclose').remove();
            $(this).parents('tr').next('tr.rclose').hide();
            $(this).removeClass('read-email-close');
            $(this).addClass('read-email');
        });
        $(document.body).on("click", '.reply_compose_email',function(){
       //$('.reply_compose_email').click(function (e) {
            $('#whoto').val([]).trigger("change");
            CKEDITOR.instances['mailbody'].setData("");
            var email_con_id = $(this).attr('email_con_id');
            var ethread = $(this).attr('ethread');
            var iore = $("#reply_iore").val();

            var filename = $("#filenameVal").val();
            $('.compose-mail').trigger("click");
            $.ajax({
                url: '<?php echo base_url(); ?>email/reply_email',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    filename: filename,
                    email_con_id: email_con_id,
                    ethread: ethread,
                    iore:iore
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    $("#mailType").val("reply");
                    $('#whoto').val(data.emails[0].whofrom).trigger("change");
                    if(data.emails[0].iore =='E'){$('#emails').val(data.emails[0].whofrom);}
                    $('#mail_subject').val(data.emails[0].subject);
                    $('#mail_urgent').iCheck('uncheck');
                    if (data.emails[0].urgent == 'Y') {
                        //$('#mail_urgent').parent('div').addClass('checked');
                        $('#mail_urgent').iCheck('check');
                    }
                    $('#case_no').val(data.emails[0].caseno);

                    //CKEDITOR.instances['mailbody'].setData(data.emails[0].mail_body)
                }
            });
        });

        $('.move_to_old_one').click(function (e) {
            var filenames = $("#filenameVal").val();
            if (filenames != '') {
                swal({
                    title: "Are you sure?",
                    text: "Do you want to move all selected emails to Old folder?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, move it!",
                    closeOnConfirm: true
                  },
                  function(){
                    $.ajax({
                        url: '<?php echo base_url('email/mark_as_old'); ?>',
                        dataType: 'json',
                        type: 'POST',
                        deferRender: true,
                        serverSide: true,
                        data: {
                            filenames: filenames
                        },
                        beforeSend: function () {
                            $.blockUI();
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data.result == "true") {
                               get_email_data();
                            } else if (data.result == "false") {
                                swal({
                                    title: "Alert",
                                    text: "Oops! Some error has occured. Please try again.",
                                    type: "warning"
                                });
                            }
                        }
                    });
                  });

            } else {
                swal({
                    title: "Alerts",
                    text: "Please select atleast one Unread Email to Mark it as Read",
                    type: "warning"
                });
            }
            //alert(filename);
        });
        $('.forward_email').click(function (e) {
            $('#whoto').val([]).trigger("change");
            CKEDITOR.instances['mailbody'].setData("");
            var iore = $("#reply_iore").val();
            var filename = $("#filenameVal").val();
            $('.compose-mail').trigger("click");
            $.ajax({
                url: '<?php echo base_url(); ?>email/forward_email',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    filename: filename,
                    iore:iore
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    //console.log(data);
                    $("#mailType").val("forward");
                    //$('#whoto').val(data.emails[0].whofrom).trigger("change");
                    $('#mail_subject').val(data.emails[0].subject);
                    if (data.emails[0].urgent == 'Y') {
                        $('#mail_urgent').parent('div').addClass('checked');
                    } else {
                        $('#mail_urgent').parent('div').removeClass('checked');
                    }
                    if (data.emails[0].caseno > 0) {
                        $('#case_no').val(data.emails[0].caseno);
                    }
                    CKEDITOR.instances['mailbody'].setData(data.emails[0].mail_body);
                }
            });
        });

        $('.mail-list-get').click(function (e) {
            var search = $("#search").val();
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    search_data: search,
                    start: 0,
                    length: length
                },
                beforeSend: function () {
                    start = 0;
                    if(search != '')
                        $.blockUI();
                },
                complete: function () {
                    if(search != '')
                        $.unblockUI();
                },
                success: function (data) {
                    show_hide_main_window('email_listing');
                    total = data.count;
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    } else {
                        $('.total_mail_count').hide();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    var listing = "";
                    $.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            });

        });

        $('.menu').click(function (e) {
            var menu_item = $(this).attr("data-menu");
            inbox_send(menu_item);
        });


        $('.move_to_trash').click(function (e) {
            e.preventDefault();
            var filenames = [];
            var filenames_count = 0;
            $.each($('.icheckbox_square-green.checked').parents('tr').find('input'), function (index, item) {
                filenames.push($(item).val());
            });
            filenames_count = filenames.length;
            filenames = filenames.join();
            if (filenames_count > 0) {
                swal({
                  title: "Are you sure?",
                  text: "You will not be able to recover this imaginary file!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  closeOnConfirm: false
                },
                function(){

                $.ajax({
                    url: '<?php echo base_url('email/move_to_trash'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    serverSide: true,
                    data: {
                        filenames: filenames
                    },
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.result == "true") {
                            get_email_data();
                           setTimeout(function() {
                               swal({
                                    title: "Success!",
                                    text: "Your imaginary file has been deleted.",
                                    type: "success"
                                });
                            }, 2000);

                            /* $.ajax({
                                    url: '<?php echo base_url('email/refresh_listing'); ?>',
                                    dataType: 'json',
                                    type: 'POST',
                                    data: {
                                        current_list: current_list,
                                        start: 0,
                                        length: length
                                    },
                                    beforeSend: function () {
                                        start = 0;
                                        $.blockUI();
                                    },
                                    complete: function () {
                                        $.unblockUI();
                                    },
                                    success: function (data) {
                                        //total = data.count;
                                        if (data.unread_count > 0) {
                                            $('.total_mail_count').html(data.unread_count).show();
                                            $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                                        } else {
                                            $('.total_mail_count').hide();
                                        }
                                        inbox_send(current_list);
                                        show_hide_navigation_buttons();
                                        var listing = "";
                                        $.each(data.result, function (index, item) {
                                            listing += generate_tr_html(item);
                                        });
                                        $('.mail-box tbody').html(listing);
                                        $('.i-checks').iCheck({
                                            checkboxClass: 'icheckbox_square-green',
                                            radioClass: 'iradio_square-green',
                                        });
                                    }
                                });*/
                        } else if (data.result == "false") {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again.",
                                type: "warning"
                            });
                        }
                    }
                });

            });

            } else {
                swal({
                    title: "Alerts",
                    text: "Please select atleast one  Email to Delete",
                    type: "warning"
                });
            }


        });

         $('.move_to_trash_one').click(function (e) {
            e.preventDefault();
            var filenames = $('.move_to_trash_one').attr('data-id');
             $.ajax({
                    url: '<?php echo base_url('email/move_to_trash'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    deferRender: true,
                    serverSide: true,
                    data: {
                        filenames: filenames
                    },
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.result == "true") {
                            swal({
                                title: "Alert",
                                text: "Email successfully deleted.",
                                type: "success"
                            });
                           window.location.href = '<?php echo base_url('email'); ?>';
                        } else if (data.result == "false") {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again.",
                                type: "warning"
                            });
                        }
                    }
                });

        });
        $(document.body).on('click', '.print_email', function () {
        var filename = $('.print_email').attr('data-id');
        var iore = $("#reply_iore").val();
        $.ajax({
            url: '<?php echo base_url() . 'email/printEmail' ?>',
            method: "POST",
            deferRender: true,
            serverSide: true,
            //dataType: 'json',
            data: { current_list: current_list,
                    filename: filename,
                    iore:iore },
            success: function (result) {
              $(result).printThis({
                    debug: false,
                    header: "<h2><center>View Message</center></h2>"
                });
            }
        });
    });
       $(document.body).on("click", "a.open-read-email", function () {
        var filename = $(this).data('filename');
            var email_con_id = $(this).attr('email_con_id');
            var ethread = $(this).attr('ethread');
            var iore = $(this).attr('iore');
            var $this = $(this);
            var selec_id = '#collapse'+filename;
            $.ajax({
                    url: '<?php echo base_url('email/view'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    deferRender: true,
                    serverSide: true,
                    data: {
                        current_list: current_list,
                        filename: filename,
                        iore: iore
                    },
                    beforeSend: function () {
                        start = 0;
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        //show_hide_main_window('email_detail');

                        $('#filenameVal').val(filename);
                        $("#reply_iore").val(iore);
                        if (data.unread_count > 0) {
                            $(' .total_mail_count').html(data.unread_count).show();
                            $(' .mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                        } else {
                            $(' .total_mail_count').hide();
                        }
                        if (current_list == 'sent')
                        {
                            $(' .total_mail_count').hide();
                        }
                        show_hide_navigation_buttons();
                        $('body').find(selec_id).find('#emailSubject').html(data.emails.subject);
                        $('body').find(selec_id).find('#emailDatetime').html(moment(data.emails.datesent).format('HH:mmA DD MMM YYYY'));
                        $('body').find(selec_id).find('#emailfromName').html(data.emails.whofrom_name + '(' + data.emails.whofrom + ')');
                        $('body').find(selec_id).find('.mail-body #emailBody').html(data.emails.mail_body);
                        $('body').find(selec_id).find('.mail-body #file_with_attachment').html(data.file_with_attachment);
                        $('body').find(selec_id).find('.move_to_trash_one').attr('data-id',filename);
                        $('body').find(selec_id).find('.print_email').attr('data-id',filename);
                    }
                });
       });
       $(document.body).on("click", '.reply_all_compose_email',function(){
       //$('.reply_compose_email').click(function (e) {
            $('#whoto').val([]).trigger("change");
            CKEDITOR.instances['mailbody'].setData("");
            var email_con_id = $(this).attr('email_con_id');
            var ethread = $(this).attr('ethread');
            var iore = $("#reply_iore").val();

            var filename = $("#filenameVal").val();
            $('.compose-mail').trigger("click");
            $.ajax({
                url: '<?php echo base_url(); ?>email/reply_all_email',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    filename: filename,
                    email_con_id: email_con_id,
                    iore: iore
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    $("#mailType").val("reply");
                    if (data.internal_party.length > 0){
                        //console.log(data.internal_party);
                        $('#whoto').val(data.internal_party).trigger('change');
                    }
                    if(data.external_party !=''){$('#emails').val(data.external_party);}
                    $('#mail_subject').val(data.emails[0].subject);
                    $('#mail_urgent').iCheck('uncheck');
                    if (data.emails[0].urgent == 'Y') {
                        //$('#mail_urgent').parent('div').addClass('checked');
                        $('#mail_urgent').iCheck('check');
                    }
                    $('#case_no').val(data.emails[0].caseno);
                    //$('#case_no').val(data.emails.caseno);

                    //CKEDITOR.instances['mailbody'].setData(data.emails[0].mail_body)
                }
            });
        });
   /* $('#send_mail_form').on('focusout','#emails',function(){
        if ($('input[name="emails"]').val() != ''){
            validate_ext_email($('input[name="emails"]').val());
        }
    });*/
});
    function validate_ext_email(v_ext_emails)
    {
        var emails = '';
        var count_e = 0;
        if (v_ext_emails.indexOf(',') !== -1){
            emails = v_ext_emails.split(",");
            count_e = emails.length;
        }
        var pattern = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (count_e > 0)
        {
            for (i = 0; i < emails.length; i++) {
                if(!pattern.test(emails[i].trim())) {
                    swal({
                        title: "Alert",
                        text: "Oops! Please enter valid email ID.\n"+ emails[i].trim(),
                        type: "warning"
                    });
                    return false;
                }
            }

        }
        else{
             if(!pattern.test(v_ext_emails.trim())) {
                    swal({
                        title: "Alert",
                        text: "Oops! Please enter valid email address.\n"+ v_ext_emails.trim(),
                        type: "warning"
                   });
                return false;
                }
        }
        return true;
    }
        $(document.body).on("ifToggled", 'input#selectall',function(event){
            var chkToggle;
            $(this).is(':checked') ? chkToggle = "check" : chkToggle = "uncheck";
            $('input.selector:not(.all)').iCheck(chkToggle);
            $('.i-checks').iCheck(chkToggle);
        });
    $(document).on('change','input#afiles', function (){
        var ins = $('input#afiles')[0].files.length;
           for (var x = 0; x < ins; x++) {
                validateAttachFileExtension($('#afiles')[0].files[x].name);
            }
    });
    $(document).on('click','i.fa-times-circle', function (){
    var remove_file = $(this).attr('data');
    var $this = $(this);
    swal({
            title:"warning",
            text: "Are you sure to Remove this attachment?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.blockUI();
            $.ajax({
                url: '<?php echo base_url(); ?>email/removeattachment',
                data: {remove_file: remove_file},
                method: "POST",
                deferRender: true,
                serverSide: true,
                success: function (result) {
                    $.unblockUI();
                    var data = $.parseJSON(result);
                    if (data.status == true) {
                        swal("Success !", data.message, "success");
                        $this.parent('li').remove();
                    } else {
                        swal("Oops...", data.message, "error");
                    }
                }
            });
        });

});

    function validateAttachFileExtension(fld_value) {
    var valid_extension = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.gif|\.png|\.txt|\.xlsx|\.xls|\.rtf|\.html|\.htm|\.wpd|\.wps|\.csv|\.ppt|\.pptx|\.zip|\.tar|\.xml|\.XLR|\.mp3|\.rar)$/i;
    if(!valid_extension.test(fld_value)) {
        swal({
                    title: "Alert",
                    text: "Oops! Please provide valid file format.\n" + fld_value + ' is not valid file format',
                    type: "warning"
                });
        return false;
    }
    return true;
}

    function getOldEmail()
    {
            $('#searchFiltr').css("display", "block");
            //current_list = 'inbox';
            emailType = $('#emailtypes:checked').val();
            caseType = $('#casetypes:checked').val();
            current_list = 'old';
            if($('.category-list a').hasClass('activeli'))
            {
                list_type = $('.activeli').data('listing');
            }
            else
            {
                list_type = '';
            }
            $.ajax({
                url: '<?php echo base_url('email/old_email_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length,
                    emailType : emailType,
                    caseType : caseType,
                    list_type :list_type
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    //show_hide_main_window('email_listing');
                    //console.log(data);
                    //total = data.oldemail_cnt;
                    //total = Object.keys(data.result).length;
                    //alert(total);
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    } else {
                        $('.total_mail_count').hide();
                    }

                    if (data.unread_count > 0 && current_list =='inbox') {
                        total = data.count;
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    }
                    else if(data.withcno_count > 0 && current_list =='withcase'){
                        total = data.withcno_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.withoutcno_count > 0 && current_list =='withoutcase'){
                        total = data.withoutcno_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.internal_count > 0 && current_list =='interemails'){
                        total = data.internal_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.internal_count + ')').show();
                    }
                    else if(data.external_count > 0 && current_list =='extemails'){
                        total = data.external_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.external_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='urgent'){
                        total = data.count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='unread'){
                        total = data.unread_count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='read')
                    {
                        total = data.count-data.unread_count;
                    }
                    else if(data.count > 0 && current_list =='new')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentnew_cnt;
                            }

                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadnew_cnt;
                            }

                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.newemail_cnt;
                            }

                        }

                    }
                    else if(data.count > 0 && current_list =='old')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentold_cnt;
                            }
                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadold_cnt;
                            }
                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.oldemail_cnt;
                            }
                        }
                    }
                    else {
                        $('.total_mail_count').hide();
                    }

                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    $('.total_mail').html(total);
                    var listing = "";
                    /*$.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });*/
                    if(total > 0)
                    {
                        $.each(data.result, function (index, item) {
                            listing += generate_tr_html(item);
                        });
                    }
                    else
                    {
                        listing = "No Email Found.";
                        $('#selectall_div').css("display","none");
                    }
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            });
    }

    function getNewEmail()
    {
            $('#searchFiltr').css("display", "block");
            current_list = 'new';
            emailType = $('#emailtypes:checked').val();
            caseType = $('#casetypes:checked').val();
            if($('.category-list a').hasClass('activeli'))
            {
                list_type = $('.activeli').data('listing');
            }
            else
            {
                list_type = '';
            }
            $.ajax({
                url: '<?php echo base_url('email/new_email_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length,
                    emailType : emailType,
                    caseType : caseType,
                    list_type : list_type
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    show_hide_main_window('email_listing');

                    //total = Object.keys(data.result).length;
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    } else {
                        $('.total_mail_count').hide();
                    }

                   if (data.unread_count > 0 && current_list =='inbox') {
                        total = data.count;
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    }
                    else if(data.withcno_count > 0 && current_list =='withcase'){
                        total = data.withcno_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.withoutcno_count > 0 && current_list =='withoutcase'){
                        total = data.withoutcno_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.internal_count > 0 && current_list =='interemails'){
                        total = data.internal_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.internal_count + ')').show();
                    }
                    else if(data.external_count > 0 && current_list =='extemails'){
                        total = data.external_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.external_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='urgent'){
                        total = data.count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='unread'){
                        total = data.unread_count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='read')
                    {
                        total = data.count-data.unread_count;
                    }
                    else if(data.count > 0 && current_list =='new')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentnew_cnt;
                            }

                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadnew_cnt;
                            }

                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.newemail_cnt;
                            }

                        }

                    }
                    else if(data.count > 0 && current_list =='old')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentold_cnt;
                            }
                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadold_cnt;
                            }
                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.newemail_cnt;
                            }
                        }
                    }
                    else {
                        $('.total_mail_count').hide();
                    }
                    inbox_send(current_list);
                    show_hide_navigation_buttons();
                    $('.total_mail').html(total);
                    var listing = "";
                    if(total > 0)
                    {
                        $.each(data.result, function (index, item) {
                            listing += generate_tr_html(item);
                        });
                    }
                    else
                    {
                        listing = "No Email Found.";
                        $('#selectall_div').css("display","none");
                    }

                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            });
    }

    function getSentEmail()
    {
            $('#searchFiltr').css("display", "none");

            current_list = 'sent';
            emailType = $('#emailtypes:checked').val();
            caseType = $('#casetypes:checked').val();
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length,
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    show_hide_main_window('email_listing');
                    total = data.count;
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    } else {
                        $('.total_mail_count').hide();
                    }
                    //inbox_send(current_list);
                    $(".mark_as_read").hide();
                    $(".mark_as_old").hide();
                    $(".mark_as_imp").hide();
                    $(".move_to_trash").show();
                    show_hide_navigation_buttons();
                    $('.total_mail').html(total);
                    var listing = "";
                    $.each(data.result, function (index, item) {
                        listing += generate_tr_html(item);
                    });
                    $('.mail-box tbody').html(listing);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                }
            });
    }

    function get_email_data()
    {
        //alert($('#emailtypes:checked').val());
        var chk = $("#task-navigation li.active").attr('id');
        /*if(chk == 'new')
        {
            getNewEmail();
        }
        if(chk == 'old')
        {
            getOldEmail();
        }
        if(chk == 'sent')
        {
            getSentEmail();
        }*/
        $('#searchFiltr').css("display", "block");
            if($('.category-list a').hasClass('activeli'))
            {
                list_type = $('.activeli').data('listing');
            }
            else
            {
                list_type = '';
            }
            current_list = chk;
            if($('#emailtypes:checked').val())
                emailType = $('#emailtypes:checked').val();
            else
                emailType = '';
            if($('#casetypes:checked').val())
                caseType = $('#casetypes:checked').val();
            else
                caseType = '';
            //alert(caseType);
            $.ajax({
                url: '<?php echo base_url('email/refresh_listing'); ?>',
                dataType: 'json',
                type: 'POST',
                deferRender: true,
                serverSide: true,
                data: {
                    current_list: current_list,
                    start: 0,
                    length: length,
                    emailType : emailType,
                    caseType : caseType,
                    list_type : list_type
                },
                beforeSend: function () {
                    start = 0;
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    show_hide_main_window('email_listing');
                    //total = data.count;
                    total = Object.keys(data.result).length;
                    if (data.unread_count > 0) {
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    } else {
                        $('.total_mail_count').hide();
                    }
                    //alert(emailType);
                    if (data.unread_count > 0 && current_list =='inbox') {
                        total = data.count;
                        $('.total_mail_count').html(data.unread_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                    }
                    else if(data.withcno_count > 0 && (current_list =='withcase')) {
                        total = data.withcno_count;
                        $('.total_mail_count').html(data.withcno_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.withoutcno_count > 0 && current_list =='withoutcase') {
                        total = data.withoutcno_count;
                        $('.total_mail_count').html(data.withoutcno_count).show();
                        $('.mail-box-header .total_mail_count').html('(' + data.withoutcno_count + ')').show();
                    }
                    else if(data.internal_count > 0 && current_list =='interemails'){
                        total = data.internal_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.internal_count + ')').show();
                    }
                    else if(data.external_count > 0 && current_list =='extemails'){
                        total = data.external_count;
                        $('.mail-box-header .total_mail_count').html('(' + data.external_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='urgent'){
                        total = data.count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                     else if(data.count > 0 && current_list =='unread'){
                        total = data.unread_count;
                        //$('.mail-box-header .total_mail_count').html('(' + data.withcno_count + ')').show();
                    }
                    else if(data.count > 0 && current_list =='read')
                    {
                        total = data.count-data.unread_count;
                    }
                    else if(data.count > 0 && current_list =='new')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentnew_cnt;
                            }

                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadnew_cnt;
                            }

                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.new_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.new_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.new_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.new_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.new_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.new_int_wc_cnt;
                            }
                            else
                            {
                                total = data.newemail_cnt;
                            }

                        }

                    }
                    else if(data.count > 0 && current_list =='old')
                    {
                        if(list_type == 'urgent')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.urgentold_cnt;
                            }
                        }
                        else if(list_type == 'unread')
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.unreadold_cnt;
                            }
                        }
                        else
                        {
                            if(emailType == '' && caseType == 'withoutcase')
                            {
                                total = data.withoutcno_count;
                            }
                            else if(emailType == '' && caseType == 'withcase')
                            {
                                total = data.withcno_count;
                            }
                            else if(emailType == 'interemails' && caseType == '')
                            {
                                total = data.old_int_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == '')
                            {
                                total = data.old_ext_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withoutcase')
                            {
                                total = data.old_ext_woc_cnt;
                            }
                            else if(emailType == 'extemails' && caseType == 'withcase')
                            {
                                total = data.old_ext_wc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withoutcase')
                            {
                                total = data.old_int_woc_cnt;
                            }
                            else if(emailType == 'interemails' && caseType == 'withcase')
                            {
                                total = data.old_int_wc_cnt;
                            }
                            else
                            {
                                total = data.oldemail_cnt;
                            }
                        }
                    }
                    else {
                        $('.total_mail_count').hide();
                    }

                    //inbox_send(current_list);
                    show_hide_navigation_buttons();
                    $('.total_mail').html(total);
                    var listing = "";
                    if(total > 0)
                    {
                        $.each(data.result, function (index, item) {
                            listing += generate_tr_html(item);
                        });
                    }
                    else
                    {
                        listing = "No Email Found.";
                        $('#selectall_div').css("display","none");
                    }

                    $('.mail-box tbody').html(listing);
                   /* $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });*/
                }
            });
    }

    function inbox_send(menu_item) {
        //alert(menu_item);
         if (menu_item == 'sent') {
             $(".inbox-send").text("Sent");
             $(".total_mail_count").hide();
             $(".mark_as_read").hide();
             $(".mark_as_old").hide();
             $(".mark_as_imp").hide();
             $(".move_to_trash").show();
             $('.category-list').hide();
             $(".icheckbox_square-green").hide();
         } else if (menu_item == 'urgent') {
             $(".inbox-send").text("Urgent");
             $(".mark_as_read").hide();
             $(".mark_as_imp").hide();
             $(".mark_as_old").show();
             $(".move_to_trash").hide();
             $('.category-list').show();
             $(".icheckbox_square-green").hide();
         } else {
             $(".inbox-send").text("Inbox");
             $(".total_mail_count").show();
             $(".mark_as_read").show();
             $(".mark_as_imp").show();
             $(".mark_as_old").show();
             $(".move_to_trash").show();
             $('.category-list').show();
             $(".icheckbox_square-green").show();
         }
         show_hide_navigation_buttons();
     }


    $('ul#task-navigation li a').on('click', function (e) {
        var status = $(this).parent().attr('id');

        //alert(status);
        //searchBy['taskBy'] = status;

        $('ul#task-navigation').find('li:not(.'+status+')').removeClass('active');
        //$('ul#task-navigation').find('li.'+status).addClass('active');
        $(this).closest('li').addClass('active');
    });

</script>