<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <a href="<?php echo base_url('email'); ?>">Email</a> /
                <span>Email Detail</span>
            </div>
        </div>
        <?php $this->load->view('email/email_menu'); ?>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Reply"><i class="fa fa-reply"></i> Reply</a>
                    <a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Print email"><i class="fa fa-print"></i> </a>
                    <a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Move to trash"><i class="fa fa-trash-o"></i> </a>
                </div>
                <h2>
                    View Message
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <h3>
                        <span class="font-normal">Subject: </span><?php echo $emails->subject; ?>
                    </h3>
                    <h5>
                        <span class="pull-right font-normal"><?php echo date('H:iA d M Y', strtotime($emails->datesent)); ?></span>
                        <span class="font-normal">From: </span><?php echo $emails->whofrom; /*echo $emails->whofrom_name . ' (' . $emails->whofrom . ')' */?>
                    </h5>
                </div>
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <?php echo $emails->mail_body; ?>
                    <?php // echo str_replace($emails->mail_body, "/\n/g", "<br />"); ?>
                </div>
                <div class="mail-attachment">
                    <p>
                        <span><i class="fa fa-paperclip"></i> 2 attachments - </span>
                        <a href="javascript:;">Download all</a> | <a href="javascript:;">View all images</a>
                    </p>
                    <div class="attachment">
                        <div class="file-box">
                            <div class="file">
                                <a href="javascript:;">
                                    <span class="corner"></span>
                                    <div class="icon">
                                        <i class="fa fa-file"></i>
                                    </div>
                                    <div class="file-name">
                                        Document_2014.doc
                                        <br/>
                                        <small>Added: Jan 11, 2014</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="file-box">
                            <div class="file">
                                <a href="javascript:;">
                                    <span class="corner"></span>

                                    <div class="image">
                                        <img alt="image" class="img-responsive" src="<?php echo base_url('assets/img/p1.jpg'); ?>">
                                    </div>
                                    <div class="file-name">
                                        Italy street.jpg
                                        <br/>
                                        <small>Added: Jan 6, 2014</small>
                                    </div>
                                </a>

                            </div>
                        </div>
                        <div class="file-box">
                            <div class="file">
                                <a href="javascript:;">
                                    <span class="corner"></span>

                                    <div class="image">
                                        <img alt="image" class="img-responsive" src="<?php echo base_url('assets/img/p2.jpg'); ?>">
                                    </div>
                                    <div class="file-name">
                                        My feel.png
                                        <br/>
                                        <small>Added: Jan 7, 2014</small>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="mail-body text-right tooltip-demo">
                    <a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-reply"></i> Reply</a>
                    <a class="btn btn-sm btn-white" href="mail_compose.html"><i class="fa fa-arrow-right"></i> Forward</a>
                    <button class="btn btn-sm btn-white" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Print"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-sm btn-white" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Trash"><i class="fa fa-trash-o"></i> Remove</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>