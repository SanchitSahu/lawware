<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/ckeditor/ckeditor.js"></script>
<style>
    .category-list li {
        display: inline-block !important;
    }
    .notreadyet{font-weight: 700!important;}
    div.dataTables_wrapper div.dataTables_processing { border: 1px solid #1ab394; background-color: #1ab394; color: #fff; }
    .dataTables_wrapper > .row:first-child{width: calc(100% - 120px); display: inline-flex;margin-left: 130px !important;}
    .dataTables_wrapper > .row:first-child .col-sm-6:first-child {width: auto; order: 2;flex: 100%; text-align: right; padding-right:15px;}
    .dataTables_wrapper > .row:first-child .col-sm-6:last-child {width: auto; order: 1;}
    .dataTables_wrapper > .row:first-child .col-sm-6:first-child .dataTables_length{display: inline-block;float: none;}
    .ibox-content{position: relative;}
    #selectall_div{position: absolute;top: 34px;left: 10px;z-index:1;}
    .addfile,.removefile {top:0px;cursor: pointer;position: absolute;}
    .removefile {margin-left: 26px;color: #ee5d5c;}
    .addfile {color: #4d7ab1;}
    .dataTables_wrapper{overflow: hidden;}
    .email-new-dataTable{overflow: hidden !important;}
    .email-new-dataTable tr th:nth-child(1){ width:20px !important;}
    .email-new-dataTable tr th:nth-child(2){ width:150px !important;word-wrap: break-word !important;}
    .email-new-dataTable tr th:nth-child(3){ width:300px !important;word-wrap: break-word !important;}
    .email-new-dataTable tr th:nth-child(4){ width:50px !important;word-wrap: break-word;}
    .email-new-dataTable tr td:nth-child(3) a{word-wrap: break-word; white-space: initial;
    display: block;
    }
    #send_mail_form label{font-weight: normal;}

</style>
<div class="wrapper wrapper-content">

    <div class="emailHeader">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                    <span>Email</span>
                </div>
            </div>
            <div class="col-lg-12">



                <div class="searchFiltr" id="searchFiltr">

                    <div class="col-lg-6"><label>Email Type :</label>
                        <label class="container-radio">Internal
                            <input type="radio" class="internalemailtype" checked="checked" name="emailtypes" id="emailtypes" value="interemails">
                            <span class="checkmark"></span>
                        </label>
                        <label class="container-radio">External
                            <input type="radio" class="externalemailtype" value="extemails" name="emailtypes" id="emailtypes">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <!-- <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="interemails" name="emailtypes" id="emailtypes" style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> Internal</label></div>
                    <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="extemails" name="emailtypes" id="emailtypes" style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> External</label></div>
                    </div> -->

                    <div class="col-lg-6">
                        <label>Status :</label>
                        <label class="container-radio">Without case
                            <input type="radio" checked="checked" value="withoutcase" name="casetypes" id="casetypes">
                            <span class="checkmark"></span>
                        </label>
                        <label class="container-radio">With case
                            <input type="radio" value="withcase" name="casetypes" id="casetypes">
                            <span class="checkmark"></span>
                        </label>
                        <!-- <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                                <input type="radio" checked="" value="withoutcase" name="casetypes" id="casetypes" style="position: absolute; opacity: 0;" indeterminate="true">
                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                            </div> <i></i> Without Case</label></div>
                        <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                                <input type="radio" checked="" value="withcase" name="casetypes" id="casetypes" style="position: absolute; opacity: 0;" indeterminate="true">
                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div> <i></i> With Case</label></div> -->
                        <!-- <button class="btn btn-white" title="Filter Email" id="email_filter" ><i class="fa fa-filter"></i></button> -->
                        <button class="btn btn-primary" title="Clear Email Filter" id="clear_email_filter" ><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 marg-top10">
                <div class="col-lg-3 mark_button">
                    <button class="btn btn-primary btn-sm mark_as_read" data-toggle="tooltip" data-placement="bottom" title="Mark as read"><i class="fa fa-eye fa"></i></button>
                    <button class="btn btn-primary btn-sm mark_as_old" data-toggle="tooltip" data-placement="bottom" title="Send to Old"><i class="fa fa-angle-double-right"></i></button>
                    <button class="btn btn-primary btn-sm mark_as_imp" data-toggle="tooltip" data-placement="bottom" title="Mark as urgent"><i class="fa fa-exclamation"></i> </button>
                    <button class="btn btn-primary btn-danger btn-sm move_to_trash" data-toggle="tooltip" data-placement="bottom" title="Move to delete"><i class="fa fa-trash-o"></i> </button>
                </div>
                <div class="col-lg-3">
                    <ul class="category-list" style="padding: 0; list-style: none;">
                            <!--li><a href="javascript:;" data-listing="read" title="Inbox"><i class="fa fa-circle text-warning"></i> New </a></li-->
                        <li><a href="javascript:;" data-listing="urgent" title="Urgent"><i class="fa fa-circle text-danger"></i> Urgent </a></li>
                        <li><a href="javascript:;" data-listing="unread" title="Unread"><i class="fa fa-circle text-navy"></i> Unread </a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 marg-top10">
            <div class="mail-box">
                <div class="col-lg-2 pull-right">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary compose-mail" href="javascript:;"  title="Compose Mail">Compose Mail</a>
                    </div>
                </div>
                <div class="col-lg-2 pull-right">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary refresh-mail" href="javascript:;" title="refresh" ><i class="fa fa-refresh"></i> Refresh</a>
                    </div>
                </div>
                <ul id="task-navigation" class="nav nav-pills">
                    <li id="new" class="whoto active"><a ><span class="glyphicon glyphicon-home"></span>  New</a></li>
                    <li id="old" class="whofrom"><a ><span class="glyphicon glyphicon-user"></span> Old</a></li>
                    <li id="sent" class="whofrom"><a ><span class="fa fa-paper-plane"></span> Sent</a></li>
                    <li id="Self" class="whofrom"><a ><span class="fa fa-briefcase"></span> Self</a></li>
                    <li id="delete" class="whofrom"><a><span class="fa fa-trash-o"></span> Deleted</a></li>

                </ul>

                <div class="ibox float-e-margins">

                    <div class="ibox-content marg-bot15">
                        <div style="margin: 0 0 10px 12px;" id="selectall_div">
                            <label class="container-checkbox">Select all
                                <input type="checkbox" id="selectall" >
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered email-new-dataTable">
                                <thead>
                                    <tr>
                                        <th style="width:10% !important;">#</th>
                                        <th >From</th>
                                        <th >Subject</th>
                                        <th >Date</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 animated fadeInRight changeContentDiv emailDetailDiv" style="display: none;">
        <div class="mail-box-header">
            <div class="pull-right tooltip-demo">
                <a href="javascript:;" class="btn btn-primary btn-sm back_email"><i class="fa fa-arrow-left"></i> Back</a>
                <a class="btn btn-sm btn-primary move_to_old_one" email_con_id="" href="javascript:;"><i class="fa fa-angle-double-right" style="display: none;"></i>Move to old</a>
                <a class="btn btn-sm btn-primary reply_all_compose_email" email_con_id="" href="javascript:;"><i class="fa fa-reply" style="display: none;"></i> Reply All</a>
                <a href="javascript:;" class="btn btn-primary btn-sm reply_compose_email" data-toggle="tooltip" data-placement="bottom" title="Reply"><i class="fa fa-reply"></i> Reply</a>
                <a href="javascript:;" class="btn btn-sm btn-primary forward_email" data-toggle="tooltip" data-placement="bottom" title="Forward"><i class="fa fa-arrow-right"></i> Forward</a>
                <a href="javascript:;" class="btn btn-primary btn-sm print_email" data-toggle="tooltip" data-placement="bottom" title="Print email"><i class="fa fa-print"></i> </a>
                <a href="javascript:;" class="btn btn-danger btn-sm move_to_trash_one" data-toggle="tooltip" data-placement="bottom" title="Move to delete"><i class="fa fa-trash-o"></i> </a>
                <a href="javascript:;" class="btn btn-danger btn-sm back_email"><i class="fa fa-times"></i> Close</a>
            </div>
            <h2>
                View Message
            </h2>
            <div class="mail-tools tooltip-demo m-t-md">
                <h3><span class="font-normal">Subject: </span><span id="emailSubject"></span></h3>
                <h5>
                    <span class="pull-right font-normal" id="emailDatetime"></span>
                    <span class="font-normal" id="sentto"></span>
                    <span class="font-normal">From: </span><span id="emailfromName"></span>
                    <span class="font-normal" id="inboxto"></span>
                </h5>
                <span class="pull-right" id="gotocase" style="margin-top:7px !important;"> </span>
            </div>
        </div>
        <div class="mail-box">
            <div class="mail-body">
                <pre id="emailBody">

                </pre>
                <div id="file_with_attachment">
                </div>
            </div>
            <!--                <div class="mail-attachment">
                                <p>
                                    <span><i class="fa fa-paperclip"></i> 1 attachments - </span>
                                    <a href="javascript:;">Download all</a> | <a href="javascript:;">View all images</a>
                                </p>
                                <div class="attachment">
                                    <div class="file-box">
                                        <div class="file">
                                            <a href="javascript:;">
                                                <span class="corner"></span>

                                                <div class="image">
                                                    <img alt="image" class="img-responsive" src="<?php echo base_url('assets/img/p1.jpg'); ?>">
                                                </div>
                                                <div class="file-name">
                                                    Italy street.jpg
                                                    <br/>
                                                    <small>Added: Jan 6, 2014</small>
                                                </div>
                                            </a>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>-->
            <input type="hidden" name="filenameVal" id="filenameVal">
            <input type="hidden" name="reply_iore" id="reply_iore">

            <div class="mail-body text-right tooltip-demo">
                <a class="btn btn-sm btn-primary move_to_old_one" email_con_id="" href="javascript:;"><i class="fa fa-angle-double-right" style="display: none;"></i>Move to old</a>
                <a class="btn btn-sm btn-primary reply_all_compose_email" email_con_id="" href="javascript:;"><i class="fa fa-reply" style="display: none;"></i> Reply All</a>
                <a class="btn btn-sm btn-primary reply_compose_email" href="javascript:;"><i class="fa fa-reply"></i> Reply</a>
                <a class="btn btn-sm btn-primary forward_email" href="javascript:;"><i class="fa fa-arrow-right"></i> Forward</a>
                <button class="btn btn-sm btn-primary print_email" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Print"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-sm btn-danger move_to_trash_one" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Move to delete"><i class="fa fa-trash-o"></i> </button>
                <a href="javascript:;" class="btn btn-danger btn-sm back_email"><i class="fa fa-times"></i> Close</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 animated fadeInRight changeContentDiv composeEmailDiv no-padding" style="display: none;">
        <div class="mail-box-header">
            <div class="pull-right tooltip-demo">
                <!--<a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->
<!--                    <a href="javascript:;" class="btn btn-danger btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>-->
            </div>
            <div class="pull-right">
                <a href="javascript:;" class="btn btn-danger btn-sm close_email" data-toggle="tooltip" data-placement="bottom" title="Close"><i class="fa fa-times"></i> Close</a>
            </div>
            <h2 class="compose_title">Compose Email</h2>
        </div>
        <div class="mail-box">
            <form class="form-horizontal" id="send_mail_form" name="send_mail_form" enctype="multipart/form-data">
                <div class="mail-body">
                       <div class="col-sm-6 ">

                    <div class="form-group">
                        <label class="col-sm-3 no-padding">To: <span class="asterisk">*</span></label>
                        <div class="col-sm-9">
                            <select name="whoto" id="whoto"   multiple>
                                <!-- <option value="">-- Select User to Send Email --</option> -->
                                <?php foreach ($staff_list as $staff) { ?>
                                    <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                <?php } ?>
                            </select>
                        </div></div>
                    </div>
                       <div class="col-sm-6">

                    <div class="form-group"><label class="col-sm-4 no-left-padding" style="padding:0px 0px 0px 0px;">External Parties: <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="emails" id="emails" style="font-size:13px !important; color:#000 !important;" placeholder="Add Multiple External Parties (Comma-Separated)" class="form-control">
                        </div>
                    </div>
                       </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-6">
                    <div class="form-group"><label class="col-sm-3 no-padding">Subject: <span class="asterisk">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" name="mail_subject" id="mail_subject" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">

                            <label class="col-sm-4 no-padding"> Case&nbsp;No:</label>
                            <div class="col-sm-8">
                                <input type="text" name="case_no" id="case_no" class="form-control" onkeyup="if (/\D/g.test(this.value))
                                    this.value = this.value.replace(/\D/g, '')"/>
                            </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">

                            <label class="container-checkbox" style="padding-left: 25px;">Urgent
                                <input type="checkbox" name="mail_urgent" id="mail_urgent" value="Y">
                                <span class="checkmark" ></span>
                            </label>

                    </div>
                </div>
                </div>
                <div class="clearfix"></div>
                <div class="mail-text h-200">
                    <textarea id="mailbody" class="mail-body" name="mail-body"></textarea>
                    <div class="clearfix"></div>
                </div>
                <!-- <div class="col-sm-10">
                    <div id="dragAndDropFiles" class="uploadArea">
                        <h1>Drag &amp; Drop Files Here</h1>
                    </div> </div> -->
                <div class="mail-body text-right tooltip-demo buttonalignment">
                    <div class="col-sm-8">
                    <div class="attatchmentset" style="max-height: 85px !important;display: inline-block !important;width: 100% !important;overflow: hidden !important;overflow-y: auto !important;">
                        <label class="col-sm-2 control-label">Attachment :</label>
                        <div class="col-sm-7 att-add-file marg-top5"> <input type="file" name="afiles[]" id="afiles" multiple > <i class="fa fa-plus-circle marg-top5 addfile" aria-hidden="true"></i></div>
                    </div>
                    </div>
                    <div class="col-sm-4">
                        <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send" id="mailsenddisable"><i class="fa fa-reply"></i> Send</button>
                        <a href="javascript:;" class="btn btn-danger btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                    </div>
                        <!--<a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>-->

                </div>
                <div class="col-sm-12 supported-files"><b>Supported Files :</b> PNG, JPG, JPEG, BMP,TXT, ZIP,RAR,DOC,DOCX,PDF,XLS,XLSX,PPT,PTTX </div>
                <div class="clearfix"></div>
                <input type="hidden" name="mailType" id="mailType">
                <div id="file_with_attachment_fwd">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-lg-9 animated fadeInRight email_conversation" style="display: none;">

</div>


<script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>

<script type="text/javascript">
    var url = '<?php echo base_url() ?>';
    var current_user_injs = '<?php echo $this->session->userdata('user_data')['initials'] ?>';
    var current_segment = '<?php echo $this->uri->segment(1); ?>';
</script>