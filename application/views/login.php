<style>
    form{
        background: #FFF;
        padding: 30px 15px;
    }
    label{
        font-weight: normal;
        font-size: 13px;
    }
    input, input:before, input:after {
      -webkit-user-select: initial;
      -khtml-user-select: initial;
      -moz-user-select: initial;
      -ms-user-select: initial;
      user-select: initial;
     } 
</style>

<div class="middle-box height_login loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name text-center">CC</h1>
        </div>
        <h3 class="text-center text-navy">Welcome to <?php echo APPLICATION_NAME; ?>!</h3>
        <form class="m-t" name="loginForm" id="loginForm" role="form" method="post" action="<?php echo base_url() . 'login/loginAuthentication' ?>" >
            <div class="form-group">
                <?php
                    if (isset($_COOKIE['username'])) {
                        $username = $_COOKIE['username'];
                        $password = $_COOKIE['password'];
                    } else {
                        $username = '';
                        $password = '';
                    }
                ?>
                <input type="text" placeholder="Enter Username" name="username" id="username" value="<?php if (isset($_COOKIE['username'])) {echo $_COOKIE['username'];}?>" class="form-control" autocomplete="off">
            </div>
            <div class="form-group">
                <input name="password" type="password" value="<?php echo $password; ?>" class="form-control" placeholder="Enter Password" autocomplete="off" >
                <input type="hidden" name="actlink" id="actlink" value="<?php echo $this->uri->segment(3); ?>">
            </div>
            <div class="form-group" style="display: inline-block;width: 100%;margin-bottom: 8px;">
                <div class="i-checks pull-left" ><label> <input type="checkbox" <?php if (isset($_COOKIE['username'])) {echo 'checked';}?>  name="remember"><i></i> Remember me </label></div>
                <button type="submit" class="btn btn-primary pull-right">Login</button>
            </div>
            <div class="form-group" style="margin-bottom: 0px;">
            <a href="#forgotpass" data-backdrop="static" data-toggle="modal" data-keyboard="false"><small>Forgot password?</small></a>
            </div>
            <?php if ($this->session->flashdata('message')) {
                echo '<div><span style="color:red">' . $this->session->flashdata('message') . '</span></div>';
            } ?>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        setValidation()

        $('form[name=loginForm]').submit(function(e) {
            if($('#loginForm').valid()){
                return true;
            }
            return false;
        });
    });

    function setValidation(){
        var validator = $('#loginForm').validate({
            ignore : [],
            rules:{
                username:{required:true},
                password:{required:true},
            },
            messages:{
                username:{required:"Please Fill Username"},
                password:{required:"Please Fill Password"},
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            }
        });
        return validator;
    }
</script>

<!-- Forgot password modal start -->
<div id="forgotpass" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <form id="forgotPassForm" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'login/forgotpass' ?>">

                    <div>
                        <div id="name" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox float-e-margins marg-bot10">

                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <center><div id="SuccessMessage" style="padding-bottom:10px;"></div></center>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                        <div class="col-md-12">
                                                            <label>Your Registered Email Address:</label>
                                                            <input type="text" id="emailid" name="emailid" class="form-control" value="" />
                                                            <span id="ErrorMessage" style="color:#FF0000"></span>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="button" id="forgotPass" onclick="validateEmail()" class="btn btn-md btn-primary">Send</button>
                        <button type="button" class="btn btn-md btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Forgot password modal popup end-->

<script>
    function validateEmail() {
        var email = document.getElementById("emailid").value;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(email.length == 0) {
            document.getElementById("ErrorMessage").innerHTML = 'Please enter your registerd Email ID.';
        } else if(reg.test(email) == false) {
             document.getElementById("ErrorMessage").innerHTML = 'Please enter valid Email ID.';
        } else {
            document.getElementById("ErrorMessage").innerHTML = '';
            $.ajax({
                type:"post",
                url: "<?php echo base_url(); ?>login/forgotpass",
                data:{ email:email},
                success:function(response) {
                    if (response == 1) {
                        document.getElementById("SuccessMessage").innerHTML = "<span style='color:#18a689'><h4>Check your inbox and click the link to reset your password</h4></span>";
                        setTimeout(function(){
                            $('#forgotpass').modal('hide');
                        }, 3000);
                    } else {
                        document.getElementById("SuccessMessage").innerHTML = "<span style='color:#FF0000'><h4>You have entered an invalid Username or Email ID.</h4></span>";
                    }
                }
            });
        }
    }

    $('#forgotpass').on('shown.bs.modal', function (e) {
        document.getElementById("SuccessMessage").innerHTML = '';
        document.getElementById("emailid").value = '';
    })

	function noBack(){ 
        window.history.pushState(null, "", window.location.href);        
        window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
        };
    }

</script>
