<script type="text/javascript">
	var isAppleDevice = navigator.userAgent.match(/(iPod|iPhone|iPad)/) != null;

    var inputs = jQuery("input");
    if ( isAppleDevice ){
    $(document).on('touchstart','body', function (evt) {
    var targetTouches = event.targetTouches;
    if ( !inputs.is(targetTouches)){
    inputs.context.activeElement.blur();
    }
    });
    }
</script>

<script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.4.min.js"></script>
<!-- <script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/buttons.print.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/rowReordering.min.js"></script>
</body>

</html>

