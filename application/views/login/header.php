<!DOCTYPE html>
<html lang="en" class="no-scroll">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><?php echo $pageTitle; ?></title>
        <script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets'); ?>/img/favicon.png">
        <!-- <link href="<?php echo base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet"> -->
        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/dev_styles.css" rel="stylesheet">   
    </head>

    <body class="gray-bg" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">

     <script type="text/javascript">
            var message = '<?php echo $this->session->flashdata('message') ?>';
            var HTTP_PATH = '<?php echo base_url(); ?>';
            var menu = '<?php echo $menu; ?>';
        </script>
