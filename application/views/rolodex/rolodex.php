<style type="text/css">
    .date-rangepicker .input-group-addon {
        padding: 6px 12px;
        font-size: 14px;
        font-weight: normal;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: #eee;
        border: 1px solid #ccc;
        border-radius: 4px;
    }
    .date-rangepicker input[type="text"] {
        background: #fff;
    }
    #cardRel {
        width: 90px;
    }
    #addcontact .tabs-container .panel-body {
        padding: 0px;
    }
    #addcontact .modal-body {
        padding: 0px 10px 5px;
    }
    #addcontact .form-group input, #addcontact .form-group select, #addcontact .ibox {
        margin-bottom: 5px;
    }
    #addRolodexForm label{font-weight: normal !important;}
    #addcontact .modal-dialog{
        margin: 5px auto;
    }
    .format_text{
        white-space: normal;
        overflow-wrap: normal;
    }
</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Rolodex</span>
            </div>
            <div class="ibox float-e-margins marg-bot0">
                <div class="ibox-title collapse-link">
                    <h5>Rolodex</h5>
                    <a class="btn pull-right btn-primary btn-sm">Filter <i class="fa fa-angle-down"></i></a>
                    <a class="btn pull-right btn-primary btn-sm marg-right5" href="#addcontact" data-backdrop="static" data-toggle="modal" title="Add Contact" data-keyboard="false">Add Contact</a>
                </div>
                <div class="ibox-content" style="display: none;">
                    <form role="form" id="filterRolodexForm" name="filterRolodexForm" method="post" >
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label>Firm/Company Name</label>
                                    <input type="text" id="searchByFirmName" name="searchByFirmName" placeholder="Search By Firm/Company Name" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label>First Name</label>
                                    <input type="text" id="searchByFirstName" onblur="checkAlpha(this.value,'searchByFirstName');" name="searchByFirstName" placeholder="Search By First Name" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label>Last Name</label>
                                    <input type="text" onblur="checkAlpha(this.value,'searchByLastName');" id="searchByLastName" name="searchByLastName" placeholder="Search By Last Name" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label>Contact Type</label>
                                    <select class="form-control" id="searchByContactType" name="searchByContactType">
                                        <option value="">All</option>
                                        <option value="individual">Individual</option>
                                        <option value="firm">Firm</option>
                                    </select>
                                </div>
                                <!--div class="col-lg-3">
                                    <label>Email Id</label>
                                    <input type="text" id="searchByEmailId" onblur="validateEmail('searchByEmailId')" name="searchByEmailId" placeholder="Search By Email Id" class="form-control"></select>
                                </div-->
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label>Social Security No</label>
                                    <input type="text"id="searchBySSNo" name="searchBySSNo" placeholder="Search By Social Security Number" class="form-control social_sec" autocomplete="off">
                                </div>

                                <div class="col-lg-3">
                                    <label>License No</label>
                                    <input type="text" id="searchByLicenseNo" name="searchByLicenseNo" placeholder="Search By License Number" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-6">
                                    <label>Card Number</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <select class="form-control" id="cardRel" name="cardRel">
                                                <option value="">None</option>
                                                <option value="less">Less</option>
                                                <option value="more">More</option>
                                            </select>
                                        </span>
                                        <input type="text" id="searchByCardNumber" name="searchByCardNumber" placeholder="Search By Card Number" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label>Phone Number</label>
                                    <input type="text" id="searchByPhoneNumber" name="searchByPhoneNumber" placeholder="Search By Contact Number" class="form-control contact valid" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label>Type</label>
                                    <select id="searchCardType" name="searchCardType" class="form-control">
                                        <option value="">Select Card Type</option>
                                        <?php foreach ($types as $key => $val) {?>
                                            <option value="<?php echo $val->type; ?>"><?php echo $val->type; ?></option>
                                        <?php }?>
                                    </select>
                                    <!-- <input type="text" id="searchCardType" name="searchCardType" placeholder="Search By Card Type" class="form-control"> -->
                                </div>
                                <div class="col-lg-6">
                                    <label>Birth Date</label>

                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_birthdate" class="input-small form-control" name="start_birthdate" placeholder="From Date" readonly autocomplete="off"/>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_birthdate" class="input-small form-control" name="end_birthdate" placeholder="To Date" readonly autocomplete="off"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group marg-top15 inline-block">
                            <button class="btn btn-sm btn-primary m-t-n-xs" id="btnSubmit" title="Search" type="search"><strong>Search</strong></button>
                            <button class="btn btn-sm btn-primary m-t-n-xs btn-danger" id="resetFilter" title="Clear" type="clear"><strong>Clear</strong></button>
                            <!-- <button class="btn btn-sm btn-primary m-t-n-xs" type="save"><strong>Save Option</strong></button> -->
                        </div>
                    </form>

                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content marg-bot15">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-rolodex display" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th class="text-center">Card Code</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">No. of Cases</th>
                                    <th class="text-center">City</th>
                                    <th class="text-center">Firm Name</th>
                                    <th class="text-center">Home Phone</th>
                                    <th class="text-center">Biz Phone</th>
                                    <th class="text-center">Private Line</th>
                                    <th class="text-center">SS No</th>
                                    <th class="text-center">Date of Birth</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">License No.</th>
                                    <th class="text-center">Speciality</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var caselist = "";
    var cardcode = '';
    $(document).ready(function () {
        $('#Case-list').on('shown.bs.modal', function (e) {
            caselist.columns.adjust();
        });

        rolodaxdataTable = $('.dataTables-rolodex').DataTable({
            "processing": true,
            "serverSide": true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            "bFilter": true,
            "sScrollX": "100%",
            "sScrollXInner": "150%",
            "autoWidth": false,
            lengthMenu: [[5,10, 25, 50,100,500,1000, -1], [5,10, 25, 50,100,500,1000, "All"]],
            "order": [[ 0, "desc" ]],
            dom: 'l<"html5buttons"B>tr<"col-sm-5 no-left-padding"i><"col-sm-7 no-padding"p>',
            buttons: [
                {extend: 'excel',text:'<i class="fa fa-file-excel-o"></i> Excel', title: '<?php echo APPLICATION_NAME; ?> : Search Rolodex',
                    exportOptions: {
                    columns: ':not(:last-child)'
                    }
                },
                {extend: 'pdfHtml5', text:'<i class="fa fa-file-pdf-o"></i> PDF',title: '<?php echo APPLICATION_NAME; ?> : Rolodex List', header : true,
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                      exportOptions: {
                      columns: ':visible',
                      columns: ':not(:last-child)'
                    },
                 customize: function (doc) {
                        doc.styles.table = {
                           width: '920px',
                        };
                        doc.styles.tableHeader.alignment = 'left';
                        doc['header']=(function(page, pages) {
                            return {
                                columns: [
                                    '<?php echo APPLICATION_NAME; ?> ',
                                    {
                                        alignment: 'left',
                                        text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: [5, 0]
                            }
                        });
                        }
                      },
                {extend: 'print',text:'<i class="fa fa-print" aria-hidden="true"></i> Print',
                    exportOptions: {
                    columns: ':not(:last-child)',
                    },
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "ajax": {
                url: "<?php echo base_url(); ?>rolodex/getRolodoxAjaxData",
                type: "POST"
            },
            /*"columnDefs": [
                { className: "format_text", "targets": [ 4 ] }
            ],*/
            aoColumnDefs: [
                {
                    width: "7%",
                    "render": function (data, type, row) {
                        return (data != null) ? '<a href="javascript:void(0)"  onClick="editContact(' + row[0] + ')">' + data + '</a>' : '';
                    },
                    "targets": 0
                },
                {
                    width: "12%",
                    "render": function (data, type, row) {
                        return (data != null) ? '<a href="javascript:void(0)" id="rolo_'+row[0]+'" onClick="editContact(' + row[0] + ')">' + data + '</a>' : '';
                    },
                    className: "format_text",
                    render: function (data, type, full, meta) {
                        return "<div style='white-space:normal; width: 100%; overflow-wrap: normal;'>" + data + "</div>";
                    },
                    "targets": 1
                },
                {
                    width: "5%",
                    "render": function (data, type, row) {
                        return (data != null && row[2] != 0) ? '<a href="javascript:void(0)" data-rolodex_id="' + row[0] + '" class="pull_cases" >' + data + '</a>' : data;
                    },
                    "targets": 2
                },
                {
                    width: "8%",
                    "targets": 3
                },
                {
                    width: "10%",
                    className: "format_text",
                    render: function (data, type, full, meta) {
                        return "<div style='white-space:normal; width: 100%; overflow-wrap: normal;'>" + data + "</div>";
                    },
                    "targets": 4
                },
                {
                    width: "7%",
                    "targets": 5
                },
                {
                    width: "7%",
                    "targets": 6
                },
                {
                    width: "7%",
                    "targets": 7
                },
                {
                    width: "7%",
                    "targets": 8
                },
                {
                    width: "7%",
                    "targets": 9
                },
                {
                    width: "7%",
                    "targets": 10
                },
                {
                    width: "7%",
                    "targets": 11
                },
                {
                    width: "5%",
                    "targets": 12
                },
                {
                    width: "4%",
                    "render": function (data, type, row) {
                        return (data != null) ? '<button class="btn btn-info btn-circle-action btn-circle" type="button" onClick="editContact(' + row[0] + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;<button class="btn btn-danger btn-circle-action btn-circle" type="button" onClick="deleteContact(' + row[0] + ')"><i class="fa fa-trash"></i></button>&nbsp;&nbsp;<a class="btn btn-info btn-circle-action btn-circle pull_cases" data-rolodex_id="'+ row[0] +'" title="Pull Case"><i class="fa fa-folder-open"></i></a>' : '';
                    },
                    "targets": 13,
                    "bSortable": false,
                    "bSearchable": false
                }
            ]
        });

        setTimeout(function () {
            rolodaxdataTable.columns.adjust();
        }, 1000);

        $('select[name=DataTables_Table_0_length]').on('change', function() {
            setTimeout(function () {
                rolodaxdataTable.columns.adjust();
            }, 1000);
        });

        $('#start_birthdate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        }).on('changeDate', function (ev) {
            $('#end_birthdate').datetimepicker('setStartDate', ev.date);
        });

        $('#end_birthdate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        }).on('changeDate', function (ev) {
            $('#start_birthdate').datetimepicker('setEndDate', ev.date);
        });

        setValidation();

        $('#btnSubmit').click(function () {
            rolodaxdataTable.search($("#filterRolodexForm").serialize()).draw();
            return false;
        });

        $('#resetFilter').click(function () {
            rolodaxdataTable.search('').draw();
            $('#filterRolodexForm')[0].reset();
            return false;
        });
    });

    $(document).on('click','.pull_cases',function() {
        cardcode=$(this).attr('data-rolodex_id');
        GetCaseData();

    });

    function GetCaseData() {
        if(caselist != "") {
            caselist.ajax.reload();
        } else {
            caselist = $('#caselist').DataTable({
                processing: true,
                serverSide: true,
				stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
					data.order = [0, "desc"];
				},
                bFilter: true,
                order: [[3, "asc"]],
                autoWidth: true,
                scrollX: true,
                aoColumnDefs: [{
                        bSortable: false,
                        aTargets: [8]
                    }
                ],
                ajax: {
                    url: "<?php echo base_url(); ?>rolodex/getCaseData",
                    type: "POST",
                    data: function ( d ) {
                        d.cardcode = cardcode;
                    },
                    complete: function() {
                       $("#caselist tbody tr").eq(0).trigger("click");
                       $('#Case-list').modal('show');
                    }
                },
                fnCreatedRow: function(nRow, aData, iDataIndex) {
                    $(nRow).attr('id', 'id_' + aData[10]).addClass('viewDetailsRow');
                }
            });
        }
    }

    $(document.body).on('click', '.viewDetailsRow', function(e) {
        var cardcode = $(this).find('[data-card]').data('card');
        var caseno = $(this).find('[data-case]').data('case');
        var trid = $(this).attr('id');
        $('table#caselist').find('.table-selected').not('tr#' + trid).removeClass('table-selected');
        $('#' + trid).addClass('table-selected');
        setCaseViewData(cardcode, caseno);
    });

    $(document).on('dblclick', '#caselist tbody tr td:first-child a', function () {
        $('#Case-list').modal('hide');
        var caseno = $(this).data('case');
        window.open("<?php echo base_url() ?>cases/case_details/"+caseno,'_blank');
        return false;
    });

    function deleteContact(cardCode) {
        var rName = $('#rolo_'+cardCode).html();
        $.ajax({
            url: "<?php echo base_url() . "rolodex/checkCase" ?>",
            method: "POST",
            data: {'cardcode': cardCode},
            success: function (result) {
                if (result > 0) {
                    swal("Delete Not Allowed", "The card you are trying to delete is currently attached to a case. Click the pull case button from the rolodex and pull the case "+rName+" is attached to. Then from the people section from the client card, detach that rolodex card from the case", "warning");
                } else {
                    swal({
                        title: "Are you sure you want to delete "+rName+" from the rolodex?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    }, function () {
                        $.ajax({
                            url: "<?php echo base_url() . "rolodex/deleteRec" ?>",
                            method: "POST",
                            data: {'cardcode': cardCode},
                            success: function (result) {
                                var data = $.parseJSON(result);
                                if (data == 1) {
                                    swal("Deleted!", "Rolodex successfully deleted.", "success");
                                    rolodaxdataTable.ajax.reload();
                                } else {
                                    swal("Oops...", "Oops! Something went wrong.", "error");
                                }
                            }
                        });
                    });
                }
            }
        });
    }

    function checkAlphaNumeric(inputtxt,id) {
        var letters = /^[0-9a-zA-Z]+$/;
        if(inputtxt != '') {
            if(inputtxt.match(letters)) {
                $('#'+id).parent().find('span').remove();
                return true;
            } else {
                $('#'+id).parent().find('span').remove();
                $('#'+id).parent().append('<span style="color:red;">Please enter alpha numeric value</span>');
                document.getElementById(id).focus();
                return false;
            }
        } else {
            $('#'+id).parent().find('span').remove();
        }
    }

    function checkAlphaNumericwithspace(inputtxt,id) {
        var letters = "^[0-9a-zA-Z ]+$";
        if(inputtxt != '') {
            if(inputtxt.match(letters)) {
                $('#'+id).parent().find('span').remove();
                return true;
            } else {
                $('#'+id).parent().find('span').remove();
                $('#'+id).parent().append('<span style="color:red;">Please enter alpha numeric value</span>');
                document.getElementById(id).focus();
                return false;
            }
        } else {
            $('#'+id).parent().find('span').remove();
        }
    }

    function checkAlpha(inputtxt,id) {
        var letters = /^[A-Za-z]+$/;
        if(inputtxt != '') {
            if(inputtxt.match(letters)) {
                $('#'+id).parent().find('span').remove();
                return true;
            } else {
                $('#'+id).parent().find('span').remove();
                $('#'+id).parent().append('<span style="color:red;">Please enter alphabets value</span>');
                document.getElementById(id).focus();
                 return false;
            }
        } else {
            $('#'+id).parent().find('span').remove();
        }

    }

    function validateEmail(id) {
        var x = document.getElementById(id).value;
        if(x != '') {
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
                $('#'+id).parent().find('span').remove();
                $('#'+id).parent().append('<span style="color:red;">Please enter valid email</span>');
                document.getElementById(id).focus();
                return false;
            } else {
                $('#'+id).parent().find('span').remove();
            }
        } else {
            $('#'+id).parent().find('span').remove();
        }
    }

    $('#searchByCardNumber').keypress(function (e) {
        if(e.which == 13) {
            $( "#btnSubmit" ).trigger( "click" );
        }
        var regex = new RegExp("^[0-9-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    $(document).on('keypress','#searchByPhoneNumber,#searchByLicenseNo,#searchByLastName,#searchByFirstName #searchByFirmName',function()    {
        $( "#btnSubmit" ).trigger( "click" );
    });

    $(document).on('change','#searchCardType,#searchByContactType,#start_birthdate,#end_birthdate',function()    {
        $( "#btnSubmit" ).trigger( "click" );
    });

    function setCaseViewData(cardcode, caseNo) {
        $.ajax({
            url: '<?php echo base_url(); ?>rolodex/getCaseAjaxData',
            method: 'POST',
            dataType: 'json',
            data: {
                cardcode : cardcode,
                caseno: caseNo
            },
            success: function (data) {
                if (data == false) {
                    swal({
                        title: "Alert",
                        text: "Oops! Some error has been occured while fetching details. Please try again later.",
                        type: "warning"
                    });
                } else {
                    var injury_data = data;
                    data = data[0];
                    $('#case_party_name').html(data.fullname);
                    $('#case_firm_name').html(data.firm);
                    $('#case_card_type').html(data.type);
                    $('#firm_name').html(data.firm);
                    $('#case_phone').html(data.phone);
                    $('#case_ss_no').html(data.social_sec);
                    $('#case_type').html(data.casetype);
                    $('#case_status').html(data.casestat);
                    if(data.followup) {
                        $('#case_follow').html(moment(data.followup).format('MM/DD/YYYY'));
                    } else {
                        $('#case_follow').html('');
                    }
                    if(data.dateenter) {
                        $('#case_entered').html(moment(data.dateenter).format('MM/DD/YYYY'));
                    } else {
                        $('#case_entered').html('');
                    }
                    $('#case_fileno').html(data.yourfileno);
                    $('#case_staff').html((data.atty_resp == null ? '--' : data.atty_resp) + '  ' + (data.atty_hand == null ? '--' : data.atty_hand) + '  ' + (data.para_hand == null ? '--' : data.para_hand) + '  ' + (data.sec_hand == null ? '--' : data.sec_hand));
                    var injDt = '';
                    var injuryHtml = '';
                    injury_data.forEach(function(item, index) {
                        if(item.injury_id != null) {
                            injuryHtml += (index+1)+") " + (item.adj1c == null ? '' : item.adj1c) + " - " + (item.eamsno == null || item.eamsno == "" ? '' : 'ADJ'+item.eamsno) + " - " + (item.case_no == null ? '' : item.case_no) + "<br>"
                        }
                    });
                    $('#case_injury').html(injuryHtml);
                }
            }
        });
    }

    $(document.body).on('click', 'div#addcontact a[data-toggle="tab"]', function(e) {
        if (!$('#addRolodexForm').valid()) {
            e.preventDefault();
            return false;
        }
    });

    $(document.body).on('blur','input[name=card2_state]',function(){
        var add1 = $('#street_number').val();
        var add2 = $('#route').val();
        var city = $('#locality').val();
        var state = $(this).val();
        if(state != '') {
            rolodexSearch.clear();
            $("#firmcompanylist").attr("style", "z-index:9999 !important");
            $('#firmcompanylist').modal('show');
            rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                processing: true,
                rolodexsearchAddress: true,
                serverSide: true,
                searching : true,
				stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
					data.order = [0, "ASC"];
				},
                bFilter: false,
                scrollX: true,
                sScrollY: 400,
                bScrollCollapse: true,
                destroy: true,
                autoWidth: true,
                lengthMenu: [5, 10, 25, 50, 100],
                order: [[0, "ASC"]],
                ajax: {
                    url: '<?php echo base_url(); ?>cases/searchForRolodexAddress',
                    method: 'POST',
                    data: {add1: add1, add2: add2, city: city, state: state},
                },
                fnRowCallback: function (nRow, data) {
                    $(nRow).attr("cardcode", '' + data[8]);
                    $(nRow).addClass('viewsearchPartiesDetail1');
                },
                select: {
                    style: 'single'
                }
            });
        }
    });
</script>