<style>
    .chat-active {
        background: green;
        color: white;
        font-weight: 700;
    }
    .chat-user:hover {
        background: green;
        color: white;
        font-weight: 700;
    }
</style>    
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="dashboard.php">Home</a> /
                <span>Chat</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <small class="pull-right text-muted">Last message:  Mon Jan 26 2015 - 18:39:23</small>
                    <h5>Broadcast Panel</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-9 ">
                                <div class="chat-discussion" id="chat-content">
                                    <div class="chat-message left">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="chat-users">
                                    <div class="users-list">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="chat-message-form">
                                    <form id="chatMessage" name="chatMessage" method="post">
                                        <div class="form-group">
                                            <textarea class="form-control message-input" name="message" placeholder="Enter message text"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="pull-right btn btn-primary btn-md" id="send">Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    Single Chat Box-->
<!--    <div class="small-chat-box fadeInRight animated">
            <div class="heading" draggable="true">
                <small class="chat-date pull-right">
                    02.19.2015
                </small>
                Small chat
            </div>
        <div class="">
            
        </div>
        <div class="content">
            
            <div class="left">
                <div class="author-name">
                    Monica Jackson <small class="chat-date">
                    10:02 am
                </small>
                </div>
                <div class="chat-message active">
                    Lorem Ipsum is simply dummy text input.
                </div>
            </div>
            <div class="right">
                <div class="author-name">
                    Mick Smith
                    <small class="chat-date">
                        11:24 am
                    </small>
                </div>
                <div class="chat-message">
                    Lorem Ipsum is simpl.
                </div>
            </div>
            <div class="left">
                <div class="author-name">
                    Alice Novak
                    <small class="chat-date">
                        08:45 pm
                    </small>
                </div>
                <div class="chat-message active">
                    Check this stock char.
                </div>
            </div>
            <div class="right">
                <div class="author-name">
                    Anna Lamson
                    <small class="chat-date">
                        11:24 am
                    </small>
                </div>
                <div class="chat-message">
                    The standard chunk of Lorem Ipsum
                </div>
            </div>
            <div class="left">
                <div class="author-name">
                    Mick Lane
                    <small class="chat-date">
                        08:45 pm
                    </small>
                </div>
                <div class="chat-message active">
                    I belive that. Lorem Ipsum is simply dummy text.
                </div>
            </div>
        </div>
        <div class="form-chat">
            <div class="input-group input-group-sm"><input type="text" class="form-control"> <span class="input-group-btn"> <button
                    class="btn btn-primary" type="button">Send
            </button> </span></div>
        </div>
    </div>
    <div id="small-chat">
        <span class="badge badge-warning pull-right">5</span>
        <a class="open-small-chat">
            <i class="fa fa-comments"></i>
        </a>
    </div>
</div>-->
<script type="text/javascript">
    var lastData = 1;
    var arrayData = [];
    var dataHTML = '';
    $(document).ready(function () {

        update_chats(1);
        getChatUser();
        
//        setInterval(function () {
//            update_chats(2);
//            getChatUser(2);
//        }, 5000);
        
        $('#send').click(function (e) {
            // e.preventDefault();
            var $field = $('textarea[name=message]');
            var data = $field.val();

            $field.addClass('disabled').attr('disabled', 'disabled');
            sendChat(data, function () {
                $field.val('').removeClass('disabled').removeAttr('disabled');
                update_chats('1','bottom');
            });
        });
        
//        $('textarea[name=message]').keypress(function() {
//            console.log('message');
//        });
        
        //Call this Function in Scrolling
        $('#chat-content').on('scroll', function (e) {
            if (this.scrollTop == 0) {
                $.ajax({
                    url: '<?php echo base_url() . "chat/get_messages" ?>',
                    method: "GET",
                    data: {'length': lastData},
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        //$('#chat-content').block();
                        //$('#chat-content').css({overflow: 'hidden'});
                    },
                    complete: function (jqXHR, textStatus) {
                        $('#chat-content').unblock();
                    },
                    success: function (result) {
                        if (result.length > 0)
                            lastData++;

                        dataHTML = append_chat_data(result);
                        var scHeight = $("#chat-content")[0].scrollHeight;
                        
                        $("#chat-content").prepend(dataHTML);
                        if($("#chat-content")[0].scrollHeight - scHeight > 0){
                            $('#chat-content').scrollTop($("#chat-content")[0].scrollHeight - scHeight);
                        }
                    }
                });
            }
        });
    });
    //get Chat Users
    function getChatUser(call){
        $.ajax({
            url: '<?php echo base_url() ?>chat/getUsers',
            method: "GET",
            data:{'timestamp':request_timestamp},
            dataType: 'json',
            beforeSend: function (xhr) {
                //$('#chat-content').block();
            },
            complete: function (jqXHR, textStatus) {
                //$('#chat-content').unblock();
            },
            success: function (result) {
                var htmlContent = createUserHtml(result);
                if(call == 2){
                    $('.users-list').prepend(htmlContent);
                }else{
                    $('.users-list').html(htmlContent);
                }
            }
        });
    }
    //function apendUsersListin
    function createUserHtml(recordData){
        
        var userHtml = '';
        
        recordData.forEach(function (data) {
            
            if(data.initials !== '<?php echo $loginUser; ?>'){
                    
                if($('#user-'+data.initials).length){
                    $('#user-'+data.initials).remove();
                }    
            
//                var msgs = (data.unread > 0) ? '<span class="badge badge-warning pull-right">'+data.unread+'</span>' : '';

                userHtml += '<div class="chat-user" id="user-'+data.initials+'">';
                userHtml += '	<div class="chat-user-name">';
                userHtml += '           <a href="javascript:;"><span>'+data.initials+'</span></a>';
                userHtml += '     </div>';
                userHtml += '</div>';
            }
//            
        });
    
        return userHtml;
    }
    //get Unique Array
    function getUnique(array, newArray) {
        $.each(array, function (i, e) {
            var count = $.grep(newArray, function (el, index) {
                return el.id == e.id;
            });

            if(count.length < 1) {
                newArray.push(e);
            }
        });

        return newArray;
    }
    //Chat Functionality
    var sendChat = function (message, callback) {
        $.ajax({
            url: '<?php echo base_url() . "broadcast/send_message" ?>',
            method: "POST",
            data: {'message': message, 'initials': '<?php echo $loginUser ?>'},
            dataType: 'json',
            beforeSend: function (xhr) {
                $('#chat-content').block()
            },
            complete: function (jqXHR, textStatus) {
                //$.unblockUI();
            },
            success: function (result) {
                callback();
            }
        });
    }
    //Append Request in HTML Content
    var append_chat_data = function (chat_data) {
        var html = '';

        chat_data.sort(function (a, b) {
            return new Date(a.datetime).getTime() - new Date(b.datetime).getTime()
        });

        chat_data.forEach(function (data) {
            var is_me = false;

            if (data.whofrom == '<?php echo $loginUser; ?>') {
                is_me = true;
            }

            if (is_me) {
                html += '<div class="chat-message left">';
                html += '	<div class="message">';
                html += '           <a class="message-author" href="#">' + data.whofrom + '</a>';
                html += '           <span class="message-date">' + parseTimestamp(data.timestamp) + '</span>';
                html += '           <span class="message-content">' + data.message + '</span>';
                html += '	</div>';
                html += '</div>';
            } else {
                html += '<div class="chat-message right">';
                html += '	<div class="message">';
                html += '           <a class="message-author" href="#">' + data.whofrom + '</a>';
                html += '           <span class="message-date">' + parseTimestamp(data.timestamp) + '</span>';
                html += '           <span class="message-content">' + data.message + '</span>';
                html += '	</div>';
                html += '</div>';
            }
        });
        return html;
    }
    //get Response in Interval
    var update_chats = function(call) {

        if(typeof(lastData) == 'undefined') {
            lastData = 0;
        }
        if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
                request_timestamp = 0;
	}

        $.getJSON('<?php echo base_url(); ?>broadcast/get_messages?timestamp='+request_timestamp, function (data) {
            dataHTML = append_chat_data(data);
            
            request_timestamp = parseInt(Date.now()/1000);
            
            $("#chat-content").append(dataHTML);
            
            $('#chat-content').unblock();
            
            if (call == 1) {
                $('#chat-content').animate({scrollTop: $('#chat-content')[0].scrollHeight}, 1000);
            }
        });
    }
</script>