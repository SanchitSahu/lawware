<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/ckeditor/ckeditor.js"></script>
<style>
	#myProgress {
		width: 100%;
		background-color: #ddd;
	}
	#myBar {
		width: 0%;
		height: 30px;
		background-color: #4CAF50;
		text-align: center;
		line-height: 30px;
		color: white;
	}
</style>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumbs">
				<a href="<?php echo base_url() . 'dashboard'; ?>">Home</a> /
				<span>Profile</span>
			</div>

			<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#tab-1">Personal Information</a></li>
					<li class=""><a data-toggle="tab" href="#tab-2">Change Password</a></li>
					<li class=""><a data-toggle="tab" href="#tab-3"> Signature Settings</a></li>
				</ul>
				<div class="tab-content">
					<div id="tab-1" class="tab-pane active">
						<div class="panel-body">
							<form method="post" name="updateProfile" role="form" enctype="multipart/form-data">
								<div class="ibox float-e-margins">
									<div class="ibox-content">

										<div class="row" style="margin-top:20px;;">
											<div class="col-md-2">
												<label>Initials</label>
											</div>
											<div class="col-md-6">
												<input type="text" maxlength="3" id="initial" value="<?php echo $profile[0]->initials; ?>" readonly name="initial" class="form-control" value="" />
											</div>
										</div>
										<div class="row" style="margin-top:20px;">
											<div class="col-md-2">
												<label>Username</label>
											</div>
											<div class="col-md-6">
												<input type="text" name="uname" value="<?php echo $profile[0]->username; ?>" readonly maxlength="3" id="uname" class="form-control" value="" />
											</div>
										</div>
										<div class="row" style="margin-top:20px;">
											<div class="col-md-2">
												<label>Designation</label>
											</div>
											<div class="col-md-6">
												<input type="text" name="uname" value="<?php echo $profile[0]->title; ?>" readonly maxlength="3" id="uname" class="form-control" value="" />
											</div>
										</div>
										<div class="row" style="margin-top:20px;">
											<div class="col-md-2">
												<label>Profile Image</label>
											</div>
											<div class="col-xs-6">
												<input type="file" name="profile" maxlength="3" id="profile" class="form-control" value="" />
												<!--span style="color:red">Max upload size is 2MB</span-->
												<input type="hidden" id="oldProfile" value="<?php echo $profile[0]->profilepic ?>">
												<div id="myProgress" style="display:none">
													<div id="myBar"></div>
												</div>
											</div>
											<?php
											if (!empty($profile[0]->profilepic)) {
												?>
												<div class="col-xs-3">
													<?php
													$filename = $_SERVER['DOCUMENT_ROOT'] . '/assets/profileimages/' . $profile[0]->profilepic;
													if (file_exists($filename)) {
														?>
														<img style="margin-top: -9px; height: 50px; width: 50px;" src="<?php echo base_url('assets/profileimages') . "/" . $profile[0]->profilepic; ?>" class="img-circle" height="50" width="50">

														<?php
													} else {
														?>
														<img style="margin-top: -9px; height: 50px; width: 50px;" src="<?php echo base_url('assets/profileimages') . "/users.png"; ?>" class="img-circle" height="50" width="50">
														<?php
													}
													?>

												</div>
												<?php
											}
											?>
										</div>
									</div>
									<div class="form-group text-center marg-top15 col-md-12">
										<button type="button" id="saveprofile" class="btn btn-md btn-primary" onclick="saveProfile()">Update</button>
										<button type="button" id="cancel" class="btn btn-md btn-primary" data-dismiss="modal" onclick="window.location = '<?php echo base_url() . "dashboard" ?>'">Cancel</button>
									</div>
								</div>

							</form>
						</div>
					</div>
					<div id="tab-2" class="tab-pane">
						<div class="panel-body">
							<div class="ibox float-e-margins">
								<div class="ibox-content">
									<div class="row">
										<div class="col-sm-12">
											<div class="row" style="margin-top:20px;">
												<div class="col-md-2">
													<label>Old Password</label>
												</div>
												<div class="col-md-6">
													<input type="password" onBlur="clear_error('oldpass', 'opassError')" name="oldpass" id="oldpass" class="form-control" value="" />
													<span id="opassError" style="color:#FF0000 !important;"></span>
												</div>
											</div>
											<div class="row" style="margin-top:20px;">
												<div class="col-md-2">
													<label>New Password</label>
												</div>
												<div class="col-md-6">
													<input type="password" name="pass" onBlur="clear_error('pass', 'passError')" id="pass" class="form-control" value="" />
													<span id="passError" style="color:#FF0000 !important;"></span>
												</div>
											</div>
											<div class="row" style="margin-top:20px;">
												<div class="col-md-2">
													<label>Confirm Password</label>
												</div>
												<div class="col-md-6">
													<input type="password" name="cpass" onBlur="clear_error('cpass', 'cpassError')" id="cpass" class="form-control" value="" />
													<span id="cpassError" style="color:#FF0000 !important;"></span>
												</div>

											</div>
										</div>
										<div class="form-group text-center marg-top15 col-md-12">
											<button type="button" id="saveprofile" class="btn btn-md btn-primary" onclick="changePsaaword()">Update</button>
											<button type="reset" id="resetpass" class="btn btn-md btn-primary" onclick="resetpassword()">Reset</button>
											<button type="button" id="cancel" class="btn btn-md btn-primary" data-dismiss="modal" onclick="window.location = '<?php echo base_url() . "dashboard" ?>'">Cancel</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="tab-3" class="tab-pane">
						<div class="panel-body">
							<div class="ibox float-e-margins">
								<div class="ibox-content">
									<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="container-checkbox" style="padding-left: 25px;">On/Off Signature
												 <input type="checkbox" name="onoffsignature" id="onoffsignature" class="onoffsignature" value="<?php echo $profile[0]->onoffsignature; ?>" <?php if($profile[0]->onoffsignature == 1) { echo "checked"; } ?>>
													 <span class="checkmark" ></span>
											</label>
										</div>
										<div id="fulldivforsignature">
										<form class="form-horizontal" id="update_signature_form" name="update_signature_form" enctype="multipart/form-data">
											<div class="clearfix"></div>
										<div class="signature-text h-200">
												<textarea id="mailbody" class="mail-body" name="mailbody">
													<?php echo $profile[0]->signature;?>
												</textarea>
												<div class="clearfix"></div>
										</div>
										<div class="form-group text-center marg-top15 col-md-12">
											<button type="button" id="updatesignature" class="btn btn-md btn-primary" >Update</button>
											<button type="button" id="blanksignature" class="btn btn-md btn-danger" >Clear</button>
										</div>
										<div class="col-sm-12 supported-files text-center" id="note"><b>Note: </b> TO MAKE SIGNATURE <b>EMPTY</b> CLICK ON CLEAR BUTTON & THEN ON UPDATE</div>
											<div id = "hiddendivforsignature" style="display:none">
											 <?php echo htmlspecialchars_decode($profile[0]->signature) ?> 
											</div>
										</form>
										</div>
									</div>
									 </div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!--End tabs-container-->

		</div>
	</div>
</div>
<script>
	function saveProfile()
	{
		if ($('#profile').val() != '')
		{
			document.getElementById('myProgress').style.display = 'block';
			var elem = document.getElementById("myBar");
			var width = 10;
			var id = setInterval(frame, 10);
			function frame()
			{
				if (width >= 100) {
					var file_data = $('#profile').prop('files')[0];
					var old_file = $('#oldProfile').val();
					var form_data = new FormData();
					form_data.append('file', file_data);

					$.ajax({
						url: '<?php echo base_url(); ?>profile/updateprofilepic/' + old_file, // point to server-side controller method
						dataType: 'text', // what to expect back from the server
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,
						type: 'post',
						success: function (response) {
							//alert(response);
							if (response == 1)
							{
								location.reload();
							}
						},
						error: function (response) {
							console.log(response);
						}
					});
					clearInterval(id);
				} else {
					width++;
					elem.style.width = width + '%';
					elem.innerHTML = width * 1 + '%';
				}


			}
		}
	}



	function changePsaaword()
	{
		var opassword = document.getElementById("oldpass").value;
		var password = document.getElementById("pass").value;
		var cpassword = document.getElementById("cpass").value;

		if (opassword.length == 0)
		{
			document.getElementById("opassError").innerHTML = 'Please enter your old password.';
		} else
		{
			document.getElementById("opassError").innerHTML = '';
		}

		if (password.length == 0)
		{
			document.getElementById("passError").innerHTML = 'Please enter your new password.';
		} else
		{
			document.getElementById("passError").innerHTML = '';
		}

		if (cpassword.length == 0)
		{
			document.getElementById("cpassError").innerHTML = 'Please enter confirm password.';
		} else
		{
			document.getElementById("cpassError").innerHTML = '';
		}

		if (password.length != 0 && cpassword.length != 0)
		{
			document.getElementById("opassError").innerHTML = "";
			document.getElementById("passError").innerHTML = "";
			if (password != cpassword)
			{
				document.getElementById("cpassError").innerHTML = 'Password and confirm password should be same.';
			} else
			{
				$.ajax(
						{
							type: "post",
							url: "<?php echo base_url(); ?>profile/resetpass",
							data: {oldpass: opassword, pass: password},
							success: function (response)
							{
								if (response == 1)
								{
									swal({
										title: "Success!",
										text: "Password updated successfully.Please login with new password.",
										type: "success"},
											function ()
											{
												window.location = '<?php echo base_url(); ?>login/logout';
											}
									);

								} else
								{
									document.getElementById("cpassError").innerHTML = "Old password does not matched.";
								}
							}

						});
			}
		}

	}

	function resetpassword()
	{
		document.getElementById("oldpass").value = "";
		document.getElementById("pass").value = "";
		document.getElementById("cpass").value = "";
		document.getElementById("opassError").innerHTML = "";
		document.getElementById("passError").innerHTML = "";
		document.getElementById("cpassError").innerHTML = "";
	}

	function clear_error(id, eid)
	{
		var fieldval = document.getElementById(id).value;
		if (fieldval.length > 0)
		{
			document.getElementById(eid).innerHTML = '';
		}
	}

	// To load ck editor 
	$(document).ready(function () 
	{
		CKEDITOR.replace('mailbody');	
	});

	 // To Update Signature
		$(document).on("click","#updatesignature",function()
		 {
			if($("#onoffsignature").prop('checked') == true)
			{	
				
			var onoffsignature = 1;
			} 
			if($("#onoffsignature").prop('checked') == false)
			{	
				var onoffsignature = 0;
			} 
			var mailbody = CKEDITOR.instances.mailbody.getData();
			$.ajax({
						url: '<?php echo base_url(); ?>profile/updatesignature',
						method: 'POST',
						data : {mailbody:mailbody,onoffsignature:onoffsignature},
						dataType:"json",
						beforeSend: function (xhr) {
							$.blockUI();
						},
						complete: function (jqXHR, textStatus) {
							$.unblockUI();
						},
						success: function (data) {
							// var response = JSON.parse(data);
							if(data.status = 'success'){
								swal({
								title: "Success",
								text: "Signature Updated Sucessfully",
								type: "success"
								});	
							}
						CKEDITOR.instances['mailbody'].setData(data[0].signature);
							if(data[0].onoffsignature == 1)
							{
							$('#onoffsignature').prop('checked', true);
							}
							else{
							$('#onoffsignature').prop('checked', false);
							}	
						}
					});
		});

		$(document).on("click","#blanksignature",function()
	   {
		CKEDITOR.instances['mailbody'].setData("");
		});

</script>