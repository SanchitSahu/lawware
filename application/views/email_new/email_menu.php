<div class="col-lg-3">
    <div class="ibox float-e-margins">
        <div class="ibox-content mailbox-content">
            <div class="file-manager">
                <a class="btn btn-block btn-primary compose-mail" href="javascript:;" title="Compose Mail">Compose Mail</a>
                <div class="space-25"></div>
                <h5>Folders</h5>
                <ul class="folder-list m-b-md" style="padding: 0">
                    <li>
                        <a href="javascript:;"  data-menu="inbox" class="inbox menu" title="Inbox"><i class="fa fa-inbox "></i> Inbox
                            <span style="display: <?php echo ($unread_count != 0) ? 'inline' : 'none'; ?>" class="label label-warning pull-right total_mail_count"><?php echo $unread_count; ?></span>
                        </a>
                    </li>
                    <li><a href="javascript:;" data-menu="sent" class="sent menu" title="Sent Mail"><i class="fa fa-envelope-o"></i> Sent Mail</a></li>
                    <!--<li><a href="javascript:;"><i class="fa fa-certificate"></i> Important</a></li>-->
                    <!--<li><a href="javascript:;"><i class="fa fa-file-text-o"></i> Drafts <span class="label label-danger pull-right">2</span></a></li>-->
                    <!--<li><a href="javascript:;"><i class="fa fa-trash-o"></i> Trash</a></li>-->
                </ul>
                <h5>Categories</h5>
                <ul class="category-list" style="padding: 0">
                    <li><a href="javascript:;" data-listing="inbox" title="Inbox"><i class="fa fa-circle text-warning"></i> Inbox / Read</a></li>
                    <li><a href="javascript:;" data-listing="urgent" title="Urgent"><i class="fa fa-circle text-danger"></i> Urgent </a></li>
                    <li><a href="javascript:;" data-listing="unread" title="Unread"><i class="fa fa-circle text-navy"></i> Unread </a></li>
                    <li><a href="javascript:;" data-listing="withcase" title="With Case No"><i class="fa fa-diamond text-navy"></i>With Case Number
                    <span style="display: <?php echo ($withcno_count != 0) ? 'inline' : 'none'; ?>" class="label label-primary pull-right withcno_count"><?php echo $withcno_count; ?></span></a></li>
                    <li><a href="javascript:;" data-listing="withoutcase" title="Without Case No"><i class="fa fa-file-o text-primary" aria-hidden="true"></i> Without Case Number
                        <span style="display: <?php echo ($withoutcno_count != 0) ? 'inline' : 'none'; ?>" class="label label-success pull-right withoutcase_count"><?php echo $withoutcno_count; ?></a></span></li>
                    <li><a href="javascript:;" data-listing="interemails" title="Internal Emails"><i class="fa fa-file-image-o text-warning" aria-hidden="true"></i> Internal Emails
                        <span style="display: <?php echo ($internal_count != 0) ? 'inline' : 'none'; ?>" class="label label-warning pull-right withoutcase_count"><?php echo $internal_count; ?></a></span></li>
                    <li><a href="javascript:;" data-listing="extemails" title="External Emails"><i class="fa fa-file-archive-o text-primary" aria-hidden="true"></i> External Emails
                        <span style="display: <?php echo ($external_count != 0) ? 'inline' : 'none'; ?>" class="label label-success pull-right withoutcase_count"><?php echo $external_count; ?></a></span></li>
                </ul>

                <h5 class="tag-title">Labels</h5>
                <ul class="tag-list" style="padding: 0">
                    <li><a href="javascript:;" data-listing="inbox" title="Inbox"><i class="fa fa-tag"></i> Inbox</a></li>
                    <li><a href="javascript:;" data-listing="urgent" title="Urgent"><i class="fa fa-tag"></i> Urgent</a></li>
                    <li><a href="javascript:;" data-listing="unread" title="Unread"><i class="fa fa-tag"></i> Unread</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>