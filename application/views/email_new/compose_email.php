<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <a href="<?php echo base_url('email'); ?>">Email</a> /
                <span>Email Compose</span>
            </div>
        </div>
        <?php $this->load->view('email/email_menu'); ?>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>
                    <a href="javascript:;" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                </div>
                <h2>Compose mail</h2>
            </div>
            <div class="mail-box">
                <form class="form-horizontal" id="send_mail_form" method="POST" enctype="multipart/form-data">
                    <div class="mail-body">
                        <div class="form-group"><label class="col-sm-2 control-label">To:</label>
                            <div class="col-sm-10">
                                <select name="whoto" style="width:100%">
                                    <option value="">-- Select User to Send Email --</option>
                                    <?php foreach ($staff_list as $staff) { ?>
                                        <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Subject:</label>
                            <div class="col-sm-10">
                                <input type="text" name="mail_subject" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="mail-text h-200">
                        <textarea id="mail-body" class="mail-body">
                            <h3>Hello Jonathan! </h3>
                            dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                            typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                            <br/>
                            <br/>
                        </textarea>
                        <div class="clearfix"></div>
                    </div>

                    <div class="mail-body text-right tooltip-demo">
                        <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send"><i class="fa fa-reply"></i> Send</button>
                        <a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                        <a href="javascript:;" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="whoto"]').select2({selectOnClose: true}).on('change', function () {
            $('select[name="whoto"]').valid();
        });

        CKEDITOR.replace('mail-body');

        $('#send_mail_form').validate({
            rules: {
                whoto: {
                    required: true
                },
                mail_subject: {
                    required: true
                }

            },
            messages: {
                whoto: {
                    required: 'Please Select a User to send mail to'
                },
                mail_subject: {
                    required: 'Please enter a Mail Subject'
                }
            },
            errorPlacement: function (error, element) {
                if ($(element).attr('name') == 'whoto') {
                    $(element).parent().append(error);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                swal({
                    title: "Success!",
                    text: "Mail is successfully sent.",
                    type: "success"
                });
                return false;
            }
        });

    });
</script>