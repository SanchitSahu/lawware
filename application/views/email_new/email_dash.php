<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/ckeditor/ckeditor.js"></script>
<style>
    .category-list li { display: inline-block !important; }
    .notreadyet{font-weight: 700!important;}
    div.dataTables_wrapper div.dataTables_processing { border: 1px solid #4d7ab1; background-color: #4d7ab1; color: #fff; }
    /*.dataTables_wrapper > .row:first-child{width: calc(100% - 120px); display: inline-flex;margin-left: 130px !important;}*/
    .dataTables_wrapper > .row:first-child{display: flex;margin-bottom: 30px !important;}
    .dataTables_wrapper > .row:first-child .col-sm-6:first-child {width: auto; order: 2;flex: 100%; text-align: right; padding-right:15px;}
    .dataTables_wrapper > .row:first-child .col-sm-6:last-child {width: auto; order: 1;}
    .dataTables_wrapper > .row:first-child .col-sm-6:first-child .dataTables_length{display: inline-block;float: none;}
    div.table-responsive>div.dataTables_wrapper>div.row>div[class^="col-"]:last-child{padding-left: 0;}
    .ibox-content{position: relative;}
    #selectall_div{position: absolute;top: 55px;left: 5px;z-index:1;}
    .addfile,.removefile {top:0px;right:0;cursor: pointer;position: absolute;font-size: 20px;}
    .removefile {right: 20px;color: #ee5d5c;}
    .addfile {color: #4d7ab1;}
    .dataTables_wrapper{overflow: hidden;}
    .email-new-dataTable{overflow: hidden !important;}
    /*    .email-new-dataTable tr th:nth-child(1){ width:20px !important;}
        .email-new-dataTable tr th:nth-child(2){ width:150px !important;word-wrap: break-word !important;}
        .email-new-dataTable tr th:nth-child(3){ width:300px !important;word-wrap: break-word !important;}
        .email-new-dataTable tr th:nth-child(4){ width:50px !important;word-wrap: break-word;}*/
    .email-new-dataTable tr th:nth-child(2){word-wrap: break-word !important;}
    .email-new-dataTable tr th:nth-child(3){word-wrap: break-word !important;}
    .email-new-dataTable tr th:nth-child(4){word-wrap: break-word;}
    .email-new-dataTable tr td:nth-child(3) a{ word-wrap: break-word; white-space: initial; display: block; }
    #send_mail_form label{font-weight: normal;}
    .container-radio{min-width: 120px;}
    .emailHeader .searchFiltr .col-lg-6 label:not(.container-radio){min-width: 105px;}
    .composeEmailDiv .close_email {margin-bottom: 0;}
    .composeEmailDiv .mail-box-header { padding: 5px 10px 5px;}
    .emailDetailDiv .mail-tools{margin-top: 50px;}
    div.dataTables_wrapper div.dataTables_processing{top: 25%;}
    #DataTables_Table_0 tbody tr.odd {background-color: #f9f9f9;}
   @media (min-width:768px) and (max-width:882px){
   div.dataTables_wrapper div.dataTables_filter input{
       width:100px !important;
    }
}
</style>
<div class="wrapper wrapper-content">
    <div class="emailHeader">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                    <span>Email</span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="searchFiltr" id="searchFiltr">
                    <div class="col-lg-6">
                        <div id="email_type_filter">
                            <label>Email Type :</label>
                            <label class="container-radio">Internal
                                <input type="radio" class="internalemailtype" checked="checked" name="emailtypes" id="emailtypes" value="interemails">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container-radio">External
                                <input type="radio" class="externalemailtype" value="extemails" name="emailtypes" id="emailtypes">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <label>Status :</label>
                        <label class="container-radio">Without case
                            <input type="radio" checked="checked" value="withoutcase" name="casetypes" id="casetypes">
                            <span class="checkmark"></span>
                        </label>
                        <label class="container-radio">With case
                            <input type="radio" value="withcase" name="casetypes" id="casetypes">
                            <span class="checkmark"></span>
                        </label>
                        <button class="btn btn-primary" title="Clear Email Filter" id="clear_email_filter" ><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 marg-top10">
                <div class="col-lg-3 mark_button">
                    <button class="btn btn-primary btn-sm mark_as_read" data-toggle="tooltip" data-placement="bottom" title="Mark as read"><i class="fa fa-eye fa"></i></button>
                    <button class="btn btn-primary btn-sm mark_as_old" data-toggle="tooltip" data-placement="bottom" title="Send to Old"><i class="fa fa-angle-double-right"></i></button>
                    <button class="btn btn-primary btn-sm mark_as_imp" data-toggle="tooltip" data-placement="bottom" title="Mark as urgent"><i class="fa fa-exclamation"></i> </button>
                    <button class="btn btn-primary btn-danger btn-sm move_to_trash" data-toggle="tooltip" data-placement="bottom" title="Move to delete"><i class="fa fa-trash-o"></i> </button>
                    <button class="btn btn-primary btn-sm move_to_new" data-toggle="tooltip" data-placement="bottom" title="Move to inbox"><i class="fa fa-inbox" aria-hidden="true"></i></button>
                </div>
                <div class="col-lg-3">
                    <ul class="category-list" style="padding: 0; list-style: none;">
                        <li><a href="javascript:;" data-listing="urgent" title="Urgent"><i class="fa fa-circle text-danger"></i> Urgent </a></li>
                        <li><a href="javascript:;" data-listing="unread" title="Unread"><i class="fa fa-circle text-navy"></i> Unread </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 marg-top10">
            <div class="mail-box">
                <div class="col-lg-2 pull-right marg-top10">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary compose-mail" href="javascript:;"  title="Compose Mail"><i class="fa fa-pencil-square-o"></i> Compose Mail</a>
                    </div>
                </div>
                <div class="col-lg-2 pull-right marg-top10">
                    <div class="file-manager">
                        <a class="btn btn-block btn-primary refresh-mail" href="javascript:;" title="refresh" ><i class="fa fa-refresh"></i> Refresh</a>
                    </div>
                </div>
                <ul id="task-navigation" class="nav nav-pills">
                    <li id="new" class="whoto active"><a ><span class="glyphicon glyphicon-home"></span>  New</a></li>
                    <li id="old" class="whofrom"><a ><span class="glyphicon glyphicon-user"></span> Old</a></li>
                    <li id="sent" class="whofrom"><a ><span class="fa fa-paper-plane"></span> Sent</a></li>
                    <li id="Self" class="whofrom"><a ><span class="fa fa-briefcase"></span> Self</a></li>
                    <li id="delete" class="whofrom"><a><span class="fa fa-trash-o"></span> Deleted</a></li>
                </ul>

                <div class="float-e-margins">
                    <div class="ibox-content">
                        <div style="margin: 0 0 10px 12px;" id="selectall_div">
                            <label class="container-checkbox">Select all
                                <input type="checkbox" id="selectall" >
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-sm-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered email-new-dataTable" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th >#</th>
                                                <th >Emails</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <?php // <editor-fold defaultstate="collapsed" desc="Email Details">?>
                            <div class="col-md-7 col-sm-6">
                                <div class="changeContentDiv emailDetailDiv" style="display: none;">
                                    <div class="mail-box-header">
                                        <div class="tooltip-demo small-fonts col-sm-8 text-left no-padding">
                                            <a href="javascript:;" class="btn btn-sm btn-primary back_email hidden" data-toggle="tooltip" data-placement="bottom" title="Back"><i class="fa fa-arrow-left"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary move_to_old_one" data-toggle="tooltip" data-placement="bottom" title="Move to old" email_con_id=""><i class="fa fa-angle-double-left"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary reply_compose_email" data-toggle="tooltip" data-placement="bottom" title="Reply"><i class="fa fa-reply"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary reply_all_compose_email" data-toggle="tooltip" data-placement="bottom" title="Reply All" email_con_id="" ><i class="fa fa-reply-all"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary forward_email" data-toggle="tooltip" data-placement="bottom" title="Forward"><i class="fa fa-arrow-right"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary print_email" data-toggle="tooltip" data-placement="bottom" title="Print email"><i class="fa fa-print"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary envelope_email_open" data-filename="" iore="" email_con_id="" data-toggle="tooltip" data-placement="bottom" title="Mark as read"><i class="fa fa-eye fa"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary envelope_email_open_unread" data-filename="" iore="" email_con_id="" data-toggle="tooltip" data-placement="bottom" title="Mark as unread"><i class="fa fa-envelope"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-primary move_to_new_one" data-filename="" iore="" email_con_id="" data-toggle="tooltip" data-placement="bottom" title="Move to inbox"><i class="fa fa-inbox" aria-hidden="true"></i></a>
                                            <input type="hidden" name="filename" class="openunread_filename">
                                        </div>
                                        <div class="tooltip-demo small-fonts col-sm-4 text-right no-padding">
                                            <a href="javascript:;" class="btn btn-sm btn-danger move_to_trash_one" data-toggle="tooltip" data-placement="bottom" title="Move to delete"><i class="fa fa-trash-o"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-danger back_email" data-toggle="tooltip" data-placement="bottom" title="Close"><i class="fa fa-times"></i></a>

                                        </div>
                                        <div class="mail-tools tooltip-demo m-t-md">
                                            <h3><span class="font-normal">Subject: </span><span id="emailSubject"></span></h3>
                                            <h5>
                                                <div class="marg-top10"><span class="pull-right font-normal" id="emailDatetime"></span>
                                                <span class="font-normal" id="sentto"></span>
                                                <span class="font-normal">From: </span><span id="emailfromName"></span></div>

												<div class="marg-top10"><span class="font-normal" id="inboxto" style="word-break: break-all;"></span></div>
												<div class="marg-top10">
                                                    <span class="gotocase" id="gotocase"> </span>
												</div>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="mail-box">
                                        <div class="mail-body">
                                            <pre id="emailBody"></pre>
                                            <div id="file_with_attachment"></div>
                                        </div>
                                        <!--<div class="mail-attachment">
                                            <p>
                                                <span><i class="fa fa-paperclip"></i> 1 attachments - </span>
                                                <a href="javascript:;">Download all</a> | <a href="javascript:;">View all images</a>
                                            </p>
                                            <div class="attachment">
                                                <div class="file-box">
                                                    <div class="file">
                                                        <a href="javascript:;">
                                                            <span class="corner"></span>
                                                            <div class="image">
                                                                <img alt="image" class="img-responsive" src="<?php echo base_url('assets/img/p1.jpg'); ?>">
                                                            </div>
                                                            <div class="file-name">
                                                                Italy street.jpg
                                                                <br/>
                                                                <small>Added: Jan 6, 2014</small>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>-->
                                        <input type="hidden" name="filenameVal" id="filenameVal">
                                        <input type="hidden" name="reply_iore" id="reply_iore">
                                        <div class="clearfix"></div>
                                        <div class="mail-body text-right tooltip-demo small-fonts">
                                            <div class="tooltip-demo small-fonts col-sm-8 text-left no-padding">
                                                <a href="javascript:;" class="btn btn-sm btn-primary back_email hidden" data-toggle="tooltip" data-placement="bottom" title="Back"><i class="fa fa-arrow-left"></i></a>
                                                <a class="btn btn-sm btn-primary move_to_old_one" email_con_id="" href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="Move to old"><i class="fa fa-angle-double-left"></i></a>
                                                <a class="btn btn-sm btn-primary reply_compose_email" href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="Reply"><i class="fa fa-reply"></i></a>
                                                <a class="btn btn-sm btn-primary reply_all_compose_email" email_con_id="" href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="Reply to all"><i class="fa fa-reply-all"></i></a>
                                                <a class="btn btn-sm btn-primary forward_email" href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="Forward"><i class="fa fa-arrow-right"></i></a>
                                                <button class="btn btn-sm btn-primary print_email" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Print"><i class="fa fa-print"></i></button>
                                                <a href="javascript:;" class="btn btn-sm btn-primary envelope_email_open" data-filename="" iore="" email_con_id="" data-toggle="tooltip" data-placement="bottom" title="Mark as read"><i class="fa fa-eye fa"></i></a>
                                                <a href="javascript:;" class="btn btn-sm btn-primary envelope_email_open_unread" data-filename="" iore="" email_con_id="" data-toggle="tooltip" data-placement="bottom" title="Mark as unread"><i class="fa fa-envelope"></i></a>
                                                <a href="javascript:;" class="btn btn-sm btn-primary move_to_new_one" data-filename="" iore="" email_con_id="" data-toggle="tooltip" data-placement="bottom" title="Move to inbox"><i class="fa fa-inbox" aria-hidden="true"></i></a>
                                            </div>
                                            <div class="tooltip-demo small-fonts col-sm-4 text-right no-padding">
                                                <button class="btn btn-sm btn-danger move_to_trash_one" data-placement="bottom" data-toggle="tooltip" type="button" data-original-title="Move to delete"><i class="fa fa-trash-o"></i></button>
                                                <a href="javascript:;" class="btn btn-danger btn-sm back_email" data-toggle="tooltip" data-placement="bottom" title="Close"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <?php // </editor-fold>?>

                            <?php // <editor-fold defaultstate="collapsed" desc="Compose email DIV"> ?>
                            <div class="col-md-7 col-sm-6">
                                <div class="changeContentDiv composeEmailDiv no-padding" style="display: none;">
                                    <div class="mail-box-header text-right">
                                    <button class="btn btn-sm btn-primary" title="Include Signature" id="includesignature" style="margin: 0px 0px 0px 0px;">Include Signature</button>
                                    <button class="btn btn-sm btn-primary" title="Send" id="mailsenddisable1" style="margin: 1px 0px 0px 0px;"><i class="fa fa-paper-plane"></i> Send</button>
                                        <a href="javascript:;" class="btn btn-danger btn-sm close_email" data-toggle="tooltip" data-placement="bottom" title="Close"><i class="fa fa-times"></i> Close</a>
                                    </div>
                                    <div class="mail-box">
                                        <form class="form-horizontal" id="send_mail_form" name="send_mail_form" enctype="multipart/form-data">
                                            <div class="mail-body">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding">To: <span class="asterisk">*</span></label>
                                                        <div class="col-sm-8">
                                                            <select name="whoto" id="whoto" multiple>
                                                                <?php foreach ($staff_list as $staff) {?>
                                                                    <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding">External Parties: <span class="asterisk">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="emails" id="emails" style="font-size:13px !important; color:#000 !important;" placeholder="Add Multiple External Parties (Comma-Separated)" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding">Subject: </label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="mail_subject" id="mail_subject" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding"> Case No:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" name="case_no" id="case_no" class="form-control" onkeyup="if (/\D/g.test(this.value))
                                                                        this.value = this.value.replace(/\D/g, '')"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="container-checkbox" style="padding-left: 25px;">Urgent
                                                            <input type="checkbox" name="mail_urgent" id="mail_urgent" value="Y">
                                                            <span class="checkmark" ></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="mail-text h-200">
                                                <textarea id="mailbody" class="mail-body" name="mail-body">
                                                </textarea>
                                                <div class="clearfix"></div>
                                            </div>

                                            <div class="mail-body text-right tooltip-demo buttonalignment">
                                            </div>
                                            <div class="col-sm-12">
											<div id="selectedloader"></div>
											<div id="selectedFiles"></div>
                                                <div class="attatchmentset" style="display: inline-block !important;width: 100% !important;overflow: hidden !important;overflow-y: auto !important;">
                                                    <div class="col-sm-12 no-padding att-add-file marg-top5">
                                                        <input type="file" name="afiles[]" id="afiles">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-12 text-right">
                                                <button class="btn btn-sm btn-primary" title="Send" id="mailsenddisable"><i class="fa fa-paper-plane"></i> Send</button>
                                                <a href="javascript:;" class="btn btn-danger btn-sm discard_message" title="Discard email"><i class="fa fa-trash"></i> Discard</a>
                                            </div>
                                            
                                            <div class="col-sm-12 supported-files text-center"><b>Note: </b> File size upload upto 20 MB <b>Supported Files :</b> PNG, JPG, JPEG, BMP, TXT, ZIP, RAR, DOC, DOCX, PDF, XLS, XLSX, PPT, PTTX, MP4</div>
                                            <div class="clearfix"></div>
                                            <input type="hidden" name="mailType" id="mailType">
                                            <div id="file_with_attachment_fwd"></div>
                                            <input type="hidden" name="totalFileSize" id="totalFileSize" value="0">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php // </editor-fold> ?>

                            <?php // <editor-fold defaultstate="collapsed" desc="Placeholder DIV"> ?>
                            <div class="col-md-7 col-sm-6">
                                <div class="changeContentDiv placeholderEmailDiv no-padding">
                                    <img src="<?php echo base_url('assets/img/no-email.svg'); ?>" width="120px" height="120px"/>
                                    <p>Select an item to read</p>
                                </div>
                            </div>
                            <?php // </editor-fold> ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id='signaturediv' class="signaturediv" style="display:none;" >
    <?php 
        if( $this->data['signature']->onoffsignature == 1) {
            if(isset($this->data['signature']->signature) || !($this->data['signature']->signature) ) { 
    ?>
                <br>
                <?php 
                    echo $this->data['signature']->signature;
            } 
        }
    ?> 
</div>
<?php
$url_segment = $this->uri->segment(1);
if ($url_segment == 'email_New' || $url_segment == 'email_new') {
?>
<input type="hidden" name="emailmodule" id="emailmodule" value="email_New">
<?php }else if($url_segment == 'documents'){ ?>
<input type="hidden" name="emailmodule" id="emailmodule" value="email">
<?php } ?>
<?php // <editor-fold defaultstate="collapsed" desc="Email Conversation">?>
<div class="col-lg-9 animated fadeInRight email_conversation" style="display: none;"></div>
<?php // </editor-fold>?>

<script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>
<script>
    var url = '<?php echo base_url() ?>';
    var current_user_injs = '<?php echo $this->session->userdata('user_data')['initials'] ?>';
    var current_segment = '<?php echo $this->uri->segment(1); ?>';
    $(document).on('click',".compose-mail",function (e) {
        $("#check_click_unread").val("0");
        $('.displayattatchment').hide();
        $('.fileremove').remove();
        $('#totalFileSize').val('0');
        $("#send_mail_form")[0].reset();
        $('.select2-selection.select2-selection--multiple').focus();
    });

	$(function() {
        $('#afiles').bind("change", function() {
            var fileSize = parseInt($('#totalFileSize').val());
            var formData = new FormData();
            var fileName = document.getElementById('afiles').files;
            var fi = document.getElementById('afiles');
            var fsize = '';
            for (var i = 0, len = document.getElementById('afiles').files.length; i < len; i++) {
                var fsize = fi.files.item(i).size / 1024;
                fileSize = fileSize + fsize;
                if(fsize < 20001) {
                    formData.append("file" + i, document.getElementById('afiles').files[i]);
		        } else {
                    var filenameerror = fi.files.item(i).name;
                    swal({
                        title: "Alert",
                        text: "Your " + filenameerror + " file size " + Math.round(fsize/1024) + " MB. Please upload file size up to 20 MB",
                        type: "warning"
                    });
                    var input = $("#afiles");
                    var fileName = input.val();
                    if(fileName) {
                        input.val('');
                    }
                }
            }

            if(fileSize >= 20001) {
                swal({
                    title: "Alert",
                    text: "Total size of all files including the current file which you have selected is " + Math.round(fileSize/1024) + " MB. Please upload files which are in total of size up to 20 MB",
                    type: "warning"
                });
                fileSize = fileSize - fsize;
                $("#afiles").val('');
                return false;
            }

            $('#totalFileSize').val(fileSize);
            var chkmodulee = $('#emailmodule').val();
            if(chkmodulee == 'email_New' || chkmodulee == 'email_new'){
                var neweUrl = '<?php echo base_url(); ?>email_new/emailattaupload';
            }else{
                var neweUrl = '<?php echo base_url('email/emailattaupload'); ?>';
            }

            $.ajax({
                url: neweUrl,
                type: 'post',
                data: formData,
                dataType: 'html',
                async: true,
                processData: false,
                contentType: false,
                beforeSend: function () { $('#selectedloader').addClass('loader');},
                complete: function () {
                $('#selectedloader').removeClass('loader');
                },
                success : function(data) {
                    var input = $("input[type=file]");
                    var fileName = input.val();

                    if(fileName) {
                        input.val('');
                    }
                    $('#selectedFiles').append(data);

                },
                error : function(request) {
                    console.log(request.responseText);
                }
            });
        });
    });


    $(document).on('click',".remove",function (e) {
        var a = $(this).parent().css('display','none');
        var imagename = $(this).data('image');
        var chkmodulee = $('#emailmodule').val();
            if(chkmodulee == 'email_New' || chkmodulee == 'email_new'){
                var neweUrl = '<?php echo base_url(); ?>email_new/emailattaremove';
            }else{
                var neweUrl = '<?php echo base_url('email/emailattaremove'); ?>';
            }

        $.ajax({
            url: neweUrl,
            type: 'post',
            data: {'imagename': imagename},
            success : function(data) {
                removedFileSize = data.split(':')[1];
                totalFileSize = $('#totalFileSize').val() - removedFileSize;
                console.log(totalFileSize);
                $('#totalFileSize').val(totalFileSize);
            },
            error : function(request) { }
        });
    });
    var editor;
    $(document).on('click',"#includesignature",function (e) {
        $('#includesignature').attr("disabled",true);
        editor = CKEDITOR.instances['mailbody'];
        var focusManager = CKEDITOR.focusManager(editor);
        focusManager.lock();
        editor.insertHtml( $("div.signaturediv").html());
    }
    );
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#check_click_unread").val("0");
        $('a.navbar-minimalize.minimalize-styl-2.btn.btn-primary').click(function () {
        setTimeout(function () {
                n.columns.adjust();
            }, 100)
        });
    });
</script>