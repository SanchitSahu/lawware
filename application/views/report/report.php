<link href="<?php echo base_url('assets'); ?>/css/custom-report.css" rel="stylesheet">

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Reports</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Reports</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="reportTable">
                            <thead>
                                <tr>
                                    <th>Report Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($reportName)) {
                                    foreach ($reportName as $key => $val) :
                                        ?>

                                        <tr>
                                            <td><?php echo $val['name']; ?></td>
                                            <?php if (isset($val['type']) && $val['type'] == 'modal') { ?>
                                                <td align="center"><a class="btn btn-circle-action btn-circle btn-primary activity_edit <?php echo $val['field']; ?>" data-toggle="modal" data-target="<?php echo '#' . $val['field']; ?>" data-dismiss="modal" href="JavaScript:Void(0);" title="View"><i class="icon fa fa-eye"></i></a></td>
                                            <?php } else if (isset($val['type']) && $val['type'] == 'url') { ?>
                                                <td align="center"><a class="btn btn-circle-action btn-circle btn-primary marginClass activity_edit" target="_blank" href="<?php echo base_url() . $val['field'] ?>" title="View"><i class="icon fa fa-eye"></i></a></td>
                                            <?php } else { ?>
                                                <td align="center"></td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    endforeach;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $tabindex = 1; ?>

<!-- MODAL FOR CASE COUNT REPORT SEARCH FORM [START] -->
<div id="case_count" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Count Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseCountReportForm" id="caseCountReportForm" method="post">
                    <input type="hidden" id="CaseCountReportUrl" value="<?php echo base_url(); ?>reports/getFilterCasecount"/>
                    <div class="form-group form-inline">
                        <label class="col-md-2">Report</label>
                        <select class="form-control" id="isType" name="isType">
                            <option value="byType">By Atty/Type</option>
                            <option value="byMonth">By Atty/All Months</option>
                        </select>
                    </div>
                    <div class="form-group legendDiv">
                        <div class="float-e-margins scheduler-border">
                            <div class="ibox-title">
                                <h5 id="casecount-isType">By Atty/Type</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-5 no-left-padding">Search By</label>
                                            <div class="col-sm-7 no-padding marg-bot5">
                                                <select class="form-control"  name="searchBy">
                                                    <option value="attyResp_dateOpen">AttyResp / DateOpen</option>
                                                    <option value="attyResp_dateEntered">AttyResp / Date Entered</option>
                                                    <option value="attyResp_dateClosed">AttyResp / Date Closed</option>
                                                    <option value="attyHand_dateOpen">AttyHand / DateOpen</option>
                                                    <option value="attyHand_dateEntered">AttyHand / Date Entered</option>
                                                    <option value="attyHand_dateClosed">AttyHand / Date Closed</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" id="casecount-types">
                                            <label class="col-sm-5 no-left-padding">Types</label>
                                            <div class="col-sm-7 no-padding marg-bot5">
                                                <input class="form-control" type="text" name="types" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="checkbox m-r-xs">
                                                <label class="container-checkbox">&nbsp;Include unassigned attorneys
                                                    <input type="checkbox" name="unassign_attorneys">
                                                    <span class="checkmark" style="padding-right: 5px;"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-5 no-left-padding">Month</label>
                                            <div class="col-sm-7 no-padding marg-bot5">
                                                <select class="form-control" name="month">
                                                    <?php foreach ($months as $key => $val) : ?>
                                                        <option value="<?php echo $key; ?>" <?php if (date('n') == $key) { ?> selected <?php } ?>><?php echo $val; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" id="casecount-year">
                                            <label class="col-sm-5 no-left-padding">Year</label>
                                            <div class="col-sm-7 no-padding marg-bot5">
                                                <input class="form-control" type="number" name="year" onKeyPress="if (this.value.length == 4)
                                                            return false;" value="<?php echo date('Y'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="caseCountProceed" href="javascript:void(0)">Search</a>
                <a class="btn btn-md btn-danger" href="#" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE COUNT REPORT SEARCH FORM [END] -->

<!-- MODAL FOR CASE COUNT REPORT TABLE [START] -->
<div id="CaseCountpReport_list" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseCountReport_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Follow Up</th>
                                                <th>Client</th>
                                                <th>Quick Note</th>
                                                <th>Date / Time</th>
                                                <th>Next Cal Event</th>
                                                <th>Venue Card</th>
                                                <th>Venue Cal</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>AttyR</th>
                                                <th>AttyH</th>
                                                <th>ParaH</th>
                                                <th>Caption</th>
                                                <th>Date Open</th>
                                                <th>User Defined</th>
                                                <th>YourFileNo</th>
                                                <th>Atty Assigned</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printMarkupReport" title="Print">Print</a>
                        <button class="btn btn-md btn-danger pullCase" disabled title="Pull Case">Pull Case</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE COUNT REPORT TABLE [END] -->


<!-- MODAL FOR CASE REPORT SEARCH FORM [START] -->
<div id="CASE_REPORT" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseReportForm" id="caseReportForm" method="post">
                    <input type="hidden" id="CaseReportUrl" value="<?php echo base_url(); ?>reports/getFilterCaseReport"/>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Type / Case</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the type of case">
                                            <select name="casetype" id="Ccasetype" class="form-control select2Class" >
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (isset($filters['casetype']) && !empty($filters['casetype'])) {
                                                    foreach ($filters['casetype'] as $key => $val) :
                                                        ?>
                                                        <option value = "casetype-<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the case status">
                                            <select name="case_status" id="Ccase_status" class="form-control select2Class">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (isset($filters['casestat']) && !empty($filters['casestat'])) {
                                                    foreach ($filters['casestat'] as $key => $val) :
                                                        ?>
                                                        <option value="<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Card Type</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the card type (i.e. DR, DR-AME, DR-QME)">
                                            <select placeholder="Select Category" name="card_type" id="Ccard_type" class="form-control select2Class">
                                                <?php foreach ($filters['type'] as $key => $val) { ?>
                                                    <option value="<?php echo $val['type']; ?>"><?php echo $val['type']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div></div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Search By</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter type to Search by">
                                            <select id="searchBy" name="searchBy"  class="form-control select2Class">
                                                <option value="">Search by</option>
                                                <?php
                                                if (isset($filters['searchBy']) && !empty($filters['searchBy'])) {
                                                    foreach ($filters['searchBy'] as $key => $val) :
                                                        ?>
                                                        <option value="<?php echo $key; ?>" <?php echo ($key == 'cd.last') ? 'selected' : ""; ?> ><?php echo $val; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div></div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Search For</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" data-toggle="tooltip" title="Please Select Search By" data-placement="bottom" id="searchText" name="searchText" placeholder="Search for" class="form-control" autocomplete="off">
                                            <div style="display:none;">   <input type="text" autocomplete="off"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="ibox-title">
                                    <h5>Staff Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Resp</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter attorney(s) reponsible initials, separate by a comma">
                                            <select name="atty_resp" id="Catty_resp" class="form-control select2Class">
                                                <option value="">Select Atty Resp</option>
                                                <?php foreach ($filters['atty_resp'] as $key => $val) { ?>
                                                    <option value="atty_resp-<?php echo $val['atty_resp']; ?>"><?php echo $val['atty_resp']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter an attorney handling initials, separate by a comma">
                                            <select name="atty_hand" id="Catty_hand" class="form-control select2Class">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($filters['atty_hand'])) {
                                                    foreach ($filters['atty_hand'] as $key => $val) {
                                                        ?>
                                                        <option value="atty_hand-<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Para Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter paraletal(s) initials, separate by a comma">
                                            <select name="para_hand" id="Cpara_hand" class="form-control select2Class">
                                                <option value="">Select Para Hand</option>
                                                <?php
                                                if (isset($filters['para_hand']) && !empty($filters['para_hand'])) {
                                                    foreach ($filters['para_hand'] as $key => $val) :
                                                        ?>
                                                        <option value="para_hand-<?php echo $val['para_hand']; ?>"><?php echo $val['para_hand']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Sec Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter secretary(s) initals, separate by a comma">
                                            <select name="sec_hand" id="Csec_hand" class="form-control select2Class">
                                                <option value="">Select Sec Hand</option>
                                                <?php
                                                if (isset($filters['sec_hand']) && !empty($filters['sec_hand'])) {
                                                    foreach ($filters['sec_hand'] as $key => $val) :
                                                        ?>
                                                        <option value="sec_hand-<?php echo $val['sec_hand']; ?>"><?php echo $val['sec_hand']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="checkbox m-r-xs"> 
                                            <label class="container-checkbox">&nbsp;Primary Names only
                                                <input type="checkbox" name="primary_names" id="Cprimary_names"> 
                                                <span class="checkmark" style="padding-right: 5px;" title="Limit this search to only the primary names in a case"></span> 
                                            </label> 
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <div class="checkbox m-r-xs"> 
                                            <label class="container-checkbox">&nbsp;Close window 
                                                <input type="checkbox" name="close_window" id="Cclose_window"> 
                                                <span class="checkmark" style="padding-right: 5px;" title="Close this window after pulling a case"></span> 
                                            </label> 
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <div class="checkbox m-r-xs"> 
                                            <label class="container-checkbox">&nbsp;Include Deleted Cases 
                                                <input type="checkbox" name="deleted_cases"> 
                                                <span class="checkmark" style="padding-right: 5px;" title="Include the deleted case in the found case window"></span> 
                                            </label> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="CaseRepotProceed" title="Search for all matching case" data-url="<?php echo base_url(); ?>cases/case_details/">Search</a>
                <button class="btn btn-md btn-danger" title="Clear" id="resetCaseReportFilter" type="button">Clear</button>
                <a class="btn btn-md btn-danger" href="#" data-dismiss="modal" title="Close this window">Cancel</a>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE REPORT SEARCH FORM [END] -->

<!-- MODAL FOR CASE REPORT TABLE [START] -->
<div id="case_report_list" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closetable" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-8" id="CaseRepTable">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseReport_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Name</th>
                                                <th>Attyr</th>
                                                <th>Attyh</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>Phone</th>
                                                <th>Your File No</th>
                                                <th>Follow Up</th>
                                                <th>Date Entered</th>
                                                <th>Parah</th>
                                                <th>Sech</th>
                                                <th>SS No</th>
                                                <th>Card Type</th>
                                                <th>Cd Code</th>
                                                <th>Firm Name</th>
                                                <th>Short Default</th>
                                                <th>Caption</th>
                                                <th>Mail 1</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-4" id="CaseDetails">
                                <div class="ibox-content" style="max-height: 550px; overflow-y: auto;">
                                    <div class="row">
                                        <div class="col-md-4"><strong>Name</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-name"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Firmname</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-firnmame"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Card Type</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-cardtype"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Phone</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-phone"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>SS No.</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-ssno"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Type of Case</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-casetype"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Case Status</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-casestatus"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Followup</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-followup"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Date Entered</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-dateentered"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Your File no</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-fileno"></span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Staff</strong></div>
                                        <div class="col-md-1"><strong>:</strong></div>
                                        <div class="col-md-7"><span id="Caserow-staff"></span></div>
                                    </div>
                                    <div class="row marg-top15">
                                        <div class="col-md-12 text-center">
                                            <strong>Dates of injury</strong>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" id="row-injurydates">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row marg-top15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="checkbox m-r-xs pull-left"> 
                                    <label class="container-checkbox">&nbsp;Unique
                                        <input type="checkbox" id="Ctable_unique" name="Ctable_unique"> 
                                        <span class="checkmark" style="padding-right: 5px;" title="Do not display duplicates"></span> 
                                    </label> 
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group pull-right">
                                <div class="checkbox m-r-xs"> 
                                    &nbsp;Alternate Format
                                    <label class="container-checkbox casetable"> 
                                        <input type="checkbox" id="Ctable_altFormat">
                                        <span class="checkmark" style="padding-right: 5px;"></span> 

                                    </label> 
                                </div>
                                <div class="checkbox m-r-xs"> 
                                    &nbsp;Auto Pull
                                    <label class="container-checkbox casetable"> 
                                        <input type="checkbox" id="Ctable_AutoPull">
                                        <span class="checkmark" style="padding-right: 5px;" title="Automatically pull the client card if only one name is found"></span> 
                                    </label> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="OpenCase" data-url="<?php echo base_url(); ?>cases/case_details/" title="Open Selected Case" href="javascript:void(0)">Open</a>
                        <a class="btn btn-md btn-primary" id="printCaseReport" title="Print" href="javascript:void(0)">Print</a>
                        <a class="btn btn-md btn-danger closetable" href="#" title="Cancel" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE REPORT TABLE [END] -->


<!-- MODAL FOR CASE REFERRAL REPORT SEARCH FORM [START] -->
<div id="referral_report" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Referral Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseReferralForm" id="caseReferralForm" method="post">
                    <input type="hidden" id="ReferralreportUrl" value="<?php echo base_url(); ?>reports/getSearchReferralReport">
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ibox-title">
                                    <h5>Case Referral Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Type of Case</label>
                                        <div class="col-sm-5 no-padding marg-bot5" title="Enter all or part of the type of case">
                                            <select name="casetype" class="form-control select2Class">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (isset($filters['casetype']) && !empty($filters['casetype'])) {
                                                    foreach ($filters['casetype'] as $key => $val) :
                                                        ?>
                                                        <option value = "<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div></div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                        <div class="col-sm-5 no-padding marg-bot5" title="Enter all or part of the case status">
                                            <select name="case_status" class="form-control select2Class">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (isset($filters['casestat']) && !empty($filters['casestat'])) {
                                                    foreach ($filters['casestat'] as $key => $val) :
                                                        ?>
                                                        <option value="<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Hand</label>
                                        <div class="col-sm-5 no-padding marg-bot5" title="Enter an attorney's initials, separate by commas">
                                            <select name="atty_hand" class="form-control select2Class">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($filters['atty_hand'])) {
                                                    foreach ($filters['atty_hand'] as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">D/R</label>
                                        <div class="col-sm-5 no-padding marg-bot5">
                                            <div class="input-group date-rangepicker">
                                                <input type="text" id="start_dr_date" class="input-small form-control" name="start_d_r" placeholder="Enter Starting Date" title="Enter Starting Date" readonly autocomplete="off"/>
                                                <span class="input-group-addon">To</span>
                                                <input type="text" id="end_dr_date" class="input-small form-control" name="end_d_r" placeholder="Enter Ending Date" title="Enter Ending Date" readonly autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">R/B</label>
                                        <div class="col-sm-5 no-padding marg-bot5">
                                            <input type="number" name="rb" class="form-control r_b" value="" placeholder="Enter Card No" title="Enter the card no or leave it blank for all" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="getReferralReport" title="Run the report">Search</a>
                <button class="btn btn-md btn-danger" title="Clear" id="resetReferralReportFilter" type="button">Clear</button>
                <a class="btn btn-md btn-danger" href="#" data-dismiss="modal" title="Cancel the report">Cancel</a>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE REFERRAL REPORT SEARCH FORM [END] -->

<!-- MODAL FOR REFERRAL REPORT TABLE [STARTS] -->
<div id="case_referral_list_modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Referral Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseReferralReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Case Status</th>
                                                <th>Attyh</th>
                                                <th>D/R</th>
                                                <th>Referred by</th>
                                                <th class="row-width">Case Caption</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <select name="" class="form-control select2Class pull-left">
                            <option value="">Normal</option>
                            <option value="">Detailed</option>
                            <option value="">Full</option>
                        </select>
                        <a class="btn btn-md btn-primary" id="printReferrall" title="Print" href="javascript:void(0)">Print</a>
                        <a class="btn btn-md btn-danger" href="#" title="Cancel" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR REFERRAL REPORT TABLE [ENDS] -->

<!-- MODAL FOR TASK REPORT [START] -->
<div id="task_report" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Task Report</h4>
            </div>
            <form name="taskActitvityForm" id="taskActitvityForm" method="post">
            <div class="modal-body">
                
                    <input type="hidden" id="TaskReportUrl" value="<?php echo base_url(); ?>reports/getFilterTaskReport">
                    <input type="hidden" id="taskpoolReportUrl" value="<?php echo base_url(); ?>reports/getTaskpoolrepot">
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Type of Case</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the type of case">
                                            <select name="Tcasetype" class="form-control select2Class" title="Select Case Type">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (isset($filters['casetype']) && !empty($filters['casetype'])) {
                                                    foreach ($filters['casetype'] as $key => $val) :
                                                        ?>
                                                        <option value = "<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the case status">
                                            <select name="Tcase_status" class="form-control select2Class">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (isset($filters['casestat']) && !empty($filters['casestat'])) {
                                                    foreach ($filters['casestat'] as $key => $val) :
                                                        ?>
                                                        <option value="<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div></div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter an attroney's initials, separated by commas">
                                            <select name="Tatty_hand" class="form-control select2Class">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($filters['atty_hand'])) {
                                                    foreach ($filters['atty_hand'] as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>User Defined Fields</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">From/to</label>
                                        <div class="col-sm-4 no-padding marg-bot5" title="Enter the initials of who the task is from, separated by comma">
                                            <select name="Twhofrom" class="form-control select2Class">
                                                <option value="">Select From</option>
                                                <?php
                                                if (isset($Stafflist) && !empty($Stafflist)) {
                                                    foreach ($Stafflist as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-4 no-padding marg-bot5" title="Enter the initials of who the task is assigned to, separated by comma">
                                            <select name="Twhoto" class="form-control select2Class">
                                                <option value="">Select To</option>
                                                <?php
                                                if (isset($Stafflist) && !empty($Stafflist)) {
                                                    foreach ($Stafflist as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Beg/End Date</label>
                                        <div class="col-sm-8 no-padding marg-bot5">
                                            <div class="input-group date-rangepicker">
                                                <input type="text" id="start_task_date" class="input-small form-control date-range" name="start_task_date" placeholder="From Date" title="Find task to be completed starting with this date (usually left blank)" readonly autocomplete="off"/>
                                                <span class="input-group-addon">To</span>
                                                <input type="text" id="end_task_date" class="input-small form-control date-range" name="end_task_date" placeholder="To Date" title="Find task to be completed up to this date" readonly autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Task</label>
                                        <div class="col-sm-8 no-padding">
                                            <input type="text" name="Tevent" class="form-control" title="Enter all or part of the task event" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="checkbox m-r-xs"> 
                                            <label class="container-checkbox">&nbsp;Contains
                                                <input type="checkbox" name="Tcontains"> 
                                                <span class="checkmark" style="padding-right: 5px;"></span> 
                                            </label> 
                                        </div>
                                        <div class="checkbox m-r-xs"> 
                                            <label class="container-checkbox">&nbsp;Display Completed Tasks
                                                <input type="checkbox" name="TisCompleted"> 
                                                <span class="checkmark" style="padding-right: 5px;"></span> 
                                            </label> 
                                        </div>
                                        <div class="checkbox m-r-xs"> 
                                            <label class="container-checkbox">&nbsp;Only Display High Priority Tasks
                                                <input type="checkbox" name="TisHighPriority"> 
                                                <span class="checkmark" style="padding-right: 5px;"></span> 
                                            </label> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <div class="pull-left form-group"> 
                    <select name="filterType" class="form-control select2Class">
                        <option value="individual">Individual</option>
                        <option value="noact">No Activity</option>
                    </select>
                </div>
                                    
                <a class="btn btn-md btn-primary" id="TaskRepotProceed" title="Run the report" data-url="<?php echo base_url(); ?>cases/case_details/">Search</a>
                <button class="btn btn-md btn-danger" title="Clear Filter" id="resetTaskReportFilter" type="button">Clear</button>
                <a class="btn btn-md btn-danger" href="javascript:void(0)" data-dismiss="modal" title="Close this window">Cancel</a>
                <a class="btn btn-md btn-primary pull-right" id="TaskPoolReport" href="javascript:void(0)" title="Pool Report">Pool Report</a>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL FOR TASK REPORT [END] -->

<!-- MODAL FOR TASK REPORT TABLE [START] -->
<div id="taskReportListing" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Task Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped hi" id="tastReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Case Status</th>
                                                <th>Atty Hand</th>
                                                <th>Finish Date</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Task</th>
                                                <th>Name</th>
                                                <th>Type Task</th>
                                                <th>Priority</th>
                                                <th>Date Assigned</th>
                                                <th>Completed</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#" id="printTaskList" title="Print">Print</a>
                        <a class="btn btn-md btn-danger" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                        <a class="btn btn-md btn-primary" id="OpenRelatedCase" data-url="<?php echo base_url(); ?>cases/case_details/" title="Open Selected Case" href="javascript:void(0)">Open</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR TASK REPORT TABLE [END] -->

<!-- MODAL FOR TASK POOL REPORT TABLE [START] -->
<div id="taskpoolReportlist" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pool Task Report</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped hi" id="taskpoolReport_table">
                                        <thead>
                                            <tr>
                                                <th style="min-width: 150px;">Who To</th>
                                                <th>Total</th>
                                                <th>Total Pending</th>
                                                <th>Last Three Months Completed</th>
                                                <th>To date Completed</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#" id="printTaskPool" title="Print">Print</a>
                        <a class="btn btn-md btn-danger" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR TASK REPORT TABLE [END] -->



<!-- MODAL FOR MARKUP REPORT[START] --->
<div id="markup_report" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report</h4>
            </div>
            <div class="modal-body">
                <form name="markuReportForm" id="markuReportForm" method="post">
                    <input type="hidden" id="MarkupReportUrl"  value="<?php echo base_url(); ?>reports/getFilterMarkupReport">
                    <div class="float-e-margins">
                        <div class="ibox-title">
                            <h5>Case Filters</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Type of Case</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <select name="case_type" class="form-control select2Class" title="Select Case Type">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (isset($casetype) && !empty($casetype)) {
                                                    foreach ($casetype as $k => $v) :
                                                        ?>
                                                        <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <select name="case_status" class="form-control select2Class" title="Select Case Status">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (isset($casestat) && !empty($casestat)) {
                                                    foreach ($casestat as $k => $v) :
                                                        ?>
                                                        <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Resp</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <select name="atty_resp" class="form-control select2Class" title="Select Atty Resp">
                                                <option value="">Select Atty Resp</option>
                                                <?php
                                                if (isset($atty_resp) && !empty($atty_resp)) {
                                                    foreach ($atty_resp as $k => $v) :
                                                        ?>
                                                        <option value="<?php echo $v->atty_resp; ?>"><?php echo $v->atty_resp; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <select name="atty_hand" class="form-control select2Class" title="Select Atty Hand">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (isset($atty_hand) && !empty($atty_hand)) {
                                                    foreach ($atty_hand as $k => $v) :
                                                        ?>
                                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Para Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <select name="para_hand" class="form-control select2Class" title="Select Para Hand">
                                                <option value="">Select Para Hand</option>
                                                <?php
                                                if (!empty($para_hand)) {
                                                    foreach ($para_hand as $k => $v) :
                                                        ?>
                                                        <option value="<?php echo $v->para_hand; ?>"><?php echo $v->para_hand; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Card Vanue</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" class="form-control" name="venue" title="Card Vanue" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-6 no-left-padding">Markup Date Begin/End</label>
                                        <div class="col-sm-6 no-padding marg-bot5">
                                            <div class="input-group date-rangepicker">
                                                <input type="text" id="start_dr_date" class="input-small form-control date-range" name="start_followup" placeholder="From Date" title="From Date" readonly autocomplete="off"/>
                                                <span class="input-group-addon">To</span>
                                                <input type="text" id="end_dr_date" class="input-small form-control date-range" name="end_followup" placeholder="To Date" title="To Date" readonly autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-6 no-left-padding">Date Open Begin/End</label>
                                        <div class="col-sm-6 no-padding marg-bot5">
                                            <div class="input-group date-rangepicker">
                                                <input type="text" id="start_date_open" class="input-small form-control date-range" name="start_date_open" placeholder="From Date" title="From Date" readonly autocomplete="off"/>
                                                <span class="input-group-addon">To</span>
                                                <input type="text" id="end_date_open" class="input-small form-control date-range" name="end_date_open" placeholder="To Date" title="To Date" readonly autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-6 no-left-padding">UDF</label>
                                        <div class="col-sm-6 no-padding marg-bot5">
                                            <input type="text" class="form-control" title="UDF" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="checkbox m-r-xs"> 
                                            <label class="container-checkbox">&nbsp;Remove calendar duplicates 
                                                <input type="checkbox" name="no_injury"> 
                                                <span class="checkmark" style="padding-right: 5px;"></span> 
                                            </label> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                        <a class="btn btn-md btn-primary" id="getMarkUpRecord" title="Search">Search</a>
                        <a class="btn btn-md btn-danger" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-md btn-primary pull-right" href="#">S</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR MARKUP REPORT[END] --->

<!-- MODAL FOR MARKUP REPORT TABLE [START] -->
<div id="markupReport_Content" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="markupReport_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Follow Up</th>
                                                <th>Client</th>
                                                <th>Quick Note</th>
                                                <th>Date / Time</th>
                                                <th>Next Cal Event</th>
                                                <th>Venue Card</th>
                                                <th>Venue Cal</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>AttyR</th>
                                                <th>AttyH</th>
                                                <th>ParaH</th>
                                                <th>Caption</th>
                                                <th>Date Open</th>
                                                <th>User Defined</th>
                                                <th>YourFileNo</th>
                                                <th>Atty Assigned</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-danger" id="printMarkupReport" title="Print">Print</a>
                        <button class="btn btn-md btn-danger pullCase" disabled title="Pull Case">Pull Case</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR MARKUP REPORT TABLE [END] -->


<!-- Modal for case injury report [start] -->
<div id="case_injury_report" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Injury Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseInjuryForm" id="caseInjuryForm" method="post">
                    <input type="hidden" id="InjuryreportUrl" value="<?php echo base_url(); ?>reports/getSearchInjuryReport">
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Type of Case</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the type of case">
                                            <select name="casetype" class="form-control select2Class">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (isset($filters['casetype']) && !empty($filters['casetype'])) {
                                                    foreach ($filters['casetype'] as $key => $val) :
                                                        ?>
                                                        <option value = "<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the case status">
                                            <select name="case_status" class="form-control select2Class">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (isset($filters['casestat']) && !empty($filters['casestat'])) {
                                                    foreach ($filters['casestat'] as $key => $val) :
                                                        ?>
                                                        <option value="<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter an attorney's initials, separate by commas">
                                            <select name="atty_hand" class="form-control select2Class">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($filters['atty_hand'])) {
                                                    foreach ($filters['atty_hand'] as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Injury Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Status</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
<!--                                            <input type="text" name="injury_status" placeholder="Enter Status" class="form-control" title="Enter all or part of the status of the injury" autocomplete="off"/>-->
                                            <select name="injury_status" id="injury_status" class="form-control select2Class">
                                                <option value="">Select Status</option>
                                                <option value="OPEN">OPEN</option>
                                                <option value="CLOSED">CLOSED</option>
                                                <option value="REOPEN">REOPEN</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">EAMS/WCAB No (contains)</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" name="i_wcab" class="form-control" placeholder="Enter EAMS/WCAB No" title="Enter all or part of the WCAB No" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Claim No</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" name="i_claimno" class="form-control" placeholder="Enter Claim No" title="Enter all or part of the Claim No" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Date of Injury</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" id="date_of_injury" class="input-small form-control" name="doi" placeholder="Enter Date of Injury" title="Date of injury contains?" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Parts of Body</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" name="pob" class="form-control" autocomplete="off" placeholder="Enter Other body Parts" title="Parts of body contains?" autocomplete="off"/>
                                            <select name="body_parts" class="form-control select2Class" title="Select Parts of body">
                                                <option value="">Select Parts of body</option>
                                                <?php
                                                $bodyParts = json_decode(bodyParts);
                                                if (!empty($bodyParts)) {
                                                    foreach ($bodyParts as $key => $val) :
                                                        ?>
                                                        <option value="<?php echo $key ?>"><?php echo $key . " " . $val; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">DOI Month/Year</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" name="doi_my" class="form-control doi_my" placeholder="mm/YYYY" title="Enter month and four digit year, i.e 09/2001" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <div class="checkbox m-r-xs pull-left"> 
                            <label class="container-checkbox">&nbsp;No Injury
                                <input type="checkbox" name="no_injury"> 
                                <span class="checkmark" style="padding-right: 5px;" title="Show all cases with no matching injuries for the injury filters"></span> 
                            </label> 
                        </div>
                        <a class="btn btn-md btn-primary" id="getInjuryReport" title="Run the report">Search</a>
                        <button class="btn btn-md btn-danger" title="Clear" id="resetCaseInjuryReportFilter" type="button">Clear</button>
                        <a class="btn btn-md btn-danger" href="#" data-dismiss="modal" title="Cancel the report">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal for case injury report [end] -->

<!-- MODAL FOR CASE INJURY REPORT TABLE [STARTS] -->
<div id="case_injury_list_modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Injury Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseInjurylReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Case Status</th>
                                                <th>Atty Hand</th>
                                                <th>Client</th>
                                                <th>Employer</th>
                                                <th>Carrier</th>
                                                <th>Attorney</th>
                                                <th>Date of Injury</th>
                                                <th>Parts of Body</th>
                                                <th>How Occurred</th>
                                                <th>WCAB No.</th>
                                                <th>Claim No.</th>
                                                <th>DOI2</th>
                                                <th>Your File No.</th>
                                                <th>Status 2</th>
                                                <th>Venue</th>
                                                <th>Occupation</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printInury" title="Print" href="javascript:void(0)">Print</a>
                        <a class="btn btn-md btn-danger" href="#" title="Cancel" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE INJURY REPORT TABLE [ENDS] -->


<!-- MODAL FOR CASE ACTIVITY REPORT SEARCH FORM [START] -->
<div id="case_activity_report" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Activity Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseActivityForm" id="caseActivityForm" method="post">
                    <input type="hidden" id="caseActivityUrl" value="<?php echo base_url(); ?>reports/getCaseactivity"/>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Type of Case</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the type of case">
                                            <select name="casetype" class="form-control select2Class">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (isset($filters['casetype']) && !empty($filters['casetype'])) {
                                                    foreach ($filters['casetype'] as $key => $val) :
                                                        ?>
                                                        <option value = "<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter all or part of the case status">
                                            <select name="case_status" class="form-control select2Class">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (isset($filters['casestat']) && !empty($filters['casestat'])) {
                                                    foreach ($filters['casestat'] as $key => $val) :
                                                        ?>
                                                        <option value="<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Atty Hand</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Enter an attorney's initials, separate by commas">
                                            <select name="atty_hand" class="form-control select2Class">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($filters['atty_hand'])) {
                                                    foreach ($filters['atty_hand'] as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Activity Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Categories</label>
                                        <div class="col-sm-7 no-padding marg-bot5" title="Select Category">
                                            <select name="categories" class="form-control select2Class">
                                                <option value="">Select Category</option>
                                                <?php foreach ($category as $k => $v) { ?>
                                                    <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Beg/End Date</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <div class="input-group date-rangepicker">
                                                <input type="text" id="start_ca_date" class="input-small form-control date-range" name="start_ca_date" placeholder="Beg Date" title="Enter the starting case activity date" readonly autocomplete="off"/>
                                                <span class="input-group-addon">To</span>
                                                <input type="text" id="end_ca_date" class="input-small form-control date-range" name="end_ca_date" placeholder="End Date" title="Enter the ending case activity date" readonly autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Event</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="text" name="event" class="form-control" placeholder="Enter Event" title="Enter all or part of the case event" autocomplete="off"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 no-left-padding">Max Category</label>
                                        <div class="col-sm-7 no-padding marg-bot5">
                                            <input type="number" name="max_category" min="0"  max-length="3" class="form-control" title="Enter the maximum category to include in the report" placeholder="Enter Max Category" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 marg-top10">
                                <?php /*<div class="form-group">
                                    <label class="col-sm-5 no-left-padding">Search Documents</label>
                                    <div class="col-sm-7 no-padding marg-bot5">
                                        <input type="text" class="form-control" name="documents" id="documents" title="Search Documents" autocomplete="off"/>
                                    </div>
                                </div>*/?>
<!--                                <div class="form-group">
                                    <select class="form-control pull-left" name="reportWith">
                                        <option>Normal</option>
                                        <option>Fee Report</option>
                                        <option>Cost Report</option>
                                    </select>
                                </div>-->
                            </div>
                            <div class="col-sm-6">
                                <div class="checkbox m-r-xs"> 
                                    <label class="container-checkbox">&nbsp;Use Format First Name-Last Name 
                                        <input type="checkbox" name="isFLName"> 
                                        <span class="checkmark" style="padding-right: 5px;"></span> 
                                    </label> 
                                </div>
                                <div class="checkbox m-r-xs"> 
                                    <label class="container-checkbox">&nbsp; No Activity
                                        <input type="checkbox" name="noActivity"> 
                                        <span class="checkmark" style="padding-right: 5px;" title="Show all cases with no activity for the specified begin/end dates"></span> 
                                    </label> 
                                </div>
                                <div class="checkbox m-r-xs"> 
                                    <label class="container-checkbox">&nbsp;Event Contains
                                        <input type="checkbox" name="event_contains"> 
                                        <span class="checkmark" style="padding-right: 5px;"></span> 
                                    </label> 
                                </div>
                                <div class="checkbox m-r-xs"> 
                                    <label class="container-checkbox">&nbsp;Most Recent
                                        <input type="checkbox" name="most_recent"> 
                                        <span class="checkmark" style="padding-right: 5px;" title="Only show the most recent activity for each case"></span> 
                                    </label> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="getCaseActivityReport" title="Run the report" href="javascript:void(0)">Search</a>
                        <button class="btn btn-md btn-danger" title="Clear" id="resetcaseactivityForm" type="button">Clear</button>
                        <a class="btn btn-md btn-danger" href="#" data-dismiss="modal" title="Cancel the report">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE ACTIVITY REPORT SEARCH FORM [END] -->

<!-- MODAL FOR CASE ACTIVITY REPORT TABLE [STARTS] -->
<div id="caseActivityReportData" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closetable" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Activity Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseActivityReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>AttyHand</th>
                                                <th>Date</th>
                                                <th>Who</th>
                                                <th>Name</th>
                                                <th>Event</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group  marg-top15">
                        <div class="row">
<!--                            <div class="col-md-3">
                                <label class="col-sm-5 no-left-padding">Money</label>
                                <div class="col-sm-7 no-padding marg-bot5">
                                    <select name="" class="form-control select2Class pull-left">
                                        <option value="">None</option>
                                        <option value="">Sum 1$</option>
                                        <option value="">Sum 2$</option>
                                        <option value="">Sum 3$</option>
                                        <option value="">Sum 4$</option>
                                        <option value="">Sum 5$</option>
                                    </select>
                                </div>
                            </div>-->
                            <div class="col-md-6 text-center">
                                <a class="btn btn-md btn-primary" id="printCaseActivityReport" title="Print" href="javascript:void(0)">Print</a>
                                <a class="btn btn-md btn-primary" id="CaseActPullCase" data-url="<?php echo base_url(); ?>cases/case_details/" title="Print" href="javaScript:void(0)">Pull Case</a>
                                <!--<a class="btn btn-md btn-primary" id="CaseActViewForm" title="Print" href="javascript:void(0)">View Form</a>-->
                                <a class="btn btn-md btn-danger closetable" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                            </div>
                            <div class="col-md-3">
                                <select name="" class="form-control select2Class pull-left">
                                    <option value="">Normal</option>
                                    <option value="">Detailed</option>
                                    <option value="">Detailed (2-lines)</option>
                                    <option value="">By Category</option>
                                    <option value="">By Category #2</option>
                                    <option value="">By Category #3</option>
                                    <option value="">By Category w page</option>
                                    <option value="">Normal/Initials</option>
                                    <option value="">Normal/Initials 1 line</option>
                                    <option value="">Normal/Initials 1 line & Time</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="Showdocument-modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closetable" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Search Notes and Documents</h4>
            </div>
            <div class="modal-body">
                <p>Searching documents that contains all the words in the list can take a long time. You may press then control key and type zero (CONTROL-ZERO) to End Task and break out of the search.</p>
                <p>Results in red indicate the document was too big to search and might not match.</p>
                <p>Results in blank indicate a match in the case activity notes.</p>
                <p>Results in light blue indicate a match in the document or attached file.</p>
                <p>NOTICE: The more information you enter on the left such as type of case, status, attorney handling the faster the search will be. Consider entering only the first letter of the type of case and case status or leave it blank for everything. Leaving it blank will take will take longer otherwise enter the first letter.</p>
                <p>Click Yes to Continue</p>
                <p></p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-md btn-primary" id="ConfirmSearchdocument" type="button">Yes</button>
                <a class="btn btn-md btn-danger" href="javascript:void(0)" data-dismiss="modal">No</a>
            </div>
        </div>
    </div>
</div>
<div id="Event-Containt-modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closetable" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Event Detail</h4>
            </div>
            <div class="modal-body">
                <p id="Event-Containt"></p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-danger" href="javascript:void(0)" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE ACTIVITY REPORT TABLE [ENDS] -->
<input type="hidden" id="APPLICATION_NAME" value="<?php echo APPLICATION_NAME ?>" name="APPLICATION_NAME" />
<script src="<?php echo base_url('assets'); ?>/js/custom/report.js"></script>










