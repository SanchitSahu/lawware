<style type="text/css">
    .dataTables_filter {
        display:none;
    }
    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
                box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
    }
    div.legendDiv {
        margin-top: 16px;
    }
    .input-group.date-rangepicker .date-range {
        font-size: 13px !important;
    }
    .noPadding.noPadding {
        text-align: left !important;
    }
/*    table {
        table-layout:fixed;
    }*/
    td{
        overflow:hidden;
        text-overflow: ellipsis;
    }
    .caseact_event{
        white-space: pre-line;
    }
    .row-width {
        white-space: pre-line !important;
    }
    a.btn-default.buttons-print {
        display: none !important;
    }
    table.dataTable tbody th,
    table.dataTable tbody td {
        white-space: nowrap;
    }
    table.dataTable,
    table.dataTable th,
    table.dataTable td {
      -webkit-box-sizing: content-box;
      -moz-box-sizing: content-box;
      box-sizing: content-box;
    }
    .scrollStyle
    {
     height:200px;overflow-x:auto;overflow-y:scroll;
    }
    @media print {
        .dataTables_wrapper th, td {
            white-space: normal !important;
        }
    }
    tr.selected {
        background-color:#acbad4 !important;
    }

</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Reports</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Reports</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="reportTable">
                            <thead>
                                <tr>
                                    <th>Group</th>
                                    <th>Report Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($reportName as $key=>$val) { ?>
                                <tr>
                                    <td>LAW</td>
                                    <td><?php echo $val['name']; ?></td>
                                    <?php if(isset($val['type']) && $val['type'] == 'modal') { ?>
                                        <td><a class="btn btn-xs btn-info marginClass activity_edit" data-toggle="modal" data-target="<?php echo '#'.$val['field']; ?>" data-dismiss="modal" href="javascript:;" title="View"><i class="icon fa fa-paste"></i> View</a></td>
                                    <?php }else if(isset($val['type']) && $val['type'] == 'url'){ ?>
                                        <td><a class="btn btn-xs btn-info marginClass activity_edit" target="_blank" href="<?php echo base_url().$val['field']?>" title="View"><i class="icon fa fa-paste"></i> View</a></td>
                                    <?php }else { ?>
                                        <td></td>
                                    <?php } ?>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Report - Case Count -->
<div id="case_count" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Report Count</h4>
            </div>
            <div class="modal-body">
                <form name="caseCountReportForm" id="caseCountReportForm">
                    <div class="form-inline">
                        <label class="col-md-2">Report</label>
                        <select class="form-control" id="isType" name="isType">
                            <option value="byType">By Atty/Type</option>
                            <option value="byMonth">By Atty/All Months</option>
                        </select>
                    </div>
                    <div class="form-group legendDiv" id="atty_type">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">By Atty/Type</legend>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-2">Search By</label>
                                    <select class="form-control col-md-10" name="searchBy">
                                        <option value="attyResp_dateOpen">AttyResp/DateOpen</option>
                                        <option value="attyResp_dateEntered">AttyResp/Date Entered</option>
                                        <option value="attyResp_dateClosed">AttyResp/Date Closed</option>
                                        <option value="attyHand_dateOpen">AttyHand/DateOpen</option>
                                        <option value="attyHand_dateEntered">AttyHand/Date Entered</option>
                                        <option value="attyHand_dateClosed">AttyHand/Date Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-2">Month</label>
                                    <select class="col-md-2 form-control">
                                        <?php foreach($months as $key=>$val) { ?>
                                        <option value="<?php echo $key; ?>" <?php if(date('n') == $key) { ?> selected <?php } ?>><?php echo $val; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label class="col-md-2">Year</label>
                                    <input class="col-md-3 form-control" type="number" name="year" onKeyPress="if(this.value.length==4) return false;" value="<?php echo date('Y'); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-2">Types</label>
                                    <input class="col-md-10 form-control" type="text" value="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-6">Include unassigned attorneys</label>
                                    <input class="col-md-3 form-control" type="checkbox" name="unassign_attorneys">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="form-group legendDiv" id="atty_months" style="display:none;">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">By Atty/All Months</legend>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-3">Search By</label>
                                    <select class="form-control col-md-9" name="isCC">
                                        <option value="attyResp_dateOpen">AttyResp/DateOpen</option>
                                        <option value="attyResp_dateEntered">AttyResp/Date Entered</option>
                                        <option value="attyResp_dateClosed">AttyResp/Date Closed</option>
                                        <option value="attyHand_dateOpen">AttyHand/DateOpen</option>
                                        <option value="attyHand_dateEntered">AttyHand/Date Entered</option>
                                        <option value="attyHand_dateClosed">AttyHand/Date Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-3">Year</label>
                                    <input class="col-md-3 form-control" type="number" name="year" min="1970" onKeyPress="if(this.value.length==4) return false;" value="<?php echo date('Y'); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-6">Include unassigned attorneys</label>
                                    <input class="col-md-3 form-control" type="checkbox" name="unassign_attorneys">
                                </div>
                            </div>
                        </fieldset>
                    </div>
<!--                    <div class="form-group text-center marg-top15">

                    </div>-->
              </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="caseCountProceed" href="#">Proceed</a>
                <a class="btn btn-md btn-primary" href="#" id="caseCountCancel">Cancel</a>
            </div>
        </div>
    </div>
</div>
<div id="proceed2" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Search for a Case</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Case Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Type/Case</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Card Type</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Search By</label>
                                    <select class="form-control"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Staff Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Atty Resp</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Para Hand</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Sec Hand</label>
                                    <select class="form-control"></select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 marg-top15">
                            <div class="form-group">
                                <label>Search For</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6 marg-top15">
                            <div class="form-group">
                                <span><label><input type="checkbox" class="i-checks"> Primary Names only </label></span>
                                <span><label><input type="checkbox" class="i-checks"> Close Window </label></span>
                                <span><label><input type="checkbox" class="i-checks"> Include Deleted Cases </label></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#proceed21" data-toggle="modal" data-dismiss="modal">Search</a>
                    <a class="btn btn-md btn-primary" href="#">Clear</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Close</a>
                    <a class="btn btn-md btn-primary" href="#">Save</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed1 modal end-->

    <div id="proceed21" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Found Cases</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="ibox-content marg-bot15 party-in">
                                <form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th width="40"></th>
                                                                <th>Case No</th>
                                                                <th>Name</th>
                                                                <th>Attyr</th>
                                                                <th>Attyh</th>
                                                                <th>Type</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                                <td>4340</td>
                                                                <td>Hubert Jessup</td>
                                                                <td>EAB</td>
                                                                <td>EAB</td>
                                                                <td>WC</td>
                                                                <td>Closed</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                   </div>
                               </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="ibox-content marg-bot15 party">
                                <form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="table-responsive">
                                                    <table class="table no-bottom-margin">
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Name :</strong></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>FirmName :</strong></td>
                                                                <td>Hubert Jessup</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>CardType :</strong></td>
                                                                <td>Employer</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Phone :</strong></td>
                                                                <td>818-769-8141</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>S S No :</strong></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Type of Case :</strong></td>
                                                                <td>PI</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Case Status :</strong></td>
                                                                <td>CL PI BX#1599 3/01=MTC</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Followup :</strong></td>
                                                                <td> / / </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Date Entered :</strong></td>
                                                                <td> / / </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Your File No. :</strong></td>
                                                                <td>4340-MHZ</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Staff :</strong></td>
                                                                <td>iamiran@tstarc.com</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <label>Dates of Injury</label>
                                        <textarea class="form-control" style="height: 150px;"></textarea>
                                   </div>
                               </form>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="inline-block-100">
                    <div class="form-group text-center marg-top15 pull-left">
                        <a class="btn btn-md btn-primary" href="#">Open</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Close</a>
                        <a class="btn btn-md btn-primary" href="#">Save</a>
                    </div>
                    <div class="form-group pull-right">
                        <span class="block"><label><input type="checkbox" class="i-checks"></label> Alternate Format </span>
                        <span class="block"><label><input type="checkbox" class="i-checks"></label> Close Window </span>
                        <span class="block"><label><input type="checkbox" class="i-checks"></label> Auto Pull </span>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed1 modal end-->

<div id="referral_report" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Case Referral Report</h4>
          </div>
          <div class="modal-body">
              <form name="caseReferralForm" id="caseReferralForm">
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox-title">
                                <h5>Case Referral Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select name="case_type" class="form-control select2Class">
                                        <option value="">Select Case Type</option>
                                    <?php foreach($casetype as $k=>$v) { ?>
                                        <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select name="case_status" class="form-control select2Class">
                                        <option value="">Select Case Status</option>
                                    <?php foreach($casestat as $k=>$v) { ?>
                                        <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select name="atty_hand" class="form-control select2Class">
                                        <option value="">Select Atty Hand</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>D/R</label>
<!--                                    <input type="text" name="d_r" class="form-control injurydate date-field" />-->
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_d_r" class="input-small form-control" name="start_d_r" placeholder="From Date" readonly=""/>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_d_r" class="input-small form-control" name="end_d_r" placeholder="To Date" readonly=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>R/B</label>
                                    <input type="text" name="rb" class="form-control" value="0"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
          </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="getReferralReport">Run</a>
                <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Cancel</a>
            </div>
        </div>
      </div>
    </div> <!--Proceed1 modal end-->

    <div id="case_referral_list_modal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Case Referral Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="caseReferralReport_table">
                                    <thead>
                                        <tr>
                                            <th>Case No</th>
                                            <th>Type</th>
                                            <th>Case Status</th>
                                            <th>Attyh</th>
                                            <th>D/R</th>
                                            <th>Referred by</th>
                                            <th>Case Caption</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" id="printReferrall" title="Print" href="#">Print</a>
                    <a class="btn btn-md btn-primary" href="#" title="Cancel" data-dismiss="modal">Cancel</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed1 modal end-->

    <div id="case_activity_report" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Case Activity Report</h4>
          </div>
          <div class="modal-body">
              <form name="caseActivityForm" id="caseActivityForm">
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Case Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select name="case_type" class="form-control select2Class">
                                        <option value="">Select Case Type</option>
                                    <?php foreach($casetype as $k=>$v) { ?>
                                        <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select name="case_status" class="form-control select2Class">
                                        <option value="">Select Case Status</option>
                                    <?php foreach($casestat as $k=>$v) { ?>
                                        <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select name="atty_hand" class="form-control select2Class">
                                        <option value="">Select Atty Hand</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Case Activity Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Categories</label>
                                    <select name="categories" class="form-control select2Class">
                                        <option value="">Select Category</option>
                                    <?php foreach($category as $k=>$v) { ?>
                                        <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Beg/End Date</label>
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_ca_d_r" class="input-small form-control date-range" name="start_ca_d_r" placeholder="From Date" readonly=""/>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_ca_d_r" class="input-small form-control date-range" name="end_ca_d_r" placeholder="To Date" readonly=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Event</label>
                                    <input type="text" name="event" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Max Category</label>
                                    <input type="number" name="max_category" min="0" max="999" max-length="3" class="form-control" value="999" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Search Documents</label>
                                <input type="text" class="form-control" name="documents" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 marg-top15">
                            <span class="block"><label><input type="checkbox" name="isFLName" class="i-checks"> Use Format First Name-Last Name </label></span>
                            <span class="block"><label><input type="checkbox" name="noActivity" class="i-checks"> No Activity </label></span>
                        </div>
                        <div class="col-sm-6 marg-top15">
                            <select class="form-control pull-left" name="reportWith">
                                <option>Normal</option>
                                <option>Fee Report</option>
                                <option>Cost Report</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" id="generateCaseActivity" title="Run" href="javaScript:void(0)">Run</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    <span class="pull-right"><label><input type="checkbox" class="i-checks"> Event Contains </label></span>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed4 modal end-->

    <div id="caseActivityReportData" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Case Activity Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="caseActivityReport_table">
                                    <thead>
                                        <tr>
                                            <th>Case No</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>AttyHand</th>
                                            <th>Date</th>
                                            <th>Who</th>
                                            <th>Name</th>
                                            <td>Event</td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" id="printCaseActivityReport" title="Print" href="#">Print</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed41 modal end-->

    <div id="case_injury_report" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Case Injury Report</h4>
          </div>
          <div class="modal-body">
              <form name="caseInjuryForm" id="caseInjuryForm">
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Case Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select name="case_type" class="form-control select2Class">
                                        <option value="">Select Case Type</option>
                                    <?php foreach($casetype as $k=>$v) { ?>
                                        <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select name="case_status" class="form-control select2Class">
                                        <option value="">Select Case Status</option>
                                    <?php foreach($casestat as $k=>$v) { ?>
                                        <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select name="atty_hand" class="form-control select2Class">
                                        <option value="">Select Atty Hand</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Case Injury Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <input type="text" name="injury_status" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>EAMS/WCAB No (contains)</label>
                                            <input type="text" name="i_wcab" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>Claim No</label>
                                            <input type="text" name="i_claimno" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Date of Injury</label>
                                            <input type="text" name="doi" class="form-control injurydate date-field" />
                                        </div>
                                        <div class="form-group">
                                            <label>Parts of Body</label>
                                            <input type="text" name="pob" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>DOI Month/Year</label>
                                            <input type="text" name="doi_my" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <span class="pull-left"><label><input type="checkbox" name="no_injury" class="i-checks"> No Injury </label></span>
                    <a class="btn btn-md btn-primary" id="getCaseReportModel" title="Run">Run</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    <span class="pull-right"><label><input type="checkbox" name="err_ignores" class="i-checks"> Ignore Errors </label></span>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed5 modal end-->

    <div id="proceed6" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Case Referral Report</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Case Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Advanced</label>
                                    <textarea class="form-control" style="height: 75px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>User Defined Fields</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Page</label>
                                    <select class="form-control">
                                        <option>Page 1</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Field</label>
                                    <select class="form-control">
                                        <option>Fees 1-3</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Content</label>
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Advanced</label>
                                    <textarea class="form-control" style="height: 75px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <span class="pull-left"><label><input type="checkbox" class="i-checks"> No Activity </label></span>
                    <a class="btn btn-md btn-primary" href="#proceed61" data-dismiss="modal" data-toggle="modal" title="Run">Run</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed6 modal end-->

    <div id="proceed61" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">User Defined Field Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th width="40"></th>
                                            <th>Case No</th>
                                            <th>Type</th>
                                            <th>Case Status</th>
                                            <th>AttyHand</th>
                                            <th>Name</th>
                                            <th>Fees 1-3</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>8109</td>
                                            <td>WC</td>
                                            <td>OP 2017-6-10</td>
                                            <td>EAB</td>
                                            <td>Gainer, Morgan</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#" title="Print">Print</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed61 modal end-->

    <div id="task_report" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Task Activity Report</h4>
          </div>
          <div class="modal-body">
              <form name="taskActitvityForm" id="taskActitvityForm">
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Case Filters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select name="case_type" class="form-control select2Class">
                                        <option value="">Select Case Type</option>
                                    <?php foreach($casetype as $k=>$v) { ?>
                                        <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select name="case_status" class="form-control select2Class">
                                        <option value="">Select Case Status</option>
                                    <?php foreach($casestat as $k=>$v) { ?>
                                        <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select name="atty_hand" class="form-control select2Class">
                                        <option value="">Select Atty Hand</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>User Defined Fields</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>From/to</label>
                                    <div class="input-group date-rangepicker">
                                        <select name="whofrom" class="form-control select2Class">
                                            <option value="">From User</option>
                                        <?php foreach($atty_hand as $k=>$v) { ?>
                                            <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                        <?php } ?>
                                        </select>
                                        <span class="input-group-addon">To</span>
                                        <select name="whoto" class="form-control select2Class">
                                            <option value="">To User</option>
                                        <?php foreach($atty_hand as $k=>$v) { ?>
                                            <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Beg/End Date</label>
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_d_req" class="input-small form-control date-range" name="start_d_req" placeholder="From Date" readonly=""/>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_d_req" class="input-small form-control date-range" name="end_d_req" placeholder="To Date" readonly=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Task</label>
                                    <input type="text" name="event" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <span><label><input type="checkbox" name="contains" class="i-checks"> Contains </label></span>
                                    <span><label><input type="checkbox" name="isCompleted" class="i-checks"> Display Completed Tasks </label></span>
                                    <span><label><input type="checkbox" name="isHighPriority" class="i-checks"> Only Display High Priority Tasks </label></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
          </div>
            <div class="modal-footer noPadding">
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control">
                            <option>Normal</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-md btn-primary" id="filterTaskReport" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-md btn-primary pull-right" href="#" title="Pool Report">Pool Report</a>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div> <!--Proceed7 modal end-->

    <div id="taskReportListing" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Task Activity Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped hi" id="tastReport_table">
                                    <thead>
                                        <tr>
                                            <th>Case No</th>
                                            <th>Type</th>
                                            <th>Case Status</th>
                                            <th>AttyHand</th>
                                            <th>Date</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th class="row-width">Task</th>
                                            <th>Name</th>
                                            <th>Type Task</th>
                                            <th>Priority</th>
                                            <th>Date Assigned</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#" id="printTaskList" title="Print">Print</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    <!--<a class="btn btn-md btn-primary pull-right pullCase" disabled >Pull Case</a>-->
                     <button class="btn btn-md btn-primary pullCase" disabled title="Pull Case">Pull Case</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed71 modal end-->

    <div id="markup_report" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Mark Up Report</h4>
            </div>
            <div class="modal-body">
              <form name="markuReportForm" id="markuReportForm">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Case Filters</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select name="case_type" class="form-control select2Class">
                                        <option value="">Select Case Type</option>
                                    <?php foreach($casetype as $k=>$v) { ?>
                                        <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select name="case_status" class="form-control select2Class">
                                        <option value="">Select Case Status</option>
                                    <?php foreach($casestat as $k=>$v) { ?>
                                        <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Resp</label>
                                    <select name="atty_resp" class="form-control select2Class">
                                        <option value="">Select Atty Resp</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select name="atty_hand" class="form-control select2Class">
                                        <option value="">Select Atty Hand</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Para Hand</label>
                                    <select name="para_hand" class="form-control select2Class">
                                        <option value="">Select Para Hand</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Card Vanue</label>
                                    <input type="text" class="form-control" name="venue" >
<!--                                    <select class="form-control"></select>-->
                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Markup Date Begin/End</label>
<!--                                    <div class="input-daterange" id="datepicker1">
                                        <input type="text" class="input-sm form-control" name="start" >
                                        <input type="text" class="input-sm form-control" name="end">
                                    </div>-->
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_followup" class="input-small form-control date-range" name="start_followup" placeholder="From Date" readonly=""/>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_followup" class="input-small form-control date-range" name="end_followup" placeholder="To Date" readonly=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Date Open Begin/End</label>
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_date_open" class="input-small form-control date-range" name="start_date_open" placeholder="From Date" readonly=""/>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_date_open" class="input-small form-control date-range" name="end_date_open" placeholder="To Date" readonly=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>UDF</label>
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <span><label><input type="checkbox" class="i-checks"> Remove calendar duplicates </label></span>
                                </div>
<!--                                <div class="form-group">
                                    <label>Advanced</label>
                                    <textarea class="form-control" style="height: 75px;"></textarea>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                        <a class="btn btn-md btn-primary" id="getMarkUpRecord" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-md btn-primary pull-right" href="#">S</a>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div> <!--Proceed8 modal end-->

    <div id="markupReport_Content" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mark Up Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="markupReport_dataTable">
                                    <thead>
                                        <tr>
                                            <th>Case No</th>
                                            <th>Follow Up</th>
                                            <th>Client</th>
                                            <th>Quick Note</th>
                                            <th>Date / Time</th>
                                            <th>Next Cal Event</th>
                                            <th>Venue Card</th>
                                            <th>Venue Cal</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>AttyR</th>
                                            <th>AttyH</th>
                                            <th>ParaH</th>
                                            <th>Caption</th>
                                            <th>Date Open</th>
                                            <th>User Defined</th>
                                            <th>YourFileNo</th>
                                            <th>Atty Assigned</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" id="printMarkupReport" title="Print">Print</a>
                    <button class="btn btn-md btn-primary pullCase" disabled title="Pull Case">Pull Case</button>

                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed81 modal end-->

    <div id="markup_report_udf" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mark Up Report With User Defined Fields</h4>
          </div>
          <div class="modal-body">
              <form name="markupWithUdf" id="markupWithUdf">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Case Filters</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select name="case_type" class="form-control select2Class">
                                        <option value="">Select Case Type</option>
                                    <?php foreach($casetype as $k=>$v) { ?>
                                        <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select name="case_status" class="form-control select2Class">
                                        <option value="">Select Case Status</option>
                                    <?php foreach($casestat as $k=>$v) { ?>
                                        <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Resp</label>
                                    <select name="atty_resp" class="form-control select2Class">
                                        <option value="">Select Atty Resp</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select name="atty_hand" class="form-control select2Class">
                                        <option value="">Select Atty Hand</option>
                                    <?php foreach($atty_hand as $k=>$v) { ?>
                                        <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Start / End Date</label>
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_udf_open" class="input-small form-control date-range" name="start_d_req" placeholder="From Date" readonly=""/>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_udf_open" class="input-small form-control date-range" name="end_d_req" placeholder="To Date" readonly=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Program</label>
                                    <input type="text" name="program" class="form-control" />
                                </div>
<!--                                <div class="form-group">
                                    <label>Advanced</label>
                                    <textarea class="form-control" style="height: 75px;"></textarea>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
          </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="form-inline col-md-6">
                        <label>Report</label>
                        <select class="form-control">
                            <option>Select Report Type</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <a class="btn btn-md btn-primary" id="getMarkupUdfReport" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div> <!--Proceed9 modal end-->

    <div id="showMarkupUdfReport" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mark Up Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped dataTable-table" id="markupUdf_dataTable">
                                    <thead>
                                        <tr>
                                            <th>Case No</th>
                                            <th>Follow Up</th>
                                            <th>Client</th>
                                            <th>Date Open</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Quick Note</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" id="printMarkupUdfReport" title="Print">Print</a>
                    <button class="btn btn-md btn-primary pullCase" title="Pull Case" >Pull Case</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed91 modal end-->

    <div id="proceed10" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">User Programs and User Defined Reports</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th width="40"></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>Program</td>
                                            <td>Compute two numbers</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" data-toggle="modal" title="Run">Run</a>
                    <a class="btn btn-md btn-primary" href="#proceed101" data-dismiss="modal" data-toggle="modal" title="Add">Add</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Delete">Delete</a>
                    <a class="btn btn-md btn-primary" href="#proceed101" data-dismiss="modal" data-toggle="modal" title="Edit">Edit</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed9 modal end-->

    <div id="proceed101" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit a User Defined Program</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Category</label>
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Code</label>
                                    <textarea class="form-control" style="height: 200px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#"  data-toggle="modal" title="Save">Save</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    <a class="btn btn-md btn-primary" href="#" title="Expr">Expr</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed101 modal end-->

    <div id="proceed111" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mark Up Report With User Defined Fields</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Case Filters</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Resp</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Special Count</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date F/U</label>
                                    <div class="input-daterange" id="datepicker1">
                                        <input type="text" class="input-sm form-control" name="start" >
                                        <input type="text" class="input-sm form-control" name="end">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Date Entered</label>
                                    <div class="input-daterange" id="datepicker1">
                                        <input type="text" class="input-sm form-control" name="start" >
                                        <input type="text" class="input-sm form-control" name="end">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Date Open</label>
                                    <div class="input-daterange" id="datepicker1">
                                        <input type="text" class="input-sm form-control" name="start" >
                                        <input type="text" class="input-sm form-control" name="end">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>UDF Page ID</label>
                                    <input type="text" class="form-control" />
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Advanced</label>
                                    <textarea class="form-control" style="height: 75px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary pull-left" href="#" title="Save">Save</a>
                    <a class="btn btn-md btn-primary" href="#proceed1111" data-dismiss="modal" data-toggle="modal" title="Run">Run</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed111 modal end-->

    <div id="proceed1111" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Case Count Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th width="40"></th>
                                            <th>Case No</th>
                                            <th>Date Entered</th>
                                            <th>Date Open</th>
                                            <th>Follow up</th>
                                            <th>Client</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Atty Rasp</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>5</td>
                                            <td> / / </td>
                                            <td> / / </td>
                                            <td> / / </td>
                                            <td>BRANDREEF, MARY</td>
                                            <td>PI</td>
                                            <td>CLOSED</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group marg-top15">
                    <select class="form-control">
                        <option>Normal</option>
                    </select>
                </div>
                <div class="form-group marg-top15">
                    <select class="form-control">
                        <option>Summary</option>
                    </select>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#">Print</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Close</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed1111 modal end-->

    <div id="proceed112" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">File Tracker Report</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Case Filters</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Type of Case</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Resp</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Atty Hand</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <label>Location</label>
                                    <select class="form-control"></select>
                                </div>
                                <div class="form-group">
                                    <span><label><input type="checkbox" class="i-checks"> Include empty locations </label></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Exclusions (separate with a comma)</label>
                                    <textarea class="form-control" style="height: 100px;"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Advanced</label>
                                    <textarea class="form-control" style="height: 100px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#proceed1121" data-dismiss="modal" data-toggle="modal" title="Run">Run</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed112 modal end-->

    <div id="proceed1121" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">File Tracker Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th width="40"></th>
                                            <th>Case No</th>
                                            <th>Type</th>
                                            <th>Case Status</th>
                                            <th>AttyR</th>
                                            <th>AttyH</th>
                                            <th>Location</th>
                                            <th>Your File No</th>
                                            <th>Follow Up</th>
                                            <th>Full Name</th>
                                            <th>Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td>4120</td>
                                            <td>WC </td>
                                            <td>CLOSED</td>
                                            <td></td>
                                            <td>EAB</td>
                                            <td>BX 1561</td>
                                            <td>4120-EAB</td>
                                            <td> / / </td>
                                            <td>RAY ad liteMICH</td>
                                            <td>WC</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group marg-top15">
                    <select class="form-control">
                        <option>Normal</option>
                    </select>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary" href="#" title="Print">Print</a>
                    <a class="btn btn-md btn-primary" href="#" title="Pull Case">Pull Case</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed1121 modal end-->

    <div id="proceed113" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Billibg Report</h4>
          </div>
          <div class="modal-body">
            <form action="" role="form">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">Case Filters</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">Billing Filters</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Miscellaneous</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4">Advanced</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <form>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Type of Case</label>
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Case Status</label>
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Location</label>
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Case No</label>
                                                        <input type="text" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Atty Resp</label>
                                                        <select class="form-control"></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Atty Hand</label>
                                                        <select class="form-control"></select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <form>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group" style="min-height: 200px;">
                                                    </div>
                                                    <div class="form-group">
                                                        <a class="btn btn-md btn-primary" href="#">Recalculate</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <form>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>Normal billign only</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Print only if more than</label>
                                                        <input type="text" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Billing cycle From - To</label>
                                                        <div class="input-daterange" id="datepicker1">
                                                            <input type="text" class="input-sm form-control" name="start" >
                                                            <input type="text" class="input-sm form-control" name="end">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Payment due date</label>
                                                        <input type="text" class="form-control injurydate date-field" />
                                                    </div>
                                                    <div class="form-group">
                                                        <span><label><input type="checkbox" class="i-checks"> Print invoices for all due dates </label></span>
                                                        <span><label><input type="checkbox" class="i-checks"> Ignore payment due date </label></span>
                                                        <span><label><input type="checkbox" class="i-checks"> Ignore zero payments </label></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <form>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea class="form-control" style="height: 250px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary pull-left" href="#proceed1131" data-toggle="modal" data-dismiss="modal" title="Quick Summary">Quick Summary</a>
                    <a class="btn btn-md btn-primary" href="#proceed1132" data-toggle="modal" data-dismiss="modal" title="Run">Run</a>
                    <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--proceed113 modal end-->

    <div id="proceed1131" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Billing - Quick Summary</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Months</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Who</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Today</label>
                                <input type="text" class="form-control injurydate date-field" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#">Calculate</a>
                        <a class="btn btn-md btn-primary" href="#">More</a>
                    </div>
                </div>
                <div class="form-group marg-top15">
                    <textarea class="form-control" style="height: 200px;"></textarea>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed1131 modal end-->

    <div id="proceed1132" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Billing Report Results</h4>
          </div>
          <div class="modal-body">
            <form>
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th width="40"></th>
                                            <th>Print</th>
                                            <th>Case No</th>
                                            <th>Case Type</th>
                                            <th>Case Status</th>
                                            <th>AttyR</th>
                                            <th>AttyH</th>
                                            <th>Full Name</th>
                                            <th>Balance</th>
                                            <th>Fees</th>
                                            <th>Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group marg-top15">
                    <select class="form-control">
                        <option>Invoices</option>
                    </select>
                </div>
                <div class="form-group text-center marg-top15">
                    <a class="btn btn-md btn-primary pull-left" href="#" title="Unselect All">Unselect All</a>
                    <a class="btn btn-md btn-primary" href="#" title="Print">Print</a>
                    <a class="btn btn-md btn-primary" href="#" title="Pull Case">Pull Case</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div> <!--Proceed1132 modal end-->
<script type="text/javascript">
var referralDataTable = '';
var caseActDataTable = '';
var taskTable = '';
var caseInjuryTable = '';
var markupTable = '';
$(document).ready(function () {

    $('input[type=checkbox]').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('input[type=number]').keypress(function(event){
        var maxlength = $(this).attr('max-length');

        if(typeof maxlength != 'undefined'){
            if(this.value.length >= maxlength){
                event.preventDefault();
            }
        }
    });
    //referral DataTable Assign
    referralDataTable = $('#caseReferralReport_table').DataTable();
    //Case Act DataTable assign
//    caseActDataTable = $('#caseActivityReport_table').DataTable();
    //Case Activity DataTable Listing
    caseActDataTable = $('#caseActivityReport_table').DataTable( {
            dom: 'Blfrtip',
            buttons: [{
                extend:'print',
                title: 'Case Activity Report',
                autoPrint: 'false',
                footer: 'true',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                },
            }],
            lengthMenu: [10, 25, 50,100,500,1000, "All"],
            autoWidth: true,
            sScrollY:"400px",
            bScrollCollapse : true,
            destroy: true,
            columns: [
                { data: "caseno",title:'Case No'},
                { data: "casetype",title:'Type'},
                { data: "casestat",title:'Status'},
                { data: "atty_hand",title:'Atty Hand'},
                { data: "date",title:'Date'},
                { data: "initials",title:'Who'},
                { data: "name",title:'Name'},
                { data: "event",title:'Event'},
            ],
            columnDefs: [{
                targets: 7,
                className: 'row-width',
                width:200,
            }],
        });

    taskTable = $('#tastReport_table').DataTable();
    caseInjuryTable = $('#tastReport_table').DataTable();
    markupTable = $('#markupReport_dataTable').DataTable();

    $('#tastReport_table tbody').on('click', 'tr', function () {
      //  alert("hi");
        if($(this).hasClass('selected')){
         $('.pullCase').prop('disabled', true); //console.log( "hi");
     }else{
           $('.pullCase').prop('disabled', false); // console.log( "hi1");
     }
    });

     $('#markupReport_dataTable tbody').on('click', 'tr', function () {
     if($(this).hasClass('selected')){
         $('.pullCase').prop('disabled', true); //console.log( "hi");
     }else{
           $('.pullCase').prop('disabled', false); // console.log( "hi1");
     }
 });

    markupUdfTable = $('#markupUdf_dataTable').DataTable();

    $('.injurydate').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
//        startDate: new Date()
    });

    $('#start_d_r').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#end_d_r').datetimepicker('setStartDate', ev.date);
    });

    $('#end_d_r').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#start_d_r').datetimepicker('setEndDate', ev.date);
    });

    $('#start_ca_d_r').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#end_ca_d_r').datetimepicker('setStartDate', ev.date);
    });

    $('#end_ca_d_r').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#start_ca_d_r').datetimepicker('setEndDate', ev.date);
    });

    // Task Report Datepicker
    $('#start_d_req').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#end_d_req').datetimepicker('setStartDate', ev.date);
    });

    $('#end_d_req').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#start_d_req').datetimepicker('setEndDate', ev.date);
    });

    // Markup Report Datepicker
    $('#start_followup').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#end_followup').datetimepicker('setStartDate', ev.date);
    });

    $('#end_followup').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#start_followup').datetimepicker('setEndDate', ev.date);
    });

    $('#start_date_open').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#end_date_open').datetimepicker('setStartDate', ev.date);
    });

    $('#end_date_open').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#start_date_open').datetimepicker('setEndDate', ev.date);
    });

    // Markup Report with UDF Datepicker Range
    $('#start_udf_open').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#end_udf_open').datetimepicker('setStartDate', ev.date);
    });

    $('#end_udf_open').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
    }).on('changeDate', function (ev) {
        $('#start_udf_open').datetimepicker('setEndDate', ev.date);
    });



//
//    $('.injurytime').clockpicker({
//         autoclose: true
//    });

//    $('.injurycolor').colorpicker({
//         autoclose: true
//    });

    // Upload File
    $('#uploadBtn3').change(function(){
        if ($(this).val() != '') {
            $('#uploadFile3').html($(this).val());
        }else{
            $('#uploadFile3').html('Upload File');
        }
    });

    $('.modal').on('hidden.bs.modal', function () {
        $(this).find('input[type="text"],input[type="email"],textarea,select,input[type="checkbox"]').each(function () {
//            console.log(this);
            $(this).val('').trigger('change');
            $(this).iCheck('uncheck');
            this.value = '';
        });
    });

    $('.modal').on('shown.bs.modal', function () {
        $('#'+this.id).data('bs.modal').options.backdrop = 'static';
    });

    $(document.body).on('change','#isType',function(){
        var value = this.value;

        if(this.value == 'byMonth'){
            $('#atty_type').hide();
            $('#atty_months').show();
        }else{
            $('#atty_type').show();
            $('#atty_months').hide();
        }
    })

    //Referral Ajax Request
    $(document.body).on('click','#getReferralReport',function(){
        referralDataTable.destroy();
        $('#case_referral_list_modal').modal('show');

        referralDataTable = $('#caseReferralReport_table').DataTable({
                buttons: [{
                    extend:'print',
                    title: 'Referral Report Results',
                    autoPrint: 'false',
                    footer: 'true',
                }],
                lengthMenu: [[10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"]],
                processing: true,
                serverSide: true,
                bFilter: true,
                scrollX: true,
                sScrollY:400,
                bScrollCollapse : true,
                destroy: true,
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getSearchReferralReport",
                    type: "POST",
                    "data": function (ae) {
                         ae.searchData = $('#caseReferralForm').serializeArray();
                    }
                },
                select: {
                    style: 'single'
                }
            });
        $('#referral_report').modal('hide');

    });

    $(document.body).on('click','#generateCaseActivity',function(){
        $.blockUI();
        $.ajax({
            url: '<?php echo base_url(); ?>reports/getCaseActivityReport',
            data: $('#caseActivityForm').serialize(),
            method: "POST",
            dataType:'json',
            success: function (result) {
                $('#case_activity_report').modal('hide');
                $.unblockUI();
                if(result.status > 0){
                    console.log(result.status);
                    caseActDataTable.clear();
                    caseActDataTable.rows.add(result.data);

                    setTimeout(function(){  caseActDataTable.draw(); }, 200);

                }
                $('#caseActivityReportData').modal('show');
            }

        });
    });

//    $(document.body).on('click','#generateCaseActivity',function(){
////        $.blockUI();
//        caseActDataTable.destroy();
//        $('#caseActivityReportData').modal('show');
//
//        caseActDataTable = $('#caseActivityReport_table').DataTable({
//                buttons: [{
//                    extend:'print',
//                    title: 'Case Activity Report Results',
//                    autoPrint: 'false',
//                    footer: 'true',
//                }],
//                lengthMenu: [[10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"]],
//                processing: true,
//                serverSide: true,
//                bFilter: true,
//                scrollX: true,
//                sScrollY:400,
//                bScrollCollapse : true,
//                destroy: true,
//                autoWidth: true,
//                ajax: {
//                    url: "<?php echo base_url(); ?>reports/getCaseActivityReportNew",
//                    type: "POST",
//                    "data": function (ae) {
//                         ae.searchData = $('#caseActivityForm').serializeArray();
//                    }
//                },
//                columnDefs: [{
//                    targets: 7,
//                    className: 'row-width',
//                    width:200,
//                }],
//            });
//        $('#case_activity_report').modal('hide');
//    });

    $(document.body).on('click','#filterTaskReport',function(){
        console.log('test');
        taskTable.destroy();
        $('#taskReportListing').modal('show');
        var serarray = $('#taskActitvityForm').serializeArray();

        taskTable = $('#tastReport_table').DataTable({
                buttons: [{
                    extend:'print',
                    title: 'Task Activity Report Results',
                    autoPrint: 'false',
                    footer: 'true',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7]
                    }
                }],
                lengthMenu: [[10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"]],
                processing: true,
                serverSide: true,
                bFilter: true,
                scrollX: true,
                sScrollY:400,
                bScrollCollapse : true,
                destroy: true,
                order: [[4, "asc"]],
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getFilterTaskReportNew",
                    type: "POST",
                    "data": function (ae) {
                         ae.searchData = serarray;
                         //ae.searchData = $('#taskActitvityForm').serializeArray();
                    }
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id",aData[0]);
                    return nRow;
                },
                columnDefs: [{
                    targets: 7,
                    className: 'row-width',
                    width:200,
                }],
                select: {
                    style: 'single'
                }
            });
        $('#task_report').modal('hide');
    });

    $(document.body).on('click','#getCaseReportModel',function(){
        //console.log($('#caseInjuryForm').serializeArray());
    });

    $(document.body).on('click','#getMarkUpRecord',function(){
        markupTable.destroy();
        $('#markupReport_Content').modal('show');

        markupTable = $('#markupReport_dataTable').DataTable({
                buttons: [{
                    extend:'print',
                    title: 'Markup Report Results',
                    autoPrint: 'false',
                    footer: 'true',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,7,9]
                    }
                }],
                lengthMenu: [[10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"]],
                processing: true,
                serverSide: true,
                bFilter: true,
                scrollX: true,
                sScrollY:400,
                bScrollCollapse : true,
                destroy: true,
//                order: [[4, "asc"]],
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getFilterMarkupReport",
                    type: "POST",
                    "data": function (ae) {
                         ae.searchData = $('#markuReportForm').serializeArray();
                    }
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id",aData[0]);
                    return nRow;
                },
                columnDefs: [{
                    targets: 3,
                    className: 'row-width',
                    width:200,
                },{
                    targets: 13,
                    className: 'row-width',
                    width:200,
                }],
                select: {
                    style: 'single'
                }
            });
        $('#markup_report').modal('hide');
    });

    
$(document.body).on('click','#caseCountProceed',function(){
//        markupTable.destroy();
//        $('#markupReport_Content').modal('show');
//
//        markupTable = $('#markupReport_dataTable').DataTable({
//                buttons: [{
//                    extend:'print',
//                    title: 'Markup Report Results',
//                    autoPrint: 'false',
//                    footer: 'true',
//                    exportOptions: {
//                        columns: [ 0, 1, 2, 3,4,5,7,9]
//                    }
//                }],
//                lengthMenu: [[10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"]],
//                processing: true,
//                serverSide: true,
//                bFilter: true,
//                scrollX: true,
//                sScrollY:400,
//                bScrollCollapse : true,
//                destroy: true,
////                order: [[4, "asc"]],
//                autoWidth: true,
//                ajax: {
//                    url: "<?php echo base_url(); ?>reports/getFilterMarkupReport",
//                    type: "POST",
//                    "data": function (ae) {
//                         ae.searchData = $('#markuReportForm').serializeArray();
//                    }
//                },
//                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).attr("id",aData[0]);
//                    return nRow;
//                },
//                columnDefs: [{
//                    targets: 3,
//                    className: 'row-width',
//                    width:200,
//                },{
//                    targets: 13,
//                    className: 'row-width',
//                    width:200,
//                }],
//                select: {
//                    style: 'single'
//                }
//            });
//        $('#markup_report').modal('hide');

    });




$(document.body).on('click','#getMarkupUdfReport',function(){

        markupUdfTable = $('#markupUdf_dataTable').DataTable({
                buttons: [{
                    extend:'print',
                    title: 'Markup Report User Defined',
                    autoPrint: 'false',
                    footer: 'true',
//                    exportOptions: {
//                        columns: [ 0, 1, 2, 3,4,5,7,9]
//                    }
                }],
                lengthMenu: [[10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"]],
                processing: true,
                serverSide: true,
                bFilter: true,
                scrollX: true,
                sScrollY:400,
                bScrollCollapse : true,
                destroy: true,
//                order: [[4, "asc"]],
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getFilterMarkupReportUDF",
                    type: "POST",
                    "data": function (ae) {
                         ae.searchData = $('#markupWithUdf').serializeArray();
                    }
                },
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id",aData[0]);
                    return nRow;
                },
                columnDefs: [{
                    targets: 6,
                    className: 'row-width',
                    width:200,
                }],
                select: {
                    style: 'single'
                }
            });
        $('#markup_report_udf').modal('hide');
        $('#showMarkupUdfReport').modal('show');
        //markupUdfTable
    });


    $(document.body).on('click','#printReferrall',function(){
        referralDataTable.buttons('.buttons-print').trigger();
//        $('#case_referral_list_modal').modal('hide');
    });

    $(document.body).on('click','#printCaseActivityReport',function(){
        caseActDataTable.buttons('.buttons-print').trigger();
        $('#caseActivityReportData').modal('hide');
    });

    $(document.body).on('click','#printTaskList',function(){
        taskTable.buttons('.buttons-print').trigger();
//        $('#taskReportListing').modal('hide');
    });

    $(document.body).on('click','#printMarkupReport',function(){
        markupTable.buttons('.buttons-print').trigger();
    });

    $(document.body).on('click','#printMarkupUdfReport',function(){
        markupUdfTable.buttons('.buttons-print').trigger();
    });

    $(document.body).on('click','#caseCountProceed',function(){
        
        console.log($('#caseCountReportForm').serialize());
    });

    $(document.body).on('click','#caseCountCancel',function(){
        $('#case_count').modal('hide');
    });
    


    $(document.body).on('click','.pullCase',function(){
        //console.log($(this).closest('form').find('table'));
        var caseno = $(this).closest('form').find('table').find('tr.selected').attr('id');
        window.open("<?php echo base_url().'cases/case_details/' ?>"+caseno, '_blank');
    });


});
</script>



