<link href="<?php echo base_url('assets'); ?>/css/report.css" rel="stylesheet">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Reports</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Reports</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="reportTable">
                            <thead>
                                <tr>
                                    <th>Group</th>
                                    <th>Report Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($reportName)) {
                                    foreach ($reportName as $key => $val) :
                                        ?>
                                        <tr>
                                            <td>LAW</td>
                                            <td><?php echo $val['name']; ?></td>
                                            <?php if (isset($val['type']) && $val['type'] == 'modal') { ?>
                                                <td><a class="btn btn-xs btn-info marginClass activity_edit" data-toggle="modal" data-target="<?php echo '#' . $val['field']; ?>" data-dismiss="modal" href="javascript:void(0);" title="View"><i class="icon fa fa-paste"></i> View</a></td>
                                            <?php } else if (isset($val['type']) && $val['type'] == 'url') { ?>
                                                <td><a class="btn btn-xs btn-info marginClass activity_edit" target="_blank" href="<?php echo base_url() . $val['field'] ?>" title="View"><i class="icon fa fa-paste"></i> View</a></td>
                                            <?php } else { ?>
                                                <td></td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    endforeach;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $tabindex = 1; ?>

<!-- MODAL FOR CASE COUNT REPORT SEARCH FORM [START] -->
<div id="case_count" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Count Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseCountReportForm" id="caseCountReportForm">
                    <input type="hidden" id="CaseCountReportUrl" value="<?php echo base_url(); ?>reports/getFilterCasecount"/>
                    <div class="form-inline">
                        <label class="col-md-2">Report</label>
                        <select class="form-control" id="isType" name="isType">
                            <option value="byType">By Atty/Type</option>
                            <option value="byMonth">By Atty/All Months</option>
                        </select>
                    </div>
                    <div class="form-group legendDiv">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border" id="casecount-isType">By Atty/Type</legend>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-3">Search By</label>
                                    <select class="form-control col-md-9"  name="searchBy">
                                        <option value="attyResp_dateOpen">AttyResp / DateOpen</option>
                                        <option value="attyResp_dateEntered">AttyResp / Date Entered</option>
                                        <option value="attyResp_dateClosed">AttyResp / Date Closed</option>
                                        <option value="attyHand_dateOpen">AttyHand / DateOpen</option>
                                        <option value="attyHand_dateEntered">AttyHand / Date Entered</option>
                                        <option value="attyHand_dateClosed">AttyHand / Date Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <div class="col-md-6">
                                        <label class="col-md-6">Month</label>
                                        <select class="form-control" name="month">
                                            <?php foreach ($months as $key => $val) : ?>
                                                <option value="<?php echo $key; ?>" <?php if (date('n') == $key) { ?> selected <?php } ?>><?php echo $val; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6" id="casecount-year">
                                        <label class="col-md-3">Year</label>
                                        <input class="col-md-9 form-control" type="number" name="year" onKeyPress="if (this.value.length == 4)
                                                    return false;" value="<?php echo date('Y'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline" id="casecount-types">
                                    <label class="col-md-3">Types</label>
                                    <input class="col-md-9 form-control" type="text" name="types">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-inline">
                                    <label class="col-md-6">Include unassigned attorneys</label>
                                    <input class="col-md-6 form-control" type="checkbox" name="unassign_attorneys">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="caseCountProceed" href="javascript:void(0)">Proceed</a>
                <a class="btn btn-md btn-primary" href="javascript:void(0)" id="caseCountCancel">Cancel</a>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE COUNT REPORT SEARCH FORM [END] -->


<!-- MODAL FOR CASE REPORT SEARCH FORM [START] -->
<div id="CASE_REPORT" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseReportForm" id="caseReportForm">
                    <input type="hidden" id="CaseReportUrl" value="<?php echo base_url(); ?>reports/getFilterCaseReport"/>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Type / Case</label>
                                        <select name="casetype" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php
                                            if (!empty($casetype)) {
                                                foreach ($casetype as $k => $v) :
                                                    ?>
                                                    <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php
                                            if (isset($casestat) && !empty($casestat)) {
                                                foreach ($casestat as $k => $v) :
                                                    ?>
                                                    <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Card Type</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Card Type</option>
                                            <?php
                                            if (isset($cardType) && !empty($cardType)) {
                                                foreach ($cardType as $k => $v) :
                                                    ?>
                                                    <option value="<?php echo $v->type; ?>"><?php echo $v->type; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Search By</label>
                                        <select id="searchBy" name="searchBy" placeholder="Search by"  class="form-control select2Class">
                                            <option value="">Search by</option>
                                            <?php
                                            if (isset($filters['searchBy']) && !empty($filters['searchBy'])) {
                                                foreach ($filters['searchBy'] as $key => $val) :
                                                    ?>
                                                    <option value="<?php echo $key; ?>" <?php echo ($key == 'cd.last') ? 'selected' : ""; ?> ><?php echo $val; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Search For</label>
                                        <input type="text" data-toggle="tooltip" title="please Select Search By" data-placement="bottom" id="searchText" name="searchText" placeholder="Search for" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="ibox-title">
                                    <h5>Staff Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Atty Resp</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php
                                            if (isset($atty_resp) && !empty($atty_resp)) {
                                                foreach ($atty_resp as $k => $v) :
                                                    ?>
                                                    <option value="<?php echo $v->atty_resp; ?>"><?php echo $v->atty_resp; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php
                                            if (isset($atty_hand) && !empty($atty_hand)) {
                                                foreach ($atty_hand as $k => $v) :
                                                    ?>
                                                    <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Para Hand</label>
                                        <select name="para_hand" class="form-control select2Class">
                                            <option value="">Select Para Hand</option>
                                            <?php
                                            if (isset($para_hand) && !empty($para_hand)) {
                                                foreach ($para_hand as $k => $v) :
                                                    ?>
                                                    <option value="<?php echo $v->para_hand; ?>"><?php echo $v->para_hand; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sec Hand</label>
                                        <select name="sec_hand" class="form-control select2Class">
                                            <option value="">Select Sec Hand</option>
                                            <?php
                                            if (isset($sec_hand) && !empty($sec_hand)) {
                                                foreach ($sec_hand as $k => $v) :
                                                    ?>
                                                    <option value="<?php echo $v->sec_hand; ?>"><?php echo $v->sec_hand; ?></option>
                                                    <?php
                                                endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="m-r-xs">
                                            <input type="checkbox" name="primary_news" id="checkbox1">
                                            <label for="checkbox1">
                                                Primary News only
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="m-r-xs">
                                            <input type="checkbox" name="close_window" id="checkbox2">
                                            <label for="checkbox2">
                                                Close window
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="m-r-xs">
                                            <input type="checkbox" name="deleted_cases" id="checkbox3">
                                            <label for="checkbox3">
                                                Include Deleted Cases
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="CaseRepotProceed">Run</a>
                <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FOR CASE REPORT SEARCH FORM [END] -->


<!-- MODAL FOR REFERRAL REPORT SEARCH [START] -->
<div id="referral_report" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Referral Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseReferralForm" id="caseReferralForm">
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ibox-title">
                                    <h5>Case Referral Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select name="case_type" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php foreach ($casetype as $k => $v) : ?>
                                                <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php foreach ($casestat as $k => $v) : ?>
                                                <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>D/R</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_d_r" class="input-small form-control" name="start_d_r" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_d_r" class="input-small form-control" name="end_d_r" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>R/B</label>
                                        <input type="text" name="rb" class="form-control" value="0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a class="btn btn-md btn-primary" id="getReferralReport">Run</a>
                <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FOR REFERRAL REPORT SEARCH [END] -->


<!-- MODAL FOR CASE ACTIVITY REPORT SEARCH [START] -->
<div id="case_activity_report" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Activity Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseActivityForm" id="caseActivityForm">
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select name="case_type" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php foreach ($casetype as $k => $v) : ?>
                                                <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php foreach ($casestat as $k => $v) : ?>
                                                <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Activity Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Categories</label>
                                        <select name="categories" class="form-control select2Class">
                                            <option value="">Select Category</option>
                                            <?php foreach ($category as $k => $v) : ?>
                                                <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Beg/End Date</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_ca_d_r" class="input-small form-control date-range" name="start_ca_d_r" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_ca_d_r" class="input-small form-control date-range" name="end_ca_d_r" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Event</label>
                                        <input type="text" name="event" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Max Category</label>
                                        <input type="number" name="max_category" min="0" max="999" max-length="3" class="form-control" value="999" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Search Documents</label>
                                    <input type="text" class="form-control" name="documents" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 marg-top15">
                                <span class="block"><label><input type="checkbox" name="isFLName" class="i-checks"> Use Format First Name-Last Name </label></span>
                                <span class="block"><label><input type="checkbox" name="noActivity" class="i-checks"> No Activity </label></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 marg-top15">
                                <select class="form-control pull-left" name="reportWith">
                                    <option>Normal</option>
                                    <option>Fee Report</option>
                                    <option>Cost Report</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="generateCaseActivity" title="Run" href="javaScript:void(0)">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                        <span class="pull-right"><label><input type="checkbox" class="i-checks"> Event Contains </label></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FOR CASE ACTIVITY REPORT SEARCH [END] -->

<!-- MODAL FOR CASE INJURY REPORT SEARCH [START] -->
<div id="case_injury_report" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Injury Report</h4>
            </div>
            <div class="modal-body">
                <form name="caseInjuryForm" id="caseInjuryForm">
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select name="case_type" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php foreach ($casetype as $k => $v) : ?>
                                                <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php foreach ($casestat as $k => $v) : ?>
                                                <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Injury Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <input type="text" name="injury_status" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>EAMS/WCAB No (contains)</label>
                                                <input type="text" name="i_wcab" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Claim No</label>
                                                <input type="text" name="i_claimno" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Date of Injury</label>
                                                <input type="text" name="doi" class="form-control injurydate date-field" />
                                            </div>
                                            <div class="form-group">
                                                <label>Parts of Body</label>
                                                <input type="text" name="pob" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>DOI Month/Year</label>
                                                <input type="text" name="doi_my" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <span class="pull-left"><label><input type="checkbox" name="no_injury" class="i-checks"> No Injury </label></span>
                        <a class="btn btn-md btn-primary" id="getCaseReportModel" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                        <span class="pull-right"><label><input type="checkbox" name="err_ignores" class="i-checks"> Ignore Errors </label></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FOR CASE INJURY REPORT SEARCH [END] -->

<!-- MODAL FOR TASK REPORT SEARCH [START] -->
<div id="task_report" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Task Report</h4>
            </div>
            <div class="modal-body">
                <form name="taskActitvityForm" id="taskActitvityForm">
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select name="case_type" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php foreach ($casetype as $k => $v) : ?>
                                                <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php foreach ($casestat as $k => $v) : ?>
                                                <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>User Defined Fields</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>From/to</label>
                                        <div class="input-group date-rangepicker">
                                            <select name="whofrom" class="form-control select2Class">
                                                <option value="">From User</option>
                                                <?php foreach ($atty_hand as $k => $v) : ?>
                                                    <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="input-group-addon">To</span>
                                            <select name="whoto" class="form-control select2Class">
                                                <option value="">To User</option>
                                                <?php foreach ($atty_hand as $k => $v) : ?>
                                                    <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Beg/End Date</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_d_req" class="input-small form-control date-range" name="start_d_req" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_d_req" class="input-small form-control date-range" name="end_d_req" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Task</label>
                                        <input type="text" name="event" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <span><label><input type="checkbox" name="contains" class="i-checks"> Contains </label></span>
                                        <span><label><input type="checkbox" name="isCompleted" class="i-checks"> Display Completed Tasks </label></span>
                                        <span><label><input type="checkbox" name="isHighPriority" class="i-checks"> Only Display High Priority Tasks </label></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer noPadding">
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control">
                            <option>Normal</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-md btn-primary" id="filterTaskReport" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-md btn-primary pull-right" href="#" title="Pool Report">Pool Report</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR TASK REPORT SEARCH [END] -->

<!-- MODAL FOR MARKUP REPORT [SEARCH] -->
<div id="markup_report" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report</h4>
            </div>
            <div class="modal-body">
                <form name="CasecountForm" id="CasecountForm">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Case Filters</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select name="case_type" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php foreach ($casetype as $k => $v) : ?>
                                                <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php foreach ($casestat as $k => $v) : ?>
                                                <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Resp</label>
                                        <select name="atty_resp" class="form-control select2Class">
                                            <option value="">Select Atty Resp</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Para Hand</label>
                                        <select name="para_hand" class="form-control select2Class">
                                            <option value="">Select Para Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Card Vanue</label>
                                        <input type="text" class="form-control" name="venue" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Markup Date Begin/End</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_followup" class="input-small form-control date-range" name="start_followup" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_followup" class="input-small form-control date-range" name="end_followup" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Date Open Begin/End</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_date_open" class="input-small form-control date-range" name="start_date_open" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_date_open" class="input-small form-control date-range" name="end_date_open" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>UDF</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <span><label><input type="checkbox" class="i-checks"> Remove calendar duplicates </label></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                        <a class="btn btn-md btn-primary" id="getMarkUpRecord" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-md btn-primary pull-right" href="#">S</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FOR MARKUP REPORT SEARCH [END] -->

<!-- MODAL FOR MARKUP REPORT WITH USER DEFINED FIELD SEARCH [START] -->
<div id="markup_report_udf" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report With User Defined Fields</h4>
            </div>
            <div class="modal-body">
                <form name="markupWithUdf" id="markupWithUdf">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Case Filters</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select name="case_type" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php foreach ($casetype as $k => $v) : ?>
                                                <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php foreach ($casestat as $k => $v) : ?>
                                                <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Resp</label>
                                        <select name="atty_resp" class="form-control select2Class">
                                            <option value="">Select Atty Resp</option>
                                            <?php foreach ($atty_resp as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_resp; ?>"><?php echo $v->atty_resp; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Start / End Date</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_udf_open" class="input-small form-control date-range" name="start_d_req" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_udf_open" class="input-small form-control date-range" name="end_d_req" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Program</label>
                                        <input type="text" name="program" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="form-inline col-md-6">
                        <label>Report</label>
                        <select class="form-control">
                            <option>Select Report Type</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <a class="btn btn-md btn-primary" id="getMarkupUdfReport" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR MARKUP REPORT WITH USER DEFINED FIELD SEARCH [END] -->



<!-- ****************** LISTING  ******************* -->

<!-- MODAL FOR CASE COUNT REPORT TABLE [START] -->
<div id="CaseCountpReport_list" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseCountReport_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Follow Up</th>
                                                <th>Client</th>
                                                <th>Quick Note</th>
                                                <th>Date / Time</th>
                                                <th>Next Cal Event</th>
                                                <th>Venue Card</th>
                                                <th>Venue Cal</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>AttyR</th>
                                                <th>AttyH</th>
                                                <th>ParaH</th>
                                                <th>Caption</th>
                                                <th>Date Open</th>
                                                <th>User Defined</th>
                                                <th>YourFileNo</th>
                                                <th>Atty Assigned</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printMarkupReport" title="Print">Print</a>
                        <button class="btn btn-md btn-primary pullCase" disabled title="Pull Case">Pull Case</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FOR CASE COUNT REPORT TABLE [END] -->

<!-- MODAL FOR CASE REPORT TABLE [START] -->
<div id="case_referral_list_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Referral Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseReferralReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Case Status</th>
                                                <th>Attyh</th>
                                                <th>D/R</th>
                                                <th>Referred by</th>
                                                <th>Case Caption</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printReferrall" title="Print" href="#">Print</a>
                        <a class="btn btn-md btn-primary" href="#" title="Cancel" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR CASE REPORT TABLE [END] -->

<!-- MODAL FOR REFERRAL REPORT LISTING [START] -->
<div id="case_referral_list_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Referral Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseReferralReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Case Status</th>
                                                <th>Attyh</th>
                                                <th>D/R</th>
                                                <th>Referred by</th>
                                                <th>Case Caption</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printReferrall" title="Print" href="#">Print</a>
                        <a class="btn btn-md btn-primary" href="#" title="Cancel" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR REFERRAL REPORT LISTING [END] -->

<!-- MODAL FOR CASE ACTIVITY LISTING [START] -->
<div id="caseActivityReportData" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Activity Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="caseActivityReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>AttyHand</th>
                                                <th>Date</th>
                                                <th>Who</th>
                                                <th>Name</th>
                                                <td>Event</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printCaseActivityReport" title="Print" href="#">Print</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FOR CASE ACTIVITY LISTING [START] -->

<!-- MODAL FOR TASK REPORT TABLE [START] -->
<div id="taskReportListing" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Task Activity Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped hi" id="tastReport_table">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Case Status</th>
                                                <th>AttyHand</th>
                                                <th>Date</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Task</th>
                                                <th>Name</th>
                                                <th>Type Task</th>
                                                <th>Priority</th>
                                                <th>Date Assigned</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#" id="printTaskList" title="Print">Print</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                        <button class="btn btn-md btn-primary pullCase" disabled title="Pull Case">Pull Case</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR TASK REPORT TABLE [END] -->

<!-- MODAL FOR MARKUP REPORT LIST [START] -->
<div id="markupReport_Content" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="markupReport_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Follow Up</th>
                                                <th>Client</th>
                                                <th>Quick Note</th>
                                                <th>Date / Time</th>
                                                <th>Next Cal Event</th>
                                                <th>Venue Card</th>
                                                <th>Venue Cal</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>AttyR</th>
                                                <th>AttyH</th>
                                                <th>ParaH</th>
                                                <th>Caption</th>
                                                <th>Date Open</th>
                                                <th>User Defined</th>
                                                <th>YourFileNo</th>
                                                <th>Atty Assigned</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printMarkupReport" title="Print">Print</a>
                        <button class="btn btn-md btn-primary pullCase" disabled title="Pull Case">Pull Case</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR MARKUP REPORT LIST [END] -->


<!-- MODAL FOR MARKUP REPORT WITH USER DEFINED FIELD [START] -->
<div id="showMarkupUdfReport" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped dataTable-table" id="markupUdf_dataTable">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Follow Up</th>
                                                <th>Client</th>
                                                <th>Date Open</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>Quick Note</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="printMarkupUdfReport" title="Print">Print</a>
                        <button class="btn btn-md btn-primary pullCase" title="Pull Case" >Pull Case</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FOR MARKUP REPORT WITH USER DEFINED FIELD [START] -->



<div id="proceed2" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Search for a Case</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Type/Case</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Card Type</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Search By</label>
                                        <select class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Staff Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Atty Resp</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Para Hand</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Sec Hand</label>
                                        <select class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 marg-top15">
                                <div class="form-group">
                                    <label>Search For</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-6 marg-top15">
                                <div class="form-group">
                                    <span><label><input type="checkbox" class="i-checks"> Primary Names only </label></span>
                                    <span><label><input type="checkbox" class="i-checks"> Close Window </label></span>
                                    <span><label><input type="checkbox" class="i-checks"> Include Deleted Cases </label></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#proceed21" data-toggle="modal" data-dismiss="modal">Search</a>
                        <a class="btn btn-md btn-primary" href="#">Clear</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Close</a>
                        <a class="btn btn-md btn-primary" href="#">Save</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> <!--Proceed1 modal end-->

<div id="proceed21" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Found Cases</h4>
            </div>
            <div class="modal-body">
                <div class="ibox float-e-margins">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="ibox-content marg-bot15 party-in">
                                <form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th width="40"></th>
                                                                <th>Case No</th>
                                                                <th>Name</th>
                                                                <th>Attyr</th>
                                                                <th>Attyh</th>
                                                                <th>Type</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                                <td>4340</td>
                                                                <td>Hubert Jessup</td>
                                                                <td>EAB</td>
                                                                <td>EAB</td>
                                                                <td>WC</td>
                                                                <td>Closed</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="ibox-content marg-bot15 party">
                                <form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="table-responsive">
                                                    <table class="table no-bottom-margin">
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Name :</strong></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>FirmName :</strong></td>
                                                                <td>Hubert Jessup</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>CardType :</strong></td>
                                                                <td>Employer</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Phone :</strong></td>
                                                                <td>818-769-8141</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>S S No :</strong></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Type of Case :</strong></td>
                                                                <td>PI</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Case Status :</strong></td>
                                                                <td>CL PI BX#1599 3/01=MTC</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Followup :</strong></td>
                                                                <td> / / </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Date Entered :</strong></td>
                                                                <td> / / </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Your File No. :</strong></td>
                                                                <td>4340-MHZ</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="100"><strong>Staff :</strong></td>
                                                                <td>iamiran@tstarc.com</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <label>Dates of Injury</label>
                                        <textarea class="form-control" style="height: 150px;"></textarea>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="inline-block-100">
                    <div class="form-group text-center marg-top15 pull-left">
                        <a class="btn btn-md btn-primary" href="#">Open</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Close</a>
                        <a class="btn btn-md btn-primary" href="#">Save</a>
                    </div>
                    <div class="form-group pull-right">
                        <span class="block"><label><input type="checkbox" class="i-checks"></label> Alternate Format </span>
                        <span class="block"><label><input type="checkbox" class="i-checks"></label> Close Window </span>
                        <span class="block"><label><input type="checkbox" class="i-checks"></label> Auto Pull </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--Proceed1 modal end-->

<div id="proceed6" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Referral Report</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>Case Filters</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Advanced</label>
                                        <textarea class="form-control" style="height: 75px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox-title">
                                    <h5>User Defined Fields</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <label>Page</label>
                                        <select class="form-control">
                                            <option>Page 1</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Field</label>
                                        <select class="form-control">
                                            <option>Fees 1-3</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Content</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Advanced</label>
                                        <textarea class="form-control" style="height: 75px;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <span class="pull-left"><label><input type="checkbox" class="i-checks"> No Activity </label></span>
                        <a class="btn btn-md btn-primary" href="#proceed61" data-dismiss="modal" data-toggle="modal" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> <!--Proceed6 modal end-->

<div id="proceed61" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Defined Field Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th width="40"></th>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Case Status</th>
                                                <th>AttyHand</th>
                                                <th>Name</th>
                                                <th>Fees 1-3</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>8109</td>
                                                <td>WC</td>
                                                <td>OP 2017-6-10</td>
                                                <td>EAB</td>
                                                <td>Gainer, Morgan</td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#" title="Print">Print</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> <!--Proceed61 modal end-->


<div id="markup_report" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report</h4>
            </div>
            <div class="modal-body">
                <form name="markuReportForm" id="markuReportForm">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Case Filters</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select name="case_type" class="form-control select2Class">
                                            <option value="">Select Case Type</option>
                                            <?php foreach ($casetype as $k => $v) : ?>
                                                <option value="<?php echo $v->casetype; ?>"><?php echo $v->casetype; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select name="case_status" class="form-control select2Class">
                                            <option value="">Select Case Status</option>
                                            <?php foreach ($casestat as $k => $v) : ?>
                                                <option value="<?php echo $v->casestat; ?>"><?php echo $v->casestat; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Resp</label>
                                        <select name="atty_resp" class="form-control select2Class">
                                            <option value="">Select Atty Resp</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select name="atty_hand" class="form-control select2Class">
                                            <option value="">Select Atty Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Para Hand</label>
                                        <select name="para_hand" class="form-control select2Class">
                                            <option value="">Select Para Hand</option>
                                            <?php foreach ($atty_hand as $k => $v) : ?>
                                                <option value="<?php echo $v->atty_hand; ?>"><?php echo $v->atty_hand; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Card Vanue</label>
                                        <input type="text" class="form-control" name="venue" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Markup Date Begin/End</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_followup" class="input-small form-control date-range" name="start_followup" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_followup" class="input-small form-control date-range" name="end_followup" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Date Open Begin/End</label>
                                        <div class="input-group date-rangepicker">
                                            <input type="text" id="start_date_open" class="input-small form-control date-range" name="start_date_open" placeholder="From Date" readonly=""/>
                                            <span class="input-group-addon">To</span>
                                            <input type="text" id="end_date_open" class="input-small form-control date-range" name="end_date_open" placeholder="To Date" readonly=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>UDF</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <span><label><input type="checkbox" class="i-checks"> Remove calendar duplicates </label></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                        <a class="btn btn-md btn-primary" id="getMarkUpRecord" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-md btn-primary pull-right" href="#">S</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="proceed10" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Programs and User Defined Reports</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th width="40"></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>Program</td>
                                                <td>Compute two numbers</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" data-toggle="modal" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#proceed101" data-dismiss="modal" data-toggle="modal" title="Add">Add</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Delete">Delete</a>
                        <a class="btn btn-md btn-primary" href="#proceed101" data-dismiss="modal" data-toggle="modal" title="Edit">Edit</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> <!--Proceed9 modal end-->

<div id="proceed101" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit a User Defined Program</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Code</label>
                                        <textarea class="form-control" style="height: 200px;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#"  data-toggle="modal" title="Save">Save</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                        <a class="btn btn-md btn-primary" href="#" title="Expr">Expr</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> <!--Proceed101 modal end-->

<div id="proceed111" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Up Report With User Defined Fields</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Case Filters</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Resp</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Atty Hand</label>
                                        <select class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label>Special Count</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date F/U</label>
                                        <div class="input-daterange" id="datepicker1">
                                            <input type="text" class="input-sm form-control" name="start" >
                                            <input type="text" class="input-sm form-control" name="end">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Date Entered</label>
                                        <div class="input-daterange" id="datepicker1">
                                            <input type="text" class="input-sm form-control" name="start" >
                                            <input type="text" class="input-sm form-control" name="end">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Date Open</label>
                                        <div class="input-daterange" id="datepicker1">
                                            <input type="text" class="input-sm form-control" name="start" >
                                            <input type="text" class="input-sm form-control" name="end">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>UDF Page ID</label>
                                        <input type="text" class="form-control" />
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Advanced</label>
                                        <textarea class="form-control" style="height: 75px;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary pull-left" href="#" title="Save">Save</a>
                        <a class="btn btn-md btn-primary" href="#proceed1111" data-dismiss="modal" data-toggle="modal" title="Run">Run</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal" title="Cancel">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> <!--Proceed111 modal end-->

<div id="proceed1111" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Case Count Report Results</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="ibox float-e-margins">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th width="40"></th>
                                                <th>Case No</th>
                                                <th>Date Entered</th>
                                                <th>Date Open</th>
                                                <th>Follow up</th>
                                                <th>Client</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>Atty Rasp</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><span><label><input type="checkbox" class="i-checks"></label></span></td>
                                                <td>5</td>
                                                <td> / / </td>
                                                <td> / / </td>
                                                <td> / / </td>
                                                <td>BRANDREEF, MARY</td>
                                                <td>PI</td>
                                                <td>CLOSED</td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group marg-top15">
                        <select class="form-control">
                            <option>Normal</option>
                        </select>
                    </div>
                    <div class="form-group marg-top15">
                        <select class="form-control">
                            <option>Summary</option>
                        </select>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#">Print</a>
                        <a class="btn btn-md btn-primary" href="#" data-dismiss="modal">Close</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        taskTable = $('#tastReport_table').DataTable();
        caseInjuryTable = $('#tastReport_table').DataTable();
        markupTable = $('#markupReport_dataTable').DataTable();
        caseCountTable = $('#caseCountReport_dataTable').DataTable();
    });

</script>

<!-- Script for Case Count Report -->
<script type="text/javascript">

    $(document.body).on('change', '#isType', function () {
        var value = this.value;

        if (this.value == 'byMonth') {
            $('#casecount-isType').html('By Atty/All Months');
            $('#casecount-year').hide();
            $('#casecount-types').hide();

        } else {
            $('#casecount-isType').html('By Atty/All Months');
            $('#casecount-year').show();
            $('#casecount-types').show();
        }
    })


    $(document.body).on('click', '#caseCountProceed', function () {
        console.log($('#caseCountReportForm').serialize());
    });

    $(document.body).on('click', '#caseCountProceed', function () {

        var CaseCountReportUrl = $("#CaseCountReportUrl").val();
        //alert(CaseCountReportUrl);
        caseCountTable.destroy();
//        $('#CaseCountpReport_list').modal('show');

        caseCountTable = $('#caseCountReport_dataTable').DataTable({
            buttons: [{
                    extend: 'print',
                    title: 'Case Count Report Results',
                    autoPrint: 'false',
                    footer: 'true',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 7, 9]
                    }
                }],
            lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            processing: true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
			},
            serverSide: true,
            bFilter: true,
            scrollX: true,
            sScrollY: 400,
            bScrollCollapse: true,
            destroy: true,
//                order: [[4, "asc"]],
            autoWidth: true,
            ajax: {
                url: CaseCountReportUrl,
                type: "POST",
                "data": function (ae) {
                    ae.searchData = $('#caseCountReportForm').serializeArray();
                }
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).attr("id", aData[0]);
                return nRow;
            },
            columnDefs: [{
                    targets: 3,
                    className: 'row-width',
                    width: 200,
                }, {
                    targets: 13,
                    className: 'row-width',
                    width: 200,
                }],
            select: {
                style: 'single'
            }
        });
        $('#case_count').modal('hide');

    });
</script>


<!-- SCRIPT FOR CASE REPORT -->
<script type="text/javascript">
    $(document.body).on('click', '#CaseRepotProceed', function () {
        referralDataTable.destroy();
        $('#case_referral_list_modal').modal('show');

        referralDataTable = $('#caseReferralReport_table').DataTable({
            buttons: [{
                    extend: 'print',
                    title: 'Referral Report Results',
                    autoPrint: 'false',
                    footer: 'true',
                }],
            lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            processing: true,
            serverSide: true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
			},
            bFilter: true,
            scrollX: true,
            sScrollY: 400,
            bScrollCollapse: true,
            destroy: true,
            autoWidth: true,
            ajax: {
                url: "<?php echo base_url(); ?>reports/getSearchReferralReport",
                type: "POST",
                "data": function (ae) {
                    ae.searchData = $('#caseReferralForm').serializeArray();
                }
            },
            select: {
                style: 'single'
            }
        });
        $('#referral_report').modal('hide');

    });

</script>



<script type="text/javascript">
    var referralDataTable = '';
    var caseActDataTable = '';
    var taskTable = '';
    var caseInjuryTable = '';
    var markupTable = '';
    $(document).ready(function () {

        $('input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('input[type=number]').keypress(function (event) {
            var maxlength = $(this).attr('max-length');

            if (typeof maxlength != 'undefined') {
                if (this.value.length >= maxlength) {
                    event.preventDefault();
                }
            }
        });
        //referral DataTable Assign
        referralDataTable = $('#caseReferralReport_table').DataTable();
        //Case Act DataTable assign
//    caseActDataTable = $('#caseActivityReport_table').DataTable();
        //Case Activity DataTable Listing
        caseActDataTable = $('#caseActivityReport_table').DataTable({
            dom: 'Blfrtip',
            buttons: [{
                    extend: 'print',
                    title: 'Case Activity Report',
                    autoPrint: 'false',
                    footer: 'true',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        }
                    },
                }],
            lengthMenu: [10, 25, 50, 100, 500, 1000, "All"],
            autoWidth: true,
			stateSave: true,
            sScrollY: "400px",
            bScrollCollapse: true,
            destroy: true,
            columns: [
                {data: "caseno", title: 'Case No'},
                {data: "casetype", title: 'Type'},
                {data: "casestat", title: 'Status'},
                {data: "atty_hand", title: 'Atty Hand'},
                {data: "date", title: 'Date'},
                {data: "initials", title: 'Who'},
                {data: "name", title: 'Name'},
                {data: "event", title: 'Event'},
            ],
            columnDefs: [{
                    targets: 7,
                    className: 'row-width',
                    width: 200,
                }],
        });


        $('#tastReport_table tbody').on('click', 'tr', function () {
            //  alert("hi");
            if ($(this).hasClass('selected')) {
                $('.pullCase').prop('disabled', true); //console.log( "hi");
            } else {
                $('.pullCase').prop('disabled', false); // console.log( "hi1");
            }
        });

        $('#markupReport_dataTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $('.pullCase').prop('disabled', true); //console.log( "hi");
            } else {
                $('.pullCase').prop('disabled', false); // console.log( "hi1");
            }
        });

        markupUdfTable = $('#markupUdf_dataTable').DataTable();

        $('.injurydate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
//        startDate: new Date()
        });

        $('#start_d_r').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#end_d_r').datetimepicker('setStartDate', ev.date);
        });

        $('#end_d_r').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#start_d_r').datetimepicker('setEndDate', ev.date);
        });

        $('#start_ca_d_r').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#end_ca_d_r').datetimepicker('setStartDate', ev.date);
        });

        $('#end_ca_d_r').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#start_ca_d_r').datetimepicker('setEndDate', ev.date);
        });

        // Task Report Datepicker
        $('#start_d_req').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#end_d_req').datetimepicker('setStartDate', ev.date);
        });

        $('#end_d_req').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#start_d_req').datetimepicker('setEndDate', ev.date);
        });

        // Markup Report Datepicker
        $('#start_followup').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#end_followup').datetimepicker('setStartDate', ev.date);
        });

        $('#end_followup').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#start_followup').datetimepicker('setEndDate', ev.date);
        });

        $('#start_date_open').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#end_date_open').datetimepicker('setStartDate', ev.date);
        });

        $('#end_date_open').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#start_date_open').datetimepicker('setEndDate', ev.date);
        });

        // Markup Report with UDF Datepicker Range
        $('#start_udf_open').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#end_udf_open').datetimepicker('setStartDate', ev.date);
        });

        $('#end_udf_open').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
        }).on('changeDate', function (ev) {
            $('#start_udf_open').datetimepicker('setEndDate', ev.date);
        });



//
//    $('.injurytime').clockpicker({
//         autoclose: true
//    });

//    $('.injurycolor').colorpicker({
//         autoclose: true
//    });

        // Upload File
        $('#uploadBtn3').change(function () {
            if ($(this).val() != '') {
                $('#uploadFile3').html($(this).val());
            } else {
                $('#uploadFile3').html('Upload File');
            }
        });

        $('.modal').on('hidden.bs.modal', function () {
            $(this).find('input[type="text"],input[type="email"],textarea,select,input[type="checkbox"]').each(function () {
//            console.log(this);
                $(this).val('').trigger('change');
                $(this).iCheck('uncheck');
                this.value = '';
            });
        });

        $('.modal').on('shown.bs.modal', function () {
            $('#' + this.id).data('bs.modal').options.backdrop = 'static';
        });



        //Referral Ajax Request
        $(document.body).on('click', '#getReferralReport', function () {
            referralDataTable.destroy();
            $('#case_referral_list_modal').modal('show');

            referralDataTable = $('#caseReferralReport_table').DataTable({
                buttons: [{
                        extend: 'print',
                        title: 'Referral Report Results',
                        autoPrint: 'false',
                        footer: 'true',
                    }],
                lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
                processing: true,
                serverSide: true,
				stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
				},
                bFilter: true,
                scrollX: true,
                sScrollY: 400,
                bScrollCollapse: true,
                destroy: true,
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getSearchReferralReport",
                    type: "POST",
                    "data": function (ae) {
                        ae.searchData = $('#caseReferralForm').serializeArray();
                    }
                },
                select: {
                    style: 'single'
                }
            });
            $('#referral_report').modal('hide');

        });

        $(document.body).on('click', '#generateCaseActivity', function () {
            $.blockUI();
            $.ajax({
                url: '<?php echo base_url(); ?>reports/getCaseActivityReport',
                data: $('#caseActivityForm').serialize(),
                method: "POST",
                dataType: 'json',
                success: function (result) {
                    $('#case_activity_report').modal('hide');
                    $.unblockUI();
                    if (result.status > 0) {
                        console.log(result.status);
                        caseActDataTable.clear();
                        caseActDataTable.rows.add(result.data);

                        setTimeout(function () {
                            caseActDataTable.draw();
                        }, 200);

                    }
                    $('#caseActivityReportData').modal('show');
                }

            });
        });

        $(document.body).on('click', '#generateCaseActivity', function () {
//        $.blockUI();
            caseActDataTable.destroy();
            $('#caseActivityReportData').modal('show');

            caseActDataTable = $('#caseActivityReport_table').DataTable({
                buttons: [{
                        extend: 'print',
                        title: 'Case Activity Report Results',
                        autoPrint: 'false',
                        footer: 'true',
                    }],
                lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
                processing: true,
                serverSide: true,
				stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
				},
                bFilter: true,
                scrollX: true,
                sScrollY: 400,
                bScrollCollapse: true,
                destroy: true,
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getCaseActivityReportNew",
                    type: "POST",
                    "data": function (ae) {
                        ae.searchData = $('#caseActivityForm').serializeArray();
                    }
                },
                columnDefs: [{
                        targets: 7,
                        className: 'row-width',
                        width: 200,
                    }],
            });
            $('#case_activity_report').modal('hide');
        });

        $(document.body).on('click', '#filterTaskReport', function () {
            console.log('test');
            taskTable.destroy();
            $('#taskReportListing').modal('show');
            var serarray = $('#taskActitvityForm').serializeArray();

            taskTable = $('#tastReport_table').DataTable({
                buttons: [{
                        extend: 'print',
                        title: 'Task Activity Report Results',
                        autoPrint: 'false',
                        footer: 'true',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7]
                        }
                    }],
                lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
                processing: true,
                serverSide: true,
				stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
					data.order = [4, "ASC"];
				},
                bFilter: true,
                scrollX: true,
                sScrollY: 400,
                bScrollCollapse: true,
                destroy: true,
                order: [[4, "asc"]],
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getFilterTaskReportNew",
                    type: "POST",
                    "data": function (ae) {
                        ae.searchData = serarray;
                        //ae.searchData = $('#taskActitvityForm').serializeArray();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData[0]);
                    return nRow;
                },
                /*columnDefs: [{
                        targets: 7,
                        className: 'row-width',
                        width: 200,
                    }],*/
                select: {
                    style: 'single'
                }
            });
            $('#task_report').modal('hide');
        });
        
       $(document.body).on('click', '#getCaseReportModel', function () {
            //console.log($('#caseInjuryForm').serializeArray());
        });

        $(document.body).on('click', '#getMarkUpRecord', function () {
            markupTable.destroy();
            $('#markupReport_Content').modal('show');

            markupTable = $('#markupReport_dataTable').DataTable({
                buttons: [{
                        extend: 'print',
                        title: 'Markup Report Results',
                        autoPrint: 'false',
                        footer: 'true',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 7, 9]
                        }
                    }],
                lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
                processing: true,
                serverSide: true,
				stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
				},
                bFilter: true,
                scrollX: true,
                sScrollY: 400,
                bScrollCollapse: true,
                destroy: true,
//                order: [[4, "asc"]],
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getFilterMarkupReport",
                    type: "POST",
                    "data": function (ae) {
                        ae.searchData = $('#markuReportForm').serializeArray();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData[0]);
                    return nRow;
                },
                columnDefs: [{
                        targets: 3,
                        className: 'row-width',
                        width: 200,
                    }, {
                        targets: 13,
                        className: 'row-width',
                        width: 200,
                    }],
                select: {
                    style: 'single'
                }
            });
            $('#markup_report').modal('hide');
        });







        $(document.body).on('click', '#getMarkupUdfReport', function () {

            markupUdfTable = $('#markupUdf_dataTable').DataTable({
                buttons: [{
                        extend: 'print',
                        title: 'Markup Report User Defined',
                        autoPrint: 'false',
                        footer: 'true',
//                    exportOptions: {
//                        columns: [ 0, 1, 2, 3,4,5,7,9]
//                    }
                    }],
                lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
                processing: true,
                serverSide: true,
				stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
				},
                bFilter: true,
                scrollX: true,
                sScrollY: 400,
                bScrollCollapse: true,
                destroy: true,
//                order: [[4, "asc"]],
                autoWidth: true,
                ajax: {
                    url: "<?php echo base_url(); ?>reports/getFilterMarkupReportUDF",
                    type: "POST",
                    "data": function (ae) {
                        ae.searchData = $('#markupWithUdf').serializeArray();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("id", aData[0]);
                    return nRow;
                },
                columnDefs: [{
                        targets: 6,
                        className: 'row-width',
                        width: 200,
                    }],
                select: {
                    style: 'single'
                }
            });
            $('#markup_report_udf').modal('hide');
            $('#showMarkupUdfReport').modal('show');
            //markupUdfTable
        });


        $(document.body).on('click', '#printReferrall', function () {
            referralDataTable.buttons('.buttons-print').trigger();
//        $('#case_referral_list_modal').modal('hide');
        });

        $(document.body).on('click', '#printCaseActivityReport', function () {
            caseActDataTable.buttons('.buttons-print').trigger();
            $('#caseActivityReportData').modal('hide');
        });

        $(document.body).on('click', '#printTaskList', function () {
            taskTable.buttons('.buttons-print').trigger();
//        $('#taskReportListing').modal('hide');
        });

        $(document.body).on('click', '#printMarkupReport', function () {
            markupTable.buttons('.buttons-print').trigger();
        });

        $(document.body).on('click', '#printMarkupUdfReport', function () {
            markupUdfTable.buttons('.buttons-print').trigger();
        });

        $(document.body).on('click', '#caseCountCancel', function () {
            $('#case_count').modal('hide');
        });



        $(document.body).on('click', '.pullCase', function () {
            //console.log($(this).closest('form').find('table'));
            var caseno = $(this).closest('form').find('table').find('tr.selected').attr('id');
            window.open("<?php echo base_url() . 'cases/case_details/' ?>" + caseno, '_blank');
        });


    });
</script>


<script>
    $(document).ready(function () {
        $('.contact').mask("(000) 000-0000");
        //change the value of search box according to search by
        $('#searchBy').change(function () {
            var searchText = $('#searchBy').val().split('.');
            var message = '';
            var placeholder = '';

            if (searchText[1] == 'contact')
            {
                $("#searchText").addClass("contact");
            } else {
                $("#searchText").val('');
                $("#searchText").unmask();
                $("#searchText").removeAttr("maxlength");
                $("#searchText").removeClass("contact");
            }
            $('#searchText').datetimepicker('remove');
            switch (searchText[1]) {
                case 'first':
                    message = 'Enter all the Part of the First Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'last':
                    message = 'Enter all the Part of the Last Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'firm':
                    message = 'Enter all the Part of the Firm Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'caseno':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'caseno_less':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'caseno_more':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'yourfileno':
                    message = 'Enter all or part of your own internal office file Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'social_sec':
                    message = 'Enter all or part of the Social Security Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'followup':
                    message = 'Display case follow up dates,use -date,+date or date1 to date2';
                    placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'contact':
                    message = "Enter all or part of the phone number to appear anywhere within the card or firm's for telephone number";
                    placeholder = 'Search By ie. (111)111-1111';
                    break;
                case 'birth_date':
                    message = 'Enter the date of birth or a range ie 2/3/1947 - 4/5/1947';
                    placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'birth_month':
                    message = 'Enter the number for a month ie: 1=Jan, 2=Feb, 12=Dec, 0=All';
                    placeholder = 'search by numbers in 0-12 Range';
                    break;
                case 'dateenter':
                    message = 'Display case date entered dates,use -date,+date or date1 to date2';
                    placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'dateopen':
                    message = 'Display case date opened dates,use -date,+date or date1 to date2';
                    placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'dateclosed':
                    message = 'Display case date closed dates,use -date,+date or date1 to date2';
                    placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
            }

            $('#searchText').attr("title", message);
            $('#searchText').attr("placeholder", placeholder);
        });

        $('#searchText').on('blur', function () {
            dtValue = $(this).val();
            //alert(dtValue);
            if (dtValue)
            {
                if ($('#searchBy').val() == 'cd.birth_date' || $('#searchBy').val() == 'c.dateenter' || $('#searchBy').val() == 'c.dateopen' || $('#searchBy').val() == 'c.dateclosed')
                {
                    var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
                    if (!dtRegex.test(dtValue))
                    {
                        $(this).parent().append('<span style="color:red;">Please enter a valid date</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() == 'cd.birth_month')
                {
                    var dtRegex = new RegExp(/^[0-9]{0,2}$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $(this).parent().append('<span style="color:red;">Please enter a valid month</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() != 'cd.contact' && $('#searchBy').val() != 'c.yourfileno' && $('#searchBy').val() != 'cd2.firm') {
                    var dtRegex = new RegExp(/^\s*[a-zA-Z0-9,\s]+\s*$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $('#searchText').parent().find('span').remove();
                        $(this).parent().append('<span style="color:red;">Please enter a valid input</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                }
            }
        });
    });
</script>





