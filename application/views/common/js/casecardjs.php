<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/mdtimepicker.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') . '/css/plugins/jquery-slider/jquery.radiant_scroller.css'; ?>" media="all">
<script src="<?php echo base_url('assets'); ?>/js/plugins/clockpicker/mdtimepicker.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/dropzone/dropzone.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/ckeditor/ckeditor.js"></script>

<script src="<?php echo base_url('assets') . '/js/plugins/jquery-slider/jquery.radiant_scroller.js'; ?>"></script>