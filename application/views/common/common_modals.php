<?php
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '')
            unset($holodaylist[$key]);
    }
}
if (isset($staff_details)) {
    $Uservacation = isset($this->data['staff_details'][0]->vacation) ? (explode("##", trim($this->data['staff_details'][0]->vacation))) : '';
    if (!empty($Uservacation)) {
        $Uservacation = array_map('trim', $Uservacation);
        foreach ($Uservacation as $key => $value) {
            if (is_null($value) || $value == '')
                unset($Uservacation[$key]);
        }
    }
}
?>
<style>
    .chosen-container{ width: 100% !important;}.pac-container{z-index: 9999;}.injuryalignment .panel-body{ padding: 0px;}.addinjuryalignment .modal-body { padding: 20px 20px 30px 20px !important;}.injuryaddalignment input{ width: auto !important;}
    #eamslist{
        z-index: 9999 !important;
    }
    #caseActUploadDocSpan {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        cursor: pointer;
    }
    #caseActUploadDocSpan:hover {
        background-color: #4d7ab1;
        border-color: #4d7ab1;
        color: #fff;
    }
    #caseActUploadDoc {
        margin-left: -5px;
        border-radius: 0;
        cursor: pointer;
    }
    #caseActUploadDoc:hover {
        background-color: #4d7ab1;
        border-color: #4d7ab1;
        color: #fff;
    }
    #caseActUploadDocDelete {
        margin-left: -5px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        cursor: pointer;
    }
    #caseActUploadDocDelete:hover {
        background-color: #ed5565;
        border-color: #ed5565;
        color: #fff;
    }
</style>

<div id="addcontact" class="modal fade" data-backdrop="static" data-keyboard="false" style="z-index: 9999 !important;" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Contact</h4>
            </div>
            <div class="modal-body">
                <form id="addRolodexForm" method="post" enctype="multipart/form-data">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#name">Name</a></li>
                            <li class=""><a data-toggle="tab" href="#contact">Telephone/Email</a></li>
                            <li class=""><a data-toggle="tab" href="#details">Details</a></li>
                            <li class=""><a data-toggle="tab" href="#comments">Comments</a></li>
                            <li class=""><a data-toggle="tab" href="#address">Addresses</a></li>
                            <li class=""><a data-toggle="tab" href="#profile">Profile</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="name" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>General Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Salutation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input name="form_submit" type="hidden" value="1">
                                                            <select name="card_salutation" id="card_salutation" class="form-control <?php
                                                            if (isset($system_data[0]->cdsaldd) && $system_data[0]->cdsaldd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" >
                                                                <option value="">Select Salutation</option>
                                                                <?php foreach ($saluatation as $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">First <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="card_first" name="card_first" class="form-control" value="" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Middle</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_middle" class="form-control" value=""  />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_last" class="form-control" value="" />
                                                        </div></div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Suffix</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_suffix" id="card_suffix" class="form-control <?php
                                                            if (isset($system_data[0]->cdsufdd) && $system_data[0]->cdsufdd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>">
                                                                <option value="" disabled="disabled">Select Suffix</option>
                                                                <?php
                                                                if (isset($suffix)) {
                                                                    foreach ($suffix as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Title</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_title" class="form-control select2Class" >
                                                                <option value="">Select Title</option>
                                                                <?php
                                                                if (isset($title)) {
                                                                    foreach ($title as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Category <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select placeholder="Select Category" name="card_type" id="card_type" class="form-control <?php
                                                            if (isset($system_data[0]->cardtypedd) && $system_data[0]->cardtypedd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" required>
                                                                        <?php
                                                                        if (isset($category)) {
                                                                            foreach ($category as $val) {
                                                                                ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>

                                                        </div>

                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Popular</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_popular" class="form-control" value="" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Sal for Ltr   <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="This should normally be left empty.  However, you may sometimes decide to fill this field in to override the default salutation in form letters.  For example : To the honorable Judge Smith

                                                                                                                 " id="sfl"></i></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_letsal" class="form-control" value="" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Firm/Corp <span class="asterisk">*</span></label>
                                                        <div class="col-sm-6 no-padding">
                                                            <input type="text" name="card2_firm" class="form-control" value="" /></div><div class="col-sm-1 no-padding"><a href="#" class="btn btn-md btn-primary pull-right rolodex-company">A</a>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address Lookup</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input id="autocomplete" placeholder="Enter your address" class="form-control" onFocus="geolocate()" type="text">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address 1 <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_address1" id="street_number" class="form-control" value="" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <div class="row">

                                                            <div class="col-lg-12">
                                                                <label class="col-sm-5 no-left-padding">City <span class="asterisk">*</span></label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" id="locality" name="card2_city" class="form-control" value="" />
                                                                </div></div>


                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Venue</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
<!--                                                        <input type="text" name="card2_venue" class="form-control" />-->
                                                            <select class="form-control cstmdrp cstmdrp-placeholder" name="card2_venue" placeholder="Select Venue">
                                                                <option value="" disabled>Select Venue</option>
                                                                <option value=""></option>
                                                                <?php
                                                                if (isset($Venue)) {
                                                                    foreach ($Venue as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-sm-6">
                                                    
                                                    <div class="form-group">
                                                        <div class="row">

                                                            <div class="col-lg-12">
                                                                <a class="btn pull-right btn-primary btn-sm" href="#eamslist" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Add Eamsref" data-keyboard="false">EAMS</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">EAMS Name</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="hidden" name="card2_eamsref">
                                                            <input type="text" id="eamsref_name" name="eamsref_name" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_address2" id="route" class="form-control" value="" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">State <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="administrative_area_level_1" name="card2_state" class="form-control" value="" />
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Zip <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" id="postal_code" name="card2_zip" class="form-control" value="" />
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="contact" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Personal Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Business</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_business" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Home</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_home" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Cell</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_car" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Email</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_email" class="form-control" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Note</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_notes" class="form-control" />
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Business Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Telephone 1</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone1" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Telephone 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone2" placeholder="(xxx) xxx-xxxx" class="form-control contact"  />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Tax ID</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_tax_id" class="form-control" /><!-- onlyNumbers -->
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax2" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="details" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Contact Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">SS No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_social_sec" class="form-control social_sec"  placeholder="234-56-7898"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Date of Birth</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_birth_date" pattern="\d{1,2}/\d{1,2}/\d{4}"  class="form-control datepickerClass date-field" data-mask="99/99/9999" placeholder="mm/dd/YY ex: 03/25/1995" id="datepicker" onkeydown="return false" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">License No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_licenseno" class="form-control" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Specialty</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_specialty" class="form-control select2Class">
                                                                <option value="">Select Specialty</option>
                                                                <?php
                                                                if (isset($specialty)) {
                                                                    foreach ($specialty as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Maidan Name</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_mothermaid" class="form-control" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Card No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_cardcode" readonly class="form-control" value=""/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Firm No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_firmcode" readonly  class="form-control">
                                                        </div>        </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Interpreter Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Interpreter</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_interpret" class="form-control select2Class">
                                                                <option value="">Select Interpreter</option>
                                                                <option value="Y">Yes</option>
                                                                <option value="N">No</option>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>

                                                    <div class="form-group" id="data_1">
                                                        <label class="col-sm-5 no-padding">Language</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_language" class="select2Class form-control">
                                                                <?php foreach ($languageList as $key => $val) { ?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="float-e-margins">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="">Original</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime1" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Last</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime2" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Original2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime3" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Last2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime4" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="comments" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card2_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="address" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Alternate Name and Addresses</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address1</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing1" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address2</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing2" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address3</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing3" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address4</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing4" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="profile" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Occupation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_occupation" class="form-control" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Employer</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_employer" class="form-control" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date of Hire</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="date_of_hire" pattern="\d{1,2}/\d{1,2}/\d{4}"  class="form-control datepickerClass date-field weekendsalert" data-mask="99/99/9999" placeholder="mm/dd/YY ex: 03/25/1995" id="datepicker" autocomplete="off" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Profile Picture</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="file" name="profilepic" id="profilepic" class="form-control" accept="image/*" />
                                                            <span style="color:red">Max Upload size 5Mb</span>
                                                            <input type="hidden" name="hiddenimg" id="hiddenimg" value="" >
                                                            <span id="imgerror" style="color:red"></span>
                                                        </div>
                                                        <div id="profilepicDisp" style="display:none">
                                                            <div class="form-group">
                                                                <img id="dispProfile" height="50" width="50">
                                                            </div>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group text-center marg-top15 text-right">
                        <input type="hidden" id="new_case" name="new_case" />
                        <input type="hidden" id="prospect_id" name="prospect_id" />
                        <input type="hidden" id="newRolodexCard" name="newRolodexCard" value="0"/>
                        <input type="hidden" id="oldCardValue" name="oldCardValue" value="0"/>
                        <input type="hidden" id="oldCaseNo" name="oldCaseNo" value="0"/>
                        <button type="button" id="addContact" class="btn btn-md btn-primary">Save</button>
                        <button type="button" id="cancelContact" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="addparty" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Find a Name in the Rolodex</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <form action="" role="form" name="rolodex_search_parties_form" id="rolodex_search_parties_form">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins marg-bot5">

                                <div class="ibox-title collapse-link">
                                    <h5 class="marg-top5">What you want to search for</h5>
                                    <div class="pull-right btn btn-primary btn-sm">Filters
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display: none;">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="rolodex_search_text" id="rolodex_search_text">
                                    </div>
                                    <div class="form-group">
                                        <label>Search By</label>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-firm" name="rolodex_search_parties"> <i></i> Name or Firm </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-social_sec" name="rolodex_search_parties"> <i></i> SS No </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-first" name="rolodex_search_parties"> <i></i> First Name </label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-first-last" name="rolodex_search_parties"> <i></i> Name (last,first) </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-email" name="rolodex_search_parties"> <i></i> Email </label></div>
                                            </div>
                                            <!-- <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-details" name="rolodex_search_parties"> <i></i> Details </label></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-company" name="rolodex_search_parties"> <i></i> Firm/Company </label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-licenseno" name="rolodex_search_parties"> <i></i> License No </label></div>
                                            </div>
                                            <!-- <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-relational" name="rolodex_search_parties"> <i></i> Relational </label></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-cardno" name="rolodex_search_parties"> <i></i> Card Number </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-type" name="rolodex_search_parties"> <i></i> Type </label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label id="rolodex_search_parties-error" class="error" for="rolodex_search_parties"></label>
                                        </div> </div>
                                    <div class="form-group no-bottom-margin">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div>
                            <div class="float-e-margins marg-bot5">
                                <div class="ibox-content">
                                    <a class="btn pull-right btn-primary btn-sm" href="#addcontact" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Add Contact" data-keyboard="false">Add Contact</a>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-responsive table-bordered table-hover table-striped dataTables-example" id="rolodexPartiesResult">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Firm/Company</th>
                                                        <th>Type</th>
                                                        <th>City</th>
                                                        <th>Address</th>
                                                        <th>Zip</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6 hidden">
                                            <input type="hidden" id="searched_party_fullname">
                                            <input type="hidden" id="searched_party_firmname">
                                            <div class="table-responsive">
                                                <table class="table no-bottom-margin" id="searchedPartiesTable">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Type :</strong>
                                                            </td>
                                                            <td id="searched_party_type"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Speciality :</strong>
                                                            </td>
                                                            <td id="searched_party_speciality"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Address :</strong>
                                                            </td>
                                                            <td id="searched_party_address"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>City :</strong>
                                                            </td>
                                                            <td id="searched_party_city"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Card Number :</strong>
                                                            </td>
                                                            <td id="searched_party_cardnumber"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Social Security No :</strong>
                                                            </td>
                                                            <td id="searched_party_ssno"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Email :</strong>
                                                            </td>
                                                            <td id="searched_party_email"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>License No :</strong>
                                                            </td>
                                                            <td id="searched_party_licenseno"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Home Phone :</strong>
                                                            </td>
                                                            <td id="searched_party_homephone"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100">
                                                                <strong>Business :</strong>
                                                            </td>
                                                            <td id="searched_party_business"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-sm-12 text-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary addSearchedParties">Proceed</button>
                            <button data-toggle="modal" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                            <button data-toggle="modal" class="btn btn-primary editPartyCard">Edit</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="editcasedetail" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Case Information</h4>
            </div>
            <div class="modal-body">
                <form id="edit_case_details" method="post">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#case_info">Case</a></li>
                            <li class=""><a data-toggle="tab" href="#staff_info">Staff</a></li>
                            <li class=""><a data-toggle="tab" href="#case_dates">Dates</a></li>
                            <li class=""><a data-toggle="tab" href="#quick_note">Quick Notes</a></li>
                            <li class=""><a data-toggle="tab" href="#caption_tab">Caption</a></li>
                            <li class=""><a data-toggle="tab" href="#messages">Messages</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="case_info" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Case Information</h5>
                                                </div>
                                                <div class="ibox-content marg-bot15">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Type of Case</label>
                                                        <?php
                                                        if (isset($casetypedd) && $casetypedd == '1') {
                                                            $classTpDD = 'cstmdrp';
                                                        } else {
                                                            $classTpDD = 'select2Class';
                                                        }
                                                        ?>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control <?php echo $classTpDD; ?>" id="editCaseType" name="editCaseType"></select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                                        <?php
                                                        if (isset($casestatdd) && $casestatdd == '1') {
                                                            $casestatdd = 'cstmdrp';
                                                        } else {
                                                            $casestatdd = 'select2Class';
                                                        }
                                                        ?>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control <?php echo $casestatdd; ?>" id="editCaseStat" name="editCaseStat"></select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Other Cases</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" id="editOtherClass" name="editOtherClass">
                                                                <option value=""></option>
                                                                <option value="No">No</option>
                                                                <option value="Yes">Yes</option>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>File Information</h5>
                                                </div>
                                                <div class="ibox-content marg-bot15">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Your File No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" id="fileno" name="fileno"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">File Location</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" id="fileloc" name="fileloc"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Closed File No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" id="closedfileno" name="closedfileno"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Sub-type</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <select name="editCaseSubType" class="form-control select2Class">
                                                                        <option value="">Select SubType</option>
                                                                        <?php
                                                                        if (isset($sub_type)) {
                                                                            foreach ($sub_type as $val) {
                                                                                ?>
                                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div></div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="no-left-padding">Venue</label>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control" name="editCaseVenue1" id="editCaseVenue1" readonly />
                                                                    <input type="hidden" class="form-control" name="editCaseVenue" id="editCaseVenue"  readonly />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <a  href="#" class="venuelist">Venue</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="staff_info" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox-title">
                                                    <h5>Staff Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <?php
                                                        if (isset($cdstaffdd) && $cdstaffdd == '1') {
                                                            $casestaffdrp = 'cstmdrp';
                                                        } else {
                                                            $casestaffdrp = 'select2Class';
                                                        }
                                                        ?>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-6 no-left-padding">Attorney Responsible</label>
                                                                <div class="col-sm-6 no-padding marg-bot5">
                                                                    <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttyResp" name="editCaseAttyResp"></select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-6 no-left-padding">Attorney Handling</label>
                                                                <div class="col-sm-6 no-padding marg-bot5">
                                                                    <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttyHand" name="editCaseAttyHand"></select>
                                                                </div>
                                                            </div></div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-6 no-left-padding">Paralegal Handling</label>
                                                                <div class="col-sm-6 no-padding marg-bot5">
                                                                    <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttyPara" name="editCaseAttyPara"></select>
                                                                </div></div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-6 no-left-padding">Secretary Handling</label>
                                                                <div class="col-sm-6 no-padding marg-bot5">
                                                                    <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttySec" name="editCaseAttySec"></select>
                                                                </div></div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="case_dates" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="ibox-title inline-block-100">
                                                    <h5 class="">Case Dates/Important Dates</h5>
                                                    <a class="btn pull-right btn-primary btn-sm" target="_blank" href = "<?php echo base_url('cases/tasks/' . $case_id) ?>">Add P&S Task</a>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Entered</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <input type="text" class="form-control dates datepicker" id="casedate_entered" name="casedate_entered"/>
                                                                </div></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Open</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <input type="text" class="form-control dates datepicker" id="casedate_open" name="casedate_open" />
                                                                </div></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Follow Up</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <input type="text" class="form-control"  id="casedate_followup" name="casedate_followup"/>
                                                                </div></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Closed</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <input type="text" class="form-control" id="casedate_closed" name="casedate_closed"/>
                                                                </div></div>

                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?php
                                                            if (isset($ref) && $ref == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label class="col-sm-7 no-left-padding">Referred by</label>
                                                                    <div class="col-sm-5 no-padding marg-bot5">
                                                                        <select class="form-control select2Class" name="case_reffered_by" id="case_reffered_by">

                                                                            <?php
                                                                            if (isset($case_cards) && $case_cards != '') {
                                                                                foreach ($case_cards as $key => $details) {
                                                                                    $name = "";
                                                                                    if ($details->firm == '') {
                                                                                        $name = $details->last . ', ' . $details->first;
                                                                                    } else {
                                                                                        $name = $details->firm;
                                                                                    }
                                                                                    ?>
                                                                                    <option value=""></option>
                                                                                    <option value="<?php echo $details->cardcode; ?>"><?php echo $name; ?></option>

                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div></div>
                                                                <div class="clearfix"></div>
                                                                <?php
                                                            }
                                                            if (isset($pns) && $pns == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label class="col-sm-7 no-left-padding">P & S (Is applicant P & S?)</label>
                                                                    <div class="col-sm-5 no-padding marg-bot5">
                                                                        <select class="form-control select2Class" name="case_ps" id="case_ps">
                                                                            <option value=""></option>
                                                                            <option value="Y">Yes</option>
                                                                            <option value="N">No</option>
                                                                        </select>
                                                                    </div></div>
                                                                <div class="clearfix"></div>
                                                                <?php
                                                            }
                                                            if (isset($pnsdt) && $pnsdt == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label class="col-sm-7 no-left-padding">P & S Date</label>
                                                                    <div class="col-sm-5 no-padding marg-bot5">
                                                                        <input type="text" class="form-control dates datepicker" id="casedate_psdate" name="casedate_psdate"/>
                                                                    </div></div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <div class="clearfix"></div>
                                                            <?php
                                                            if (isset($refdt) && $refdt == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label class="col-sm-7 no-left-padding">Referred</label>
                                                                    <div class="col-sm-5 no-padding marg-bot5">
                                                                        <input type="text" class="form-control dates datepicker" id="casedate_referred" name="casedate_referred"/>
                                                                    </div></div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="quick_note" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="ibox-title">
                                                    <h5> Add Note</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <textarea class="form-control" style="width: 100%;height: 200px;" name="editcase_quicknote" id="editcase_quicknote"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="caption_tab" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox-title">
                                                    <h5>Caption</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <?php
                                                        if (isset($CaptionAccess) && $CaptionAccess->captionAccess == 1) {
                                                            $displayCptn = '';
                                                        } else {
                                                            $displayCptn = 'readonly';
                                                        }
                                                        ?>
                                                        <textarea class="form-control" style="height: 300px;width: 100%;" name="editcase_caption" id="editcase_caption" <?php echo $displayCptn; ?> ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="messages" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-lg-6">
<!--                                                <div class="ibox-title">
                                                    <h5>Messages</h5>
                                                </div>-->
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Message 1</label>
                                                        <textarea class="form-control" name="caseedit_message1" id="caseedit_message1" style="height: 80px;width: 100%;background:<?php echo isset($main1->msg1) ? $main1->msg1 : '' ?>;color:<?php echo isset($main1->msg1c) ? $main1->msg1c : '' ?>"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Message 2</label>
                                                        <textarea class="form-control" name="caseedit_message2" id="caseedit_message2" style="height: 80px;width: 100%;background:<?php echo isset($main1->msg2) ? $main1->msg2 : '' ?>;color:<?php echo isset($main1->msg2c) ? $main1->msg2c : '' ?>"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Message 3</label>
                                                        <textarea class="form-control" name="caseedit_message3" id="caseedit_message3" style="height: 80px;width: 100%;background:<?php echo isset($main1->msg3) ? $main1->msg3 : '' ?>;color:<?php echo isset($main1->msg3c) ? $main1->msg3c : '' ?>"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Message 4</label>
                                                        <textarea class="form-control" name="caseedit_message4" id="caseedit_message4" style="height: 80px;width: 100%;background:<?php echo isset($main1->msg4) ? $main1->msg4 : '' ?>;color:<?php echo isset($main1->msg4c) ? $main1->msg4c : '' ?>"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $caseNo = $this->uri->segment(3); ?>
                    <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo ?>">
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-btn text-center">

                    <button type="submit" id="savecase" class="btn btn-primary btn-md">Save</button>
                    <button type="button" class="btn btn-primary btn-md btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$session_Arr = $this->session->userdata('user_data');
?>

<div id="addcaseact"  class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class=" modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Case Activity</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="add_new_case_activity" class="dropzone" enctype="multipart/form-data" name="add_new_case_activity" method="post" action="<?php echo base_url(); ?>cases/addEditActivity" onSubmit="">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-26">General</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-28">Properties</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-29">OLE</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-26" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>General</h5>
                                            <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" name="caseActNo" id="caseActNo">
                                            <input type="hidden" value="" name="activity_no" id="activity_no">
                                        </div>
                                        <div class="ibox-content">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control add_case_cal datepickerClass" name="add_case_cal" id="add_case_cal" value="<?php echo date('m/d/Y'); ?>" readonly/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Time</label>
                                                        <div class="col-sm-7 no-padding marg-bot5 uk-autocomplete">
                                                            <input type="text" class="form-control" name="add_case_time" id="add_case_time" data-uk-timepicker="{format:'12h'}"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding marg-bot5">Origin Initials</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" value="<?php echo $session_Arr['initials']; ?>" name="orignal_ini" id="original_ini" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding marg-bot5">Last Initials</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" value="<?php echo $this->session->userdata('user_data')['initials']; ?>" name="last_ini" id="last_ini" class="form-control" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15 inline-block-100">
                                                        <!--                                                        <button type="button" class="btn btn-md btn-primary pull-right marg-left10">Save</button>-->
                                                        <button type="button" class="autorecover btn btn-md btn-primary pull-right">Auto Recover</button>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Event<span class="asterisk">*</span></label>
                                                        <textarea class="form-control" style="height: 150px;width: 100%;" name="activity_event" id="activity_event"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-27" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Checks, Fees and Costs</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 300px;">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Attorney/parralegal</label>
                                                    <select class="form-control select2Class" name="case_act_atty" id="case_act_atty">
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Time/Minutes</label>
                                                    <input class="form-control" type="text"  name="case_act_time"  id="case_act_time" onkeypress='validate(event)'/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Hourly Rate</label>
                                                    <input class="form-control" type="number" min="0"  value="0" name="case_act_hour_rate" id="case_act_hour_rate"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Costs / Check Amount</label>
                                                    <input class="form-control" type="number"  min="0" value="0.00" name="case_act_cost_amt" id="case_act_cost_amt"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <select class="form-control select2Class" name="activity_title" id="activity_title">
                                                        <?php
                                                        $jsonactivity_type = json_decode(activity_title);
                                                        foreach ($jsonactivity_type as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Type of Fee or Cost</label>
                                                    <select name="activity_fee" id="activity_fee" class="form-control select2Class">
                                                        <?php
                                                        $jsonfee_cost = json_decode(fee_cost);
                                                        foreach ($jsonfee_cost as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="activity_fee1" id="activity_fee1"  style="display: none"> </div>
                                                <div class="form-group">
                                                    <label>Fee Classification</label>
                                                </div>
                                                <div class="form-group">
                                                    <span><label><input type="radio" value="1" class="i-checks" name="activity_classification"> None </label></span>
                                                    <span><label><input type="radio" value="2" class="i-checks" name="activity_classification"> Account Payable </label></span>
                                                    <span><label><input type="radio" value="3" class="i-checks" name="activity_classification"> Account Receivable </label></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-28" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot25  inline-block-100">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Genaral Note Properties</h5>
                                                </div>
                                                <div class="ibox-content caseact-generalnotes">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Category</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select id="case_category" name="case_category" class="form-control select2Class">
                                                                <?php foreach ($caseactcategory as $k => $v) { ?>
                                                                    <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Color</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select id="color_codes" name="color_codes" class="form-control select2Class">
                                                                <?php
                                                                $jsoncolor_codes = json_decode(color_codes);
                                                                foreach ($jsoncolor_codes as $key => $option) {
                                                                    echo '<option value=' . $key . '>' . $option[0] . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Security</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="security" id="security" class="form-control select2Class">
                                                                <?php
                                                                $jsonsecurity = json_decode(security);
                                                                foreach ($jsonsecurity as $key => $option) {
                                                                    echo '<option value=' . $key . '>' . $option . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Type of Note</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <span><label><input type="checkbox" name="activity_mainnotes" id="activity_mainnotes" class="i-checks" value="1"> Show In All </label></span>
                                                        <span><label><input type="checkbox" name="activity_redalert" id="activity_redalert" class="i-checks" value="1"> Red Alert </label></span>
                                                        <span><label><input type="checkbox" name="activity_upontop" id="activity_upontop" class="i-checks" value="1"> Up On Top </label></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Type of activity</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" id="activity_typeact" name="activity_typeact">
                                                                <?php
                                                                $jsonactivity_type = json_decode(type_of_activity);
                                                                foreach ($jsonactivity_type as $key => $option) {
                                                                    echo '<option value=' . $key . '>' . $option . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix">&nbsp;</div>
                                                    <div class="clearfix">&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="dropzonePreview" class="row dz-default dz-message">
                                            <div class="col-sm-12">
                                                <span class="label label-info">Click here to attach file</span>
                                            </div>
                                        </div>
                                        <div class="row documentAttch hidden">
                                            <div class="col-sm-12">

                                                <div class="col-sm-2"><div class="col-sm-2">Document:</div> <div class="documentPath col-sm-10"></div>Activity No: </div><div class="activityNo col-sm-10"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-29" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="ibox-title">
                                                    <h5>General Information</h5>
                                                </div>
                                                <div class="ibox-content caseact-generalinfo">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Document</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <div class="caseActUploadDocument" style="display:none; cursor:pointer;">
                                                                <span class="btn btn-primary btn-sm" id="caseActUploadDocSpan"></span>
                                                                <a download="" class="btn btn-sm btn-primary btn-outline view_attach" id="caseActUploadDoc" data-filename="f38_2.jpg"  href=""><i class="icon fa fa-file-text-o"></i> </a>
                                                                <a class="btn btn-sm btn-danger btn-outline delete_attach" id="caseActUploadDocDelete" data-filename="f38_2.jpg"><i class="icon fa fa-trash"></i> </a>
                                                            </div>
                                                            <input type="hidden" class="" id="caseact_uploaddocDefault" name="caseact_uploaddocDefault">
                                                            <div class="fileContainer upload-button" style="margin-top:5px; text-align: left; padding-left:5px;display: block;">
                                                                <input type="file" id="uploadBtn3" name="">
                                                                <button type="button" id="uploadFile3" class="btn btn-sm btn-primary">Upload</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Document Type</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="activity_doc_type" id="activity_doc_type"/>
                                                            <?php
                                                            $jsonactivity_doc_type = json_decode(activity_doc_type);
                                                            foreach ($jsonactivity_doc_type as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Style</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="activity_style" id="activity_style"/>
                                                            <?php
                                                            $jsonactivity_style = json_decode(activity_style);
                                                            foreach ($jsonactivity_style as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">&nbsp;</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            &nbsp;                                                       </div>
                                                    </div>
                                                    <div class="clearfix">&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-30" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Law Form Letter Margins</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 300px;">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Top</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_TM" id="case_act_TM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Bottom</label>
                                                    <input type="number" step="0.10" min="-1.00" value="0.00" name="case_act_BM" id="case_act_BM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Left</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_LM" id="case_act_LM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Right</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_RM" id="case_act_RM" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Page Width</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_PW" id="case_act_PW" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Page Height</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_PH" id="case_act_PH" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Orientation</label>
                                                    <select name="case_act_orientation" id="case_act_orientation" class="form-control select2Class">
                                                        <option value="0"></option>
                                                        <option value="1">Portrait</option>
                                                        <option value="2">Landscape</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Copies</label>
                                                    <input type="number" value="0" name="case_act_copies" id="case_act_copies" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-31" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Billing</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 415px;">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Entry Type</label>
                                                    <select class="form-control select2Class" name="activity_entry_type" id="activity_entry_type">
                                                        <option value="">Select Entry Type</option>
                                                        <?php
                                                        $jsonactivity_entry_type = json_decode(activity_entry_type);
                                                        foreach ($jsonactivity_entry_type as $key => $option) {
                                                            echo '<option data-key=' . $option[1] . ' value=' . $key . '>' . $option[0] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group hidden">
                                                    <label>Freeze Until</label>
                                                    <span class="freezeUntil"></span>
                                                </div>
                                            </div>


                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Team no</label>
                                                    <input type="number" class="form-control" value="0" min='0'id="activity_teamno" name="activity_teamno" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Attorney/Staff</label>
                                                    <select class="form-control select2Class" id="activity_staff" name="activity_staff">
                                                        <?php
                                                        $username = isset($username) ? $username : $this->session->userdata('user_data')['username'];
                                                        if (isset($staffList)) {
                                                            foreach ($staffList as $val) {
                                                                ?>
                                                                <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $username) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Short Description</label>
                                                    <textarea class="form-control" style="width: 100%;height: 100px;" id="activity_short_desc" name="activity_short_desc"></textarea>
                                                </div>

                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Billing Cycle</label>
                                                    <input type="text" readonly name="activity_biling_cycle" id="activity_biling_cycle" value="<?php echo date('m/d/Y'); ?>" class="form-control datepickerClass" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of Last Payment <span class="dateoflastpayment"></span></label>
                                                    <a class="btn btn-sm btn-primary pull-right no-bottom-margin" href="#adddefault" data-toggle="modal">Defaults</a>
                                                </div>
                                                <div class="form-group">
                                                    <label>Event/Detailed Description</label>
                                                    <textarea class="form-control" style="width: 100%;height: 150px;" id="activity_event_desc" name="activity_event_desc"></textarea>
                                                </div>


                                                <div id="payment" class="billingtab tab1 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Payment Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 370px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Check postmark Date</label>
                                                                        <input type="text" readonly class="form-control datepickerClass" name="activity_postmark_date" id="activity_postmark_date"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Payment due Date</label>
                                                                        <input type="text" readonly class="form-control datepickerClass" name="activity_payment_due" id="activity_payment_due"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Amount Paid</label>
                                                                        <input type="number" class="form-control" step="5" value="0" name="activity_amount" id="activity_amount"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Late Fee</label>
                                                                        <input type="number" class="form-control" step="5" value="0" name="activity_latefee" id="activity_latefee"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Check no</label>
                                                                        <input type="text" class="form-control numberinput" name="activity_check_no" id="activity_check_no" />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="fee" class="billingtab tab2 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 300px; height: 100%;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Total Time(Hours)</label>
                                                                        <input type="number" value="0.1000"  min='0' class="form-control" name="activity_bi_hour" id="activity_bi_hour" step="0.1000"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Total Time(minutes)</label>
                                                                        <input type="number" value="0"  min='0' class="form-control" name="activity_bi_min" id="activity_bi_min"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Hourly Rate</label>
                                                                        <input type="number" value="0"  min='0' class="form-control" name="activity_bi_rate" id="activity_bi_rate"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Total Fee</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" min='0' step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="cost" class="billingtab tab3 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Cost Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 155px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Cost</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_cost" id="activity_bi_cost" step="5"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Your Check No</label>
                                                                        <input type="text" maxlength="20" class="form-control" name="activity_check_no" id="activity_check_no"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="flat-fee" class="billingtab tab4 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Flat Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Flat Fee Amount</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="late-fee" class="billingtab tab5 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Late Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Late Fee Amount</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_latefee" id="activity_bi_latefee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="credit-account" class="billingtab tab6 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Adjustment/Credit Account</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Credit Account</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_creditac" id="activity_bi_creditac" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="debit-account" class="billingtab tab7 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins" >
                                                            <div class="ibox-title">
                                                                <h5>Adjustment/Debit Account</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Debit Account</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="none" class="billingtab tab8 hidden" >

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center marg-top15">
                                <button type="submit" class="btn btn-md btn-primary"   style="margin-bottom:0px;">Save</button>

                                <button type="button" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-md btn-primary CaseActEmail">Email</button>
                                <button type="button" class="btn btn-md btn-primary CaseActPrint">Print</button>
                                <!--button type="button" class="btn btn-md btn-primary pull-right">Scan</button-->
                            </div>
                            <!--<span><center><h4>Upload file or drag & drop<h4></center></span>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="viewMultipleDocument" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Documents</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content viewAllAtachment">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-btn text-center">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="partiesNameChangeWarning" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <span>If you make changes in the rolodex, it may affect several cases.</span>
                                <ul class="no-left-padding marg-top15">
                                    <li>
                                        Click Yes <span class="marg-left35">To make changes for this case only.</span>
                                    </li>
                                    <li>
                                        Click No <span class="marg-left35">To make changes in the rolodex which may affect several cases.</span>
                                    </li>
                                    <li>
                                        Click Cancel <span class="marg-left15">To cancel and not make changes at all.</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <?php $caseNo = $this->uri->segment(3); ?>
                        <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo; ?>">
                        <input id="caseCardCode" type="hidden">
                        <button id="change_name_warning" type="button" class="btn btn-primary">Yes</button>
                        <button id="change_name_warning_no" type="button" class="btn btn-primary">No</button>
                        <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="caseSpecific" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Party Specific Information</h4>
            </div>
            <form method="post" name="parties_case_specific_data" id="parties_case_specific_data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins">
                                <div class="">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-5 no-left-padding">Type</label>
                                            <div class="col-sm-7 no-padding">

                                                <select class="form-control" name="parties_case_specific" id="parties_case_specific">
                                                    <?php
                                                    if (isset($types)) {
                                                        foreach ($types as $key => $val) {
                                                            ?>
                                                            <option value="<?php echo $val->type; ?>"><?php echo $val->type; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div></div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label class="col-sm-5 no-left-padding">Side</label>
                                            <div class="col-sm-7 no-padding marg-bot5">
                                                <select class="form-control" name="parites_side" id="parties_side">
                                                    <?php
                                                    if (isset($side)) {
                                                        foreach ($side as $key => $val) {
                                                            ?>
                                                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div></div>
                                        <div class="form-group">
                                            <label class="col-sm-5 no-left-padding">Their Office Number</label>
                                            <div class="col-sm-7 no-padding marg-bot5">
                                                <input type="text" name="parties_office_number" id="parties_office_number" class="form-control">
                                            </div></div>

                                        <div class="form-group">
                                            <label class="">Show the name on the Party Sheet
                                                <input type="checkbox" value="1" name="parties_party_sheet" id="parties_party_sheet" class="i-checks">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="">Party Notes</label>
                                        <textarea class="form-control" rows="9" id="parties_notes" name="parties_notes"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <div class="">
                            <?php $caseNo = $this->uri->segment(3); ?>
                            <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo; ?>">
                            <input id="partiesCardCode" name="partiesCardCode" type="hidden" value="">
                            <button id="save_case_specific" type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="addRelatedCase" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Related Case</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <form id="addRelatedcaseForm" name="addRelatedcaseForm" >
                    <div class="float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group ui-widget">
                                        <label>Enter the case number to Relate this case</label>
                                        <input id="relatedcasenumber" name="relatedcasenumber" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary" id="addRelatedcase" >Process</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="venulist_model" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Venue List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <form>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive party-table1">
                                    <table class="table table-bordered table-hover table-striped" id="venue_table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Firm</th>
                                                <th>Venue</th>
                                                <th>city</th>
                                                <th >Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($case_cards) && $case_cards != '') {
                                                foreach ($case_cards as $key => $details) {
                                                    $name = $details->first . " " . $details->middle . " " . $details->last;
                                                    echo '<tr id="' . $details->cardcode . '">' .
                                                    '<td class="card_details" title="' . ($name) . '">' . $name . '</td>' .
                                                    '<td>' . $details->firm . '</td>' .
                                                    '<td>' . $details->venue . '</td>' .
                                                    '<td>' . $details->city . '</td>' .
                                                    '<td>' . $details->address1 . '</td>' .
                                                    '</tr>';
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="4">Data not found.</td>
                                                    <td style="display: none;"></td>
                                                    <td style="display: none;"></td>
                                                    <td style="display: none;"></td>
                                                </tr>
                                            <?php }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="set_venue" >Attach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="firmcompanylist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md firmcompanylist">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Firm Company List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="form-group">
                    <form action="#" role="form" method="post" id="companylistForm" name="companylistForm">
                        <label>Enter Firm or Company Name</label>
                        <input type="text" class="form-control" name="rolodex_search_text_name" id="rolodex_search_text_name"/>
                        <input type="hidden" class="form-control" name="rolodex_search_parties_name" id="rolodex_search_parties_name" value="rolodex-company" />
                    </form>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped dataTables-example" id="rolodexsearchResult_data">
                                    <thead>
                                        <tr>
                                            <th>Firm/Company</th>
                                            <th>City</th>
                                            <th>Address</th>
                                            <th>Address2</th>
                                            <th>Phone</th>
                                            <th>Name</th>
                                            <th>State</th>
                                            <th>Zip</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="viewSearchedPartieDetails" >Attach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="eventnotelist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select a case activity note</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="filterbottom">
                                    <div class="col-sm-6">
                                        <span><label><input type="checkbox" name="activity_list" id="activity_list" class="i-checks category_event_list"> Show All </label></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <select id="case_category_event" name="case_category_event" class="form-control select2Class category_event_list">
                                            <?php foreach ($caseactcategory as $k => $v) { ?>
                                                <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive rolodexsearch">
                                                <table class="table table-bordered table-hover table-striped dataTables-example" id="caseEventlist" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <button type="button" class="btn btn-md btn-primary" id="caseactivityevent" >Process</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


<div id="CaseEmailModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Write mail: Unattached Documents</h4>
            </div>
            <div class="modal-body">
                <div class="ibox-content marg-bot15">
                    <form class="form-horizontal" id="send_mail_form1"  name="send_mail_form1" method="post" enctype="multipart/form-data">
                        <div class="form-group"><label class="col-sm-2 control-label">Internal User:</label>
                            <div class="col-sm-10">
                                <select name="whoto1" id="whoto1" style="width:100%" multiple  placeholder="To" class="form-control select2Class">
                                    <?php
                                    if (isset($staff_list)) {
                                        foreach ($staff_list as $staff) {
                                            ?>
                                            <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">External Parties:</label>
                            <div class="col-sm-10">
                                <input type="text" name="emails1" id="emails1" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Subject :</label>
                            <div class="col-sm-10">
                                <input type="text" name="mail_subject1" id="mail_subject1" class="form-control">
                                <input type="hidden" name="filenameVal1" id="filenameVal1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group inline-block marg-right40 col-sm-3 m-t-xs" align="center">
                                <label style="margin-left:15px !important;">Urgent</label>
<!--                                <input type="checkbox" class="js-switch" class="margin-right10" sty name="mail_urgent1" id="mail_urgent1"  />-->
                                 <input type="checkbox" name="mail_urgent1" id="mail_urgent1" value="Y" class="i-checks">
                            </div>
                            <div class="form-inline col-sm-5" align="center">
                                <label class="marg-right10">Case No:</label>
                                <input type="text" name='caseno1' id="case_no1" class="form-control" readonly value="<?php echo $this->uri->segment(3); ?>">
                            </div>
                            <div class="form-inline col-sm-4 pull-right">
                                <select id="case_category1" name="case_category1" class="form-control select2Class">
                                    <?php foreach ($caseactcategory as $k => $v) { ?>
                                        <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class=" form-group mail-text h-200" style="padding-right:10px; padding-left:10px;">
                            <label>Mail Content <span class="asterisk">*</span></label>
                            <textarea id="mailbody1"  name="mailbody1"></textarea>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group text-right">
                            <div class="col-sm-12">
                                <!-- <label class="col-sm-2 control-label">Attachment :</label> -->
                                <div id="selectedloader1"></div>
                                    <div id="selectedFiles1"></div>
                                    <div class="attatchmentset" style="display: inline-block !important;width: 100% !important;overflow: hidden !important;overflow-y: auto !important;">
                                        <div class="col-sm-12 att-add-file marg-top5"> <input type="file" name="afiles2[]" id="afiles2" multiple > <!-- <i class="fa fa-plus-circle marg-top5 ca-addfile" aria-hidden="true"></i> --></div>
                                    </div>
                            </div>
                            <div class="col-sm-12 m-t-sm">
                                <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send"><i class="fa fa-reply"></i> Send</button>
                                <a href="javascript:;" class="btn btn-danger btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                            </div>
                        </div>
                        <div class="col-sm-12 supported-files"><b>Note: </b> File size upload upto 20 MB <b>Supported Files :</b> PNG, JPG, JPEG, BMP,TXT, ZIP,RAR,DOC,DOCX,PDF,XLS,XLSX,PPT,PTTX,MP4 </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="mailType" id="mailType">
                        <input type="hidden" name="totalFileSize" id="totalFileSize" value="0">
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>

<div id="copyemail">
    <textarea id="copytoemail" STYLE="display:none;">
    </textarea>
</div>
<div id="eamslist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md eamsreflist">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EAMS List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-eamsLookup" >
                                <thead>
                                    <tr>
                                        <th>Eamsref</th>
                                        <th>Name</th>
                                        <th>Add1</th>
                                        <th>Add2</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Zip</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="eamsrefDetails" >Attach</button>
                    <button type="button" class="btn btn-md btn-primary" id="eamsrefDetach" >Detach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
$url_segment = $this->uri->segment(2);
if ($url_segment == 'case_details_new') {
?>
<input type="hidden" name="casemodule" id="casemodule" value="case_details_new">
<?php }else if($url_segment == 'case_details'){ ?>
<input type="hidden" name="casemodule" id="casemodule" value="case_details">
<?php } ?>
<script type="text/javascript" src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>
<?php include('js/casecardjs.php'); ?>
<script type="text/javascript">
            var notesLocalStorageTimer = '';
            var uri_seg = '<?php echo (isset($url_seg)) ? $url_seg : ''; ?>';
            var myDropzone = "";
            var selectedArray = [];
            var rolodexSearchDatatable = '';
            var rolodexSearch = '';
            var categoryeventlist = '';
            var venue_table_data = '';
            var caseactTable = '';
            var partytable = "";
            var swalFunction = '';
            var CaseHID = $("#hdnCaseId").val();
            var ProspectListDatatable;
            var searchBy1 = {'prosno':<?php echo isset($record[0]->proskey) ? $record[0]->proskey : 'null'; ?>};
            let send_mail_form1_validate = '';
            var eamsData = '';
            $(function () {
                setValidation();
                $("#relatedcasenumber,.caseno").autocomplete({
                    source: '<?php echo base_url(); ?>cases/autosearch/' + CaseHID
                });
            });
            $(function() {
                $('#afiles2').bind("change", function() {
                    var fileSize = parseInt($('#totalFileSize').val());
                    var formData = new FormData();
                    var fileName = document.getElementById('afiles2').files;
                    var fi = document.getElementById('afiles2');
                    var fsize = '';
                    for (var i = 0, len = document.getElementById('afiles2').files.length; i < len; i++) {
                        var fsize = fi.files.item(i).size / 1024;
                        fileSize = fileSize + fsize;
                        
                        if(fsize < 20001)
                        {
                            formData.append("file" + i, document.getElementById('afiles2').files[i]);
                        }else
                        {
                            var filenameerror = fi.files.item(i).name;
                            swal({
                                    title: "Alert",
                                    text: "Your " + filenameerror + " file size " + Math.round(fsize/1024) + " MB. Please upload file size up to 20 MB",
                                    type: "warning"
                                });
                            var input = $("#afiles2");
                            var fileName = input.val();
                            if(fileName) {
                                input.val('');
                            }
                        }
                    
                    }
                    if(fileSize >= 20001) {
                        swal({
                            title: "Alert",
                            text: "Total size of all files including the current file which you have selected is " + Math.round(fileSize/1024) + " MB. Please upload files which are in total of size up to 20 MB",
                            type: "warning"
                        });
                        $("#afiles2").val('');
                        fileSize = fileSize - fsize;
                        return false;
                    }
                    $('#totalFileSize').val(fileSize);
                    $.ajax({
                        url: "../../email/emailattaupload",
                        type: 'post',
                        data: formData,
                        dataType: 'html',
                        async: true,
                        processData: false,
                        contentType: false,
                        beforeSend: function () { $('#selectedloader1').addClass('loader');},
                        complete: function () {
                        $('#selectedloader1').removeClass('loader');
                        },
                        success : function(data) {
                            var input = $('input[name="afiles2[]"]');
                            console.log(input);
                                var fileName = input.val();
                                console.log(fileName);
                
                            if(fileName) { 
                                input.val('');
                            }
                            $('#selectedFiles1').append(data);
                                
                        },
                        error : function(request) {
                            console.log(request.responseText);
                        }
                    });
                });
            });
            $(document).ready(function () {
                $('#caseSpecific').on('hidden.bs.modal', function() {
                    $('#parties_case_specific_data #parties_case_specific').val($('#parties_case_specific_data #parties_case_specific option:first').val());
                });

                eamsData = $('.dataTables-eamsLookup').DataTable({
                    "processing": true,
                    "serverSide": true,
                    stateSave: true,
                    stateSaveParams: function (settings, data) {
                        data.search.search = "";
                        data.start = 0;
                        delete data.order;
                    },
                    "bFilter": true,
                    "sScrollX": "100%",
                    "sScrollXInner": "150%",
                    "ajax": {
                        url: "<?php echo base_url(); ?>eams_lookup/getEamsAjaxData",
                        type: "POST"
                    }
                });
                rolodexSearchDatatable = $('#rolodexPartiesResult').DataTable({
                    lengthMenu: [5, 10, 25, 50, 100],
                    scrollX: true,
                    stateSave: true,
                    stateSaveParams: function (settings, data) {
                        data.search.search = "";
                        data.start = 0;
                        delete data.order;
                    },
                    scrollY: "300px",
                    scrollCollapse: true,
                    processing: true,
                    "columns": [
                        {width: '20% !important', targets: 0},
                        {width: '15% !important', targets: 1},
                        {width: '15% !important', targets: 2},
                        {width: '20% !important', targets: 3},
                        {width: '20% !important', targets: 4},
                        {width: '10% !important', targets: 5}
                    ],
                    fixedColumns: true
                });
                $("#uploadBtn3").change(function () {
                    if ($(this).val() != "") {
                        var docFileData = $(this).prop('files')[0];
                        $("#uploadFile3").html(docFileData.name);
                    } else {
                        $("#uploadFile3").html("Upload File");
                    }
                });

                ProspectListDatatable = $('#ProspectListDatatable').DataTable({
                    scrollX: true,
                    stateSave: true,
                    stateSaveParams: function (settings, data) {
                        data.search.search = "";
                        data.start = 0;
                        delete data.order;
                    },
                });
                $('input.numberinput').bind('keypress', function (e) {
                    return !(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46);
                });
                var calYearRange = $('#calYearRange').val();
                venue_table_data = $('#venue_table').DataTable();
                categoryeventlist = $('#caseEventlist').DataTable();
                rolodaxdataTable = $('.dataTables-rolodex').DataTable();
                $('#print_datepicker').hide();
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'
                });
                $('#taskdatepicker2, #taskdatepicker, #datepicker, .datepicker, #caldate, .rdate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                $('.dob').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: '+0d'
                });
                var d = new Date();
                var hours = d.getHours();
                var minutes = d.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                var currenttime = d;

                if(d.getHours() > 12) {
                    d.setHours(d.getHours() - 12);
                }
                if(d.getHours() < 10) {
                    hours = '0' + d.getHours();
                } else {
                    hours = d.getHours();
                }
                if(d.getMinutes() < 10) {
                    minutes = '0' + d.getMinutes();
                } else {
                    minutes = d.getMinutes();
                }
                var currenttime = hours+":"+minutes+" "+ampm;
                $('#caltime').val(currenttime);
                var input1 = $('#caltime'); 
                $('#oletime').val(currenttime);
                $('#tickledate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date()
                });
                $('.preinjurydate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                $('#start_printData').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true
                }).on('changeDate', function (ev) {
                    $('#end_printData').datetimepicker('setStartDate', ev.date);
                });
                $('#end_printData').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true
                }).on('changeDate', function (ev) {
                    $('#start_printData').datetimepicker('setEndDate', ev.date);
                });
                $('select[name=print_type]').on('change', function () {
                    if (this.value == 4 || this.value == 5) {
                        $('#print_datepicker').show();
                    } else {
                        $('#start_printData').val('').datetimepicker("update");
                        $('#end_printData').val('').datetimepicker("update");
                    }
                });
                $('#casedate_followup').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date()
                });

                $.fn.modal.Constructor.prototype.enforceFocus = function () {};
                $('.contact').mask("(000) 000-0000");
                $('.social_sec').mask("000-00-0000");
                $('#addcontact.modal').on('show.bs.modal', function () {
                    document.getElementById('imgerror').innerHTML = '';
                    document.getElementById('hiddenimg').value = '';
                    $(".chosen-language").chosen();
                });
                $('#addRolodexForm a[data-toggle="tab"]').on('click', function (e) {

                    setValidation();
                    e.preventDefault();
                });
                $('#addcontact.modal').on('hidden.bs.modal', function () {
                    $('#addcontact.modal-title').text('Add New Contact');
                    $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                        this.value = '';
                    });
                    document.getElementById('profilepicDisp').style.display = 'none';
                    document.getElementById('dispProfile').src = '';
                    $('#addcontact a[href="#name"]').tab('show');
                    var addContactValidator = setValidation();
                    addContactValidator.resetForm();
                });

                $('#addContact').on('click', function () {
                    var cardcodeVal = $('input[name="card_cardcode"]').val();
                    if (cardcodeVal == '') {
                        if (!$('#addRolodexForm').valid()) {
                            return false;
                        }
                    }
                    if (!$('#addRolodexForm').valid()) {
                        return false;
                    }
                    var myImg = $('#profilepic').prop('files')[0];
                    if(myImg){
                        var sizeKB = myImg.size / 1024; 
                        if(sizeKB > 5000)
                        {
                            swal("Failure !", "The file size exceeds the limit allowed.", "error");
                            return false;
                        }
                    }
                    $.blockUI();
                    var url = '<?php echo base_url() . "rolodex/addCardData" ?>';
                    if ($('input[name="card_cardcode"]').val() != '') {
                        url = '<?php echo base_url() . "rolodex/editCardData" ?>';
                    }
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: $('#addRolodexForm').serialize(),
                        success: function (result) {
                            var data = $.parseJSON(result);
                            var editCardcode = data.editcardid;
                            
                            if ($('#profilepic').val() != '')
                            {
                                var old_img = document.getElementById('hiddenimg').value;
                                var images = '';
                                var file_data = $('#profilepic').prop('files')[0];
                                var form_data = new FormData();
                                form_data.append('file', file_data);
                                $.ajax({
                                    url: '<?php echo base_url(); ?>rolodex/updateprofilepic/' + editCardcode + '/' + old_img,
                                    dataType: 'text',
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    data: form_data,
                                    type: 'post',
                                    success: function (response) {
                                        if (response)
                                        {
                                            images = response.trim();
                                            var plink = '<?php echo base_url() ?>assets/rolodexprofileimages/' + images;

                                            setTimeout(function ()
                                            {
                                                if (images != null) {
                                                    if ($('#applicantImage').length == 0) {
                                                        $('.client-img').find('img.img-circle').attr("src", plink);
                                                    } else
                                                    {
                                                        $('#applicantImage').remove();
                                                        $('.client-img').append('<center><img class="img-circle" height="67px" src="' + plink + '" /></center>');
                                                    }
                                                } else {
                                                }
                                            }, 500);
                                        }
                                    },
                                    error: function (response) {
                                    }
                                });
                            }
                            $.unblockUI();
                            if (data.status == '1' || data.status == 1) {
                                $('#addRolodexForm')[0].reset();
                                $('#addcontact').modal('hide');
                                if (partytable != "") {
                                    partytable.ajax.reload(null, false);
                                }

                                $('#addcontact .modal-title').html('Add New Contact');
                                if ((data.caseid != 0 || data.caseid != 'undefined')) {
                                    var caseid = $("#hdnCaseId").val();
                                    if (data.caseid != 0 && $("#new_case").val() == 1) {
                                        var url = "<?php echo base_url() ?>cases/case_details/" + data.caseid;
                                        setTimeout(function ()
                                        {
                                            window.location.href = url;
                                        }, 3000);
                                    }
                                    if (cardcodeVal != '' || $("#new_case").val() == 'parties') {
                                        $.ajax({
                                            url: '<?php echo base_url() . "cases/case_details" ?>',
                                            method: "POST",
                                            data: {'caseId': $("#hdnCaseId").val()},
                                            success: function (result) {
                                                var res = $.parseJSON(result);

                                                setTimeout(function () {
                                                    if (res.display_details.firm)
                                                    {
                                                        $(".applicant_name").html("<b>" + res.display_details.firm + "</b>");
                                                    } else {
                                                        $(".applicant_name").html("<b>" + res.display_details.salutation + " " + res.display_details.first + " " + res.display_details.last + "</b>");
                                                        $('.handling_attr').addClass('hidden');
                                                    }
                                                    if (res.display_details.popular)
                                                    {
                                                            
                                                            if ((res.display_details.popular).length < 10) {
                                                            if($('.popular').length > 0)
                                                                var htm = '<span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular + '</b></span>';       
                                                            else
                                                                var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular + '</b></span>'
                                                        } else
                                                        {
                                                            if($('.popular').length > 0)
                                                                var htm = '<span class="f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular.substr(1, 10) + '..</b></span>';
                                                            else
                                                                var htm = '<br/><span class="f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular.substr(1, 10) + '..</b></span>';
                                                        }
                                                        if ($('.popular').length > 0) {
                                                            $(".popular").replaceWith(htm);
                                                        } else
                                                        {
                                                            $(".applicant_name").after(htm);
                                                        }
                                                    }
                                                    else{
                                                        $(".popular").html('');
                                                    }
                                                    if (res.display_details.address2)
                                                        $(".applicant_address").html(res.display_details.address1 + ',' + res.display_details.address2);
                                                    else
                                                        $(".applicant_address").html(res.display_details.address1);
                                                        if(res.display_details.city || res.display_details.state || res.display_details.zip)                                                                                                
                                                        $(".applicant_address2").html(res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);
                                                    if (res.display_details.address1) {
                                                        $('.viewonmap').removeClass('hidden');
                                                    } else
                                                    {
                                                        $('.viewonmap').addClass('hidden');
                                                    }
                                                    $('#address_map').attr('href', "https://maps.google.com/?q=" + res.display_details.address1 + '' + res.display_details.address2 + " " + res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);
                                                    if (res.display_details.home)
                                                        $(".home").html(res.display_details.home);
                                                    else
                                                        $(".home").html('NA');
                                                        /*if (res.display_details.business)
                                                            $(".business").html(res.display_details.business);
                                                        else
                                                            $(".business").html('NA');*/
                                                    if (res.display_details.car)
                                                            $(".business").html('(C)' +res.display_details.car);
                                                    else if(res.display_details.phone1)
                                                            $(".business").html('(B)' +res.display_details.phone1);
                                                    else if(res.display_details.business)
                                                            $(".business").html('(B)' +res.display_details.business);
                                                    if (res.display_details.email)
                                                        $(".email").html(res.display_details.email);
                                                    else
                                                        $(".email").html('NA');
                                                    var dateString = res.display_details.birth_date;
                                                    var dob = new Date(dateString.replace(' ', 'T'));
                                                    var today = new Date();
                                                    var age = calcAge(dob, today);
                                                    if (res.display_details.birth_date) {
                                                        $(".birth_date").html("");
                                                        $(".birth_date").html('Born on ' + moment(res.display_details.birth_date).format('MM/DD/YYYY') + ', Age: ' + age);
                                                    }
                                                    else
                                                        $(".birth_date").html('NA');
                                                    if (res.display_details.social_sec)
                                                        $(".ssNo").html(res.display_details.social_sec);
                                                    else
                                                        $(".ssNo").html('NA');

                                                    $("#party_name").html(res.display_details.first + " " + res.display_details.last);
                                                    if(res.display_details.interpret != '') {
                                                        interpret = res.display_details.interpret;
                                                    } else {
                                                        interpret = 'NA';
                                                    }
                                                    if(res.display_details.language != '') {
                                                        language = res.display_details.language;
                                                    } else {
                                                        language = 'NA';
                                                    }
                                                    $("#interp_text").text("");
                                                    $('#interp_text').text(interpret+' '+language);
                                                    $("#firm_name").html(res.display_details.firm);
                                                    $("#party_beeper").val(res.display_details.beeper);
                                                    var p_tr_id = $('table#party-table-data').find('.table-selected');
                                                    $('table#party-table-data').find('.table-selected .view_party').trigger("click");
                                                    if (res.RefName)
                                                    {
                                                        if (res.display_details.rb) {
                                                            $(".rb").html('<a class="edit-card" data-cardcode="' + res.display_details.rb + '">' + res.RefName + '</a>');
                                                        } else {
                                                            $(".rb").html(res.RefName);
                                                        }
                                                    }
                                                    if (res.display_details.Displayvenue)
                                                    {
                                                        if (res.display_details.Displayvenue) {
                                                            $("#locationDisplay").html(res.display_details.Displayvenue);
                                                        } else {
                                                            $("#locationDisplay").html(res.display_details.Displayvenue);
                                                        }
                                                    }
                                                    if (res.display_details.Displayvenue)
                                                    {
                                                        $(".venue").html(res.display_details.Displayvenue);
                                                    }
                                                    p_tr_id = p_tr_id.attr('id');
                                                    $("#" + p_tr_id).find(".parties_name").html(res.display_details.first + ", " + res.display_details.last);
                                                }, 1000);
                                            }
                                        });
                                    }
                                }
                                rolodaxdataTable.draw(false);
                            } else {
                                swal("Oops...", data.message, "error");
                            }
                        }
                    });
                    return false;
                });
                $('#add_new_case_activity').validate({
                    ignore: [],
                    rules: {
                        activity_event: {
                            required: true
                        }
                    },
                    messages: {
                        activity_event: {required: "Event Description is Required"}
                    },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');
                        $('#addcaseact a[href="#' + tab + '"]').tab('show');
                    }
                });
                $.validator.addMethod("phoneUSA", function (phone_number, element) {
                    phone_number = phone_number.replace(/\s+/g, "");
                    return this.optional(element) || phone_number.length > 9 &&
                            phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
                }, "Please specify a valid phone number");
                $("#prospectForm").validate({
                    ignore: [],
                    rules: {
                        pro_first: {
                            required: true
                        },
                        pro_last: {
                            required: true
                        },
                        pro_type: {
                            required: true
                        },
                        pro_business: {phoneUSA: true},
                        pro_home: {phoneUSA: true},
                        pro_cell: {phoneUSA: true},
                        pro_social_sec: {ssId: true},
                        pro_state: {stateUS: true},
                        pro_zip: {zipcodeUS: true},
                        pro_caseno: {
                            remote: {
                                url: '<?php echo base_url(); ?>prospects/CheckCaseno',
                                type: "post",
                                data: {
                                    pro_caseno: function () {
                                        return $('#prospectForm :input[name="pro_caseno"]').val();
                                    }
                                }
                            },
                            digits: true
                        }
                    },
                    messages: {
                        pro_first: {
                            required: "Please provide first name"
                        },
                        pro_last: {
                            required: "Please provide last name"
                        },
                        pro_type: {
                            required: "Please select Category"
                        },
                        pro_caseno: {
                            remote: "This caseno is not exist"
                        },
                        pro_state: {stateUS: "Invalid USA state abbreviation"}
                    },
                    submitHandler: function (form) {
                        return false;
                    },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');
                        $('#proceedprospects a[href="#' + tab + '"]').tab('show');
                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "pro_type") {
                            error.insertAfter(element.next());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $('.dtpickDisDayofWeek').datetimepicker({
                    daysOfWeekDisabled: [0],
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date()
                });
                $('.datepickerClass').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true
                });
                $('#casedate_closed').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                $('#activity_entry_type').change(function () {
                    var data_key = $('#activity_entry_type option:selected').data('key');
                    $(".billingtab").addClass("hidden");
                    $("#" + data_key).removeClass("hidden");
                });
                var data_key = $('#activity_entry_type option:selected').data('key');
                $(".billingtab").addClass("hidden");
                $("#" + data_key).removeClass("hidden");
                Dropzone.options.addNewCaseActivity = {
                    autoProcessQueue: false,
                    uploadMultiple: true,
                    parallelUploads: 11,
                    maxFiles: 11,
                    addRemoveLinks: true,
                    uploadprogress: true,
                    enqueueForUpload: false,
                    paramName: 'document',
                    removedfile: function(file) {
                        var _ref;
                        /*_ref = file.previewElement;*/
                        _ref = file.name;
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/remove_clicked_file',
                            datatype: 'json',
                            method: 'post',
                            data: {
                                caseNo: $('#caseNo').val(),
                                fileName: _ref
                            },
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function(result) {
                                if(result == 2) {
                                    swal("Failure !", "Remove of file failed.", "error");
                                } else {
                                    file.previewElement.parentNode.removeChild(file.previewElement);
                                }
                            }
                        });
                        /*return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;*/
                    },
                    init: function () {
                        myDropzone = this;
                        this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                            $.blockUI();
                            $("#caseActNo").val($("#hdnCaseId").val());
                            e.preventDefault();
                            e.stopPropagation();
                            var form = $(this).closest('#add_new_case_activity');
                            if (form.valid() == true) {
                                if (myDropzone.getQueuedFiles().length > 0) {
                                    myDropzone.processQueue();
                                } else {
                                    myDropzone.uploadFiles([]);
                                }
                            } else {
                                $.unblockUI();
                            }
                        });
                        this.on("maxfilesreached", function () {
                            swal("Failure !", "The file size exceeds the limit allowed.", "error");
                            if (this.files[1] != null) {
                                this.removeFile(this.files[0]);
                            }
                        });
                        this.on("addedfile", function (file) {
                            if (this.files.length) {
                                var _i, _len;
                                for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
                                    if (this.files[_i].name === file.name) {
                                        /*this.removeFile(file);*/
                                        /*$( ".dz-preview" ).remove();*/
                                    }
                                }
                            }
                        }),
                                this.on("success", function (file, responseText) {
                                    console.log(file);
                                });
                        this.on("sendingmultiple", function (data, xhr, formData) {
                            if ($('#uploadBtn3').prop('files').length > 0) {
                                var caseactUploaddocData = $('#uploadBtn3').prop('files')[0];
                                formData.append("caseact_uploaddoc", caseactUploaddocData);
                            }
                        });
                        this.on("successmultiple", function (file, responseText) {
                            $.unblockUI();
                            var html = '';
                            var view = '';
                            $("#addcaseact").modal("hide");
                            $("#activity_mainnotes").iCheck('uncheck');
                            $('input[name=activity_no]').val('');
                            var data = JSON.parse(responseText);
                            var link = '<?php echo base_url() ?>' + 'assets/clients/' + $('#caseNo').val() + '/' + data.file_name;
                            if (file.length == 0) {
                                if ((file.length + data.fileExist) > 1) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0)"><i class="icon fa fa-eye"></i> View All</a>';
                                } else if ((file.length + data.fileExist) > 0) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_attach" download=' + data.file_name + ' data-actid=' + data.actno + ' href="' + link + '"><i class="icon fa fa-eye"></i> View</a>';
                                } else {
                                    view = '';
                                }
                            } else {
                                if (data.fileExist > 1) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0)"><i class="icon fa fa-eye"></i> View All</a>';
                                } else if (data.fileExist > 0) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_attach" download=' + data.file_name + ' data-actid=' + data.actno + ' href="' + link + '"><i class="icon fa fa-eye"></i> View</a>';
                                } else {
                                    view = '';
                                }
                            }
                            html += "<tr id=caseAct" + data.actno + "><td>" + data.actData.add_case_cal + "</td>";
                            html += "<td>" + data.actData.add_case_time + "</td>";
                            html += "<td>" + data.actData.activity_event + "</td>";
                            html += "<td>" + data.actData.last_ini + "</td>";
                            html += "<td>" + data.actData.orignal_ini + "</td>";
                            html += "<td>" + view + "</td>";
                            html += "<td>";
                            html += "<a class='btn btn-xs btn-info marginClass activity_edit' data-actid=" + data.actno + " href='javascript:' title='Edit'><i class='icon fa fa-edit'></i> Edit</a>";
                            html += "<a class='btn btn-xs btn-danger marginClass activity_delete' data-actid=" + data.actno + " href='javascript:' title='Delete'><i class='icon fa fa-times'></i> Delete</a>";
                            html += "</td>";
                            html += "</tr>";
                            swal({
                                title: "Success!",
                                text: "Activity List is successfully updated!",
                                type: "success"
                            });
                            if (data.status == 2) {
                                if (caseactTable.rows('[id=caseAct' + data.actno + ']').any()) {
                                    caseactTable.rows($('tr#caseAct' + data.actno)).remove();
                                }
                                swal({
                                    title: "Success!",
                                    text: "Activity is successfully updated!",
                                    type: "success"
                                }, function () {
                                    caseactTable.row.add($(html));
                                    caseactTable.draw();
                                });
                            } else if (data.status == 1) {
                                caseactTable.row.add($(html));
                                caseactTable.draw();
                            } else {
                                swal("Failure !", "Can't add the Activity! Try again", "error");
                            }
                            document.getElementById("add_new_case_activity").reset();
                        });
                        this.on("errormultiple", function (files, response) {
                            console.log(response);
                            console.log(files);
                        });
                        this.on("completemultiple", function (file) {
                            /*this.removeAllFiles(true);*/
                            $( ".dz-preview" ).remove();
                        });
                    }
                };

                rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                    "scrollX": true,
                    stateSave: true,
                    stateSaveParams: function (settings, data) {
                        data.search.search = "";
                        data.start = 0;
                        delete data.order;
                    },
                    "sScrollX": "100%",
                    "sScrollXInner": "150%",
                    "lengthMenu": [5, 10, 25, 50, 100]
                });
                duplicateDatatable = $('#duplicateEntries').DataTable();
                $("#rolodex_search_parties_form").validate({
                    rules: {
                        rolodex_search_text: {
                            required: true
                        },
                        rolodex_search_parties: {
                            required: true
                        }
                    },
                    messages: {
                        rolodex_search_text: {
                            required: "Please provide search text"
                        },
                        rolodex_search_parties: {
                            required: "Please provide search by"
                        }
                    },
                    submitHandler: function (form, event) {
                        event.preventDefault();
                        var alldata = $("form#rolodex_search_parties_form").serialize();
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/searchForRolodex1',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                                var text = $("#rolodex_search_text").val();
                                var substring = text.substr(0, 1);
                                var radioVal = $("input[name='rolodex_search_parties']:checked").val();
                                if (substring == "<") {
                                    var res = $.parseJSON(data);
                                    $("#searched_party_fullname").val(res[0].fullname);
                                    $("#searched_party_firmname").val(res[0].firm);
                                    $('#searched_party_type').html(res[0].type);
                                    $('#searched_party_cardnumber').html(res[0].cardcode);
                                    $(".addSearchedParties").trigger("click");
                                }
                                rolodexSearchDatatable.clear();
                                var obj = $.parseJSON(data);
                                $.each(obj, function (index, item) {
                                    var html = '';
                                    html += "<tr class='viewPartiesDetail' data-cardcode=" + item.cardcode + ">";
                                    html += "<td>" + item.name + "</td>";
                                    html += "<td>" + item.firm + "</td>";
                                    html += "<td>" + item.type + "</td>";
                                    html += "<td>" + item.city + "</td>";
                                    html += "<td>" + item.address + "</td>";
                                    html += "<td>" + item.zip + "</td>";
                                    html += "</tr>";
                                    rolodexSearchDatatable.row.add($(html));
                                });
                                rolodexSearchDatatable.draw();
                            }
                        });
                    }
                });
                $("#companylistForm").validate({
                    rules: {
                        rolodex_search_text_name: {
                            required: true,
                            noBlankSpace: true
                        }
                    },
                    messages: {
                        rolodex_search_text_name: {
                            required: "Please enter Firm/Company name "
                        }
                    },
                    submitHandler: function (form, event) {
                        event.preventDefault();
                    }
                });
                $("#addNewCase").validate({
                    rules: {
                        first_name: {
                            required: true,
                            noBlankSpace: true
                        },
                        last_name: {
                            required: true,
                            noBlankSpace: true
                        }
                    },
                    messages: {
                        first_name: {
                            required: "Please provide first name"
                        },
                        last_name: {
                            required: "Please provide last name"
                        }
                    },
                    submitHandler: function (form, event) {
                        setValidation();
                        event.preventDefault();
                        $("#caseNo").val($("#hdnCaseId").val());
                        var alldata = $("form#addNewCase").serialize();
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/checkDuplicateCase',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (result) {
                                var obj = $.parseJSON(result);
                                duplicateDatatable.rows().remove().draw();
                                if (obj.status == '1') {
                                    $("#addcase").modal("hide");
                                    $.each(obj.data, function (index, item) {
                                        var html = '';
                                        html += "<tr class='selectDuplicateData' data-cardcode=" + item.cardcode + ">";
                                        html += "<td>" + item.caseno + "</td>";
                                        html += "<td>" + item.casetype + "</td>";
                                        html += "<td>" + item.casestat + "</td>";
                                        html += "<td>" + item.dateenter + "</td>";
                                        html += "<td>" + item.social_sec + "</td>";
                                        html += "<td>" + item.cardtype + "</td>";
                                        html += "<td>" + item.caption1 + "</td>";
                                        html += "</tr>";
                                        duplicateDatatable.row.add($(html));
                                    });
                                    duplicateDatatable.draw();
                                    if (obj.data.length == 1) {
                                        $("#duplicateEntries tr.selectDuplicateData").addClass("highlight");
                                        $('#edit_new_case').prop('disabled', false);
                                        $('#attach_name').prop('disabled', false);
                                    }
                                    $('input[name=first_name]').val($("#first_name").val());
                                    $('input[name=last_name]').val($("#last_name").val());
                                    $("#duplicateData").modal("show");
                                } else {
                                    if ($("#prospect_id").val() != '')
                                    {
                                        $.ajax({
                                            url: '<?php echo base_url(); ?>prospects/getProspectsdata',
                                            method: "POST",
                                            data: {prospect_id: $("#prospect_id").val()},
                                            beforeSend: function (xhr) {
                                                $.blockUI();
                                            },
                                            complete: function (jqXHR, textStatus) {
                                                $.unblockUI();
                                            },
                                            success: function (result) {

                                                var data = $.parseJSON(result).fadeIn();
                                                $('#card_salutation').val(data.data.salutation);
                                                $('#card_suffix').val(data.data.suffix);
                                                $('#card_type').val(data.data.type);
                                                $('input[name=card_first]').val(data.data.first);
                                                $('input[name=card_last]').val(data.data.last);
                                                $('input[name=card2_address1]').val(data.data.address1);
                                                $('input[name=card2_city]').val(data.data.city);
                                                $('input[name=card2_state]').val(data.data.state);
                                                $('input[name=card2_zip]').val(data.data.zip);
                                                $('input[name=card_business]').val(data.data.business);
                                                $('input[name=card_home]').val(data.data.home);
                                                $('input[name=card_car]').val(data.data.cell);
                                                $('input[name=card_email]').val(data.data.email);
                                                $('input[name=card_social_sec]').val(data.data.social_sec);
                                                var dob = new Date(data.data.birthdate);
                                                var today = new Date();
                                                var age = calcAge(dob, today);
                                                if (data.data.birthdate == 'NA') {
                                                    $('input[name=card_birth_date]').val('');
                                                } else {
                                                    $('input[name=card_birth_date]').val(moment(data.data.birthdate).format('MM/DD/YYYY'));
                                                }
                                                $("#new_case").val(1);
                                                $("#prospect_id").val($("#prospect_id").val());
                                                $("#addcase").modal("hide");
                                                $("#addRolodexForm select").trigger('change');
                                                $('select[name=card_language]').val('English').trigger("change");

                                                $("#addcontact").modal("show");

                                            }
                                        });
                                    } else
                                    {
                                        $('input[name=card_first]').val($("#first_name").val());
                                        $('input[name=card_last]').val($("#last_name").val());
                                        $("#new_case").val(1);
                                        $("#addcase").modal("hide");
                                        $("#addRolodexForm select").trigger('change');
                                        $('select[name=card_language]').val('English').trigger("change");

                                        $("#addcontact").modal("show");
                                    }
                                }
                            }
                        });
                    }
                });
                $('#savecase').on('click', function () {
                    var alldata = $("form#edit_case_details").serialize();
                    var rbName = $("#case_reffered_by option:selected").text();
                    var rbVal = $("#case_reffered_by").val();
                    if (rbName != '')
                    {
                        rbName = '<a class="edit-card" data-cardcode="' + rbVal + '">' + rbName + '</a>';
                    }
                    var nwCaption = $("#editcase_caption").text();
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/editCase',
                        method: 'POST',
                        data: alldata,
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (result) {
                            var data = $.parseJSON(result);
                            $.unblockUI();
                            if (data.status == '1') {
                                swal("Success !", data.message, "success");
                                $('#edit_case_details')[0].reset();
                                $('#editcasedetail').modal('hide');
                                $('#editcasedetail .modal-title').html('Edit Case');
                            } else if (data.status == '0') {
                                swal("Warning !", data.message, "error");
                                $('#edit_case_details')[0].reset();
                                $('#editcasedetail').modal('hide');
                                $('#editcasedetail .modal-title').html('Edit Case');
                            }
                            $.ajax({
                                url: '<?php echo base_url() . "cases/case_details" ?>',
                                method: "POST",
                                data: {'caseId': $("#hdnCaseId").val()},
                                success: function (result) {
                                    var res = $.parseJSON(result);
                                    $(".casetype").html(res.display_details.casetype);
                                    $(".location").html(res.display_details.location);
                                    $(".casestat").html(res.display_details.casestat);
                                    $(".yourfileno").html(res.display_details.yourfileno);
                                    if (res.display_details.Displayvenue) {
                                        $(".venue").html(res.display_details.Displayvenue);
                                    } else {
                                        $(".venue").html('');
                                    }

                                    if (res.display_details.atty_resp != '') {
                                        $(".atty_resp").html(res.display_details.atty_resp);
                                        $("#atty_resp_ini").val(res.display_details.atty_resp);
                                        $(".atty_resp").prev().html(res.display_details.atty_resp.charAt(0));
                                    } else {
                                        $(".atty_resp").html('NA');
                                        $("#atty_resp_ini").val('');
                                        $(".atty_resp").prev().html('&nbsp;');
                                    }

                                    if (res.display_details.para_hand != '') {
                                        $(".para_hand").html(res.display_details.para_hand);
                                        $("#para_hand_ini").val(res.display_details.para_hand);
                                        $(".para_hand").prev().html(res.display_details.para_hand.charAt(0));
                                    } else {
                                        $(".para_hand").html('NA');
                                        $("#para_hand_ini").val('');
                                        $(".para_hand").prev().html('&nbsp;');
                                    }

                                    if (res.display_details.atty_hand != '') {
                                        $(".atty_hand").html(res.display_details.atty_hand);
                                        $("#atty_hand_ini").val(res.display_details.atty_hand);
                                        $(".atty_hand").prev().html(res.display_details.atty_hand.charAt(0));
                                    } else {
                                        $(".atty_hand").html('NA');
                                        $("#atty_hand_ini").val('');
                                        $(".atty_hand").prev().html('&nbsp;');
                                    }

                                    if (res.display_details.sec_hand != '') {
                                        $(".sec_hand").html(res.display_details.sec_hand);
                                        $("#sec_hand_ini").val(res.display_details.sec_hand);
                                        $(".sec_hand").prev().html(res.display_details.sec_hand.charAt(0));
                                    } else {
                                        $(".sec_hand").html('NA');
                                        $("#sec_hand_ini").val('');
                                        $(".sec_hand").prev().html('&nbsp;');
                                    }
                                    if (res.display_details.caption1.length > 55)
                                        $(".caption #caption_details").html(res.display_details.caption1.substring(0, 55) + '...');
                                    else
                                        $(".caption #caption_details").html(res.display_details.caption1);
                                    $(".casesubtype").html(res.display_details.udfall);
                                    if ($('div.sticky.caption').hasClass('caption-closed')) {
                                        $('div.sticky.caption').html(res.display_details.caption1 + ' - ' + 'CLOSED');
                                    }
                                    var dateenter = 'NA';
                                    var dateopen = 'NA';
                                    var followup = 'NA';
                                    var dateclosed = 'NA';
                                    var dr = 'NA';
                                    var psdate = 'NA';
                                    if (res.display_details.dateenter == '1899-12-31 00:00:00' || res.display_details.dateenter == '1899-12-30 00:00:00' || res.display_details.dateenter == '1970-01-01 00:00:00') {
                                        dateenter = 'NA';
                                    } else {
                                        dateenter = moment(res.display_details.dateenter).format('MM/DD/YYYY');
                                    }
                                    if (res.display_details.dateopen == '1899-12-31 00:00:00' || res.display_details.dateopen == '1899-12-30 00:00:00' || res.display_details.dateopen == '1970-01-01 00:00:00') {
                                        dateopen = 'NA';
                                    } else {
                                        dateopen = moment(res.display_details.dateopen).format('MM/DD/YYYY');
                                    }
                                    if (res.display_details.followup == '1899-12-31 00:00:00' || res.display_details.followup == '1899-12-30 00:00:00' || res.display_details.followup == '1970-01-01 00:00:00') {
                                        followup = 'NA';
                                    } else {
                                        followup = moment(res.display_details.followup).format('MM/DD/YYYY');
                                    }
                                    if (res.display_details.dateclosed == '1899-12-31 00:00:00' || res.display_details.dateclosed == '1899-12-30 00:00:00' || res.display_details.dateclosed == '1970-01-01 00:00:00') {
                                        dateclosed = 'NA';
                                    } else {
                                        dateclosed = moment(res.display_details.dateclosed).format('MM/DD/YYYY');
                                    }
                                    if (res.display_details.d_r == '1899-12-31 00:00:00' || res.display_details.d_r == '1899-12-30 00:00:00' || res.display_details.d_r == '1970-01-01 00:00:00') {
                                        dr = 'NA';
                                    } else {
                                        dr = moment(res.display_details.d_r).format('MM/DD/YYYY');
                                    }
                                    if (res.display_details.psdate == '1899-12-31 00:00:00' || res.display_details.psdate == '1899-12-30 00:00:00' || res.display_details.psdate == '1970-01-01 00:00:00' || res.display_details.psdate == '') {
                                        psdate = 'NA';
                                    } else {
                                        psdate = moment(res.display_details.psdate).format('MM/DD/YYYY');
                                    }
                                    $(".dateenter").html(dateenter);
                                    $(".dateopen").html(dateopen);
                                    $(".followup").html(followup);
                                    $(".d_r").html(dr);
                                    if (res.RefName)
                                    {
                                        if (res.display_details.rb) {
                                            $(".rb").html('<a class="edit-card" data-cardcode="' + res.display_details.rb + '">' + res.RefName + '</a>');
                                        } else {
                                            $(".rb").html(res.RefName);
                                        }
                                    }
                                    if (res.message1 != '')
                                    {
                                        $('#div_casemessage1').css('display', 'block');
                                        $('#casemessage1').text(res.message1);
                                    } else {
                                        $('#div_casemessage1').css('display', 'none');
                                    }
                                    if (res.message2 != '')
                                    {
                                        $('#div_casemessage2').css('display', 'block');
                                        $('#casemessage2').text(res.message2);
                                    } else {
                                        $('#div_casemessage2').css('display', 'none');
                                    }
                                    if (res.message3 != '')
                                    {
                                        $('#div_casemessage3').css('display', 'block');
                                        $('#casemessage3').text(res.message3);
                                    } else {
                                        $('#div_casemessage3').css('display', 'none');
                                    }
                                    if (res.message4 != '')
                                    {
                                        $('#div_casemessage4').css('display', 'block');
                                        $('#casemessage4').text(res.message4);
                                    } else {
                                        $('#div_casemessage4').css('display', 'none');
                                    }
                                    $(".ps").html(psdate);
                                    $(".dateclosed").html(dateclosed);
                                    $(".notes").html(res.category_array);
                                }
                            });
                        }
                    });
                });
                $("#parties_case_specific_data").validate({
                    submitHandler: function (form, event) {
                        event.preventDefault();
                        $("#caseNo").val($("#hdnCaseId").val());
                        var alldata = $("form#parties_case_specific_data").serialize();
                        $('#partiesCardCode').val();
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/setPartiesCaseSpecific',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (res) {
                                var data = $.parseJSON(res);
                                if (data.status == '1') {
                                    $.ajax({
                                        url: '<?php echo base_url(); ?>cases/getCaseSpecific',
                                        method: 'POST',
                                        dataType: 'json',
                                        data: {
                                            cardcode: $('#partiesCardCode').val(),
                                            caseno: $('#caseNo').val()
                                        },
                                        beforeSend: function (xhr) {
                                            $.blockUI();
                                        },
                                        complete: function (jqXHR, textStatus) {
                                            $.unblockUI();
                                        },
                                        success: function (data) {
                                            $("#party_type").html(data[0].type);
                                            $("#party_side").html(data[0].side);
                                            $("#party_offc_no").html(data[0].officeno);
                                            $("#party_notes").html(data[0].notes);
                                            $("#card_Type").html(data[0].type);
                                            partytable.draw();
                                        }
                                    });
                                    swal("Success !", data.message, "success");
                                } else {
                                    swal("Alert !", data.message, "warning");
                                }
                                $('#caseSpecific').modal('hide');
                            }
                        });
                    }
                });
                $('#sfl').tooltip();
                send_mail_form1_validate = $('#send_mail_form1').validate({
                    rules: {
                        whoto1: {
                            required: function () {
                                if ($('#emails1').val() == "")
                                    return true;
                                else
                                    return false;
                            }
                        },
                    },
                    messages: {
                        whoto1: {
                            required: 'Please Select a User to send mail to'
                        },
                    },
                    errorPlacement: function (error, element) {
                        if ($(element).attr('name') == 'whoto1') {
                            $(element).parent().append(error);
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    submitHandler: function (form) {
                        var ext_emails = $('input[name="emails1"]').val();
                        let valid_data = true;
                        if (ext_emails != '') {
                            valid_data = validate_ext_email($("input[name=emails1]").val());
                        }
                        if(valid_data == false) {
                            return false;
                        }
                        var whoto = [];
                        if($('select[name="whoto1"]').val()){
                            whoto = $('select[name="whoto1"]').val();
                        }
                        var subject = $('#mail_subject1').val();
                        var urgent = $('input[name="mail_urgent1"]:checked').val();
                        var ext_emails = $('input[name="emails1"]').val();
                        var mailbody = CKEDITOR.instances.mailbody1.getData();
                        var filenameVal = $("#filenameVal").val();
                        var mailType = $("#mailType").val();
                        var case_no = $("#case_no1").val();
                        var case_category1 = $("#case_category1").val();
                        var form_data = new FormData();
                        
                        if (whoto == '' && ext_emails == '') {
                            $('select[name="whoto1"]').parent().append('<span id="wvalid" style="color:red">Please Select a User to send mail to</span>');
                            $('input[name="emails1"]').parent().append('<span style="color:red">Please add external party to send email </span>');
                            setTimeout(function () {
                                $('select[name="whoto1"]').parent().find('#wvalid').remove();
                                $('input[name="emails1"]').parent().find('span').remove();
                            }, 5000);
                            vaild_data = false;
                        }
                        if (ext_emails != '') {
                            vaild_data = validate_ext_email($("input[name=emails1]").val());
                        }
                        if (mailbody == '') {
                            vaild_data = false;
                            $('.mail-text label').after('<span id="content-mail-text" style="color:red">Please Enter Mail Content</span>');
                            setTimeout(function () {
                                $('.mail-text').find('#content-mail-text').remove();
                            }, 5000);
                            return false;
                        }
                        
                        form_data.append("whoto", whoto);
                        form_data.append("subject", subject);
                        form_data.append("urgent", urgent);
                        form_data.append("ext_emails", ext_emails);
                        form_data.append("mailbody", mailbody);
                        form_data.append("mailType", mailType);
                        form_data.append("filenameVal", filenameVal);
                        form_data.append("caseNo", case_no);
                        form_data.append("caseCategory", case_category1);
                        var p = 0;
                        var filelength = $("input[name='afiles1[]']");
                        var fileGlobal = [];
                        var tmp_fileGlobal = $("input[name='files_global_var[]']").map(function(){return $(this).val();}).get();
                        $.each(tmp_fileGlobal, function(k, v) {
                            var array = $.map(JSON.parse(v), function(value, index) {
                                return [ index + ':' + value];
                            });
                            fileGlobal[k] = array;
                        });
                        var files_arry =[];
                        form_data.append("fileGlobalVariable",JSON.stringify(fileGlobal));
                        $.each($('input[name="afiles1[]"]'), function (i, obj) {
                            $.each(obj.files, function (x, file) {
                                vaild_data = validateAttachFileExtension(file.name);
                                form_data.append("files_" + p, file);
                                p = p + 1;
                            });
                            files_arry.push(obj.defaultValue);
                            if(filelength.length == files_arry.length)
                            {
                                form_data.append("files" ,files_arry);
                            }
                        });
                        var chkmodule = $('#casemodule').val();
                        var newcUrl = '<?php echo base_url(); ?>email/send_email';
                        $.ajax({
                            url: newcUrl,
                            method: 'POST',
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                                var rltcnt = $('.related_cnt').html();
                                rltcnt++;
                                $('.related_cnt').html(rltcnt);
                                if (data.result == "true") {
                                    swal({
                                        title: "Success!",
                                        text: "Mail is successfully sent.",
                                        type: "success"
                                    });
                                } else if (data.result == "true" && data.ext_party == '0')
                                {
                                    swal({
                                        title: "Alert",
                                        text: "Oops! Something went wrong. Email wasn't send to External Party.",
                                        type: "warning"
                                    });
                                } else if (data.result == "false" && data.attachment_issue != '')
                                {
                                    swal({
                                        title: "Alert",
                                        text: "Oops! " + data.attachment_issue,
                                        type: "warning"
                                    });
                                    return false;
                                } else {
                                    swal({
                                        title: "Alert",
                                        text: "Oops! Some error has occured. Please try again.",
                                        type: "warning"
                                    });
                                }
                                $('#CaseEmailModal').modal('hide');
                                $('.fileremove').remove();
                                $('input[name="afiles1[]"]').remove();
                                $('#totalFileSize').val('0');
                                $('input[name="files_global_var[]"]').remove();
                                caseactTable.draw();
                            }
                        });
                        return false;
                    }
                });
                var adarr = '<?php echo isset($system_caldata[0]->calattyass) ? $system_caldata[0]->calattyass : '' ?>';
                if (adarr == 3) {
                    $("select[name=attyass]").attr("disabled", true);
                } else {
                    $("select[name=attyass]").removeAttr("disabled");
                }
            });
            $(document.body).on('click', '.activity_edit', function () {
                var id = $(this).data('actid');
                caseActEdit(id);
            });
            function caseActEdit(id) {
                $(".caseActUploadDocument").hide();
                $("#uploadFile3").html("Upload");
                $("#caseact_uploaddocDefault").val();
                $("#caseActUploadDoc").attr("href", "");
                $("#caseActUploadDoc").html("");
                $("#caseActUploadDocSpan").html("");
                var caseNo = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getAjaxActivityDetails',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        actno: id,
                        caseno: caseNo
                    },
                    success: function (data) {
                        $('#add_new_case_activity')[0].reset();
                        if (data == false) {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has been occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        } else {
                            if (data.access == '2') {
                                if (data.initials == '<?php echo $username ?>') {
                                    $('#add_new_case_activity .btn-primary').prop('disabled', false);
                                    $('#addcaseact .modal-title').html('Edit Case Activity');
                                } else {
                                    $('#add_new_case_activity .btn-primary').prop('disabled', true);
                                    $('#addcaseact .modal-title').html('Edit Case Activity <small>(This Activity is Restricted to be change by ' + data.initials0 + ')</small>');
                                }
                            } else {
                                $('#add_new_case_activity .btn-primary').prop('disabled', false);
                                $('#addcaseact .modal-title').html('Edit Case Activity');
                            }
                            /*myDropzone.removeAllFiles(true);*/
                            $( ".dz-preview" ).remove();
                            if (data.document != "") {
                                $.each(data.document, function (index, item) {
                                    var baseUrl = '<?php echo base_url(); ?>' + item;
                                    var mockFile = {name: item, size: 12345, status: 'success'};
                                    myDropzone.emit("addedfile", mockFile);
                                    myDropzone.emit("thumbnail", mockFile, baseUrl);
                                    myDropzone.files.push(mockFile);
                                });
                            }
                            if (typeof (data.ole3) != 'object' && data.ole3 !== "") {
                                var OleDocUrl = '<?php echo base_url(); ?>' + 'assets/clients/' + data.caseno + '/' + data.ole3;
                                $(".caseActUploadDocument").show();
                                $("#uploadFile3").html("Upload New");
                                $("#caseact_uploaddocDefault").val(data.ole3);
                                $("#caseActUploadDoc").attr("href", OleDocUrl);
                                $("#caseActUploadDoc").data("filename", data.ole3);
                                $("#caseActUploadDoc").html(" <i class='fa fa-download' aria-hidden='true'></i>");
                                $("#caseActUploadDocSpan").html(data.ole3);
                                $("#caseActUploadDocSpan").on('click', function() {
                                    var win = window.open(OleDocUrl, '_blank');
                                    win.focus();
                                });
                            }
                            $('#original_ini').val(data.initials0);
                            var datefrmt = moment(data.date).format('MM/DD/YYYY');
                            var datetime = moment(data.date).format('hh:mm a');
                            var datebicyclefrmt = moment(data.bicycle).format('MM/DD/YYYY');
                            var datepmtdate = moment(data.bipmtdate).format('MM/DD/YYYY');
                            var datepmduedate = moment(data.bipmtduedt).format('MM/DD/YYYY');
                            $("#activity_no").val(data.actno);
                            $('#add_case_cal').val(datefrmt);
                            $('#add_case_time').val(datetime);
                            if(data.event != '' && data.event != null) {
                                $('#activity_event').html(data.event.replace('<br>', '&#13;&#10;'));
                            }
                            if ($('#case_act_atty option:contains(' + data.atty + ')').length) {
                                $('select[name=case_act_atty]').val(data.atty);
                            } else
                            {
                                $('#case_act_atty').append("<option value='" + data.atty + "' selected='selected'>" + data.atty + "</option>");
                            }
                            $('#case_act_time').val(data.minutes);
                            $('#case_act_hour_rate').val(data.rate);
                            $('#case_act_cost_amt').val(data.cost);
                            $('#activity_title').val(data.title).trigger("change");
                            var str1 = data.type;
                            if (str1 != null) {
                                if (str1.indexOf('OTHER-') != -1) {
                                    var ret = data.type.replace('OTHER-', '');
                                    $('#activity_fee').val('OTHER').trigger('change');
                                    $('#activity_fee1').val(ret);
                                }
                            } else {
                                $('#activity_fee').val(data.type).trigger('change');
                            }
                            $("input[name=activity_classification][value=" + data.arap + "]").parent('div').addClass('checked');
                            $('#color_codes').val(data.color).trigger("change");
                            $('#security').val(data.access).trigger("change");
                            $('select[name=case_category]').val(data.category);
                            if (data.mainnotes != 0) {
                                $('#activity_mainnotes').parent('div').addClass('checked');
                                $('input[name=activity_mainnotes]').prop('checked', true).triggerHandler('click');
                            }
                            if (data.redalert != 0) {
                                $('#activity_redalert').parent('div').addClass('checked');
                                $('input[name=activity_redalert]').prop('checked', true).triggerHandler('click');
                            }
                            if (data.upontop != 0) {
                                $('#activity_upontop').parent('div').addClass('checked');
                                $('input[name=activity_upontop]').prop('checked', true).triggerHandler('click');
                            }
                            $('#activity_typeact').val(data.typeact).trigger("change");
                            $('#activity_doc_type').val(data.wordstyle).trigger("change");
                            $('#activity_style').val(data.ole3style).trigger("change");
                            $('#case_act_TM').val(data.frmtm);
                            $('#case_act_BM').val(data.frmbm);
                            $('#case_act_LM').val(data.frmlm);
                            $('#case_act_RM').val(data.frmrm);
                            $('#case_act_PW').val(data.frmwidth);
                            $('#case_act_PH').val(data.frmheight);
                            $('#case_act_orientation').val(data.orient).trigger("change");
                            $('#case_act_copies').val(data.copies);
                            $('#activity_entry_type').val(data.bistyle).trigger("change");
                            $('#activity_teamno').val(data.biteamno);
                            $('#activity_staff').html(data.biowner);
                            $('#activity_short_desc').html(data.bidesc);
                            $('#activity_event_desc').html(data.bicost);
                            $('#activity_biling_cycle').val(datebicyclefrmt);
                            $('#activity_postmark_date').val(datepmtdate);
                            $('#activity_payment_due').val(datepmduedate);
                            $('#activity_latefee').val(data.bilatefee);
                            $('#activity_check_no').val(data.bicheckno);
                            $('#activity_bi_hour').html(data.event);
                            $('#activity_bi_min').html(data.bitime);
                            $('#activity_bi_rate').html(data.bihourrate);
                            $('#activity_bi_fee').html(data.bifee);
                            $('#activity_bi_latefee').html(data.bilatefee);
                            $('#activity_bi_creditac').html(data.bipmt);
                            $("#add_new_case_activity select").trigger('change');
                            $('#addcaseact').modal('show');
                            caseNo = $('#caseActNo').val();
                            actNo = $('#activity_no').val();
                            $.ajax({
                                url: '<?php echo base_url(); ?>cases/get_uploaded_files',
                                method: 'POST',
                                dataType: 'json',
                                data: {
                                    caseNo: caseNo,
                                    actNo: actNo
                                },
                                success: function(res) {
                                    $.each(res, function (index, item) {
                                        if($('#add_new_case_activity').find('.dz-preview').length == 0) {
                                            html = '<div class="dz-preview dz-image-preview"></div>';
                                            $('#add_new_case_activity').append(html);
                                            $('.dz-preview').first().remove();
                                        }
                                        item.path = item.path.replace('<?php echo ASSPATH; ?>', '<?php echo base_url()."assets/"; ?>');
                                        var baseUrl = item.path;
                                        var mockFile = {name: item.name, size: 12345, status: 'success'};
                                        myDropzone.emit("addedfile", mockFile);
                                        myDropzone.emit("thumbnail", mockFile, baseUrl);
                                        myDropzone.files.push(mockFile);
                                    });
                                }
                            });
                        }
                    }
                });
            }
            function setValidation() {
                $.validator.addMethod("checkLicenseNo", function (license_no, element) {
                    var pattern = /^[0-9a-zA-Z]*$/;
                    return pattern.test(license_no);
                }, "Please enter proper License number");
                var validator = $('#addRolodexForm').validate({
                    ignore: [],
                    rules: {
                        card_first: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() == '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_firm: {
                            required: function (element) {
                                if ($("input[name=card_first]").val() == '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_address1: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_city: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_state: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            },
                            stateUS: true
                        },
                        card2_zip: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            },
                            zipcodeUS: true
                        },
                        card_type: {
                            required: true
                        },
                        card_business: {phoneUS: true},
                        card_home: {phoneUS: true},
                        card_fax: {phoneUS: true, maxlength: 15},
                        card_email: {email: true},
                        card2_phone1: {phoneUS: true},
                        card2_fax: {phoneUS: true},
                        card2_phone2: {phoneUS: true},
                        card2_fax2: {phoneUS: true},
                        card2_tax_id: {alphanumeric: true, maxlength: 15},
                        card_social_sec: {ssId: true},
                        card_speciality: {letterswithbasicpunc: true},
                        card_licenseno: {
                            maxlength: 15
                        }
                    },
                    messages: {
                        card_first: {required: "Please Fill First Name or Firm Name"},
                        card2_firm: {required: "Please Fill First Name or Firm Name"},
                        card2_fax: {phoneUS: "Please specify a valid fax number"},
                        card2_fax2: {phoneUS: "Please specify a valid fax number"},
                        card_fax: {phoneUS: "Please specify a valid fax number"}
                    },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');
                        $('#addcontact a[href="#' + tab + '"]').tab('show');
                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "card_type") {
                            error.insertAfter(element.next());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                return validator;
            }
            function editContact(cardCode) {
                $.blockUI();
                $.ajax({
                    url: "<?php echo base_url() . 'rolodex/getEditRolodex'; ?>",
                    method: "POST",
                    data: {'cardcode': cardCode},
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments = '';
                        if (data.card_comments) {
                            if(isJson(data.card_comments)) {
                                notes_comments = $.parseJSON(data.card_comments);
                            } else {
                                notes_comments = data.card_comments;
                            }
                        }
                        $('#addcontact .modal-title').html('Edit Contact');
                        $('#card_salutation').val(data.salutation);
                        $('#card_suffix').val(data.suffix);
                        $('select[name=card_title]').val(data.title);
                        $('#card_type').val(data.type);
                        $('input[name=card_first]').val(data.first);
                        $('input[name=card_middle]').val(data.middle);
                        $('input[name=card_last]').val(data.last);
                        $('input[name=card_popular]').val(data.popular);
                        $('input[name=card_letsal]').val(data.letsal);
                        $('input[name=card_occupation]').val(data.occupation);
                        $('input[name=card_employer]').val(data.employer);
                        if (data.date_of_hire == 'NA') {
                            $('input[name=date_of_hire]').val('');
                        } else {
                            $('input[name=date_of_hire]').val(moment(data.date_of_hire).format('MM/DD/YYYY'));
                        }
                        $('input[name=hiddenimg]').val(data.picture);
                        if (data.picture)
                        {
                            document.getElementById('profilepicDisp').style.display = 'block';
                            document.getElementById('dispProfile').src = data.picturePath;
                        } else
                        {
                            document.getElementById('dispProfile').src = '';
                        }
                        $('input[name=card2_firm]').val(data.firm);
                        $('input[name=card2_address1]').val(data.address1);
                        $('input[name=card2_address2]').val(data.address2);
                        $('input[name=card2_city]').val(data.city);
                        $('input[name=card2_state]').val(data.state);
                        $('input[name=card2_zip]').val(data.zip);
                        $('input[name=card2_venue]').val(data.venue);
                        $('input[name=card2_eamsref]').val(data.eamsref);
                        $('input[name=eamsref_name]').val(data.eamsname);
                        $('input[name=card_business]').val(data.business);
                        $('input[name=card_fax]').val(data.card_fax);
                        $('input[name=card_home]').val(data.home);
                        $('input[name=card_car]').val(data.car);
                        $('input[name=card_email]').val(data.email);
                        if (notes_comments != '')
                            $('input[name=card_notes]').val(notes_comments[0].notes);
                        else
                            $('input[name=card_notes]').val();
                        $('input[name=card2_phone1]').val(data.phone1);
                        $('input[name=card2_phone2]').val(data.phone2);
                        $('input[name=card2_tax_id]').val(data.tax_id);
                        $('input[name=card2_fax]').val(data.card2_fax);
                        $('input[name=card2_fax2]').val(data.fax2);
                        $('input[name=card_social_sec]').val(data.social_sec);
                        if (data.birth_date == 'NA') {
                            $('input[name=card_birth_date]').val('');
                        } else {
                            $('input[name=card_birth_date]').val(moment(data.birthdate).format('MM/DD/YYYY'));
                        }
                        $('input[name=card_licenseno]').val(data.licenseno);
                        $('select[name=card_specialty]').val(data.specialty);
                        $('input[name=card_mothermaid]').val(data.mothermaid);
                        $('input[name=card_cardcode]').val(data.cardcode);
                        $('input[name=card_firmcode]').val(data.firmcode);
                        $('select[name=card_interpret]').val(data.interpret);
                        $('select[name=card_language]').val(data.language).trigger("change");
                        $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                        $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                        $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                        $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                        if (notes_comments != '')
                            $('textarea[name=card_comments]').val(notes_comments[1].comments);
                        else
                            $('textarea[name=card_comments]').val();
                        $('textarea[name=card2_comments]').val(data.card2_comments);
                        $('textarea[name=card2_mailing1]').val(data.mailing1);
                        $('textarea[name=card2_mailing2]').val(data.mailing2);
                        $('textarea[name=card2_mailing3]').val(data.mailing3);
                        $('textarea[name=card2_mailing4]').val(data.mailing4);
                        $('#new_case').val('parties');
                        $('input[name="card_cardcode"]').val(data.cardcode);
                        $('#addcontact').modal({backdrop: 'static', keyboard: false});
                        $("#addcontact").modal('show');
                        $("#addRolodexForm select").trigger('change');
                        $.unblockUI();
                    }
                });
            }
            function editCaseDetails(caseNo, selectTab) {
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getAjaxCaseDetail',
                    data: 'caseno=' + caseNo,
                    method: 'POST',
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        setTimeout(function () {
                            $.unblockUI();
                        }, 500);
                    },
                    success: function (data) {
                        data = data[0];
                        setTimeout(function () {
                            if (data.dateenter != '1970-01-01 05:30:00' && data.dateenter != '1899-12-30 00:00:00' && data.dateenter != '0000-00-00 00:00:00' && data.dateenter != null) {
                                var dateenter = moment(data.dateenter).format('MM/DD/YYYY');
                                $('#casedate_entered').val(dateenter);
                            }

                            if (data.dateopen != '1970-01-01 05:30:00' && data.dateopen != '1899-12-30 00:00:00' && data.dateopen != '0000-00-00 00:00:00' && data.dateopen != null) {
                                var dateopen = moment(data.dateopen).format('MM/DD/YYYY');
                                $('#casedate_open').val(dateopen);
                            }

                            if (data.followup != '1970-01-01 05:30:00' && data.followup != '1899-12-30 00:00:00' && data.followup != '0000-00-00 00:00:00' && data.followup != null) {
                                var followup = moment(data.followup).format('MM/DD/YYYY');
                                $('#casedate_followup').val(followup);
                            }

                            if (data.dateclosed != '1970-01-01 05:30:00' && data.dateclosed != '1899-12-30 00:00:00' && data.dateclosed != '0000-00-00 00:00:00' && data.dateclosed != null) {
                                var dateclosed = moment(data.dateclosed).format('MM/DD/YYYY');
                                $('#casedate_closed').val(dateclosed);
                            }

                            if (data.psdate != '1970-01-01 05:30:00' && data.psdate != '1899-12-30 00:00:00' && data.psdate != '0000-00-00 00:00:00' && data.psdate != null) {
                                var psdate = moment(data.psdate).format('MM/DD/YYYY');
                                $('#casedate_psdate').val(psdate);
                            }

                            if (data.d_r != '1970-01-01 05:30:00' && data.d_r != '1899-12-30 00:00:00' && data.d_r != '0000-00-00 00:00:00' && data.d_r != null) {
                                var d_r = moment(data.d_r).format('MM/DD/YYYY');
                                $('#casedate_referred').val(d_r);
                            }

                            if (data.udfall2 != null) {
                                var res = data.udfall2.split(",");
                                $('#closedfileno').val(res[0]);
                                $('select[name=editOtherClass]').val(res[1]);
                            }

                            $('#fileno').val(data.yourfileno);
                            if (data.mainVanue) {
                                $('#editCaseVenue1').val(data.mainVanue);
                            } else {
                                $('#editCaseVenue1').val(data.last);
                            }

                            $('#editCaseVenue').val(data.venue);
                            $('select[name=editCaseSubType]').val(data.udfall);
                            if ($("#editCaseType").hasClass("select2Class")) {
                                if ($('#editCaseType option:contains(' + data.casetype + ')').length) {
                                    $('select[name=editCaseType]').val(data.casetype);
                                } else {
                                    if (data.casetype) {
                                        $('#editCaseType').append("<option value='" + data.casetype + "' selected='selected'>" + data.casetype + "</option>");
                                    }
                                }
                            } else if ($("#editCaseType").hasClass("cstmdrp")) {
                                if (data.casetype) {
                                    $('#editCaseType').val(data.casetype);
                                }
                            }

                            if ($("#editCaseStat").hasClass("select2Class")) {
                                if ($('#editCaseStat option:contains(' + data.casetype + ')').length) {
                                    $('select[name=editCaseStat]').val(data.casetype);
                                } else {
                                    if (data.casestat) {
                                        $('#editCaseStat').append("<option value='" + data.casestat + "' selected='selected'>" + data.casestat + "</option>");
                                    }
                                }
                            } else if ($("#editCaseStat").hasClass("cstmdrp")) {
                                if (data.casestat) {
                                    $('#editCaseStat').val(data.casestat);
                                }
                            }

                            if ($("#editCaseAttyResp").hasClass("select2Class")) {
                                if ($('#editCaseAttyResp option:contains(' + data.atty_resp + ')').length) {
                                    $('select[name=editCaseAttyResp]').val(data.atty_resp);
                                } else {
                                    if (data.atty_resp) {
                                        $('#editCaseAttyResp').append("<option value='" + data.atty_resp + "' selected='selected'>" + data.atty_resp + "</option>");
                                    }
                                }
                            } else if ($("#editCaseAttyResp").hasClass("cstmdrp")) {
                                if (data.atty_resp) {
                                    $('#editCaseAttyResp').val(data.atty_resp);
                                }
                            }

                            if ($("#editCaseAttyHand").hasClass("select2Class")) {
                                if ($('#editCaseAttyHand option:contains(' + data.atty_hand + ')').length) {
                                    $('select[name=editCaseAttyHand]').val(data.atty_hand);
                                } else {
                                    if (data.atty_hand) {
                                        $('#editCaseAttyHand').append("<option value='" + data.atty_hand + "' selected='selected'>" + data.atty_hand + "</option>");
                                    }
                                }
                            } else if ($("#editCaseAttyHand").hasClass("cstmdrp")) {
                                if (data.atty_hand) {
                                    $('#editCaseAttyHand').val(data.atty_hand);
                                }
                            }

                            if ($("#editCaseAttyPara").hasClass("select2Class")) {
                                if ($('#editCaseAttyPara option:contains(' + data.para_hand + ')').length) {
                                    $('select[name=editCaseAttyPara]').val(data.para_hand);
                                } else {
                                    if (data.para_hand) {
                                        $('#editCaseAttyPara').append("<option value='" + data.para_hand + "' selected='selected'>" + data.para_hand + "</option>");
                                    }
                                }
                            } else if ($("#editCaseAttyPara").hasClass("cstmdrp")) {
                                if (data.para_hand) {
                                    $('#editCaseAttyPara').val(data.para_hand);
                                }
                            }

                            if ($("#editCaseAttySec").hasClass("select2Class")) {
                                if ($('#editCaseAttySec option:contains(' + data.sec_hand + ')').length) {
                                    $('select[name=editCaseAttySec]').val(data.sec_hand);
                                } else {
                                    if (data.sec_hand) {
                                        $('#editCaseAttySec').append("<option value='" + data.sec_hand + "' selected='selected'>" + data.sec_hand + "</option>");
                                    }
                                }
                            } else if ($("#editCaseAttySec").hasClass("cstmdrp")) {
                                if (data.sec_hand) {
                                    $('#editCaseAttySec').val(data.sec_hand);
                                }
                            }
                            if(data.category_events && data.category_events.message1)

                                $('#caseedit_message1').val(data.category_events.message1);
                            else
                                $('#caseedit_message1').val('');
                            
                            if(data.category_events && data.category_events.message2)
                                $('#caseedit_message2').val(data.category_events.message2);
                            else
                                $('#caseedit_message2').val('');
                            
                            if(data.category_events && data.category_events.message3)
                                $('#caseedit_message3').val(data.category_events.message3);
                            else
                                $('#caseedit_message3').val('');
                            
                                if(data.category_events && data.category_events.message4)
                                $('#caseedit_message4').val(data.category_events.message4);
                            else
                                $('#caseedit_message4').val('');

                            $('#case_ps').val(data.ps);
                            $('#fileloc').val(data.location);
                            $('select[name=case_reffered_by]').val(data.rb);
                            if (typeof data.category_events !== 'undefined') {
                                if (typeof data.category_events.quickNote !== 'undefined') {
                                    $('#editcase_quicknote').val(data.category_events.quickNote);
                                }
                                if (typeof data.category_events.message1 !== 'undefined') {
                                    $('#caseedit_message1').text(data.category_events.message1);
                                }
                                if (typeof data.category_events.message2 !== 'undefined') {
                                    $('#caseedit_message2').text(data.category_events.message2);
                                }
                                if (typeof data.category_events.message3 !== 'undefined') {
                                    $('#caseedit_message3').text(data.category_events.message3);
                                }
                            }
                            $('#editcase_caption').val(data.caption1);
                            $('#editcasedetail').modal('show');
                            $("#edit_case_details select").trigger('change');
                            $("a[href='#" + selectTab + "']").trigger("click");
                        }, 500);
                    }
                });
            }
            $(document.body).on('click', '.viewPartiesDetail', function () {
                $("#rolodexPartiesResult tr").removeClass("highlight");
                $(this).addClass('highlight');
                var caseNo = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: $(this).data('cardcode'),
                        caseno: caseNo
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data == false) {
                            swal({
                                title: "Alert",
                                text: "No Party Selected.",
                                type: "warning"
                            });
                        } else {
                            $("#searched_party_fullname").val(data.fullname);
                            $("#searched_party_firmname").val(data.firm);
                            $('#searched_party_type').html(data.type);
                            $('#searched_party_speciality').html(data.speciality);
                            $('#searched_party_address').html(data.address1);
                            $('#searched_party_city').html(data.city);
                            $('#searched_party_cardnumber').html(data.cardcode);
                            $('#searched_party_ssno').html(data.social_sec);
                            $('#searched_party_email').html(data.email);
                            $('#searched_party_licenseno').html(data.licenseno);
                            $('#searched_party_homephone').html(data.home);
                            $('#searched_party_business').html(data.business);
                            $('.addSearchedParties').attr("disabled", false);
                            $('.editPartyCard').attr("disabled", false);
                        }
                    }
                });
            });
            function setPartyViewData(cardcode, caseNo) {

                $('.edit-party').show();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getAjaxCardDetails',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        caseno: caseNo
                    },
                    beforeSend: function (xhr) {
                    },
                    complete: function (jqXHR, textStatus) {
                    },
                    success: function (data) {
                        if (data == false) {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has been occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        } else {

                            var notes_comments = '';
                            if (data.comment_per) {
                                if(isJson(data.comment_per)) {
                                    notes_comments = $.parseJSON(data.comment_per);
                                } else {
                                    notes_comments = data.comment_per;
                                }
                            }
                            $('.edit-party').attr('data-casecard', cardcode);
                            $('.edit-party').attr('data-case', caseNo);
                            $('#party_name').html(data.fullname);
                            $('#party_name').data('cardcodeWarning', data.cardcode);
                            $('#party_type').data('cardcodeWarning', data.cardcode);
                            $('#partiesCardCode').val(data.cardcode);
                            $('#party_type').html(data.type);
                            $('#firm_name').html(data.firm);
                            $('.addressVal').attr('href', "https://maps.google.com/?q=" + data.address1 + data.address2);
                            $('#party_address1').html(data.address1);
                            $('#party_address2').html(data.address2);
                            $('#party_city').html(data.city);
                            $('#party_phn_home').html(data.home);
                            $('#party_phn_office').html(data.business);
                            $('#party_fax').html(data.fax);
                            $('#party_email').html(data.email);
                            $('#party_side').html(data.side);
                            $('#party_offc_no').html(data.officeno);
                            $('#party_notes').html('');
                            if (data.eamsref)
                                $('#party_eams').html(data.name + ' ' + data.eamsref);
                            else
                                $('#party_eams').html('');
                            $('#party_cell').html(data.car);
                            $('#party_beeper').html(data.beeper);
                            $('#party_notes').html(data.notes);
                            if (data.comment_bus != '') {
                                $('.busComment').css({'visibility': 'visible'});
                                $('#comment_bus').html(data.comment_bus);
                            } else {
                                $('.busComment').css({'visibility': 'hidden'});
                                $('#comment_bus').html(data.comment_bus);
                            }
                            if (data.comment_per && notes_comments[1]) {
                                $('.busComment').css({'visibility': 'visible'});
                                $('#comment_per').html(notes_comments[1].comments);
                            } else {
                                $('.busComment').css({'visibility': 'hidden'});
                                $('#comment_per').html('');
                            }
                            if (data.atty_hand)
                            {
                                if ($('#case_act_atty option:contains(' + data.atty_hand + ')').length) {
                                    $('select[name=case_act_atty]').val(data.atty_hand);
                                } else
                                {
                                    $('#case_act_atty').append("<option value='" + data.atty_hand + "' selected='selected'>" + data.atty_hand + "</option>");
                                }
                            }
                        }
                    }
                });
                
            }
            /* End Party Content */
            $(document.body).on('click', '#change_name_warning', function () {
                $("#partiesNameChangeWarning").modal("hide");
                var cardCode = $('#caseCardCode').val();
                $.ajax({
                    url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                    method: "POST",
                    data: {'cardcode': cardCode},
                    success: function (result) {
                        if (result > 1) {
                            swal({
                                title: "Warning!",
                                type: "warning",
                                text: "This card is currently attached to " + result + " cases.\nChange to this card will affect " + result + " cases.\n\nIf you want changes to be reflected for only one case please create a new card \n\nAre you sure you want to create new rolodex and replace it to this rolodex card? ",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true
                            }, function () {
                                CreateNewRolodex();
                            });
                        } else
                        {
                            ChangeRolodex();
                        }
                    }
                });

            });
            $(document.body).on('click', '#change_name_warning_no', function () {
                $("#partiesNameChangeWarning").modal("hide");
                var cardCode = $('#caseCardCode').val();
                $.ajax({
                    url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                    method: "POST",
                    data: {'cardcode': cardCode},
                    success: function (result) {
                        if (result > 1) {
                            swal({
                                title: "Warning!",
                                type: "warning",
                                text: "This card is currently attached to " + result + " cases.\nChange to this card will affect " + result + " cases.\n\nIf you want changes to be reflected for only one case please detach that card from that case at the Parties sheet and attach another card\n\nAre you sure you want to make changes to this rolodex card? ",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes",
                                closeOnConfirm: true
                            }, function () {
                                ChangeRolodex();
                            });
                        } else
                        {
                            ChangeRolodex();
                        }
                    }
                });
            });
            $(document.body).on('click', '.typeLink', function () {
                var cardcode = $('#party_type').data('cardcodeWarning');
                var caseno = $("#caseNo").val();
                if(cardcode){
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getCaseSpecific',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        caseno: caseno
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        /* $("#parties_case_specific option[value='" + data[0].type + "']").attr('selected', 'selected').trigger('change');*/
                        $('select[name=parties_case_specific]').val(data[0].type);
                        $("#parties_side").val(data[0].side);
                        $("#parties_office_number").val(data[0].officeno);
                        $("#parties_notes").val(data[0].notes);
                        (data[0].flags != '1') ? $('#parties_party_sheet').iCheck('uncheck') : $('#parties_party_sheet').iCheck('check');
                    }
                });
                $("#caseSpecific").modal("show");
                }else{
                    swal({
                        title: "Alert",
                        text: "There are no Clients attached to this case.",
                        type: "warning"
                    });
                }
            });
            $(document.body).on('click', '.editPartyCard', function () {
                var cardcode = $('#searched_party_cardnumber').html();
                if (cardcode == "" || cardcode == 'undefinded') {
                    swal({
                        title: "Alert",
                        text: "The card does not exist in the Rolodex or cannot be edited at this time. Please try searching again.",
                        type: "warning"
                    });
                } else {
                    $("#addcontact").attr("style", "z-index:9999 !important");
                    editContact(cardcode);
                }
            });
            $(document.body).on('click', '#attach_name', function () {
                var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                var caseno = $("#duplicateEntries .highlight").data('caseno');
                var first_name = $(".first_name").val();
                var last_name = $(".last_name").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/attach_name',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        first_name: first_name,
                        last_name: last_name,
                        caseno: caseno
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (result) {
                        var url = "<?php echo base_url() ?>cases/case_details/" + result.caseno;
                        $(location).attr('href', url);
                    }
                });
            });
            $(document.body).on('click', '.selectDuplicateData', function () {
                $("#duplicateEntries tr").removeClass("highlight");
                $(this).addClass('highlight');
                var numItems = $('.highlight').length;
                if (numItems == '1')
                {
                    $('#edit_new_case').prop('disabled', false);
                    $('#attach_name').prop('disabled', false);
                }
            });
            $(document.body).on('click', '#add_name_case', function () {
                $('input[name=card_first]').val($(".first_name").val());
                $('input[name=card_last]').val($(".last_name").val());
                $("#new_case").val(1);
                $("#addcase").modal("hide");
                $("#duplicateData").modal("hide");
                $("#addcontact").modal("show");
                return false;
            });
            $(document.body).on('click', '#edit_new_case', function () {
                $("#duplicateData").modal("hide");
                var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                editContact(cardcode);
            });
            $(document.body).on('click', '.addparty', function () {
                $('.addSearchedParties').attr("disabled", true);
                $('.editPartyCard').attr("disabled", true);
                $("#addparty").modal('show');
            });
            $(document.body).on('click', '.venuelist', function () {
                $("#venulist_model").modal("show");
            });
            $('#venue_table tbody').on('click', 'tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                    venue_table_data.$('tr.table-selected').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click', '#set_venue', function () {
                var contentArray = [];
                $.map(venue_table_data.rows('.table-selected').data(), function (item) {
                    contentArray = item;
                    return contentArray;
                });
                var nm = contentArray[0].split(" ");
                if (contentArray[2]) {
                    $("#editCaseVenue1").val(contentArray[2]);
                } else {
                    $("#editCaseVenue1").val('NA');
                }
                $("#editCaseVenue").val(contentArray.DT_RowId);
                $("#venulist_model").modal("hide");
            });
            $(document.body).on('click', '.edit-party', function () {
                if($('#edit-party').attr('data-casecard')){
                    $("#partiesNameChangeWarning").modal("show");
                    $('#caseCardCode').val($('#edit-party').attr('data-casecard'));
                }
                else{
                    swal({
                        title: "Alert",
                        text: "There are no Clients attached to this case.",
                        type: "warning"
                    });
                }
                

            });
            function CreateNewRolodex() {
                $('#newRolodexCard').val('1');
                $('#oldCardValue').val($('#caseCardCode').val());
                $('#oldCaseNo').val($('#caseNo').val());
                var cardcode = $('#caseCardCode').val();
                var caseno = $('#caseNo').val();
                $('#show_conform_model').modal('hide');
                $.ajax({
                    url: '<?php echo base_url(); ?>rolodex/getEditRolodex',
                    data: {cardcode: cardcode, caseNO: caseno, status: 'Yes'},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments = '';
                        if (data.card_comments) {
                            if(isJson(data.card_comments)) {
                                notes_comments = $.parseJSON(data.card_comments);
                            } else {
                                notes_comments = data.card_comments;
                            }
                        }
                        swal.close();
                        if (data.status == '0')
                        {
                            var name = '';
                            if (data.data.type == 'APPLICANT')
                            {
                                name = data.data.last + ', ' + data.data.first;
                                $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                            } else if (data.data.type == 'LIEN' || data.data.type == 'BOARD' || data.data.type == 'INSURANCE' || data.data.type == 'ATTORNEY' || data.data.type == 'EMPLOYER')
                            {
                                name = data.data.firm;
                                $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                            } else if (data.data.type == 'DR') {
                                if (data.data.firm.length == 0) {
                                    name = data.data.last + ', ' + data.data.first;
                                    $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                } else {
                                    name = data.data.firm;
                                    $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                }
                            } else
                            {
                                name = data.data.last + ', ' + data.data.first;
                                $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                            }
                            $('#rolodex_search_text').val(name);
                            $("#rolodex_search_parties_form").submit();
                            $("#addparty").modal('show');
                        } else
                        {
                            $('#addcontact .modal-title').html('Edit Contact');
                            $('#card_salutation').val(data.salutation);
                            $('#card_suffix').val(data.suffix);
                            $('select[name=card_title]').val(data.title);
                            $('#card_type').val(data.type);
                            $('input[name=card_first]').val(data.first);
                            $('input[name=card_middle]').val(data.middle);
                            $('input[name=card_last]').val(data.last);
                            $('input[name=card_popular]').val(data.popular);
                            $('input[name=card_letsal]').val(data.letsal);
                            $('input[name=card_occupation]').val(data.occupation);
                            $('input[name=card_employer]').val(data.employer);
                            if (data.date_of_hire == 'NA') {
                                $('input[name=date_of_hire]').val('');
                            } else {
                                $('input[name=date_of_hire]').val(moment(data.date_of_hire).format('MM/DD/YYYY'));
                            }
                            $('input[name=hiddenimg]').val(data.picture);
                            if (data.picture)
                            {
                                document.getElementById('profilepicDisp').style.display = 'block';
                                document.getElementById('dispProfile').src = data.picturePath;
                            } else
                            {
                                document.getElementById('dispProfile').src = '';
                            }
                            $('input[name=card2_firm]').val(data.firm);
                            $('input[name=card2_address1]').val(data.address1);
                            $('input[name=card2_address2]').val(data.address2);
                            $('input[name=card2_city]').val(data.city);
                            $('input[name=card2_state]').val(data.state);
                            $('input[name=card2_zip]').val(data.zip);
                            $('input[name=card2_venue]').val(data.venue);
                            $('input[name=card2_eamsref]').val(data.eamsref);
                            $('input[name=eamsref_name]').val(data.eamsname);
                            $('input[name=card_business]').val(data.business);
                            $('input[name=card_fax]').val(data.card_fax);
                            $('input[name=card_home]').val(data.home);
                            $('input[name=card_car]').val(data.car);
                            $('input[name=card_email]').val(data.email);
                            if (notes_comments != '')
                                $('input[name=card_notes]').val(notes_comments[0].notes);
                            else
                                $('input[name=card_notes]').val();
                            $('input[name=card2_phone1]').val(data.phone1);
                            $('input[name=card2_phone2]').val(data.phone2);
                            $('input[name=card2_tax_id]').val(data.tax_id);
                            $('input[name=card2_fax]').val(data.card2_fax);
                            $('input[name=card2_fax2]').val(data.fax2);
                            $('input[name=card_social_sec]').val(data.social_sec);
                            if (data.birth_date == 'NA') {
                                $('input[name=card_birth_date]').val('');
                            } else {
                                $('input[name=card_birth_date]').val(moment(data.birthdate).format('MM/DD/YYYY'));
                            }
                            $('input[name=card_licenseno]').val(data.licenseno);
                            $('select[name=card_specialty]').val(data.specialty);
                            $('input[name=card_mothermaid]').val(data.mothermaid);
                            $('input[name=card_cardcode]').val(data.cardcode);
                            $('input[name=card_firmcode]').val(data.firmcode);
                            $('select[name=card_interpret]').val(data.interpret);
                            $('select[name=card_language]').val(data.language).trigger("change");
                            $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                            $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                            $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                            $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                            if (notes_comments != '')
                                $('textarea[name=card_comments]').val(notes_comments[1].comments);
                            else
                                $('textarea[name=card_comments]').val();
                            $('textarea[name=card2_comments]').val(data.card2_comments);
                            $('textarea[name=card2_mailing1]').val(data.mailing1);
                            $('textarea[name=card2_mailing2]').val(data.mailing2);
                            $('textarea[name=card2_mailing3]').val(data.mailing3);
                            $('textarea[name=card2_mailing4]').val(data.mailing4);
                            $('#new_case').val('parties');
                            $('input[name="card_cardcode"]').val(data.cardcode);
                            $('#addcontact').modal({backdrop: 'static', keyboard: false});
                            $("#addcontact").modal('show');
                            $("#addRolodexForm select").trigger('change');
                            $.unblockUI();
                        }
                    }
                });
                $('#addcontact').modal({backdrop: 'static', keyboard: false});
                $("#addcontact").modal('show');
            }
            function ChangeRolodex() {
                $('#newRolodexCard').val('0');
                var cardcode = $('#caseCardCode').val();
                var caseno = $('#caseNo').val();
                $('#show_conform_model').modal('hide');
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>rolodex/getEditRolodex',
                    data: {cardcode: cardcode, caseNO: caseno, status: 'Yes'},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments = '';
                        if (data.card_comments) {
                            if(isJson(data.card_comments)) {
                                notes_comments = $.parseJSON(data.card_comments);
                            } else {
                                notes_comments = data.card_comments;
                            }
                        }
                        swal.close();
                        if (data.status == '0')
                        {
                            var name = '';
                            if (data.data.type == 'APPLICANT')
                            {
                                name = data.data.last + ', ' + data.data.first;
                                $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                            } else if (data.data.type == 'LIEN' || data.data.type == 'BOARD' || data.data.type == 'INSURANCE' || data.data.type == 'ATTORNEY' || data.data.type == 'EMPLOYER')
                            {
                                name = data.data.firm;
                                $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                            } else if (data.data.type == 'DR') {
                                if (data.data.firm.length == 0) {
                                    name = data.data.last + ', ' + data.data.first;
                                    $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                } else {
                                    name = data.data.firm;
                                    $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                }
                            } else
                            {
                                name = data.data.last + ', ' + data.data.first;
                                $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                            }
                            $('#rolodex_search_text').val(name);
                            $("#rolodex_search_parties_form").submit();
                            $("#addparty").modal('show');
                        } else
                        {
                            $('#addcontact .modal-title').html('Edit Contact');
                            $('#card_salutation').val(data.salutation);
                            $('#card_suffix').val(data.suffix);
                            $('select[name=card_title]').val(data.title);
                            $('#card_type').val(data.type);
                            $('input[name=card_first]').val(data.first);
                            $('input[name=card_middle]').val(data.middle);
                            $('input[name=card_last]').val(data.last);
                            $('input[name=card_popular]').val(data.popular);
                            $('input[name=card_letsal]').val(data.letsal);
                            $('input[name=card_occupation]').val(data.occupation);
                            $('input[name=card_employer]').val(data.employer);
                            if (data.date_of_hire == 'NA') {
                                $('input[name=date_of_hire]').val('');
                            } else {
                                $('input[name=date_of_hire]').val(moment(data.date_of_hire).format('MM/DD/YYYY'));
                            }
                            $('input[name=hiddenimg]').val(data.picture);
                            if (data.picture)
                            {
                                document.getElementById('profilepicDisp').style.display = 'block';
                                document.getElementById('dispProfile').src = data.picturePath;
                            } else
                            {
                                document.getElementById('dispProfile').src = '';
                            }
                            $('input[name=card2_firm]').val(data.firm);
                            $('input[name=card2_address1]').val(data.address1);
                            $('input[name=card2_address2]').val(data.address2);
                            $('input[name=card2_city]').val(data.city);
                            $('input[name=card2_state]').val(data.state);
                            $('input[name=card2_zip]').val(data.zip);
                            $('input[name=card2_venue]').val(data.venue);
                            $('input[name=card2_eamsref]').val(data.eamsref);
                            $('input[name=eamsref_name]').val(data.eamsname);
                            $('input[name=card_business]').val(data.business);
                            $('input[name=card_fax]').val(data.card_fax);
                            $('input[name=card_home]').val(data.home);
                            $('input[name=card_car]').val(data.car);
                            $('input[name=card_email]').val(data.email);
                            if (notes_comments != '')
                                $('input[name=card_notes]').val(notes_comments[0].notes);
                            else
                                $('input[name=card_notes]').val();
                            $('input[name=card2_phone1]').val(data.phone1);
                            $('input[name=card2_phone2]').val(data.phone2);
                            $('input[name=card2_tax_id]').val(data.tax_id);
                            $('input[name=card2_fax]').val(data.card2_fax);
                            $('input[name=card2_fax2]').val(data.fax2);
                            $('input[name=card_social_sec]').val(data.social_sec);
                            if (data.birth_date == 'NA') {
                                $('input[name=card_birth_date]').val('');
                            } else {
                                $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                            }
                            $('input[name=card_licenseno]').val(data.licenseno);
                            $('select[name=card_specialty]').val(data.specialty);
                            $('input[name=card_mothermaid]').val(data.mothermaid);
                            $('input[name=card_cardcode]').val(data.cardcode);
                            $('input[name=card_firmcode]').val(data.firmcode);
                            $('select[name=card_interpret]').val(data.interpret);
                            $('select[name=card_language]').val(data.language).trigger("change");
                            $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                            $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                            $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                            $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                            if (notes_comments != '')
                                $('textarea[name=card_comments]').val(notes_comments[1].comments);
                            else
                                $('textarea[name=card_comments]').val();
                            $('textarea[name=card2_comments]').val(data.card2_comments);
                            $('textarea[name=card2_mailing1]').val(data.mailing1);
                            $('textarea[name=card2_mailing2]').val(data.mailing2);
                            $('textarea[name=card2_mailing3]').val(data.mailing3);
                            $('textarea[name=card2_mailing4]').val(data.mailing4);
                            $('#new_case').val('parties');
                            $('input[name="card_cardcode"]').val(data.cardcode);
                            $('#addcontact').modal({backdrop: 'static', keyboard: false});
                            $("#addcontact").modal('show');
                            $("#addRolodexForm select").trigger('change');
                            $.unblockUI();
                        }
                    }
                });
            }

            $(document.body).on('click', '#relodex_no', function () {
                $('#show_conform_model').modal('hide');
                var cardcode = $('#edit-party').attr('data-casecard');
                var caseno = $('#edit-party').attr('data-case');
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>rolodex/editRolodexdetail',
                    data: {cardcode: cardcode, caseNO: caseno, status: 'NO'},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments = $.parseJSON(data.data.card_comments);
                        if (data.status == '0')
                        {
                            $.unblockUI();
                            swal({
                                title: 'Are you sure to make changes to this Rolodex Card?',
                                text: ' ' + data.message,
                                showCancelButton: true,
                                confirmButtonColor: "#1ab394",
                                confirmButtonText: "Yes",
                                closeOnConfirm: false,
                                showLoaderOnConfirm: true
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    swal.close();
                                    $('#addcontact .modal-title').html('Edit Contact');
                                    $('#card_salutation').val(data.data.salutation);
                                    $('#card_suffix').val(data.data.suffix);
                                    $('select[name=card_title]').val(data.data.title);
                                    $('#card_type').val(data.data.type);
                                    $('input[name=card_first]').val(data.data.first);
                                    $('input[name=card_middle]').val(data.data.middle);
                                    $('input[name=card_last]').val(data.data.last);
                                    $('input[name=card_letsal]').val(data.data.letsal);
                                    $('input[name=card_occupation]').val(data.data.occupation);
                                    $('input[name=card_employer]').val(data.data.employer);
                                    $('input[name=hiddenimg]').val(data.data.picture);
                                    if (data.data.picture)
                                    {
                                        document.getElementById('profilepicDisp').style.display = 'block';
                                        document.getElementById('dispProfile').src = data.data.picturePath;
                                    } else
                                    {
                                        document.getElementById('dispProfile').src = '';
                                    }
                                    $('input[name=card2_firm]').val(data.data.firm);
                                    $('input[name=card2_address1]').val(data.data.address1);
                                    $('input[name=card2_address2]').val(data.data.address2);
                                    $('input[name=card2_city]').val(data.data.city);
                                    $('input[name=card2_state]').val(data.data.state);
                                    $('input[name=card2_zip]').val(data.data.zip);
                                    $('input[name=card2_venue]').val(data.data.venue);
                                    $('input[name=card2_eamsref]').val(data.data.eamsref);
                                    $('input[name=eamsref_name]').val(data.eamsname);
                                    $('input[name=card_business]').val(data.data.business);
                                    $('input[name=card_fax]').val(data.data.card_fax);
                                    $('input[name=card_home]').val(data.data.home);
                                    $('input[name=card_car]').val(data.data.car);
                                    $('input[name=card_email]').val(data.data.email);
                                    if (notes_comments != '')
                                        $('input[name=card_notes]').val(notes_comments[0].notes);
                                    else
                                        $('input[name=card_notes]').val();
                                    $('input[name=card2_phone1]').val(data.data.phone1);
                                    $('input[name=card2_phone2]').val(data.data.phone2);
                                    $('input[name=card2_tax_id]').val(data.data.tax_id);
                                    $('input[name=card2_fax]').val(data.data.card2_fax);
                                    $('input[name=card2_fax2]').val(data.data.fax2);
                                    $('input[name=card_social_sec]').val(data.data.social_sec);
                                    if (data.data.birth_date == 'NA') {
                                        $('input[name=card_birth_date]').val('');
                                    } else {
                                        $('input[name=card_birth_date]').val(moment(data.data.birthdate).format('MM/DD/YYYY'));
                                    }
                                    $('input[name=card_licenseno]').val(data.data.licenseno);
                                    $('select[name=card_specialty]').val(data.data.specialty);
                                    $('input[name=card_mothermaid]').val(data.data.mothermaid);
                                    $('input[name=card_cardcode]').val(data.data.cardcode);
                                    $('input[name=card_firmcode]').val(data.data.firmcode);
                                    $('select[name=card_interpret]').val(data.data.interpret);
                                    $('select[name=card_language]').val(data.data.language).trigger("change");
                                    $('#form_datetime1').val(data.data.card_origchg + ' - ' + data.data.card_origdt);
                                    $('#form_datetime2').val(data.data.card_lastchg + ' - ' + data.data.card_lastdt);
                                    $('#form_datetime3').val(data.data.card2_origchg + ' - ' + data.data.card2_origdt);
                                    $('#form_datetime4').val(data.data.card2_lastchg + ' - ' + data.data.card2_lastdt);
                                    if (notes_comments != '')
                                        $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                    else
                                        $('textarea[name=card_comments]').val();
                                    $('textarea[name=card2_comments]').val(data.data.card2_comments);
                                    $('textarea[name=card2_mailing1]').val(data.data.mailing1);
                                    $('textarea[name=card2_mailing2]').val(data.data.mailing2);
                                    $('textarea[name=card2_mailing3]').val(data.data.mailing3);
                                    $('textarea[name=card2_mailing4]').val(data.data.mailing4);
                                    $('#new_case').val('parties');
                                    $('input[name="card_cardcode"]').val(data.data.cardcode);
                                    $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                    $("#addcontact").modal('show');
                                    $("#addRolodexForm select").trigger('change');
                                    $.unblockUI();
                                }
                            });
                        } else
                        {
                            $('#addcontact .modal-title').html('Edit Contact');
                            $('#card_salutation').val(data.data.salutation);
                            $('#card_suffix').val(data.data.suffix);
                            $('select[name=card_title]').val(data.data.title);
                            $('#card_type').val(data.data.type);
                            $('input[name=card_first]').val(data.data.first);
                            $('input[name=card_middle]').val(data.data.middle);
                            $('input[name=card_last]').val(data.data.last);
                            $('input[name=card_letsal]').val(data.data.letsal);
                            $('input[name=card_occupation]').val(data.data.occupation);
                            $('input[name=card_employer]').val(data.data.employer);
                            $('input[name=hiddenimg]').val(data.data.picture);
                            if (data.data.picture)
                            {
                                document.getElementById('profilepicDisp').style.display = 'block';
                                document.getElementById('dispProfile').src = data.data.picturePath;
                            } else
                            {
                                document.getElementById('dispProfile').src = '';
                            }
                            $('input[name=card2_firm]').val(data.data.firm);
                            $('input[name=card2_address1]').val(data.data.address1);
                            $('input[name=card2_address2]').val(data.data.address2);
                            $('input[name=card2_city]').val(data.data.city);
                            $('input[name=card2_state]').val(data.data.state);
                            $('input[name=card2_zip]').val(data.data.zip);
                            $('input[name=card2_venue]').val(data.data.venue);
                            $('input[name=card2_eamsref]').val(data.data.eamsref);
                            $('input[name=eamsref_name]').val(data.eamsname);
                            $('input[name=card_business]').val(data.data.business);
                            $('input[name=card_fax]').val(data.data.card_fax);
                            $('input[name=card_home]').val(data.data.home);
                            $('input[name=card_car]').val(data.data.car);
                            $('input[name=card_email]').val(data.data.email);
                            $('input[name=card2_phone1]').val(data.data.phone1);
                            $('input[name=card2_phone2]').val(data.data.phone2);
                            $('input[name=card2_tax_id]').val(data.data.tax_id);
                            $('input[name=card2_fax]').val(data.data.card2_fax);
                            $('input[name=card2_fax2]').val(data.data.fax2);
                            $('input[name=card_social_sec]').val(data.data.social_sec);
                            if (data.data.birth_date == 'NA') {
                                $('input[name=card_birth_date]').val('');
                            } else {
                                $('input[name=card_birth_date]').val(moment(data.data.birthdate).format('MM/DD/YYYY'));
                            }
                            $('input[name=card_licenseno]').val(data.data.licenseno);
                            $('select[name=card_specialty]').val(data.data.specialty);
                            $('input[name=card_mothermaid]').val(data.data.mothermaid);
                            $('input[name=card_cardcode]').val(data.data.cardcode);
                            $('input[name=card_firmcode]').val(data.data.firmcode);
                            $('select[name=card_interpret]').val(data.data.interpret);
                            $('select[name=card_language]').val(data.data.language).trigger("change");
                            $('#form_datetime1').val(data.data.card_origchg + ' - ' + data.data.card_origdt);
                            $('#form_datetime2').val(data.data.card_lastchg + ' - ' + data.data.card_lastdt);
                            $('#form_datetime3').val(data.data.card2_origchg + ' - ' + data.data.card2_origdt);
                            $('#form_datetime4').val(data.data.card2_lastchg + ' - ' + data.data.card2_lastdt);
                            $('textarea[name=card_comments]').val(data.data.card_comments);
                            $('textarea[name=card2_comments]').val(data.data.card2_comments);
                            $('textarea[name=card2_mailing1]').val(data.data.mailing1);
                            $('textarea[name=card2_mailing2]').val(data.data.mailing2);
                            $('textarea[name=card2_mailing3]').val(data.data.mailing3);
                            $('textarea[name=card2_mailing4]').val(data.data.mailing4);
                            $('#new_case').val('parties');
                            $('input[name="card_cardcode"]').val(data.data.cardcode);
                            $('#addcontact').modal({backdrop: 'static', keyboard: false});
                            $("#addcontact").modal('show');
                            $("#addRolodexForm select").trigger('change');
                            $.unblockUI();
                        }
                    }
                });
            });
            $(document.body).on('click', '.rolodex-company', function () {
                $("#firmcompanylist").attr("style", "z-index:9999 !important");
                $("#rolodexsearchResult_data  tbody").remove();
                rolodexSearch.destroy();
                $('#firmcompanylist').modal('show');
                $('#rolodexsearchResult_data').DataTable().clear();
            });
            $(document.body).on('keyup', '#rolodex_search_text_name', function (e) {
                if (e.keyCode == '13') {
                    $('#rolodex_search_text_name').trigger("blur");
                    $('#rolodex_search_text_name').trigger("focus");
                }
            });
            $(document.body).on('blur', '#rolodex_search_text_name', function (e) {
                if (e.type == 'focusout' || e.keyCode == '13')
                {
                    if ($('#companylistForm').valid())
                    {
                        if ($('#rolodex_search_text_name').val().trim().length > 0)
                        {
                            rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                                processing: true,
                                stateSave: true,
                                rolodexsearchAddress: true,
                                serverSide: true,
                                bFilter: true,
                                scrollX: true,
                                sScrollY: 400,
                                bScrollCollapse: true,
                                destroy: true,
                                autoWidth: true,
                                "lengthMenu": [5, 10, 25, 50, 100],
                                order: [[0, "ASC"]],
                                ajax: {
                                    url: '<?php echo base_url(); ?>cases/searchForRolodexByfirm',
                                    method: 'POST',
                                    data: {rolodex_search_parties: $('#rolodex_search_parties_name').val(), rolodex_search_text: $(this).val()},
                                },
                                fnRowCallback: function (nRow, data) {
                                    $(nRow).attr("cardcode", '' + data[8]);
                                    $(nRow).addClass('viewsearchPartiesDetail1');
                                },
                                fnDrawCallback: function (oSettings) {
                                },
                                initComplete: function (settings, json) {

                                },
                                select: {
                                    style: 'single'
                                }
                            });
                        } else
                        {
                            swal({
                                title: "Alert",
                                text: "Please enter Firm name.",
                                type: "warning"
                            });
                        }
                    }
                }
            });
            $(document.body).on('click', '#rolodexsearchResult_data tbody tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                    $('#rolodexsearchResult_data tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click', '.dataTables-eamsLookup tbody tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                    $('.dataTables-eamsLookup tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click', '#viewSearchedPartieDetails', function () {
                var selectedrows = rolodexSearch.rows('.table-selected').data();
                var caseNo = $("#caseNo").val();
                if (selectedrows.length > 0) {
                    $('#firmcompanylist').modal('hide');
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            cardcode: selectedrows[0][8],
                            caseno: caseNo
                        },
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data == false) {
                                swal({
                                    title: "Alert",
                                    text: "No Party Selected.",
                                    type: "warning"
                                });
                            } else {
                                var city = data.city;
                                if (city != null)
                                {
                                    var strarray = city.split(',');
                                    $('input[name="card2_city"]').val($.trim(strarray[0]));
                                    $('input[name="card2_state"]').val($.trim(strarray[1]));
                                    $('input[name="card2_zip"]').val($.trim(strarray[2]));
                                }
                                $('input[name="card2_firm"]').val(data.firm);
                                $('input[name="card2_address1"]').val(data.address1);
                                $('input[name="card2_address2"]').val(data.address2);
                                $('input[name=card_firmcode]').val(data.firmcode);
                                $('.addSearchedParties').attr("disabled", false);
                                $('.editPartyCard').attr("disabled", false);
                            }
                        }
                    });
                } else {
                    swal({
                        title: "Alert",
                        text: "No Party Selected.",
                        type: "warning"
                    });
                }
            });
            
            
            $(document.body).on('click', '#eamsrefDetails', function () {
                var selectedrows = eamsData.rows('.table-selected').data();
                var caseNo = $("#caseNo").val();
                var cardCode = $('#caseCardCode').val();
                if (selectedrows.length > 0) {
                    $('#eamslist').modal('hide');
                    swal({
                        title: 'EAMS',
                        text: 'Do you want to attach '+selectedrows[0][1]+' to the rolodex?',
                        showCancelButton: true,
                        confirmButtonColor: "#1ab394",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            swal.close();
                            $.ajax({
                                url: '<?php echo base_url(); ?>eams_lookup/eamsrefDetails',
                                method: 'POST',
                                dataType: 'json',
                                data: {
                                    eamsref: selectedrows[0][0],caseno: caseNo,cardCode:cardCode
                                },
                                beforeSend: function (xhr) {
                                    $.blockUI();
                                },
                                complete: function (jqXHR, textStatus) {
                                    $.unblockUI();
                                },
                                success: function (data) {
                                    if (data == false) {
                                        swal({
                                            title: "Alert",
                                            text: "No Party Selected.",
                                            type: "warning"
                                        });
                                    } else {

                                        $('input[name="card2_eamsref"]').val(data.eamsref);
                                        $('input[name="eamsref_name"]').val(data.name);
                                    }
                                }
                            });
                        }
                    });
                    
                } else {
                    swal({
                        title: "Alert",
                        text: "No Party Selected.",
                        type: "warning"
                    });
                }
            });
            
            $(document.body).on('click', '.event_note_list', function () {
                $('#eventnotelist').modal('show');
            });
            $('#eventnotelist').on('shown.bs.modal', function (e) {
                category_list($('#case_category_event').val(), $('#caseNo').val());
            });
            $(document.body).on('change', '.category_event_list', function () {
                category_list($('#case_category_event').val(), $('#caseNo').val());
            });
            $('#activity_list').on('ifClicked', function (event) {
                category_list($('#case_category_event').val(), $('#caseNo').val());
            });
            function category_list(category, caseno)
            {
                var status = '';
                if ($('#activity_list').prop("checked") == true) {
                    status = 1;
                } else {
                    status = 0;
                }
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getcategoryeventlist',
                    method: 'POST',
                    data: {category_list: category, case_id: caseno, status: status},
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        categoryeventlist.clear();
                        var obj = $.parseJSON(data);
                        $.each(obj, function (index, item) {
                            var html = '';
                            html += '<tr class="categorylistevent" data-event="' + (item.event) + '" data-category=' + item.category + '><td>' + item.event + '</td>';
                            html += '</tr>';
                            categoryeventlist.row.add($(html));
                        });
                        categoryeventlist.draw();
                    }
                });
            }
            $('#caseEventlist tbody').on('click', 'tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                    $('#caseEventlist tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click', '#caseactivityevent', function () {
                var row = categoryeventlist.row('.table-selected').node();
                $('#eventnotelist').modal('hide');
                $('#activity_event').val($(row).attr('data-event').replace('<br>', '&#13;&#10;'));
            });
            $('#addcase.modal').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });
            });
            $('#firmcompanylist').on('hidden.bs.modal', function () {
                $("#rolodex_search_text_name").val("");
                $('tr.table-selected').removeClass('table-selected');
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });
                $('#rolodexsearchResult_data').dataTable().fnClearTable();
            });
            $('#addparty').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });
                /*$("input:radio[name='rolodex_search_parties']").each(function () {
                    $(this).iCheck('uncheck');
                });*/
                rolodexSearchDatatable.clear();
                rolodexSearchDatatable.draw();
            });
            function caseDetails(cardCode) {
                $.ajax({
                    url: "<?php echo base_url() . 'cases/getCasrcardDetails' ?>",
                    method: "POST",
                    data: {
                        cardCode: cardCode
                    },
                    success: function (result) {
                        $('#viewAllCases').html(result);
                        $("#caseCardDetails").modal('show');
                    }
                });
            }
            function fileCheck(obj) {
                var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1)
                {
                    document.getElementById('imgerror').innerHTML = "Only '.jpeg','.jpg', '.png', '.gif', '.bmp' formats are allowed.";
                    return false;
                }
            }
            $('#addparty.modal').on('show.bs.modal', function () {
                if ($('table#rolodexPartiesResult tr').hasClass('highlight') == false)
                {
                    $('.addSearchedParties').prop('disabled', true);
                    $('.editPartyCard').prop('disabled', true);
                }
            });
            $('#addparty.modal').on('shown.bs.modal', function () {
                rolodexSearchDatatable.draw();
            });
            $('#firmcompanylist').on('show.bs.modal', function () {
                $(".dataTables-example").removeAttr("style");
                $(".dataTables_scrollHeadInner").removeAttr("style");
                $(".dataTables-example").css("width", "100% !important;");
                $(".dataTables_scrollHeadInner").css("width", "100% !important;");
            });
            
            $('#firmcompanylist').on('hide.bs.modal', function () {
                $('#rolodex_search_text_name-error').remove();
                $('#rolodex_search_text_name').removeClass('error');
            });
            $('#addcaseact.modal').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                    this.value = '';
                        $("#activity_mainnotes").iCheck('uncheck');
                        $("#activity_redalert").iCheck('uncheck');
                        $("#activity_upontop").iCheck('uncheck');
                        $("#activity_override").iCheck('uncheck');
                        $('.select2Class#case_category').val('1').trigger("change");
                        $('input[name=activity_no]').val('');
                });
                /*myDropzone.removeAllFiles(true);*/
                $( ".dz-preview" ).remove();
                $('#addcaseact a[href="#tab-26"]').tab('show');
            });
            $(document.body).on('click', '.CaseActEmail', function () {
                var activity_event = $('#activity_event').val();
                if (CKEDITOR.instances.mailbody1)
                    CKEDITOR.instances.mailbody1.destroy();
                CKEDITOR.replace('mailbody1');
                $('#addcaseact').modal('hide');
                $("#activity_mainnotes").iCheck('uncheck');
                $('input[name=activity_no]').val('');
                $('#CaseEmailModal').modal('show');
                $('#mailbody1').val(activity_event);
            });
            $(document.body).on('click', '.CaseActPrint', function () {
                $.ajax({
                    url: '<?php echo base_url() . 'cases/printpde' ?>',
                    method: "POST",
                    data: {caseno: $('#hdnCaseId').val()},
                    success: function (result) {
                        var based = "<?php echo base_url(); ?>" + 'assets/printCaseAct/' + result;
                        window.open(based, '_blank');
                    }
                });
            });
            $('#CaseEmailModal.modal').on('hidden.bs.modal', function () {
                $(this).find('input[type="email"],textarea,select,file').each(function () {
                    this.value = '';
                });
                /*$('#CaseEmailModal').find('#mail_urgent1').prop('checked', false);*/
                $('#CaseEmailModal').find('#mail_urgent1').iCheck("uncheck");
                $('#case_category1').val('1');
                $('#whoto1').val('0').trigger('change');
                $('#mail_subject1').val('');
                $('#emails1').val('');
                $("#send_mail_form1 select").trigger('change');
                var $el = $('#afiles2');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            });
            $(document.body).on('change', '#activity_fee', function () {
                if ($(this).val() == 'OTHER') {
                    $("#activity_fee1").show();
                } else {
                    $("#activity_fee1").hide();
                    $("#activity_fee1").val('');
                }
            });
            $(document.body).on('blur', '#case_act_time', function () {
                var a = eval($(this).val());
                $('#case_act_time').val(a);
                var hour = $('#case_act_hour_rate').val();
                if (hour > 0) {
                    var cost = parseInt(a * hour) / parseInt(60);
                    $('#case_act_cost_amt').val(cost);
                }
            });
            $(document.body).on('blur', '#case_act_hour_rate', function () {
                var a = eval($(this).val());
                var hour = $('#case_act_time').val();
                if (hour > 0) {
                    var cost = parseInt(a * hour) / parseInt(60);
                    $('#case_act_cost_amt').val(cost);
                }
            });
            var regExp = /[a-z]/i;
            function validate(e) {
                var value = String.fromCharCode(e.which) || e.key;
                if (regExp.test(value)) {
                    e.preventDefault();
                    return false;
                }
            }
            function remove_error(id) {
                var myElem = document.getElementById(id + '-error');
                if (myElem === null)
                {
                } else
                {
                    document.getElementById(id + '-error').innerHTML = '';
                }
            }
            function SubmitForm() {
                if ($("#add_new_case_activity").valid()) {
                    return true;
                } else {
                    return false;
                }
            }
            $('#addcaseact').on('hidden.bs.modal', function () {
                $("#activity_mainnotes").iCheck('uncheck');
                $("#activity_redalert").iCheck('uncheck');
                $("#activity_upontop").iCheck('uncheck');
                $("#activity_override").iCheck('uncheck');
                $('.select2Class#case_category').val('1').trigger("change");
                $('input[name=activity_no]').val('');
                $( ".dz-preview" ).remove();
                var $alertas = $('#add_new_case_activity');
                $alertas.validate().resetForm();
                $alertas.find('.error').removeClass('error');
            });
            $(document.body).on('change click', '.weekendsalert', function () {
                var weekalert = "<?php echo isset($holidayset->weekends) ? $holidayset->weekends : 0 ?>";
                if (weekalert != 0 && weekalert == 'on') {
                    var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var a = new Date($(this).val());
                    if (weekday[a.getDay()] == 'Saturday' || weekday[a.getDay()] == 'Sunday')
                    {
                        swal({
                            title: "",
                            text: "You are trying to add a task on non business day. Are you sure?",
                            customClass: 'swal-wide',
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "OK"
                        });
                    }
                }
            });
            $(document.body).on('change click', '.alerttonholiday', function () {
                var aletholi = $(this).val();
                var aftholiday = "<?php echo isset($holidayset->aftholiday) ? $holidayset->aftholiday : 0 ?>";
                if (aftholiday != 0 && aftholiday == 'on' && $(this).val() != '') {
                    var holidaylist = '<?php echo isset($holodaylist) ? json_encode($holodaylist) : 0; ?>';
                    if (JSON.parse(holidaylist).length > 0) {
                        $.each(JSON.parse(holidaylist), function (key, value) {
                            var temp = new Array();
                            temp = value.split(",");
                            var c = new Date(temp[0]);
                            var b = new Date(aletholi);
                            if (c.getTime() == b.getTime()) {
                                swal({
                                    title: "",
                                    text: "You are trying to add a task on non business day. Are you sure?",
                                    customClass: 'swal-wide',
                                    confirmButtonColor: "#1ab394",
                                    confirmButtonText: "OK"
                                });
                            }
                        });
                    }
                }
            });
            $(document.body).on('change click', '.birthcalalert', function () {
                var bialaddcalevent = "<?php echo isset($main1->bialaddcalevent) ? $main1->bialaddcalevent : 0 ?>";
                if (bialaddcalevent != 0 && bialaddcalevent == 'on') {
                    var birthdate = "<?php echo isset($display_details->birth_date) ? date('m/d/Y', strtotime($display_details->birth_date)) : 0 ?>";
                    if (birthdate != '' && birthdate != 0)
                    {
                        if ($(this).val() != '') {
                            var b = new Date(birthdate);
                            var y = new Date();
                            var year = y.getFullYear();
                            var month = b.getMonth();
                            var day = b.getDate();
                            var c = new Date(year, month, day);
                            var b = new Date($(this).val());
                            if (b.getTime() === c.getTime())
                            {
                                swal({
                                    title: "Notice",
                                    text: "You are scheduling this appointment on the client's birthday",
                                    confirmButtonColor: "#1ab394",
                                    confirmButtonText: "OK"
                                });
                            }
                        }
                    }
                }
            });
            $(document.body).on('change click', '.staffvacation', function () {
                var Uservacation = '<?php echo isset($Uservacation) ? json_encode($Uservacation) : 0; ?>';
                if ($('.staffTO').val() != "" && JSON.parse(Uservacation).length > 0 && $('.staffTO').val() == '<?php echo $this->session->userdata('user_data')['username'] ?>' && $('.sfinishby').val() != '') {
                    $.each(JSON.parse(Uservacation), function (key, value) {
                        var temp = new Array();
                        temp = value.split(",");
                        if (temp[0].includes("-") == true) {
                            var result = temp[0].split('-');
                            var dateFrom = result[0];
                            var dateTo = result[1];
                            var dateCheck = $('.sfinishby').val();
                            var from = Date.parse(dateFrom);
                            var to = Date.parse(dateTo);
                            var check = Date.parse(dateCheck);
                            if ((check <= to && check >= from))
                                swal({
                                    title: "",
                                    text: temp[1],
                                    customClass: 'swal-wide',
                                    confirmButtonColor: "#1ab394",
                                    confirmButtonText: "OK",
                                    timer: 2000
                                });
                        } else {
                            var c = new Date(temp[0]);
                            var b = new Date($('.sfinishby').val());
                            if (c.getTime() == b.getTime()) {
                                swal({
                                    title: "",
                                    text: temp[1],
                                    customClass: 'swal-wide',
                                    confirmButtonColor: "#1ab394",
                                    confirmButtonText: "OK"
                                });
                            }
                        }
                    });
                }
            });
            $(document.body).on('click', '.activity_view', function () {
                var fname = $(this).data('actid');
                viewActDoc(fname);
            });
            $('.discard_message').click(function (e) {
                swal({
                    title: "Are you sure?",
                    text: "The changes you made will be lost",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, discard it!",
                    closeOnConfirm: true
                }, function () {
                    $('#CaseEmailModal').find('.att-add-file').not(':first').remove();
                    $('#CaseEmailModal').modal("hide");
                });
            });
            $(document).on('click', '.ca-addfile', function () {
                $('.att-add-file:last').after('<label class="col-sm-2 control-label"></label><div class="col-sm-9 att-add-file marg-top5 clearfix"> <input type="file" name="afiles2[]" id="afiles2" multiple><i class="fa fa-plus-circle marg-top5 ca-addfile" aria-hidden="true"></i> <i class="fa fa-times marg-top5 removefile" aria-hidden="true"></i></div>');
            });
            $(document).on('click', '.removefile', function () {
                $(this).closest('.att-add-file').prev('label').remove();
                $(this).closest('.att-add-file').remove();
            });
            function viewActDoc(fname) {
                var caseNo = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/viewcaseactdoc',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        filename: fname,
                        caseno: caseNo
                    },
                    success: function (data) {
                    }
                });
            }

            function calcAge(dtFrom, dtTo) {
                var a = dtTo.getDate() + (dtTo.getMonth() + (dtTo.getFullYear() - 1700) * 16) * 32;
                var b = dtFrom.getDate() + (dtFrom.getMonth() + (dtFrom.getFullYear() - 1700) * 16) * 32;
                var x = Math.floor((a - b) / 32 / 16);
                return x < 0 ? null : x;
            }

            $(document.body).on('change', '#entry_date', function () {
                var array = [];
                var array1 = [];
                var option = $('#entry_date').val();
                if (option == 2) {
                    for (var i = 0; i < 14; i++) {
                        var d = moment($('#caldate').val()).add(i, 'days');
                        var d1 = d._d;
                        array.push(moment(d1).format('M/D/YYYY ,dddd'));
                        array1.push(moment(d1).format('YYYY-M-D'));
                    }
                } else if (option == 3) {
                    for (var i = 0; i < 30; i++) {
                        var d = moment($('#caldate').val()).add(i, 'days');
                        var d1 = d._d;
                        array.push(moment(d1).format('M/D/YYYY ,dddd'));
                        array1.push(moment(d1).format('YYYY-M-D'));
                    }
                } else if (option == 4) {
                    for (var i = 0; i < 4; i++) {
                        var d = moment($('#caldate').val()).add(i, 'weeks');
                        var d1 = d._d;
                        array.push(moment(d1).format('M/D/YYYY ,dddd'));
                        array1.push(moment(d1).format('YYYY-M-D'));
                    }
                } else if (option == 5) {
                    for (var i = 0; i < 24; i++) {
                        var d = moment($('#caldate').val()).add(i, 'weeks');
                        var d1 = d._d;
                        array.push(moment(d1).format('M/D/YYYY ,dddd'));
                        array1.push(moment(d1).format('YYYY-M-D'));
                    }
                } else if (option == 6) {
                    for (var i = 0; i < 52; i++) {
                        var d = moment($('#caldate').val()).add(i, 'weeks');
                        var d1 = d._d;
                        array.push(moment(d1).format('M/D/YYYY ,dddd'));
                        array1.push(moment(d1).format('YYYY-M-D'));
                    }
                } else if (option == 7) {
                    for (var i = 0; i < 6; i++) {
                        var d = moment($('#caldate').val()).add(i, 'month');
                        var d1 = d._d;
                        array.push(moment(d1).format('M/D/YYYY ,dddd'));
                        array1.push(moment(d1).format('YYYY-M-D'));
                    }
                } else if (option == 8) {
                    for (var i = 0; i < 12; i++) {
                        var d = moment($('#caldate').val()).add(i, 'month');
                        var d1 = d._d;
                        array.push(moment(d1).format('M/D/YYYY ,dddd'));
                        array1.push(moment(d1).format('YYYY-M-D'));
                    }
                } else
                {
                    array.push(moment($('#caldate').val()).format('M/D/YYYY , dddd'));
                    array1.push(moment(d1).format('YYYY-M-D'));
                }
                $('#Date_copy').text('');
                $('#startend1').val(array1);
                for (var i = 0; i < array.length; i++) {
                    $('#Date_copy').append(array[i] + '\n');
                }
            });

            $('.autorecover').on('click', function () {
                swal({
                    title: 'Recover Notes',
                    text: 'Notes are being saved: Every 60 seconds.\nYou are requesting to restore notes that were automatically saved.\nAre you sure this is what you want to do?',
                    showCancelButton: true,
                    confirmButtonColor: "#1ab394",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        swal.close();
                        $('textarea#activity_event').val(localStorage.getItem('autorecoverevent'));
                    }
                });
            });


            $('#addcaseact').on('shown.bs.modal', function (e) {
                var d = new Date();
                var hours = d.getHours();
                var minutes = d.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                var currenttime = d;

                if(d.getHours() > 12) {
                    d.setHours(d.getHours() - 12);
                }
                if(d.getHours() < 10) {
                    hours = '0' + d.getHours();
                } else {
                    hours = d.getHours();
                }
                if(d.getMinutes() < 10) {
                    minutes = '0' + d.getMinutes();
                } else {
                    minutes = d.getMinutes();
                }
                var currenttime = hours+":"+minutes+" "+ampm;
                if($('#add_case_time').val() == '') {
                    $('#add_case_time').val(currenttime);
                }
                notesLocalStorageTimer = setInterval(function () {
                    var eval = $('textarea#activity_event').val();
                    if (eval)
                        localStorage.setItem('autorecoverevent', eval);
                }, 60000);

                $('.dz-success-mark').css('display','none');
                $('.dz-error-mark').css('display','none');
                $('.dropzone.dz-started .dz-message').css('opacity','1');
            });
            
            $('#eamslist').on('shown.bs.modal', function (e) {
                var eams = $('input[name="card2_eamsref"]').val();
                if(eams && eams != 0){
                    $('#eamsrefDetach').prop('disabled', false);
                }
                else{
                    $('#eamsrefDetach').prop('disabled', true);
                }
                eamsData.columns.adjust();
            });
            
            $('#eamslist').on('hide.bs.modal', function (e) {
                $('.input-sm').val('');
                eamsData.search('').draw(); 
                eamsData.ajax.reload();
            });
            
            $('#addcaseact').on('hide.bs.modal', function (e) {
                $("#activity_mainnotes").iCheck('uncheck');
                $("#activity_redalert").iCheck('uncheck');
                $("#activity_upontop").iCheck('uncheck');
                $("#activity_override").iCheck('uncheck');
                $('.select2Class#case_category').val('1').trigger("change");
                /*$('input[name=activity_no]').val('');*/
                $( ".dz-preview" ).remove();
                clearInterval(notesLocalStorageTimer);
            });

            $(document.body).on('blur', 'input[name=card2_state]', function () {
                var add1 = $('#street_number').val();
                var add2 = $('#route').val();
                var city = $('#locality').val();
                var state = $(this).val();

                if (state != '') {
                    rolodexSearch.clear();
                    $("#firmcompanylist").attr("style", "z-index:9999 !important");
                    $('#firmcompanylist').modal('show');
                    rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                        processing: true,
                        stateSave: true,
                        searching : true,
                        stateSaveParams: function (settings, data) {
                            data.search.search = "";
                            data.start = 0;
                            delete data.order;
                            data.order = [0, "ASC"];
                        },
                        rolodexsearchAddress: true,
                        serverSide: true,
                        bFilter: false,
                        scrollX: true,
                        sScrollY: 400,
                        bScrollCollapse: true,
                        destroy: true,
                        autoWidth: true,
                        "lengthMenu": [5, 10, 25, 50, 100],
                        order: [[0, "ASC"]],
                        ajax: {
                            url: '<?php echo base_url(); ?>cases/searchForRolodexAddress',
                            method: 'POST',
                            data: {add1: add1, add2: add2, city: city, state: state},
                        },
                        fnRowCallback: function (nRow, data) {
                            $(nRow).attr("cardcode", '' + data[8]);
                            $(nRow).addClass('viewsearchPartiesDetail1');
                        },
                        fnDrawCallback: function (oSettings) {
                        },
                        initComplete: function (settings, json) {

                        },
                        select: {
                            style: 'single'
                        }
                    });
                }
            });
                                                        
$(document.body).on('click', '#eamsrefDetach', function () {
    var caseNo = $("#caseNo").val();
    var cardCode = $('#caseCardCode').val();
    $('#eamslist').modal('hide');
    swal({
        title: 'EAMS',
        text: 'Are you sure you want to detach any EAMS uniform references to this rolodex card and use defaults?',
        showCancelButton: true,
        confirmButtonColor: "#1ab394",
        confirmButtonText: "Yes",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
            swal.close();
            $.ajax({
                url: '<?php echo base_url(); ?>eams_lookup/eamsrefDetached',
                method: 'POST',
                dataType: 'json',
                data: {
                    caseno: caseNo,cardCode:cardCode
                },
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (data) {
                    $('input[name="card2_eamsref"]').val('');
                    $('input[name="eamsref_name"]').val('');
                   
                }
            });
        }
    });
    
    
    });
    
function setPartyViewDataEmpty() {
            $('.edit-party').attr('data-casecard', '');
            $('.edit-party').attr('data-case', '');
            $('#party_name').html('');
            $('#party_name').data('cardcodeWarning', '');
            $('#party_type').data('cardcodeWarning', '');
            $('#partiesCardCode').val('');
            $('#party_type').html('');
            $('#firm_name').html('');
            $('.addressVal').attr('href', "");
            $('#party_address1').html("");
            $('#party_address2').html("");
            $('#party_city').html("");
            $('#party_phn_home').html("");
            $('#party_phn_office').html("");
            $('#party_fax').html("");
            $('#party_email').html("");
            $('#party_side').html("");
            $('#party_offc_no').html("");
            $('#party_notes').html('');
            $('#party_eams').html('');
            $('#party_cell').html('');
            $('#party_beeper').html('');
            $('#party_notes').html('');
            $('.busComment').css({'visibility': 'hidden'});
            $('#comment_bus').html('');
            $('.busComment').css({'visibility': 'hidden'});
            $('#comment_per').html('');
        }

    $(document).on('click', '.delete_attach', function() {
        caseNo = $('#caseActNo').val();
        actNo = $('#activity_no').val();
        fileName = $('#caseact_uploaddocDefault').val();
        $.ajax({
            url: '<?php echo base_url(); ?>cases/remove_case_activity_attachment',
            method: 'POST',
            dataType: 'json',
            data: {
                caseNo: caseNo,
                actNo: actNo,
                fileName: fileName
            },
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (data) {
                if(data.status == 1) {
                    $('.caseActUploadDocument').css('display', 'none');
                    $('#caseact_uploaddocDefault').val('');
                    $('#caseActUploadDocSpan').val('');
                    $('#uploadFile3').html('Upload');
                    $('#caseActUploadDoc').attr('href', '');
                } else {
                    swal("Failure !", "Can't remove the Document! Try again", "error");
                }
            }
        });
    });
</script>