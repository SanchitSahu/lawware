<?php
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '')
            unset($holodaylist[$key]);
    }
}
if (isset($staff_details)) {
    $Uservacation = isset($this->data['staff_details'][0]->vacation) ? (explode("##", trim($this->data['staff_details'][0]->vacation))) : '';
    if (!empty($Uservacation)) {
        $Uservacation = array_map('trim', $Uservacation);
        foreach ($Uservacation as $key => $value) {
            if (is_null($value) || $value == '')
                unset($Uservacation[$key]);
        }
    }
}
?>
<style>
    .chosen-container {
        width: 100% !important;
    }
    .pac-container{z-index: 9999;}
    #eamslist{
        z-index: 9999 !important;
    }

    #addcase .modal-body{
        padding: 20px 30px 10px;
    }
.highlight_new {
    background-color: #4d7ab1 !important;
    color: #fff !important;
}
</style>
<link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
 <link href="assets/css/plugins/iCheck/icheck.css" rel="stylesheet">
 <script src="<?php echo base_url('assets'); ?>/js/plugins/ckeditor/ckeditor.js"></script>
<!-- new partyemail -->
<div id="partyemail" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <!-- <link href="assets/css/plugins/iCheck/icheck.css" rel="stylesheet"> -->
    <!-- <script src="assets/js/default.js" > </script> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Find Names in the Rolodex to Send Email </h4>
                <input type="hidden" id="staff_list_email">            
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="" role="form" name="rolodex_search_parties_form_for_partyemail" id="rolodex_search_parties_form_for_partyemail">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins marg-bot5">
                                <div class="ibox-title collapse-link">
                                    <h5 class="marg-top5">What you want to search for</h5>
                                    <div class="pull-right btn btn-primary btn-sm">Filters
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                </div>
                                <div class="ibox-content" style="display:none">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="rolodex_search_text_for_partyemail" id="rolodex_search_text_for_partyemail">
                                    </div>
                                    <div class="row">
                                            <label id="rolodex_search_parties-error_for_partyemail1" class="error" for="rolodex_search_parties_for_partyemail1"></label>
                                        </div> 
                                    <div class="form-group">
                                        <label>Search By</label>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-firm" name="rolodex_search_parties_for_partyemail" id="name_firm"> <i></i> Name or Firm </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-social_sec" name="rolodex_search_parties_for_partyemail" id="ss_no"> <i></i> SS No </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-first" name="rolodex_search_parties_for_partyemail" id="firstname"> <i></i> First Name </label></div>
                                            </div>
                                          </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-first-last" name="rolodex_search_parties_for_partyemail" id="lastname"> <i></i> Name (last,first) </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-email" name="rolodex_search_parties_for_partyemail" id="email_rolodex"> <i></i> Email </label></div>
                                            </div>
                                            <!-- <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-details" name="rolodex_search_parties_for_partyemail" id="details"> <i></i> Details </label></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-company" name="rolodex_search_parties_for_partyemail" id="firm_company"> <i></i> Firm/Company </label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-licenseno" name="rolodex_search_parties_for_partyemail" id="license_no"> <i></i> License No </label></div>
                                            </div>
                                            <!-- <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-relational" name="rolodex_search_parties_for_partyemail" id="relational"> <i></i> Relational </label></div>
                                            </div> -->
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-cardno" name="rolodex_search_parties_for_partyemail" id="card_number"> <i></i> Card Number </label></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="i-checks"><label> <input type="radio" value="rolodex-type" name="rolodex_search_parties_for_partyemail" id="type_rolodex"> <i></i> Type </label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label id="rolodex_search_parties-error_for_partyemail" class="error" for="rolodex_search_parties_for_partyemail"></label>
                                        </div> </div>
                                    <div class="form-group no-bottom-margin">
                                        <input type="button" class="btn btn-primary search_party_email " id="search_party_email" value="Search">
                                    </div>
                                </div>
                            </div>
                            <div class="float-e-margins marg-bot5">
                                <div class="ibox-content">
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-responsive table-bordered table-hover table-striped dataTables-example_for_partyemail" id="rolodexPartiesResult_for_partyemail">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Firm/Company</th>
                                                        <th>Type</th>
                                                        <th>City</th>
                                                        <th>Address</th>
                                                        <th>Zip</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6 hidden">
                                            <input type="hidden" id="searched_party_fullname_for_partyemail">
                                            <input type="hidden" id="searched_party_firmname_for_partyemail">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-sm-12 text-center">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary addSearchedParties_for_partyemail">Proceed</button>
                            <button data-toggle="modal" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- for partyemail new model -->
<div id="partyemailproceed" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Write mail</h4>
            </div>
            <div class="modal-body">
                <div class="ibox-content marg-bot15">
                    <form class="form-horizontal" id="send_mail_form1_partyemail"  name="send_mail_form1_partyemail" method="post" enctype="multipart/form-data">
                        <div class="form-group"><label class="col-sm-2 control-label">Internal User:</label>
                            <div class="col-sm-10">
                                <select name="whoto1_partyemail" id="whoto1_partyemail" style="width:100%" multiple  placeholder="To" class="form-control select2Class">
                                    <option value=""> Select User to Send Email </option>
                                    <?php
                                    if (isset($staff_list)) {
                                        foreach ($staff_list as $staff) {
                                            ?>
                                            <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">External Parties:</label>
                            <div class="col-sm-10">
                                <input type="text" name="emails1_partyemail" id="emails1_partyemail" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Subject :</label>
                            <div class="col-sm-10">
                                <input type="text" name="mail_subject1_partyemail" id="mail_subject1_partyemail" class="form-control">
                                <input type="hidden" name="filenameVal1_partyemail" id="filenameVal1_partyemail">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group inline-block marg-right40 col-sm-3" align="center">
                                <label style="margin-left:15px !important;">Urgent</label>
                                <input type="checkbox" class="js-switch" class="margin-right10" sty name="mail_urgent1_partyemail" id="mail_urgent1_partyemail"  />
                            </div>
                            <!-- <div class="form-inline col-sm-5" align="center">
                                <label class="marg-right10">Case No:</label>
                                <input type="text" name='caseno1' id="case_no1" class="form-control" readonly value="<?php echo $display_details->caseno; ?>">
                            </div> -->
                            <!-- <div class="form-inline col-sm-4 pull-right">
                                <select id="case_category1_partyemail" name="case_category1_partyemail" class="form-control select2Class">
                                    <?php foreach ($caseactcategory as $k => $v) { ?>
                                        <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                    <?php } ?>
                                </select>
                            </div> -->
                        </div>
                        <div class=" form-group mail-text h-200" style="padding-right:10px; padding-left:10px;">
                            <textarea id="mailbody1_partyemail"  name="mailbody1_partyemail"></textarea>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group text-right">
                            <div class="col-sm-8 text-left">
                                <!-- <label class="col-sm-2 control-label">Attachment :</label> -->
                                <div id="selectedloader1"></div>
                                <div id="selectedFiles1"></div>
                                <div class="attatchmentset" style="display: inline-block !important;width: 100% !important;overflow: hidden !important;overflow-y: auto !important;">
                                    <div class="col-sm-12 att-add-file marg-top5"> <input type="file" name="afiles3_partyemail[]" id="afiles3_partyemail" multiple > <!-- <i class="fa fa-plus-circle marg-top5 ca-addfile" aria-hidden="true"></i> --></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send"><i class="fa fa-reply"></i> Send</button>
                                <a href="javascript:;" class="btn btn-danger btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                            </div>
                        </div>
                        <div class="col-sm-12 supported-files"><b>Note: </b> File size upload upto 20 MB <b>Supported Files :</b> PNG, JPG, JPEG, BMP,TXT, ZIP,RAR,DOC,DOCX,PDF,XLS,XLSX,PPT,PTTX </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="totalFileSize_partyemail" id="totalFileSize_partyemail" value="0">
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>

<!-- Rolodex add/edit popup START-->
<div id="addcontact" class="modal fade" data-backdrop="static" data-keyboard="false" style="z-index: 9999 !important;" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Contact</h4>
            </div>
            <div class="modal-body">
                <form id="addRolodexForm" method="post" enctype="multipart/form-data">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#name">Name</a></li>
                            <li class=""><a data-toggle="tab" href="#contact">Telephone/Email</a></li>
                            <li class=""><a data-toggle="tab" href="#details">Details</a></li>
                            <li class=""><a data-toggle="tab" href="#comments">Comments</a></li>
                            <li class=""><a data-toggle="tab" href="#address">Addresses</a></li>
                            <li class=""><a data-toggle="tab" href="#profile">Profile</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="name" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>General Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Salutation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input name="form_submit" type="hidden" value="1">
                                                            <select name="card_salutation" id="card_salutation" class="form-control <?php
                                                            if (isset($system_data[0]->cdsaldd) && $system_data[0]->cdsaldd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" >
                                                                <option value="">Select Salutation</option>
                                                                <?php foreach ($saluatation as $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <!--div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label>First</label>
                                                                <input type="text" id="card_first" name="card_first" class="form-control" value="" />
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label>Middle</label>
                                                                <input type="text" name="card_middle" class="form-control" value=""  />
                                                            </div>
                                                        </div>
                                                    </div-->
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">First <span style="color:red">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="card_first" name="card_first" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Middle</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_middle" class="form-control" value=""  autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last <span style="color:red">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_last" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Suffix</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_suffix" id="card_suffix" class="form-control <?php
                                                            if (isset($system_data[0]->cdsufdd) && $system_data[0]->cdsufdd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>">
                                                                <option value="" disabled="disabed">Select Suffix</option>
                                                                <?php
                                                                if (isset($suffix)) {
                                                                    foreach ($suffix as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Title</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_title" class="form-control select2Class" >
                                                                <option value="">Select Title</option>
                                                                <?php
                                                                if (isset($title)) {
                                                                    foreach ($title as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Category <span style="color:red">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select placeholder="Select Category" name="card_type" id="card_type" class="form-control <?php
                                                            if (isset($system_data[0]->cardtypedd) && $system_data[0]->cardtypedd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" required>
                                                                        <?php
                                                                        if (isset($category)) {
                                                                            foreach ($category as $val) {
                                                                                ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Popular</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_popular" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Sal for Ltr <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="This should normally be left empty.  However, you may sometimes decide to fill this field in to override the default salutation in form letters.  For example : To the honorable Judge Smith" id="sfl"></i></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_letsal" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Firm/Corp </label>
                                                        <div class="col-sm-6 no-padding marg-bot5">
                                                            <input type="text" name="card2_firm" class="form-control" value="" autocomplete="off"/>
                                                        </div>
                                                        <div class="col-sm-1 no-padding">
                                                            <a href="#" class="btn btn-md btn-primary pull-right rolodex-company">A</a>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address Lookup</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input id="autocomplete" placeholder="Enter your address" class="form-control" onFocus="geolocate()" type="text">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address 1</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_address1" id="street_number" class="form-control" value="" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">

                                                        <label class="col-sm-5 no-left-padding">City</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="locality" name="card2_city" class="form-control" value="" />
                                                        </div>


                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Venue</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
<!--                                                        <input type="text" name="card2_venue" class="form-control" />-->
                                                            <select class="form-control cstmdrp cstmdrp-placeholder" name="card2_venue" placeholder="Select Venue">
                                                                <option value="" disabled>Select Venue</option>
                                                                <option value=""></option>
                                                                <?php
                                                                if (isset($Venue)) {
                                                                    foreach ($Venue as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">

                                                            <div class="col-lg-12">
                                                                <a class="btn pull-right btn-primary btn-sm" href="#eamslist" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Add Eamsref" data-keyboard="false">EAMS</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding">EAMS Name</label>
                                                        <div class="col-sm-8 no-padding marg-bot5">
                                                            <input type="hidden" name="card2_eamsref">
                                                            <input type="text" id="eamsref_name" name="eamsref_name" class="form-control" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding">Address 2</label>
                                                        <div class="col-sm-8 no-padding marg-bot5">
                                                            <input type="text" name="card2_address2" id="route" class="form-control" value="" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding">State</label>
                                                        <div class="col-sm-8 no-padding marg-bot5">
                                                            <input type="text" id="administrative_area_level_1" name="card2_state" class="form-control" value="" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 no-left-padding">Zip</label>
                                                        <div class="col-sm-8 no-padding marg-bot5">
                                                            <input type="text" id="postal_code" name="card2_zip" class="form-control" value="" />
                                                        </div></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="contact" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Personal Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Business</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_business" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div><div class="clearfix"></div>
                                                    <!--div class="form-group">
                                                        <label>Beeper</label>
                                                        <input type="text" name="card_beeper" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Home</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_home" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Cell</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_car" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Email</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_email" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Note</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_notes" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Business Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Telephone 1</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone1" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Telephone 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone2" placeholder="(xxx) xxx-xxxx" class="form-control contact"  autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Tax ID</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_tax_id" class="form-control" autocomplete="off"/><!-- onlyNumbers -->
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax2" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="details" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Contact Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">SS No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_social_sec" class="form-control social_sec"  placeholder="234-56-7898" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date of Birth</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_birth_date" pattern="\d{1,2}/\d{1,2}/\d{4}"  class="form-control datepickerClass date-field weekendsalert" data-mask="99/99/9999" placeholder="mm/dd/YY ex: 03/25/1995" id="datepicker" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">License No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_licenseno" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Specialty</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">

                                                            <select name="card_specialty" class="form-control select2Class">
                                                                <option value="">Select Specialty</option>
                                                                <?php
                                                                if (isset($specialty)) {
                                                                    foreach ($specialty as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Maidan Name</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_mothermaid" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Card No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_cardcode" readonly class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Firm No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_firmcode" readonly class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Interpreter Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Interpreter</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_interpret" class="form-control select2Class">
                                                                <option value="">Select Interpreter</option>
                                                                <option value="Y">Yes</option>
                                                                <option value="N">No</option>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group" id="data_1">
                                                        <label class="col-sm-5 no-left-padding">Language</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_language" class="select2Class form-control">
                                                                <!-- <option value="">Select Language</option>-->
                                                                <?php foreach ($languageList as $key => $val) { ?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                                                                <?php } ?>
                                                            </select>
        <!--                                                    <input type="text" name="card_language" class="form-control" />-->
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="float-e-margins">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="">Original</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime1" readonly data-date-format="mm/dd/yyyy - HH:ii p"  autocomplete="off">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Last</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime2" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Original2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime3" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Last2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime4" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="comments" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card2_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="address" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Alternate Name and Addresses</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address1</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing1" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address2</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing2" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address3</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing3" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address4</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing4" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="profile" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Occupation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_occupation" class="form-control" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Employer</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_employer" class="form-control" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date of Hire</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="date_of_hire" pattern="\d{1,2}/\d{1,2}/\d{4}"  class="form-control datepickerClass date-field weekendsalert" data-mask="99/99/9999" placeholder="mm/dd/YY ex: 03/25/1995" id="datepicker" autocomplete="off" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Profile Picture</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="file" name="profilepic" id="profilepic" class="form-control" accept="image/*" />
                                                            <span style="color:red">Max Upload size 5Mb</span>
                                                            <input type="hidden" name="hiddenimg" id="hiddenimg" value="" >
                                                            <span id="imgerror" style="color:red"></span>

                                                            <div id="profilepicDisp" style="display:none">
                                                                <div class="form-group">
                                                                    <img id="dispProfile" height="50" width="50">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15 text-right">
                        <input type="hidden" id="new_case" name="new_case" />
                        <input type="hidden" id="prospect_id" name="prospect_id" />
                        <input type="hidden" id="newRolodexCard" name="newRolodexCard" value="0"/>
                        <input type="hidden" id="oldCardValue" name="oldCardValue" value="0"/>
                        <input type="hidden" id="oldCaseNo" name="oldCaseNo" value="0"/>
                        <button type="button" id="addContact" class="btn btn-md btn-primary m-r-xs">Save</button>
                        <button type="button" id="cancelContact" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Rolodex add/edit popup END-->


<!-- Add Case modal START -->
<div id="addcase" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Case</h4>
            </div>
            <div class="modal-body">
                <form id="addNewCase" method="post">
                    <div class="form-group row">
                        <label class="col-sm-3 m-t-xs">First Name <span style="color:red">*</span></label>
                        <div class="col-sm-9"><input type="text" name="first_name" id="first_name" class="form-control marg-bot0" autocomplete="off"/>
                        </div></div>
                    <div class="form-group row">
                        <label class="col-sm-3 m-t-xs">Last Name  <span style="color:red">*</span></label>
                        <div class="col-sm-9"><input type="text" name="last_name" id="last_name" class="form-control marg-bot0" autocomplete="off" />
                        </div></div>
                    <div class="form-group text-center marg-top15 text-right">
                        <input type="hidden" name="prospect_id" id="prospect_id" class="form-control" />

                        <input type="submit" value="Proceed" class="btn btn-md btn-primary marg-bot0 m-r-xs" data-toggle="modal">
                        <!--<a class="btn btn-md btn-primary" href="#addcontact" data-toggle="modal" data-dismiss="modal">Proceed</a>-->
                        <input type="button" value="Cancel" data-dismiss="modal" class="btn btn-md btn-primary btn-danger marg-bot0">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add Case modal END -->

<?php
$session_Arr = $this->session->userdata('user_data');
?>

<div id="duplicateData" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Possible Duplicate</h4>
            </div>
            <form method="post" name="parties_case_specific" id="parties_case_specific">
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <table id="duplicateEntries" class="table table-bordered table-hover table-striped dataTables-example" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>Date Entered</th>
                                                <th>SS No</th>
                                                <th>Card Type</th>
                                                <th>Caption</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input id="last_name" name="last_name" type="hidden" class="last_name" value="">
                    <input id="first_name" name="first_name" type="hidden" class="first_name"  value="">
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <div class="">

                            <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $this->uri->segment(3); ?>">
                            <input id="duplicateCardCode" name="duplicateCardCode" type="hidden" value="">
                            <button id="add_name_case" type="submit" class="btn btn-primary">Add Name & Case</button>
                            <button id="attach_name" type="button" class="btn btn-primary" disabled>Attach Name</button>
                            <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                            <button id="edit_new_case" type="button" class="btn btn-primary pull-right" disabled>Edit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- start firm companylist  -->
<div id="firmcompanylist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md firmcompanylist">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Firm Company List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="form-group">
                    <form action="#" role="form" method="post" id="companylistForm" name="companylistForm">
                        <label>Enter Firm or Company Name</label>
                        <input type="text" class="form-control" name="rolodex_search_text_name" id="rolodex_search_text_name"/>
                        <input type="hidden" class="form-control" name="rolodex_search_parties_name" id="rolodex_search_parties_name" value="rolodex-company" />
                    </form>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped dataTables-example" id="rolodexsearchResult_data">
                                    <thead>
                                        <tr>
                                            <th>Firm/Company</th>
                                            <th>City</th>
                                            <th>Address</th>
                                            <th>Address2</th>
                                            <th>Phone</th>
                                            <th>Name</th>
                                            <th>State</th>
                                            <th>Zip</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="viewSearchedPartieDetails" >Attach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>
<!---  end firmcompanylist --->

<div id="partiesNameChangeWarning" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <span>If you make changes in the rolodex, it may affect several cases.</span>
                                <ul class="no-left-padding marg-top15">
                                    <li>
                                        Click Yes <span class="marg-left35">To make changes for this case only.</span>
                                    </li>
                                    <li>
                                        Click No <span class="marg-left35">To make changes in the rolodex which may affect several cases.</span>
                                    </li>
                                    <li>
                                        Click Cancel <span class="marg-left15">To cancel and not make changes at all.</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <input id="caseNo" name="caseNo" type="hidden">
                        <input id="caseCardCode" type="hidden">
                        <button id="change_name_warning" type="button" class="btn btn-primary">Yes</button>
                        <button id="change_name_warning_no" type="button" class="btn btn-primary">No</button>
                        <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="copyemail">
    <textarea id="copytoemail" STYLE="display:none;">
    </textarea>
</div>

<div id="eamslist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md eamsreflist">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EAMS List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-eamsLookup" >
                                <thead>
                                    <tr>
                                        <th>Eamsref</th>
                                        <th>Name</th>
                                        <th>Add1</th>
                                        <th>Add2</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Zip</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="eamsrefDetails" >Attach</button>
                    <button type="button" class="btn btn-md btn-primary" id="eamsrefDetach" >Detach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('js/casejs.php'); ?>
<script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>
<script type="text/javascript">
                                                                var uri_seg = '<?php echo (isset($url_seg)) ? $url_seg : ''; ?>';
                                                                var selectedArray = [];
                                                                var rolodexSearchDatatable = '';
                                                                var rolodexSearchDatatableforpartyemail = '';
                                                                var rolodexSearch = '';
                                                                var rolodexSearchforpartyemail = '';
                                                                var categoryeventlist = '';
                                                                var casepartylist_data = '';
                                                                var CaseHID = $("#hdnCaseId").val();
                                                                var ProspectListDatatable;
                                                                var eamsData = '';
                                                                var searchBy1 = {'prosno':<?php echo isset($record[0]->proskey) ? $record[0]->proskey : 'null'; ?>};

                                                                $(document).ready(function () {
                                                                    $('.datepickerClass').datetimepicker({
                                                                        format: 'mm/dd/yyyy',
                                                                        minView: 2,
                                                                        autoclose: true,
                                                                        endDate: new Date()
                                                                    });
                                                                    $('#addcontact.modal').on('hidden.bs.modal', function () {
                                                                        $('#addRolodexForm')[0].reset();
                                                                    });
                                                                    duplicateDatatable = $('#duplicateEntries').DataTable();
                                                                    eamsData = $('.dataTables-eamsLookup').DataTable({
                                                                        "processing": true,
                                                                        "serverSide": true,
                                                                        stateSave: true,
                                                                                    stateSaveParams: function (settings, data) {
                                                                                            data.search.search = "";
                                                                                            data.start = 0;
                                                                                            delete data.order;
                                                                                    },
                                                                        "bFilter": true,
                                                                        "sScrollX": "100%",
                                                                        "sScrollXInner": "150%",
                                                                        "ajax": {
                                                                            url: "<?php echo base_url(); ?>eams_lookup/getEamsAjaxData",
                                                                            type: "POST"
                                                                        }
                                                                    });
        
                                                                    function editContact(cardCode) {
                                                                        $.blockUI();

                                                                        $.ajax({
                                                                            url: "<?php echo base_url() . "rolodex/getEditRolodex" ?>",
                                                                            method: "POST",
                                                                            data: {'cardcode': cardCode},
                                                                            success: function (result) {
                                                                                var data = $.parseJSON(result);
                                                                                var notes_comments = '';
                                                                                if (data.card_comments) {
                                                                                    if(isJson(data.card_comments)) {
                                                                                        notes_comments = $.parseJSON(data.card_comments);
                                                                                    } else {
                                                                                        notes_comments = data.card_comments;
                                                                                    }
                                                                                }
                                                                                $('#addcontact .modal-title').html('Edit Contact');
                                                                                $('#card_salutation').val(data.salutation);
                                                                                $('#card_suffix').val(data.suffix);
                                                                                $('select[name=card_title]').val(data.title);
                                                                                $('#card_type').val(data.type);
                                                                                $('input[name=card_first]').val(data.first);
                                                                                $('input[name=card_middle]').val(data.middle);
                                                                                $('input[name=card_last]').val(data.last);
                                                                                $('input[name=card_popular]').val(data.popular);
                                                                                $('input[name=card_letsal]').val(data.letsal);
                                                                                $('input[name=card_occupation]').val(data.occupation);
                                                                                $('input[name=card_employer]').val(data.employer);
                                                                                if (data.date_of_hire == 'NA') {
                                                                                    $('input[name=date_of_hire]').val('');
                                                                                } else {
                                                                                    $('input[name=date_of_hire]').val(moment(data.date_of_hire).format('MM/DD/YYYY'));
                                                                                }
                                                                                $('input[name=hiddenimg]').val(data.picture);
                                                                                if (data.picture)
                                                                                {
                                                                                    document.getElementById('profilepicDisp').style.display = 'block';
                                                                                    document.getElementById('dispProfile').src = '<?php echo base_url() ?>assets/rolodexprofileimages/' + data.picture;
                                                                                } else
                                                                                {
                                                                                    document.getElementById('dispProfile').src = '';
                                                                                }
                                                                                $('input[name=card2_firm]').val(data.firm);
                                                                                $('input[name=card2_address1]').val(data.address1);
                                                                                $('input[name=card2_address2]').val(data.address2);
                                                                                $('input[name=card2_city]').val(data.city);
                                                                                $('input[name=card2_state]').val(data.state);
                                                                                $('input[name=card2_zip]').val(data.zip);
                                                                                $('input[name=card2_venue]').val(data.venue);
                                                                                $('input[name=card2_eamsref]').val(data.eamsref);
                                                                                $('input[name=eamsref_name]').val(data.eamsname);
                                                                                $('input[name=card_business]').val(data.business);
                                                                                $('input[name=card_fax]').val(data.card_fax);
                                                                                $('input[name=card_home]').val(data.home);
                                                                                $('input[name=card_car]').val(data.car);
                                                                                $('input[name=card_email]').val(data.email);
                                                                                if (notes_comments != '')
                                                                                    $('input[name=card_notes]').val(notes_comments[0].notes);
                                                                                else
                                                                                    $('input[name=card_notes]').val();
                                                                                $('input[name=card2_phone1]').val(data.phone1);
                                                                                $('input[name=card2_phone2]').val(data.phone2);
                                                                                $('input[name=card2_tax_id]').val(data.tax_id);
                                                                                $('input[name=card2_fax]').val(data.card2_fax);
                                                                                $('input[name=card2_fax2]').val(data.fax2);
                                                                                $('input[name=card_social_sec]').val(data.social_sec);
                                                                                if (data.birth_date == 'NA') {
                                                                                    $('input[name=card_birth_date]').val('');
                                                                                } else {
                                                                                    $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                                                                                }

                                                                                $('input[name=card_licenseno]').val(data.licenseno);
                                                                                $('select[name=card_specialty]').val(data.specialty);
                                                                                $('input[name=card_mothermaid]').val(data.mothermaid);
                                                                                $('input[name=card_cardcode]').val(data.cardcode);
                                                                                $('input[name=card_firmcode]').val(data.firmcode);
                                                                                $('select[name=card_interpret]').val(data.interpret);
                                                                                $('select[name=card_language]').val(data.language);

                                                                                $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                                                                                $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                                                                                $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                                                                                $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                                                                                if (notes_comments != '')
                                                                                    $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                                                                else
                                                                                    $('textarea[name=card_comments]').val();
                                                                                $('textarea[name=card2_comments]').val(data.card2_comments);
                                                                                $('textarea[name=card2_mailing1]').val(data.mailing1);
                                                                                $('textarea[name=card2_mailing2]').val(data.mailing2);
                                                                                $('textarea[name=card2_mailing3]').val(data.mailing3);
                                                                                $('textarea[name=card2_mailing4]').val(data.mailing4);



                                                                                $('#new_case').val('parties');
                                                                                $('input[name="card_cardcode"]').val(data.cardcode);

                                                                                $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                                $("#addcontact").modal('show');

                                                                                $("#addRolodexForm select").trigger('change');

                                                                                $.unblockUI();
                                                                            }
                                                                        });
                                                                    }
                                                                    $('#addcontact.modal').on('show.bs.modal', function () {
                                                                        document.getElementById('imgerror').innerHTML = '';
                                                                        document.getElementById('hiddenimg').value = '';

                                                                        $(".chosen-language").chosen();
                                                                    });

                                                                    $('#casedate_followup').datetimepicker({
                                                                        format: 'mm/dd/yyyy',
                                                                        minView: 2,
                                                                        autoclose: true,
                                                                        startDate: new Date()
                                                                    });
                                                                    /* CODE TO ADD PARTY IN DASHBOARD STARTS*/
                                                                    $('#firmcompanylist').on('show.bs.modal', function () {
                                                                        $(".dataTables-example").removeAttr("style");
                                                                        $(".dataTables_scrollHeadInner").removeAttr("style");
                                                                        $(".dataTables-example").css("width", "100% !important;");
                                                                        $(".dataTables_scrollHeadInner").css("width", "100% !important;");
                                                                    });
                                                                    $('#firmcompanylist').on('hide.bs.modal', function () {
                                                                        $('#rolodex_search_text_name-error').remove();
                                                                        $('#rolodex_search_text_name').removeClass('error');
                                                                    });
                                                                    $(document.body).on('click', '#viewSearchedPartieDetails', function () {

                                                                        var selectedrows = rolodexSearch.rows('.table-selected').data();
                                                                        var caseNo = $("#caseNo").val();
                                                                        if (selectedrows.length > 0) {
                                                                            $('#firmcompanylist').modal('hide');
                                                                            $.ajax({
                                                                                url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                                                                                method: 'POST',
                                                                                dataType: 'json',
                                                                                data: {
                                                                                    cardcode: selectedrows[0][8],
                                                                                    caseno: caseNo
                                                                                },
                                                                                beforeSend: function (xhr) {
                                                                                    $.blockUI();
                                                                                },
                                                                                complete: function (jqXHR, textStatus) {
                                                                                    $.unblockUI();
                                                                                },
                                                                                success: function (data) {
                                                                                    if (data == false) {
                                                                                        swal({
                                                                                            title: "Alert",
                                                                                            text: "Please attach the existing firm from the firm list to continue.",
                                                                                            type: "warning"
                                                                                        });
                                                                                    } else {
                                                                                        var city = data.city;
                                                                                        if (city != null)
                                                                                        {
                                                                                            var strarray = city.split(',');
                                                                                            $('input[name="card2_city"]').val($.trim(strarray[0]));
                                                                                            $('input[name="card2_state"]').val($.trim(strarray[1]));
                                                                                            $('input[name="card2_zip"]').val($.trim(strarray[2]));
                                                                                        }
                                                                                        $('input[name="card2_firm"]').val(data.firm);
                                                                                        $('input[name="card2_address1"]').val(data.address1);
                                                                                        $('input[name="card2_address2"]').val(data.address2);
                                                                                        $('input[name=card_firmcode]').val(data.firmcode);
                                                                                        /*$('input[name="card2_venue"]').val(data.venue);*/
                                                                                        $('.addSearchedParties').attr("disabled", false);
                                                                                        $('.editPartyCard').attr("disabled", false);
                                                                                    }
                                                                                }
                                                                            });
                                                                        } else {
                                                                            swal({
                                                                                title: "Alert",
                                                                                text: "Please attach the existing firm from the firm list to continue.",
                                                                                type: "warning"
                                                                            });
                                                                        }
                                                                    });
                                                                    $(document.body).on('click', '#attach_name', function () {
                                                                        var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                                                                        var caseno = $("#duplicateEntries .highlight").data('caseno');
                                                                        var first_name = $(".first_name").val();
                                                                        var last_name = $(".last_name").val();
                                                                        $.ajax({
                                                                            url: '<?php echo base_url(); ?>cases/attach_name',
                                                                            method: 'POST',
                                                                            dataType: 'json',
                                                                            data: {
                                                                                cardcode: cardcode,
                                                                                first_name: first_name,
                                                                                last_name: last_name,
                                                                                casetype: '<?php echo $system_data[0]->casetype; ?>',
                                                                                casestat: '<?php echo $system_data[0]->casestat; ?>',
                                                                                caseno: caseno

                                                                            },
                                                                            beforeSend: function (xhr) {
                                                                                $.blockUI();
                                                                            },
                                                                            complete: function (jqXHR, textStatus) {
                                                                                $.unblockUI();
                                                                            },
                                                                            success: function (result) {
                                                                                var url = "<?php echo base_url() ?>cases/case_details/" + result.caseno;
                                                                                $(location).attr('href', url);
                                                                            }
                                                                        });

                                                                    });

                                                                    $(document.body).on('click', '#edit_new_case', function () {
                                                                        /*$("#duplicateData").modal("hide");
                                                                         var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                                                                         editContact(cardcode);*/
                                                                        var cardCode = $('.highlight').data('cardcode');
                                                                        var caseno = $('.highlight').data('caseno');
                                                                        $('#caseCardCode').val(cardCode);
                                                                        $('#caseNo').val(caseno);
                                                                        $.ajax({
                                                                            url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                                                                            method: "POST",
                                                                            data: {'cardcode': cardCode},
                                                                            success: function (result) {
                                                                                if (result > 1) {
                                                                                    $("#partiesNameChangeWarning").modal("show");
                                                                                } else
                                                                                {
                                                                                    ChangeRolodex();
                                                                                }
                                                                            }
                                                                        });

                                                                    });


                                                                    $(document.body).on('click', '.viewPartiesDetail', function () {
                                                                        $("#rolodexPartiesResult tr").removeClass("highlight");
                                                                        $(this).addClass('highlight');
                                                                        var caseNo = $("#caseNo").val();
                                                                        $.ajax({
                                                                            url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                                                                            method: 'POST',
                                                                            dataType: 'json',
                                                                            data: {
                                                                                cardcode: $(this).data('cardcode'),
                                                                                caseno: caseNo
                                                                            },
                                                                            beforeSend: function (xhr) {
                                                                                $.blockUI();
                                                                            },
                                                                            complete: function (jqXHR, textStatus) {
                                                                                $.unblockUI();
                                                                            },
                                                                            success: function (data) {
                                                                                if (data == false) {
                                                                                    swal({
                                                                                        title: "Alert",
                                                                                        text: "No Party Selected.",
                                                                                        type: "warning"
                                                                                    });
                                                                                } else {
                                                                                    $("#searched_party_fullname").val(data.fullname);
                                                                                    $("#searched_party_firmname").val(data.firm);
                                                                                    $('#searched_party_type').html(data.type);
                                                                                    $('#searched_party_speciality').html(data.speciality);
                                                                                    $('#searched_party_address').html(data.address1);
                                                                                    $('#searched_party_city').html(data.city);
                                                                                    $('#searched_party_cardnumber').html(data.cardcode);
                                                                                    $('#searched_party_ssno').html(data.social_sec);
                                                                                    $('#searched_party_email').html(data.email);
                                                                                    $('#searched_party_licenseno').html(data.licenseno);
                                                                                    $('#searched_party_homephone').html(data.home);
                                                                                    $('#searched_party_business').html(data.business);
                                                                                    $('input[name=card_firmcode]').val(data.firmcode);
                                                                                    $('.addSearchedParties').attr("disabled", false);
                                                                                    $('.editPartyCard').attr("disabled", false);
                                                                                }
                                                                            }
                                                                        });



                                                                    });

                                                                    $("#companylistForm").validate({
                                                                        rules: {
                                                                            rolodex_search_text_name: {
                                                                                required: true,
                                                                                noBlankSpace: true
                                                                            }
                                                                        },
                                                                        messages: {
                                                                            rolodex_search_text_name: {
                                                                                required: "Please enter Firm/Company name "
                                                                            }
                                                                        },
                                                                        submitHandler: function (form, event) {
                                                                            event.preventDefault();
                                                                        }
                                                                    });

                                                                    rolodexSearchDatatable = $('#rolodexPartiesResult').DataTable({
                                                                        stateSave: true,
																		stateSaveParams: function (settings, data) {
																			data.search.search = "";
																			data.start = 0;
																			delete data.order;
																		},
                                                                        "lengthMenu": [5, 10, 25, 50, 100]
                                                                    });
                                                                    rolodexSearchDatatableforpartyemail = $('#rolodexPartiesResult_for_partyemail').DataTable({
                                                                        stateSave: true,
                                                                        responsive: true,
                                                                        stateSaveParams: function (settings, data) {
                                                                            data.search.search = "";
                                                                            data.start = 0;
                                                                            delete data.order;
                                                                        },
                                                                        "lengthMenu": [5, 10, 25, 50, 100],
                                                                        "scrollX": true,
                                                                    });
                                                                    rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                                                                        stateSave: true,
																		stateSaveParams: function (settings, data) {
																			data.search.search = "";
																			data.start = 0;
																			delete data.order;
																		},
                                                                        "scrollX": true,
                                                                        "lengthMenu": [5, 10, 25, 50, 100]
                                                                    });
                                                                    rolodexSearchforpartyemail = $('#rolodexsearchResult_data_for_partyemail').DataTable({
                                                                        stateSave: true,
                                                                        responsive: true,
																		stateSaveParams: function (settings, data) {
																			data.search.search = "";
																			data.start = 0;
																			delete data.order;
																		},
                                                                        "scrollX": true,
                                                                        "lengthMenu": [5, 10, 25, 50, 100]
                                                                    });

                                                                    $('#firmcompanylist').on('hidden.bs.modal', function () {
                                                                        $("#rolodex_search_text_name").val("");
                                                                        $('tr.table-selected').removeClass('table-selected');
                                                                    });
                                                                    $(document.body).on('click', '.rolodex-company', function () {
                                                                        $("#firmcompanylist").attr("style", "z-index:9999 !important");
                                                                        $("#rolodexsearchResult_data  tbody").remove();
                                                                        rolodexSearch.destroy();
                                                                        $('#firmcompanylist').modal('show');
                                                                        $('#rolodexsearchResult_data').DataTable().clear();
                                                                    });
                                                                    $(document.body).on('keyup', '#rolodex_search_text_name', function (e) {
                                                                        if (e.keyCode == '13') {
                                                                            $('#rolodex_search_text_name').trigger("blur");
                                                                            $('#rolodex_search_text_name').trigger("focus");
                                                                        }
                                                                    });
                                                                    $(document.body).on('blur', '#rolodex_search_text_name', function (e) {
                                                                        if (e.type == 'focusout' || e.keyCode == '13')
                                                                        {
                                                                            if ($('#companylistForm').valid())
                                                                            {
                                                                                if ($('#rolodex_search_text_name').val().trim().length > 0)
                                                                                {

                                                                                    rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                                                                                        processing: true,
                                                                                        stateSave: true,
                                                                                        "searching" : true,
																						stateSaveParams: function (settings, data) {
																							data.search.search = "";
																							data.start = 0;
																							delete data.order;
																							data.order = [0, "ASC"];
																						},
                                                                                        rolodexsearchAddress: true,
                                                                                        serverSide: true,
                                                                                        bFilter: false,
                                                                                        scrollX: true,
                                                                                        sScrollY: 400,
                                                                                        bScrollCollapse: true,
                                                                                        destroy: true,
                                                                                        autoWidth: true,
                                                                                        "lengthMenu": [5, 10, 25, 50, 100],
                                                                                        order: [[0, "ASC"]],
                                                                                        ajax: {
                                                                                            url: '<?php echo base_url(); ?>cases/searchForRolodexByfirm',
                                                                                            method: 'POST',
                                                                                            data: {rolodex_search_parties: $('#rolodex_search_parties_name').val(), rolodex_search_text: $(this).val()},
                                                                                        },
                                                                                        fnRowCallback: function (nRow, data) {
                                                                                            $(nRow).attr("cardcode", '' + data[8]);
                                                                                            $(nRow).addClass('viewsearchPartiesDetail1');
                                                                                        },
                                                                                        fnDrawCallback: function (oSettings) {
                                                                                        },
                                                                                        initComplete: function (settings, json) {

                                                                                        },
                                                                                        select: {
                                                                                            style: 'single'
                                                                                        }
                                                                                    });
                                                                                    /*$.ajax({
                                                                                     url: '<?php //echo base_url();     ?>cases/searchForRolodex',
                                                                                     method: 'POST',
                                                                                     data: {rolodex_search_parties:$('#rolodex_search_parties_name').val(),rolodex_search_text:$(this).val()},
                                                                                     beforeSend: function (xhr) {
                                                                                     $.blockUI();
                                                                                     },
                                                                                     complete: function (jqXHR, textStatus) {
                                                                                     $.unblockUI();
                                                                                     },
                                                                                     success: function (data) {
                                                                                     rolodexSearch.clear();
                                                                                     
                                                                                     var obj = $.parseJSON(data);
                                                                                     $.each(obj, function (index, item) {
                                                                                     var html = '';
                                                                                     html += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + item.cardcode + "><td>" + item.firm + "</td>";
                                                                                     html += "<td>" + item.city + "</td>";
                                                                                     html += "<td>" + item.address + "</td>";
                                                                                     html += "<td>" + item.address2 + "</td>";
                                                                                     html += "<td>" + item.phone + "</td>";
                                                                                     html += "<td>" + item.name + "</td>";
                                                                                     html += "<td>" + item.state + "</td>";
                                                                                     html += "<td>" + item.zip + "</td>";
                                                                                     html += "</tr>";
                                                                                     rolodexSearch.row.add($(html));
                                                                                     });
                                                                                     rolodexSearch.draw();
                                                                                     }
                                                                                     });*/
                                                                                } else
                                                                                {
                                                                                    swal({
                                                                                        title: "Alert",
                                                                                        text: "Please enter Firm name.",
                                                                                        type: "warning"
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    });

                                                                    $(document.body).on('click', '#rolodexsearchResult_data tbody tr', function () {
                                                                        if ($(this).hasClass('table-selected')) {
                                                                            $(this).removeClass('table-selected');
                                                                        } else {
                                                                            $('#rolodexsearchResult_data tr').removeClass('table-selected');
                                                                            $(this).addClass('table-selected');
                                                                        }
                                                                    });

                                                                    /* CODE TO ADD PARTY IN DASHBOARD ENDS*/
                                                                    /* CODE TO ADD CASE IN DASHBOARD STARTS*/

                                                                    $("#addNewCase").validate({
                                                                        rules: {
                                                                            first_name: {
                                                                                required: true,
                                                                                noBlankSpace: true
                                                                            },
                                                                            last_name: {
                                                                                required: true,
                                                                                noBlankSpace: true
                                                                            }
                                                                        },
                                                                        messages: {
                                                                            first_name: {
                                                                                required: "Please provide first name"
                                                                            },
                                                                            last_name: {
                                                                                required: "Please provide last name"
                                                                            }
                                                                        },
                                                                        submitHandler: function (form, event) {
                                                                            setValidation();
                                                                            event.preventDefault();
                                                                            $("#caseNo").val($("#hdnCaseId").val());
                                                                            var alldata = $("form#addNewCase").serialize();
                                                                            $.ajax({
                                                                                url: '<?php echo base_url(); ?>cases/checkDuplicateCase',
                                                                                method: 'POST',
                                                                                data: alldata,
                                                                                beforeSend: function (xhr) {
                                                                                    $.blockUI();
                                                                                },
                                                                                complete: function (jqXHR, textStatus) {
                                                                                    $.unblockUI();
                                                                                },
                                                                                success: function (result) {
                                                                                    var obj = $.parseJSON(result);
                                                                                    duplicateDatatable.rows().remove().draw();
                                                                                    if (obj.status == '1') {
                                                                                        $("#addcase").modal("hide");
                                                                                        $.each(obj.data, function (index, item) {
                                                                                            var html = '';
                                                                                            html += "<tr class='selectDuplicateData' data-caseno=" + item.caseno + " data-cardcode=" + item.cardcode + ">";
                                                                                            html += "<td>" + item.caseno + "</td>";
                                                                                            html += "<td>" + item.casetype + "</td>";
                                                                                            html += "<td>" + item.casestat + "</td>";
                                                                                            html += "<td>" + item.dateenter + "</td>";
                                                                                            html += "<td>" + item.social_sec + "</td>";
                                                                                            html += "<td>" + item.cardtype + "</td>";
                                                                                            html += "<td>" + item.caption1 + "</td>";
                                                                                            html += "</tr>";
                                                                                            duplicateDatatable.row.add($(html));
                                                                                        });
                                                                                        duplicateDatatable.draw();
                                                                                        $('input[name=first_name]').val($("#first_name").val());
                                                                                        $('input[name=last_name]').val($("#last_name").val());
                                                                                        $("#duplicateData").modal("show");
                                                                                        if (obj.data.length > 0) {
                                                                                            $('.selectDuplicateData').click(function () {
                                                                                                var cardcode = $(this).attr("data-cardcode");
                                                                                                var caseno = $(this).attr("data-caseno");
                                                                                                $('.selectDuplicateData').removeClass('highlight');
                                                                                                $('tr[ data-caseno=' + caseno + ']').addClass('highlight');
                                                                                                $('#edit_new_case').prop('disabled', false);
                                                                                                $('#attach_name').prop('disabled', false);
                                                                                            });
                                                                                        }
                                                                                    } else {
                                                                                        if ($("#prospect_id").val() != '')
                                                                                        {
                                                                                            $.ajax({
                                                                                                url: '<?php echo base_url(); ?>prospects/getProspectsdata',
                                                                                                method: "POST",
                                                                                                data: {prospect_id: $("#prospect_id").val()},
                                                                                                beforeSend: function (xhr) {
                                                                                                    $.blockUI();
                                                                                                },
                                                                                                complete: function (jqXHR, textStatus) {
                                                                                                    $.unblockUI();
                                                                                                },
                                                                                                success: function (result) {
                                                                                                    var data = $.parseJSON(result);

                                                                                                    $('#card_salutation').val(data.data.salutation);
                                                                                                    $('#card_suffix').val(data.data.suffix);
                                                                                                    $('#card_type').val(data.data.type);
                                                                                                    $('input[name=card_first]').val(data.data.first);
                                                                                                    $('input[name=card_last]').val(data.data.last);
                                                                                                    $('input[name=card2_address1]').val(data.data.address1);
                                                                                                    $('input[name=card2_city]').val(data.data.city);
                                                                                                    $('input[name=card2_state]').val(data.data.state);
                                                                                                    $('input[name=card2_zip]').val(data.data.zip);
                                                                                                    $('input[name=card_business]').val(data.data.business);
                                                                                                    $('input[name=card_home]').val(data.data.home);
                                                                                                    $('input[name=card_car]').val(data.data.cell);
                                                                                                    $('input[name=card_email]').val(data.data.email);
                                                                                                    $('input[name=card_social_sec]').val(data.data.social_sec);
                                                                                                    if (data.data.birthdate == 'NA') {
                                                                                                        $('input[name=card_birth_date]').val('');
                                                                                                    } else {
                                                                                                        $('input[name=card_birth_date]').val(moment(data.data.birthdate).format('MM/DD/YYYY'));
                                                                                                    }
                                                                                                    $("#new_case").val(1);
                                                                                                    $("#prospect_id").val($("#prospect_id").val());
                                                                                                    $("#addcase").modal("hide");
                                                                                                    $("#addRolodexForm select").trigger('change');
                                                                                                    $('select[name=card_language]').val('English').trigger("change");
                                                                                                    $("#addcontact").modal("show");
                                                                                                }
                                                                                            });
                                                                                        } else
                                                                                        {
                                                                                            $('input[name=card_first]').val($("#first_name").val());
                                                                                            $('input[name=card_last]').val($("#last_name").val());
                                                                                            $("#new_case").val(1);
                                                                                            $("#addcase").modal("hide");
                                                                                            $("#addRolodexForm select").trigger('change');
                                                                                            $('select[name=card_language]').val('English').trigger("change");
                                                                                            $("#addcontact").modal("show");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                    });

                                                                    $('#addContact').on('click', function () {
                                                                        var cardcodeVal = $('input[name="card_cardcode"]').val();
                                                                        if (cardcodeVal == '') {
                                                                            if (!$('#addRolodexForm').valid()) {
                                                                                return false;
                                                                            }
                                                                        }
                                                                        if (!$('#addRolodexForm').valid()) {
                                                                            return false;
                                                                        }
                                                                        var myImg = $('#profilepic').prop('files')[0];
                                                                        if(myImg){
                                                                            var sizeKB = myImg.size / 1024; ;
                                                                            if(sizeKB > 5000)
                                                                            {
                                                                                swal("Failure !", "The file size exceeds the limit allowed.", "error");
                                                                                return false;
                                                                            }
                                                                        }
                                                                        $.blockUI();
                                                                        var url = '<?php echo base_url() . "rolodex/addCardData" ?>';
                                                                        if ($('input[name="card_cardcode"]').val() != '') {
                                                                            url = '<?php echo base_url() . "rolodex/editCardData" ?>';
                                                                        }
                                                                        $.ajax({
                                                                            url: url,
                                                                            method: "POST",
                                                                            data: $('#addRolodexForm').serialize(),
                                                                            success: function (result) {
                                                                                var data = $.parseJSON(result);
                                                                                var editCardcode = data.editcardid;
                                                                                if ($('#profilepic').val() != '')
                                                                                {
                                                                                    var old_img = document.getElementById('hiddenimg').value;
                                                                                    var images = '';
                                                                                    var file_data = $('#profilepic').prop('files')[0];
                                                                                    var form_data = new FormData();
                                                                                    form_data.append('file', file_data);
                                                                                    $.ajax({
                                                                                        url: '<?php echo base_url(); ?>rolodex/updateprofilepic/' + editCardcode + '/' + old_img,
                                                                                        dataType: 'text',
                                                                                        cache: false,
                                                                                        contentType: false,
                                                                                        processData: false,
                                                                                        data: form_data,
                                                                                        type: 'post',
                                                                                        success: function (response) {

                                                                                            if (response)
                                                                                            {
                                                                                                images = response;
                                                                                                var plink = '<?php echo base_url() ?>assets/rolodexprofileimages/' + images;
                                                                                                setTimeout(function ()
                                                                                                {
                                                                                                    if (images != null) {
                                                                                                        if ($('#applicantImage').length == 0) {
                                                                                                            $('.client-img').find('img.img-circle').attr("src", plink);
                                                                                                        } else
                                                                                                        {
                                                                                                            $('#applicantImage').remove();
                                                                                                            $('.client-img').append('<center><img class="img-circle" height="67px" src="' + plink + '" /></center>');
                                                                                                        }
                                                                                                    } else {
                                                                                                    }
                                                                                                }, 500);
                                                                                            }
                                                                                        },
                                                                                        error: function (response) {
                                                                                        }
                                                                                    });
                                                                                }
                                                                                $.unblockUI();
                                                                                if (data.status == '1') {
                                                                                    $('#addRolodexForm')[0].reset();
                                                                                    $('#addcontact').modal('hide');
                                                                                    $('#addcontact .modal-title').html('Add New Contact');

                                                                                    if ((data.caseid != 0 || data.caseid != 'undefined')) {
                                                                                        var caseid = $("#hdnCaseId").val();
                                                                                        if (data.caseid != 0 && $("#new_case").val() == 1) {
                                                                                            var url = "<?php echo base_url() ?>cases/case_details/" + data.caseid;
                                                                                            setTimeout(function ()
                                                                                            {
                                                                                                window.location.href = url;
                                                                                            }, 3000);
                                                                                        }
                                                                                        if (cardcodeVal != '' || $("#new_case").val() == 'parties') {

                                                                                            $.ajax({
                                                                                                url: '<?php echo base_url() . "cases/case_details" ?>',
                                                                                                method: "POST",
                                                                                                data: {'caseId': $("#hdnCaseId").val()},
                                                                                                success: function (result) {
                                                                                                    var res = $.parseJSON(result);
                                                                                                    setTimeout(function ()
                                                                                                    {
                                                                                                        $(".applicant_name").html("<b>" + res.display_details.salutation + " " + res.display_details.first + " " + res.display_details.last + "</b>");
                                                                                                        if (res.display_details.popular)
                                                                                                        {
                                                                                                            if ((res.display_details.popular).length < 10) {
                                                                                                                var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular + '</b></span><br/>';
                                                                                                            } else
                                                                                                            {
                                                                                                                var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular.substr(1, 10) + '..</b></span><br/>';
                                                                                                            }
                                                                                                            if ($('.popular').length > 0) {
                                                                                                                $(".popular").replaceWith(htm);
                                                                                                            } else
                                                                                                            {
                                                                                                                $(".applicant_name").after(htm);
                                                                                                            }

                                                                                                        }
                                                                                                        $(".applicant_address").html(res.display_details.address1);
                                                                                                        $(".applicant_address2").html(res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);

                                                                                                        $('#address_map').attr('href', "https://maps.google.com/?q=" + res.display_details.address1 + '' + res.display_details.address2 + " " + res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);
                                                                                                        $(".home").html(res.display_details.home);
                                                                                                        if (res.display_details.car == "")
                                                                                                            $(".business").html(res.display_details.business);
                                                                                                        else
                                                                                                            $(".business").html(res.display_details.car);

                                                                                                        $(".email").html(res.display_details.email);
                                                                                                        var dob = new Date(res.display_details.birth_date);
                                                                                                        var today = new Date();
                                                                                                        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                                                                                                        $(".birth_date").html('Born on ' + moment(res.display_details.birth_date).format('MM/DD/YYYY') + ' ,' + age + 'years old');
                                                                                                        $("#party_name").html(res.display_details.first + " " + res.display_details.last);
                                                                                                        $("#party_beeper").val(res.display_details.beeper);
                                                                                                        var p_tr_id = $('table#party-table-data').find('.table-selected');
                                                                                                        p_tr_id = p_tr_id.attr('id');
                                                                                                        $("#" + p_tr_id).find(".parties_name").html(res.display_details.first + ", " + res.display_details.last);
                                                                                                    }, 1000);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    swal("Oops...", data.message, "error");
                                                                                }
                                                                            }
                                                                        });

                                                                        return false;
                                                                    });

                                                                    $(document.body).on('click', '#add_name_case', function () {
                                                                        $('input[name=card_first]').val($(".first_name").val());
                                                                        $('input[name=card_last]').val($(".last_name").val());
                                                                        $("#new_case").val(1);
                                                                        $("#addcase").modal("hide");
                                                                        $("#duplicateData").modal("hide");
                                                                        $("#addcontact").modal("show");
                                                                        return false;
                                                                    });
                                                                    $('#addcase.modal').on('hidden.bs.modal', function () {
                                                                        $(this).find('input[type="text"]').each(function () {
                                                                            this.value = '';
                                                                        });
                                                                    });
                                                                    /* CODE TO ADD CASE IN DASHBOARD ENDS*/
                                                                    $('.contact').mask("(000) 000-0000");
                                                                    $('.social_sec').mask("000-00-0000");
                                                                });

                                                                function setValidation() {
                                                                    var validator = $('#addRolodexForm').validate({
                                                                        ignore: [],
                                                                        rules: {
                                                                            card_first: {
                                                                                required: function (element) {
                                                                                    if ($("input[name=card2_firm]").val() == '') {
                                                                                        return true;
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                            },
                                                                            card2_firm: {
                                                                                required: function (element) {
                                                                                    if ($("input[name=card_first]").val() == '') {
                                                                                        return true;
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                            },
                                                                            card2_address1: {
                                                                                required: function (element) {
                                                                                    if ($("input[name=card2_firm]").val() != '') {
                                                                                        return true;
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                            },
                                                                            card2_city: {
                                                                                required: function (element) {
                                                                                    if ($("input[name=card2_firm]").val() != '') {
                                                                                        return true;
                                                                                    }
                                                                                    return false;
                                                                                }
                                                                            },
                                                                            card2_state: {
                                                                                required: function (element) {
                                                                                    if ($("input[name=card2_firm]").val() != '') {
                                                                                        return true;
                                                                                    }
                                                                                    return false;
                                                                                },
                                                                                stateUS: true
                                                                            },
                                                                            card2_zip: {
                                                                                required: function (element) {
                                                                                    if ($("input[name=card2_firm]").val() != '') {
                                                                                        return true;
                                                                                    }
                                                                                    return false;
                                                                                },
                                                                                zipcodeUS: true
                                                                            },
                                                                            card_type: {
                                                                                required: true
                                                                                        /*function (element) {
                                                                                         if ($("input[name=card_type]").val() != '') {
                                                                                         return true;
                                                                                         }
                                                                                         return false;
                                                                                         }*/
                                                                            },
                                                                            /*card_middle: {letterwithspace: true},
                                                                            card_last: {letterwithspace: true},*/
                                                                            card_business: {phoneUS: true},
                                                                            card_home: {phoneUS: true},
                                                                            card_fax: {phoneUS: true, maxlength: 15},
                                                                            card_email: {email: true},
                                                                            card2_phone1: {phoneUS: true},
                                                                            card2_fax: {phoneUS: true},
                                                                            card2_phone2: {phoneUS: true},
                                                                            card2_fax2: {phoneUS: true},
                                                                            card2_tax_id: {alphanumeric: true, maxlength: 15},
                                                                            card_social_sec: {ssId: true},
                                                                            card_speciality: {letterswithbasicpunc: true}
                                                                        },
                                                                        messages: {
                                                                            card_first: {required: "Please Fill First Name or Firm Name"},
                                                                            card2_firm: {required: "Please Fill First Name or Firm Name"},
                                                                            card2_fax: {phoneUS: "Please specify a valid fax number"},
                                                                            card2_fax2: {phoneUS: "Please specify a valid fax number"},
                                                                            card_fax: {phoneUS: "Please specify a valid fax number"}
                                                                        },
                                                                        invalidHandler: function (form, validator) {
                                                                            var element = validator.invalidElements().get(0);
                                                                            var tab = $(element).closest('.tab-pane').attr('id');

                                                                            $('#addcontact a[href="#' + tab + '"]').tab('show');
                                                                        },
                                                                        errorPlacement: function (error, element) {
                                                                            if (element.attr("name") == "card_type") {
                                                                                error.insertAfter(element.next());
                                                                            } else {
                                                                                error.insertAfter(element);
                                                                            }
                                                                        }
                                                                    });
                                                                    return validator;
                                                                }

                                                                function ChangeRolodex() {
                                                                    var cardcode = $('#caseCardCode').val();
                                                                    var caseno = $('#caseNo').val();
                                                                    $('#show_conform_model').modal('hide');
                                                                    $.blockUI();
                                                                    $.ajax({
                                                                        url: '<?php echo base_url(); ?>rolodex/getEditRolodex',
                                                                        data: {cardcode: cardcode, caseNO: caseno, status: 'Yes'},
                                                                        method: "POST",
                                                                        success: function (result) {
                                                                            var data = $.parseJSON(result);
                                                                            var notes_comments = '';
                                                                            if (data.card_comments) {
                                                                                if(isJson(data.card_comments)) {
                                                                                    notes_comments = $.parseJSON(data.card_comments);
                                                                                } else {
                                                                                    notes_comments = data.card_comments;
                                                                                }
                                                                            }
                                                                            swal.close();
                                                                            if (data.status == '0')
                                                                            {
                                                                                var name = '';
                                                                                if (data.data.type == 'APPLICANT')
                                                                                {
                                                                                    name = data.data.last + ', ' + data.data.first;
                                                                                    $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                                } else if (data.data.type == 'LIEN' || data.data.type == 'BOARD' || data.data.type == 'INSURANCE' || data.data.type == 'ATTORNEY' || data.data.type == 'EMPLOYER')
                                                                                {
                                                                                    name = data.data.firm;
                                                                                    $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                                } else if (data.data.type == 'DR') {
                                                                                    if (data.data.firm.length == 0) {
                                                                                        name = data.data.last + ', ' + data.data.first;
                                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                                    } else {
                                                                                        name = data.data.firm;
                                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                                    }
                                                                                } else
                                                                                {
                                                                                    name = data.data.last + ', ' + data.data.first;
                                                                                    $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                                }
                                                                                $('#rolodex_search_text').val(name);
                                                                                $("#rolodex_search_parties_form").submit();
                                                                                $("#addparty").modal('show');
                                                                            } else
                                                                            {
                                                                                $('#addcontact .modal-title').html('Edit Contact');
                                                                                $('#card_salutation').val(data.salutation);
                                                                                $('#card_suffix').val(data.suffix);
                                                                                $('select[name=card_title]').val(data.title);
                                                                                $('#card_type').val(data.type);
                                                                                $('input[name=card_first]').val(data.first);
                                                                                $('input[name=card_middle]').val(data.middle);
                                                                                $('input[name=card_last]').val(data.last);
                                                                                $('input[name=card_popular]').val(data.popular);
                                                                                $('input[name=card_letsal]').val(data.letsal);
                                                                                $('input[name=card_occupation]').val(data.occupation);
                                                                                $('input[name=card_employer]').val(data.employer);
                                                                                if (data.date_of_hire == 'NA') {
                                                                                    $('input[name=date_of_hire]').val('');
                                                                                } else {
                                                                                    $('input[name=date_of_hire]').val(moment(data.date_of_hire).format('MM/DD/YYYY'));
                                                                                }
                                                                                $('input[name=hiddenimg]').val(data.picture);
                                                                                if (data.picture)
                                                                                {
                                                                                    document.getElementById('profilepicDisp').style.display = 'block';
                                                                                    document.getElementById('dispProfile').src = data.picturePath;
                                                                                } else
                                                                                {
                                                                                    document.getElementById('dispProfile').src = '';
                                                                                }
                                                                                $('input[name=card2_firm]').val(data.firm);
                                                                                $('input[name=card2_address1]').val(data.address1);
                                                                                $('input[name=card2_address2]').val(data.address2);
                                                                                $('input[name=card2_city]').val(data.city);
                                                                                $('input[name=card2_state]').val(data.state);
                                                                                $('input[name=card2_zip]').val(data.zip);
                                                                                $('input[name=card2_venue]').val(data.venue);
                                                                                $('input[name=card2_eamsref]').val(data.eamsref);
                                                                                $('input[name=eamsref_name]').val(data.eamsname);
                                                                                $('input[name=card_business]').val(data.business);
                                                                                $('input[name=card_fax]').val(data.card_fax);
                                                                                $('input[name=card_home]').val(data.home);
                                                                                $('input[name=card_car]').val(data.car);
                                                                                $('input[name=card_email]').val(data.email);
                                                                                if (notes_comments != '')
                                                                                    $('input[name=card_notes]').val(notes_comments[0].notes);
                                                                                else
                                                                                    $('input[name=card_notes]').val();
                                                                                $('input[name=card2_phone1]').val(data.phone1);
                                                                                $('input[name=card2_phone2]').val(data.phone2);
                                                                                $('input[name=card2_tax_id]').val(data.tax_id);
                                                                                $('input[name=card2_fax]').val(data.card2_fax);
                                                                                $('input[name=card2_fax2]').val(data.fax2);
                                                                                $('input[name=card_social_sec]').val(data.social_sec);
                                                                                if (data.birth_date == 'NA') {
                                                                                    $('input[name=card_birth_date]').val('');
                                                                                } else {
                                                                                    /*$('input[name=card_birth_date]').val(data.birth_date);*/
                                                                                    $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                                                                                }
                                                                                $('input[name=card_licenseno]').val(data.licenseno);
                                                                                $('select[name=card_specialty]').val(data.specialty);
                                                                                $('input[name=card_mothermaid]').val(data.mothermaid);
                                                                                $('input[name=card_cardcode]').val(data.cardcode);
                                                                                $('input[name=card_firmcode]').val(data.firmcode);
                                                                                $('select[name=card_interpret]').val(data.interpret);
                                                                                $('select[name=card_language]').val(data.language).trigger("change");
                                                                                $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                                                                                $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                                                                                $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                                                                                $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                                                                                if (notes_comments != '')
                                                                                    $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                                                                else
                                                                                    $('textarea[name=card_comments]').val();
                                                                                $('textarea[name=card2_comments]').val(data.card2_comments);
                                                                                $('textarea[name=card2_mailing1]').val(data.mailing1);
                                                                                $('textarea[name=card2_mailing2]').val(data.mailing2);
                                                                                $('textarea[name=card2_mailing3]').val(data.mailing3);
                                                                                $('textarea[name=card2_mailing4]').val(data.mailing4);
                                                                                $('#new_case').val('parties');
                                                                                $('input[name="card_cardcode"]').val(data.cardcode);
                                                                                $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                                $("#addcontact").modal('show');
                                                                                $("#addRolodexForm select").trigger('change');
                                                                                $.unblockUI();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                                $(document.body).on('click', '#change_name_warning', function () {
                                                                    $("#partiesNameChangeWarning").modal("hide");
                                                                    /*editContact($('#party_name').data('cardcodeWarning'));*/
                                                                    var cardCode = $('#caseCardCode').val();
                                                                    $.ajax({
                                                                        url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                                                                        method: "POST",
                                                                        data: {'cardcode': cardCode},
                                                                        success: function (result) {
                                                                            if (result > 1) {
                                                                                swal({
                                                                                    title: "Warning!",
                                                                                    type: "warning",
                                                                                    text: "This card is currently attached to " + result + " cases.\nChange to this card will affect " + result + " cases.\n\nIf you want changes to be reflected for only one case please create a new card \n\nAre you sure you want to create new rolodex and replace it to this rolodex card? ",
                                                                                    showCancelButton: true,
                                                                                    confirmButtonColor: "#DD6B55",
                                                                                    confirmButtonText: "Yes",
                                                                                    closeOnConfirm: true
                                                                                }, function () {
                                                                                    CreateNewRolodex();
                                                                                });
                                                                            } else
                                                                            {
                                                                                ChangeRolodex();
                                                                            }
                                                                        }
                                                                    });

                                                                });
                                                                $(document.body).on('click', '#change_name_warning_no', function () {
                                                                    $("#partiesNameChangeWarning").modal("hide");
                                                                    var cardCode = $('#caseCardCode').val();
                                                                    $.ajax({
                                                                        url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                                                                        method: "POST",
                                                                        data: {'cardcode': cardCode},
                                                                        success: function (result) {
                                                                            if (result > 1) {
                                                                                swal({
                                                                                    title: "Warning!",
                                                                                    type: "warning",
                                                                                    text: "This card is currently attached to " + result + " cases.\nChange to this card will affect " + result + " cases.\n\nIf you want changes to be reflected for only one case please detach that card from that case at the Parties sheet and attach another card\n\nAre you sure you want to make changes to this rolodex card? ",
                                                                                    showCancelButton: true,
                                                                                    confirmButtonColor: "#DD6B55",
                                                                                    confirmButtonText: "Yes",
                                                                                    closeOnConfirm: true
                                                                                }, function () {
                                                                                    ChangeRolodex();
                                                                                });
                                                                            } else
                                                                            {
                                                                                ChangeRolodex();
                                                                            }
                                                                        }
                                                                    });
                                                                });

                                                                function CreateNewRolodex() {
                                                                    $('#newRolodexCard').val('1');
                                                                    $('#oldCardValue').val($('#caseCardCode').val());
                                                                    $('#oldCaseNo').val($('#caseNo').val());
                                                                    var cardcode = $('#caseCardCode').val();
                                                                    var caseno = $('#caseNo').val();
                                                                    $('#show_conform_model').modal('hide');
                                                                    $.ajax({
                                                                        url: '<?php echo base_url(); ?>rolodex/getEditRolodex',
                                                                        data: {cardcode: cardcode, caseNO: caseno, status: 'Yes'},
                                                                        method: "POST",
                                                                        success: function (result) {
                                                                            var data = $.parseJSON(result);
                                                                            var notes_comments = '';
                                                                            if (data.card_comments) {
                                                                                if(isJson(data.card_comments)) {
                                                                                    notes_comments = $.parseJSON(data.card_comments);
                                                                                } else {
                                                                                    notes_comments = data.card_comments;
                                                                                }
                                                                            }
                                                                            swal.close();
                                                                            if (data.status == '0')
                                                                            {
                                                                                var name = '';
                                                                                if (data.data.type == 'APPLICANT')
                                                                                {
                                                                                    name = data.data.last + ', ' + data.data.first;
                                                                                    $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                                } else if (data.data.type == 'LIEN' || data.data.type == 'BOARD' || data.data.type == 'INSURANCE' || data.data.type == 'ATTORNEY' || data.data.type == 'EMPLOYER')
                                                                                {
                                                                                    name = data.data.firm;
                                                                                    $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                                } else if (data.data.type == 'DR') {
                                                                                    if (data.data.firm.length == 0) {
                                                                                        name = data.data.last + ', ' + data.data.first;
                                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                                    } else {
                                                                                        name = data.data.firm;
                                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                                    }
                                                                                } else
                                                                                {
                                                                                    name = data.data.last + ', ' + data.data.first;
                                                                                    $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                                }
                                                                                $('#rolodex_search_text').val(name);
                                                                                $("#rolodex_search_parties_form").submit();
                                                                                $("#addparty").modal('show');
                                                                            } else
                                                                            {
                                                                                $('#addcontact .modal-title').html('Edit Contact');
                                                                                $('#card_salutation').val(data.salutation);
                                                                                $('#card_suffix').val(data.suffix);
                                                                                $('select[name=card_title]').val(data.title);
                                                                                $('#card_type').val(data.type);
                                                                                $('input[name=card_first]').val(data.first);
                                                                                $('input[name=card_middle]').val(data.middle);
                                                                                $('input[name=card_last]').val(data.last);
                                                                                $('input[name=card_popular]').val(data.popular);
                                                                                $('input[name=card_letsal]').val(data.letsal);
                                                                                $('input[name=card_occupation]').val(data.occupation);
                                                                                $('input[name=card_employer]').val(data.employer);
                                                                                if (data.date_of_hire == 'NA') {
                                                                                    $('input[name=date_of_hire]').val('');
                                                                                } else {
                                                                                    $('input[name=date_of_hire]').val(moment(data.date_of_hire).format('MM/DD/YYYY'));
                                                                                }
                                                                                $('input[name=hiddenimg]').val(data.picture);
                                                                                if (data.picture)
                                                                                {
                                                                                    document.getElementById('profilepicDisp').style.display = 'block';
                                                                                    document.getElementById('dispProfile').src = data.picturePath;
                                                                                } else
                                                                                {
                                                                                    document.getElementById('dispProfile').src = '';
                                                                                }
                                                                                $('input[name=card2_firm]').val(data.firm);
                                                                                $('input[name=card2_address1]').val(data.address1);
                                                                                $('input[name=card2_address2]').val(data.address2);
                                                                                $('input[name=card2_city]').val(data.city);
                                                                                $('input[name=card2_state]').val(data.state);
                                                                                $('input[name=card2_zip]').val(data.zip);
                                                                                $('input[name=card2_venue]').val(data.venue);
                                                                                $('input[name=card2_eamsref]').val(data.eamsref);
                                                                                $('input[name=eamsref_name]').val(data.eamsname);
                                                                                $('input[name=card_business]').val(data.business);
                                                                                $('input[name=card_fax]').val(data.card_fax);
                                                                                $('input[name=card_home]').val(data.home);
                                                                                $('input[name=card_car]').val(data.car);
                                                                                $('input[name=card_email]').val(data.email);
                                                                                if (notes_comments != '')
                                                                                    $('input[name=card_notes]').val(notes_comments[0].notes);
                                                                                else
                                                                                    $('input[name=card_notes]').val();
                                                                                $('input[name=card2_phone1]').val(data.phone1);
                                                                                $('input[name=card2_phone2]').val(data.phone2);
                                                                                $('input[name=card2_tax_id]').val(data.tax_id);
                                                                                $('input[name=card2_fax]').val(data.card2_fax);
                                                                                $('input[name=card2_fax2]').val(data.fax2);
                                                                                $('input[name=card_social_sec]').val(data.social_sec);
                                                                                if (data.birth_date == 'NA') {
                                                                                    $('input[name=card_birth_date]').val('');
                                                                                } else {
                                                                                    /*$('input[name=card_birth_date]').val(data.birth_date);*/
                                                                                    $('input[name=card_birth_date]').val(moment(data.birthdate).format('MM/DD/YYYY'));
                                                                                }
                                                                                $('input[name=card_licenseno]').val(data.licenseno);
                                                                                $('select[name=card_specialty]').val(data.specialty);
                                                                                $('input[name=card_mothermaid]').val(data.mothermaid);
                                                                                $('input[name=card_cardcode]').val(data.cardcode);
                                                                                $('input[name=card_firmcode]').val(data.firmcode);
                                                                                $('select[name=card_interpret]').val(data.interpret);
                                                                                $('select[name=card_language]').val(data.language).trigger("change");
                                                                                $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                                                                                $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                                                                                $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                                                                                $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                                                                                if (notes_comments != '')
                                                                                    $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                                                                else
                                                                                    $('textarea[name=card_comments]').val();
                                                                                $('textarea[name=card2_comments]').val(data.card2_comments);
                                                                                $('textarea[name=card2_mailing1]').val(data.mailing1);
                                                                                $('textarea[name=card2_mailing2]').val(data.mailing2);
                                                                                $('textarea[name=card2_mailing3]').val(data.mailing3);
                                                                                $('textarea[name=card2_mailing4]').val(data.mailing4);
                                                                                $('#new_case').val('parties');
                                                                                $('input[name="card_cardcode"]').val(data.cardcode);
                                                                                $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                                $("#addcontact").modal('show');
                                                                                $("#addRolodexForm select").trigger('change');
                                                                                $.unblockUI();
                                                                            }
                                                                        }
                                                                    });
                                                                    $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                    $("#addcontact").modal('show');
                                                                }
                                                                
                                                                $(document.body).on('click', '.dataTables-eamsLookup tbody tr', function () {
                                                                    if ($(this).hasClass('table-selected')) {
                                                                        $(this).removeClass('table-selected');
                                                                    } else {
                                                                        $('.dataTables-eamsLookup tr').removeClass('table-selected');
                                                                        $(this).addClass('table-selected');
                                                                    }
                                                                });
                                                                $(document.body).on('click', '#eamsrefDetails', function () {
                                                                    var selectedrows = eamsData.rows('.table-selected').data();
                                                                    var caseNo = $("#caseNo").val();
                                                                    var cardCode = $('#caseCardCode').val();
                                                                    /*alert(selectedrows[0][0]); return false;*/
                                                                    if (selectedrows.length > 0) {
                                                                        $('#eamslist').modal('hide');
                                                                        swal({
                                                                            title: 'EAMS',
                                                                            text: 'Do you want to attach '+selectedrows[0][1]+' to the rolodex?',
                                                                            showCancelButton: true,
                                                                            confirmButtonColor: "#1ab394",
                                                                            confirmButtonText: "Yes",
                                                                            closeOnConfirm: false,
                                                                            showLoaderOnConfirm: true
                                                                        }, function (isConfirm) {
                                                                            if (isConfirm) {
                                                                                swal.close();
                                                                                $.ajax({
                                                                                    url: '<?php echo base_url(); ?>eams_lookup/eamsrefDetails',
                                                                                    method: 'POST',
                                                                                    dataType: 'json',
                                                                                    data: {
                                                                                        eamsref: selectedrows[0][0],caseno: caseNo,cardCode:cardCode
                                                                                    },
                                                                                    beforeSend: function (xhr) {
                                                                                        $.blockUI();
                                                                                    },
                                                                                    complete: function (jqXHR, textStatus) {
                                                                                        $.unblockUI();
                                                                                    },
                                                                                    success: function (data) {
                                                                                        if (data == false) {
                                                                                            swal({
                                                                                                title: "Alert",
                                                                                                text: "No Party Selected.",
                                                                                                type: "warning"
                                                                                            });
                                                                                        } else {

                                                                                            $('input[name="card2_eamsref"]').val(data.eamsref);
                                                                                            $('input[name="eamsref_name"]').val(data.name);
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                        });

                                                                    } else {
                                                                        swal({
                                                                            title: "Alert",
                                                                            text: "No Party Selected.",
                                                                            type: "warning"
                                                                        });
                                                                    }
                                                                });
                                                                
    $(document.body).on('click', '#eamsrefDetach', function () {
    var caseNo = $("#caseNo").val();
    var cardCode = $('#caseCardCode').val();
    /*alert(selectedrows[0][0]); return false;*/
    $('#eamslist').modal('hide');
    swal({
        title: 'EAMS',
        text: 'Are you sure you want to detach any EAMS uniform references to this rolodex card and use defaults?',
        showCancelButton: true,
        confirmButtonColor: "#1ab394",
        confirmButtonText: "Yes",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
            swal.close();
            $.ajax({
                url: '<?php echo base_url(); ?>eams_lookup/eamsrefDetached',
                method: 'POST',
                dataType: 'json',
                data: {
                    caseno: caseNo,cardCode:cardCode
                },
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (data) {
                    /*if (data == false) {
                        swal({
                            title: "Alert",
                            text: "No Party Selected.",
                            type: "warning"
                        });
                    } else {*/
                        
                        $('input[name="card2_eamsref"]').val('');
                        $('input[name="eamsref_name"]').val('');
                   /* } */
                }
            });
        }
    });
    
    
    });
    
    
    $('#eamslist').on('shown.bs.modal', function (e) {
        var eams = $('input[name="card2_eamsref"]').val();
        if(eams && eams != 0){
            $('#eamsrefDetach').prop('disabled', false);
        }
        else{
            $('#eamsrefDetach').prop('disabled', true);
        }
        eamsData.columns.adjust();
    });

    $('#eamslist').on('hide.bs.modal', function (e) {
        $('.input-sm').val('');
        eamsData.search('').draw(); 
        eamsData.ajax.reload();
    });

    // new partyemail
    $(document.body).on('click', '.partyemail', function () {
    $('.addSearchedParties_for_partyemail').attr("disabled", true);
    $("#partyemail").modal('show');
    });

    $("#search_party_email").click( function() {
        event.preventDefault();
         var temp = $('#rolodex_search_text_for_partyemail').val();
         var flag = 0 ;
        if(temp == "")
        {
            error = "Please enter text to search";
            document.getElementById("rolodex_search_parties-error_for_partyemail1").innerHTML = error;
            flag = 1;
        }
        else{
            flag = 0;
            document.getElementById("rolodex_search_parties-error_for_partyemail1").innerHTML = "";
        }
        if($('#name_firm').prop('checked') || $('#ss_no').prop('checked') || $('#firstname').prop('checked') || $('#lastname').prop('checked') || $('#email_rolodex').prop('checked') || $('#details').prop('checked') || $('#firm_company').prop('checked') || $('#license_no').prop('checked') || $('#relational').prop('checked') || $('#card_number').prop('checked') || $('#type_rolodex').prop('checked')) {
            document.getElementById("rolodex_search_parties-error_for_partyemail").innerHTML = "";
            if(flag == 0) {
                flag = 0;
            } else {
                flag = 1;
            }
        } else {
            flag = 1;
            error = "Please Select a radio to search";
            document.getElementById("rolodex_search_parties-error_for_partyemail").innerHTML = error;
        }
       
        if(flag == 0) {
            document.getElementById("rolodex_search_parties-error_for_partyemail1").innerHTML = "";
            document.getElementById("rolodex_search_parties-error_for_partyemail").innerHTML = "";
            var alldata = $("form#rolodex_search_parties_form_for_partyemail").serialize();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/searchForRolodex_email',
                method: 'POST',
                data: alldata,
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (data) {
                    var text = $("#rolodex_search_text_for_partyemail").val();
                    var substring = text.substr(0, 1);
                    var radioVal = $("input[name='rolodex_search_parties_for_partyemail']:checked").val();
                    if (substring == "<") {
                        var res = $.parseJSON(data);
                        $("#searched_party_fullname_for_partyemail").val(res[0].fullname);
                        $("#searched_party_firmname_for_partyemail").val(res[0].firm);
                        $('#searched_party_type_for_partyemail').html(res[0].type);
                        $('#searched_party_cardnumber_for_partyemail').html(res[0].cardcode);
                        }
                    rolodexSearchDatatableforpartyemail.clear();
                    var obj = $.parseJSON(data);
                    $.each(obj, function (index, item) {
                        var html = '';
                        html += "<tr class='viewPartiesDetailforemailparty' data-cardcode=" + item.cardcode + ">";
                        html += "<td>" + item.name + "</td>";
                        html += "<td>" + item.firm + "</td>";
                        html += "<td>" + item.type + "</td>";
                        html += "<td>" + item.city + "</td>";
                        html += "<td>" + item.address + "</td>";
                        html += "<td>" + item.zip + "</td>";
                        html += "</tr>";
                        rolodexSearchDatatableforpartyemail.row.add($(html));
                    });
                    rolodexSearchDatatableforpartyemail.draw();
                }
            });
        } else {
            return false;
        }
    });

     $('#partyemail').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });
                $("input:radio[name='rolodex_search_parties_for_partyemail']").each(function () {
                $(this).iCheck('uncheck');
                });
                rolodexSearchDatatableforpartyemail.clear();
                rolodexSearchDatatableforpartyemail.draw();
                }); 
                
         $('#partyemail.modal').on('shown.bs.modal', function () {
             rolodexSearchDatatableforpartyemail.draw();
         });

        $(document.body).on('click', '.viewPartiesDetailforemailparty', function () {
            if ($('.viewPartiesDetailforemailparty').hasClass('highlight_new') == true)
            {
                $('.viewPartiesDetailforemailparty').removeClass("highlight_new")
            }
             $(this).addClass('highlight_new');
             var caseNo = $("#caseNo").val();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    cardcode: $(this).data('cardcode'),
                    caseno: caseNo
                },
                beforeSend: function (xhr) {
                    $.blockUI();
                },
               
                success: function (data) {
                    if (data == false) {
                        swal({
                            title: "Alert",
                            text: "No Party Selected.",
                            type: "warning"
                        });
                    } else {
                        var card_code = data.cardcode ;
                        $.ajax({
                        url: '<?php echo base_url(); ?>cases/get_details_for_emailparty',
                        method: 'POST',
                        dataType: 'json',
                        data: {card_code: card_code },
                        success: function (response) {
                            $('#staff_list_email').val(response);
                            $('.addSearchedParties_for_partyemail').attr("disabled", false); 
                            $.unblockUI();
                            }
                         });
                    }
                    }
            });
        });

        $(document.body).on('click', '.addSearchedParties_for_partyemail ', function () {
            $("#partyemail").modal('hide');
            $("#partyemailproceed").modal('show'); 
            var staffList = $('#staff_list_email').val();
            var convertedstaffList =  staffList.split(',');
            convertedstaffList = convertedstaffList.map(function (el) {
             return el.trim();
            });
            var convertedstaffList1 = convertedstaffList.filter(function(v){return v!==''});
            $('#whoto1_partyemail').val(convertedstaffList1).trigger('change');
            CKEDITOR.replace('mailbody1_partyemail', {
            height: 100
            });
        });

$('#partyemailproceed.modal').on('hidden.bs.modal', function () {
            $(this).find('input[type="email"],textarea,select,file').each(function () {
                this.value = '';
            });
            $('#partyemailproceed').find('#mail_urgent1').prop('checked', false);
            $('#partyemailproceed').find('.att-add-file').not(':first').remove();
            $('input[name="afiles1[]"]').remove();
            $('input[name="files_global_var[]"]').remove();
            $('.fileremove').html('');
            $('#case_category1_partyemail').val('1');
            $('#whoto1_partyemail').val('0').trigger('change');
            $('#mail_subject1_partyemail').val('');
            $('#emails1_partyemail').val('');
            $("#send_mail_form1_partyemail select").trigger('change');
            var $el = $('#afiles2');
            $el.unwrap();
            CKEDITOR.instances.mailbody1_partyemail.setData("");
            // $('#send_mail_form1_partyemail').resetForm();
        });

        $('.discard_message').click(function (e) {
            swal({
                title: "Are you sure?",
                text: "The changes you made will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, discard it!",
                closeOnConfirm: true
            }, function () {
                $('#partyemailproceed').find('.att-add-file').not(':first').remove();
                $('#partyemailproceed').modal("hide");
            });
        });

        $(document).ready(function () {
            $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
            });
            let send_mail_form1_validate = "";
            send_mail_form1_validate =$('#send_mail_form1_partyemail').validate({
            rules: {
                whoto1_partyemail: {required: function () {
                        if ($('#whoto1_partyemail').val() == "")
                            return true;
                        else
                            return false;
                    }
                },
                mail_subject1_partyemail: {
                    required: true
                }
            },
            messages: {
                whoto1_partyemail: {
                    required: 'Please Select a User to send mail to'
                },
                mail_subject1_partyemail: {
                    required: 'Please enter a Mail Subject'
                }
            },
            errorPlacement: function (error, element) {
                if ($(element).attr('name') == 'whoto1_partyemail') {
                    $(element).parent().append(error);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                var whoto = [];
                whoto = $('select[name="whoto1_partyemail"]').val();
                var subject = $('#mail_subject1_partyemail').val();
                var urgent = $('input[name="mail_urgent1_partyemail"]:checked').val();
                var ext_emails = $('input[name="emails1_partyemail"]').val();
                var mailbody = CKEDITOR.instances.mailbody1_partyemail.getData();
                var filenameVal = $("#filenameVal_partyemail").val();
                var mailType = $("#mailType1").val();
                // var case_no = $("#case_no1").val();
                // var case_category1 = $("#case_category1_partyemail").val();
                var form_data = new FormData();
                
                var ext_emails = $('input[name="emails1_partyemail"]').val();
                let valid_data = true;
                if (ext_emails != '') {
                    valid_data = validate_ext_email($("input[name=emails1_partyemail]").val());
                }
                if(valid_data == false) {
                    return false;
                }
                form_data.append("whoto", whoto);
                form_data.append("subject", subject);
                form_data.append("urgent", urgent);
                form_data.append("ext_emails", ext_emails);
                form_data.append("mailbody", mailbody);
                form_data.append("mailType", mailType);
                form_data.append("filenameVal", filenameVal);
                // form_data.append("caseNo", case_no);
                // form_data.append("caseCategory", case_category1);
                var p = 0;
                var filelength = $("input[name='afiles1[]']");
                var fileGlobal = [];
                var tmp_fileGlobal = $("input[name='files_global_var[]']").map(function(){return $(this).val();}).get();
                $.each(tmp_fileGlobal, function(k, v) {
                    var array = $.map(JSON.parse(v), function(value, index) {
                        return [ index + ':' + value];
                    });
                    fileGlobal[k] = array;
                });
                var files_arry =[];
                form_data.append("fileGlobalVariable",JSON.stringify(fileGlobal));
                $.each($('input[name="afiles1[]"]'), function (i, obj) {
                    $.each(obj.files, function (x, file) {
                        vaild_data = validateAttachFileExtension(file.name);
                        form_data.append("files_" + p, file);
                        p = p + 1;
                    });
                    files_arry.push(obj.defaultValue);
                    if(filelength.length == files_arry.length)
                    {
                        form_data.append("files" ,files_arry);
                    }
                });
                $.ajax({
                    url: '<?php echo base_url(); ?>email/send_email',
                    method: 'POST',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.result == "true") {
                            swal({
                                title: "Success!",
                                text: "Mail is successfully sent.",
                                type: "success"
                            });
                        } else if (data.result == "true" && data.ext_party == '0')
                        {
                            swal({
                                title: "Alert",
                                text: "Oops! Something went wrong. Email wasn't send to External Party.",
                                type: "warning"
                            });
                        } else if (data.result == "false" && data.attachment_issue != '')
                        {
                            swal({
                                title: "Alert",
                                text: "Oops! " + data.attachment_issue,
                                type: "warning"
                            });
                            return false;
                        } else {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again.",
                                type: "warning"
                            });
                        }
                        $('#partyemailproceed').modal('hide');
                        $('.fileremove').remove();
                        $('input[name="afiles1[]"]').remove();
                        $('#totalFileSize_partyemail').val('0');
                      $('input[name="files_global_var[]"]').remove();
                    }
                    });
                return false;
            }
        }); 
 });

 function validate_ext_email(v_ext_emails) {
        var emails = '';
        var count_e = 0;
        if (v_ext_emails.indexOf(',') !== -1) {
            emails = v_ext_emails.split(",");
            count_e = emails.length;
        }
        var pattern = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (count_e > 0) {
            for (i = 0; i < emails.length; i++) {
                if (!pattern.test(emails[i].trim())) {
                    swal({
                        title: "Alert",
                        text: "Oops! Please enter valid email ID.\n" + emails[i].trim(),
                        type: "warning"
                    });
                    return false;
                }
            }
        } else {
            if (!pattern.test(v_ext_emails.trim())) {
                swal({
                    title: "Alert",
                    text: "Oops! Please enter valid email ID.\n" + v_ext_emails.trim(),
                    type: "warning"
                });
                return false;
            }
        }
        return true;
    }

    $(function() {
        $('#afiles3_partyemail').bind("change", function() {
            var fileSize = parseInt($('#totalFileSize_partyemail').val());
            var formData = new FormData();
            var fileName = document.getElementById('afiles3_partyemail').files;
            var fi = document.getElementById('afiles3_partyemail');
            var fsize = '';
            for (var i = 0, len = document.getElementById('afiles3_partyemail').files.length; i < len; i++) {
            var fsize = fi.files.item(i).size / 1024;
            fileSize = fileSize + fsize;
            
            if(fsize < 20001)
            {
                formData.append("file" + i, document.getElementById('afiles3_partyemail').files[i]);
            }else
            {
                var filenameerror = fi.files.item(i).name;
                swal({
                        title: "Alert",
                        text: "Your " + filenameerror + " file size " + Math.round(fsize/1024) + " MB. Please upload file size up to 20 MB",
                        type: "warning"
                    });
                var input = $("#afiles3_partyemail");
                var fileName = input.val();
                if(fileName) {
                    input.val('');
                }
            }
            
            }
            if(fileSize >= 20001) {
                swal({
                        title: "Alert",
                        text: "Total size of all files including the current file which you have selected is " + Math.round(fileSize/1024) + " MB. Please upload files which are in total of size up to 20 MB",
                        type: "warning"
                    });
                fileSize = fileSize - fsize;
                return false;
            }
            $('#totalFileSize_partyemail').val(fileSize);
            $.ajax({
                url: "email/emailattaupload",
                type: 'post',
                data: formData,
                dataType: 'html',
                async: true,
                processData: false,
                contentType: false,
                beforeSend: function () { $('#selectedloader1').addClass('loader');},
                complete: function () {
                $('#selectedloader1').removeClass('loader');
                },
                success : function(data) {
                    var input = $('input[name="afiles3_partyemail[]"]');
                    console.log(input);
                     var fileName = input.val();
                     console.log(fileName);
        
                    if(fileName) { 
                        input.val('');
                    }
                    $('#selectedFiles1').append(data);
                     
                },
                error : function(request) {
                    console.log(request.responseText);
                }
            });
        });
    });       
</script>



