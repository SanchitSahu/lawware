<?php
include('js/casecardjs.php');
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '')
            unset($holodaylist[$key]);
    }
}
if (isset($staff_details)) {
    $Uservacation = isset($this->data['staff_details'][0]->vacation) ? (explode("##", trim($this->data['staff_details'][0]->vacation))) : '';
    if (!empty($Uservacation)) {
        $Uservacation = array_map('trim', $Uservacation);
        foreach ($Uservacation as $key => $value) {
            if (is_null($value) || $value == '')
                unset($Uservacation[$key]);
        }
    }
}

$session_Arr = $this->session->userdata('user_data');
?>
<style>
    .chosen-container {
        width: 100% !important;
    }
    .pac-container{z-index: 9999;}
</style>
<!-- Rolodex add/edit popup START-->
<div id="addcontact" class="modal fade" style="z-index: 9999 !important;" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Contact</h4>
            </div>
            <div class="modal-body">
                <form id="addRolodexForm" method="post" enctype="multipart/form-data">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#name">Name</a></li>
                            <li class=""><a data-toggle="tab" href="#contact">Telephone/Email</a></li>
                            <li class=""><a data-toggle="tab" href="#details">Details</a></li>
                            <li class=""><a data-toggle="tab" href="#comments">Comments</a></li>
                            <li class=""><a data-toggle="tab" href="#address">Addresses</a></li>
                            <li class=""><a data-toggle="tab" href="#profile">Profile</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="name" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>General Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Salutation</label>
                                                        <input name="form_submit" type="hidden" value="1">
                                                        <select name="card_salutation" id="card_salutation" class="form-control <?php
                                                        if (isset($system_data[0]->cdsaldd) && $system_data[0]->cdsaldd == 1) {
                                                            echo 'cstmdrp';
                                                        } else {
                                                            echo 'select2Class';
                                                        }
                                                        ?>" >
                                                            <option value="">Select Salutation</option>
                                                            <?php foreach ($saluatation as $val) { ?>
                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <!--div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label>First</label>
                                                                <input type="text" id="card_first" name="card_first" class="form-control" value="" />
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label>Middle</label>
                                                                <input type="text" name="card_middle" class="form-control" value=""  />
                                                            </div>
                                                        </div>
                                                    </div-->
                                                    <div class="form-group">
                                                        <label>First</label>
                                                        <input type="text" id="card_first" name="card_first" class="form-control" value="" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Middle</label>
                                                        <input type="text" name="card_middle" class="form-control" value=""  />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last</label>
                                                        <input type="text" name="card_last" class="form-control" value="" />
                                                    </div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Suffix</label>
                                                        <select name="card_suffix" id="card_suffix" class="form-control <?php
                                                        if (isset($system_data[0]->cdsufdd) && $system_data[0]->cdsufdd == 1) {
                                                            echo 'cstmdrp';
                                                        } else {
                                                            echo 'select2Class';
                                                        }
                                                        ?>">
                                                            <option value="">Select Suffix</option>
                                                            <?php
                                                            if (isset($suffix)) {
                                                                foreach ($suffix as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <select name="card_title" class="form-control select2Class" >
                                                            <option value="">Select Title</option>
                                                            <?php
                                                            if (isset($title)) {
                                                                foreach ($title as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Category</label>
                                                        <select placeholder="Select Category" name="card_type" id="card_type" class="form-control <?php
                                                        if (isset($system_data[0]->cardtypedd) && $system_data[0]->cardtypedd == 1) {
                                                            echo 'cstmdrp';
                                                        } else {
                                                            echo 'select2Class';
                                                        }
                                                        ?>" required>
                                                                    <?php
                                                                    if (isset($category)) {
                                                                        foreach ($category as $val) {
                                                                            ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Popular</label>
                                                        <input type="text" name="card_popular" class="form-control" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Sal for Ltr</label>  <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="This should normally be left empty.  However, you may sometimes decide to fill this field in to override the default salutation in form letters.  For example : To the honorable Judge Smith

                                                                                       " id="sfl"></i>

                                                        <input type="text" name="card_letsal" class="form-control" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Firm/Corp </label><a href="#" class="btn btn-md btn-primary pull-right rolodex-company">A</a>
                                                        <input type="text" name="card2_firm" class="form-control" value="" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address Lookup</label>
                                                        <input id="autocomplete" placeholder="Enter your address" class="form-control" onFocus="geolocate()" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address 1</label>
                                                        <input type="text" name="card2_address1" id="street_number" class="form-control" value="" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address 2</label>
                                                        <input type="text" name="card2_address2" id="route" class="form-control" value="" />
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <label>City</label>
                                                                <input type="text" id="locality" name="card2_city" class="form-control" value="" />
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <label>State</label>
                                                                <input type="text" id="administrative_area_level_1" name="card2_state" class="form-control" value="" />
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <label>Zip</label>
                                                                <input type="text" id="postal_code" name="card2_zip" class="form-control" value="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Venue</label>
                                                        <input type="text" name="card2_venue" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>EAMS Name</label>
                                                        <input type="text" name="card2_eamsref" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="contact" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Personal Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Business</label>
                                                        <input type="text" name="card_business" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="card_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div>
                                                    <!--div class="form-group">
                                                        <label>Beeper</label>
                                                        <input type="text" name="card_beeper" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Home</label>
                                                        <input type="text" name="card_home" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Cell</label>
                                                        <input type="text" name="card_car" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="text" name="card_email" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <input type="text" name="card_notes" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Business Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Telephone 1</label>
                                                        <input type="text" name="card2_phone1" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Telephone 2</label>
                                                        <input type="text" name="card2_phone2" placeholder="(xxx) xxx-xxxx" class="form-control contact"  />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tax ID</label>
                                                        <input type="text" name="card2_tax_id" class="form-control" /><!-- onlyNumbers -->
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="card2_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax 2</label>
                                                        <input type="text" name="card2_fax2" placeholder="(xxx) xxx-xxxx" class="form-control contact" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="details" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Contact Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>SS No.</label>
                                                        <input type="text" name="card_social_sec" class="form-control social_sec"  placeholder="234-56-7898"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Date of Birth</label>
                                                        <input type="text" name="card_birth_date" pattern="\d{1,2}/\d{1,2}/\d{4}"  class="form-control datepickerClass date-field weekendsalert" data-mask="99/99/9999" placeholder="mm/dd/YY ex: 03/25/1995" id="datepicker" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>License No</label>
                                                        <input type="text" name="card_licenseno" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Specialty</label>

                                                        <select name="card_specialty" class="form-control select2Class">
                                                            <option value="">Select Specialty</option>
                                                            <?php
                                                            if (isset($specialty)) {
                                                                foreach ($specialty as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Maidan Name</label>
                                                        <input type="text" name="card_mothermaid" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Card No.</label>
                                                        <input type="text" name="card_cardcode" readonly class="form-control" value=""/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Firm No.</label>
                                                        <input type="text" name="card_firmcode" readonly class="form-control" value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Interpreter Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Interpreter</label>
                                                        <select name="card_interpret" class="form-control select2Class">
                                                            <option value="">Select Interpreter</option>
                                                            <option value="Y">Yes</option>
                                                            <option value="N">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group" id="data_1">
                                                        <label>Language</label>
                                                        <select name="card_language" class="select2Class form-control">
                                                            <!-- <option value="">Select Language</option>-->
                                                            <?php foreach ($languageList as $key => $val) { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                                                            <?php } ?>
                                                        </select>
    <!--                                                    <input type="text" name="card_language" class="form-control" />-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Original</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime1" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime2" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Original2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime3" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime4" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="comments" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card2_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="address" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Alternate Name and Addresses</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address1</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing1" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address2</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing2" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address3</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing3" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address4</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing4" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="profile" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Occupation</label>
                                                        <input type="text" name="card_occupation" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Employer</label>
                                                        <input type="text" name="card_employer" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Profile Picture</label>
                                                        <input type="file" name="profilepic" id="profilepic" class="form-control" accept="image/*" />
                                                        <span style="color:red">Max Upload size 5Mb</span>
                                                        <input type="hidden" name="hiddenimg" id="hiddenimg" value="" >
                                                        <span id="imgerror" style="color:red"></span>
                                                    </div>
                                                    <div id="profilepicDisp" style="display:none">
                                                        <div class="form-group">
                                                            <img id="dispProfile" height="50" width="50">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <input type="hidden" id="new_case" name="new_case" />
                        <input type="hidden" id="prospect_id" name="prospect_id" />
                        <input type="hidden" id="newRolodexCard" name="newRolodexCard" value="0"/>
                        <input type="hidden" id="oldCardValue" name="oldCardValue" value="0"/>
                        <input type="hidden" id="oldCaseNo" name="oldCaseNo" value="0"/>
                        <button type="button" id="addContact" class="btn btn-md btn-primary">Save</button>
                        <button type="button" id="cancelContact" class="btn btn-md btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Rolodex add/edit popup END-->

<div id="protactcase" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Protect Case</h4>
            </div>
            <div class="modal-body">
                <form name="protectCaseForm" id="protectCaseForm">
                    <?php
                    if (empty($display_details->password)) {
                        ?>
                        <p>Create a password to open this case.</p>
                        <p style="color:red">Leave the fields blank to remove password.</p>
                        <?php
                    } else {
                        ?>
                        <p>Enter password to open this case. Leave blank to clear password</p>
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <input type="hidden" name="pro_case_no" id="pro_case_no" value="<?php echo $this->uri->segment(3); ?>" >
                        <input type="password" id="propassword" name="propassword" placeholder="New Password" class="form-control" />
                    </div>
                    <div class="form-group">
                        <input type="password" id="procpassword" name="procpassword" placeholder="Confirm New Password" class="form-control" />
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="protectCasebtn">Proceed</a>
                        <a class="btn btn-md btn-primary" id="protectCasecnlbtn">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- TASK RELATED MODALS START  -->
<div id="addtask" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <form id="addTaskForm" name="addTaskForm" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <label>From</label>
                                        <select class="form-control select2Class" name="whofrom" id="whofrom">
                                            <?php
                                            if (isset($staffList)) {
                                                foreach ($staffList as $val) {
                                                    ?>
                                                    <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected="selected"' : ''; ?>><?php echo $val->initials; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>To</label>
                                        <select class="form-control select2Class staffTO staffvacation" name="whoto2" onchange="return remove_error('whoto2')" id="whoto2">
                                            <?php
                                            if (isset($staffList)) {
                                                foreach ($staffList as $val) {
                                                    ?>
                                                    <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Finish By</label>
                                        <input type="text" class="form-control weekendsalert alerttonholiday staffvacation sfinishby" name="datereq" onchange="return remove_error('taskdatepicker')" id="taskdatepicker" readonly />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task</label>
                                        <textarea class="form-control" name="event" style="height: 150px;resize: none;"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Color</label>
                                        <select class="form-control select2Class" name="color" id="color">
                                            <?php
                                            $jsoncolor_codes = json_decode(color_codes, true);
                                            foreach ($jsoncolor_codes as $k => $color) {
                                                ?>
                                                <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control select2Class" name="typetask">
                                            <option value="">Task Type</option>
                                            <option value=""></option>
                                            <option value="follow up">follow up</option>
                                            <option value="statute">statute</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Priority</label>
                                        <select class="form-control select2Class" name="priority" id="priority">
                                            <option value="3">Low</option>
                                            <option value="2">Medium</option>
                                            <option value="1">High</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phase</label>
                                        <select class="form-control select2Class" name="phase">
                                            <option value="">Select Phase</option>
                                            <option value=""></option>
                                            <option value="begining">Begining</option>
                                            <option value="middle">middle</option>
                                            <option value="final">final</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <!--label class="">Reminder&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="reminder" ></label-->
                                        <label class="container-checkbox pull-left">Reminder
                                            <input type="checkbox" class="treminder" name="reminder" onchange="showhidedt();">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" name="rdate" class="rdate" data-mask="99/99/9999" placeholder class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="form-group">
                                        <label>Time</label>
                                        <input type="text" name="rtime" class="rtime" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary pull-left">Save to Group</button>
                                        <button type="button" class="btn btn-md btn-primary" id="addTask" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                                        <span class="marg-top5 pull-right"><label>Email&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="notify" ></label></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Add/Edit Tasks Modal -->

<!-- Edit case specific information -->

<div id="TaskCompleted" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Task Completed</h4>
            </div>
            <div class="modal-body">
                <form id="edit_case_details" method="post">
                    <div class="panel-body">
                        <div class="float-e-margins">
                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="ibox-content">
                                        <div class="form-group">
                                            <textarea class="form-control" name="case_event" id="case_event" style="height: 100px;width: 100%;"></textarea>
                                            <input type='hidden' id='caseTaskId' name='caseTaskId'>
                                            <input type='hidden' id='caseno' name='caseno'>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter any information to add to the  task to be posted to case activity</label>
                                            <textarea class="form-control" name="case_extra_event" id="case_extra_event" style="height: 100px;width: 100%;"></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class='col-lg-6'>
                    <select id="case_category" name="case_category" class="form-control select2Class">
                        <?php
                        $jsonCategory = json_decode(category);
                        foreach ($jsonCategory as $key => $option) {
                            echo '<option value=' . $key . '>' . $option . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class='col-lg-6'>
                    <div class="form-btn text-right">
                        <button type="submit" id="saveCompletedTask" class="btn btn-primary btn-md">Done</button>
                        <button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="Task-list" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span id="record_count"> </span>Task For PNC </h4>
                <a class="btn pull-right btn-primary btn-sm marg-left5" href="#addtask1" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Task</a>
                <a class="btn pull-right btn-primary btn-sm marg-left5" href="#printtask" data-backdrop="static" data-keyboard="false" data-toggle="modal">Print</a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content" style="overflow:auto;">
                                <table class="table table-bordered task-dataTable1">
                                    <thead>
                                        <tr>
                                            <th id="th-date" width="10%">Date</th>
<!--                                                        <th id="th-time" width="10%">Reminder Time</th>-->
                                            <th id="th-from" width="5%">From</th>
                                            <th id="th-to" width="5%">To</th>
                                            <th id="th-finishby" width="10%">Finish By</th>
                                            <th id="th-type" width="5%">Type</th>
                                            <th id="th-priority" width="5%">Priority</th>
                                            <th id="th-event" width="30%" class="caseact_event">Event</th>
                                            <th id="th-phase" width="5%">Phase</th>
                                            <th id="th-completed" width="10%">Completed</th>
                                            <th id="th-action" width="10%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addtask1" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <form id="addTaskForm1" name="addTaskForm1" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <label>From</label>
                                        <select class="form-control select2Class" name="whofrom" id="whofrom">
                                            <?php
                                            if (isset($staffList)) {
                                                foreach ($staffList as $val) {
                                                    ?>
                                                    <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>To</label>
                                        <select class="form-control select2Class" name="whoto3" id="whoto3">
                                            <?php
                                            if (isset($staffList)) {
                                                foreach ($staffList as $val) {
                                                    ?>
                                                    <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Finish By</label>
                                        <input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" id="taskdatepicker2" readonly />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task</label>
                                        <textarea class="form-control" name="event" style="height: 150px;resize: none;"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Color</label>
                                        <select class="form-control select2Class" name="color" id="color">
                                            <?php
                                            $jsoncolor_codes = json_decode(color_codes, true);
                                            foreach ($jsoncolor_codes as $k => $color) {
                                                ?>
                                                <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control select2Class" name="typetask">
                                            <option value="">Task Type</option>
                                            <option value=""></option>
                                            <option value="follow up">follow up</option>
                                            <option value="statute">statute</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Priority</label>
                                        <select class="form-control select2Class" name="priority" id="priority">
                                            <option value="3">Low</option>
                                            <option value="2">Medium</option>
                                            <option value="1">High</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phase</label>
                                        <select class="form-control select2Class" name="phase">
                                            <option value="">Select Phase</option>
                                            <option value=""></option>
                                            <option value="begining">Begining</option>
                                            <option value="middle">middle</option>
                                            <option value="final">final</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <!--label class="">Reminder&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="reminder" ></label-->
                                            <label class="container-checkbox pull-left">Reminder
                                                <input type="checkbox" class="treminder" name="reminder1" onchange="showhidedt1();">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="text" name="rdate" class="rdate1" data-mask="99/99/9999" placeholder class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="form-group">
                                            <label>Time</label>
                                            <input type="text" name="rtime" class="rtime" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary pull-left">Save to Group</button>
                                        <button type="button" class="btn btn-md btn-primary" id="addTask1" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                                        <span class="marg-top5 pull-right"><label>Email&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="notify" ></label></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Add/Edit Tasks Modal -->
<!--Start Case Activity-->

<div id="addcaseact"  class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class=" modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Case Activity</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="add_new_case_activity" class="dropzone" enctype="multipart/form-data" name="add_new_case_activity" method="post" action="<?php echo base_url(); ?>cases/addEditActivity" onSubmit="">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-26">General</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-28">Properties</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-29">OLE</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-26" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>General</h5>
                                            <input type="hidden" value="<?php echo $this->uri->segment(3) ?>" name="caseActNo" id="caseActNo">
                                            <input type="hidden" value="" name="activity_no" id="activity_no">
                                        </div>
                                        <div class="ibox-content">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input type="text" class="form-control add_case_cal datepickerClass" name="add_case_cal" id="add_case_cal" value="<?php echo date('m/d/Y'); ?>" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Time</label>
                                                        <input type="text" class="form-control" name="add_case_time" id="add_case_time" data-uk-timepicker="{format:'12h'}"/>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Origin Initials</label>
                                                        <input type="text" value="<?php echo $session_Arr['initials']; ?>" name="orignal_ini" id="original_ini" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Initials</label>
                                                        <input type="text" value="<?php echo $session_Arr['initials']; ?>" name="last_ini" id="last_ini" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15 inline-block-100">
                                                        <button type="button" class="btn btn-md btn-primary pull-right marg-left10">Save</button>
                                                        <button type="button" class="btn btn-md btn-primary pull-right">Auto Recover</button>
                                                    </div>
                                                    <div class="form-group">
                                                        <label><a href="#" class="event_note_list">Event</a></label>
                                                        <textarea class="form-control" style="height: 150px;width: 100%;" name="activity_event" id="activity_event"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-27" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Checks, Fees and Costs</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 300px;">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Attorney/parralegal</label>
                                                    <select class="form-control select2Class" name="case_act_atty" id="case_act_atty">
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Time/Minutes</label>
                                                    <input class="form-control" type="text"  name="case_act_time"  id="case_act_time" onkeypress='validate(event)'/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Hourly Rate</label>
                                                    <input class="form-control" type="number" min="0"  value="0" name="case_act_hour_rate" id="case_act_hour_rate"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Costs / Check Amount</label>
                                                    <input class="form-control" type="number"  min="0" value="0.00" name="case_act_cost_amt" id="case_act_cost_amt"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <select class="form-control select2Class" name="activity_title" id="activity_title">
                                                        <?php
                                                        $jsonactivity_type = json_decode(activity_title);
                                                        foreach ($jsonactivity_type as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Type of Fee or Cost</label>
                                                    <select name="activity_fee" id="activity_fee" class="form-control select2Class">
                                                        <?php
                                                        $jsonfee_cost = json_decode(fee_cost);
                                                        foreach ($jsonfee_cost as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="activity_fee1" id="activity_fee1"  style="display: none"> </div>
                                                <div class="form-group">
                                                    <label>Fee Classification</label>
                                                </div>
                                                <div class="form-group">
                                                    <span><label><input type="radio" value="1" class="i-checks" name="activity_classification"> None </label></span>
                                                    <span><label><input type="radio" value="2" class="i-checks" name="activity_classification"> Account Payable </label></span>
                                                    <span><label><input type="radio" value="3" class="i-checks" name="activity_classification"> Account Receivable </label></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-28" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Genaral Note Properties</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Category</label>
                                                        <select id="case_category" name="case_category" class="form-control select2Class">
                                                            <?php
                                                            $jsonfee_cost = json_decode(fee_cost);
                                                            $jsonCategory = json_decode(category);
                                                            foreach ($jsonCategory as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Color</label>
                                                        <select id="color_codes" name="color_codes" class="form-control select2Class">
                                                            <?php
                                                            $jsoncolor_codes = json_decode(color_codes);
                                                            foreach ($jsoncolor_codes as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option[0] . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Security</label>
                                                        <select name="security" id="security" class="form-control select2Class">
                                                            <?php
                                                            $jsonsecurity = json_decode(security);
                                                            foreach ($jsonsecurity as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Type of Note</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <span><label><input type="checkbox" name="activity_mainnotes" id="activity_mainnotes" class="i-checks" value="1"> Show In All </label></span>
                                                        <span><label><input type="checkbox" name="activity_redalert" id="activity_redalert" class="i-checks" value="1"> Red Alert </label></span>
                                                        <span><label><input type="checkbox" name="activity_upontop" id="activity_upontop" class="i-checks" value="1"> Up On Top </label></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Type of activity</label>
                                                        <select class="form-control select2Class" id="activity_typeact" name="activity_typeact">
                                                            <?php
                                                            $jsonactivity_type = json_decode(type_of_activity);
                                                            foreach ($jsonactivity_type as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="dropzonePreview" class="dz-default dz-message">
                                            <span class="label label-info">Click here to attach file</span>
                                        </div>
                                        <div class="row documentAttch hidden">
                                            <div class="col-sm-12">

                                                <div class="col-sm-2"><div class="col-sm-2">Document:</div> <div class="documentPath col-sm-10"></div>Activity No: </div><div class="activityNo col-sm-10"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-29" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="ibox-title">
                                                    <h5>General Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Document Type</label>
                                                        <div class="fileContainer upload-button marg-top15">
                                                            <input type="file" class="" id="uploadBtn3">
                                                            <button type="button" id="uploadFile3" class="btn btn-sm btn-primary">Upload</button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Document Type</label>
                                                        <select class="form-control select2Class" name="activity_doc_type" id="activity_doc_type"/>
                                                        <?php
                                                        $jsonactivity_doc_type = json_decode(activity_doc_type);
                                                        foreach ($jsonactivity_doc_type as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Style</label>
                                                        <select class="form-control select2Class" name="activity_style" id="activity_style"/>
                                                        <?php
                                                        $jsonactivity_style = json_decode(activity_style);
                                                        foreach ($jsonactivity_style as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Origin Initials</label>
                                                        <input type="text" class="form-control" name="activity_o_init" id="activity_o_init" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Initials</label>
                                                        <input type="text" class="form-control" name="activity_l_init" id="activity_l_init"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <span><label>Override <input type="checkbox" class="i-checks" name="activity_override" id="activity_override"></label></span>
                                                    </div>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Last Initials</label>
                                                        <input type="text" class="form-control" name="activity_last_init" id="activity_last_init"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input type="text" class="form-control injurydate" data-mask="99/99/9999" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Time</label>
                                                        <input type="text" id="oletime" class="form-control injurytime" />
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-30" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Law Form Letter Margins</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 300px;">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Top</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_TM" id="case_act_TM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Bottom</label>
                                                    <input type="number" step="0.10" min="-1.00" value="0.00" name="case_act_BM" id="case_act_BM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Left</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_LM" id="case_act_LM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Right</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_RM" id="case_act_RM" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Page Width</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_PW" id="case_act_PW" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Page Height</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_PH" id="case_act_PH" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Orientation</label>
                                                    <select name="case_act_orientation" id="case_act_orientation" class="form-control select2Class">
                                                        <option value="0"></option>
                                                        <option value="1">Portrait</option>
                                                        <option value="2">Landscape</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Copies</label>
                                                    <input type="number" value="0" name="case_act_copies" id="case_act_copies" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-31" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Billing</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 415px;">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Entry Type</label>
                                                    <select class="form-control select2Class" name="activity_entry_type" id="activity_entry_type">
                                                        <option value="">Select Entry Type</option>
                                                        <?php
                                                        $jsonactivity_entry_type = json_decode(activity_entry_type);
                                                        foreach ($jsonactivity_entry_type as $key => $option) {
                                                            echo '<option data-key=' . $option[1] . ' value=' . $key . '>' . $option[0] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group hidden">
                                                    <label>Freeze Until</label>
                                                    <span class="freezeUntil"></span>
                                                </div>
                                            </div>


                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Team no</label>
                                                    <input type="number" class="form-control" value="0" min='0'id="activity_teamno" name="activity_teamno" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Attorney/Staff</label>
                                                    <select class="form-control select2Class" id="activity_staff" name="activity_staff">
                                                        <?php
                                                        $username = isset($username) ? $username : $this->session->userdata('user_data')['username'];
                                                        if (isset($staffList)) {
                                                            foreach ($staffList as $val) {
                                                                ?>
                                                                <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $username) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Short Description</label>
                                                    <textarea class="form-control" style="width: 100%;height: 100px;" id="activity_short_desc" name="activity_short_desc"></textarea>
                                                </div>

                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Billing Cycle</label>
                                                    <input type="text" readonly name="activity_biling_cycle" id="activity_biling_cycle" value="<?php echo date('m/d/Y'); ?>" class="form-control datepickerClass" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of Last Payment <span class="dateoflastpayment"></span></label>
                                                    <a class="btn btn-sm btn-primary pull-right no-bottom-margin" href="#adddefault" data-toggle="modal">Defaults</a>
                                                </div>
                                                <div class="form-group">
                                                    <label>Event/Detailed Description</label>
                                                    <textarea class="form-control" style="width: 100%;height: 150px;" id="activity_event_desc" name="activity_event_desc"></textarea>
                                                </div>


                                                <div id="payment" class="billingtab tab1 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Payment Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 370px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Check postmark Date</label>
                                                                        <input type="text" readonly class="form-control datepickerClass" name="activity_postmark_date" id="activity_postmark_date"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Payment due Date</label>
                                                                        <input type="text" readonly class="form-control datepickerClass" name="activity_payment_due" id="activity_payment_due"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Amount Paid</label>
                                                                        <input type="number" class="form-control" step="5" value="0" name="activity_amount" id="activity_amount"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Late Fee</label>
                                                                        <input type="number" class="form-control" step="5" value="0" name="activity_latefee" id="activity_latefee"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Check no</label>
                                                                        <input type="text" class="form-control numberinput" name="activity_check_no" id="activity_check_no" />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="fee" class="billingtab tab2 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 300px; height: 100%;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Total Time(Hours)</label>
                                                                        <input type="number" value="0.1000"  min='0' class="form-control" name="activity_bi_hour" id="activity_bi_hour" step="0.1000"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Total Time(minutes)</label>
                                                                        <input type="number" value="0"  min='0' class="form-control" name="activity_bi_min" id="activity_bi_min"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Hourly Rate</label>
                                                                        <input type="number" value="0"  min='0' class="form-control" name="activity_bi_rate" id="activity_bi_rate"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Total Fee</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" min='0' step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="cost" class="billingtab tab3 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Cost Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 155px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Cost</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_cost" id="activity_bi_cost" step="5"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Your Check No</label>
                                                                        <input type="text" maxlength="20" class="form-control" name="activity_check_no" id="activity_check_no"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="flat-fee" class="billingtab tab4 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Flat Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Flat Fee Amount</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="late-fee" class="billingtab tab5 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Late Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Late Fee Amount</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_latefee" id="activity_bi_latefee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="credit-account" class="billingtab tab6 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Adjustment/Credit Account</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Credit Account</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_creditac" id="activity_bi_creditac" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="debit-account" class="billingtab tab7 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins" >
                                                            <div class="ibox-title">
                                                                <h5>Adjustment/Debit Account</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Debit Account</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="none" class="billingtab tab8 hidden" >

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center marg-top15">
                                <button type="submit" class="btn btn-md btn-primary"   style="margin-bottom:0px;">Save</button>

                                <button type="button" class="btn btn-md btn-primary" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-md btn-primary CaseActEmail">Email</button>
                                <button type="button" class="btn btn-md btn-primary CaseActPrint">Print</button>
                                <button type="button" class="btn btn-md btn-primary pull-right">Scan</button>
                            </div>
                            <!--<span><center><h4>Upload file or drag & drop<h4></center></span>-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- End Case Activity-->
<!--start Event List-->

<div id="eventnotelist" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select a case activity note</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <span><label><input type="checkbox" name="activity_list" id="activity_list" class="i-checks category_event_list"> Show All </label></span>
                                    </div>
                                    <div class="col-sm-6">
                                        <select id="case_category_event" name="case_category_event" class="form-control select2Class category_event_list">
                                            <?php
                                            $jsonCategory = json_decode(category);
                                            foreach ($jsonCategory as $key => $option) {
                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive rolodexsearch">
                                                <table class="table table-bordered table-hover table-striped dataTables-example" id="caseEventlist" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <button type="button" class="btn btn-md btn-primary" id="caseactivityevent" >Process</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<!--End Event List-->

<div id="printtask" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Task</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <form id="taskPrint" name="taskPrint">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <!-- <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Days to Print</label>
                                        <input type="number" name="printDay" class="form-control"/>
                                    </div>
                                </div> -->
                                <div class="col-sm-12" id="print_datepicker">
                                    <label>Days to Print</label>
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_printData" class="input-small form-control" name="start_printData" placeholder="From Date" readonly>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_printData" class="input-small form-control" name="end_printData" placeholder="To Date" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Type of Report</label>
                                        <select class="form-control" name="print_type">
                                            <?php
                                            $repType = json_decode(print_type);
                                            foreach ($repType as $key => $val) {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <span><label> <input type="checkbox" name="namecheck" class="i-checks">Include the name before each task</label></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center marg-top15">
                                        <button type="button" id="btn-taskPrint" class="btn btn-md btn-primary">Proceed</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="addRelatedCase" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Related Case</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <form id="addRelatedcaseForm" name="addRelatedcaseForm" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group ui-widget">
                                        <label>Enter the case number to Relate this case</label>
                                        <input id="relatedcasenumber" name="relatedcasenumber" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary" id="addRelatedcase" >Process</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="show_conform_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <div class="row">
                            <div class="form-group">
                                <label>If you make changes in the rolodex,it may affect several cases</label>
                            </div>
                            <div class="form-group">
                                <label><b>Click Yes</b> </label>: To make changes for this case only
                            </div>
                            <div class="form-group">
                                <label><b>Click No </b> </label>: To make changes in the rolodex which may affect several cases</label>
                            </div>
                            <div class="form-group">
                                <label><b>Click Cancel</b> </label> :To Cancel and not make any changes at all</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="relodex_yes" >Yes</button>
                    <button type="button" class="btn btn-md btn-primary" id="relodex_no" >No</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="copyemail">
    <textarea id="copytoemail" STYLE="display:none;"></textarea>
</div>

<!-- TASK RELATED MODALS END -->
<!-- start firm companylist  -->
<div id="firmcompanylist" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md firmcompanylist">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Firm Company List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="form-group">
                    <form action="#" role="form" method="post" id="companylistForm" name="companylistForm">
                        <label>Enter Firm or Company Name</label>
                        <input type="text" class="form-control" name="rolodex_search_text_name" id="rolodex_search_text_name"/>
                        <input type="hidden" class="form-control" name="rolodex_search_parties_name" id="rolodex_search_parties_name" value="rolodex-company" />
                    </form>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped dataTables-example" id="rolodexsearchResult_data">
                                    <thead>
                                        <tr>
                                            <th>Firm/Company</th>
                                            <th>City</th>
                                            <th>Address</th>
                                            <th>Address2</th>
                                            <th>Phone</th>
                                            <th>Name</th>
                                            <th>State</th>
                                            <th>Zip</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="viewSearchedPartieDetails" >Attach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>
<!--  end firmcompanylist --->
<div id="editcasedetail" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Case Information</h4>
            </div>
            <div class="modal-body">
                <form id="edit_case_details" method="post">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#case_info">Case</a></li>
                            <li class=""><a data-toggle="tab" href="#staff_info">Staff</a></li>
                            <li class=""><a data-toggle="tab" href="#case_dates">Dates</a></li>
                            <li class=""><a data-toggle="tab" href="#quick_note">Quick Notes</a></li>
                            <li class=""><a data-toggle="tab" href="#caption_tab">Caption</a></li>
                            <li class=""><a data-toggle="tab" href="#messages">Messages</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="case_info" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Case Information</h5>
                                                </div>
                                                <div class="ibox-content marg-bot15">
                                                    <div class="form-group">
                                                        <label>Type of Case</label>
                                                        <?php
                                                        if (isset($casetypedd) && $casetypedd == '1') {
                                                            $classTpDD = 'cstmdrp';
                                                        } else {
                                                            $classTpDD = 'select2Class';
                                                        }
                                                        ?>
                                                        <select class="form-control <?php echo $classTpDD; ?>" id="editCaseType" name="editCaseType">
                                                            <?php foreach ($filters['casetype'] as $key => $val) { ?>
                                                                <option value="<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Case Status</label>
                                                        <?php
                                                        if (isset($casestatdd) && $casestatdd == '1') {
                                                            $casestatdd = 'cstmdrp';
                                                        } else {
                                                            $casestatdd = 'select2Class';
                                                        }
                                                        ?>
                                                        <select class="form-control <?php echo $casestatdd; ?>" id="editCaseStat" name="editCaseStat">
                                                            <?php foreach ($filters['casestat'] as $key => $val) { ?>
                                                                <option value="<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Other Cases</label>
                                                        <select class="form-control select2Class" id="editOtherClass" name="editOtherClass">
                                                            <option value=""></option>
                                                            <option value="No">No</option>
                                                            <option value="Yes">Yes</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>File Information</h5>
                                                </div>
                                                <div class="ibox-content marg-bot15">
                                                    <div class="form-group">
                                                        <label>Your File No</label>
                                                        <input type="text" class="form-control" id="fileno" name="fileno"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>File Location</label>
                                                        <input type="text" class="form-control" id="fileloc" name="fileloc"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Closed File No</label>
                                                        <input type="text" class="form-control" id="closedfileno" name="closedfileno"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Sub-type</label>
                                                                <select name="editCaseSubType" class="form-control select2Class">
                                                                    <option value="">Select SubType</option>
                                                                    <?php
                                                                    if (isset($sub_type)) {
                                                                        foreach ($sub_type as $val) {
                                                                            ?>
                                                                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <label>Venue</label>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <a  href="#" class="venuelist">Venue</a>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="editCaseVenue1" id="editCaseVenue1" readonly />
                                                                    <input type="hidden" class="form-control" name="editCaseVenue" id="editCaseVenue"  readonly />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="staff_info" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox-title">
                                                    <h5>Staff Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <?php
                                                        if (isset($cdstaffdd) && $cdstaffdd == '1') {
                                                            $casestaffdrp = 'cstmdrp';
                                                        } else {
                                                            $casestaffdrp = 'select2Class';
                                                        }
                                                        ?>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Attorney Responsible</label>
                                                                <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttyResp" name="editCaseAttyResp">
                                                                    <?php foreach ($filters['atty_resp'] as $key => $val) { ?>
                                                                        <option value="<?php echo $val['atty_resp']; ?>"><?php echo $val['atty_resp']; ?></option>
                                                                    <?php } ?></select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Attorney Handling</label>
                                                                <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttyHand" name="editCaseAttyHand">
                                                                    <?php foreach ($filters['atty_hand'] as $key => $val) { ?>
                                                                        <option value="<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Paralegal Handling</label>
                                                                <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttyPara" name="editCaseAttyPara">
                                                                    <?php foreach ($filters['para_hand'] as $key => $val) { ?>
                                                                        <option value="<?php echo $val['para_hand']; ?>"><?php echo $val['para_hand']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Secretary Handling</label>
                                                                <select class="form-control <?php echo $casestaffdrp; ?>" id="editCaseAttySec" name="editCaseAttySec">
                                                                    <?php foreach ($filters['sec_hand'] as $key => $val) { ?>
                                                                        <option value="<?php echo $val['sec_hand']; ?>"><?php echo $val['sec_hand']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="case_dates" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="ibox-title inline-block-100">
                                                    <h5 class="">Case Dates/Important Dates</h5>
                                                    <a class="btn pull-right btn-primary btn-sm" href="#addtask" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add P&S Task</a>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Entered</label>
                                                                <input type="text" class="form-control dates datepicker" id="casedate_entered" name="casedate_entered"/>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Open</label>
                                                                <input type="text" class="form-control dates datepicker" id="casedate_open" name="casedate_open" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Follow Up</label>
                                                                <input type="text" class="form-control"  id="casedate_followup" name="casedate_followup"/>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Closed</label>
                                                                <input type="text" class="form-control" id="casedate_closed" name="casedate_closed"/>
                                                            </div>
                                                            <?php
                                                            if (isset($refdt) && $refdt == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label>Referred</label>
                                                                    <input type="text" class="form-control dates datepicker" id="casedate_referred" name="casedate_referred"/>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?php
                                                            if (isset($ref) && $ref == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label>Referred by</label>
                                                                    <select class="form-control select2Class" name="case_reffered_by" id="case_reffered_by">
                                                                        <option value=""></option>
                                                                        <?php
                                                                        if (isset($case_cards) && $case_cards != '') {
                                                                            foreach ($case_cards as $key => $details) {
                                                                                $name = "";

                                                                                if ($details->type == 'APPLICANT') {
                                                                                    $name = $details->last . ', ' . $details->first;
                                                                                } else if ($details->type == 'LIEN' || $details->type == 'BOARD' || $details->type == 'INSURANCE' || $details->type == 'ATTORNEY' || $details->type == 'EMPLOYER') {
                                                                                    $name = $details->firm;
                                                                                } else if ($details->type == 'DR' || strrpos($details->type, 'DR') >= 0) {
                                                                                    if ($details->firm == '') {
                                                                                        $name = $details->last . ', ' . $details->first;
                                                                                    } else {
                                                                                        $name = $details->firm;
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <option value="<?php echo $details->cardcode; ?>"><?php echo $name; ?></option>

                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <?php
                                                            }
                                                            if (isset($pns) && $pns == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label>P & S (Is applicant P & S?)</label>
                                                                    <select class="form-control select2Class" name="case_ps" id="case_ps">
                                                                        <option value=""></option>
                                                                        <option value="Y">Yes</option>
                                                                        <option value="N">No</option>
                                                                    </select>
                                                                </div>
                                                                <?php
                                                            }
                                                            if (isset($pnsdt) && $pnsdt == 1) {
                                                                ?>
                                                                <div class="form-group">
                                                                    <label>P & S Date</label>
                                                                    <input type="text" class="form-control dates datepicker" id="casedate_psdate" name="casedate_psdate"/>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="quick_note" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="ibox-title">
                                                    <h5> Add Note</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <textarea class="form-control" style="width: 100%;height: 200px;" name="editcase_quicknote" id="editcase_quicknote"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="caption_tab" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox-title">
                                                    <h5>Caption</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <?php
                                                        if (isset($CaptionAccess) && $CaptionAccess->captionAccess == 1) {
                                                            $displayCptn = '';
                                                        } else {
                                                            $displayCptn = 'readonly';
                                                        }
                                                        ?>
                                                        <textarea class="form-control" style="height: 300px;width: 100%;" name="editcase_caption" id="editcase_caption" <?php echo $displayCptn; ?> ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="messages" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox-title">
                                                    <h5>Messages</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Message 1</label>
                                                        <textarea class="form-control" name="caseedit_message1" id="caseedit_message1" style="height: 100px;width: 100%;background:<?php echo isset($main1->msg1) ? $main1->msg1 : '' ?>;color:<?php echo isset($main1->msg1c) ? $main1->msg1c : '' ?>"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Message 2</label>
                                                        <textarea class="form-control" name="caseedit_message2" id="caseedit_message2" style="height: 100px;width: 100%;background:<?php echo isset($main1->msg2) ? $main1->msg2 : '' ?>;color:<?php echo isset($main1->msg2c) ? $main1->msg2c : '' ?>"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Message 3</label>
                                                        <textarea class="form-control" name="caseedit_message3" id="caseedit_message3" style="height: 100px;width: 100%;background:<?php echo isset($main1->msg3) ? $main1->msg3 : '' ?>;color:<?php echo isset($main1->msg3c) ? $main1->msg3c : '' ?>"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $caseNo = $this->uri->segment(3); ?>
                    <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo ?>">
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-btn text-center">

                    <button type="submit" id="savecase" class="btn btn-primary btn-md">Save</button>
                    <button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addinjury" data-backdrop="static" data-keyboard="false" class="modal fade addinjuryalignment" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Injury</h4>
            </div>
            <div class="modal-body">
                <form action="" role="form" id="addInjuryForm">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general">General</a></li>
                            <li><a data-toggle="tab" href="#court">Court</a></li>
                            <li class=""><a data-toggle="tab" href="#employers">Employers</a></li>
                            <li class=""><a data-toggle="tab" href="#carriers">Carriers</a></li>
                            <li class=""><a data-toggle="tab" href="#attorneys">Attorneys</a></li>
                            <li class=""><a data-toggle="tab" href="#1">1</a></li>
                            <li class=""><a data-toggle="tab" href="#2-6">2-6</a></li>
                            <li class=""><a data-toggle="tab" href="#7-10">7-10</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-22">Important Dates</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-23">Add'l Info</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-md btn-primary " onclick="attachParty('general');" >Attach</a>
                                                </div>
                                                <div class="ibox-title">
                                                    <h5>Application Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <input type="hidden" name="field_injury_id" class="form-control" value="0"/>
                                                    <div class="form-group">
                                                        <label>First Name</label>
                                                        <input type="text" name="field_first" class="form-control" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->first;
                                                        }
                                                        ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input type="text" name="field_last" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->last;
                                                        }
                                                        ?>" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" name="field_address" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->address1;
                                                        }
                                                        ?>" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" name="field_city" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->city;
                                                        }
                                                        ?>" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" name="field_state" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->state;
                                                        }
                                                        ?>" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" name="field_zip_code" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->zip;
                                                        }
                                                        ?>" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>SS No.</label>
                                                        <input type="text" name="field_social_sec" class="form-control" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->social_sec;
                                                        }
                                                        ?>" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>AOE/COE Status</label>
                                                        <input type="text" name="field_aoe_coe_status" class="form-control" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Other Dates for this Injury</h5>
                                                </div>
                                                <div class="ibox-content marg-bot15">
                                                    <div class="form-group">
                                                        <label>DOR Field</label>
                                                        <input type="text" name="field_dor_date" class="form-control injurydate" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>S & W</label>
                                                        <input type="text" name="field_s_w" class="form-control injurydate" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Liab</label>
                                                        <input type="text" name="field_liab" class="form-control injurydate" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Other SOL</label>
                                                        <input type="text" name="field_other_sol" class="form-control injurydate" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Follow up</label>
                                                        <input type="text" name="field_follow_up" class="form-control injurydate" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>General Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Status</label>
                                                        <input type="text" name="field_app_status" value="OPEN" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Office No</label>
                                                        <input type="text" name="field_office" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="court" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="ibox-content">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select name="field_status2" class="form-control select2Class">
                                                        <option value="">Select Status</option>
                                                        <?php
                                                        $court_status = json_decode(injuryStatus);
                                                        foreach ($court_status as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Amended Application</label>
                                                    <select name="field_amended" class="form-control select2Class">
                                                        <option value="">Select Amended</option>
                                                        <?php
                                                        $amendedApp = json_decode(amendedApplication);
                                                        foreach ($amendedApp as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>WCAB No.</label>
                                                    <input type="text" name="field_case_no" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>EAMS ADJ NO (Numbers Only)</label>
                                                    <input type="text" name="field_eamsno" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="employers" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-md btn-primary " onclick="attachParty('employers1');"  >Attach</a>
                                                </div>
                                                <div class="ibox-title">
                                                    <h5>Employer Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Employer</label>
                                                        <input type="text" name="field_e_name" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" name="field_e_address" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" name="field_e_city" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" name="field_e_state" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" name="field_e_zip" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" name="field_e_phone" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="field_e_fax" class="form-control contact"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-md btn-primary" onclick="attachParty('employers2');" >Attach</a>
                                                </div>
                                                <div class="ibox-title">
                                                    <h5>Employer 2 Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Employer</label>
                                                        <input type="text" name="field_e2_name" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" name="field_e2_address" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" name="field_e2_city" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" name="field_e2_state" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" name="field_e2_zip" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" name="field_e2_phone" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="field_e2_fax" class="form-control contact" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="carriers" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-md btn-primary" onclick="attachParty('carriers1');" >Attach</a>
                                                </div>
                                                <div class="ibox-title">
                                                    <h5>Carrier Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Salutation</label>
                                                        <select name="field_i_adjsal" class="form-control select2Class" >
                                                            <option value="">Select Salutation</option>
                                                            <?php foreach ($saluatation as $val) { ?>
                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>First</label>
                                                        <input type="text" name="field_i_adjfst" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last</label>
                                                        <input type="text" name="field_i_adjuster" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Claim No.</label>
                                                        <input type="text" name="field_i_claimno" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Carrier</label>
                                                        <input type="text" name="field_i_name" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" name="field_i_address" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" name="field_i_city" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" name="field_i_state" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" name="field_i_zip" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" name="field_i_phone" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="field_i_fax" class="form-control contact" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-md btn-primary " onclick="attachParty('carriers2');">Attach</a>
                                                </div>
                                                <div class="ibox-title">
                                                    <h5>Carrier 2 Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Salutation</label>
                                                        <select name="field_i2_adjsal" class="form-control select2Class" >
                                                            <option value="">Select Salutation</option>
                                                            <?php foreach ($saluatation as $val) { ?>
                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>First</label>
                                                        <input type="text" name="field_i2_adjfst" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last</label>
                                                        <input type="text" name="field_i2_adjuste" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Claim No.</label>
                                                        <input type="text" name="field_i2_claimno" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Carrier</label>
                                                        <input type="text" name="field_i2_name" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" name="field_i2_address" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" name="field_i2_city" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" name="field_i2_state" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" name="field_i2_zip" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" name="field_i2_phone" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="field_i2_fax" class="form-control contact" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="attorneys" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-md btn-primary "  onclick="attachParty('attorneys1');">Attach</a>
                                                </div>
                                                <div class="ibox-title">
                                                    <h5>Defence Attorney Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>First</label>
                                                        <input type="text" name="field_d1_first" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last</label>
                                                        <input type="text" name="field_d1_last" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Firm</label>
                                                        <input type="text" name="field_d1_firm" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" name="field_d1_address" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" name="field_d1_city" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" name="field_d1_state" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" name="field_d1_zip" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" name="field_d1_phone" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="field_d1_fax" class="form-control contact" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <a href="#" class="btn btn-md btn-primary "  onclick="attachParty('attorneys2');">Attach</a>
                                                </div>
                                                <div class="ibox-title">
                                                    <h5>Defence Attorney Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>First</label>
                                                        <input type="text" name="field_d2_first" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last</label>
                                                        <input type="text" name="field_d2_last" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Firm</label>
                                                        <input type="text" name="field_d2_firm" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" name="field_d2_address" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" name="field_d2_city" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" name="field_d2_state" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <input type="text" name="field_d2_zip" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" name="field_d2_phone" class="form-control contact" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input type="text" name="field_d2_fax" class="form-control contact" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="1" class="tab-pane injuryalignment">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content injuryone marg-bot10">
                                            <div class="form-group">
                                                <label>Date of Injury for EAMS and all new Injuries</label>
                                                <div class="i-checks"><label> <input type="checkbox" name="field_ct1" id="field_ct1" value=""> <i></i>CT</label></div>
                                                <div class="input-daterange" id="datepicker1">
                                                    <input type="text" class="input-sm form-control injurydate"  name="field_doi" id="field_doi"> <!-- name="start" -->
                                                    <input type="text" class="input-sm form-control injurydate" readonly name="field_doi2" id="field_doi2"><!-- name="end" -->
                                                </div>
                                                <label id="field_doi2-error" class="error" for="field_doi2"></label>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-12">
                                                        <div style="line-height: 50px;"><span><b>1.</b></span>&nbsp;The Injured employee, born </div>
                                                        <div style="line-height: 40px;">a <input type="text" name="field_adj1b"  class="form-control bg-field" /> </div>
                                                        <div style="line-height: 40px;">at <input type="text" name="field_adj1d" id="field_adj1d"  class="form-control bg-field" /></div>
                                                        <div style="line-height: 40px;" class="injuryaddalignment">State <input type="text" name="field_adj1d3"  class="form-control" /></div>
                                                    </div>
                                                    <div class="col-md-6 col-xs-12">
                                                        <div style="line-height: 40px;"><input type="text" name="field_adj1a" id="field_adj1a" class="form-control" /> While employed as</div>
                                                        <div style="line-height: 40px;">on&nbsp;&nbsp; <input type="text" name="field_adj1c" id="field_adj1c"  class="form-control  bg-field" /></div>
                                                        <div style="line-height: 40px;">City <input type="text" name="field_adj1d2"  class="form-control  bg-field" /></div>
                                                        <div style="line-height: 40px;">Zip&nbsp; <input type="text" class="form-control bg-field" name="field_adj1d4" /></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="ibox-content injuryone">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Body Parts (EAMS App Only)</label>
                                                        <?php $bodyParts = json_decode(bodyParts); ?>
                                                        <select class="form-control select2Class" name="field_pob1" >
                                                            <option value="">Select Body Part 1</option>
                                                            <?php foreach ($bodyParts as $key => $val) { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <select class="form-control select2Class" name="field_pob2" >
                                                            <option value="">Select Body Part 2</option>
                                                            <?php foreach ($bodyParts as $key => $val) { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <select class="form-control select2Class" name="field_pob3" >
                                                            <option value="">Select Body Part 3</option>
                                                            <?php foreach ($bodyParts as $key => $val) { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <select class="form-control select2Class" name="field_pob4" >
                                                            <option value="">Select Body Part 4</option>
                                                            <?php foreach ($bodyParts as $key => $val) { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <select class="form-control select2Class" name="field_pob5" >
                                                            <option value="">Select Body Part 5</option>
                                                            <?php foreach ($bodyParts as $key => $val) { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Other Body Parts (Legacy and EAMS App)</label>
                                                        <textarea class="form-control" name="field_adj1e"  style="width: 100%;height: 150px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="2-6" class="tab-pane twosix">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-content injuryone">
                                            <div class="form-group">
                                                <label>2. The Injury occured as follows:</label>
                                                <textarea class="form-control" name="field_adj2a"  style="width: 100%;height: 150px;"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>3. Actual earnings at the time were</label>
                                                <input type="text" name="field_adj3a"  class="form-control full-width">
                                            </div>
                                            <div class="form-group">
                                                <label>4. The Injury caused disabilty as follows:</label>
                                                <input type="text" name="field_adj4a" class="form-control full-width">
                                            </div>
                                            <div class="form-group">
                                                <p><span style="display:inline;"><b>5.</b></span>&nbsp;Comp pd <input type="text" name="field_adj5a" maxlength="1" class="form-control xs-field"> Total pd <input type="text" name="field_adj5b" class="form-control"> Weekly rate <input type="text" name="field_adj5c" class="form-control"> Date last pmt <input type="text" name="field_adj5d" class="form-control injurydate"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>6. Unemployment Insurance or unemployment compensetion disability benefits have been received since the date of injury</label>
                                                <input type="text" name="field_adj6a" maxlength="1" class="form-control xs-field">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="7-10" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-content injuryone">
                                            <div class="form-group">
                                                <label>7. The Injury occured as follows:</label>
                                                <p>Medical treatment was received <input type="text" name="field_adj7a" maxlength="1" class="form-control xs-field"> Date of last treatment <input type="text" name="field_adj7b" class="form-control injurydate"> All treatment was furnished by the employer or insurance company <input type="text" name="field_adj7c" maxlength="1" class="form-control xs-field">
                                                    Other treatment was provided or paid for by <input type="text" name="field_adj7d" class="form-control"> Did Medi-cal pay for any healthcare related to this claim <input type="text" name="field_adj7e" maxlength="1" class="form-control xs-field">
                                                    Doctors not providing or paid for by employer or insurance company who treated or examined for this injury are <input type="text" name="field_doctors" class="form-control full-width"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>8. Other Cases have been field for industrial Injuries by this employee as follows:</label>
                                                <input type="text" name="field_adj8a" class="form-control full-width">
                                            </div>
                                            <div class="form-group">
                                                <label>9. This application is field because of a disagreement regarding liabilty for:</label>
                                                <p> TD indemnity <input type="text" name="field_adj9a" value="<?php echo isset($injury9->tdind) ? $injury9->tdind : '' ?>" maxlength="1" class="form-control xs-field">
                                                    PD indemnity <input type="text" value="<?php echo isset($injury9->pdind) ? $injury9->pdind : '' ?>" name="field_adj9b" maxlength="1" class="form-control xs-field">
                                                    Reimbursement for medical expanse <input type="text" name="field_adj9c" value="<?php echo isset($injury9->Reformeexpense) ? $injury9->Reformeexpense : ''; ?>" class="form-control bg-field">
                                                    Medical treatment <input type="text" name="field_adj9d" value="<?php echo isset($injury9->meditreatment) ? $injury9->meditreatment : ''; ?>" maxlength="1" class="form-control xs-field">
                                                    Compansation at proper rate <input type="text" name="field_adj9e" value="<?php echo isset($injury9->comatprorate) ? $injury9->comatprorate : ''; ?>"class="form-control xs-field">
                                                    Rehabilitation <input type="text" name="field_adj9f" value="<?php echo isset($injury9->rehabilitation) ? $injury9->rehabilitation : ''; ?>" maxlength="1" class="form-control xs-field">
                                                    Other Specify <input type="text" name="field_adj9g" value="<?php echo isset($injury9->otherspecify) ? $injury9->otherspecify : '' ?>" class="form-control bg-field"> </p>
                                            </div>
                                            <div class="form-group">
                                                <p><span style="display:inline;"><b>10.</b></span>&nbsp;Dated at <input type="text" name="field_adj10city"  class="form-control"> Date <input type="text" name="field_adj10a" id="field_adj10a" class="form-control" value="<?php echo date('m/d/Y'); ?>"> <br />Applicant Attorney <input type="text" name="field_firmatty1" class="form-control bg-field"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-22" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-content injuryone">
                                            <div class="ibox-title">
                                                <h5>User Define Fields</h5>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group">
                                                <label>Injury Status</label>
                                                <select name="field_fld1" class="form-control select2Class" >
                                                    <option value="">Select Status</option>
                                                    <option value="Accepted as of">Accepted as of</option>
                                                    <option value="Delayed as of">Delayed as of</option>
                                                    <option value="Denied as of">Denied as of</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>5402 Date (POS+93)</label>
                                                <input type="text" name="field_fld2" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>4850 Start Date</label>
                                                <input type="text" name="field_fld3" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>4658(d) Date (P&S+60)</label>
                                                <input type="text" name="field_fld4" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Notice of Offer Date</label>
                                                <input type="text" name="field_fld5" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-23" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-content injuryone">
                                            <div class="ibox-title">
                                                <h5>User Define Fields</h5>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group">
                                                <label>Current PTP</label>
                                                <input type="text" name="field_fld11" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Prior PTP</label>
                                                <input type="text" name="field_fld12" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Prior Awards</label>
                                                <input type="text" name="field_fld13" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Prior Attorney</label>
                                                <input type="text" name="field_fld14" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Add'I Notes</label>
                                                <input type="text" name="field_fld20" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="submit" id="addInjury" class="btn btn-md btn-primary">Save</button>
                    <button data-dismiss="modal" id="closeInjury" class="btn btn-md btn-primary">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="casepartyDetaillist" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select A Party To Attach</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive rolodexsearch">
                                                <table class="table table-bordered table-hover table-striped dataTables-example" id="casePartylist" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Firm</th>
                                                            <th>City</th>
                                                            <th>Address</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <button type="button" class="btn btn-md btn-primary" id="casepartysave" >Process</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="injury_Print" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print the Application</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <form action="" role="form" name="print_injures_form" id="print_injures_form">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="injuryId" id="injuryId">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="i-checks"><label> <input type="radio" value="form-data" name="form_data_type" checked> <i></i>Form and Data</label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="i-checks"><label> <input type="radio" value="data-only" name="form_data_type"> <i></i> Data only </label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="i-checks"><label> <input type="radio" value="form-only" name="form_data_type"> <i></i> Form only </label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-primary print_injury_data">Proceed</button>
                        <button data-toggle="modal" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<div id="clonecase" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Clone A Case</h4>
            </div>
            <div class="modal-body">
                <form name="cloneCaseForm" id="cloneCaseForm">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="number" id="caseFrom" name="caseFrom" min="0" placeholder="Copy Information from Case" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input class="form-control" type="text" id="caseFromCaption" readonly placeholder="From Case Caption" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" name="caseTo" readonly placeholder="Copy Information into Case" value="<?php echo $this->uri->segment(3); ?>" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input class="form-control" type="text" readonly placeholder="To Case Caption" value="<?php echo $display_details->caption1; ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="display-block"><label> <input type="checkbox" name="CopyData[]" id="cParties" value="cParties" class="i-checks"> Copy Parties </label></span>
                        <span class="display-block"><label> <input type="checkbox" name="CopyData[]" id="cInjuries" value="cInjuries" class="i-checks"> Copy Injuries </label></span>
                        <span class="display-block"><label> <input type="checkbox" name="CopyData[]" id="cCaseActivity" value="cCaseActivity" class="i-checks"> Copy Case Activity </label></span>

                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="cloneCaseBtn" href="#" >Proceed</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="deletecase" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form>
                    <p class="text-center">Are you sure you want to delete this case? All information related to this case will be removed.</p>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#">Proceed</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="addcalendar" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Appointment/Event To Calendar </h4>
            </div>
            <div class="modal-body">
                <form role="form" id="calendarForm" name="calendarForm" method="post" >
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general1">General</a></li>
                            <li class=""><a data-toggle="tab" href="#notes">Notes</a></li>
                            <li class=""><a data-toggle="tab" href="#ticklers">Ticklers/Alerts</a></li>
                            <li class=""><a data-toggle="tab" href="#properties">Properties</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>General Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <input type="hidden" name="eventno" value="0"/>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Attorney/Handling</label>
                                                        <select class="form-control cstmdrp" id="atty_hand" name="atty_hand" value="<?php
                                                        if (!empty($calDefAtty)) {
                                                            echo $calDefAtty;
                                                        }
                                                        ?>" >
                                                            <option value="">Select Attorney Handling</option>
                                                            <?php
                                                            if (isset($atty_hand)) {
                                                                foreach ($atty_hand as $key => $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val ?>" <?php
                                                                    if (isset($display_details->atty_hand) && $val == $display_details->atty_hand) {
                                                                        echo "selected='selected'";
                                                                    }
                                                                    ?> ><?php echo $val; ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Assigned</label>
                                                        <select class="form-control cstmdrp" id="attyass" name="attyass" value="<?php
                                                        if (!empty($calDefAttass)) {
                                                            echo $calDefAttass;
                                                        }
                                                        ?>" >
                                                            <option value="">Select Attorney Assigned</option>
                                                            <?php
                                                            if (isset($atty_resp)) {
                                                                foreach ($atty_resp as $key => $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val ?>" <?php
                                                                    if (isset($display_details->atty_resp) && $val == $display_details->atty_resp) {
                                                                        echo "selected='selected'";
                                                                    }
                                                                    ?>><?php echo $val; ?></option>

                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Judge</label>

                                                        <select  class="form-control cstmdrp" name="judge" id="judge" >
                                                            <?php
                                                            if (isset($JudgeName)) {
                                                                foreach ($JudgeName as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Location</label>
                                                        <input type="text" class="form-control" name="location"/>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>First Name </label>
                                                        <input type="text" class="form-control" value="<?php echo isset($display_details->first) ? $display_details->first : ''; ?>" name="first" id="first"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Name </label>
                                                        <input type="text" class="form-control" value="<?php echo isset($display_details->last) ? $display_details->last : ''; ?>" name="last" id="last"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Defendant</label>
                                                        <?php
                                                        if (isset($client_defendant) && !empty($client_defendant)) {
                                                            $defendant = $client_defendant[0]->first . ' ' . $client_defendant[0]->last;
                                                        } else {
                                                            $defendant = '';
                                                        }
                                                        ?>
                                                        <input type="text" class="form-control" name="defendant" value="<?php echo $defendant; ?>"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Venue</label>

                                                        <select class="form-control cstmdrp cstmdrp-placeholder" name="venue" id="venue" placeholder="Select Venue">
                                                            <option value="">Select Venue</option>
                                                            <?php
                                                            if (isset($Venue)) {
                                                                foreach ($Venue as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input type="text" class="form-control injurydate date-field birthcalalert alerttonholiday" pattern="\d{1,2}/\d{1,2}/\d{4}" name="caldate" id="caldate" data-mask="99/99/9999" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Time</label>
                                                        <input class="form-control" name="caltime" id="caltime"  pattern="\d{1,2}:\d{2}( ?[apAP][mM])?" value=""/>

                                                    </div>

                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-md btn-primary clear_form">Clear</button>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Event</label>
                                                        <select  class="form-control cstmdrp" name="event" id="event" style="">
                                                            <?php
                                                            $calevent = isset($main1->event) ? $main1->event : '';
                                                            $calevent = explode(',', $calevent);
                                                            foreach ($calevent as $val) {
                                                                ?>
                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Total Time in Minutes</label>
                                                        <input type="number" class="form-control" name="totime" min="0" value="0"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="notes" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5><a data-toggle="tab" href="#" class="calnote">Notes</a></h5>

                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group">
                                                <textarea class="form-control" name="notes" style="height: 300px;width: 100%;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ticklers" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Tickler / Alerts</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Type of Appointment</label>
                                                        <select class="form-control select2Class" name="tickle">
                                                            <option value="">Normal</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input type="text" name="tickledate" class="form-control injurydate date-field" id="tickledate" readonly />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Time</label>
                                                        <input type="text" name="tickletime" class="form-control injurytime date-field" id="tickletime" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-md btn-primary">Clear</button>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>From</label>
                                                        <select class="form-control select2Class" name="whofrom" id="whofrom">
                                                            <?php
                                                            if (isset($staffList)) {
                                                                foreach ($staffList as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['username']) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tickler Assigned to</label>
                                                        <select class="form-control select2Class" name="whoto" id="whoto">
                                                            <?php
                                                            if (isset($staffList)) {
                                                                foreach ($staffList as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="properties" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>General Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Category</label>
                                                        <select class="form-control select2Class" name="todo" id="todo">

                                                            <?php
                                                            if (isset($system_caldata[0]->calendars))
                                                                $calendars = trim($system_caldata[0]->calendars);
                                                            else
                                                                $calendars = '';
                                                            if ($calendars == null) {
                                                                ?>  <option value="">Calendars</option>
                                                                <?php
                                                            } else {
                                                                $calcate = explode(',', $system_caldata[0]->calendars);
                                                                ?>
                                                                <?php foreach ($calcate as $key => $val) { ?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Color</label>
                                                        <select class="form-control select2Class" name="color"/>
                                                        <option value="0">Select Color</option>
                                                        <?php
                                                        if (isset($filterArray)) {
                                                            foreach ($filterArray['ccolor'] as $key => $val) {
                                                                ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Case No</label>
                                                        <input type="text" class="form-control  caseno" name="caseno" />
                                                    </div>
                                                    <div class="form-group">
                                                        <span><label>Protected <input type="checkbox" class="i-checks" name="check_protected" value="1"></label></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <span><label>Rollover <input type="checkbox" class="i-checks" name="rollover" value="1"></label></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Last Modification Details</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Origin Initials</label>
                                                        <input type="text" name="initials0" class="form-control" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Initials</label>
                                                        <input type="text" name="initials" class="form-control" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <span><label>Override <input type="checkbox" class="i-checks"></label></span>
                                                    </div>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Last Initials</label>
                                                        <input type="text" class="form-control" readonly name="chgwho"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Date</label>
                                                        <input type="text" class="form-control injurydate date-field" data-mask="99/99/9999" readonly id="lastDate" style="background-color: #eee !important" disabled="disabled" name="chgdate"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Time</label>
                                                        <input type="text" class="form-control injurytime" readonly name="chgtime"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-group text-center ">
                    <button type="submit" class="btn btn-md btn-primary pull-left calendar_clone">Clone</button>
                    <button type="submit" id="btnSaveCalendar" class="btn btn-md btn-primary">Save</button>
                    <button type="button" id="btnCloseCalendar" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function createtaskTable()
    {
        taskTable = $(".task-dataTable").DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            bFilter: true,
            order: [
                [0, "DESC"]
            ],
            autoWidth: false,
            ajax: {
                url: "<?php echo base_url(); ?>tasks/getTasksData",
                type: "POST",
                data: function (d) {
                    var c = $("ul#task-navigation").find("li.active").attr("id");
                    if ($("#task_reminder").is(":checked") == true) {
                        var b = 1;
                    } else {
                        var b = "";
                    }
                    var e = {
                        caseno: $("#caseNo").val()
                    };
                    e.taskBy = c;
                    e.reminder = b;
                    $.extend(d, e);
                }
            },
            fnRowCallback: function (f, e, d, c) {
                $(f).attr("id", e[10]);
                if (e[e.length - 2] != "") {
                    var b = e[e.length - 2].split("-");
                    if (b[0] != "") {
                        $(f).css({
                            "background-color": b[0],
                            color: b[1]
                        });
                    } else {
                        $(f).css({
                            "background-color": "#ffffff",
                            color: "#000000"
                        });
                    }
                }
            },
            columnDefs: [{
                    render: function (e, c, f) {
                        if (e != null) {
                            var b = "";
                            var d = "";
                            if (f[8] == "") {
                                if (f[1] === "<?php echo isset($username) ? $username : $this->session->userdata('user_data')['username']; ?>") {
                                    d = '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + e + ')"><i class="fa fa-trash"></i></button>';
                                }
                                b = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + e + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;' + d;
                                b += '&nbsp;&nbsp;<button class="btn btn-primary btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + e + ')"><i class="fa fa-check"></i></button>';
                                if (f[11] != 0) {
                                    b += '&nbsp;&nbsp;<a class="btn btn-primary btn-circle-action btn-circle" title="PullCase" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + f[11] + '"><i class="fa fa-folder-open"></i></a>';
                                }
                            }
                        }
                        return b;
                    },
                    targets: 9,
                    bSortable: false,
                    bSearchable: false
                }, {
                    targets: 6,
                    className: "textwrap-line"
                }]
        });
    }

    $(window).load(function () {
        $('.loader').fadeOut();
    });
    var searchBy = {
        'caseno': $('#caseNo').val()
    };
    var searchBycat = {
        'caseno': $('#caseNo').val()
    };
    var caseactTable = '';
    var myDropzone = "";
    var caseDetailCalendarTable = '';
    var taskTable = '';
    $('.edit-party').hide();
    var rcs = '';
    var table = '';
    var injurytable = "";
    var formdatalist = "";
    var Prospect_data = JSON.parse('<?php echo json_encode($Prospect_data); ?>');
    var start = 0;
    var length = <?php echo DEFAULT_LISTING_LENGTH; ?>;
    var total = 0
    var current_list = 'inbox';
    var birthdaynotification = '<?php echo isset($birthdaynotification) ? $birthdaynotification : 0; ?>';
    var caseno = $("#caseNo").val();
    $(document).ready(function () {
        $('.datepickerClass').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            /*endDate: new Date()*/
        });
        var data_key = $('#activity_entry_type option:selected').data('key');
        $(".billingtab").addClass("hidden");
        $("#" + data_key).removeClass("hidden");
        Dropzone.options.addNewCaseActivity = {
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 11,
            maxFiles: 11,
            addRemoveLinks: true,
            uploadprogress: true,
            enqueueForUpload: false,
            paramName: 'document',
            /*acceptedFiles: "image/jpeg,application/docx,application/pdf,text/plain,application/msword",*/
            init: function () {
                myDropzone = this;
                this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                    $.blockUI();
                    $("#caseActNo").val($("#hdnCaseId").val());
                    e.preventDefault();
                    e.stopPropagation();
                    var form = $(this).closest('#add_new_case_activity');
                    if (form.valid() == true) {
                        if (myDropzone.getQueuedFiles().length > 0) {
                            myDropzone.processQueue();
                        } else {
                            myDropzone.uploadFiles([]);
                        }
                    } else {
                        $.unblockUI();
                    }
                });
                this.on("maxfilesreached", function () {
                    swal("Failure !", "Max file allowed exceeded", "error");
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);
                    }
                });
                this.on("addedfile", function (file) {
                    if (this.files.length) {
                        /*if($('#activity_event').val() == '') {
                            $('#activity_event').val(file.name.replace(".msg", ""));
                        }*/
                        var _i, _len;
                        for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
                            if (this.files[_i].name === file.name) {
                                this.removeFile(file);
                            }
                        }
                    }
                }),
                        this.on("success", function (file, responseText) {
                        });
                this.on("sendingmultiple", function () {
                });
                this.on("successmultiple", function (file, responseText) {
                    $.unblockUI();
                    var html = '';
                    var view = '';
                    $("#addcaseact").modal("hide");
                    var data = JSON.parse(responseText);
                    var link = '<?php echo base_url() ?>' + 'assets/clients/' + $('#caseNo').val() + '/' + data.file_name;
                    if (file.length == 0) {
                        if ((file.length + data.fileExist) > 1) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0)"><i class="icon fa fa-eye"></i> View All</a>';
                        } else if ((file.length + data.fileExist) > 0) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_attach" download=' + data.file_name + ' data-actid=' + data.actno + ' href="' + link + '"><i class="icon fa fa-eye"></i> View</a>';
                        } else {
                            view = '';
                        }
                    } else {
                        if (data.fileExist > 1) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0)"><i class="icon fa fa-eye"></i> View All</a>';
                        } else if (data.fileExist > 0) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_attach" download=' + data.file_name + ' data-actid=' + data.actno + ' href="' + link + '"><i class="icon fa fa-eye"></i> View</a>';
                        } else {
                            view = '';
                        }
                    }
                    html += "<tr id=caseAct" + data.actno + "><td>" + data.actData.add_case_cal + "</td>";
                    html += "<td>" + data.actData.add_case_time + "</td>";
                    html += "<td>" + data.actData.activity_event + "</td>";
                    html += "<td>" + data.actData.last_ini + "</td>";
                    html += "<td>" + data.actData.orignal_ini + "</td>";
                    html += "<td>" + view + "</td>";
                    html += "<td>";
                    html += "<a class='btn btn-xs btn-info marginClass activity_edit' data-actid=" + data.actno + " href='javascript:' title='Edit'><i class='icon fa fa-edit'></i> Edit</a>";
                    html += "<a class='btn btn-xs btn-danger marginClass activity_delete' data-actid=" + data.actno + " href='javascript:' title='Delete'><i class='icon fa fa-times'></i> Delete</a>";
                    html += "</td>";
                    html += "</tr>";
                    swal({
                        title: "Success!",
                        text: "Activity List updated successfuly",
                        type: "success"
                    },
                            function ()
                            {
                                location.reload();
                            }
                    );
                    if (data.status == 2) {
                        if (caseactTable.rows('[id=caseAct' + data.actno + ']').any()) {
                            caseactTable.rows($('tr#caseAct' + data.actno)).remove();
                        }
                        caseactTable.row.add($(html)).draw();
                        swal({
                            title: "Success!",
                            text: "Activity updated successfuly",
                            type: "success"
                        },
                                function ()
                                {
                                    location.reload();
                                }
                        );
                    } else if (data.status == 1) {
                        caseactTable.row.add($(html)).draw();
                    } else {
                        swal("Failure !", "Fail to add activity", "error");
                    }
                    document.getElementById("add_new_case_activity").reset();
                });
                this.on("errormultiple", function (files, response) {
                    console.log(response);
                    console.log(files);
                });
                this.on("completemultiple", function (file) {
                    this.removeAllFiles(true);
                });
            }
        };

        $('.contact').mask("(000) 000-0000");
        $('.social_sec').mask("000-00-0000");
        rolodexSearchDatatable = $('#rolodexPartiesResult').DataTable({
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            "lengthMenu": [5, 10, 25, 50, 100]
        });
        rolodexSearch = $('#rolodexsearchResult_data').DataTable({
            "scrollX": true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            "lengthMenu": [5, 10, 25, 50, 100]
        });
        releteddataTable = $('#related_case_table').DataTable({
            "serverSide": true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            "order": [
                [0, "desc"]
            ],
            "ajax": {
                url: "<?php echo base_url(); ?>cases/getReletedDataAjax",
                data: {
                    'caseNo': $("#caseNo").val()
                },
                type: "POST",
                async: true
            },
            "fnDrawCallback": function (oSettings) {
                $("#count_related").html('(' + oSettings._iRecordsTotal + ')');
            },
            "columnDefs": [{
                    "render": function (data, type, row) {
                        return '<button class="btn btn-danger btn-circle-action btn-circle" type="button" onClick="deleteRelatedcase(' + data + ')"><i class="fa fa-trash"></i></button>';
                    },
                    "targets": 5,
                    "bSortable": false,
                    "bSearchable": false
                },
                {
                    "render": function (data, type, row) {
                        return '<a href="javascript:void(0)" id="' + row[0] + '" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-popover-content="#data">' + data + '</a>';
                    },
                    "targets": 0
                }
            ],
        });
        $("#companylistForm").validate({
            rules: {
                rolodex_search_text_name: {
                    required: true,
                    noBlankSpace: true
                }
            },
            messages: {
                rolodex_search_text_name: {
                    required: "Please enter Firm/Company name "
                }
            },
            submitHandler: function (form, event) {
                event.preventDefault();
            }
        });
        $('#sfl').tooltip();
        $("#addInjuryForm").validate({
            rules: {
                field_first: {
                    required: true
                },
                field_last: {
                    required: true
                },
                field_doi2: {required: function () {
                        if ($('#field_doi').val() == "")
                            return true;
                        else
                            return false;
                    }
                }
            },
            messages: {
                field_first: {
                    required: "First name required"
                },
                field_last: {
                    remote: "Last name required"
                },
                field_doi2: {
                    required: "CT Dates is required."
                }
            }
        });
        /*TASK RELATED CODE START*/
        createtaskTable();
        $(".rdate1").datetimepicker({
            format: "mm/dd/yyyy",
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('.datepicker').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        });
        $('#datepicker').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        });
        $('.dob').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: '+0d'
        });
        $('#taskdatepicker').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('#taskdatepicker2').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('#caldate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            /*endDate: "+"+calYearRange+"y",*/
            startDate: new Date()
        });
        $('.rdate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('#casedate_followup').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date(),
        });
        $('#casedate_closed').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date(),
        });
        $('.injurydate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('#field_adj1a').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date(),
        });
        var d = new Date();
        var hours = d.getHours();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        var currenttime = '<?php echo date('h:i a') ?>';
        var selectedArray = [];
        $('#caltime').val(currenttime);
        var input1 = $('#caltime');
        input1.mdtimepicker({
            autoclose: true
        });
        $('.rtime').mdtimepicker({
            autoclose: true
        });
        $('#oletime').mdtimepicker({
            autoclose: true,
            'default': currenttime,
        });
        $('#tickledate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('#tickletime').mdtimepicker({
            autoclose: true
        });
        jQuery.validator.addMethod("greaterThan", function (c, b, d) {
            if ($(d).val() != "") {
                if (!/Invalid|NaN/.test(new Date(c))) {
                    return new Date(c) >= new Date($(d).val());
                }
                return isNaN(c) && isNaN($(d).val()) || (Number(c) >= Number($(d).val()));
            } else {
                return true;
            }
        }, "Must be greater than Reminder Date.");
        var a = $("#addTaskForm").validate({
            ignore: [],
            rules: {
                whofrom: {
                    required: true
                },
                whoto3: {
                    required: true
                },
                datereq: {
                    required: true,
                    greaterThan: ".rdate1"
                },
                event: {
                    required: true,
                    noBlankSpace: true
                }
            }
        });
        var b = $("#addTaskForm1").validate({
            ignore: [],
            rules: {
                whofrom: {
                    required: true
                },
                whoto3: {
                    required: true
                },
                datereq: {
                    required: true,
                    greaterThan: ".rdate1"
                },
                event: {
                    required: true,
                    noBlankSpace: true
                }
            }
        });
        $("#addtask.modal").on("hidden.bs.modal", function () {
            $("#addtask h4.modal-title").text("Add a Task");
            $("#addTaskForm")[0].reset();
            $("input[name=taskid]").val(0);
            $('#addtask a[href="#name"]').tab("show");
        });
        $(".task-dataTable tbody").on("click", "tr", function () {
            if ($(this).hasClass("table-selected")) {
                $(this).removeClass("table-selected");
                selectedArray.splice($.inArray(this.id, selectedArray), 1);
            } else {
                selectedArray.push(this.id);
            }
        });

        /* TASK RELATED CODE ENDS*/
        $.ajax({
            url: '<?php echo base_url('cases/getRedAlertActivity'); ?>',
            dataType: 'json',
            type: 'POST',
            async: true,
            data: {
                caseno: caseno
            },
            beforeSend: function () {},
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {
                $("#redalertAct").css("display", "block");
                $("#contentData").html(data);
                console.log(data);
            }
        });
        table = $('#party-table-data').DataTable({
            rowReorder: true,
            stateSave: true,
            serverSide: true,
            searching : true,
            stateSaveParams: function (settings, data) {
                data.search.search = "";
                data.start = 0;
                delete data.order;
            },
            aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [0,3]
                },
                {
                    bSearchable: false,
                    aTargets: [0,3]
                }
            ]
        });
        table.on('row-reorder', function (e, diff, edit) {
            var ids1 = new Array();
            var ids3 = new Array();
            for (var i = 0, ien = diff.length; i < ien; i++) {
                var rowData = table.row(diff[i].node).data();
                var ids = new Array();
                var ids2 = new Array();
                if (diff[i].newData == 1) {
                    var newid = diff[i].newData;
                    var casecard_no = $(rowData[3]).attr('data-casecard_no');
                    var card_no = $(rowData[3]).attr('data-card');
                    setTimeout(function () {
                        swal({
                            title: "Warning!",
                            text: "You are changing the name at the top of the list.\n The client should always be at the top of the list.\n Are you sure you want continue anyways?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                                ids2.push(newid);
                                ids2.push(casecard_no);
                                ids2.push(card_no);
                                ids3.push(ids2);
                                $.ajax({
                                    url: '<?php echo base_url(); ?>cases/updateorderno',
                                    type: "POST",
                                    data: {
                                        position: ids3,
                                        caseno: $('#hdnCaseId').val()
                                    },
                                    success: function (data) {
                                        if (data == 1) {
                                            swal("Success !", "Parties reorder successfully", "success");
                                            $('.case-details a[href="#parties"]').trigger('click');
                                            
                                        }
                                    }
                                });
                                swal.close();
                            }
                        });
                    }, 800);
                } else {
                    ids.push(diff[i].newData);
                    ids.push($(rowData[3]).attr('data-casecard_no'));
                    ids.push($(rowData[3]).attr('data-card'));
                    ids1.push(ids);
                }
            }
            if (ids1.length > 0) {
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/updateorderno',
                    type: "POST",
                    data: {
                        position: ids1,
                        caseno: $('#hdnCaseId').val()
                    },
                    success: function (data) {
                        if (data > 0) {
                        }
                        $('.case-details a[href="#parties"]').trigger('click');
                    }
                });
            }
        });

        $('.case-details a[data-toggle="tab"]').on('click', function (e) {
            if ($(this).attr('href') == '#injurysummary') {
                var caseno = $("#caseNo").val();

                $.ajax({
                    url: '<?php echo base_url('cases/case_injurysummary'); ?>',
                    dataType: 'html',
                    type: 'POST',
                    data: {
                        caseno: caseno
                    },
                    success: function (data) {

                        $('#injurysummary').html(data);
                    }
                });
            }
            if ($(this).attr('href') == '#caseact') {
                caseactTable = $('#case_activity_list').DataTable({
                    retrieve: true,
                    stateSave: true,
					stateSaveParams: function (settings, data) {
						data.search.search = "";
						data.start = 0;
						delete data.order;
						data.order = [0, "desc"];
					},
                    "serverSide": true,
                    "autoWidth": true,
                    "order": [
                        [0, "desc"]
                    ],
                    "columnDefs": [{
                            "width": "10%",
                            "targets": 0
                        },
                        {
                            "width": "5%",
                            "targets": 1
                        },
                        {
                            "width": "50%",
                            "targets": 2
                        },
                        {
                            "width": "4%",
                            "targets": 3
                        },
                        {
                            "width": "4%",
                            "targets": 4
                        },
                        {
                            "width": "27%",
                            "targets": 5
                        }
                    ],
                    "aoColumnDefs": [{
                            "bSortable": false,
                            "aTargets": [5]
                        },
                        {
                            "bSearchable": false,
                            "aTargets": [5]
                        },
                    ],
                    "ajax": {
                        url: "<?php echo base_url(); ?>cases/getAllActivity",
                        type: "POST",
                        data: {
                            'caseId': $("#caseNo").val()
                        }
                    },
                    "fnCreatedRow": function (nRow, aData, iDataIndex) {

                        $(nRow).attr('id', 'caseAct' + aData[6]);
                        /*  $(nRow).addClass("edit_caseAct ");*/
                        if (aData[7] != 0) {
                            $(nRow).css({
                                'background-color': aData[7],
                                'color': aData[8]
                            });
                        }
                    },
                });
            }
            if ($(this).attr('href') == '#injury') {
                injurytable = $('#injuryTable').DataTable({
                    retrieve: true,
                    stateSave: true,
					stateSaveParams: function (settings, data) {
						data.search.search = "";
						data.start = 0;
						delete data.order;
						data.order = [0, "desc"];
					},
                    "serverSide": true,
                    "bFilter": true,
                    "rowReorder": true,
                    "order": [
                        [0, "desc"]
                    ],
                    "ajax": {
                        url: "<?php echo base_url(); ?>cases/getAllInjuries",
                        type: "POST",
                        data: {
                            'caseId': $("#caseNo").val()
                        }
                    },
                    "aoColumnDefs": [{
                            "bSortable": false,
                            "aTargets": [4]
                        },
                        {
                            "bSearchable": false,
                            "aTargets": [4]
                        },
                    ],
                    "fnCreatedRow": function (nRow, aData, iDataIndex) {
                        $(nRow).attr('id', 'injury-' + aData[5]);
                    },
                    initComplete: function () {
                        this.api().columns().every(function () {
                            var column = this;
                            var input = document.createElement("input");
                            $(input).appendTo($(column.footer()).empty())
                                    .on('change', function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                        });
                        var caseNo = $("#caseNo").val();
                        var injuryId = $('#injuryTable tbody tr:eq(0)').attr('id');
                        if (injuryId != 'noRecord' && typeof injuryId != typeof undefined) {
                            injuryId = $('#injuryTable tbody tr:eq(0)').attr('id').split('-');
                            injuryId = injuryId[1];
                            $('div#injury_div').block();
                            $('table#injuryTable').find('.table-selected').not('tr#injury-' + injuryId).removeClass('table-selected');
                            $('#injury-' + injuryId).addClass('table-selected');
                            $.ajax({
                                url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                                method: 'POST',
                                dataType: 'json',
                                data: {
                                    injury_id: injuryId,
                                    caseno: caseNo,
                                },
                                success: function (data) {
                                    $('div#injury_div').unblock();
                                    if (data != false) {
                                        setInjuryViewData(data);

                                    }
                                }
                            });
                            injurytable.on('row-reorder', function (e, diff, edit) {
                                var ids1 = new Array();
                                var ids3 = new Array();
                                for (var i = 0, ien = diff.length; i < ien; i++) {
                                    var rowData = injurytable.row(diff[i].node).data();
                                    var ids = new Array();
                                    var ids2 = new Array();
                                    if (diff[i].newData == 1) {
                                        var newid = diff[i].newData;
                                        var injuryid = $(rowData[4]).attr('id');
                                        ids2.push(newid);
                                        ids2.push(injuryid);
                                        ids3.push(ids2);
                                        $.ajax({
                                            url: '<?php echo base_url(); ?>cases/updateinjuryorderno',
                                            type: "POST",
                                            data: {
                                                position: ids3,
                                                caseno: $('#hdnCaseId').val()
                                            },
                                            success: function (data) {
                                                if (data == 1) {
                                                    swal("Success !", "Injury reorder successfully", "success");
                                                    injurytable.draw();
                                                }
                                            }
                                        });
                                    } else {
                                        var injuryid = $(rowData[4]).attr('id');
                                        ids.push(diff[i].newData);
                                        ids.push(injuryid);
                                        ids1.push(ids);
                                    }
                                }
                                if (ids1.length > 0) {
                                    $.ajax({
                                        url: '<?php echo base_url(); ?>cases/updateinjuryorderno',
                                        type: "POST",
                                        data: {
                                            position: ids1,
                                            caseno: $('#hdnCaseId').val()
                                        },
                                        success: function (data) {
                                            if (data > 0) {
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
            if ($(this).attr('href') == '#calendar') {
                caseDetailCalendarTable = $('#caseDetailCalendarTable').DataTable({
                    "retrieve": true,
                    "serverSide": true,
                    stateSave: true,
					stateSaveParams: function (settings, data) {
						data.search.search = "";
						data.start = 0;
						delete data.order;
						data.order = [0, "desc"];
					},
                    "bFilter": true,
                    "bSortable": true,
                    "rowReorder": true,
                    "order": [
                        [0, "desc"]
                    ],
                    "ajax": {
                        url: "<?php echo base_url(); ?>cases/getAllCaseEvents",
                        type: "POST",
                        data: {
                            'caseId': $("#caseNo").val()
                        }
                    },
                    "aoColumnDefs": [{
                            "bSortable": false,
                            "aTargets": [8]
                        },
                        {
                            "bSearchable": false,
                            "aTargets": [8]
                        },
                    ],
                    "fnCreatedRow": function (nRow, aData, iDataIndex) {
                        $(nRow).attr('id', 'calEvent_' + aData[9]);
                        if (aData[10] != 0) {
                            $(nRow).css({
                                'background-color': aData[10],
                                'color': aData[11]
                            });
                        }
                    },
                });
            }
            if ($(this).attr('href') == '#caseEmail') {
                var caseno = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url('email/case_email_listing'); ?>',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        caseno: caseno,
                    },
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        var listing = "";
                        $.each(data.result, function (index, item) {
                            listing += generate_tr_html(item);
                        });
                        $('.mail-box tbody').html('');
                        $('.mail-box tbody').html(listing);
                        $('#related_email').DataTable({
                            "order": [
                                [2, "desc"]
                            ],
                            stateSave: true,
							stateSaveParams: function (settings, data) {
								data.search.search = "";
								data.start = 0;
								delete data.order;
								data.order = [2, "desc"];
							},
                            "destroy": true,
                        });
                    }
                });
            }
            if ($(this).attr('href') == '#parties') {
                $('#party-table-data tbody tr:eq(0)').addClass("table-selected");
                var row = table.row('.table-selected').node();
                var td = $(row).find('td')[3];
                setPartyViewData($(td).find('a').attr('data-card'), $('#hdnCaseId').val());
            }
        });
        $('#addcontact.modal').on('show.bs.modal', function () {
            document.getElementById('imgerror').innerHTML = '';
            document.getElementById('hiddenimg').value = '';
            $(".chosen-language").chosen();
        });
        $('#addcontact.modal').on('hidden.bs.modal', function () {
            $('#addcontact.modal-title').text('Add New Contact');
            $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                this.value = '';
            });
            document.getElementById('profilepicDisp').style.display = 'none';
            document.getElementById('dispProfile').src = '';
            $('#addcontact a[href="#name"]').tab('show');
        });

        $('.edit-card').click(function () {
            editContact($(this).data('cardcode'));
        });

        /*$('#addRolodexForm a[data-toggle="tab"]').on('click', function (e) {
         setValidation();
         e.preventDefault();
         });*/

        function editContact(cardCode) {
            $.blockUI();
            $.ajax({
                url: "<?php echo base_url() . 'rolodex/getEditRolodex'; ?>",
                method: "POST",
                data: {'cardcode': cardCode},
                success: function (result) {

                    var data = $.parseJSON(result);
                    var notes_comments = '';
                    if (data.card_comments) {
                        if(isJson(data.card_comments)) {
                            notes_comments = $.parseJSON(data.card_comments);
                        } else {
                            notes_comments = data.card_comments;
                        }
                    }
                    $('#addcontact .modal-title').html('Edit Contact');
                    $('#card_salutation').val(data.salutation);
                    $('#card_suffix').val(data.suffix);
                    $('select[name=card_title]').val(data.title);
                    $('#card_type').val(data.type);
                    $('input[name=card_first]').val(data.first);
                    $('input[name=card_middle]').val(data.middle);
                    $('input[name=card_last]').val(data.last);
                    $('input[name=card_popular]').val(data.popular);
                    $('input[name=card_letsal]').val(data.letsal);
                    $('input[name=card_occupation]').val(data.occupation);
                    $('input[name=card_employer]').val(data.employer);
                    $('input[name=hiddenimg]').val(data.picture);
                    if (data.picture)
                    {
                        document.getElementById('profilepicDisp').style.display = 'block';
                        document.getElementById('dispProfile').src = data.picturePath;
                    } else
                    {
                        document.getElementById('dispProfile').src = '';
                    }
                    $('input[name=card2_firm]').val(data.firm);
                    $('input[name=card2_address1]').val(data.address1);
                    $('input[name=card2_address2]').val(data.address2);
                    $('input[name=card2_city]').val(data.city);
                    $('input[name=card2_state]').val(data.state);
                    $('input[name=card2_zip]').val(data.zip);
                    $('input[name=card2_venue]').val(data.venue);
                    $('input[name=card2_eamsref]').val(data.eamsref);
                    $('input[name=card_business]').val(data.business);
                    $('input[name=card_fax]').val(data.card_fax);
                    $('input[name=card_home]').val(data.home);
                    $('input[name=card_car]').val(data.car);
                    $('input[name=card_email]').val(data.email);
                    if (notes_comments != '')
                        $('input[name=card_notes]').val(notes_comments[0].notes);
                    else
                        $('input[name=card_notes]').val();
                    $('input[name=card2_phone1]').val(data.phone1);
                    $('input[name=card2_phone2]').val(data.phone2);
                    $('input[name=card2_tax_id]').val(data.tax_id);
                    $('input[name=card2_fax]').val(data.card2_fax);
                    $('input[name=card2_fax2]').val(data.fax2);
                    $('input[name=card_social_sec]').val(data.social_sec);
                    if (data.birth_date == 'NA') {
                        $('input[name=card_birth_date]').val('');
                    } else {
                        $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                    }
                    $('input[name=card_licenseno]').val(data.licenseno);
                    $('select[name=card_specialty]').val(data.specialty);
                    $('input[name=card_mothermaid]').val(data.mothermaid);
                    $('input[name=card_cardcode]').val(data.cardcode);
                    $('input[name=card_firmcode]').val(data.firmcode);
                    $('select[name=card_interpret]').val(data.interpret);
                    $('select[name=card_language]').val(data.language).trigger("change");
                    $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                    $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                    $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                    $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                    if (notes_comments != '')
                        $('textarea[name=card_comments]').val(notes_comments[1].comments);
                    else
                        $('textarea[name=card_comments]').val();
                    $('textarea[name=card2_comments]').val(data.card2_comments);
                    $('textarea[name=card2_mailing1]').val(data.mailing1);
                    $('textarea[name=card2_mailing2]').val(data.mailing2);
                    $('textarea[name=card2_mailing3]').val(data.mailing3);
                    $('textarea[name=card2_mailing4]').val(data.mailing4);
                    $('#new_case').val('parties');
                    $('input[name="card_cardcode"]').val(data.cardcode);
                    $('#addcontact').modal({backdrop: 'static', keyboard: false});
                    $("#addcontact").modal('show');
                    $("#addRolodexForm select").trigger('change');
                    $.unblockUI();
                }
            });
        }

        if (document.getElementById('applicant_ini')) {
            var firstName = document.getElementById('applicant_ini').value;
            var intials = firstName.charAt(0);
            var profileImage = $('#applicantImage').text(intials);
        }
        if (document.getElementById('atty_resp_ini')) {
            var atty_resp = document.getElementById('atty_resp_ini').value;
            var atty_resp_intials = atty_resp.charAt(0);
            var atty_resp_Image = $('#imgRespurl').text(atty_resp_intials);
        }
        if (document.getElementById('para_hand_ini')) {
            var para_hand = document.getElementById('para_hand_ini').value;
            var para_hand_intials = para_hand.charAt(0);
            var para_hand_Image = $('#imgParaurl').text(para_hand_intials);
        }
        if (document.getElementById('atty_hand_ini')) {
            var atty_hand = document.getElementById('atty_hand_ini').value;
            var atty_hand_intials = atty_hand.charAt(0);
            var atty_hand_Image = $('#imgAttyHandurl').text(atty_hand_intials);
        }
        if (document.getElementById('sec_hand_ini')) {
            var sec_hand = document.getElementById('sec_hand_ini').value;
            var sec_hand_intials = sec_hand.charAt(0);
            var sec_hand_Image = $('#imgSecHandurl').text(sec_hand_intials);
        }

    });
    $('#addContact').on('click', function () {
        setValidation();
        var cardcodeVal = $('input[name="card_cardcode"]').val();
        /*if (cardcodeVal == '') {
         if (!$('#addRolodexForm').valid()) {
         return false;
         }
         }
         if (!$('#addRolodexForm').valid()) {
         return false;
         }*/
        var myImg = $('#profilepic').prop('files')[0];
        if(myImg){
            var sizeKB = myImg.size / 1024; ;
            if(sizeKB > 5000)
            {
                swal("Failure !", "The file size exceeds the limit allowed.", "error");
                return false;
            }
        }
        $.blockUI();
        var url = '<?php echo base_url() . "rolodex/addCardData" ?>';
        if ($('input[name="card_cardcode"]').val() != '') {
            url = '<?php echo base_url() . "rolodex/editCardData" ?>';
        }
        $.ajax({
            url: url,
            method: "POST",
            data: $('#addRolodexForm').serialize(),
            success: function (result) {
                var data = $.parseJSON(result);
                var editCardcode = data.editcardid;
                if ($('#profilepic').val() != '')
                {
                    var old_img = document.getElementById('hiddenimg').value;
                    var images = '';
                    var file_data = $('#profilepic').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: '<?php echo base_url(); ?>rolodex/updateprofilepic/' + editCardcode + '/' + old_img,
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {

                            if (response)
                            {
                                images = response;
                                var plink = '<?php echo base_url() ?>assets/rolodexprofileimages/' + images;
                                setTimeout(function ()
                                {
                                    if (images != null) {
                                        if ($('#applicantImage').length == 0) {
                                            $('.client-img').find('img.img-circle').attr("src", plink);
                                        } else
                                        {
                                            $('#applicantImage').remove();
                                            $('.client-img').append('<center><img class="img-circle" height="67px" src="' + plink + '" /></center>');
                                        }
                                    } else {

                                    }
                                }, 500);
                            }
                        },
                        error: function (response) {
                        }
                    });
                }
                $.unblockUI();


                if (data.status == '1') {
                    $('#addRolodexForm')[0].reset();
                    $('#addcontact').modal('hide');
                    $('#addcontact .modal-title').html('Add New Contact');


                    if ((data.caseid != 0 || data.caseid != 'undefined')) {
                        var caseid = $("#hdnCaseId").val();
                        if (data.caseid != 0 && $("#new_case").val() == 1) {
                            var url = "<?php echo base_url() ?>cases/case_details/" + data.caseid;
                            setTimeout(function ()
                            {
                                window.location.href = url;
                            }, 3000);
                        }
                        if (cardcodeVal != '' || $("#new_case").val() == 'parties') {

                            $.ajax({
                                url: '<?php echo base_url() . "cases/case_details" ?>',
                                method: "POST",
                                data: {'caseId': $("#hdnCaseId").val()},
                                success: function (result) {
                                    var res = $.parseJSON(result);
                                    setTimeout(function ()
                                    {
                                        $(".applicant_name").html("<b>" + res.display_details.salutation + " " + res.display_details.first + " " + res.display_details.last + "</b>");
                                        if (res.display_details.popular)
                                        {
                                            if ((res.display_details.popular).length < 10) {
                                                var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular + '</b></span><br/>';
                                            } else
                                            {
                                                var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular.substr(1, 10) + '..</b></span><br/>';
                                            }
                                            if ($('.popular').length > 0) {
                                                $(".popular").replaceWith(htm);
                                            } else
                                            {
                                                $(".applicant_name").after(htm);
                                            }

                                        }
                                        $(".applicant_address").html(res.display_details.address1);
                                        $(".applicant_address2").html(res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);

                                        $('#address_map').attr('href', "https://maps.google.com/?q=" + res.display_details.address1 + '' + res.display_details.address2 + " " + res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);
                                        $(".home").html(res.display_details.home);
                                        if (res.display_details.car == "")
                                            $(".business").html(res.display_details.business);
                                        else
                                            $(".business").html(res.display_details.car);

                                        $(".email").html(res.display_details.email);
                                        var dob = new Date(res.display_details.birth_date);
                                        var today = new Date();
                                        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                                        $(".birth_date").html('Born on ' + moment(res.display_details.birth_date).format('MM/DD/YYYY') + ' ,' + age + 'years old');
                                        $("#party_name").html(res.display_details.first + " " + res.display_details.last);
                                        $("#party_beeper").val(res.display_details.beeper);
                                        var p_tr_id = $('table#party-table-data').find('.table-selected');
                                        p_tr_id = p_tr_id.attr('id');
                                        $("#" + p_tr_id).find(".parties_name").html(res.display_details.first + ", " + res.display_details.last);
                                    }, 1000);
                                }
                            });
                        }

                    }
                } else {
                    swal("Oops...", data.message, "error");
                }

            }
        });

        return false;
    });
    $(document.body).on('click', '#printCasedetails', function () {
        $.ajax({
            url: '<?php echo base_url() . 'cases/printCaseDetails'; ?>',
            method: "POST",
            data: {
                'caseNo': $("#caseNo").val()
            },
            success: function (result) {
                $(result).printThis({
                    debug: false,
                    header: "<h2><center>General Case Information</center></h2>"
                });
            }
        });
    });
    $(document.body).on('click', '#printCase', function () {
        $.ajax({
            url: '<?php echo base_url() . 'cases/printCaseDetails'; ?>',
            method: "POST",
            data: {
                'caseNo': $("#caseNo").val()
            },
            success: function (result) {
                $(result).printThis({
                    debug: false,
                    header: "<h2><center>General Case Information</center></h2>"
                });
            }
        });
    });
    function setValidation() {
        var validator = $('#addRolodexForm').validate({
            ignore: [],
            rules: {
                card_first: {
                    required: function (element) {
                        if ($("input[name=card2_firm]").val() == '') {
                            return true;
                        }
                        return false;
                    }
                },
                card2_firm: {
                    required: function (element) {
                        if ($("input[name=card_first]").val() == '') {
                            return true;
                        }
                        return false;
                    }
                },
                card2_address1: {
                    required: function (element) {
                        if ($("input[name=card2_firm]").val() != '') {
                            return true;
                        }
                        return false;
                    }
                },
                card2_city: {
                    required: function (element) {
                        if ($("input[name=card2_firm]").val() != '') {
                            return true;
                        }
                        return false;
                    }
                },
                card2_state: {
                    required: function (element) {
                        if ($("input[name=card2_firm]").val() != '') {
                            return true;
                        }
                        return false;
                    },
                    stateUS: true
                },
                card2_zip: {
                    required: function (element) {
                        if ($("input[name=card2_firm]").val() != '') {
                            return true;
                        }
                        return false;
                    },
                    zipcodeUS: true
                },
                card_type: {
                    required: true
                            /*function (element) {
                             if ($("input[name=card_first]").val() != '') {
                             return true;
                             }
                             return false;
                             }*/
                },
                /*card_middle: {letterwithspace: true},
                card_last: {letterwithspace: true},*/
                card_business: {phoneUS: true},
                card_home: {phoneUS: true},
                card_fax: {phoneUS: true, maxlength: 15},
                card_email: {email: true},
                card2_phone1: {phoneUS: true},
                card2_fax: {phoneUS: true},
                card2_phone2: {phoneUS: true},
                card2_fax2: {phoneUS: true},
                card2_tax_id: {alphanumeric: true, maxlength: 15},
                card_social_sec: {ssId: true},
                card_speciality: {letterswithbasicpunc: true}
            },
            messages: {
                card_first: {required: "Please Fill First Name or Firm Name"},
                card2_firm: {required: "Please Fill First Name or Firm Name"},
                card2_fax: {phoneUS: "Please specify a valid fax number"},
                card2_fax2: {phoneUS: "Please specify a valid fax number"},
                card_fax: {phoneUS: "Please specify a valid fax number"}
            },
            invalidHandler: function (form, validator) {
                var element = validator.invalidElements().get(0);
                var tab = $(element).closest('.tab-pane').attr('id');

                $('#addcontact a[href="#' + tab + '"]').tab('show');
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "card_type") {
                    error.insertAfter(element.next());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        return validator;
    }
    function setCalendarValidation() {
        var validator = $('#calendarForm').validate({
            ignore: [],
            rules: {
                calstat: {letterwithspace: true},
                /*first: {
                 required: true,
                 letterswithbasicpunc: true
                 },
                 last: {
                 required: true,
                 letterswithbasicpunc: true
                 },*/
                defendant: {letterswithbasicpunc: true},
                judge: {letterswithbasicpunc: true},
                venue: {letterswithbasicpunc: true},
                caldate: {required: true},
                caltime: {required: true},
                event: {required: true},
                caseno: {number: true,
                    remote: {
                        url: '<?php echo base_url(); ?>prospects/CheckCaseno',
                        type: "post",
                        data: {
                            pro_caseno: function () {
                                return $('#calendarForm :input[name="caseno"]').val();
                            }
                        }
                    }
                }
            },
            messages: {
                caseno: {
                    remote: "This caseno is not exist"
                }
            },
            invalidHandler: function (form, validator) {
                var element = validator.invalidElements().get(0);
                var tab = $(element).closest('.tab-pane').attr('id');
                $('#addcalendar a[href="#' + tab + '"]').tab('show');
            }
        });
        return validator;
    }

    /* TASK RELATED FUNCTIONS START*/
    $('#addTask').on('click', function () {
        if ($('#addTaskForm').valid()) {
            $.blockUI();
            var url = '';
            if ($('input[name=taskid]').val() != '' && $('input[name=taskid]').val() != 0) {
                url = '<?php echo base_url(); ?>tasks/editTask';
            } else {
                url = '<?php echo base_url(); ?>tasks/addTask';
            }
            $.ajax({
                url: url,
                method: "POST",
                data: $('#addTaskForm').serialize() + '&caseno=' + $('#caseNo').val(),
                success: function (result) {
                    $('#addTaskForm')[0].reset();
                    var data = $.parseJSON(result);
                    $.unblockUI();
                    if (data.status == '1') {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success"
                        },
                                function () {

                                    $('#addtask').modal('hide');
                                    $('#addtask .modal-title').html('Add a Task');
                                    taskTable.draw(false);
                                }
                        );
                    } else {
                        swal("Oops...", data.message, "error");
                    }
                }
            });
        }
    });
    $("#addtask").on("hidden.bs.modal", function () {
        $(this).find("input,textarea").each(function () {
            this.value = "";
            $(this).iCheck("uncheck");
            $(this).trigger("change");
        });
    });
    $(document.body).on("click", "#addtaskModal", function () {
        $(".select2Class#whofrom").val("<?php echo isset($loginUser) ? $loginUser : ''; ?>").trigger("change");
        $(".select2Class#whoto").val("AEB").trigger("change");
        $(".select2Class#priority").val("2").trigger("change");
        $(".select2Class#color").val("1").trigger("change");
        $("#addtask").modal({
            backdrop: "static",
            keyboard: false
        });
        $("#addtask").modal("show");
    });
    $(document.body).on("click", "#addTask1", function () {
        if ($("#addTaskForm1").valid()) {
            $.blockUI();
            var a = "";
            if ($("input[name=taskid]").val() != "" && $("input[name=taskid]").val() != 0) {
                a = "<?php echo base_url(); ?>tasks/editTask";
            } else {
                a = "<?php echo base_url(); ?>tasks/addTask";
            }
            $.ajax({
                url: a,
                method: "POST",
                data: $("#addTaskForm1").serialize() + "&prosno=" + $(".task_list").attr("data-proskey_id"),
                success: function (b) {
                    var c = $.parseJSON(b);
                    $("#addTaskForm1")[0].reset();
                    $.unblockUI();
                    if (c.status == "1") {
                        swal({
                            title: "Success!",
                            text: c.message,
                            type: "success"
                        }, function () {
                            $("#addtask1").modal("hide");
                            $("#addtask1 .modal-title").html("Add a Task");
                            taskTable.draw(false);
                        });
                    } else {
                        swal("Oops...", c.message, "error");
                    }
                }
            });
        }
    });
    $("#addtask1.modal").on("hidden.bs.modal", function () {
        $("#addtask1 h4.modal-title").text("Add a Task");
        $("#addTaskForm1")[0].reset();
        $('#addTaskForm')[0].reset();
        var a = setTaskValidation();
        $("label.error").remove();
        $(this).find('input[type="text"],input[type="email"],textarea').each(function () {
            this.value = "";
            $(this).removeClass("error");
        });
        $("input[name=taskid]").val(0);
        $('#addtask1 a[href="#name"]').tab("show");
    });
    $(document).on("click", ".task_list", function () {
        var a = $(this).attr("data-proskey_id");
        searchBy1.prosno = a;
        $("#Task-list").modal("show");
    });
    function editTask(a) {
        $.blockUI();
        $.ajax({
            url: "<?php echo base_url(); ?>tasks/getTask",
            data: {
                taskId: a
            },
            method: "POST",
            success: function (b) {
                var c = $.parseJSON(b);
                $.unblockUI();
                if (c.status == 0) {
                    swal("Oops...", c.message, "error");
                } else {
                    $("#addtask1 .modal-title").html("Edit Task");
                    $("input[name=taskid]").val(a);
                    $("select[name=whofrom]").val(c.whofrom);
                    $("select[name=whoto3]").val(c.whoto);
                    $("input[name=datereq]").val(c.datereq);
                    $("textarea[name=event]").val(c.event);
                    $("select[name=color]").val(c.color);
                    $("select[name=priority]").val(c.priority);
                    $("select[name=typetask]").val(c.typetask);
                    $("select[name=phase]").val(c.phase);
                    if (c.notify == 1) {
                        $("input[name=notify]").iCheck("check");
                    } else {
                        $("input[name=notify]").iCheck("uncheck");
                    }
                    if (c.reminder == 1) {
                        $('input[type="checkbox"][name="reminder1"]').prop("checked", true).change();
                        $("input[name=rdate]").val(c.popupdate);
                        $("input[name=rtime]").val(c.popuptime);
                        $(".rmdt").show();
                    } else {
                        $('input[type="checkbox"][name="reminder1"]').prop("checked", false).change();
                        $(".rmdt").hide();
                    }
                    $("#addtask1").modal({
                        backdrop: "static",
                        keyboard: false
                    });
                    $("#addtask1").modal("show");
                    $("#addTaskForm1 select").trigger("change");
                }
            }
        });
    }
    function setTaskValidation() {
        var a = $("#addTaskForm").validate({
            ignore: [],
            rules: {
                whofrom: {
                    required: true
                },
                whoto2: {
                    required: true
                },
                datereq: {
                    required: true,
                    greaterThan: ".rdate"
                },
                event: {
                    required: true,
                    noBlankSpace: true
                }
            }
        });
        return a;
    }
    function showhidedt() {
        if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
            $('input[type="checkbox"][name="reminder"]').val("on");
            $(".rmdt").show();
        } else {
            $(".rmdt").hide();
            $(".rdate").val("");
            $(".rtime").val("");
        }
    }
    ;
    function showhidedt1() {
        if ($('input[type="checkbox"][name="reminder1"]').is(":checked") == true) {
            $('input[type="checkbox"][name="reminder1"]').val("on");
            $(".rmdt1").show();
        } else {
            $(".rmdt1").hide();
            $(".rdate1").val("");
            $(".rtime").val("");
        }
    }
    function getremindertasks() {
        taskTable.ajax.reload();
    }
    function deleteTask(a) {
        swal({
            title: "Alert",
            text: "Are you sure? Want to Remove This Task ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.blockUI();
            $.ajax({
                url: "<?php echo base_url(); ?>tasks/deleteRec",
                data: {
                    taskId: a
                },
                method: "POST",
                success: function (b) {
                    $.unblockUI();
                    var c = $.parseJSON(b);
                    if (c.status == 1) {
                        taskTable.ajax.reload(null, false);
                        swal("Success !", c.message, "success");
                    } else {
                        swal("Oops...", c.message, "error");
                    }
                }
            });
        });
    }
    function completeTask(a) {
        swal({
            title: "Are you sure? Want to Set Task as Completed ",
            showCancelButton: true,
            confirmButtonColor: "#1ab394",
            confirmButtonText: "Yes, Set as Complete!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            $.blockUI();
            $.ajax({
                url: "<?php echo base_url(); ?>tasks/completedTask",
                data: {
                    taskId: a
                },
                method: "POST",
                success: function (b) {
                    $.unblockUI();
                    var c = $.parseJSON(b);
                    if (c.status == 1) {
                        swal("Success !", c.message, "success");
                        taskTable.ajax.reload(null, false);
                        caseactTable.ajax.reload(null, false);
                    } else {
                        swal("Oops...", c.message, "error");
                    }
                }
            });
        });
    }

    /*TASK RELATED FUNCTIONS ENDS*/
    $(document.body).on('click', '#rolodexsearchResult_data tbody tr', function () {
        if ($(this).hasClass('table-selected')) {
            $(this).removeClass('table-selected');
        } else {
            $('#rolodexsearchResult_data tr').removeClass('table-selected');
            $(this).addClass('table-selected');
        }
    });
    $(document.body).on('click', '#viewSearchedPartieDetails', function () {
        var selectedrows = rolodexSearch.rows('.table-selected').data();
        var caseNo = $("#caseNo").val();
        $('#firmcompanylist').modal('hide');
        if (selectedrows.length > 0) {
            $.ajax({
                url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    cardcode: selectedrows[0][8],
                    caseno: caseNo
                },
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data == false) {
                        swal({
                            title: "Alert",
                            text: "Oops! Some error has occured while fetching details. Please try again later.",
                            type: "warning"
                        });
                    } else {
                        var city = data.city;
                        if (city != null)
                        {
                            var strarray = city.split(',');
                            $('input[name="card2_city"]').val($.trim(strarray[0]));
                            $('input[name="card2_state"]').val($.trim(strarray[1]));
                            $('input[name="card2_zip"]').val($.trim(strarray[2]));
                        }
                        $('input[name="card2_firm"]').val(data.firm);
                        $('input[name="card2_address1"]').val(data.address1);
                        $('input[name="card2_address2"]').val(data.address2);
                        $('input[name="card2_venue"]').val(data.venue);
                        $('.addSearchedParties').attr("disabled", false);
                        $('.editPartyCard').attr("disabled", false);
                    }
                }
            });
        } else {
            swal({
                title: "Alert",
                text: "Please select atleast one row from the list",
                type: "warning"
            });
        }

    });


    $(document.body).on('click', '.event_note_list', function () {
        $('#eventnotelist').modal('show');
    });



    /* CODE TO CHANGE FIRM IN CASECARD DETAILS START*/


    $('#firmcompanylist').on('show.bs.modal', function () {
        $(".dataTables-example").removeAttr("style");
        $(".dataTables_scrollHeadInner").removeAttr("style");
        $(".dataTables-example").css("width", "100% !important;");
        $(".dataTables_scrollHeadInner").css("width", "100% !important;");
    });
    $('#firmcompanylist').on('hide.bs.modal', function () {
        $('#rolodex_search_text_name-error').remove();
        $('#rolodex_search_text_name').removeClass('error');
    });
    $(document.body).on('click', '#viewSearchedPartieDetails', function () {
        var selectedrows = rolodexSearch.rows('.table-selected').data();
        var caseNo = $("#caseNo").val();
        if (selectedrows > 0) {
            $('#firmcompanylist').modal('hide');
            $.ajax({
                url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    cardcode: selectedrows[0][8],
                    caseno: caseNo
                },
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data == false) {
                        swal({
                            title: "Alert",
                            text: "Oops! Some error has occured while fetching details. Please try again later.",
                            type: "warning"
                        });
                    } else {
                        var city = data.city;
                        if (city != null)
                        {
                            var strarray = city.split(',');
                            $('input[name="card2_city"]').val($.trim(strarray[0]));
                            $('input[name="card2_state"]').val($.trim(strarray[1]));
                            $('input[name="card2_zip"]').val($.trim(strarray[2]));
                        }
                        $('input[name="card2_firm"]').val(data.firm);
                        $('input[name="card2_address1"]').val(data.address1);
                        $('input[name="card2_address2"]').val(data.address2);

                        $('input[name="card2_venue"]').val(data.venue);
                        $('.addSearchedParties').attr("disabled", false);
                        $('.editPartyCard').attr("disabled", false);
                    }
                }
            });
        } else {
            swal({
                title: "Alert",
                text: "Oops! Some error has occured while fetching details. Please try again later.",
                type: "warning"
            });
        }

    });
    $(document.body).on('click', '.viewPartiesDetail', function () {
        $("#rolodexPartiesResult tr").removeClass("highlight");
        $(this).addClass('highlight');
        var caseNo = $("#caseNo").val();
        $.ajax({
            url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
            method: 'POST',
            dataType: 'json',
            data: {
                cardcode: $(this).data('cardcode'),
                caseno: caseNo
            },
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (data) {
                if (data == false) {
                    swal({
                        title: "Alert",
                        text: "Oops! Some error has occured while fetching details. Please try again later.",
                        type: "warning"
                    });
                } else {
                    $("#searched_party_fullname").val(data.fullname);
                    $("#searched_party_firmname").val(data.firm);
                    $('#searched_party_type').html(data.type);
                    $('#searched_party_speciality').html(data.speciality);
                    $('#searched_party_address').html(data.address1);
                    $('#searched_party_city').html(data.city);
                    $('#searched_party_cardnumber').html(data.cardcode);
                    $('#searched_party_ssno').html(data.social_sec);
                    $('#searched_party_email').html(data.email);
                    $('#searched_party_licenseno').html(data.licenseno);
                    $('#searched_party_homephone').html(data.home);
                    $('#searched_party_business').html(data.business);
                    $('.addSearchedParties').attr("disabled", false);
                    $('.editPartyCard').attr("disabled", false);
                }
            }
        });

    });

    $(document.body).on('click', '.rolodex-company', function () {
        $("#firmcompanylist").attr("style", "z-index:9999 !important");
        $("#rolodexsearchResult_data  tbody").remove();
        rolodexSearch.destroy();
        $('#firmcompanylist').modal('show');
        $('#rolodexsearchResult_data').DataTable().clear();
    });

    $(document.body).on('keyup', '#rolodex_search_text_name', function (e) {
        if (e.keyCode == '13') {
            $('#rolodex_search_text_name').trigger("blur");
            $('#rolodex_search_text_name').trigger("focus");
        }
    });

    $(document.body).on('blur', '#rolodex_search_text_name', function (e) {
        if (e.type == 'focusout' || e.keyCode == '13')
        {
            if ($('#companylistForm').valid())
            {
                if ($('#rolodex_search_text_name').val().trim().length > 0)
                {
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/searchForRolodex',
                        method: 'POST',
                        data: {rolodex_search_parties: $('#rolodex_search_parties_name').val(), rolodex_search_text: $(this).val()},
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (data) {
                            rolodexSearch.clear();

                            var obj = $.parseJSON(data);
                            $.each(obj, function (index, item) {
                                var html = '';
                                html += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + item.cardcode + "><td>" + item.firm + "</td>";
                                html += "<td>" + item.city + "</td>";
                                html += "<td>" + item.address + "</td>";
                                html += "<td>" + item.address2 + "</td>";
                                html += "<td>" + item.phone + "</td>";
                                html += "<td>" + item.name + "</td>";
                                html += "<td>" + item.state + "</td>";
                                html += "<td>" + item.zip + "</td>";
                                html += "</tr>";
                                rolodexSearch.row.add($(html));
                            });
                            rolodexSearch.draw();
                        }
                    });
                } else
                {
                    swal({
                        title: "Alert",
                        text: "Please enter Firm name.",
                        type: "warning"
                    });
                }
            }
        }
    });
    $('#rolodexsearchResult_data tbody').on('click', 'tr', function () {
        if ($(this).hasClass('table-selected')) {
            $(this).removeClass('table-selected');
        } else {

            $('#rolodexsearchResult_data tr').removeClass('table-selected');
            $(this).addClass('table-selected');
        }
    });
    /* CODE TO CHANGE FIRM IN CASECARD DETAILS ENDS*/
    /*  CASE CARD DETAILS RELATED FUNCTIONS START*/
    $('a[data-target="#editcasedetail"]').click(function () {
        editCaseDetails($('#hdnCaseId').val(), $(this).data('tab'));
    });
    function editCaseDetails(caseNo, selectTab) {
        $.ajax({
            url: '<?php echo base_url(); ?>cases/getAjaxCaseDetail',
            data: 'caseno=' + caseNo,
            method: 'POST',
            dataType: 'json',
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (data) {
                data = data[0];
                if (data.dateenter != '1970-01-01 05:30:00' && data.dateenter != '1899-12-30 00:00:00' && data.dateenter != '0000-00-00 00:00:00' && data.dateenter != null) {
                    var dateenter = moment(data.dateenter).format('MM/DD/YYYY');
                    $('#casedate_entered').val(dateenter);
                }
                if (data.dateopen != '1970-01-01 05:30:00' && data.dateopen != '1899-12-30 00:00:00' && data.dateopen != '0000-00-00 00:00:00' && data.dateopen != null) {
                    var dateopen = moment(data.dateopen).format('MM/DD/YYYY');
                    $('#casedate_open').val(dateopen);
                }
                if (data.followup != '1970-01-01 05:30:00' && data.followup != '1899-12-30 00:00:00' && data.followup != '0000-00-00 00:00:00' && data.followup != null) {
                    var followup = moment(data.followup).format('MM/DD/YYYY');
                    $('#casedate_followup').val(followup);
                }
                if (data.dateclosed != '1970-01-01 05:30:00' && data.dateclosed != '1899-12-30 00:00:00' && data.dateclosed != '0000-00-00 00:00:00' && data.dateclosed != null) {
                    var dateclosed = moment(data.dateclosed).format('MM/DD/YYYY');
                    $('#casedate_closed').val(dateclosed);
                }
                if (data.psdate != '1970-01-01 05:30:00' && data.psdate != '1899-12-30 00:00:00' && data.psdate != '0000-00-00 00:00:00' && data.psdate != null) {
                    var psdate = moment(data.psdate).format('MM/DD/YYYY');
                    $('#casedate_psdate').val(psdate);
                }
                if (data.d_r != '1970-01-01 05:30:00' && data.d_r != '1899-12-30 00:00:00' && data.d_r != '0000-00-00 00:00:00' && data.d_r != null) {
                    var d_r = moment(data.d_r).format('MM/DD/YYYY');
                    $('#casedate_referred').val(d_r);
                }
                if (data.udfall2 != null)
                {
                    var res = data.udfall2.split(",");
                    $('#closedfileno').val(res[0]);
                    $('select[name=editOtherClass]').val(res[1]);
                }
                $('#fileno').val(data.yourfileno);
                $('#editCaseVenue1').val(data.first);
                $('#editCaseVenue').val(data.venue);
                $('select[name=editCaseSubType]').val(data.udfall);
                if ($("#editCaseType").hasClass("select2Class"))
                {
                    if ($('#editCaseType option:contains(' + data.casetype + ')').length) {
                        $('select[name=editCaseType]').val(data.casetype);
                    } else
                    {
                        if (data.casetype)
                            $('#editCaseType').append("<option value='" + data.casetype + "' selected='seleceted'>" + data.casetype + "</option>");
                    }
                } else if ($("#editCaseType").hasClass("cstmdrp"))
                {
                    if (data.casetype)
                        $('#editCaseType').val(data.casetype);
                }
                if ($("#editCaseStat").hasClass("select2Class"))
                {
                    if ($('#editCaseStat option:contains(' + data.casetype + ')').length) {
                        $('select[name=editCaseStat]').val(data.casetype);
                    } else
                    {
                        if (data.casestat)
                            $('#editCaseStat').append("<option value='" + data.casestat + "' selected='seleceted'>" + data.casestat + "</option>");
                    }
                } else if ($("#editCaseStat").hasClass("cstmdrp"))
                {
                    if (data.casestat)
                        $('#editCaseStat').val(data.casestat);
                }
                if ($("#editCaseAttyResp").hasClass("select2Class"))
                {
                    if ($('#editCaseAttyResp option:contains(' + data.atty_resp + ')').length) {
                        $('select[name=editCaseAttyResp]').val(data.atty_resp);
                    } else
                    {
                        if (data.atty_resp)
                            $('#editCaseAttyResp').append("<option value='" + data.atty_resp + "' selected='seleceted'>" + data.atty_resp + "</option>");
                    }
                } else if ($("#editCaseAttyResp").hasClass("cstmdrp"))
                {
                    if (data.atty_resp)
                        $('#editCaseAttyResp').val(data.atty_resp);
                }
                if ($("#editCaseAttyHand").hasClass("select2Class"))
                {
                    if ($('#editCaseAttyHand option:contains(' + data.atty_hand + ')').length) {
                        $('select[name=editCaseAttyHand]').val(data.atty_hand);
                    } else
                    {
                        if (data.atty_hand)
                            $('#editCaseAttyHand').append("<option value='" + data.atty_hand + "' selected='seleceted'>" + data.atty_hand + "</option>");
                    }
                } else if ($("#editCaseAttyHand").hasClass("cstmdrp"))
                {
                    if (data.atty_hand)
                        $('#editCaseAttyHand').val(data.atty_hand);
                }
                if ($("#editCaseAttyPara").hasClass("select2Class"))
                {
                    if ($('#editCaseAttyPara option:contains(' + data.para_hand + ')').length) {
                        $('select[name=editCaseAttyPara]').val(data.para_hand);
                    } else
                    {
                        if (data.para_hand)
                            $('#editCaseAttyPara').append("<option value='" + data.para_hand + "' selected='seleceted'>" + data.para_hand + "</option>");
                    }
                } else if ($("#editCaseAttyPara").hasClass("cstmdrp"))
                {
                    if (data.para_hand)
                        $('#editCaseAttyPara').val(data.para_hand);
                }
                if ($("#editCaseAttySec").hasClass("select2Class"))
                {
                    if ($('#editCaseAttySec option:contains(' + data.sec_hand + ')').length) {
                        $('select[name=editCaseAttySec]').val(data.sec_hand);
                    } else
                    {
                        if (data.sec_hand)
                            $('#editCaseAttySec').append("<option value='" + data.sec_hand + "' selected='seleceted'>" + data.sec_hand + "</option>");
                    }
                } else if ($("#editCaseAttySec").hasClass("cstmdrp"))
                {
                    if (data.sec_hand)
                        $('#editCaseAttySec').val(data.sec_hand);
                }
                $('#case_ps').val(data.ps);
                $('#fileloc').val(data.location);
                $('select[name=case_reffered_by]').val(data.rb);
                if (typeof data.category_events !== 'undefined') {
                    if (typeof data.category_events.quickNote !== 'undefined') {
                        $('#editcase_quicknote').val(data.category_events.quickNote);
                    }
                    if (typeof data.category_events.message1 !== 'undefined') {
                        $('#caseedit_message1').val(data.category_events.message1);
                    }
                    if (typeof data.category_events.message2 !== 'undefined') {
                        $('#caseedit_message2').val(data.category_events.message2);
                    }
                    if (typeof data.category_events.message3 !== 'undefined') {
                        $('#caseedit_message3').val(data.category_events.message3);
                    }
                }
                $('#editcase_caption').val(data.caption1);
                $('#editcasedetail').modal('show');
                $("#edit_case_details select").trigger('change');
                $("a[href='#" + selectTab + "']").trigger("click");
            }
        });
    }

    /* CASE CARD DETAILS RELATED FUNCTIONS END */
    /*CASE DETAILS EDIT FUNCTIONS START*/
    $('#savecase').on('click', function () {
        var alldata = $("form#edit_case_details").serialize();
        var rbName = $("#case_reffered_by option:selected").text();
        var nwCaption = $("#editcase_caption").text();
        $.ajax({
            url: '<?php echo base_url(); ?>cases/editCase',
            method: 'POST',
            data: alldata,
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (result) {
                var data = $.parseJSON(result);
                $.unblockUI();
                if (data.status == '1') {
                    swal("Success !", data.message, "success");
                    $('#edit_case_details')[0].reset();
                    $('#editcasedetail').modal('hide');
                    $('#editcasedetail .modal-title').html('Edit Case');
                } else if (data.status == '0') {
                    swal("Warning !", data.message, "error");
                    $('#edit_case_details')[0].reset();
                    $('#editcasedetail').modal('hide');
                    $('#editcasedetail .modal-title').html('Edit Case');
                }
                $.ajax({
                    url: '<?php echo base_url() . "cases/case_details" ?>',
                    method: "POST",
                    data: {'caseId': $("#hdnCaseId").val()},
                    success: function (result) {
                        var res = $.parseJSON(result);
                        $(".casetype").html(res.display_details.casetype);
                        $(".location").html(res.display_details.location);
                        $(".casestat").html(res.display_details.casestat);
                        $(".yourfileno").html(res.display_details.yourfileno);
                        $(".venue").html(res.display_details.first);
                        $(".atty_resp").html(res.display_details.atty_resp);
                        $(".para_hand").html(res.display_details.para_hand);
                        $(".atty_hand").html(res.display_details.atty_hand);
                        $(".sec_hand").html(res.display_details.sec_hand);
                        if (res.display_details.caption1.length > 55)
                            $(".caption #caption_details").html(res.display_details.caption1.substring(0, 55) + '...');
                        else
                            $(".caption #caption_details").html(res.display_details.caption1);
                        $(".casesubtype").html(res.display_details.udfall);
                        if ($('div.sticky.caption').hasClass('caption-closed')) {
                            $('div.sticky.caption').html(res.display_details.caption1 + ' - ' + 'CLOSED');
                        }
                        var dateenter = 'NA';
                        var dateopen = 'NA';
                        var followup = 'NA';
                        var dateclosed = 'NA';
                        var dr = 'NA';
                        var psdate = 'NA';
                        if (res.display_details.dateenter == '1899-12-31 00:00:00' || res.display_details.dateenter == '1899-12-30 00:00:00' || res.display_details.dateenter == '1970-01-01 00:00:00') {
                            dateenter = 'NA';
                        } else {
                            dateenter = moment(res.display_details.dateenter).format('MM/DD/YYYY');
                        }
                        if (res.display_details.dateopen == '1899-12-31 00:00:00' || res.display_details.dateopen == '1899-12-30 00:00:00' || res.display_details.dateopen == '1970-01-01 00:00:00') {
                            dateopen = 'NA';
                        } else {
                            dateopen = moment(res.display_details.dateopen).format('MM/DD/YYYY');
                        }
                        if (res.display_details.followup == '1899-12-31 00:00:00' || res.display_details.followup == '1899-12-30 00:00:00' || res.display_details.followup == '1970-01-01 00:00:00') {
                            followup = 'NA';
                        } else {
                            followup = moment(res.display_details.followup).format('MM/DD/YYYY');
                        }
                        if (res.display_details.dateclosed == '1899-12-31 00:00:00' || res.display_details.dateclosed == '1899-12-30 00:00:00' || res.display_details.dateclosed == '1970-01-01 00:00:00') {
                            dateclosed = 'NA';
                        } else {
                            dateclosed = moment(res.display_details.dateclosed).format('MM/DD/YYYY');
                        }
                        if (res.display_details.d_r == '1899-12-31 00:00:00' || res.display_details.d_r == '1899-12-30 00:00:00' || res.display_details.d_r == '1970-01-01 00:00:00') {
                            dr = 'NA';
                        } else {
                            dr = moment(res.display_details.d_r).format('MM/DD/YYYY');
                        }
                        if (res.display_details.psdate == '1899-12-31 00:00:00' || res.display_details.psdate == '1899-12-30 00:00:00' || res.display_details.psdate == '1970-01-01 00:00:00' || res.display_details.psdate == '') {
                            psdate = 'NA';
                        } else {
                            psdate = moment(res.display_details.psdate).format('MM/DD/YYYY');
                        }
                        $(".dateenter").html(dateenter);
                        $(".dateopen").html(dateopen);
                        $(".followup").html(followup);
                        $(".d_r").html(dr);
                        $(".rb").html(rbName);
                        $(".ps").html(psdate);
                        $(".dateclosed").html(dateclosed);
                        $(".notes").html(res.category_array.quickNote);
                    }
                });
            }
        });
    });
    $(document.body).on('ifToggled', 'input#field_ct1', function () {
        if ($(this).is(':checked'))
        {
            $('#field_doi2').removeAttr('readonly');
            $('#field_adj1c').mask('CT - 99/99/9999 ', {placeholder: "CT - mm/dd/yyyy"});
        } else
        {
            $("#field_doi2").prop('readonly', true);
            $('#field_adj1c').val('');
        }
    });
    $(document.body).on('change', 'input#field_doi', function () {
        if ($('#field_ct1').is(':checked'))
        {
            $('#field_doi2').removeAttr('readonly');
            $('#field_adj1c').mask('CT - 99/99/9999 ', {placeholder: "CT - mm/dd/yyyy"});
        } else
        {
            var value = $('#field_doi').val();
            $('#field_adj1c').val(value);
        }
    });
    /* INJURY RELATED*/
    $(document.body).on('click', '.injury_view', function () {
        var caseNo = $("#caseNo").val();
        var injuryId = $(this).attr('id');
        $('div#injury_div').block();
        $('table#injuryTable').find('.table-selected').not('tr#injury-' + injuryId).removeClass('table-selected');
        $('#injury-' + injuryId).addClass('table-selected');
        $.ajax({
            url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
            method: 'POST',
            dataType: 'json',
            data: {
                injury_id: injuryId,
                caseno: caseNo
            },
            success: function (data) {
                $('div#injury_div').unblock();
                if (data != false) {
                    setInjuryViewData(data);
                }
            }
        });
        return false;
    });

    function setInjuryViewData(data) {
        console.log(data);
        if (typeof data.app_status !== 'undefined') {
            if (data.app_status.toLowerCase() == 'closed') {
                $('h5#injury_status span').addClass('red');
                $('h5#injury_status span').removeClass('green');
            } else {
                $('h5#injury_status span').addClass('green');
                $('h5#injury_status span').removeClass('red');
            }
            $('h5#injury_status span').html(data.app_status.toUpperCase());
            $('#injury_div .ibox-title .injury_edit').show();
            $('#injury_div .ibox-title .injury_delete').show();
            $('#injury_div .ibox-title .injury_print').show();
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_edit.marg-left5').attr('id', data.injury_id) : $('a.injury_edit').attr('id', '');
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_delete.marg-left5').attr('id', data.injury_id) : $('a.injury_delete').attr('id', '');
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_print').attr('id', data.injury_id) : $('a.injury_print').attr('id', '');
        } else {
            $('h5#injury_status span').html('');
            $('#injury_div .ibox-title .injury_edit').hide();
            $('#injury_div .ibox-title .injury_delete').hide();
            $('#injury_div .ibox-title .injury_print').hide();
        }
        (typeof data.adj1c !== 'undefined') ? $('#td_adj1c').html(data.adj1c) : $('#td_adj1c').html('');
        (typeof data.adj1e !== 'undefined') ? $('#td_adj1e').html(data.adj1e) : $('#td_adj1e').html('');
        (typeof data.adj2a !== 'undefined') ? $('#td_adj2a').html(data.adj2a) : $('#td_adj2a').html('');
        (typeof data.adj1b !== 'undefined') ? $('#td_adj1b').html(data.adj1b) : $('#td_adj1b').html('');
        (typeof data.e_name !== 'undefined') ? $('#td_e_name').html(data.e_name) : $('#td_e_name').html('');
        (typeof data.i_name !== 'undefined') ? $('#td_i_name').html(data.i_name) : $('#td_i_name').html('');
        (typeof data.adjustername !== 'undefined') ? $('#td_i_adjuster').html(data.adjustername) : $('#td_i_adjuster').html('');
        (typeof data.i_phone !== 'undefined') ? $('#td_i_phone').html(data.i_phone) : $('#td_i_phone').html('');
        (typeof data.i_claimno !== 'undefined') ? $('#td_i_claimno').html(data.i_claimno) : $('#td_i_claimno').html('');
        (typeof data.d1name !== 'undefined') ? $('#td_d1_first').html(data.d1name) : $('#td_d1_first').html('');
        (typeof data.d1_firm !== 'undefined') ? $('#td_d1_firm').html(data.d1_firm) : $('#td_d1_firm').html('');
        (typeof data.d1_phone !== 'undefined') ? $('#td_d1_phone').html(data.d1_phone) : $('#td_d1_phone').html('');
        (typeof data.e2_name !== 'undefined') ? $('#td_e2_name').html(data.e2_name) : $('#td_e2_name').html('');
        (typeof data.i2_name !== 'undefined') ? $('#td_i2_name').html(data.i2_name) : $('#td_i2_name').html('');
        (typeof data.i2name !== 'undefined') ? $('#td_i2_first').html(data.i2name) : $('#td_i2_first').html('');
        (typeof data.i2_phone !== 'undefined') ? $('#td_i2_phone').html(data.i2_phone) : $('#td_i2_phone').html('');
    }
    $(document.body).on('click', '.injury_edit', function () {
        var caseNo = $("#caseNo").val();
        var injuryId = $(this).attr('id');
        $.ajax({
            url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
            method: 'POST',
            dataType: 'json',
            data: {
                injury_id: injuryId,
                caseno: caseNo
            },
            success: function (data) {
                if (data != false) {
                    $('#addinjury .modal-title').html('Edit Injury');
                    $('input[name=field_injury_id]').val(data.injury_id);
                    $('input[name=field_first]').val(data.first);
                    $('input[name=field_last]').val(data.last);
                    $('input[name=field_address]').val(data.address);
                    $('input[name=field_city]').val(data.city);
                    $('input[name=field_state]').val(data.state);
                    $('input[name=field_zip_code]').val(data.zip_code);
                    $('input[name=field_social_sec]').val(data.social_sec);
                    $('input[name=field_aoe_coe_status]').val(data.aoe_coe_status);
                    $('input[name=field_dor_date]').val(data.dor_date);
                    $('input[name=field_s_w]').val(data.s_w);
                    $('input[name=field_liab]').val(data.liab);
                    $('input[name=field_other_sol]').val(data.other_sol);
                    $('input[name=field_follow_up]').val(data.follow_up);
                    $('input[name=field_app_status]').val(data.app_status);
                    $('input[name=field_office]').val(data.office);
                    $('select[name=field_status2]').val(data.status2);
                    $('select[name=field_amended]').val(data.amended);
                    $('input[name=field_case_no]').val(data.case_no);
                    $('input[name=field_eamsno]').val(data.eamsno);
                    $('input[name=field_e_name]').val(data.e_name);
                    $('input[name=field_e_address]').val(data.e_address);
                    $('input[name=field_e_city]').val(data.e_city);
                    $('input[name=field_e_state]').val(data.e_state);
                    $('input[name=field_e_zip]').val(data.e_zip);
                    $('input[name=field_e_phone]').val(data.e_phone);
                    $('input[name=field_e_fax]').val(data.e_fax);
                    $('input[name=field_e2_name]').val(data.e2_name);
                    $('input[name=field_e2_address]').val(data.e2_address);
                    $('input[name=field_e2_city]').val(data.e2_city);
                    $('input[name=field_e2_state]').val(data.e2_state);
                    $('input[name=field_e2_zip]').val(data.e2_zip);
                    $('input[name=field_e2_phone]').val(data.e2_phone);
                    $('input[name=field_e2_fax]').val(data.e2_fax);
                    $('select[name=field_i_adjsal]').val(data.i_adjsal);
                    $('input[name=field_i_adjfst]').val(data.i_adjfst);
                    $('input[name=field_i_adjuster]').val(data.i_adjuster);
                    $('input[name=field_i_claimno]').val(data.i_claimno);
                    $('input[name=field_i_name]').val(data.i_name);
                    $('input[name=field_i_address]').val(data.i_address);
                    $('input[name=field_i_city]').val(data.i_city);
                    $('input[name=field_i_state]').val(data.i_state);
                    $('input[name=field_i_zip]').val(data.i_zip);
                    $('input[name=field_i_phone]').val(data.i_phone);
                    $('input[name=field_i_fax]').val(data.i_fax);
                    $('select[name=field_i2_adjsal]').val(data.i2_adjsal);
                    $('input[name=field_i2_adjfst]').val(data.i2_adjfst);
                    $('input[name=field_i2_adjuste]').val(data.i2_adjuste);
                    $('input[name=field_i2_claimno]').val(data.i2_claimno);
                    $('input[name=field_i2_name]').val(data.i2_name);
                    $('input[name=field_i2_address]').val(data.i2_address);
                    $('input[name=field_i2_city]').val(data.i2_city);
                    $('input[name=field_i2_state]').val(data.i2_state);
                    $('input[name=field_i2_zip]').val(data.i2_zip);
                    $('input[name=field_i2_phone]').val(data.i2_phone);
                    $('input[name=field_i2_fax]').val(data.i2_fax);
                    $('input[name=field_d1_first]').val(data.d1_first);
                    $('input[name=field_d1_last]').val(data.d1_last);
                    $('input[name=field_d1_firm]').val(data.d1_firm);
                    $('input[name=field_d1_address]').val(data.d1_address);
                    $('input[name=field_d1_city]').val(data.d1_city);
                    $('input[name=field_d1_state]').val(data.d1_state);
                    $('input[name=field_d1_zip]').val(data.d1_zip);
                    $('input[name=field_d1_phone]').val(data.d1_phone);
                    $('input[name=field_d1_fax]').val(data.d1_fax);
                    $('input[name=field_d2_first]').val(data.d2_first);
                    $('input[name=field_d2_last]').val(data.d2_last);
                    $('input[name=field_d2_firm]').val(data.d2_firm);
                    $('input[name=field_d2_address]').val(data.d2_address);
                    $('input[name=field_d2_city]').val(data.d2_city);
                    $('input[name=field_d2_state]').val(data.d2_state);
                    $('input[name=field_d2_zip]').val(data.d2_zip);
                    $('input[name=field_d2_phone]').val(data.d2_phone);
                    $('input[name=field_d2_fax]').val(data.d2_fax);
                    if (data.ct1 == 1) {
                        $('input[name=field_ct1]').iCheck('check');
                    }
                    $('input[name=field_doi]').val(data.doi);
                    $('input[name=field_doi2]').val(data.doi2);
                    $('input[name=field_adj1a]').val(data.adj1a);
                    $('input[name=field_adj1b]').val(data.adj1b);
                    $('input[name=field_adj1c]').val(data.adj1c);
                    $('input[name=field_adj1d]').val(data.adj1d);
                    $('input[name=field_adj1d2]').val(data.adj1d2);
                    $('input[name=field_adj1d3]').val(data.adj1d3);
                    $('input[name=field_adj1d4]').val(data.adj1d4);
                    $('textarea[name=field_adj1e]').val(data.adj1e);
                    $('select[name=field_pob1]').val(data.pob1);
                    $('select[name=field_pob2]').val(data.pob2);
                    $('select[name=field_pob3]').val(data.pob3);
                    $('select[name=field_pob4]').val(data.pob4);
                    $('select[name=field_pob5]').val(data.pob5);
                    $('textarea[name=field_adj2a]').val(data.adj2a);
                    $('input[name=field_adj3a]').val(data.adj3a);
                    $('input[name=field_adj4a]').val(data.adj4a);
                    $('input[name=field_adj5a]').val(data.adj5a);
                    $('input[name=field_adj5b]').val(data.adj5b);
                    $('input[name=field_adj5c]').val(data.adj5c);
                    $('input[name=field_adj5d]').val(data.adj5d);
                    $('input[name=field_adj6a]').val(data.adj6a);
                    $('input[name=field_adj7a]').val(data.adj7a);
                    $('input[name=field_adj7b]').val(data.adj7b);
                    $('input[name=field_adj7c]').val(data.adj7c);
                    $('input[name=field_adj7d]').val(data.adj7d);
                    $('input[name=field_adj7e]').val(data.adj7e);
                    $('input[name=field_doctors]').val(data.doctors);
                    $('input[name=field_adj8a]').val(data.adj8a);
                    $('input[name=field_adj9a]').val(data.adj9a);
                    $('input[name=field_adj9b]').val(data.adj9b);
                    $('input[name=field_adj9c]').val(data.adj9c);
                    $('input[name=field_adj9d]').val(data.adj9d);
                    $('input[name=field_adj9e]').val(data.adj9e);
                    $('input[name=field_adj9f]').val(data.adj9f);
                    $('input[name=field_adj9g]').val(data.adj9g);
                    $('input[name=field_adj10city]').val(data.adj10city);
                    $('input[name=field_adj10a]').val(data.adj10a);
                    $('input[name=field_firmatty1]').val(data.firmatty1);
                    $('select[name=field_fld1]').val(data.fld1);
                    $('input[name=field_fld2]').val(data.fld2);
                    $('input[name=field_fld3]').val(data.fld3);
                    $('input[name=field_fld4]').val(data.fld4);
                    $('input[name=field_fld5]').val(data.fld5);
                    $('input[name=field_fld11]').val(data.fld11);
                    $('input[name=field_fld12]').val(data.fld12);
                    $('input[name=field_fld13]').val(data.fld13);
                    $('input[name=field_fld14]').val(data.fld14);
                    $('input[name=field_fld20]').val(data.fld20);
                    $('#addinjury').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $("#addinjury").modal('show');
                    $("#addInjuryForm select").trigger('change');
                    $.unblockUI();
                }
            }
        });
        return false;
    });
    var clickFlag = false;
    $(document.body).on('click', '#addInjury', function (e) {

        if ($('#addInjuryForm').valid()) {
            if (clickFlag == true) {
                return false;
            }
            clickFlag = true;
            var injuryId = $('input[name=field_injury_id]').val();
            var caseNo = $("#caseNo").val();

            if (injuryId == 0)
            {
                var injurydefaultcount = $('#count_injury').text();
                var injurycount = parseInt(injurydefaultcount) + 1;
            } else
            {
                var injurydefaultcount = $('#count_injury').text();
                var injurycount = parseInt(injurydefaultcount);

            }
            e.preventDefault();
            $('#addInjury').addClass("disabled");
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/addInjuries',
                method: 'POST',
                dataType: 'json',
                data: $('form#addInjuryForm').serialize() + '&field_caseno=' + caseNo,
                success: function (data) {
                    if (data.status == 1) {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success"
                        },
                                function () {
                                    $("#addinjury").modal('hide');
                                    $.ajax({
                                        url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                                        method: 'POST',
                                        dataType: 'json',
                                        data: {
                                            injury_id: data.injury_id,
                                            caseno: caseNo
                                        },
                                        success: function (data) {
                                            $('div#injury_div').unblock();
                                            if (data != false) {
                                                injurytable.draw();
                                                setInjuryViewData(data);
                                                $("#count_injury").text(injurycount);

                                            }

                                        }
                                    });
                                }
                        );

                    } else {
                        swal("Oops...", data.message, "error");
                    }
                    $('#addInjury').removeClass("disabled");
                    clickFlag = false;
                }
            });
        }
    });
    $(document.body).on('hidden.bs.modal', '#addinjury.modal', function () {
        $('#addinjury a[href="#general"]').tab('show');
    });
    $(document.body).on('click', '.injury_delete', function () {
        var injuryId = this.id;

        var injurydefaultcount = $('#count_injury').text();
        var injurycount = parseInt(injurydefaultcount) - 1;

        swal({
            title: "Are you sure? Want to Remove This Injury ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Remove it!",
            closeOnConfirm: false
        }, function () {
            $('div#injury_div').block();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/RemoveInjury',
                data: {
                    injury_id: injuryId
                },
                method: "POST",
                dataType: 'json',
                success: function (result) {
                    $('div#injury_div').unblock();
                    if (result.status == 1) {
                        setInjuryViewData('');
                        injurytable.rows('tr#injury-' + injuryId).remove().draw();
                        $("#count_injury").text(injurycount);
                        if ($('#injuryTable tbody tr').length < 1) {
                            $('#injuryTable tbody').html('<tr id="noRecord"><td colspan="5"><h4>No Injury Found</h4></td></tr>');
                        }
                        swal("Success !", result.message, "success");
                    } else {
                        swal("Oops...", result.message, "error");
                    }
                }
            });
        });
        return false;
    });
    $(document.body).on('blur', 'input[name="caseFrom"]', function () {
        if (this.value !== '' && this.value > 0) {
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getCaseCaption',
                dataType: 'json',
                method: 'post',
                data: {
                    'caseNo': this.value
                },
                success: function (data) {
                    if (data.status == '1') {
                        $('input#caseFromCaption').val(data.caption);
                    } else {
                        $('input#caseFromCaption').val('');
                    }
                }
            });
        } else {
            $('input#caseFromCaption').val('');
        }
    });
    $(document.body).on('click', '#cloneCaseBtn', function () {
        if ($('#cloneCaseForm').valid()) {
            $.blockUI();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/cloneCase',
                dataType: 'json',
                method: 'post',
                data: $('#cloneCaseForm').serialize(),
                success: function (data) {
                    if (data.status == '1') {
                        swal("Success !", data.message, "success");
                        $('#clonecase').modal('hide');
                    } else {
                        swal("Oops...", data.message, "error");
                    }
                    $.unblockUI();
                }
            });
        }
        return false;
    });
    $('#clonecase.modal').on('hidden.bs.modal', function () {
        $("#cParties").iCheck("uncheck");
        $("#cInjuries").iCheck("uncheck");
        $("#cCaseActivity").iCheck("uncheck");
        $('#cloneCaseForm')[0].reset();
    });
    $(document.body).on('click', '#protectCasebtn', function () {
        setCaseProtectValidation();
        if ($('#protectCaseForm').valid()) {
            $.blockUI();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/protectCase',
                dataType: 'json',
                method: 'post',
                data: $('#protectCaseForm').serialize(),
                success: function (data) {
                    if (data.status == '1') {
                        swal("Success !", data.message, "success");
                        $('#protactcase').modal('hide');
                    } else {
                        swal("Oops...", data.message, "error");
                    }
                    $.unblockUI();
                }
            });
        }
        return false;
    });

    function setCaseCloneValidation() {
        var validator = $('#cloneCaseForm').validate({
            ignore: [],
            rules: {
                'CopyData[]': {
                    required: true
                },
                caseFrom: {
                    remote: {
                        url: '<?php echo base_url(); ?>Prospects/CheckCaseno',
                        type: "post",
                        data: {
                            pro_caseno: function () {
                                return $('#cloneCaseForm :input[name="caseFrom"]').val();
                            }
                        }
                    },
                    digits: true,
                    required: true
                }
            },
            messages: {
                'CopyData[]': "Please select minimum 1 checkbox.",
                caseFrom: {
                    remote: "This caseno is not exist"
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter($(element).parents('div.form-group'));
            }
        });
        return validator;
    }

    function setCaseProtectValidation() {
        var validator = $('#protectCaseForm').validate({
            rules: {
                procpassword: {
                    equalTo: "#propassword"
                }
            },
            messages: {
                procpassword: " Enter Confirm Password Same as Password"
            },
            errorPlacement: function (error, element) {
                error.insertAfter($(element).parents('div.form-group'));
            }
        });
        return validator;
    }
    $('#activity_entry_type').change(function () {
        var data_key = $('#activity_entry_type option:selected').data('key');
        $(".billingtab").addClass("hidden");
        $("#" + data_key).removeClass("hidden");
    });

    $(document.body).on('click', '.add_case_activity', function () {
        myDropzone.removeAllFiles(true);
        var caseNo = $("#caseNo").val();
        $('#add_new_case_activity')[0].reset();
        $.ajax({
            url: '<?php echo base_url(); ?>cases/getBillingDefault',
            method: 'POST',
            dataType: 'json',
            data: {
                caseno: caseNo
            },
            success: function (data) {
                $("#default_staff").val(data.initials).trigger("change");
                $("#default_entry_type").val(data.memo1).trigger("change");
                if (data.freezedt != '1970-01-01 05:30:00' && data.freezedt != '1899-12-30 00:00:00' && data.freezedt != '0000-00-00 00:00:00') {
                    $('.freezeUntil').html(data.freezedt);
                    $('.freezeUntil').parent().removeClass('hidden');
                }
                $("#activity_entry_type").val(data.memo1).trigger("change");
                var activityText = $("#activity_entry_type option:selected").text();
                $("#activity_short_desc").html(activityText);
                $("#activity_event_desc").html(activityText);
                $('#activity_teamno').val(data.biteamno);
                $('#activity_staff').val(data.initials).trigger("change");
                $('.dateoflastpayment').html(data.paidto);
                $('#activity_latefee').html(data.latefee);
                $('#activity_bi_rate').val(data.bihourrate);
            }
        });
        $("#activity_event").val('');
        $("#case_act_atty").trigger('change');
        $("#activity_title").trigger('change');
        $("#case_category").trigger('change');
        $("#color_codes").trigger('change');
        $("#security").trigger('change');
        $("#activity_typeact").trigger('change');
        $("#activity_doc_type").trigger('change');
        $("#activity_style").trigger('change');
        $("#case_act_orientation").trigger('change');
        $("#activity_staff").trigger('change');
        $('#addcaseact .modal-title').html('Add New Case Activity');
        $('#addcaseact').modal('show');
    });
    $(document.body).on('click', '.activity_delete', function () {
        var actId = $(this).data('actid');
        swal({
            title: "Are you sure? Want to Remove This Activity ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            var caseNo = $("#caseNo").val();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/deleteActivity',
                method: 'POST',
                data: {
                    actno: actId,
                    caseno: caseNo
                },
                success: function (data) {
                    if (data == 1) {
                        swal("Deleted!", "Activity Deleted Successfully !!", "success");
                        $('.activity_delete[data-actid=' + actId + ']').parents('tr').remove();
                    } else {
                        swal("Oops...", "Something went wrong!", "error");
                    }
                }
            });
        });
    });
    $(document.body).on('click', '.view_all_attach', function () {
        var caseNo = $("#caseNo").val();
        $.ajax({
            url: '<?php echo base_url(); ?>cases/viewAllAttechment',
            method: 'POST',
            data: {
                actno: $(this).data('actid'),
                caseno: caseNo
            },
            success: function (data) {
                $(".viewAllAtachment").html(data);
                $("#viewMultipleDocument").modal('show');
            }
        });
    });
    function setInjuryViewData(data) {
        console.log(data);
        if (typeof data.app_status !== 'undefined') {
            if (data.app_status.toLowerCase() == 'closed') {
                $('h5#injury_status span').addClass('red');
                $('h5#injury_status span').removeClass('green');
            } else {
                $('h5#injury_status span').addClass('green');
                $('h5#injury_status span').removeClass('red');
            }
            $('h5#injury_status span').html(data.app_status.toUpperCase());
            $('#injury_div .ibox-title .injury_edit').show();
            $('#injury_div .ibox-title .injury_delete').show();
            $('#injury_div .ibox-title .injury_print').show();
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_edit.marg-left5').attr('id', data.injury_id) : $('a.injury_edit').attr('id', '');
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_delete.marg-left5').attr('id', data.injury_id) : $('a.injury_delete').attr('id', '');
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_print').attr('id', data.injury_id) : $('a.injury_print').attr('id', '');
        } else {
            $('h5#injury_status span').html('');
            $('#injury_div .ibox-title .injury_edit').hide();
            $('#injury_div .ibox-title .injury_delete').hide();
            $('#injury_div .ibox-title .injury_print').hide();
        }
        (typeof data.adj1c !== 'undefined') ? $('#td_adj1c').html(data.adj1c) : $('#td_adj1c').html('');
        (typeof data.adj1e !== 'undefined') ? $('#td_adj1e').html(data.adj1e) : $('#td_adj1e').html('');
        (typeof data.adj2a !== 'undefined') ? $('#td_adj2a').html(data.adj2a) : $('#td_adj2a').html('');
        (typeof data.adj1b !== 'undefined') ? $('#td_adj1b').html(data.adj1b) : $('#td_adj1b').html('');
        (typeof data.e_name !== 'undefined') ? $('#td_e_name').html(data.e_name) : $('#td_e_name').html('');
        (typeof data.i_name !== 'undefined') ? $('#td_i_name').html(data.i_name) : $('#td_i_name').html('');
        (typeof data.adjustername !== 'undefined') ? $('#td_i_adjuster').html(data.adjustername) : $('#td_i_adjuster').html('');
        (typeof data.i_phone !== 'undefined') ? $('#td_i_phone').html(data.i_phone) : $('#td_i_phone').html('');
        (typeof data.i_claimno !== 'undefined') ? $('#td_i_claimno').html(data.i_claimno) : $('#td_i_claimno').html('');
        (typeof data.d1name !== 'undefined') ? $('#td_d1_first').html(data.d1name) : $('#td_d1_first').html('');
        (typeof data.d1_firm !== 'undefined') ? $('#td_d1_firm').html(data.d1_firm) : $('#td_d1_firm').html('');
        (typeof data.d1_phone !== 'undefined') ? $('#td_d1_phone').html(data.d1_phone) : $('#td_d1_phone').html('');
        (typeof data.e2_name !== 'undefined') ? $('#td_e2_name').html(data.e2_name) : $('#td_e2_name').html('');
        (typeof data.i2_name !== 'undefined') ? $('#td_i2_name').html(data.i2_name) : $('#td_i2_name').html('');
        (typeof data.i2name !== 'undefined') ? $('#td_i2_first').html(data.i2name) : $('#td_i2_first').html('');
        (typeof data.i2_phone !== 'undefined') ? $('#td_i2_phone').html(data.i2_phone) : $('#td_i2_phone').html('');
    }

    if ($("#caseStatus").val() == 0) {
        $('.case_delete').attr('disabled', "disabled");
    } else {
        $('.recall_case').attr('disabled', "disabled");
    }

    /* Code related to upload video and images start*/
    $('#uploadBtn').change(function () {
        var files = $("#uploadBtn")[0].files;
        var form_data = new FormData();
        var ins = $('input#uploadBtn')[0].files.length;
        var imageFlag = false;
        for (var x = 0; x < ins; x++) {
            var ext = $('#uploadBtn')[0].files[x].name.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            } else {
                imageFlag = true;
                form_data.append("files_" + x, $('#uploadBtn')[0].files[x]);
            }
        }
        form_data.append('caseno', $('#hdnCaseId').val());
        if (imageFlag == true) {
            document.getElementById('myProgress1').style.display = 'block';
            var elem = document.getElementById("myBar1");
            var width = 10;
            var id = setInterval(frame, 100);

            function frame() {
                if (width >= 100) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/UploadPhoto',
                        type: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            var res = $.parseJSON(data);
                            if (res.status == 1) {
                                var image_uploded = '';
                                for (var x = 0; x < res.file.length; x++) {
                                    var link = '<?php echo base_url() ?>' + 'assets/casePhotoVideo/' + $('#caseNo').val() + '/photo/' + res.file[x];
                                    var dp_link = '<?php echo DOCUMENTPATH ?>' + 'casePhotoVideo/' + $('#case_no').val() + '/photo/' + res.file[x];
                                    image_uploded += '<a class="example-image-link scroll-el" data-lightbox="example-1" href="' + link + '" title="Image from <?php echo APPLICATION_NAME; ?>" data-gallery=""><img class="example-image" src="' + link + '"width="100px" height="100px"></a><i class="fa fa-close" id="del_photo" data-id="' + dp_link + '"> </i>';
                                }
                                swal("Success !", res.message, "success");
                                $("#myBar1").text("");
                                $("#myBar1").removeAttr("style");
                                document.getElementById('myProgress1').style.display = 'none';
                                $('.image-row.lightBoxGallery').append(image_uploded);
                                setImagePagination();
                            }
                        }
                    });
                    window.clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                    elem.innerHTML = width * 1 + '%';
                }
            }
        } else {
            swal("Oops...", "Please enter valid extension('png','jpg','jpeg')");
        }
    });
    $('#uploadBtn1').change(function () {
        var files = $("#uploadBtn1")[0].files;
        var form_data = new FormData();
        var ins = $('input#uploadBtn1')[0].files.length;
        var image_uploded = '';
        var valid_file = true;
        var cnt = 0;
        for (var x = 0; x < ins; x++) {
            var valid_extension = /(\.mp4)$/i;
            var fld_value = $('#uploadBtn1')[0].files[x].name;
            form_data.append('caseno', $('#hdnCaseId').val());
            if (!valid_extension.test(fld_value)) {
                swal({
                    title: "Alert",
                    text: "Oops! Please provide valid video format.\n" + fld_value + ' is not valid video format',
                    type: "warning"
                });
            } else {
                form_data.append("files_" + x, $('#uploadBtn1')[0].files[x]);
                cnt++;
            }
        }
        if (cnt > 0) {
            document.getElementById('myProgress').style.display = 'block';
            $.ajax({
                url: '<?php echo base_url(); ?>cases/Uploadvideo',
                type: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function (xhr) {},
                complete: function (jqXHR, textStatus) {},
                success: function (data) {
                    var elem = document.getElementById("myBar");
                    var width = 10;
                    var id = setInterval(frame, 10);

                    function frame() {
                        if (width >= 100) {
                            var res = $.parseJSON(data);
                            if (res.status == 1) {
                                var video_uploded = '';
                                for (var x = 0; x < res.file.length; x++) {
                                    var link = '<?php echo base_url() ?>' + 'assets/casePhotoVideo/' + $('#caseNo').val() + '/video/' + res.file[x];
                                    var delink = '<?php echo DOCUMENTPATH; ?>casePhotoVideo/' + $('#caseNo').val() + '/video/' + res.file[x];
                                    image_uploded += '<a class="case_video" data-video="' + link + '" title="Image from Unsplash" data-toggle="modal" data-target="#videoModal" > <video width="145px" height="145px" controls><source src="' + link + '" /></video></a><i class="fa fa-close" id="del_video" data-id="' + delink + '"> </i> ';
                                }
                                swal({
                                    title: "Success!",
                                    text: res.message,
                                    type: "success"
                                });
                                $("#myBar").text("");
                                $("#myBar").removeAttr("style");
                                document.getElementById('myProgress').style.display = 'none';
                                $('.video').append(image_uploded);
                                setVideoPagination();
                            }
                            window.clearInterval(id);
                        } else {
                            width++;
                            elem.style.width = width + '%';
                            elem.innerHTML = width * 1 + '%';
                        }
                    }
                }
            });
        }
        ;
    });
    function setImagePagination() {
        var start = 0;
        var nb = 16;
        var end = start + nb;
        var length = $('.image-row.lightBoxGallery img').length;
        var list = $('.image-row.lightBoxGallery img');
        list.hide().filter(':lt(' + (end) + ')').show();
        if (start == 0) {
            $('a.prev').attr('disabled', true);
        }
        $('.prev, .next').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('prev')) {
                start -= nb;
            } else {
                start += nb;
            }
            if (start < 0 || start >= length) {
                start = 0;
            }
            end = start + nb;
            if (start == 0) {
                list.hide().filter(':lt(' + (end) + ')').show();
            } else {
                list.hide().filter(':lt(' + (end) + '):gt(' + (start - 1) + ')').show();
            }
            if (start == 0) {
                $('a.prev').attr('disabled', true);
            } else {
                $('a.prev').attr('disabled', false);
            }
            if (end == length) {
                $('a.next').attr('disabled', true);
            } else {
                $('a.next').attr('disabled', false);
            }
        });
    }
    setImagePagination();

    function setVideoPagination() {
        var start = 0;
        var nb = 9;
        var end = start + nb;
        var length = $('.video .case_video video').length;
        var list = $('.video .case_video video');
        list.hide().filter(':lt(' + (end) + ')').show();
        if (start == 0) {
            $('a.vprev').attr('disabled', true);
        }
        $('.vprev, .vnext').click(function (e) {
            e.preventDefault();
            if ($(this).hasClass('vprev')) {
                start -= nb;
            } else {
                start += nb;
            }
            if (start < 0 || start >= length) {
                start = 0;
            }
            end = start + nb;
            if (start == 0) {
                list.hide().filter(':lt(' + (end) + ')').show();
            } else {
                list.hide().filter(':lt(' + (end) + '):gt(' + (start - 1) + ')').show();
            }
            if (start == 0) {
                $('a.vprev').attr('disabled', true);
            } else {
                $('a.vprev').attr('disabled', false);
            }
            if (end == length) {
                $('a.vnext').attr('disabled', true);
            } else {
                $('a.vnext').attr('disabled', false);
            }
        });
    }
    setVideoPagination();
    /* photo/video code ends*/
    /* CALENDAR RELATED CHANGES START*/
    $('#btnSaveCalendar').on('click', function () {
        var url = '<?php echo base_url(); ?>calendar/addCalendarData';
        if ($('input[name=eventno]').val() != 0) {
            url = '<?php echo base_url(); ?>calendar/editCalendarData';
        }
        if ($('#calendarForm').valid()) {
            $.blockUI();
            $.ajax({
                url: url,
                dataType: 'json',
                method: 'post',
                data: $('#calendarForm').serialize(),
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.status == '1') {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success"
                        },
                                function ()
                                {
                                    $('#calendarForm')[0].reset();
                                    $('#addcalendar').modal('hide');
                                    $('#addcalendar .modal-title').html('Add Calendar');
                                    caseDetailCalendarTable.draw();
                                    createCalendarRow(data.calendarData);
                                }
                        );
                    } else {
                        swal("Oops...", data.message, "error");
                    }
                    $.unblockUI();
                }
            });
        } else {
        }
        return false;
    });
    function createCalendarRow(data) {
        var html = '';
        var calendarData = '';
        var json_color = jQuery.parseJSON('<?php echo color_codes ?>');
        if (data) {
            if (json_color.hasOwnProperty(data.color)) {
                style = "background-color:" + json_color[data.color][2] + ";color:" + json_color[data.color][1];
            } else {
                style = "background-color:#FFFFFF;color: #000000";
            }
            html = "<tr class='gradeX' id='calEvent_" + data.eventno + "' style='" + style + "'>";
            html += "<td>" + moment(data.date).format('DD/MM/YYYY HH:mm') + "</td>";
            html += "<td>" + data.atty_hand + "</td>";
            html += "<td>" + data.attyass + "</td>";
            html += "<td>" + data.first + ' ' + data.last + ' vs ' + data.defendant + "</td>";
            html += "<td>" + data.event + "</td>";
            html += "<td>" + data.venue + "</td>";
            html += "<td>" + data.judge + "</td>";
            html += "<td>" + data.notes + "</td>";
            html += "<td>";
            html += "<a class='btn btn-xs btn-info mar-right4 event_edit' data-eventno=" + data.eventno + " href='javascript:'><i class='icon fa fa-paste'></i> Edit</a>";
            html += "<a class='btn btn-xs btn-danger event_delete' data-eventno=" + data.eventno + " href='javascript:'><i class='icon fa fa-times'></i> Delete</a>";
            html += "</td>";
            html += "</tr>";
            var existing = document.getElementById('Nxtevtdt').innerHTML;
            var newEvtime = data.caltime;
            if (existing == 'N/A')
            {
                var newEvDate = moment(data.date).format("DD MMM YYYY");
                calendarData += '<div class="claendar-event fadeIn animated marg-bot15">';
                calendarData += '<h3>No Event Available</h3>';
            } else
            {
                var nwdt = moment(data.date).format("YYYY-MM-DD");
                var ext = moment(existing).format("YYYY-MM-DD");
                if (nwdt < ext)
                {
                    var newEvDate = moment(data.date).format("DD MMM YYYY");
                } else
                {
                    var newEvDate = existing;
                }
                calendarData += '<a href="<?php echo base_url(); ?>calendar/index/' + moment(newEvDate).format("YYYY-MM-DD") + '" target="_blank">';
                calendarData += '<div class="claendar-event fadeIn animated marg-bot15">';
                calendarData += '<span>Next Calendar Event</span><br>';
                calendarData += '<span id="Nxtevtdt">' + newEvDate + ' ' + newEvtime + '</span> <br/><span>Event Venue : ' + data.venue + '</span><br/><span>Event Attorney : ' + data.atty_hand + '</span></div></a>';
            }
            document.getElementById('calenderEventDiv').innerHTML = calendarData;
        }
        caseDetailCalendarTable.draw();
        return html;
    }
    $(document.body).on('click', '.event_edit', function () {
        $('#addcalendar').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#calendarForm')[0].reset();
        getCalendarDataByEventno($(this).data('eventno'));
    });
    $(document.body).on('click', '.event_delete', function () {
        var eventno = $(this).data('eventno');
        swal({
            title: "Are you sure Want to Delete this Event ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: '<?php echo base_url(); ?>calendar/removeEvent',
                dataType: 'json',
                method: 'post',
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                data: {
                    eventno: eventno
                },
                success: function (res) {
                    if (res.status == 1) {
                        swal("Success !", res.message, "success");
                        if (caseDetailCalendarTable.rows('[id=calEvent_' + eventno + ']').any()) {
                            caseDetailCalendarTable.rows($('tr#calEvent_' + eventno)).remove().draw();
                        }
                        calendarData = '';
                        if (res.eventDate != 'N/A') {
                            var newEvDate = moment(res.eventDate).format("DD MMM YYYY");
                            calendarData += '<a href="<?php echo base_url(); ?>calendar/index/' + moment(res.eventDate).format("YYYY-MM-DD") + '" target="_blank">';
                            calendarData += '<div class="claendar-event fadeIn animated marg-bot15">';
                            calendarData += '<span>Next Calendar Event</span><br>';
                            calendarData += '<span id="Nxtevtdt">' + newEvDate + '</span> </div>';
                        } else {
                            var newEvDate = 'N/A';
                            calendarData += '<div class="claendar-event fadeIn animated marg-bot15">';
                            calendarData += '<span><h3>No Event Available</h3></span><br>';
                        }
                        $.unblockUI();
                        document.getElementById('calenderEventDiv').innerHTML = calendarData;
                    } else {
                        swal("Oops...", res.message, "error");
                    }
                }
            });
        });
    });
    $(document.body).on('click', '.add_calendar_event', function () {
        $('#addcalendar').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#calendarForm')[0].reset();
        $('input[name=eventno]').val(0);
        if ($('#caseNo').val().length > 0) {
            $(".caseno").attr("readonly", true);
            $('#calendarForm .caseno').val($('#caseNo').val());
        }
    });
    $('#addcalendar.modal').on('hidden.bs.modal', function () {
        $('#addcalendar h4.modal-title').text('Add Appointment/Event To Calendar');
        $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
            this.value = '';
        });
        var validator = setCalendarValidation();
        validator.resetForm();
        $('input[name=eventno]').val(0);
        $('#addcalendar a[href="#general1"]').tab('show');
        $('#addcalendar .tab-content .tab-pane').not('#general1').removeClass('active');
        $('#addcalendar .tab-content div#general1').addClass('active');
    });
    function getCalendarDataByEventno(eventno) {
        $.blockUI();
        $.ajax({
            url: '<?php echo base_url(); ?>calendar/getCalendarData',
            dataType: 'json',
            method: 'post',
            data: {
                eventno: eventno
            },
            success: function (res) {
                var data = res;
                $('#addcalendar .modal-title').html('Edit Calendar');
                $('#lastDate').prop("disabled", false);
                $("#lastDate").css("background-color", "");
                $('input[name=eventno]').val(data.eventno);
                $('input[name=calstat]').val(data.calstat);
                if ($("#atty_hand").hasClass("select2Class"))
                {
                    $('select[name=atty_hand]').val(data.atty_hand);
                } else if ($("#atty_hand").hasClass("cstmdrp"))
                {
                    $('#atty_hand').val(data.atty_hand);
                }
                var addmin_attyass = '<?php echo isset($system_caldata[0]->calattyass) ? $system_caldata[0]->calattyass : '' ?>';
                if (addmin_attyass == '1') {
                    $("select[name=attyass]").removeAttr("disabled");
                    data.attyass = (data.attyass == 'NA') ? '' : data.attyass;
                    if ($("#attyass").hasClass("select2Class"))
                    {
                        $('select[name=attyass]').val(data.attyass);
                    } else if ($("#attyass").hasClass("cstmdrp"))
                    {
                        $('#attyass').val(data.attyass);
                    }
                } else {
                    $("select[name=attyass]").attr("disabled", true);
                }
                $('#judge').val(data.judge);
                $('input[name=first]').val(data.first);
                $('input[name=last]').val(data.last);
                $('input[name=defendant]').val(data.defendant);
                $('#venue').val(data.venue);
                $('input[name=location]').val(data.location);
                $('input[name=caldate]').val(data.caldate);
                var clocktime = moment(data.caltime, 'hh:mm A');
                var eveTime = clocktime.format('hh:mm A');
                $('#caltime').val(eveTime);
                $('#caltime').mdtimepicker({
                    'default': eveTime
                });
                $('input[name=totime]').val(data.tottime);
                $('#event').val(data.event);
                $('textarea[name=notes]').val(data.notes);
                $('select[name=tickle]').val(data.tickle);
                $('input[name=tickledate]').val(data.tickdate);
                $('input[name=tickletime]').val(data.ticktime);
                $('select[name=whofrom]').val(data.whofrom);
                $('select[name=whoto]').val(data.whoto);
                if (data.caseno != 0)
                    $('input[name=caseno]').val(data.caseno);
                else
                    $('input[name=caseno]').val('');
                $('input[name=initials0]').val(data.initials0);
                $('input[name=initials]').val(data.initials);
                $('input[name=chgwho]').val(data.chgwho);
                $('input[name=chgdate]').val(data.chgdate);
                $('input[name=chgtime]').val(data.chgtime);
                (data.protected == '1') ? $('input[name=protected]').iCheck('check') : $('input[name=protected]').iCheck('uncheck');
                (data.rollover == '1') ? $('input[name=rollover]').iCheck('check') : $('input[name=rollover]').iCheck('uncheck');
                $('select[name=todo]').val(data.todo);
                $('select[name=color]').val(data.color);
                $("#addcalendar select").trigger('change');
                $.unblockUI();
            }
        });
    }

    /* CALENDAR RELATED CHANGES ENDS*/
    function remove_error(id)
    {
        var myElem = document.getElementById(id + '-error');
        if (myElem === null)
        {
        } else
        {
            document.getElementById(id + '-error').innerHTML = '';
        }
    }
</script>