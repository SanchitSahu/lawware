<?php
include('js/pncjs.php');
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '')
            unset($holodaylist[$key]);
    }
}
?>
<style>
    .chosen-container {
        width: 100% !important;
    }
    .pac-container{z-index: 9999;}
    .pncerror
    {
        color:red;
    }
    .ui-datepicker-trigger{
        border:none;
        background:none;
        position: absolute;
        right: 19px;
        top: 31px;
    }
    #printenvelope label{font-weight: 400;}
    #addTaskForm1 .ui-datepicker-trigger{right: 6px !important;top: 6px !important;}
    #addTaskForm .ui-datepicker-trigger{right: 6px !important;top: 6px !important;}
    #printProspectmodal label{font-weight: normal;}
    #addcase label{font-weight: normal;}
    #addtask1 label{font-weight: normal;}
    #addtask1 .modal-body{padding: 0px 15px 0px;}
    #addtask label{font-weight: normal;}
    #addtask .modal-body{padding: 0px 15px 0px;}
    #prospectForm label{font-weight: normal;}
    #proceedprospects .modal-body{padding: 0px 15px 0px;}
    #proceedprospects .panel-body{padding: 0px;}
    #proceedprospects .modal-dialog{margin-top: 3px;}
    #proceedprospects .form-group .form-control{margin-bottom: 5px;}
    #printenvelope .modal-body,
    #printProspectmodal .modal-body {padding: 5px 15px 0px;}
    #printenvelope .modal-body{padding-bottom: 15px;}
    #addcontact .modal-body{padding:0px 10px 5px}
    #addcontact .tabs-container .panel-body{padding:0px;}
    .chosen-container-single .chosen-single{cursor: pointer;}
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<?php
$session_Arr = $this->session->userdata('user_data');
?>

<div id="addtask" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <form id="addTaskForm" name="addTaskForm" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <label class="col-sm-3 no-padding">From</label>
                                        <div class="col-sm-9 no-padding">
                                            <select class="form-control select2Class" name="whofrom" id="whofrom">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected="selected"' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 no-padding">To</label>
                                        <div class="col-sm-10 no-padding">
                                            <select class="form-control select2Class staffTO staffvacation" name="whoto2" onchange="return remove_error('whoto2')" id="whoto2">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-5 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-padding">Finish By <span class="asterisk">*</span></label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" onchange="return remove_error('taskdatepicker')" id="taskdatepicker" />

                                            <label class="pncerror"></label>
                                        </div></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task <span class="asterisk">*</span></label>
                                        <textarea class="form-control" name="event" style="height: 150px;resize: none;"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Color</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="color" id="color">
                                                <?php
                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                foreach ($jsoncolor_codes as $k => $color) {
                                                    ?>
                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Task Type</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="typetask">
                                                <option value="">Select Task Type</option>
                                                <option value=""></option>
                                                <option value="follow up">Follow Up</option>
                                                <option value="statute">Statute</option>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Priority</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="priority" id="priority">
                                                <option value="L">Low</option>
                                                <option value="M">Medium</option>
                                                <option value="H">High</option>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Phase</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="phase">
                                                <option value="">Select Phase</option>
                                                <option value=""></option>
                                                <option value="begining">Beginning</option>
                                                <option value="middle">Middle</option>
                                                <option value="final">Final</option>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="container-checkbox pull-left">Reminder
                                            <input type="checkbox" class="treminder" name="reminder" onchange="showhidedt();">
                                            <span class="checkmark"></span><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="You will get notified on your screen prior to 10 minutes of your scheduled task. To enable calender reminder notification in your browser to receive all notifications, allow notification in site settings." id="pnctaskreminder"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group">
                                        <label class="col-sm-3">Date</label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" name="rdate" class="rdate form-control" data-mask="99/99/9999" placeholder readonly>
                                        </div></div>
                                </div>
                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group">
                                        <label class="col-sm-3">Time</label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" name="rtime" class="rtime form-control" class="form-control">
                                        </div></div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <!--<button type="button" class="btn btn-md btn-primary pull-left">Save to Group</button>-->
                                        <button type="button" class="btn btn-md btn-primary" id="addTask" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
<!--                                        <span class="marg-top5 pull-right"><label>Email&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="notify" ></label></span>-->
                                        <label class="container-checkbox pull-right marg-top5">&nbsp;Email
                                            <input type="checkbox" class="" name="notify" >
                                            <span class="checkmark"  style="padding-right: 5px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="printProspectmodal" class="modal fade printProspectmodal" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Search For a PNC</h4>
            </div>
            <div class="modal-body">
                <form id="search_prospectForm" method="POST">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-15">General</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-16">Detailed</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-17">Mailing</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-19">Advanced</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-15" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Type of Case</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <select class="form-control <?php
                                                                    if (isset($casetypedrp) && $casetypedrp == 1) {
                                                                        echo 'cstmdrp';
                                                                    }
                                                                    ?> " name="pro_casetype1" id="pro_casetype1" <?php
                                                                            if (isset($casetypevalidate) && $casetypevalidate == 1) {
                                                                                echo 'required';
                                                                            }
                                                                            ?>   >
                                                                        <option value="">Select Case Type</option>
                                                                        <?php
                                                                        if (isset($casetype)) {
                                                                            foreach ($casetype as $key => $val) {
                                                                                ?>
                                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>

                                                                </div>
                                                            </div></div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Phone H</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <input type="text" class="form-control contact" placeholder="(xxx) xxx-xxxx" name="pro_home1" id="pro_home1" maxlength="14" />
                                                                </div></div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Status</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <select name="pro_casestat1" id="pro_casestat1"  placeholder="" class="form-control <?php
                                                                    if (isset($casestatusdrp) && $casestatusdrp == 1) {
                                                                        echo 'cstmdrp';
                                                                    } else {
                                                                        echo 'select2Class';
                                                                    }
                                                                    ?>" <?php
                                                                    if (isset($casestatvalidate) && $casestatvalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?>>
                                                                        <option value="">Select Case Status</option>
                                                                        <?php
                                                                        if (isset($casestat)) {
                                                                            foreach ($casestat as $key => $val) {
                                                                                ?>
                                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div></div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Phone B</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <input type="text" class="form-control contact" placeholder="(xxx) xxx-xxxx" name="pro_business1" id="pro_business1" maxlength="14"/>
                                                                </div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Initial Contact</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="pro_initials01" id="pro_initials01">
                                                                <option value="">Select Initial Contact</option>
                                                                <?php
                                                                if (isset($atty_resp)) {
                                                                    foreach ($atty_resp as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Cases</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select type="select" name="pro_case1"  placeholder="Search by" class="form-control select2Class">

                                                                <option value="All">All</option>
                                                                <option value="attach">Attached PNC</option>
                                                                <option value="unattach">UnAttached PNC</option>

                                                            </select>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Pending With</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <select class="form-control select2Class" name="pro_pendwith1" id="pro_pendwith1">
                                                                <option value="">Select Pending With</option>
                                                                <?php
                                                                if (isset($para_hand)) {
                                                                    foreach ($para_hand as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">First</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control "  name="pro_first1_s" id="pro_first1_s"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Attorney</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="pro_atty1" id="pro_atty1">
                                                                <option value="">Select Attorney</option>
                                                                <?php
                                                                if (isset($sec_hand)) {
                                                                    foreach ($sec_hand as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control "  name="pro_last1_s" id="pro_last1_s"/>
                                                        </div></div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">

                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date Entered</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control injurydate" name="pro_daterefin1" id="pro_daterefin1" data-mask="99/99/9999" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Contact Date</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control preinjurydate" name="pro_datelastcn1" id="pro_datelastcn1"/>
                                                        </div></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-16" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referral Code</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control <?php
                                                            if (isset($casereferraldrp) && $casereferraldrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>"  name="pro_refcode1" id="pro_refcode1" >
                                                                <option value="">Select Referral Code</option>
                                                                <?php
                                                                if (isset($referral)) {
                                                                    foreach ($referral as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referral Source</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control <?php
                                                            if (isset($referralsrcdrp) && $referralsrcdrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_refsource1" id="pro_refsource1" >
                                                                <option value="">Select Referral Source</option>
                                                                <?php
                                                                if (isset($referralsrc)) {
                                                                    foreach ($referralsrc as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date Referred</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control injurydate valid" id="pro_dateref1" name="pro_dateref1" data-mask="99/99/9999" aria-invalid="false">
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referral By</label>

                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control <?php
                                                            if (isset($referredbydrp) && $referredbydrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_refby1" id="pro_refby1" >
                                                                <option value="">Select Referred By</option>
                                                                <?php
                                                                if (isset($referredby)) {
                                                                    foreach ($referredby as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referred To</label>
                                                        <div class="col-sm-7 no-padding marg-bot15">
                                                            <select class="form-control <?php
                                                            if (isset($referredtodrp) && $referredtodrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_refto1" id="pro_refto1">
                                                                <option value="">Select Referred To</option>
                                                                <?php
                                                                if (isset($referredto)) {
                                                                    foreach ($referredto as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Our Ref Status</label>
                                                        <div class="col-sm-7 no-padding marg-bot15">
                                                            <select class="form-control <?php
                                                            if (isset($rstatusdrp) && $rstatusdrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_outrefstat1" name="pro_outrefstat1" >
                                                                <option value="">Select Ref Status</option>
                                                                <?php
                                                                if (isset($referalstatus)) {
                                                                    foreach ($referalstatus as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>

                                                        </div></div>
                                                    <div class="row">

                                                        <div class="form-group">
                                                            <label class="col-sm-5">Ref Fee</label>
                                                            <div class="col-sm-7 no-left-padding marg-bot15">
                                                                <select class="form-control select2Class" id="pro_reffee1" name="pro_reffee1">
                                                                    <option value="">Select Ref Fee</option>
                                                                    <option value="Yes">YES</option>
                                                                    <option value="NO">No</option>
                                                                </select>
                                                            </div></div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Amount</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" name="pro_amount1" id="pro_amount1"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date Paid</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control injurydate" name="pro_datepaid1" id="pro_datepaid1" data-mask="99/99/9999"/>
                                                        </div></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-17" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Limit the Found PNC to </label>
                                                        <label>(leave Unchecked for all)</label>
                                                    </div>

                                                    <?php
                                                    if ((isset($mtitle1) && $mtitle1 != '' ) || (isset($mtitle2) && $mtitle2 != '') || (isset($mtitle3) && $mtitle3 != '') || (isset($mtitle4) && $mtitle4 != '') || (isset($mtitle5) && $mtitle5 != '') || (isset($mtitle6) && $mtitle6 != '')) {
                                                        ?>
                                                        <div class="form-group">
                                                            <?php
                                                            if (isset($mtitle1) && $mtitle1 != '') {

                                                                echo '<span><label><input type="checkbox" name="pro_mailingh_1" class="i-checks" >Mailing - Holiday List</label></span><br/>';
                                                            }
                                                            if (isset($mtitle2) && $mtitle2 != '') {


                                                                echo '<span><label><input type="checkbox" name="pro_mailing1_1" class="i-checks">' . $mtitle2 . '</label></span><br/>';
                                                            }
                                                            if (isset($mtitle3) && $mtitle3 != '') {
                                                                echo '<span><label><input type="checkbox" name="pro_mailing2" class="i-checks">' . $mtitle3 . '</label></span><br/>';
                                                            }
                                                            if (isset($mtitle4) && $mtitle4 != '') {
                                                                echo '<span><label><input type="checkbox" name="pro_mailing3_1" class="i-checks">' . $mtitle4 . '</label></span><br/>';
                                                            }
                                                            if (isset($mtitle5) && $mtitle5 != '') {
                                                                echo '<span><label><input type="checkbox" name="pro_mailing4_1" class="i-checks">' . $mtitle5 . '</label></span><br/>';
                                                            }
                                                            if (isset($mtitle6) && $mtitle6 != '') {
                                                                echo '<span><label><input type="checkbox" name="pro_mailing5_1" class="i-checks">' . $mtitle6 . '</label></span>';
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <div class="form-group">
                                                            <span><label><input type="checkbox" name="pro_mailingh_1" class="i-checks"> Mailing - Holiday List</label></span>
                                                        </div>

                                                        <?php
                                                    }
                                                    ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-18" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Employer</label>
                                                        <input type="text" name="pro_injn_emp1" id="pro_injn_emp1" class="form-control"  />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-19" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="ibox-content">
                                                    <div class="form-group">

                                                        <input type="text" id="pro_advance1" name="pro_advance1"  class="form-control" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center marg-top15">
                                <input type="hidden" id="prospectId" name="prospectId">
                                <button type="submit" class="btn btn-md btn-primary" id="search_prospect">Search</button>
                                <button type="button" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="ListProspects" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <span id="record_count"> </span>
                    Founds PNC
                    <span class="total_prospect"> </span>
                    <button style="float: right;margin-left: 5px;margin-top: -7px;margin-right: 5px;" type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button style="float: right;margin-left: 5px;margin-top: -7px;" type="button" class="btn btn-primary edit_prospect_select">Edit</button>
                    <button style="float: right;margin-left: 5px;margin-top: -7px;" type="button" class="btn btn-primary print_prospect" >Print</button>
                </h4>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">


                            <table class="table table-bordered prospect-dataTable" id="ProspectListDatatable" width="100%">
                                <thead>
                                    <tr>
                                        <th  width="10%">Date Referred</th>
                                        <th width="5%">Pend with</th>
                                        <th  width="5%">Atty</th>
                                        <th id="th-finishby" width="10%">First</th>
                                        <th id="th-type" width="5%">Last</th>
                                        <th id="th-priority" width="5%">int cont</th>
                                        <th id="th-event" width="30%" class="caseact_event">Casetype</th>
                                        <th id="th-phase" width="5%">CaseStat</th>
                                        <th id="th-completed" width="10%">Date Last co</th>
                                        <th id="th-action" width="10%">city</th>
                                        <th id="th-phase" width="5%">State</th>
                                        <th id="th-completed" width="10%">Zip</th>
                                        <th id="th-action" width="10%">Ref code</th>
                                        <th id="th-phase" width="5%">Ref Source</th>
                                        <th id="th-completed" width="10%">Ref By</th>
                                        <th id="th-action" width="10%">Ref To</th>
                                        <th id="th-phase" width="5%">Ref fee</th>
                                        <th id="th-completed" width="10%">out Ref start</th>
                                        <th id="th-action" width="10%">Amount</th>
                                        <th id="th-completed" width="10%">Date paid</th>
                                        <th id="th-action" width="10%">To do next</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="ibox-content marg-bot15 party">
                            <form>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12 prospect_detail">
                                            <div class="table-responsive ">
                                                <table class="table no-bottom-margin" >
                                                    <tbody>
                                                        <tr >
                                                            <td class="text-right" width="100"><strong>Name :</strong></td>
                                                            <td class="pro_first_new"></td>


                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Cell Phone :</strong></td>
                                                            <td class="pro_cell_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Home Phone :</strong></td>
                                                            <td class="pro_home_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Work Phone :</strong></td>
                                                            <td class="pro_business_new"></td>

                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Address :</strong></td>
                                                            <td class="pro_address_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>City,St,Zip :</strong></td>
                                                            <td class="pro_city_st_zip_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Social Sec :</strong></td>
                                                            <td class="pro_social_sec_new">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Birth Date :</strong></td>
                                                            <td class="pro_birthdate_new">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100" style="border-bottom: 1px solid #e7eaec;"><strong>Email :</strong></td>
                                                            <td style="border-bottom: 1px solid #e7eaec;" class="pro_email_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Type :</strong></td>
                                                            <td class="pro_type_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Status :</strong></td>
                                                            <td class="pro_casestat_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Date Ent :</strong></td>
                                                            <td class="pro_daterefin_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Last Cont :</strong></td>
                                                            <td class="pro_datelastcn_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Init Contact :</strong></td>
                                                            <td class="pro_initials0_new"></td>

                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Pend with :</strong></td>
                                                            <td class="pro_pendwith_new">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Attorney :</strong></td>
                                                            <td class="pro_atty_new"></td>

                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Ref Code :</strong></td>
                                                            <td class="pro_refcode_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Ref Source :</strong></td>
                                                            <td class="pro_refsource_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100" style="border-bottom: 1px solid #e7eaec;"><strong>Ref By :</strong></td>
                                                            <td class="pro_refby_new" style="border-bottom: 1px solid #e7eaec;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Association :</strong></td>
                                                            <td class="pro_assciation_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Ref To :</strong></td>
                                                            <td class="pro_refto_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Date Ref :</strong></td>
                                                            <td class="pro_dateref_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Out Ref Stat :</strong></td>
                                                            <td class="pro_outrefstat_new">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Ref Fee :</strong></td>
                                                            <td class="pro_ref_fee_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Amount :</strong></td>
                                                            <td class="pro_amount_new"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" width="100"><strong>Date Paid/f-u //  :</strong></td>
                                                            <td class="date_follow_paid_new"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <button type="button" class="btn btn-primary print_prospect" >Print</button>
                        <button type="button" class="btn btn-primary edit_prospect_select">Edit</button>
                        <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="addcase" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Case</h4>
            </div>
            <div class="modal-body">
                <form id="addNewCase" method="post">
                    <div class="form-group">
                        <label class="col-sm-5 no-left-padding">First Name</label>
                        <div class="col-sm-7 no-padding marg-bot5">
                            <input type="text" name="first_name"  id="first_name" class="form-control" />
                        </div></div>
                    <div class="form-group">
                        <label class="col-sm-5 no-left-padding">Last Name</label>
                        <div class="col-sm-7 no-padding marg-bot5">
                            <input type="text" name="last_name"  id="last_name" class="form-control" />
                        </div></div>
                    <div class="form-group text-center marg-top15">
                        <input type="hidden" name="prospect_id" id="prospect_id" class="form-control" />

                        <input type="submit" value="Proceed" class="btn btn-md btn-primary" data-toggle="modal">
                        <input type="button" value="Cancel" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="addcontact" class="modal fade" data-backdrop="static" data-keyboard="false" style="z-index: 9999 !important;" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Contact</h4>
            </div>
            <div class="modal-body">
                <form id="addRolodexForm" method="post" enctype="multipart/form-data">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#name">Name</a></li>
                            <li class=""><a data-toggle="tab" href="#contact">Telephone/Email</a></li>
                            <li class=""><a data-toggle="tab" href="#details">Details</a></li>
                            <li class=""><a data-toggle="tab" href="#comments">Comments</a></li>
                            <li class=""><a data-toggle="tab" href="#address">Addresses</a></li>
                            <li class=""><a data-toggle="tab" href="#profile">Profile</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="name" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>General Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Salutation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input name="form_submit" type="hidden" value="1">
                                                            <select name="card_salutation" id="card_salutation" class="form-control <?php
                                                            if (isset($system_data[0]->cdsaldd) && $system_data[0]->cdsaldd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" >
                                                                <option value="">Select Salutation</option>
                                                                <?php foreach ($saluatation as $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">First <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="card_first"  name="card_first" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Middle</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_middle"  class="form-control" value=""  autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_last"  class="form-control" value="" autocomplete="off"/>
                                                        </div></div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Suffix</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_suffix" id="card_suffix" class="form-control <?php
                                                            if (isset($system_data[0]->cdsufdd) && $system_data[0]->cdsufdd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>">
                                                                <option value="">Select Suffix</option>
                                                                <?php
                                                                if (isset($suffix)) {
                                                                    foreach ($suffix as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Title</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_title" class="form-control select2Class" >
                                                                <option value="">Select Title</option>
                                                                <?php
                                                                if (isset($title)) {
                                                                    foreach ($title as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Category <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select placeholder="Select Category" name="card_type" id="card_type" class="form-control select2Class" required >
                                                                <?php
                                                                if (isset($category)) {
                                                                    foreach ($category as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Popular</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_popular"  class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Sal for Ltr   <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="This should normally be left empty.  However, you may sometimes decide to fill this field in to override the default salutation in form letters.  For example : To the honorable Judge Smith" id="sfl"></i></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_letsal" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Firm/Corp <span class="asterisk">*</span></label>
                                                        <div class="col-sm-6 no-padding">
                                                            <input type="text" name="card2_firm" class="form-control" value="" autocomplete="off"/></div><div class="col-sm-1 no-padding"><a href="#" class="btn btn-md btn-primary pull-right rolodex-company">A</a>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address Lookup</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input id="autocomplete" placeholder="Enter your address" class="form-control" onFocus="geolocate()" type="text" autocomplete="off">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address 1 <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_address1" id="street_number" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <div class="row">

                                                            <div class="col-lg-12">
                                                                <label class="col-sm-5 no-left-padding">City <span class="asterisk">*</span></label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" id="locality" name="card2_city" class="form-control" value="" autocomplete="off"/>
                                                                </div></div>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Venue</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
<!--                                                        <input type="text" name="card2_venue" class="form-control" />-->
                                                            <select class="form-control cstmdrp cstmdrp-placeholder" name="card2_venue" placeholder="Select Venue">
                                                                <option value="" disabled>Select Venue</option>
                                                                <option value=""></option>
                                                                <?php
                                                                if (isset($Venue)) {
                                                                    foreach ($Venue as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">EAMS Name</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_eamsref" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_address2" id="route" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">State <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="administrative_area_level_1" name="card2_state" class="form-control" value="" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Zip <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" id="postal_code" name="card2_zip" class="form-control" value="" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="contact" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Personal Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Business</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_business" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Home</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_home" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Cell</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_car" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Email</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_email" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Note</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_notes" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Business Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Telephone 1</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone1" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Telephone 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone2" placeholder="(xxx) xxx-xxxx" class="form-control contact"  autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Tax ID</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_tax_id" class="form-control" autocomplete="off"/><!-- onlyNumbers -->
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax2" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="details" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Contact Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">SS No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_social_sec" class="form-control social_sec"  placeholder="234-56-7898" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Date of Birth</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_birth_date" pattern="\d{1,2}/\d{1,2}/\d{4}"  class="form-control datepickerClass date-field weekendsalert" data-mask="99/99/9999" placeholder="mm/dd/YY ex: 03/25/1995" id="datepicker" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">License No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_licenseno" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Specialty</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">

                                                            <select name="card_specialty" class="form-control select2Class">
                                                                <option value="">Select Specialty</option>
                                                                <?php
                                                                if (isset($specialty)) {
                                                                    foreach ($specialty as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Maidan Name</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_mothermaid" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Card No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_cardcode" readonly class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Firm No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_firmcode" readonly class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Interpreter Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Interpreter</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_interpret" class="form-control select2Class">
                                                                <option value="">Select Interpreter</option>
                                                                <option value="Y">Yes</option>
                                                                <option value="N">No</option>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group" id="data_1">
                                                        <label class="col-sm-5 no-padding">Language</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_language" class="chosen-language form-control">
                                                                <?php foreach ($languageList as $key => $val) { ?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Original</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime1" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label>Last</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime2" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label>Original2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime3" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label>Last2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime4" readonly data-date-format="mm/dd/yyyy - HH:ii p" >
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="comments" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card2_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="address" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Alternate Name and Addresses</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address1</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing1" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address2</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing2" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address3</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing3" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address4</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing4" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="profile" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Occupation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_occupation" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Employer</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_employer" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Profile Picture</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="file" name="profilepic" id="profilepic" class="form-control" accept="image/*" />
                                                            <input type="hidden" name="hiddenimg" id="hiddenimg" value="" >
                                                            <span id="imgerror" style="color:red"></span>
                                                        </div>
                                                        <div id="profilepicDisp" style="display:none">
                                                            <div class="form-group">
                                                                <img id="dispProfile" height="50" width="50">
                                                            </div>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <input type="hidden" id="new_case" name="new_case" />
                        <input type="hidden" id="prospect_id" name="prospect_id"/>
                        <button type="button" id="addContact" class="btn btn-md btn-primary">Save</button>
                        <button type="button" id="cancelContact" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="firmcompanylist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md firmcompanylist">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Firm Company List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="form-group">
                    <form action="#" role="form" method="post" id="companylistForm" name="companylistForm">
                        <label>Enter Firm or Company Name</label>
                        <input type="text" class="form-control" name="rolodex_search_text_name" id="rolodex_search_text_name"/>
                        <input type="hidden" class="form-control" name="rolodex_search_parties_name" id="rolodex_search_parties_name" value="rolodex-company" />
                    </form>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="rolodexsearchResult_data">
                                    <thead>
                                        <tr>
                                            <th>Firm/Company</th>
                                            <th>City</th>
                                            <th>Address</th>
                                            <th>Address2</th>
                                            <th>Phone</th>
                                            <th>Name</th>
                                            <th>State</th>
                                            <th>Zip</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="viewSearchedPartieDetails" >Attach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>


<div id="printoption" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select A Report To Print</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group"><select class="form-control select2Class" name="print_option" id="print_option">
                            <option value="1">General Report#1(line 1)</option>
                            <option value="2">General Report#2(line 1)</option>
                            <option value="3">General Report#3(lines 3)</option>
                            <option value="4">General Report#4(lines 4)</option>
                            <option value="4b">General Report#4b(lines 4)</option>
                            <option value="5">General Report#5(lines 5)</option>
                            <option value="6">General Report#(Landscape)</option>
                            <option value="7">Mailing Labels(One Column/1*10)</option>
                            <option value="8">Mailing Labels(Two Column/1*10)</option>
                            <option value="9">Mailing Labels(Three Column/3*10)</option>
                            <option value="10">Mailing Labels(Two Column/2*7)</option>
                            <option value="11">Email To Clipboard</option>

                        </select>
                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="button" class="btn btn-primary print_prospect_data" >Print</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="Task-list" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close marg-left10 marg-top5" data-dismiss="modal">&times;</button>
                <h4 class="modal-title pull-left marg-top5"><span id="record_count"> </span>Tasks For PNC - <?php echo isset($Prospect_data->salutation) ? $Prospect_data->salutation : '' ?> <?php echo isset($Prospect_data->first) ? $Prospect_data->first : ''; ?> <?php echo isset($Prospect_data->last) ? $Prospect_data->last : ''; ?>   (<span id="count_tasks">0</span> tasks)</h4>

            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="form-group row">
                        <div class="col-sm-2 col-xs-6"><label>Task Status</label></div>
                        <div class="col-sm-2 col-xs-6"><select class="form-control" id="gettaskStatus" name="completed">
                                <option value="">All</option>
                                <option value="pending">Pending</option>
                                <option value="completed">Completed</option>
                            </select></div>
                        <div class="col-sm-8 col-xs-12 pull-right">
                            <a class="btn pull-right btn-primary btn-sm marg-left5" href="#addtask1" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Task</a>
                            <a class="btn pull-right btn-primary btn-sm marg-left5" href="#printtask" data-backdrop="static" data-keyboard="false" data-toggle="modal">Print</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content" style="overflow:auto;">
                                <table class="table table-bordered task-dataTable1">
                                    <thead>
                                        <tr>
                                            <th id="th-date" width="10%">Date</th>
                                            <th id="th-event text-center" width="30%">Reminder</th>
                                            <th id="th-from" width="5%">From</th>
                                            <th id="th-to" width="5%">To</th>
                                            <th id="th-finishby" width="10%">Finish By</th>
                                            <th id="th-type" width="5%">Type</th>
                                            <th id="th-priority" width="5%">Priority</th>
                                            <th id="th-event" width="30%" class="caseact_event">Event</th>
                                            <th id="th-phase" width="5%">Phase</th>
                                            <th id="th-completed" width="10%">Completed</th>
                                            <th id="th-action" width="10%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="addtask1" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body">
                <form id="addTaskForm1" name="addTaskForm1" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <label class="col-sm-3 no-padding">From</label>
                                        <div class="col-sm-9 no-padding">
                                            <select class="form-control select2Class" name="whofrom" id="whofrom">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 no-padding">To</label>
                                        <div class="col-sm-10 no-padding">
                                            <select class="form-control select2Class" name="whoto3" id="whoto3">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-5 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-padding">Finish By <span class="asterisk">*</span></label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" id="taskdatepicker2"  />
                                            <label class="error"></label>
                                        </div></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task <span class="asterisk">*</span></label>
                                        <textarea class="form-control" name="event" style="height: 150px;resize: none;"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Color</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="color" id="color">
                                                <?php
                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                foreach ($jsoncolor_codes as $k => $color) {
                                                    ?>
                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Task Type</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="typetask">
                                                <option value="">Select Task Type</option>
                                                <option value=""></option>
                                                <option value="follow up">Follow Up</option>
                                                <option value="statute">Statute</option>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Priority</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="priority" id="priority">
                                                <option value="L">Low</option>
                                                <option value="M">Medium</option>
                                                <option value="H">High</option>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Phase</label>
                                        <div class="col-sm-9 no-padding marg-bot5">
                                            <select class="form-control select2Class" name="phase">
                                                <option value="">Select Phase</option>
                                                <option value=""></option>
                                                <option value="begining">Beginning</option>
                                                <option value="middle">Middle</option>
                                                <option value="final">Final</option>
                                            </select>
                                        </div></div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <div class="form-group">

                                            <label class="container-checkbox pull-left">Reminder

                                                <input type="checkbox" class="treminder" name="reminder1" onchange="showhidedt1();">
                                                <span class="checkmark"></span>
                                                <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="You will get notified on your screen prior to 10 minutes of your scheduled task. To enable calender reminder notification in your browser to receive all notifications, allow notification in site settings." id="taskreminder"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="row form-group">
                                            <label class="col-sm-3">Date</label>
                                            <div class="col-sm-9 no-padding">
                                                <input type="text" name="rdate" data-mask="99/99/9999" class="form-control rdate1" id="rdate1" readonly>
                                            </div></div>

                                    </div>
                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="row form-group">
                                            <label class="col-sm-3">Time</label>
                                            <div class="col-sm-9 no-padding">
                                                <input type="text" name="rtime" class="form-control rtime" class="form-control">
                                            </div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <!-- <button type="button" class="btn btn-md btn-primary pull-left">Save to Group</button>-->
                                        <button type="button" class="btn btn-md btn-primary" id="addTask2" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
<!--                                        <span class="marg-top5 pull-right"><label>Email&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="notify" ></label></span>-->
                                        <label class="container-checkbox pull-right marg-top5">&nbsp;Email
                                            <input type="checkbox" class="" name="notify" >
                                            <span class="checkmark"  style="padding-right: 5px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="printtask" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Task</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <form id="taskPrint" name="taskPrint">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12" id="print_datepicker">
                                    <label>Days to Print</label>
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_printData" class="input-small form-control" name="start_printData" placeholder="From Date" readonly>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_printData" class="input-small form-control" name="end_printData" placeholder="To Date" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Type of Report</label>
                                        <select class="form-control" name="print_type">
                                            <?php
                                            $repType = json_decode(print_type);
                                            foreach ($repType as $key => $val) {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <span><label> <input type="checkbox" name="namecheck" class="i-checks">Include the name before each task</label></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center marg-top15">
                                        <button type="button" id="btn-taskPrint" class="btn btn-md btn-primary">Proceed</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="TaskCompleted" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Task Completed</h4>
            </div>
            <div class="modal-body">
                <form id="edit_case_details" method="post">
                    <div class="panel-body">
                        <div class="float-e-margins">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox-content">
                                        <div class="form-group">
                                            <textarea class="form-control" name="case_event" id="case_event" style="height: 100px;width: 100%;"></textarea>
                                            <input type='hidden' id='caseTaskId' name='caseTaskId'>
                                            <input type='hidden' id='caseno' name='caseno'>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter any information to add to the  task to be posted to case activity</label>
                                            <textarea class="form-control" name="case_extra_event" id="case_extra_event" style="height: 100px;width: 100%;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class='col-lg-6'>
                    <div class="form-btn text-center">
                        <button type="submit" id="saveCompletedTask" class="btn btn-primary btn-md">Done</button>
                        <button type="button" class="btn btn-primary btn-md btn-danger" data-dismiss="modal">Cancel</button>
                    </div></div>
                <div class='col-lg-6'>
                    <select id="case_category" name="case_category" class="form-control select2Class">
                        <?php
                        $jsonCategory = json_decode(category);
                        foreach ($jsonCategory as $key => $option) {
                            echo '<option value=' . $key . '>' . $option . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="duplicateData" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Possible Duplicate</h4>
            </div>
            <form method="post" name="parties_case_specific" id="parties_case_specific">
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <table id="duplicateEntries" class="table table-bordered table-hover table-striped dataTables-example" style="width: 100%">
                                        <thead>
                                        <th>Case No</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Date Entered</th>
                                        <th>SS No</th>
                                        <th>Card Type</th>
                                        <th>Caption</th>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input id="last_name" name="last_name" type="hidden" class="last_name" value="">
                    <input id="first_name" name="first_name" type="hidden" class="first_name"  value="">
                    <input id="prospect_id" name="prospect_id" type="hidden" class="prospect_id"  value="">
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <div class="">

                            <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $this->uri->segment(3); ?>">
                            <input id="duplicateCardCode" name="duplicateCardCode" type="hidden" value="">
                            <button id="add_name_case" type="submit" class="btn btn-primary">Add Name & Case</button>
                            <button id="attach_name" type="button" class="btn btn-primary" disabled>Attach Name</button>
                            <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                            <button id="edit_new_case" type="button" class="btn btn-primary pull-right" disabled>Edit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="partiesNameChangeWarning" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <span>If you make changes in the rolodex, it may affect several cases.</span>
                                <ul class="no-left-padding marg-top15">
                                    <li>
                                        Click Yes <span class="marg-left35">To make changes for this case only.</span>
                                    </li>
                                    <li>
                                        Click No <span class="marg-left35">To make changes in the rolodex which may affect several cases.</span>
                                    </li>
                                    <li>
                                        Click Cancel <span class="marg-left15">To cancel and not make changes at all.</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <?php $caseNo = $this->uri->segment(3); ?>
                        <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo; ?>">
                        <input id="caseCardCode" type="hidden">
                        <button id="change_name_warning" type="button" class="btn btn-primary">Yes</button>
                        <button id="change_name_warning_no" type="button" class="btn btn-primary">No</button>
                        <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="copyemail">
    <textarea id="copytoemail" STYLE="display:none;">
    </textarea>
</div>
<script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>
<script type="text/javascript">
                                                    var uri_seg = "<?php echo (isset($url_seg)) ? $url_seg : ''; ?>";
                                                    var selectedArray = [];
                                                    var rolodexSearchDatatable = "";
                                                    var rolodexSearch = "";
                                                    var categoryeventlist = "";
                                                    var casepartylist_data = "";
                                                    var venue_table_data = "";
                                                    var caseactTable = "";
                                                    var swalFunction = "";
                                                    var CaseHID = $("#hdnCaseId").val();
                                                    var ProspectListDatatable;
                                                    var searchBy1 = {'prosno':<?php echo isset($record[0]->proskey) ? $record[0]->proskey : 'null'; ?>};


                                                    $(document.body).on('ifToggled', 'input#field_ct1', function () {
                                                        if ($(this).is(':checked'))
                                                        {
                                                            $('#field_doi2').removeAttr('readonly');
                                                            $('#field_adj1c').mask('CT - 99/99/9999 ', {placeholder: "CT - mm/dd/yyyy"});
                                                        } else
                                                        {
                                                            $("#field_doi2").prop('readonly', true);
                                                            $('#field_adj1c').val('');
                                                        }
                                                    });

                                                    $(document.body).on('change', 'input#field_doi', function () {
                                                        if ($('#field_ct1').is(':checked'))
                                                        {
                                                            $('#field_doi2').removeAttr('readonly');
                                                            $('#field_adj1c').mask('CT - 99/99/9999 ', {placeholder: "CT - mm/dd/yyyy"});
                                                        } else
                                                        {
                                                            var value = $('#field_doi').val();
                                                            $('#field_adj1c').val(value);
                                                        }
                                                    });

                                                    /*            $(function() {
                                                     $( "#relatedcasenumber,.caseno" ).autocomplete({
                                                     source: '<?php echo base_url(); ?>cases/autosearch/'+CaseHID
                                                     });
                                                     });
                                                     */

                                                    $(document).ready(function () {
                                                        $('#taskreminder').tooltip();
                                                        $('#pnctaskreminder').tooltip();

                                                        ProspectListDatatable = $('#ProspectListDatatable').DataTable({
                                                            scrollX: true
                                                        });
                                                        $('#ListProspects').on('shown.bs.modal', function (e) {
                                                            ProspectListDatatable.columns.adjust();
                                                        });
                                                        $('input.numberinput').bind('keypress', function (e) {
                                                            return !(e.which != 8 && e.which != 0 &&
                                                                    (e.which < 48 || e.which > 57) && e.which != 46);
                                                        });

                                                        var calYearRange = $('#calYearRange').val();

                                                        venue_table_data = $('#venue_table').DataTable();
                                                        categoryeventlist = $('#caseEventlist').DataTable();
                                                        casepartylist_data = $('#casePartylist').DataTable();
                                                        rolodaxdataTable = $('.dataTables-rolodex').DataTable();
                                                        $('#print_datepicker').hide();

                                                        $('.i-checks').iCheck({
                                                            checkboxClass: 'icheckbox_square-green',
                                                            radioClass: 'iradio_square-green'
                                                        });

                                                        $('.datepicker').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });

                                                        $('#datepicker').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });

                                                        $('.dob').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: '+0d'
                                                        });

                                                        $("#taskdatepicker").datepicker({
                                                            dateformat: "mm/dd/yyyy",
                                                            showOn: "button",
                                                            minDate: 0,
                                                            buttonImage: false,
                                                            buttonText: '<i class="fa fa-calendar" aria-hidden="true"></i>',
                                                            buttonImageOnly: false
                                                        });

                                                        $('#taskdatepicker2').datepicker({
                                                            dateformat: "mm/dd/yyyy",
                                                            showOn: "button",
                                                            minDate: 0,
                                                            buttonImage: false,
                                                            buttonText: '<i class="fa fa-calendar" aria-hidden="true"></i>',
                                                            buttonImageOnly: false
                                                        });
                                                        $('#taskdatepicker').on('change', function () {
                                                            var today = new Date();
                                                            var ad = $(this).val();
                                                            if (isValidDate(ad))
                                                            {
                                                                $('#taskdatepicker').val(moment(ad).format('MM/DD/YYYY'));
                                                                /*$('.weekendsalert').trigger( "click" );*/
                                                            } else
                                                            {
                                                                var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                                                                $('#taskdatepicker').val(moment(new_date).format('MM/DD/YYYY'));
                                                                $('.weekendsalert').trigger("change");
                                                            }

                                                        });

                                                        $('#taskdatepicker2').on('change', function () {
                                                            var today = new Date();
                                                            var ad = $(this).val();
                                                            if (isValidDate(ad))
                                                            {
                                                                $('#taskdatepicker2').val(moment(ad).format('MM/DD/YYYY'));
                                                                /*$('.weekendsalert').trigger( "click" );*/
                                                            } else
                                                            {
                                                                var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                                                                $('#taskdatepicker2').val(moment(new_date).format('MM/DD/YYYY'));
                                                                $('.weekendsalert').trigger("change");
                                                            }
                                                        });

                                                        $('#caldate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });

                                                        $('.rdate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });
                                                        $('.rdate1').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });
                                                        var d = new Date();
                                                        var hours = d.getHours();
                                                        var ampm = hours >= 12 ? 'PM' : 'AM';

                                                        var currenttime = "<?php echo date('h:i a') ?>";
                                                        $('#caltime').val(currenttime);

                                                        var input1 = $('#caltime');
                                                        input1.mdtimepicker({
                                                            autoclose: true
                                                        });

                                                        $('.rtime').mdtimepicker({
                                                            autoclose: true
                                                        });

                                                        $('#oletime').mdtimepicker({
                                                            autoclose: true,
                                                            'default': currenttime
                                                        });
                                                        $('#tickledate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });

                                                        $('#tickletime').mdtimepicker({
                                                            autoclose: true
                                                        });

                                                        $('.preinjurydate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });

                                                        $('.injurydate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });

                                                        $('#field_adj1a').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });
                                                        $('#start_printData').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true
                                                        }).on('changeDate', function (ev) {
                                                            $('#end_printData').datetimepicker('setStartDate', ev.date);
                                                        });

                                                        $('#end_printData').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true
                                                        }).on('changeDate', function (ev) {
                                                            $('#start_printData').datetimepicker('setEndDate', ev.date);
                                                        });

                                                        $('select[name=print_type]').on('change', function () {
                                                            if (this.value == 4 || this.value == 5) {
                                                                $('#print_datepicker').show();

                                                            } else {
                                                                $('#start_printData').val('').datetimepicker("update");
                                                                $('#end_printData').val('').datetimepicker("update");
                                                            }
                                                        });

                                                        $('#casedate_followup').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });

                                                        if (uri_seg != 'rolodex' && uri_seg != 'dashboard' && uri_seg != 'email' && uri_seg != 'case') {
                                                            taskTable = $('.task-dataTable').DataTable({
                                                                "processing": true,
                                                                "serverSide": true,
                                                                scrollX: true,
                                                                "bFilter": true,
                                                                "order": [[0, "DESC"]],
                                                                "autoWidth": false,
                                                                "ajax": {
                                                                    url: "<?php echo base_url(); ?>tasks/getTasksData",
                                                                    type: "POST",
                                                                    "data": function (ae) {
                                                                        var status = $('ul#task-navigation').find('li.active').attr('id');
                                                                        if ($('#task_reminder').is(':checked') == true)
                                                                        {
                                                                            var reminder = 1;
                                                                        } else
                                                                        {
                                                                            var reminder = '';
                                                                        }
                                                                        var searchBy = {'caseno': $('#caseNo').val()};
                                                                        searchBy['taskBy'] = status;
                                                                        searchBy['reminder'] = reminder;

                                                                        $.extend(ae, searchBy);
                                                                    }
                                                                },
                                                                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                                                    $(nRow).attr('id', aData[9]);
                                                                    if (aData[aData.length - 2] != '') {
                                                                        var color = aData[aData.length - 2].split('-');
                                                                        if (color[0] != '') {
                                                                            $(nRow).css({'background-color': color[0], 'color': color[1]});
                                                                        } else {
                                                                            $(nRow).css({'background-color': '#ffffff', 'color': '#000000'});
                                                                        }
                                                                    }
                                                                },
                                                                "columnDefs": [{
                                                                        "render": function (data, type, row) {
                                                                            if (data != null) {
                                                                                var button = '';
                                                                                var button1 = '';
                                                                                if (row[8] == '') {
                                                                                    if (row[1] === "<?php echo isset($username) ? $username : $this->session->userdata('user_data')['username']; ?>") {
                                                                                        button1 = '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + data + ')"><i class="fa fa-trash"></i></button>';
                                                                                    }
                                                                                    button = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + data + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;' + button1;
                                                                                    button += '&nbsp;&nbsp;<button class="btn btn-primary btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + data + ')"><i class="fa fa-check"></i></button>';
                                                                                    if (row[11] != 0)
                                                                                    {
                                                                                        button += '&nbsp;&nbsp;<a class="btn btn-primary btn-circle-action btn-circle" title="PullCase" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + row[11] + '"><i class="fa fa-folder-open"></i></a>';
                                                                                    }
                                                                                }
                                                                            }
                                                                            return button;
                                                                        },
                                                                        "targets": 9,
                                                                        "bSortable": false,
                                                                        "bSearchable": false
                                                                    },
                                                                    {
                                                                        targets: 6,
                                                                        className: 'textwrap-line'
                                                                    }]
                                                            });
                                                        }
                                                        if (uri_seg != 'rolodex' && uri_seg != 'dashboard' && uri_seg != 'email' && uri_seg != 'case') {


                                                            taskTable1 = $('.task-dataTable1').DataTable({
                                                                "processing": true,
                                                                "serverSide": true,
                                                                "bFilter": true,
                                                                scrollX: true,
                                                                "order": [[3, "asc"]],
                                                                "autoWidth": false,
                                                                "ajax": {
                                                                    url: "<?php echo base_url(); ?>tasks/getTasksData",
                                                                    type: "POST",
                                                                    "data": function (ae) {

                                                                        searchBy = searchBy1;
                                                                        if ($('#task_reminder').is(':checked') == true)
                                                                        {
                                                                            var reminder = 1;
                                                                        } else
                                                                        {
                                                                            var reminder = '';
                                                                        }
                                                                        searchBy['reminder'] = reminder;
                                                                        var status = $('ul#task-navigation').find('li.active').attr('id');
                                                                        searchBy['taskBy'] = status;
                                                                        $.extend(ae, searchBy);
                                                                    },
                                                                    complete: function (data) {

                                                                        var array = $.map(data, function (value, index) {
                                                                            return [value];
                                                                        });

                                                                        $('#count_tasks').text(array[19].recordsTotal);
                                                                    }
                                                                },
                                                                fnRowCallback: function (f, e, d, c) {

                                                                    $(f).attr("id", e[9]);
                                                                    $(f).attr("data-id", e[9]);
                                                                    $(f).attr("data-event", e[6]);
                                                                    $(f).attr("data-caseno", e[11]);
                                                                    if (e[e.length - 2] != "") {
                                                                        var b = e[e.length - 2].split("-");
                                                                        if (b[0] != "") {
                                                                            $(f).css({
                                                                                "background-color": b[0],
                                                                                color: b[1]
                                                                            });
                                                                        } else {
                                                                            $(f).css({
                                                                                "background-color": "#ffffff",
                                                                                color: "#000000"
                                                                            });
                                                                        }
                                                                    }
                                                                },
                                                                columnDefs: [{
                                                                        render: function (e, c, f) {
                                                                            if (e != null) {
                                                                                var b = "";
                                                                                var d = "";
                                                                                if (f[9] == "") {
                                                                                    if (f[3] === "<?php echo isset($username) ? $username : $this->session->userdata('user_data')['username']; ?>") {
                                                                                        d = '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + e + ')"><i class="fa fa-trash"></i></button>';
                                                                                    }
                                                                                    b = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + e + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;' + d;
                                                                                    b += '&nbsp;&nbsp;<button class="btn btn-info btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + e + ')"><i class="fa fa-check"></i></button>';
                                                                                    if (f[12] != 0) {
                                                                                        b += '&nbsp;&nbsp;<a class="btn btn-info btn-circle-action btn-circle" title="PullCase" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + f[12] + '"><i class="fa fa-folder-open"></i></a>';
                                                                                    }
                                                                                }
                                                                            }
                                                                            return b;
                                                                        },
                                                                        targets: 10,
                                                                        bSortable: false,
                                                                        bSearchable: false
                                                                    }, {
                                                                        targets: 7,
                                                                        className: "textwrap-line"
                                                                    }]
                                                            });
                                                        }



                                                        $(document).on('click', '.task_list', function () {
                                                            var proskey = $(this).attr('data-proskey_id');
                                                            searchBy1['prosno'] = proskey;
                                                            taskTable1.ajax.reload();
                                                            $('#Task-list').modal('show');

                                                        });
                                                        $('#addtask.modal').on('hidden.bs.modal', function () {
                                                            $('#addtask h4.modal-title').text('Add a Task');

                                                            $('#addTaskForm1')[0].reset();
                                                            $('#addTaskForm')[0].reset();

                                                            $("select[name=typetask]").select2('val', 'Task Type');
                                                            $("select[name=typetask]").val('').trigger("change");

                                                            $("select[name=phase]").select2('val', 'Select Phase');
                                                            $("select[name=phase]").val('').trigger("change");

                                                            $("select[name=phase]").val('').trigger("change");

                                                            $(".select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");

                                                            $(".select2Class#color").val($(".select2Class#color option:eq(0)").val()).trigger("change");

                                                            $(".select2Class#priority").val($(".select2Class#priority option:eq(0)").val()).trigger("change");
                                                            $('#taskdatepicker').val("");
                                                            $("select[name=event]").val('');
                                                            $("select[name=rdate]").val('');

                                                            $('#addtask').modal('hide');
                                                            $('#addtask .modal-title').html('Add a Task');


                                                            var taskVal = setTaskValidation();

                                                            $('input[name=taskid]').val(0);
                                                            $('#addtask a[href="#name"]').tab('show');
                                                        });


                                                        setTaskValidation();

                                                        $('.task-dataTable tbody').on('click', 'tr', function () {

                                                            if ($(this).hasClass('table-selected')) {
                                                                $(this).removeClass('table-selected');
                                                                selectedArray.splice($.inArray(this.id, selectedArray), 1);
                                                            } else {
                                                                selectedArray.push(this.id);
                                                            }
                                                        });
                                                        setCalendarValidation();

                                                        $.fn.modal.Constructor.prototype.enforceFocus = function () {};

                                                        $('.contact').mask("(000) 000-0000");
                                                        $('.social_sec').mask("000-00-0000");

                                                        $('#addcontact.modal').on('show.bs.modal', function () {
                                                            document.getElementById('imgerror').innerHTML = '';
                                                            document.getElementById('hiddenimg').value = '';

                                                            $(".chosen-language").chosen();
                                                        });

                                                        $('#addRolodexForm a[data-toggle="tab"]').on('click', function (e) {

                                                            setValidation();
                                                            e.preventDefault();
                                                        });

                                                        $('#addcontact.modal').on('hidden.bs.modal', function () {
                                                            $('#addcontact.modal-title').text('Add New Contact');
                                                            $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                                                                this.value = '';
                                                            });
                                                            document.getElementById('profilepicDisp').style.display = 'none';
                                                            document.getElementById('dispProfile').src = '';
                                                            $('#addcontact a[href="#name"]').tab('show');
                                                            var addContactValidator = setValidation();
                                                            addContactValidator.resetForm();
                                                        });

                                                        $('#addContact').on('click', function () {
                                                            var cardcodeVal = $('input[name="card_cardcode"]').val();

                                                            if (cardcodeVal == '') {
                                                                if (!$('#addRolodexForm').valid()) {
                                                                    return false;
                                                                }
                                                            }

                                                            if (!$('#addRolodexForm').valid()) {
                                                                return false;
                                                            }

                                                            var url = '<?php echo base_url() . "rolodex/addCardData" ?>';
                                                            if ($('input[name="card_cardcode"]').val() != '') {
                                                                url = '<?php echo base_url() . "rolodex/editCardData" ?>';
                                                            }

                                                            $.blockUI();
                                                            $.ajax({
                                                                url: url,
                                                                method: "POST",
                                                                data: $('#addRolodexForm').serialize(),
                                                                success: function (result) {
                                                                    var data = $.parseJSON(result);
                                                                    var editCardcode = data.editcardid;
                                                                    if ($('#profilepic').val() != '')
                                                                    {
                                                                        var old_img = document.getElementById('hiddenimg').value;
                                                                        var images = '';
                                                                        var file_data = $('#profilepic').prop('files')[0];
                                                                        var form_data = new FormData();
                                                                        form_data.append('file', file_data);
                                                                        $.ajax({
                                                                            url: '<?php echo base_url(); ?>rolodex/updateprofilepic/' + editCardcode + '/' + old_img,
                                                                            dataType: 'text',
                                                                            cache: false,
                                                                            contentType: false,
                                                                            processData: false,
                                                                            data: form_data,
                                                                            type: 'post',
                                                                            success: function (response) {
                                                                                images = response;
                                                                                var plink = '<?php echo base_url() ?>assets/rolodexprofileimages/' + images;
                                                                                /* setTimeout(function() { */
                                                                                if (images != null) {
                                                                                    $.unblockUI();
                                                                                    $('#addRolodexForm')[0].reset();
                                                                                    $('#addcontact').modal('hide');
                                                                                    $('#addcontact .modal-title').html('Add New Contact');
                                                                                    var url = "<?php echo base_url() ?>cases/case_details/" + data.caseid;
                                                                                    setTimeout(function ()
                                                                                    {
                                                                                        window.location.href = url;
                                                                                    }, 5000);
                                                                                }
                                                                                /* }, 500);*/
                                                                            }
                                                                        });
                                                                    } else {
                                                                        if (data.status == '1') {
                                                                            $.unblockUI();
                                                                            $('#addRolodexForm')[0].reset();
                                                                            $('#addcontact').modal('hide');
                                                                            $('#addcontact .modal-title').html('Add New Contact');
                                                                            if ((data.caseid != 0 || data.caseid != 'undefined')) {
                                                                                var caseid = $("#hdnCaseId").val();
                                                                                if (data.caseid != 0 && $("#new_case").val() == 1) {
                                                                                    var url = "<?php echo base_url() ?>cases/case_details/" + data.caseid;
                                                                                    setTimeout(function ()
                                                                                    {
                                                                                        window.location.href = url;
                                                                                    }, 10000);
                                                                                }
                                                                            }
                                                                            rolodaxdataTable.draw(false);
                                                                        } else {
                                                                            swal("Oops...", data.message, "error");
                                                                        }
                                                                    }
                                                                    /* $.unblockUI(); */
                                                                }
                                                            });

                                                            return false;
                                                        });

                                                        $('#getPriority').on('change', function () {
                                                            var selectedValue = $(this).val();
                                                            taskTable.columns(5).search(JSON.stringify({'priority': selectedValue})).draw();
                                                        });

                                                        $('#gettaskStatus').on('change', function () {
                                                            var taskStatus = $(this).val();
                                                            taskTable.columns(8).search(JSON.stringify({'task-status': taskStatus})).draw();
                                                        });



                                                        $('#btn-taskPrint').on('click', function () {


                                                            $.ajax({
                                                                url: "<?php echo base_url() . 'tasks/printTask' ?>",
                                                                method: "POST",
                                                                data: $('form#taskPrint').serialize() + '&searchBy=' + JSON.stringify(searchBy) + '&selectedData=' + selectedArray,
                                                                success: function (result) {

                                                                    $(result).printThis({
                                                                        debug: false,
                                                                        header: "<h2><center><?php echo APPLICATION_NAME; ?> | Tasks </center></h2>"
                                                                    });
                                                                }
                                                            });

                                                        });


                                                        var validator = $('#addTaskForm1').validate({
                                                            ignore: [],
                                                            rules: {
                                                                whofrom: {required: true},
                                                                whoto3: {required: true},
                                                                datereq: {required: true},
                                                                event: {required: true, noBlankSpace: true}
                                                            }
                                                        });
                                                        $('#add_new_case_activity').validate({
                                                            ignore: [],
                                                            rules: {
                                                                activity_event: {
                                                                    required: true
                                                                }
                                                            },
                                                            messages: {
                                                                activity_event: {required: "Event Description is Required"}
                                                            },
                                                            invalidHandler: function (form, validator) {
                                                                var element = validator.invalidElements().get(0);
                                                                var tab = $(element).closest('.tab-pane').attr('id');
                                                                $('#addcaseact a[href="#' + tab + '"]').tab('show');
                                                            }
                                                        });
                                                        $.validator.addMethod("phoneUSA", function (phone_number, element) {
                                                            phone_number = phone_number.replace(/\s+/g, "");
                                                            return this.optional(element) || phone_number.length > 9 &&
                                                                    phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
                                                        }, "Please specify a valid phone number");
                                                        $("#prospectForm").validate({
                                                            ignore: [],
                                                            rules: {
                                                                pro_first: {
                                                                    required: true
                                                                },
                                                                pro_last: {
                                                                    required: true
                                                                },
                                                                pro_type: {
                                                                    required: true
                                                                },
                                                                pro_business: {phoneUSA: true},
                                                                pro_home: {phoneUSA: true},
                                                                pro_cell: {phoneUSA: true},
                                                                pro_social_sec: {ssId: true},
                                                                pro_state: {stateUS: true},
                                                                pro_zip: {zipcodeUS: true},
                                                                pro_caseno: {
                                                                    remote: {
                                                                        url: '<?php echo base_url(); ?>prospects/CheckCaseno',
                                                                        type: "post",
                                                                        data: {
                                                                            pro_caseno: function () {
                                                                                return $('#prospectForm :input[name="pro_caseno"]').val();
                                                                            }
                                                                        }
                                                                    },
                                                                    digits: true
                                                                }
                                                            },
                                                            messages: {
                                                                pro_first: {
                                                                    required: "Please provide first name"
                                                                },
                                                                pro_last: {
                                                                    required: "Please provide last name"
                                                                },
                                                                pro_type: {
                                                                    required: "Please select category"
                                                                },
                                                                pro_caseno: {
                                                                    remote: "This caseno is not exist"
                                                                },
                                                                pro_state: {stateUS: "Invalid USA state abbreviation"}
                                                            },
                                                            submitHandler: function (form) {
                                                                return false;
                                                            },
                                                            invalidHandler: function (form, validator) {
                                                                var element = validator.invalidElements().get(0);
                                                                var tab = $(element).closest('.tab-pane').attr('id');

                                                                $('#proceedprospects a[href="#' + tab + '"]').tab('show');
                                                            },
                                                            errorPlacement: function (error, element) {
                                                                if (element.attr("name") == "pro_type") {
                                                                    error.insertAfter(element.next());
                                                                } else {
                                                                    error.insertAfter(element);
                                                                }
                                                            }

                                                        });
                                                        $('.dtpickDisDayofWeek').datetimepicker({
                                                            daysOfWeekDisabled: [0],
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });

                                                        $('.datepickerClass').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });
                                                        $('#casedate_closed').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });
                                                        $('#add_case_time').mdtimepicker({
                                                            autoclose: true,
                                                            twelvehour: true
                                                        });

                                                        $('#activity_entry_type').change(function () {
                                                            var data_key = $('#activity_entry_type option:selected').data('key');
                                                            $(".billingtab").addClass("hidden");
                                                            $("#" + data_key).removeClass("hidden");
                                                        });

                                                        var data_key = $('#activity_entry_type option:selected').data('key');
                                                        $(".billingtab").addClass("hidden");
                                                        $("#" + data_key).removeClass("hidden");

                                                        rolodexSearchDatatable = $('#rolodexPartiesResult').DataTable({
                                                            "lengthMenu": [5, 10, 25, 50, 100]
                                                        });
                                                        rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                                                            "scrollX": true,
                                                            "lengthMenu": [5, 10, 25, 50, 100]
                                                        });


                                                        duplicateDatatable = $('#duplicateEntries').DataTable();

                                                        $("#rolodex_search_parties_form").validate({
                                                            rules: {
                                                                rolodex_search_text: {
                                                                    required: true
                                                                },
                                                                rolodex_search_parties: {
                                                                    required: true
                                                                }
                                                            },
                                                            messages: {
                                                                rolodex_search_text: {
                                                                    required: "Please provide search text"
                                                                },
                                                                rolodex_search_parties: {
                                                                    required: "Please provide search by"
                                                                }
                                                            },
                                                            submitHandler: function (form, event) {
                                                                event.preventDefault();

                                                                var alldata = $("form#rolodex_search_parties_form").serialize();
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/searchForRolodex',
                                                                    method: 'POST',
                                                                    data: alldata,
                                                                    beforeSend: function (xhr) {
                                                                        $.blockUI();
                                                                    },
                                                                    complete: function (jqXHR, textStatus) {
                                                                        $.unblockUI();
                                                                    },
                                                                    success: function (data) {
                                                                        var text = $("#rolodex_search_text").val();
                                                                        var substring = text.substr(0, 1);
                                                                        var radioVal = $("input[name='rolodex_search_parties']:checked").val();
                                                                        if (substring == "<") {

                                                                            var res = $.parseJSON(data);
                                                                            $("#searched_party_fullname").val(res[0].fullname);
                                                                            $("#searched_party_firmname").val(res[0].firm);
                                                                            $('#searched_party_type').html(res[0].type);
                                                                            $('#searched_party_cardnumber').html(res[0].cardcode);
                                                                            $(".addSearchedParties").trigger("click");

                                                                        }

                                                                        rolodexSearchDatatable.clear();

                                                                        var obj = $.parseJSON(data);
                                                                        $.each(obj, function (index, item) {
                                                                            var html = '';
                                                                            html += "<tr class='viewPartiesDetail' data-cardcode=" + item.cardcode + "><td>" + item.name + "</td>";
                                                                            html += "<td>" + item.firm + "</td>";
                                                                            html += "</tr>";
                                                                            rolodexSearchDatatable.row.add($(html));
                                                                        });
                                                                        rolodexSearchDatatable.draw();
                                                                    }
                                                                });

                                                            }
                                                        });
                                                        $("#companylistForm").validate({
                                                            rules: {
                                                                rolodex_search_text_name: {
                                                                    required: true,
                                                                    noBlankSpace: true
                                                                }
                                                            },
                                                            messages: {
                                                                rolodex_search_text_name: {
                                                                    required: "Please enter rolodex name "
                                                                }
                                                            },
                                                            submitHandler: function (form, event) {
                                                                event.preventDefault();
                                                            }
                                                        });
                                                        $("#addNewCase").validate({
                                                            rules: {
                                                                first_name: {
                                                                    required: true,
                                                                    noBlankSpace: true
                                                                },
                                                                last_name: {
                                                                    required: true,
                                                                    noBlankSpace: true
                                                                }
                                                            },
                                                            messages: {
                                                                first_name: {
                                                                    required: "Please provide first name"
                                                                },
                                                                last_name: {
                                                                    required: "Please provide last name"
                                                                }
                                                            },
                                                            submitHandler: function (form, event) {
                                                                setValidation();
                                                                event.preventDefault();
                                                                $("#caseNo").val($("#hdnCaseId").val());
                                                                var alldata = $("form#addNewCase").serialize();
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/checkDuplicateCase',
                                                                    method: 'POST',
                                                                    data: alldata,
                                                                    beforeSend: function (xhr) {
                                                                        $.blockUI();
                                                                    },
                                                                    complete: function (jqXHR, textStatus) {
                                                                        $.unblockUI();
                                                                    },
                                                                    success: function (result) {
                                                                        var obj = $.parseJSON(result);
                                                                        duplicateDatatable.rows().remove().draw();
                                                                        if (obj.status == '1') {
                                                                            $("#addcase").modal("hide");
                                                                            $.each(obj.data, function (index, item) {
                                                                                var html = '';
                                                                                html += "<tr class='selectDuplicateData' data-prospect_id=" + obj.prospect_id + " data-cardcode=" + item.cardcode + ">";
                                                                                html += "<td>" + item.caseno + "</td>";
                                                                                html += "<td>" + item.casetype + "</td>";
                                                                                html += "<td>" + item.casestat + "</td>";
                                                                                html += "<td>" + item.dateenter + "</td>";
                                                                                html += "<td>" + item.social_sec + "</td>";
                                                                                html += "<td>" + item.cardtype + "</td>";
                                                                                html += "<td>" + item.caption1 + "</td>";
                                                                                html += "</tr>";
                                                                                duplicateDatatable.row.add($(html));

                                                                            });

                                                                            duplicateDatatable.draw();
                                                                            if (obj.data.length == 1) {
                                                                                $("#duplicateEntries tr.selectDuplicateData").addClass("highlight");
                                                                                $('#edit_new_case').prop('disabled', false);
                                                                                $('#attach_name').prop('disabled', false);
                                                                            }
                                                                            $('input[name=first_name]').val($("#first_name").val());
                                                                            $('input[name=last_name]').val($("#last_name").val());
                                                                            $('input[name=prospect_id]').val(obj.prospect_id);
                                                                            $("#duplicateData").modal("show");
                                                                        } else {
                                                                            if ($("#prospect_id").val() != '')
                                                                            {
                                                                                $.ajax({
                                                                                    url: '<?php echo base_url(); ?>prospects/getProspectsdata',
                                                                                    method: "POST",
                                                                                    processData: true,
                                                                                    data: {prospect_id: $("#prospect_id").val()},
                                                                                    beforeSend: function (xhr) {
                                                                                        $.blockUI();
                                                                                    },
                                                                                    complete: function (jqXHR, textStatus) {
                                                                                        $.unblockUI();
                                                                                    },
                                                                                    success: function (result) {
                                                                                        var data = $.parseJSON(result);


                                                                                        $('#card_salutation').val(data.data.salutation);
                                                                                        $('#card_suffix').val(data.data.suffix);
                                                                                        $('#card_type').val(data.data.type);
                                                                                        $('input[name=card_first]').val(data.data.first);
                                                                                        $('input[name=card_last]').val(data.data.last);
                                                                                        $('input[name=card2_address1]').val(data.data.address1);
                                                                                        $('input[name=card2_city]').val(data.data.city);
                                                                                        $('input[name=card2_state]').val(data.data.state);
                                                                                        $('input[name=card2_zip]').val(data.data.zip);

                                                                                        $('input[name=card_business]').val(data.data.business);
                                                                                        $('input[name=card_home]').val(data.data.home);
                                                                                        $('input[name=card_car]').val(data.data.cell);
                                                                                        $('input[name=card_email]').val(data.data.email);
                                                                                        $('input[name=prospect_id]').val(data.data.prospect_id);


                                                                                        $('input[name=card_social_sec]').val(data.data.social_sec);
                                                                                        if (data.data.birthdate == '0000-00-00 00:00:00') {
                                                                                            $('input[name=card_birth_date]').val('');
                                                                                        } else {
                                                                                            $('input[name=card_birth_date]').val(moment(data.data.birthdate).format('MM/DD/YYYY'));
                                                                                        }
                                                                                        $("#new_case").val(1);
                                                                                        $("#prospect_id").val($("#prospect_id").val());

                                                                                        $("#addcase").modal("hide");
                                                                                        $("#addRolodexForm select").trigger('change');
                                                                                        $('select[name=card_language]').val('English');
                                                                                        $("select[name=card_language]").trigger('chosen:updated');
                                                                                        $("#addcontact").modal("show");
                                                                                    }
                                                                                });


                                                                            } else
                                                                            {
                                                                                $('input[name=card_first]').val($("#first_name").val());
                                                                                $('input[name=card_last]').val($("#last_name").val());
                                                                                $("#new_case").val(1);
                                                                                $("#addcase").modal("hide");
                                                                                $("#addRolodexForm select").trigger('change');
                                                                                $('select[name=card_language]').val('English');
                                                                                $("select[name=card_language]").trigger('chosen:updated');
                                                                                $("#addcontact").modal("show");
                                                                            }
                                                                        }
                                                                    }
                                                                });


                                                            }
                                                        });

                                                        $('#savecase').on('click', function () {

                                                            var alldata = $("form#edit_case_details").serialize();
                                                            var rbName = $("#case_reffered_by option:selected").text();
                                                            var nwCaption = $("#editcase_caption").text();
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/editCase',
                                                                method: 'POST',
                                                                data: alldata,
                                                                beforeSend: function (xhr) {
                                                                    $.blockUI();
                                                                },
                                                                complete: function (jqXHR, textStatus) {
                                                                    $.unblockUI();
                                                                },
                                                                success: function (result) {
                                                                    var data = $.parseJSON(result);
                                                                    $.unblockUI();
                                                                    if (data.status == '1') {
                                                                        swal("Success !", data.message, "success");
                                                                        $('#edit_case_details')[0].reset();
                                                                        $('#editcasedetail').modal('hide');
                                                                        $('#editcasedetail .modal-title').html('Edit Case');
                                                                    } else if (data.status == '0') {
                                                                        swal("Warning !", data.message, "error");
                                                                        $('#edit_case_details')[0].reset();
                                                                        $('#editcasedetail').modal('hide');
                                                                        $('#editcasedetail .modal-title').html('Edit Case');
                                                                    }
                                                                    $.ajax({
                                                                        url: '<?php echo base_url() . "cases/case_details" ?>',
                                                                        method: "POST",
                                                                        data: {'caseId': $("#hdnCaseId").val()},
                                                                        success: function (result) {
                                                                            var res = $.parseJSON(result);

                                                                            $(".casetype").html(res.display_details.casetype);
                                                                            $(".location").html(res.display_details.location);
                                                                            $(".casestat").html(res.display_details.casestat);
                                                                            $(".yourfileno").html(res.display_details.yourfileno);
                                                                            $(".venue").html(res.display_details.first);

                                                                            $(".atty_resp").html(res.display_details.atty_resp);
                                                                            $(".para_hand").html(res.display_details.para_hand);
                                                                            $(".atty_hand").html(res.display_details.atty_hand);
                                                                            $(".sec_hand").html(res.display_details.sec_hand);
                                                                            if (res.display_details.caption1.length > 55)
                                                                                $(".caption #caption_details").html(res.display_details.caption1.substring(0, 55) + '...');
                                                                            else
                                                                                $(".caption #caption_details").html(res.display_details.caption1);
                                                                            $(".casesubtype").html(res.display_details.udfall);

                                                                            if ($('div.sticky.caption').hasClass('caption-closed')) {
                                                                                $('div.sticky.caption').html(res.display_details.caption1 + ' - ' + 'CLOSED');
                                                                            }

                                                                            var dateenter = 'NA';
                                                                            var dateopen = 'NA';
                                                                            var followup = 'NA';
                                                                            var dateclosed = 'NA';
                                                                            var dr = 'NA';
                                                                            var psdate = 'NA';

                                                                            if (res.display_details.dateenter == '1899-12-31 00:00:00' || res.display_details.dateenter == '1899-12-30 00:00:00' || res.display_details.dateenter == '1970-01-01 00:00:00') {
                                                                                dateenter = 'NA';
                                                                            } else {
                                                                                dateenter = moment(res.display_details.dateenter).format('MM/DD/YYYY');
                                                                            }

                                                                            if (res.display_details.dateopen == '1899-12-31 00:00:00' || res.display_details.dateopen == '1899-12-30 00:00:00' || res.display_details.dateopen == '1970-01-01 00:00:00') {
                                                                                dateopen = 'NA';
                                                                            } else {
                                                                                dateopen = moment(res.display_details.dateopen).format('MM/DD/YYYY');
                                                                            }

                                                                            if (res.display_details.followup == '1899-12-31 00:00:00' || res.display_details.followup == '1899-12-30 00:00:00' || res.display_details.followup == '1970-01-01 00:00:00') {
                                                                                followup = 'NA';
                                                                            } else {
                                                                                followup = moment(res.display_details.followup).format('MM/DD/YYYY');
                                                                            }


                                                                            if (res.display_details.dateclosed == '1899-12-31 00:00:00' || res.display_details.dateclosed == '1899-12-30 00:00:00' || res.display_details.dateclosed == '1970-01-01 00:00:00') {
                                                                                dateclosed = 'NA';
                                                                            } else {
                                                                                dateclosed = moment(res.display_details.dateclosed).format('MM/DD/YYYY');
                                                                            }

                                                                            if (res.display_details.d_r == '1899-12-31 00:00:00' || res.display_details.d_r == '1899-12-30 00:00:00' || res.display_details.d_r == '1970-01-01 00:00:00') {
                                                                                dr = 'NA';
                                                                            } else {
                                                                                dr = moment(res.display_details.d_r).format('MM/DD/YYYY');
                                                                            }

                                                                            if (res.display_details.psdate == '1899-12-31 00:00:00' || res.display_details.psdate == '1899-12-30 00:00:00' || res.display_details.psdate == '1970-01-01 00:00:00' || res.display_details.psdate == '') {
                                                                                psdate = 'NA';
                                                                            } else {
                                                                                psdate = moment(res.display_details.psdate).format('MM/DD/YYYY');
                                                                            }

                                                                            $(".dateenter").html(dateenter);
                                                                            $(".dateopen").html(dateopen);
                                                                            $(".followup").html(followup);
                                                                            $(".d_r").html(dr);
                                                                            $(".rb").html(rbName);
                                                                            $(".ps").html(psdate);
                                                                            $(".dateclosed").html(dateclosed);
                                                                            $(".notes").html(res.category_array.quickNote);
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        });


                                                        /*                $("#parties_case_specific_data").validate({
                                                         
                                                         submitHandler: function (form, event) {
                                                         event.preventDefault();
                                                         $("#caseNo").val($("#hdnCaseId").val());
                                                         var alldata = $("form#parties_case_specific_data").serialize();
                                                         $('#partiesCardCode').val();
                                                         $.ajax({
                                                         url: '<?php echo base_url(); ?>cases/setPartiesCaseSpecific',
                                                         method: 'POST',
                                                         data: alldata,
                                                         beforeSend: function (xhr) {
                                                         $.blockUI();
                                                         },
                                                         complete: function (jqXHR, textStatus) {
                                                         $.unblockUI();
                                                         },
                                                         success: function (res) {
                                                         var data = $.parseJSON(res);
                                                         if (data.status == '1') {
                                                         
                                                         $.ajax({
                                                         url: '<?php echo base_url(); ?>cases/getCaseSpecific',
                                                         method: 'POST',
                                                         dataType: 'json',
                                                         data: {
                                                         cardcode: $('#partiesCardCode').val(),
                                                         caseno: $('#caseNo').val()
                                                         },
                                                         beforeSend: function (xhr) {
                                                         $.blockUI();
                                                         },
                                                         complete: function (jqXHR, textStatus) {
                                                         $.unblockUI();
                                                         },
                                                         success: function (data) {
                                                         
                                                         $("#party_type").html(data[0].type);
                                                         $("#party_side").html(data[0].side);
                                                         $("#party_offc_no").html(data[0].officeno);
                                                         $("#party_notes").html(data[0].notes);
                                                         }
                                                         });
                                                         swal("Success !", data.message, "success");
                                                         
                                                         } else {
                                                         swal("Alert !", data.message, "warning");
                                                         }
                                                         
                                                         $('#caseSpecific').modal('hide');
                                                         }
                                                         });
                                                         
                                                         }
                                                         });*/

                                                        $('#sfl').tooltip();
                                                        $("#addInjuryForm").validate({
                                                            rules: {
                                                                field_first: {
                                                                    required: true
                                                                },
                                                                field_last: {
                                                                    required: true
                                                                },
                                                                field_doi2: {required: function () {
                                                                        if ($('#field_doi').val() == "")
                                                                            return true;
                                                                        else
                                                                            return false;
                                                                    }
                                                                }
                                                            },
                                                            messages: {
                                                                field_first: {
                                                                    required: "First name required"
                                                                },
                                                                field_last: {
                                                                    remote: "Last name required"
                                                                },
                                                                field_doi2: {
                                                                    required: "CT Dates is required."
                                                                }
                                                            }
                                                        });


                                                        var adarr = "<?php echo isset($system_caldata[0]->calattyass) ? $system_caldata[0]->calattyass : '' ?>";
                                                        if (adarr == 3) {
                                                            $("select[name=attyass]").attr("disabled", true);
                                                        } else {
                                                            $("select[name=attyass]").removeAttr("disabled");
                                                        }
                                                    });

                                                    $(document.body).on('click', '.activity_edit', function () {
                                                        var id = $(this).data('actid');
                                                        caseActEdit(id);
                                                    });

                                                    function setValidation() {
                                                        var validator = $('#addRolodexForm').validate({
                                                            ignore: [],
                                                            rules: {
                                                                card_first: {
                                                                    required: function (element) {
                                                                        if ($("input[name=card2_firm]").val() == '') {
                                                                            return true;
                                                                        }
                                                                        return false;
                                                                    }
                                                                },
                                                                card2_firm: {
                                                                    required: function (element) {
                                                                        if ($("input[name=card_first]").val() == '') {
                                                                            return true;
                                                                        }
                                                                        return false;
                                                                    }
                                                                },
                                                                card2_address1: {
                                                                    required: function (element) {
                                                                        if ($("input[name=card2_firm]").val() != '') {
                                                                            return true;
                                                                        }
                                                                        return false;
                                                                    }
                                                                },
                                                                card2_city: {
                                                                    required: function (element) {
                                                                        if ($("input[name=card2_firm]").val() != '') {
                                                                            return true;
                                                                        }
                                                                        return false;
                                                                    }
                                                                },
                                                                card2_state: {
                                                                    required: function (element) {
                                                                        if ($("input[name=card2_firm]").val() != '') {
                                                                            return true;
                                                                        }
                                                                        return false;
                                                                    },
                                                                    stateUS: true
                                                                },
                                                                card2_zip: {
                                                                    required: function (element) {
                                                                        if ($("input[name=card2_firm]").val() != '') {
                                                                            return true;
                                                                        }
                                                                        return false;
                                                                    },
                                                                    zipcodeUS: true
                                                                },
                                                                card_type: {
                                                                    required: true
                                                                            /*function (element) {
                                                                             if ($("input[name=card_type]").val() != '') {
                                                                             return true;
                                                                             }
                                                                             return false;
                                                                             }*/
                                                                },
                                                                /*card_middle: {letterwithspace: true},
                                                                card_last: {letterwithspace: true},*/
                                                                card_business: {phoneUS: true},
                                                                card_home: {phoneUS: true},
                                                                card_fax: {phoneUS: true, maxlength: 15},
                                                                card_email: {email: true},
                                                                card2_phone1: {phoneUS: true},
                                                                card2_fax: {phoneUS: true},
                                                                card2_phone2: {phoneUS: true},
                                                                card2_fax2: {phoneUS: true},
                                                                card2_tax_id: {alphanumeric: true, maxlength: 15},
                                                                card_social_sec: {ssId: true},
                                                                card_speciality: {letterswithbasicpunc: true}
                                                            },
                                                            messages: {
                                                                card_first: {required: "Please Fill First Name or Firm Name"},
                                                                card2_firm: {required: "Please Fill First Name or Firm Name"},
                                                                card2_fax: {phoneUS: "Please specify a valid fax number"},
                                                                card2_fax2: {phoneUS: "Please specify a valid fax number"},
                                                                card_fax: {phoneUS: "Please specify a valid fax number"}
                                                            },
                                                            invalidHandler: function (form, validator) {
                                                                var element = validator.invalidElements().get(0);
                                                                var tab = $(element).closest('.tab-pane').attr('id');

                                                                $('#addcontact a[href="#' + tab + '"]').tab('show');
                                                            },
                                                            errorPlacement: function (error, element) {
                                                                if (element.attr("name") == "card_type") {
                                                                    error.insertAfter(element.next());
                                                                } else {
                                                                    error.insertAfter(element);
                                                                }
                                                            }
                                                        });

                                                        return validator;
                                                    }

                                                    function setCalendarValidation() {
                                                        var validator = $('#calendarForm').validate({
                                                            ignore: [],
                                                            rules: {
                                                                calstat: {letterwithspace: true},
                                                                first: {
                                                                    required: true,
                                                                    letterswithbasicpunc: true
                                                                },
                                                                last: {
                                                                    required: true,
                                                                    letterswithbasicpunc: true
                                                                },
                                                                defendant: {letterswithbasicpunc: true},
                                                                judge: {letterswithbasicpunc: true},
                                                                venue: {letterswithbasicpunc: true},
                                                                caldate: {required: true},
                                                                caltime: {required: true},
                                                                event: {required: true},
                                                                caseno: {number: true,
                                                                    remote: {
                                                                        url: '<?php echo base_url(); ?>prospects/CheckCaseno',
                                                                        type: "post",
                                                                        data: {
                                                                            pro_caseno: function () {
                                                                                return $('#calendarForm :input[name="caseno"]').val();
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            },
                                                            messages: {
                                                                caseno: {
                                                                    remote: "This caseno is not exist"
                                                                }
                                                            },
                                                            invalidHandler: function (form, validator) {
                                                                var element = validator.invalidElements().get(0);
                                                                var tab = $(element).closest('.tab-pane').attr('id');

                                                                $('#addcalendar a[href="#' + tab + '"]').tab('show');
                                                            }
                                                        });
                                                        return validator;
                                                    }
                                                    function editContact(cardCode) {
                                                        $.blockUI();

                                                        $.ajax({
                                                            url: "<?php echo base_url() . 'rolodex/getEditRolodex'; ?>",
                                                            method: "POST",
                                                            data: {'cardcode': cardCode},
                                                            success: function (result) {
                                                                var data = $.parseJSON(result);
                                                                var notes_comments = '';
                                                                if (data.card_comments) {
                                                                    if(isJson(data.card_comments)) {
                                                                        notes_comments = $.parseJSON(data.card_comments);
                                                                    } else {
                                                                        notes_comments = data.card_comments;
                                                                    }
                                                                }
                                                                $('#addcontact .modal-title').html('Edit Contact');
                                                                $('#card_salutation').val(data.salutation);
                                                                $('#card_suffix').val(data.suffix);
                                                                $('select[name=card_title]').val(data.title);
                                                                $('#card_type').val(data.type);
                                                                $('input[name=card_first]').val(data.first);
                                                                $('input[name=card_middle]').val(data.middle);
                                                                $('input[name=card_last]').val(data.last);
                                                                $('input[name=card_popular]').val(data.popular);
                                                                $('input[name=card_letsal]').val(data.letsal);
                                                                $('input[name=card_occupation]').val(data.occupation);
                                                                $('input[name=card_employer]').val(data.employer);
                                                                $('input[name=hiddenimg]').val(data.picture);
                                                                if (data.picture)
                                                                {
                                                                    document.getElementById('profilepicDisp').style.display = 'block';
                                                                    document.getElementById('dispProfile').src = data.picturePath;
                                                                } else
                                                                {
                                                                    document.getElementById('dispProfile').src = '';
                                                                }
                                                                $('input[name=card2_firm]').val(data.firm);
                                                                $('input[name=card2_address1]').val(data.address1);
                                                                $('input[name=card2_address2]').val(data.address2);
                                                                $('input[name=card2_city]').val(data.city);
                                                                $('input[name=card2_state]').val(data.state);
                                                                $('input[name=card2_zip]').val(data.zip);
                                                                $('input[name=card2_venue]').val(data.venue);
                                                                $('input[name=card2_eamsref]').val(data.eamsref);
                                                                $('input[name=card_business]').val(data.business);
                                                                $('input[name=card_fax]').val(data.card_fax);
                                                                $('input[name=card_home]').val(data.home);
                                                                $('input[name=card_car]').val(data.car);
                                                                $('input[name=card_email]').val(data.email);
                                                                if (notes_comments != '')
                                                                    $('input[name=card_notes]').val(notes_comments[0].notes);
                                                                else
                                                                    $('input[name=card_notes]').val();
                                                                $('input[name=card2_phone1]').val(data.phone1);
                                                                $('input[name=card2_phone2]').val(data.phone2);
                                                                $('input[name=card2_tax_id]').val(data.tax_id);
                                                                $('input[name=card2_fax]').val(data.card2_fax);
                                                                $('input[name=card2_fax2]').val(data.fax2);
                                                                $('input[name=card_social_sec]').val(data.social_sec);
                                                                if (data.birth_date == 'NA') {
                                                                    $('input[name=card_birth_date]').val('');
                                                                } else {
                                                                    $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                                                                }

                                                                $('input[name=card_licenseno]').val(data.licenseno);
                                                                $('select[name=card_specialty]').val(data.specialty);
                                                                $('input[name=card_mothermaid]').val(data.mothermaid);
                                                                $('input[name=card_cardcode]').val(data.cardcode);
                                                                $('input[name=card_firmcode]').val(data.firmcode);
                                                                $('select[name=card_interpret]').val(data.interpret);
                                                                $('select[name=card_language]').val(data.language);

                                                                $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                                                                $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                                                                $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                                                                $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                                                                if (notes_comments != '')
                                                                    $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                                                else
                                                                    $('textarea[name=card_comments]').val();
                                                                $('textarea[name=card2_comments]').val(data.card2_comments);
                                                                $('textarea[name=card2_mailing1]').val(data.mailing1);
                                                                $('textarea[name=card2_mailing2]').val(data.mailing2);
                                                                $('textarea[name=card2_mailing3]').val(data.mailing3);
                                                                $('textarea[name=card2_mailing4]').val(data.mailing4);

                                                                $('#new_case').val('parties');
                                                                $('input[name="card_cardcode"]').val(data.cardcode);

                                                                $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                $("#addcontact").modal('show');

                                                                $("#addRolodexForm select").trigger('change');

                                                                $.unblockUI();
                                                            }
                                                        });
                                                    }

                                                    function editCaseDetails(caseNo, selectTab) {
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/getAjaxCaseDetail',
                                                            data: 'caseno=' + caseNo,
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            beforeSend: function (xhr) {
                                                                $.blockUI();
                                                            },
                                                            complete: function (jqXHR, textStatus) {
                                                                $.unblockUI();
                                                            },
                                                            success: function (data) {
                                                                data = data[0];

                                                                if (data.dateenter != '1970-01-01 05:30:00' && data.dateenter != '1899-12-30 00:00:00' && data.dateenter != '0000-00-00 00:00:00' && data.dateenter != null) {
                                                                    var dateenter = moment(data.dateenter).format('MM/DD/YYYY');
                                                                    $('#casedate_entered').val(dateenter);
                                                                }

                                                                if (data.dateopen != '1970-01-01 05:30:00' && data.dateopen != '1899-12-30 00:00:00' && data.dateopen != '0000-00-00 00:00:00' && data.dateopen != null) {
                                                                    var dateopen = moment(data.dateopen).format('MM/DD/YYYY');
                                                                    $('#casedate_open').val(dateopen);
                                                                }

                                                                if (data.followup != '1970-01-01 05:30:00' && data.followup != '1899-12-30 00:00:00' && data.followup != '0000-00-00 00:00:00' && data.followup != null) {
                                                                    var followup = moment(data.followup).format('MM/DD/YYYY');
                                                                    $('#casedate_followup').val(followup);
                                                                }

                                                                if (data.dateclosed != '1970-01-01 05:30:00' && data.dateclosed != '1899-12-30 00:00:00' && data.dateclosed != '0000-00-00 00:00:00' && data.dateclosed != null) {
                                                                    var dateclosed = moment(data.dateclosed).format('MM/DD/YYYY');
                                                                    $('#casedate_closed').val(dateclosed);
                                                                }

                                                                if (data.psdate != '1970-01-01 05:30:00' && data.psdate != '1899-12-30 00:00:00' && data.psdate != '0000-00-00 00:00:00' && data.psdate != null) {
                                                                    var psdate = moment(data.psdate).format('MM/DD/YYYY');
                                                                    $('#casedate_psdate').val(psdate);
                                                                }

                                                                if (data.d_r != '1970-01-01 05:30:00' && data.d_r != '1899-12-30 00:00:00' && data.d_r != '0000-00-00 00:00:00' && data.d_r != null) {
                                                                    var d_r = moment(data.d_r).format('MM/DD/YYYY');
                                                                    $('#casedate_referred').val(d_r);
                                                                }

                                                                if (data.udfall2 != null)
                                                                {
                                                                    var res = data.udfall2.split(",");
                                                                    $('#closedfileno').val(res[0]);
                                                                    $('select[name=editOtherClass]').val(res[1]);
                                                                }

                                                                $('#fileno').val(data.yourfileno);
                                                                $('#editCaseVenue1').val(data.first);
                                                                $('#editCaseVenue').val(data.venue);
                                                                $('select[name=editCaseSubType]').val(data.udfall);

                                                                if ($("#editCaseType").hasClass("select2Class"))
                                                                {
                                                                    if ($('#editCaseType option:contains(' + data.casetype + ')').length) {
                                                                        $('select[name=editCaseType]').val(data.casetype);
                                                                    } else
                                                                    {
                                                                        if (data.casetype)
                                                                            $('#editCaseType').append("<option value='" + data.casetype + "' selected='seleceted'>" + data.casetype + "</option>");
                                                                    }
                                                                } else if ($("#editCaseType").hasClass("cstmdrp"))
                                                                {
                                                                    if (data.casetype)
                                                                        $('#editCaseType').val(data.casetype);
                                                                }

                                                                if ($("#editCaseStat").hasClass("select2Class"))
                                                                {
                                                                    if ($('#editCaseStat option:contains(' + data.casetype + ')').length) {
                                                                        $('select[name=editCaseStat]').val(data.casetype);
                                                                    } else
                                                                    {
                                                                        if (data.casestat)
                                                                            $('#editCaseStat').append("<option value='" + data.casestat + "' selected='seleceted'>" + data.casestat + "</option>");
                                                                    }
                                                                } else if ($("#editCaseStat").hasClass("cstmdrp"))
                                                                {
                                                                    if (data.casestat)
                                                                        $('#editCaseStat').val(data.casestat);
                                                                }

                                                                if ($("#editCaseAttyResp").hasClass("select2Class"))
                                                                {
                                                                    if ($('#editCaseAttyResp option:contains(' + data.atty_resp + ')').length) {
                                                                        $('select[name=editCaseAttyResp]').val(data.atty_resp);
                                                                    } else
                                                                    {
                                                                        if (data.atty_resp)
                                                                            $('#editCaseAttyResp').append("<option value='" + data.atty_resp + "' selected='seleceted'>" + data.atty_resp + "</option>");
                                                                    }
                                                                } else if ($("#editCaseAttyResp").hasClass("cstmdrp"))
                                                                {
                                                                    if (data.atty_resp)
                                                                        $('#editCaseAttyResp').val(data.atty_resp);
                                                                }

                                                                if ($("#editCaseAttyHand").hasClass("select2Class"))
                                                                {
                                                                    if ($('#editCaseAttyHand option:contains(' + data.atty_hand + ')').length) {
                                                                        $('select[name=editCaseAttyHand]').val(data.atty_hand);
                                                                    } else
                                                                    {
                                                                        if (data.atty_hand)
                                                                            $('#editCaseAttyHand').append("<option value='" + data.atty_hand + "' selected='seleceted'>" + data.atty_hand + "</option>");
                                                                    }
                                                                } else if ($("#editCaseAttyHand").hasClass("cstmdrp"))
                                                                {
                                                                    if (data.atty_hand)
                                                                        $('#editCaseAttyHand').val(data.atty_hand);
                                                                }

                                                                if ($("#editCaseAttyPara").hasClass("select2Class"))
                                                                {
                                                                    if ($('#editCaseAttyPara option:contains(' + data.para_hand + ')').length) {
                                                                        $('select[name=editCaseAttyPara]').val(data.para_hand);
                                                                    } else
                                                                    {
                                                                        if (data.para_hand)
                                                                            $('#editCaseAttyPara').append("<option value='" + data.para_hand + "' selected='seleceted'>" + data.para_hand + "</option>");
                                                                    }
                                                                } else if ($("#editCaseAttyPara").hasClass("cstmdrp"))
                                                                {
                                                                    if (data.para_hand)
                                                                        $('#editCaseAttyPara').val(data.para_hand);
                                                                }

                                                                if ($("#editCaseAttySec").hasClass("select2Class"))
                                                                {
                                                                    if ($('#editCaseAttySec option:contains(' + data.sec_hand + ')').length) {
                                                                        $('select[name=editCaseAttySec]').val(data.sec_hand);
                                                                    } else
                                                                    {
                                                                        if (data.sec_hand)
                                                                            $('#editCaseAttySec').append("<option value='" + data.sec_hand + "' selected='seleceted'>" + data.sec_hand + "</option>");
                                                                    }
                                                                } else if ($("#editCaseAttySec").hasClass("cstmdrp"))
                                                                {
                                                                    if (data.sec_hand)
                                                                        $('#editCaseAttySec').val(data.sec_hand);
                                                                }

                                                                $('#case_ps').val(data.ps);
                                                                $('#fileloc').val(data.location);
                                                                $('select[name=case_reffered_by]').val(data.rb);

                                                                if (typeof data.category_events !== 'undefined') {
                                                                    if (typeof data.category_events.quickNote !== 'undefined') {
                                                                        $('#editcase_quicknote').val(data.category_events.quickNote);
                                                                    }
                                                                    if (typeof data.category_events.message1 !== 'undefined') {
                                                                        $('#caseedit_message1').val(data.category_events.message1);
                                                                    }
                                                                    if (typeof data.category_events.message2 !== 'undefined') {
                                                                        $('#caseedit_message2').val(data.category_events.message2);
                                                                    }
                                                                    if (typeof data.category_events.message3 !== 'undefined') {
                                                                        $('#caseedit_message3').val(data.category_events.message3);
                                                                    }
                                                                }

                                                                $('#editcase_caption').val(data.caption1);

                                                                $('#editcasedetail').modal('show');
                                                                $("#edit_case_details select").trigger('change');
                                                                $("a[href='#" + selectTab + "']").trigger("click");
                                                            }
                                                        });
                                                    }


                                                    $(document.body).on('change', '#default_freeze', function () {
                                                        if ($('#default_freeze:checked').length) {
                                                            $("#default_feeze_until").attr("disabled", true);
                                                        } else {
                                                            $("#default_feeze_until").attr("disabled", false);
                                                        }
                                                    });
                                                    $(document.body).on('click', '.complete_task', function () {
                                                        var id = $(this).attr('data-id');
                                                        var event = $(this).attr('data-event');
                                                        var caseno = $(this).attr('data-caseno');
                                                        if (caseno <= 0) {
                                                            completeTask(id);
                                                        } else {
                                                            $('#case_event').val(event);
                                                            $('#caseTaskId').val(id);
                                                            $('#caseno').val(caseno);
                                                            $("#TaskCompleted").modal("show");
                                                        }
                                                    });
                                                    $(document.body).on('click', '#saveCompletedTask', function () {
                                                        /* $('#saveCompletedTask').attr('disabled','disabled');*/
                                                        var id = $('#caseTaskId').val();
                                                        var case_event = $('#case_event').val();
                                                        var case_extra_event = $('#case_extra_event').val();
                                                        var case_category = $('#case_category').val();
                                                        var caseno = $('#caseno').val();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>tasks/completedTaskWithCase',
                                                            data: {taskId: id, case_event: case_event, case_extra_event: case_extra_event, case_category: case_category, caseno: caseno},
                                                            method: "POST",
                                                            success: function (result) {
                                                                $.unblockUI();
                                                                var data = $.parseJSON(result);
                                                                if (data.status == 1) {
                                                                    swal("Success !", data.message, "success");
                                                                    $("#TaskCompleted").modal("hide");
                                                                    taskTable1.ajax.reload(null, false);
                                                                } else {
                                                                    swal("Oops...", data.message, "error");
                                                                }
                                                            }
                                                        });
                                                    });
                                                    function setTaskValidation() {

                                                        var validator = $('#addTaskForm').validate({
                                                            ignore: [],
                                                            rules: {
                                                                whofrom: {required: true},
                                                                whoto2: {required: true},
                                                                datereq: {required: true},
                                                                event: {required: true, noBlankSpace: true}
                                                            }
                                                        });
                                                        return validator;
                                                    }

                                                    function deleteTask(id) {
                                                        swal({
                                                            title: "Alert",
                                                            text: "Are you sure to Remove this Task? ",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, delete it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            $.blockUI();
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>tasks/deleteRec',
                                                                data: {taskId: id},
                                                                method: "POST",
                                                                success: function (result) {
                                                                    $.unblockUI();
                                                                    var data = $.parseJSON(result);
                                                                    if (data.status == 1) {

                                                                        taskTable1.ajax.reload();
                                                                        taskTable.ajax.reload(null, false);
                                                                        swal("Success !", data.message, "success");
                                                                    } else {
                                                                        swal("Oops...", data.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }

                                                    function completeTask(a) {
                                                        swal({
                                                            title: "Are you sure? Want to Set Task as Completed ",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#1ab394",
                                                            confirmButtonText: "Yes, Set as Complete!",
                                                            closeOnConfirm: true,
                                                            showLoaderOnConfirm: true
                                                        }, function () {
                                                            /*$.blockUI();*/

                                                            var c = $('#' + a).attr("data-id");
                                                            /* var d = $('#'+a).attr("data-event");
                                                             var b = $('#'+a).attr("data-caseno");
                                                             $("#case_event").val(d);
                                                             $("#caseTaskId").val(c);
                                                             $("#caseno").val(b);
                                                             $("#TaskCompleted").modal("show");*/
                                                            $.ajax({
                                                                url: "<?php echo base_url(); ?>tasks/completedTask",
                                                                data: {
                                                                    taskId: a
                                                                },
                                                                method: "POST",
                                                                success: function (b) {
                                                                    $.unblockUI();
                                                                    var c = $.parseJSON(b);
                                                                    if (c.status == 1) {
                                                                        swal("Success !", c.message, "success");
                                                                        taskTable1.ajax.reload(null, false);
                                                                        /*caseactTable.ajax.reload(null, false);*/
                                                                    } else {
                                                                        swal("Oops...", c.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }

                                                    function editTask(id) {
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>tasks/getTask',
                                                            data: {taskId: id},
                                                            method: "POST",
                                                            success: function (result) {
                                                                var data = $.parseJSON(result);
                                                                $.unblockUI();

                                                                if (data.status == 0) {
                                                                    swal("Oops...", data.message, "error");
                                                                } else {
                                                                    $('#addtask1 .modal-title').html('Edit Task');
                                                                    $('input[name=taskid]').val(id);
                                                                    $('select[name=whofrom]').val(data.whofrom);
                                                                    $('select[name=whoto3]').val(data.whoto);
                                                                    $('input[name=datereq]').val(data.datereq);
                                                                    $('textarea[name=event]').val(data.event);
                                                                    $('select[name=color]').val(data.color);
                                                                    $('select[name=priority]').val(data.priority);
                                                                    $('select[name=typetask]').val(data.typetask);
                                                                    $('select[name=phase]').val(data.phase);

                                                                    if (data.notify == 1) {
                                                                        $('input[name=notify]').iCheck('check');
                                                                    } else {
                                                                        $('input[name=notify]').iCheck('uncheck');
                                                                    }

                                                                    if (data.reminder == 1) {
                                                                        $('input[type="checkbox"][name="reminder1"]').prop("checked", true).change();
                                                                        $('.rdate').val(data.popupdate);
                                                                        $('.rdate1').val(data.popupdate);
                                                                        $('.rtime').val(data.popuptime);
                                                                        $('.rmdt').show();
                                                                    } else {
                                                                        $('input[type="checkbox"][name="reminder1"]').prop("checked", false).change();
                                                                        $('.rmdt').hide();
                                                                    }

                                                                    $('#addtask1').modal({backdrop: 'static', keyboard: false});
                                                                    $("#addtask1").modal('show');

                                                                    $("#addTaskForm1 select").trigger('change');

                                                                }
                                                            }
                                                        });
                                                    }


                                                    $(document.body).on('click', '#change_name_warning', function () {
                                                        $("#partiesNameChangeWarning").modal("hide");
                                                        editContact($('#party_name').data('cardcodeWarning'));
                                                    });

                                                    $(document.body).on('click', '.typeLink', function () {
                                                        var cardcode = $('#party_type').data('cardcodeWarning');
                                                        var caseno = $("#caseNo").val();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/getCaseSpecific',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                cardcode: cardcode,
                                                                caseno: caseno
                                                            },
                                                            beforeSend: function (xhr) {
                                                                $.blockUI();
                                                            },
                                                            complete: function (jqXHR, textStatus) {
                                                                $.unblockUI();
                                                            },
                                                            success: function (data) {
                                                                $("#parties_case_specific").val(data[0].type);
                                                                $("#parties_side").val(data[0].side);
                                                                $("#parties_office_number").val(data[0].officeno);
                                                                $("#parties_notes").val(data[0].notes);
                                                                (data[0].flags != '1') ? $('#parties_party_sheet').iCheck('uncheck') : $('#parties_party_sheet').iCheck('check');
                                                            }
                                                        });

                                                        $("#caseSpecific").modal("show");
                                                    });


                                                    $(document.body).on('click', '#attach_name', function () {
                                                        var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                                                        var prospect_id = $("#duplicateEntries .highlight").data('prospect_id');
                                                        var first_name = $(".first_name").val();
                                                        var last_name = $(".last_name").val();
                                                        var casetype = 'PNC';
                                                        var casestat = '<?php echo $system_data[0]->casestat; ?>';

                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/attach_name',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                cardcode: cardcode,
                                                                first_name: first_name,
                                                                last_name: last_name,
                                                                prospect_id: prospect_id,
                                                                casetype: casetype,
                                                                casestat: casestat
                                                            },
                                                            beforeSend: function (xhr) {
                                                                $.blockUI();
                                                            },
                                                            complete: function (jqXHR, textStatus) {
                                                                $.unblockUI();
                                                            },
                                                            success: function (result) {
                                                                var url = "<?php echo base_url() ?>cases/case_details/" + result.caseno;
                                                                $(location).attr('href', url);
                                                            }
                                                        });

                                                    });

                                                    $(document.body).on('click', '.selectDuplicateData', function () {
                                                        $("#duplicateEntries tr").removeClass("highlight");
                                                        $(this).addClass('highlight');
                                                        var numItems = $('.highlight').length;
                                                        if (numItems == '1')
                                                        {
                                                            $('#edit_new_case').prop('disabled', false);
                                                            $('#attach_name').prop('disabled', false);
                                                        }
                                                    });

                                                    $(document.body).on('click', '#add_name_case', function () {
                                                        var category = $("td.pro_type").text();
                                                        $('input[name=card_first]').val($(".first_name").val());
                                                        $('input[name=card_last]').val($(".last_name").val());
                                                        $("select[name=card_type]").val(category).trigger('change');
                                                        $('input[name=prospect_id]').val($(".prospect_id").val());
                                                        $("#new_case").val(1);
                                                        $("#addcase").modal("hide");
                                                        $("#duplicateData").modal("hide");
                                                        $("#addcontact").modal("show");
                                                        return false;
                                                    });

                                                    $(document.body).on('click', '#edit_new_case', function () {
                                                        var cardCode = $('.highlight').data('cardcode');
                                                        $('#caseCardCode').val(cardCode);
                                                        $.ajax({
                                                            url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                                                            method: "POST",
                                                            data: {'cardcode': cardCode},
                                                            success: function (result) {
                                                                if (result > 1) {
                                                                    $("#partiesNameChangeWarning").modal("show");
                                                                } else
                                                                {
                                                                    ChangeRolodex();
                                                                }
                                                            }
                                                        });
                                                    });

                                                    $(document.body).on('click', '.venuelist', function () {
                                                        $("#venulist_model").modal("show");
                                                    });


                                                    $('#venue_table tbody').on('click', 'tr', function () {
                                                        if ($(this).hasClass('table-selected')) {
                                                            $(this).removeClass('table-selected');
                                                        } else {
                                                            venue_table_data.$('tr.table-selected').removeClass('table-selected');
                                                            $(this).addClass('table-selected');
                                                        }
                                                    });

                                                    $(document.body).on('click', '#set_venue', function () {
                                                        var contentArray = [];
                                                        $.map(venue_table_data.rows('.table-selected').data(), function (item) {
                                                            contentArray = item;
                                                            return contentArray;
                                                        });
                                                        if (contentArray[1].lengh > 0) {
                                                            $("#editCaseVenue1").val(contentArray[1]);
                                                        } else {
                                                            $("#editCaseVenue1").val(contentArray[0]);
                                                        }
                                                        $("#editCaseVenue").val(contentArray.DT_RowId);
                                                        $("#venulist_model").modal("hide");
                                                    });
                                                    $(document.body).on('click', '.edit-party', function () {

                                                        $('#show_conform_model').modal('show');

                                                    });
                                                    $(document.body).on('click', '#relodex_yes', function () {
                                                        var cardcode = $('#edit-party').attr('data-casecard');
                                                        var caseno = $('#edit-party').attr('data-case');
                                                        $('#show_conform_model').modal('hide');
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>rolodex/getEditRolodex',
                                                            data: {cardcode: cardcode, caseNO: caseno, status: 'Yes'},
                                                            method: "POST",
                                                            success: function (result) {
                                                                var data = $.parseJSON(result);
                                                                var notes_comments = '';
                                                                if (data.card_comments) {
                                                                    if(isJson(data.card_comments)) {
                                                                        notes_comments = $.parseJSON(data.card_comments);
                                                                    } else {
                                                                        notes_comments = data.card_comments;
                                                                    }
                                                                }
                                                                swal.close();
                                                                if (data.status == '0')
                                                                {
                                                                    var name = '';
                                                                    if (data.data.type == 'APPLICANT')
                                                                    {
                                                                        name = data.data.last + ', ' + data.data.first;
                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                    } else if (data.data.type == 'LIEN' || data.data.type == 'BOARD' || data.data.type == 'INSURANCE' || data.data.type == 'ATTORNEY' || data.data.type == 'EMPLOYER')
                                                                    {
                                                                        name = data.data.firm;
                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                    } else if (data.data.type == 'DR') {
                                                                        if (data.data.firm.length == 0) {
                                                                            name = data.data.last + ', ' + data.data.first;
                                                                            $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                        } else {
                                                                            name = data.data.firm;
                                                                            $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                        }
                                                                    } else
                                                                    {
                                                                        name = data.data.last + ', ' + data.data.first;
                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');

                                                                    }


                                                                    $('#rolodex_search_text').val(name);
                                                                    $("#rolodex_search_parties_form").submit();
                                                                    $("#addparty").modal('show');
                                                                } else
                                                                {

                                                                    $('#addcontact .modal-title').html('Edit Contact');
                                                                    $('#card_salutation').val(data.salutation);
                                                                    $('#card_suffix').val(data.suffix);
                                                                    $('select[name=card_title]').val(data.title);
                                                                    $('#card_type').val(data.type);
                                                                    $('input[name=card_first]').val(data.first);
                                                                    $('input[name=card_middle]').val(data.middle);
                                                                    $('input[name=card_last]').val(data.last);
                                                                    $('input[name=card_popular]').val(data.popular);
                                                                    $('input[name=card_letsal]').val(data.letsal);
                                                                    $('input[name=card_occupation]').val(data.occupation);
                                                                    $('input[name=card_employer]').val(data.employer);
                                                                    $('input[name=hiddenimg]').val(data.picture);
                                                                    if (data.picture)
                                                                    {
                                                                        document.getElementById('profilepicDisp').style.display = 'block';
                                                                        document.getElementById('dispProfile').src = data.picturePath;
                                                                    } else
                                                                    {
                                                                        document.getElementById('dispProfile').src = '';
                                                                    }
                                                                    $('input[name=card2_firm]').val(data.firm);
                                                                    $('input[name=card2_address1]').val(data.address1);
                                                                    $('input[name=card2_address2]').val(data.address2);
                                                                    $('input[name=card2_city]').val(data.city);
                                                                    $('input[name=card2_state]').val(data.state);
                                                                    $('input[name=card2_zip]').val(data.zip);
                                                                    $('input[name=card2_venue]').val(data.venue);
                                                                    $('input[name=card2_eamsref]').val(data.eamsref);
                                                                    $('input[name=card_business]').val(data.business);
                                                                    $('input[name=card_fax]').val(data.card_fax);
                                                                    $('input[name=card_home]').val(data.home);
                                                                    $('input[name=card_car]').val(data.car);
                                                                    $('input[name=card_email]').val(data.email);
                                                                    if (notes_comments != '')
                                                                        $('input[name=card_notes]').val(notes_comments[0].notes);
                                                                    else
                                                                        $('input[name=card_notes]').val();
                                                                    $('input[name=card2_phone1]').val(data.phone1);
                                                                    $('input[name=card2_phone2]').val(data.phone2);
                                                                    $('input[name=card2_tax_id]').val(data.tax_id);
                                                                    $('input[name=card2_fax]').val(data.card2_fax);
                                                                    $('input[name=card2_fax2]').val(data.fax2);
                                                                    $('input[name=card_social_sec]').val(data.social_sec);
                                                                    if (data.birth_date == 'NA') {
                                                                        $('input[name=card_birth_date]').val('');
                                                                    } else {
                                                                        $('input[name=card_birth_date]').val(data.birth_date);
                                                                    }

                                                                    $('input[name=card_licenseno]').val(data.licenseno);
                                                                    $('select[name=card_specialty]').val(data.specialty);
                                                                    $('input[name=card_mothermaid]').val(data.mothermaid);
                                                                    $('input[name=card_cardcode]').val(data.cardcode);
                                                                    $('input[name=card_firmcode]').val(data.firmcode);
                                                                    $('select[name=card_interpret]').val(data.interpret);
                                                                    $('select[name=card_language]').val(data.language);

                                                                    $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                                                                    $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                                                                    $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                                                                    $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                                                                    if (notes_comments != '')
                                                                        $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                                                    else
                                                                        $('textarea[name=card_comments]').val();
                                                                    $('textarea[name=card2_comments]').val(data.card2_comments);
                                                                    $('textarea[name=card2_mailing1]').val(data.mailing1);
                                                                    $('textarea[name=card2_mailing2]').val(data.mailing2);
                                                                    $('textarea[name=card2_mailing3]').val(data.mailing3);
                                                                    $('textarea[name=card2_mailing4]').val(data.mailing4);



                                                                    $('#new_case').val('parties');
                                                                    $('input[name="card_cardcode"]').val(data.cardcode);

                                                                    $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                    $("#addcontact").modal('show');
                                                                    $("#addRolodexForm select").trigger('change');
                                                                    $.unblockUI();
                                                                }
                                                            }
                                                        });


                                                    });
                                                    $(document.body).on('click', '#relodex_no', function () {
                                                        $('#show_conform_model').modal('hide');
                                                        var cardcode = $('#edit-party').attr('data-casecard');
                                                        var caseno = $('#edit-party').attr('data-case');
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>rolodex/editRolodexdetail',
                                                            data: {cardcode: cardcode, caseNO: caseno, status: 'NO'},
                                                            method: "POST",
                                                            success: function (result) {
                                                                var data = $.parseJSON(result);
                                                                var notes_comments = $.parseJSON(data.data.card_comments);
                                                                if (data.status == '0')
                                                                {
                                                                    $.unblockUI();
                                                                    swal({
                                                                        title: 'Are you sure to make changes to this Rolodex Card?',
                                                                        text: ' ' + data.message,
                                                                        showCancelButton: true,
                                                                        confirmButtonColor: "#1ab394",
                                                                        confirmButtonText: "Yes",
                                                                        closeOnConfirm: false,
                                                                        showLoaderOnConfirm: true
                                                                    }, function (isConfirm) {
                                                                        if (isConfirm) {
                                                                            swal.close();
                                                                            $('#addcontact .modal-title').html('Edit Contact');
                                                                            $('#card_salutation').val(data.data.salutation);
                                                                            $('#card_suffix').val(data.data.suffix);
                                                                            $('select[name=card_title]').val(data.data.title);
                                                                            $('#card_type').val(data.data.type);
                                                                            $('input[name=card_first]').val(data.data.first);
                                                                            $('input[name=card_middle]').val(data.data.middle);
                                                                            $('input[name=card_last]').val(data.data.last);
                                                                            $('input[name=card_letsal]').val(data.data.letsal);
                                                                            $('input[name=card_occupation]').val(data.data.occupation);
                                                                            $('input[name=card_employer]').val(data.data.employer);
                                                                            $('input[name=hiddenimg]').val(data.data.picture);
                                                                            if (data.data.picture)
                                                                            {
                                                                                document.getElementById('profilepicDisp').style.display = 'block';
                                                                                document.getElementById('dispProfile').src = data.data.picturePath;
                                                                            } else
                                                                            {
                                                                                document.getElementById('dispProfile').src = '';
                                                                            }
                                                                            $('input[name=card2_firm]').val(data.data.firm);
                                                                            $('input[name=card2_address1]').val(data.data.address1);
                                                                            $('input[name=card2_address2]').val(data.data.address2);
                                                                            $('input[name=card2_city]').val(data.data.city);
                                                                            $('input[name=card2_state]').val(data.data.state);
                                                                            $('input[name=card2_zip]').val(data.data.zip);
                                                                            $('input[name=card2_venue]').val(data.data.venue);
                                                                            $('input[name=card2_eamsref]').val(data.data.eamsref);
                                                                            $('input[name=card_business]').val(data.data.business);
                                                                            $('input[name=card_fax]').val(data.data.card_fax);
                                                                            $('input[name=card_home]').val(data.data.home);
                                                                            $('input[name=card_car]').val(data.data.car);
                                                                            $('input[name=card_email]').val(data.data.email);
                                                                            if (notes_comments != '')
                                                                                $('input[name=card_notes]').val(notes_comments[0].notes);
                                                                            else
                                                                                $('input[name=card_notes]').val();
                                                                            $('input[name=card2_phone1]').val(data.data.phone1);
                                                                            $('input[name=card2_phone2]').val(data.data.phone2);
                                                                            $('input[name=card2_tax_id]').val(data.data.tax_id);
                                                                            $('input[name=card2_fax]').val(data.data.card2_fax);
                                                                            $('input[name=card2_fax2]').val(data.data.fax2);
                                                                            $('input[name=card_social_sec]').val(data.data.social_sec);
                                                                            if (data.data.birth_date == 'NA') {
                                                                                $('input[name=card_birth_date]').val('');
                                                                            } else {
                                                                                $('input[name=card_birth_date]').val(data.data.birth_date);
                                                                            }

                                                                            $('input[name=card_licenseno]').val(data.data.licenseno);
                                                                            $('select[name=card_specialty]').val(data.data.specialty);
                                                                            $('input[name=card_mothermaid]').val(data.data.mothermaid);
                                                                            $('input[name=card_cardcode]').val(data.data.cardcode);
                                                                            $('input[name=card_firmcode]').val(data.data.firmcode);
                                                                            $('select[name=card_interpret]').val(data.data.interpret);
                                                                            $('select[name=card_language]').val(data.data.language);

                                                                            $('#form_datetime1').val(data.data.card_origchg + ' - ' + data.data.card_origdt);
                                                                            $('#form_datetime2').val(data.data.card_lastchg + ' - ' + data.data.card_lastdt);
                                                                            $('#form_datetime3').val(data.data.card2_origchg + ' - ' + data.data.card2_origdt);
                                                                            $('#form_datetime4').val(data.data.card2_lastchg + ' - ' + data.data.card2_lastdt);

                                                                            if (notes_comments != '')
                                                                                $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                                                            else
                                                                                $('textarea[name=card_comments]').val();
                                                                            $('textarea[name=card2_comments]').val(data.data.card2_comments);
                                                                            $('textarea[name=card2_mailing1]').val(data.data.mailing1);
                                                                            $('textarea[name=card2_mailing2]').val(data.data.mailing2);
                                                                            $('textarea[name=card2_mailing3]').val(data.data.mailing3);
                                                                            $('textarea[name=card2_mailing4]').val(data.data.mailing4);

                                                                            $('#new_case').val('parties');
                                                                            $('input[name="card_cardcode"]').val(data.data.cardcode);

                                                                            $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                            $("#addcontact").modal('show');
                                                                            $("#addRolodexForm select").trigger('change');
                                                                            $.unblockUI();
                                                                        }
                                                                    });

                                                                } else
                                                                {
                                                                    $('#addcontact .modal-title').html('Edit Contact');
                                                                    $('#card_salutation').val(data.data.salutation);
                                                                    $('#card_suffix').val(data.data.suffix);
                                                                    $('select[name=card_title]').val(data.data.title);
                                                                    $('#card_type').val(data.data.type);
                                                                    $('input[name=card_first]').val(data.data.first);
                                                                    $('input[name=card_middle]').val(data.data.middle);
                                                                    $('input[name=card_last]').val(data.data.last);
                                                                    $('input[name=card_letsal]').val(data.data.letsal);
                                                                    $('input[name=card_occupation]').val(data.data.occupation);
                                                                    $('input[name=card_employer]').val(data.data.employer);
                                                                    $('input[name=hiddenimg]').val(data.data.picture);
                                                                    if (data.data.picture)
                                                                    {
                                                                        document.getElementById('profilepicDisp').style.display = 'block';
                                                                        document.getElementById('dispProfile').src = data.data.picturePath;
                                                                    } else
                                                                    {
                                                                        document.getElementById('dispProfile').src = '';
                                                                    }
                                                                    $('input[name=card2_firm]').val(data.data.firm);
                                                                    $('input[name=card2_address1]').val(data.data.address1);
                                                                    $('input[name=card2_address2]').val(data.data.address2);
                                                                    $('input[name=card2_city]').val(data.data.city);
                                                                    $('input[name=card2_state]').val(data.data.state);
                                                                    $('input[name=card2_zip]').val(data.data.zip);
                                                                    $('input[name=card2_venue]').val(data.data.venue);
                                                                    $('input[name=card2_eamsref]').val(data.data.eamsref);
                                                                    $('input[name=card_business]').val(data.data.business);
                                                                    $('input[name=card_fax]').val(data.data.card_fax);
                                                                    $('input[name=card_home]').val(data.data.home);
                                                                    $('input[name=card_car]').val(data.data.car);
                                                                    $('input[name=card_email]').val(data.data.email);
                                                                    $('input[name=card2_phone1]').val(data.data.phone1);
                                                                    $('input[name=card2_phone2]').val(data.data.phone2);
                                                                    $('input[name=card2_tax_id]').val(data.data.tax_id);
                                                                    $('input[name=card2_fax]').val(data.data.card2_fax);
                                                                    $('input[name=card2_fax2]').val(data.data.fax2);
                                                                    $('input[name=card_social_sec]').val(data.data.social_sec);
                                                                    if (data.data.birth_date == 'NA') {
                                                                        $('input[name=card_birth_date]').val('');
                                                                    } else {
                                                                        $('input[name=card_birth_date]').val(data.data.birth_date);
                                                                    }

                                                                    $('input[name=card_licenseno]').val(data.data.licenseno);
                                                                    $('select[name=card_specialty]').val(data.data.specialty);
                                                                    $('input[name=card_mothermaid]').val(data.data.mothermaid);
                                                                    $('input[name=card_cardcode]').val(data.data.cardcode);
                                                                    $('input[name=card_firmcode]').val(data.data.firmcode);
                                                                    $('select[name=card_interpret]').val(data.data.interpret);
                                                                    $('select[name=card_language]').val(data.data.language);

                                                                    $('#form_datetime1').val(data.data.card_origchg + ' - ' + data.data.card_origdt);
                                                                    $('#form_datetime2').val(data.data.card_lastchg + ' - ' + data.data.card_lastdt);
                                                                    $('#form_datetime3').val(data.data.card2_origchg + ' - ' + data.data.card2_origdt);
                                                                    $('#form_datetime4').val(data.data.card2_lastchg + ' - ' + data.data.card2_lastdt);
                                                                    $('textarea[name=card_comments]').val(data.data.card_comments);
                                                                    $('textarea[name=card2_comments]').val(data.data.card2_comments);
                                                                    $('textarea[name=card2_mailing1]').val(data.data.mailing1);
                                                                    $('textarea[name=card2_mailing2]').val(data.data.mailing2);
                                                                    $('textarea[name=card2_mailing3]').val(data.data.mailing3);
                                                                    $('textarea[name=card2_mailing4]').val(data.data.mailing4);

                                                                    $('#new_case').val('parties');
                                                                    $('input[name="card_cardcode"]').val(data.data.cardcode);

                                                                    $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                    $("#addcontact").modal('show');
                                                                    $("#addRolodexForm select").trigger('change');
                                                                    $.unblockUI();
                                                                }
                                                            }
                                                        });

                                                    });
                                                    $(document.body).on('click', '.rolodex-company', function () {
                                                        $("#rolodex_search_text_name").val("");
                                                        $("#firmcompanylist").attr("style", "z-index:9999 !important");
                                                        $("#rolodexsearchResult_data  tbody").remove();
                                                        rolodexSearch.destroy();
                                                        $('#firmcompanylist').modal('show');
                                                        $('#rolodexsearchResult_data').DataTable().clear();
                                                    });
                                                    $(document.body).on('keyup', '#rolodex_search_text_name', function (e) {
                                                        if (e.keyCode == '13') {
                                                            $('#rolodex_search_text_name').trigger("blur");
                                                            $('#rolodex_search_text_name').trigger("focus");
                                                        }
                                                    });
                                                    $(document.body).on('blur', '#rolodex_search_text_name', function (e) {
                                                        if (e.type == 'focusout' || e.keyCode == '13')
                                                        {
                                                            if ($('#companylistForm').valid())
                                                            {
                                                                if ($('#rolodex_search_text_name').val().trim().length > 0)
                                                                {
                                                                    $.ajax({
                                                                        url: '<?php echo base_url(); ?>cases/searchForRolodex',
                                                                        method: 'POST',
                                                                        data: {rolodex_search_parties: $('#rolodex_search_parties_name').val(), rolodex_search_text: $(this).val()},
                                                                        beforeSend: function (xhr) {
                                                                            $.blockUI();
                                                                        },
                                                                        complete: function (jqXHR, textStatus) {
                                                                            $.unblockUI();
                                                                        },
                                                                        success: function (data) {
                                                                            rolodexSearch.clear();

                                                                            var obj = $.parseJSON(data);
                                                                            $.each(obj, function (index, item) {
                                                                                var html = '';
                                                                                html += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + item.cardcode + "><td>" + item.firm + "</td>";
                                                                                html += "<td>" + item.city + "</td>";
                                                                                html += "<td>" + item.address + "</td>";
                                                                                html += "<td>" + item.phone + "</td>";
                                                                                html += "<td>" + item.name + "</td>";
                                                                                html += "</tr>";
                                                                                rolodexSearch.row.add($(html));
                                                                            });
                                                                            rolodexSearch.draw();
                                                                        }
                                                                    });
                                                                } else
                                                                {
                                                                    swal({
                                                                        title: "Alert",
                                                                        text: "Please enter Firm name.",
                                                                        type: "warning"
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    });
                                                    $('#rolodexsearchResult_data tbody').on('click', 'tr', function () {
                                                        if ($(this).hasClass('table-selected')) {
                                                            $(this).removeClass('table-selected');
                                                        } else {

                                                            $('#rolodexsearchResult_data tr').removeClass('table-selected');
                                                            $(this).addClass('table-selected');
                                                        }
                                                    });
                                                    $(document.body).on('click', '#viewSearchedPartieDetails', function () {

                                                        var selectedrows = rolodexSearch.rows('.table-selected').data();
                                                        var caseNo = $("#caseNo").val();
                                                        if (selectedrows > 0) {
                                                            $('#firmcompanylist').modal('hide');
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                                                                method: 'POST',
                                                                dataType: 'json',
                                                                data: {
                                                                    cardcode: $(row).attr('data-cardcode'),
                                                                    caseno: caseNo
                                                                },
                                                                beforeSend: function (xhr) {
                                                                    $.blockUI();
                                                                },
                                                                complete: function (jqXHR, textStatus) {
                                                                    $.unblockUI();
                                                                },
                                                                success: function (data) {
                                                                    if (data == false) {
                                                                        swal({
                                                                            title: "Alert",
                                                                            text: "Please attach the existing firm from the firm list to continue...",
                                                                            type: "warning"
                                                                        });
                                                                    } else {
                                                                        var city = data.city;
                                                                        if (city != null)
                                                                        {
                                                                            var strarray = city.split(',');
                                                                            $('input[name="card2_city"]').val($.trim(strarray[0]));
                                                                            $('input[name="card2_state"]').val($.trim(strarray[1]));
                                                                            $('input[name="card2_zip"]').val($.trim(strarray[2]));
                                                                        }
                                                                        $('input[name="card2_firm"]').val(data.firm);
                                                                        $('input[name="card2_address1"]').val(data.address1);
                                                                        $('input[name="card2_address2"]').val(data.address2);
                                                                        $('input[name=card_firmcode]').val(data.firmcode);
                                                                        $('input[name="card2_venue"]').val(data.venue);
                                                                        $('.addSearchedParties').attr("disabled", false);
                                                                        $('.editPartyCard').attr("disabled", false);
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            swal({
                                                                title: "Alert",
                                                                text: "Please attach the existing firm from the firm list to continue...",
                                                                type: "warning"
                                                            });
                                                        }

                                                    });

                                                    $(document.body).on('click', '#caseactivityevent', function () {
                                                        var row = categoryeventlist.row('.table-selected').node();
                                                        $('#eventnotelist').modal('hide');
                                                        $('#activity_event').val($(row).attr('data-event'));
                                                    });

                                                    $('#addcase.modal').on('hidden.bs.modal', function () {
                                                        $(this).find('input[type="text"]').each(function () {
                                                            this.value = '';
                                                        });
                                                    });

                                                    $('#firmcompanylist').on('hidden.bs.modal', function () {

                                                        $("#rolodex_search_text_name").val("");
                                                        $('tr.table-selected').removeClass('table-selected');
                                                        $(this).find('input[type="text"]').each(function () {
                                                            this.value = '';
                                                        });
                                                        $('#rolodexsearchResult_data').dataTable().fnClearTable();
                                                    });

                                                    $('#addparty').on('hidden.bs.modal', function () {
                                                        $(this).find('input[type="text"]').each(function () {
                                                            this.value = '';
                                                        });

                                                    });

                                                    $('#addtask').on('hidden.bs.modal', function () {

                                                        $(this).find('input,textarea').each(function () {
                                                            this.value = '';
                                                            $(this).iCheck('uncheck');
                                                            $(this).trigger('change');
                                                        });

                                                    });

                                                    $(document.body).on('click', '#addtaskModal', function () {

                                                        $('.select2Class#whofrom').val('<?php echo isset($loginUser) ? $loginUser : ''; ?>').trigger('change');
                                                        $('.select2Class#whoto').val('AEB').trigger('change');
                                                        $('.select2Class#priority').val('M').trigger('change');
                                                        $('.select2Class#color').val('1').trigger('change');

                                                        $('#addtask').modal({backdrop: 'static', keyboard: false});
                                                        $("#addtask").modal('show');
                                                    });



                                                    $(document.body).on('click', '.injury_print', function () {
                                                        var injuryId = $(this).attr('id');
                                                        $('#injury_Print').modal('show');
                                                        $('#injuryId').val(injuryId);
                                                    });
                                                    $(document.body).on('click', '.print_injury_data', function () {
                                                        var form_data_type = $('input[name=form_data_type]:checked').val();
                                                        var injuryId = $('#injuryId').val();
                                                        var caseno = $('#hdnCaseId').val();

                                                        $.ajax({
                                                            url: "<?php echo base_url() . 'cases/printInjuryDetails'; ?>",
                                                            method: "POST",
                                                            data: {
                                                                form_data_type: form_data_type,
                                                                injuryId: injuryId,
                                                                caseno: caseno
                                                            },
                                                            success: function (result) {
                                                                $('#print_injures_form').modal('hide');
                                                                $(result).printThis({
                                                                    debug: false,
                                                                });
                                                            }
                                                        });
                                                    });
                                                    function caseDetails(cardCode)
                                                    {
                                                        $.ajax({
                                                            url: "<?php echo base_url() . 'cases/getCasrcardDetails'; ?>",
                                                            method: "POST",
                                                            data: {
                                                                cardCode: cardCode
                                                            },
                                                            success: function (result) {
                                                                $('#viewAllCases').html(result);
                                                                $("#caseCardDetails").modal('show');
                                                            }
                                                        });
                                                    }


                                                    function fileCheck(obj) {
                                                        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                                                        if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1)
                                                        {
                                                            document.getElementById('imgerror').innerHTML = "Only '.jpeg','.jpg', '.png', '.gif', '.bmp' formats are allowed.";
                                                            return false;
                                                        }

                                                    }

                                                    $(document.body).on('click', '.clear_form', function () {
                                                        $('#calendarForm').find('input,textarea,select').each(function () {
                                                            this.value = '';
                                                            $(this).iCheck('uncheck');
                                                            $(this).trigger('change');
                                                        });
                                                    });

                                                    $('#firmcompanylist').on('show.bs.modal', function () {
                                                        $(".dataTables-example").removeAttr("style");
                                                        $(".dataTables_scrollHeadInner").removeAttr("style");
                                                        $(".dataTables-example").css("width", "100% !important;");
                                                        $(".dataTables_scrollHeadInner").css("width", "100% !important;");
                                                    });
                                                    $('#addcaseact.modal').on('hidden.bs.modal', function () {
                                                        $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                                                            this.value = '';
                                                        });
                                                        $('#addcaseact a[href="#tab-26"]').tab('show');
                                                    });
                                                    $(document.body).on('click', '.CaseActEmail', function () {
                                                        var activity_event = $('#activity_event').val();
                                                        if (CKEDITOR.instances.mailbody1)
                                                            CKEDITOR.instances.mailbody1.destroy();
                                                        CKEDITOR.replace('mailbody1');
                                                        $('#addcaseact').modal('hide');

                                                        $('#CaseEmailModal').modal('show');
                                                        $('#mailbody1').val(activity_event);

                                                    });
                                                    $(document.body).on('click', '.CaseActPrint', function () {
                                                        $.ajax({
                                                            url: "<?php echo base_url() . 'cases/printpde'; ?>",
                                                            method: "POST",
                                                            data: {caseno: $('#hdnCaseId').val()},
                                                            success: function (result) {

                                                                var based = "<?php echo base_url(); ?>" + 'assets/printCaseAct/' + result;
                                                                window.open(based, '_blank');
                                                            }
                                                        });

                                                    });


                                                    $('#ProspectListDatatable tbody').on('click', 'tr', function () {

                                                        if ($(this).hasClass('table-selected')) {
                                                            $(this).removeClass('table-selected');
                                                        } else
                                                        {
                                                            ProspectListDatatable.$('tr.table-selected').removeClass('table-selected');
                                                            $(this).addClass('table-selected');
                                                            var row = ProspectListDatatable.row('.table-selected').node();
                                                            prospect_detail(row);

                                                        }
                                                    });
                                                    $(document.body).on('click', '#addTask2', function () {

                                                        var rdate1 = $(".rdate1").val();
                                                        var taskdatepicker2 = $('#taskdatepicker2').val();

                                                        if (taskdatepicker2 >= rdate1)
                                                        {
                                                            console.log('Yes');
                                                        } else
                                                        {
                                                            $('.error').html('Must be greater than Reminder Date.');
                                                            return false;
                                                        }
                                                        if ($('#addTaskForm1').valid()) {
                                                            $.blockUI();
                                                            var url = '';
                                                            if ($('input[name=taskid]').val() != '' && $('input[name=taskid]').val() != 0) {
                                                                url = '<?php echo base_url(); ?>tasks/editTask';
                                                            } else {
                                                                url = '<?php echo base_url(); ?>tasks/addTask';
                                                            }
                                                            $.ajax({
                                                                url: url,
                                                                method: "POST",
                                                                data: $('#addTaskForm1').serialize() + '&prosno=' + $('.task_list').attr('data-proskey_id'),
                                                                success: function (result) {
                                                                    var data = $.parseJSON(result);
                                                                    $.unblockUI();
                                                                    if (data.status == '1') {
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: data.message,
                                                                            type: "success"
                                                                        },
                                                                                function ()
                                                                                {
                                                                                    $('#addTaskForm1')[0].reset();

                                                                                    $("select[name=typetask]").select2('val', 'Task Type');
                                                                                    $("select[name=typetask]").val('').trigger("change");

                                                                                    $("select[name=phase]").select2('val', 'Select Phase');
                                                                                    $("select[name=phase]").val('').trigger("change");

                                                                                    $("select[name=phase]").val('').trigger("change");

                                                                                    $(".select2Class#color").val($(".select2Class#color option:eq(0)").val()).trigger("change");


                                                                                    $(".select2Class#priority").val($(".select2Class#priority option:eq(0)").val()).trigger("change");


                                                                                    $(".select2Class#whoto3").val($(".select2Class#whoto3 option:eq(0)").val()).trigger("change");

                                                                                    $('#addtask1').modal('hide');
                                                                                    $('.rmdt1').css('display', 'none');
                                                                                    $('#addtask1 .modal-title').html('Add a Task');
                                                                                    taskTable.draw(false);
                                                                                    taskTable1.draw(false);
                                                                                });
                                                                    } else {
                                                                        swal("Oops...", data.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                    $('#addtask1.modal').on('hidden.bs.modal', function () {
                                                        $('#addtask1 h4.modal-title').text('Add a Task');
                                                        $('label.error').remove();

                                                        $("select[name=typetask]").select2('val', 'Task Type');
                                                        $("select[name=typetask]").val('').trigger("change");

                                                        $("select[name=phase]").select2('val', 'Select Phase');
                                                        $("select[name=phase]").val('').trigger("change");

                                                        $("select[name=phase]").val('').trigger("change");

                                                        $(".select2Class#color").val($(".select2Class#color option:eq(0)").val()).trigger("change");

                                                        $(".select2Class#priority").val($(".select2Class#priority option:eq(0)").val()).trigger("change");

                                                        $(".select2Class#whoto3").val($(".select2Class#whoto3 option:eq(0)").val()).trigger("change");


                                                        $(this).find('input[type="text"],input[type="email"],textarea').each(function () {
                                                            this.value = '';
                                                            $(this).removeClass('error');
                                                        });

                                                        $('input[name=taskid]').val(0);
                                                        $('#addtask1 a[href="#name"]').tab('show');
                                                    });
                                                    $('#proceedprospects.modal').on('hidden.bs.modal', function () {
                                                        $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                                                            this.value = '';
                                                        });
                                                        $('#prospectForm').validate().resetForm();
                                                        $('#prospectForm').find('.error').removeClass('error');
                                                        $('#proceedprospects .tab-content div#tab-6').addClass('active');
                                                    });

                                                    $('#CaseEmailModal.modal').on('hidden.bs.modal', function () {

                                                        $(this).find('input[type="email"],textarea,select,file').each(function () {
                                                            this.value = '';
                                                        });
                                                        $('#case_category1').val('1');
                                                        $('#mail_subject1').val('');
                                                        $('#emails1').val('');
                                                        $("#send_mail_form1 select").trigger('change');
                                                        var $el = $('#afiles1');
                                                        $el.wrap('<form>').closest('form').get(0).reset();
                                                        $el.unwrap();
                                                    });

                                                    $(document.body).on('change', '#activity_fee', function () {
                                                        if ($(this).val() == 'OTHER') {
                                                            $("#activity_fee1").show();
                                                        } else {
                                                            $("#activity_fee1").hide();
                                                            $("#activity_fee1").val('');
                                                        }

                                                    });
                                                    $(document.body).on('blur', '#case_act_time', function () {
                                                        var a = eval($(this).val());
                                                        $('#case_act_time').val(a);
                                                        var hour = $('#case_act_hour_rate').val();
                                                        if (hour > 0) {
                                                            var cost = parseInt(a * hour) / parseInt(60);
                                                            $('#case_act_cost_amt').val(cost);
                                                        }
                                                    });
                                                    $(document.body).on('blur', '#case_act_hour_rate', function () {
                                                        var a = eval($(this).val());

                                                        var hour = $('#case_act_time').val();
                                                        if (hour > 0) {
                                                            var cost = parseInt(a * hour) / parseInt(60);

                                                            $('#case_act_cost_amt').val(cost);
                                                        }
                                                    });
                                                    var regExp = /[a-z]/i;
                                                    function validate(e) {
                                                        var value = String.fromCharCode(e.which) || e.key;

                                                        if (regExp.test(value)) {
                                                            e.preventDefault();
                                                            return false;
                                                        }
                                                    }

                                                    function remove_error(id)
                                                    {
                                                        var myElem = document.getElementById(id + '-error');
                                                        if (myElem === null)
                                                        {

                                                        } else
                                                        {
                                                            document.getElementById(id + '-error').innerHTML = '';
                                                        }
                                                    }

                                                    function SubmitForm()
                                                    {
                                                        if ($("#add_new_case_activity").valid()) {
                                                            return true;
                                                        } else {
                                                            return false;
                                                        }
                                                    }
                                                    $('#addcaseact').on('hidden.bs.modal', function () {
                                                        var $alertas = $('#add_new_case_activity');
                                                        $alertas.validate().resetForm();
                                                        $alertas.find('.error').removeClass('error');
                                                    });
                                                    $(document.body).on('click', '.calnote', function () {
                                                        $('textarea[name=notes]').val('<?php echo isset($system_caldata[0]->calnotes) ? $system_caldata[0]->calnotes : ''; ?>');

                                                    });
                                                    $(document.body).on('click', '#printProspectdetails', function () {
                                                        $('#printProspectmodal').modal('show');
                                                    });
                                                    $(document.body).on('change', '.weekendsalert', function () {
                                                        var weekalert = "<?php echo isset($holidayset->weekends) ? $holidayset->weekends : 0 ?>";
                                                        if (weekalert != 0 && weekalert == 'on') {
                                                            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                                            var a = new Date($(this).val());
                                                            if (weekday[a.getDay()] == 'Saturday' || weekday[a.getDay()] == 'Sunday')
                                                            {
                                                                swal({
                                                                    title: "",
                                                                    text: "You are trying to add a task on non business day. Are you sure?",
                                                                    customClass: 'swal-wide',
                                                                    confirmButtonColor: "#1ab394",
                                                                    confirmButtonText: "OK"
                                                                });
                                                            }
                                                        }

                                                    });
                                                    $(document.body).on('change click', '.alerttonholiday', function () {
                                                        var aletholi = $(this).val();
                                                        var aftholiday = "<?php echo isset($holidayset->aftholiday) ? $holidayset->aftholiday : 0 ?>";
                                                        if (aftholiday != 0 && aftholiday == 'on' && $(this).val() != '') {
                                                            var holidaylist = '<?php echo isset($holodaylist) ? json_encode($holodaylist) : 0; ?>';
                                                            if (JSON.parse(holidaylist).length > 0) {
                                                                $.each(JSON.parse(holidaylist), function (key, value) {
                                                                    var temp = new Array();
                                                                    temp = value.split(",");
                                                                    var c = new Date(temp[0]);
                                                                    var b = new Date(aletholi);
                                                                    if (c.getTime() == b.getTime()) {

                                                                        swal({
                                                                            title: "",
                                                                            text: "You are trying to add a task on non business day. Are you sure?",
                                                                            customClass: 'swal-wide',
                                                                            confirmButtonColor: "#1ab394",
                                                                            confirmButtonText: "OK"
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        }

                                                    });

                                                    $(document.body).on('change click', '.birthcalalert', function () {
                                                        var bialaddcalevent = "<?php echo isset($main1->bialaddcalevent) ? $main1->bialaddcalevent : 0 ?>";
                                                        if (bialaddcalevent != 0 && bialaddcalevent == 'on') {
                                                            var birthdate = "<?php echo isset($display_details->birth_date) ? date('m/d/Y', strtotime($display_details->birth_date)) : 0 ?>";
                                                            if (birthdate != '' && birthdate != 0)
                                                            {
                                                                if ($(this).val() != '') {
                                                                    var b = new Date(birthdate);
                                                                    var y = new Date();
                                                                    var year = y.getFullYear();
                                                                    var month = b.getMonth();
                                                                    var day = b.getDate();
                                                                    var c = new Date(year, month, day);
                                                                    var b = new Date($(this).val());
                                                                    if (b.getTime() === c.getTime())
                                                                    {
                                                                        swal({
                                                                            title: "Notice",
                                                                            text: "You are scheduling this appointment on the client's birthday",
                                                                            confirmButtonColor: "#1ab394",
                                                                            confirmButtonText: "OK"
                                                                        });
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    });
                                                    $(document.body).on('change click', '.staffvacation', function () {
                                                        var Uservacation = '<?php echo isset($Uservacation) ? json_encode($Uservacation) : 0; ?>';

                                                        if ($('.staffTO').val() != "" && JSON.parse(Uservacation).length > 0 && $('.staffTO').val() == '<?php echo $this->session->userdata('user_data')['username'] ?>' && $('.sfinishby').val() != '') {
                                                            $.each(JSON.parse(Uservacation), function (key, value) {
                                                                var temp = new Array();
                                                                temp = value.split(",");
                                                                if (temp[0].includes("-") == true) {
                                                                    var result = temp[0].split('-');
                                                                    var dateFrom = result[0];
                                                                    var dateTo = result[1];
                                                                    var dateCheck = $('.sfinishby').val();
                                                                    var from = Date.parse(dateFrom);
                                                                    var to = Date.parse(dateTo);
                                                                    var check = Date.parse(dateCheck);

                                                                    if ((check <= to && check >= from))
                                                                        swal({
                                                                            title: "",
                                                                            text: temp[1],
                                                                            customClass: 'swal-wide',
                                                                            confirmButtonColor: "#1ab394",
                                                                            confirmButtonText: "OK",
                                                                            timer: 2000
                                                                        });
                                                                } else {
                                                                    var c = new Date(temp[0]);
                                                                    var b = new Date($('.sfinishby').val());
                                                                    if (c.getTime() == b.getTime()) {
                                                                        swal({
                                                                            title: "",
                                                                            text: temp[1],
                                                                            customClass: 'swal-wide',
                                                                            confirmButtonColor: "#1ab394",
                                                                            confirmButtonText: "OK"
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });

                                                    $(document.body).on('click', '.activity_view', function () {
                                                        var fname = $(this).data('actid');
                                                        viewActDoc(fname);
                                                    });
                                                    $('.discard_message').click(function (e) {
                                                        swal({
                                                            title: "Are you sure?",
                                                            text: "The changes you made will be lost",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, discard it!",
                                                            closeOnConfirm: true
                                                        }, function () {
                                                            $('#CaseEmailModal').find('.att-add-file').not(':first').remove();
                                                            $('#CaseEmailModal').modal("hide");
                                                        });
                                                    });
                                                    /*        $('.confirm').on('click',function(){
                                                     $('#saveCompletedTask').removeAttr('disabled');
                                                     });*/

                                                    $(document).on('click', '.ca-addfile', function () {
                                                        $('.att-add-file:last').after('<label class="col-sm-2 control-label"></label><div class="col-sm-9 att-add-file marg-top5 clearfix"> <input type="file" name="afiles1[]" id="afiles1" multiple ><i class="fa fa-plus-circle marg-top5 ca-addfile" aria-hidden="true"></i> <i class="fa fa-times marg-top5 removefile" aria-hidden="true"></i></div>');
                                                    });
                                                    $(document).on('click', '.removefile', function () {
                                                        $(this).closest('.att-add-file').prev('label').remove();
                                                        $(this).closest('.att-add-file').remove();
                                                    });

                                                    $('#printProspectmodal.modal').on('hidden.bs.modal', function () {
                                                        $(this).find('input,textarea,select').each(function () {
                                                            this.value = '';
                                                            $(this).iCheck('uncheck');
                                                            $(this).trigger('change');
                                                        });
                                                    });

                                                    function viewActDoc(fname) {
                                                        var caseNo = $("#caseNo").val();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/viewcaseactdoc',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                filename: fname,
                                                                caseno: caseNo
                                                            },
                                                            success: function (data) {

                                                            }
                                                        });
                                                    }


                                                    function showhidedt1()
                                                    {
                                                        if ($('input[type="checkbox"][name="reminder1"]').is(':checked') == true)
                                                        {
                                                            $('input[type="checkbox"][name="reminder1"]').val('on');
                                                            $('.rmdt1').show();
                                                        } else
                                                        {
                                                            $('.rmdt1').hide();
                                                            $('.rdate').val('');
                                                            $('.rtime').val('');
                                                        }
                                                    }

                                                    function showhidedt()
                                                    {
                                                        if ($('input[type="checkbox"][name="reminder"]').is(':checked') == true)
                                                        {
                                                            $('input[type="checkbox"][name="reminder"]').val('on');
                                                            $('.rmdt').show();
                                                        } else
                                                        {
                                                            $('.rmdt').hide();
                                                            $('.rdate').val('');
                                                            $('.rtime').val('');
                                                        }
                                                    }

                                                    $('#gettaskStatus').on('change', function () {
                                                        var taskStatus = $(this).val();
                                                        taskTable1.columns(8).search(JSON.stringify({'task-status': taskStatus})).draw();
                                                    });
                                                    $('#gettaskStatus').on('change', function () {
                                                        var taskStatus = $(this).val();
                                                        taskTable1.columns(8).search(JSON.stringify({'task-status': taskStatus})).draw();
                                                    });


                                                    function isValidDate(dateStr) {
                                                        var datePat = /^(\d{2,2})(\/)(\d{2,2})\2(\d{4}|\d{4})$/;

                                                        var matchArray = dateStr.match(datePat);
                                                        if (matchArray == null) {
                                                            return false;
                                                        }

                                                        month = matchArray[1];
                                                        day = matchArray[3];
                                                        year = matchArray[4];
                                                        if (month < 1 || month > 12) {
                                                            return false;
                                                        }
                                                        if (day < 1 || day > 31) {
                                                            return false;
                                                        }
                                                        if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
                                                            return false;
                                                        }
                                                        if (month == 2) {
                                                            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                                                            if (day > 29 || (day == 29 && !isleap)) {
                                                                return false;
                                                            }
                                                        }
                                                        return true;
                                                    }
                                                    $(document.body).on('click', 'div#addcontact a[data-toggle="tab"]', function (e) {
                                                        if (!$('#addRolodexForm').valid()) {
                                                            e.preventDefault();
                                                            return false;
                                                        }
                                                    });
                                                    $(document.body).on('blur', 'input[name=card2_state]', function () {
                                                        var add1 = $('#street_number').val();
                                                        var add2 = $('#route').val();
                                                        var city = $('#locality').val();
                                                        var state = $(this).val();
                                                        if (state != '') {
                                                            rolodexSearch.clear();
                                                            $("#firmcompanylist").attr("style", "z-index:9999 !important");
                                                            $('#firmcompanylist').modal('show');
                                                            rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                                                                processing: true,
                                                                rolodexsearchAddress: true,
                                                                serverSide: true,
                                                                bFilter: false,
                                                                scrollX: true,
                                                                sScrollY: 400,
                                                                bScrollCollapse: true,
                                                                destroy: true,
                                                                autoWidth: true,
                                                                "lengthMenu": [5, 10, 25, 50, 100],
                                                                order: [[0, "ASC"]],
                                                                ajax: {
                                                                    url: '<?php echo base_url(); ?>cases/searchForRolodexAddress',
                                                                    method: 'POST',
                                                                    data: {add1: add1, add2: add2, city: city, state: state},
                                                                },
                                                                fnRowCallback: function (nRow, data) {
                                                                    $(nRow).attr("cardcode", '' + data[8]);
                                                                    $(nRow).addClass('viewsearchPartiesDetail1');
                                                                },
                                                                fnDrawCallback: function (oSettings) {
                                                                },
                                                                initComplete: function (settings, json) {

                                                                },
                                                                select: {
                                                                    style: 'single'
                                                                }
                                                            });
                                                            /*$.ajax({
                                                             url: '<?php //echo base_url(); ?>cases/searchForRolodexAddress',
                                                             method: 'POST',
                                                             data: {add1:add1,add2:add2,city:city,state:state},
                                                             beforeSend: function (xhr) {
                                                             $.blockUI();
                                                             },
                                                             complete: function (jqXHR, textStatus) {
                                                             $.unblockUI();
                                                             },
                                                             success: function (data) {
                                                             rolodexSearch.clear();
                                                             $("#firmcompanylist").attr("style", "z-index:9999 !important");
                                                             $('#firmcompanylist').modal('show');
                                                             var obj = $.parseJSON(data);
                                                             $.each(obj, function (index, item) {
                                                             var html = '';
                                                             html += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + item.cardcode + "><td>" + item.firm + "</td>";
                                                             html += "<td>" + item.city + "</td>";
                                                             html += "<td>" + item.address + "</td>";
                                                             html += "<td>" + item.address2 + "</td>";
                                                             html += "<td>" + item.phone + "</td>";
                                                             html += "<td>" + item.name + "</td>";
                                                             html += "<td>" + item.state + "</td>";
                                                             html += "<td>" + item.zip + "</td>";
                                                             html += "</tr>";
                                                             rolodexSearch.row.add($(html));
                                                             });
                                                             rolodexSearch.draw();
                                                             }
                                                             });*/
                                                        }
                                                    });

                                                    function ChangeRolodex() {
                                                        var cardcode = $('#caseCardCode').val();
                                                        var caseno = $('#caseNo').val();
                                                        $('#show_conform_model').modal('hide');
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>rolodex/getEditRolodex',
                                                            data: {cardcode: cardcode, caseNO: caseno, status: 'Yes'},
                                                            method: "POST",
                                                            success: function (result) {
                                                                var data = $.parseJSON(result);
                                                                var notes_comments = '';
                                                                if (data.card_comments) {
                                                                    if(isJson(data.card_comments)) {
                                                                        notes_comments = $.parseJSON(data.card_comments);
                                                                    } else {
                                                                        notes_comments = data.card_comments;
                                                                    }
                                                                }
                                                                swal.close();
                                                                if (data.status == '0')
                                                                {
                                                                    var name = '';
                                                                    if (data.data.type == 'APPLICANT')
                                                                    {
                                                                        name = data.data.last + ', ' + data.data.first;
                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                    } else if (data.data.type == 'LIEN' || data.data.type == 'BOARD' || data.data.type == 'INSURANCE' || data.data.type == 'ATTORNEY' || data.data.type == 'EMPLOYER')
                                                                    {
                                                                        name = data.data.firm;
                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                    } else if (data.data.type == 'DR') {
                                                                        if (data.data.firm.length == 0) {
                                                                            name = data.data.last + ', ' + data.data.first;
                                                                            $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                        } else {
                                                                            name = data.data.firm;
                                                                            $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                                                        }
                                                                    } else
                                                                    {
                                                                        name = data.data.last + ', ' + data.data.first;
                                                                        $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                                                    }
                                                                    $('#rolodex_search_text').val(name);
                                                                    $("#rolodex_search_parties_form").submit();
                                                                    $("#addparty").modal('show');
                                                                } else
                                                                {
                                                                    $('#addcontact .modal-title').html('Edit Contact');
                                                                    $('#card_salutation').val(data.salutation);
                                                                    $('#card_suffix').val(data.suffix);
                                                                    $('select[name=card_title]').val(data.title);
                                                                    $('#card_type').val(data.type);
                                                                    $('input[name=card_first]').val(data.first);
                                                                    $('input[name=card_middle]').val(data.middle);
                                                                    $('input[name=card_last]').val(data.last);
                                                                    $('input[name=card_popular]').val(data.popular);
                                                                    $('input[name=card_letsal]').val(data.letsal);
                                                                    $('input[name=card_occupation]').val(data.occupation);
                                                                    $('input[name=card_employer]').val(data.employer);
                                                                    $('input[name=hiddenimg]').val(data.picture);
                                                                    if (data.picture)
                                                                    {
                                                                        document.getElementById('profilepicDisp').style.display = 'block';
                                                                        document.getElementById('dispProfile').src = data.picturePath;
                                                                    } else
                                                                    {
                                                                        document.getElementById('dispProfile').src = '';
                                                                    }
                                                                    $('input[name=card2_firm]').val(data.firm);
                                                                    $('input[name=card2_address1]').val(data.address1);
                                                                    $('input[name=card2_address2]').val(data.address2);
                                                                    $('input[name=card2_city]').val(data.city);
                                                                    $('input[name=card2_state]').val(data.state);
                                                                    $('input[name=card2_zip]').val(data.zip);
                                                                    $('input[name=card2_venue]').val(data.venue);
                                                                    $('input[name=card2_eamsref]').val(data.eamsref);
                                                                    $('input[name=card_business]').val(data.business);
                                                                    $('input[name=card_fax]').val(data.card_fax);
                                                                    $('input[name=card_home]').val(data.home);
                                                                    $('input[name=card_car]').val(data.car);
                                                                    $('input[name=card_email]').val(data.email);
                                                                    if (notes_comments != '')
                                                                        $('input[name=card_notes]').val(notes_comments[0].notes);
                                                                    else
                                                                        $('input[name=card_notes]').val();
                                                                    $('input[name=card2_phone1]').val(data.phone1);
                                                                    $('input[name=card2_phone2]').val(data.phone2);
                                                                    $('input[name=card2_tax_id]').val(data.tax_id);
                                                                    $('input[name=card2_fax]').val(data.card2_fax);
                                                                    $('input[name=card2_fax2]').val(data.fax2);
                                                                    $('input[name=card_social_sec]').val(data.social_sec);
                                                                    if (data.birth_date == 'NA') {
                                                                        $('input[name=card_birth_date]').val('');
                                                                    } else {
                                                                        /*$('input[name=card_birth_date]').val(data.birth_date);*/
                                                                        $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                                                                    }
                                                                    $('input[name=card_licenseno]').val(data.licenseno);
                                                                    $('select[name=card_specialty]').val(data.specialty);
                                                                    $('input[name=card_mothermaid]').val(data.mothermaid);
                                                                    $('input[name=card_cardcode]').val(data.cardcode);
                                                                    $('input[name=card_firmcode]').val(data.firmcode);
                                                                    $('select[name=card_interpret]').val(data.interpret);
                                                                    $('select[name=card_language]').val(data.language).trigger("change");
                                                                    $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                                                                    $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                                                                    $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                                                                    $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                                                                    if (notes_comments != '')
                                                                        $('textarea[name=card_comments]').val(notes_comments[1].comments);
                                                                    else
                                                                        $('textarea[name=card_comments]').val();
                                                                    $('textarea[name=card2_comments]').val(data.card2_comments);
                                                                    $('textarea[name=card2_mailing1]').val(data.mailing1);
                                                                    $('textarea[name=card2_mailing2]').val(data.mailing2);
                                                                    $('textarea[name=card2_mailing3]').val(data.mailing3);
                                                                    $('textarea[name=card2_mailing4]').val(data.mailing4);
                                                                    $('#new_case').val('parties');
                                                                    $('input[name="card_cardcode"]').val(data.cardcode);
                                                                    $('#addcontact').modal({backdrop: 'static', keyboard: false});
                                                                    $("#addcontact").modal('show');
                                                                    $("#addRolodexForm select").trigger('change');
                                                                    $.unblockUI();
                                                                }
                                                            }
                                                        });
                                                    }

                                                    $(document.body).on('click', '#change_name_warning_no', function () {
                                                        $("#partiesNameChangeWarning").modal("hide");
                                                        var cardCode = $('#caseCardCode').val();
                                                        $.ajax({
                                                            url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                                                            method: "POST",
                                                            data: {'cardcode': cardCode},
                                                            success: function (result) {
                                                                if (result > 1) {
                                                                    swal({
                                                                        title: "Warning!",
                                                                        type: "warning",
                                                                        text: "This card is currently attached to " + result + " cases.\nChange to this card will affect " + result + " cases.\n\nIf you want changes to be reflected for only one case please detach that card from that case at the Parties sheet and attach another card\n\nAre you sure you want to make changes to this rolodex card? ",
                                                                        showCancelButton: true,
                                                                        confirmButtonColor: "#DD6B55",
                                                                        confirmButtonText: "Yes",
                                                                        closeOnConfirm: true
                                                                    }, function () {
                                                                        ChangeRolodex();
                                                                    });
                                                                } else
                                                                {
                                                                    ChangeRolodex();
                                                                }
                                                            }
                                                        });
                                                    });

</script>