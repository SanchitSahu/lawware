<?php
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '')
            unset($holodaylist[$key]);
    }
}
if (isset($staff_details)) {
    $Uservacation = isset($this->data['staff_details'][0]->vacation) ? (explode("##", trim($this->data['staff_details'][0]->vacation))) : '';
    if (!empty($Uservacation)) {
        $Uservacation = array_map('trim', $Uservacation);
        foreach ($Uservacation as $key => $value) {
            if (is_null($value) || $value == '')
                unset($Uservacation[$key]);
        }
    }
}
/*
 * This file contains all the Common Modals and thier related functionalities that can be used in multiple areas on website
 *
 */
?>
<style>
    .chosen-container {
        width: 100% !important;
    }
    .pac-container{z-index: 9999;}
    #eamslist{
        z-index: 9999 !important;
    }
    #duplicateroloData{
        z-index: 9999 !important;
    }


</style>

<!-- Rolodex add/edit popup START-->
<div id="addcontact" class="modal fade" data-backdrop="static" data-keyboard="false" style="z-index: 9999 !important;" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Contact</h4>
            </div>
            <div class="modal-body">
                <form id="addRolodexForm" method="post" enctype="multipart/form-data">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#name">Name</a></li>
                            <li class=""><a data-toggle="tab" href="#contact">Telephone/Email</a></li>
                            <li class=""><a data-toggle="tab" href="#details">Details</a></li>
                            <li class=""><a data-toggle="tab" href="#comments">Comments</a></li>
                            <li class=""><a data-toggle="tab" href="#address">Addresses</a></li>
                            <li class=""><a data-toggle="tab" href="#profile">Profile</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="name" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>General Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Salutation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input name="form_submit" type="hidden" value="1">
                                                            <select name="card_salutation" id="card_salutation" class="form-control <?php
                                                            if (isset($system_data[0]->cdsaldd) && $system_data[0]->cdsaldd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" >
                                                                <option value="">Select Salutation</option>
                                                                <?php foreach ($saluatation as $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">First <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="card_first" name="card_first" class="form-control" value=""  autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Middle</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_middle" class="form-control" value="" autocomplete="off" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_last" id="card_last" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Suffix</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_suffix" id="card_suffix" class="form-control <?php
                                                            if (isset($system_data[0]->cdsufdd) && $system_data[0]->cdsufdd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>">
                                                                <option value="" disabled="disabled">Select Suffix</option>
                                                                <?php
                                                                if (isset($suffix)) {
                                                                    foreach ($suffix as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Title</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_title" class="form-control select2Class" >
                                                                <option value="">Select Title</option>
                                                                <?php
                                                                if (isset($title)) {
                                                                    foreach ($title as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Category <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select  name="card_type" id="card_type" class="form-control <?php
                                                            if (isset($system_data[0]->cardtypedd) && $system_data[0]->cardtypedd == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" required placeholder="Select Category">
                                                                         <?php
                                                                         if (isset($category)) {
                                                                             foreach ($category as $val) {
                                                                                 ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Popular</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_popular" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Sal for Ltr   <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="This should normally be left empty.  However, you may sometimes decide to fill this field in to override the default salutation in form letters.  For example : To the honorable Judge Smith" id="sfl"></i></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_letsal" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Firm/Corp <span class="asterisk">*</span></label>
                                                        <div class="col-sm-6 no-padding">
                                                            <input type="text" name="card2_firm" class="form-control" value="" autocomplete="off"/></div><div class="col-sm-1 no-padding"><a href="#" class="btn btn-md btn-primary pull-right rolodex-company">A</a>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address Lookup</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input id="autocomplete" placeholder="Enter your address" class="form-control" onFocus="geolocate()" type="text">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address 1 <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_address1" id="street_number" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <div class="row">

                                                            <div class="col-lg-12">
                                                                <label class="col-sm-5 no-left-padding">City <span class="asterisk">*</span></label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" id="locality" name="card2_city" class="form-control" value="" autocomplete="off"/>
                                                                </div></div>


                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Venue</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
<!--                                                        <input type="text" name="card2_venue" class="form-control" />-->
                                                            <select class="form-control cstmdrp cstmdrp-placeholder" name="card2_venue" placeholder="Select Venue">
                                                                <option value="" disabled>Select Venue</option>
                                                                <option value=""></option>
                                                                <?php
                                                                if (isset($Venue)) {
                                                                    foreach ($Venue as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <a class="btn pull-right btn-primary btn-sm" href="#eamslist" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Add Eamsref" data-keyboard="false">EAMS</a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">EAMS Name</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
<!--                                                            <input type="text" name="card2_eamsref" class="form-control" autocomplete="off" />-->
                                                            <input type="hidden" name="card2_eamsref">
                                                            <input type="text" id="eamsref_name" name="eamsref_name" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_address2" id="route" class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">State <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="administrative_area_level_1" name="card2_state" class="form-control" value="" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Zip <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" id="postal_code" name="card2_zip" class="form-control" value="" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="contact" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Personal Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Business</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_business" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Home</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_home" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Cell</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_car" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Email</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_email" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Note</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_notes" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Business Contact Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Telephone 1</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone1" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Telephone 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_phone2" placeholder="(xxx) xxx-xxxx" class="form-control contact"  autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Tax ID</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_tax_id" class="form-control" autocomplete="off"/><!-- onlyNumbers -->
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax 2</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card2_fax2" placeholder="(xxx) xxx-xxxx" class="form-control contact" autocomplete="off"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="details" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Contact Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">SS No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_social_sec" class="form-control social_sec"  placeholder="234-56-7898" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Date of Birth</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_birth_date" pattern="\d{1,2}/\d{1,2}/\d{4}"  class="form-control datepickerClass date-field weekendsalert" data-mask="99/99/9999" placeholder="mm/dd/YY ex: 03/25/1995" id="datepicker" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">License No</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_licenseno" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Specialty</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_specialty" class="form-control select2Class">
                                                                <option value="">Select Specialty</option>
                                                                <?php
                                                                if (isset($specialty)) {
                                                                    foreach ($specialty as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Maidan Name</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_mothermaid" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Card No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_cardcode" readonly class="form-control" value="" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Firm No.</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_firmcode" readonly  class="form-control" autocomplete="off">
                                                        </div>        </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Interpreter Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Interpreter</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_interpret" class="form-control select2Class">
                                                                <option value="">Select Interpreter</option>
                                                                <option value="Y">Yes</option>
                                                                <option value="N">No</option>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>

                                                    <div class="form-group" id="data_1">
                                                        <label class="col-sm-5 no-padding">Language</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="card_language" class="select2Class form-control">
                                                                <?php foreach ($languageList as $key => $val) { ?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="float-e-margins">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="">Original</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime1" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Last</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime2" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Original2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime3" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="">Last2</label>
                                                        <div class="marg-bot5">
                                                            <input type="text" name="" class="form-control" id="form_datetime4" readonly data-date-format="mm/dd/yyyy - HH:ii p" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="comments" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Business Comments</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <textarea style="height: 150px;resize: none;" name="card2_comments" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="address" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot10">
                                        <div class="ibox-title">
                                            <h5>Alternate Name and Addresses</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address1</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing1" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address2</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing2" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name and Address3</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing3" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-left-padding">
                                                    <div class="form-group">
                                                        <label>Name and Address4</label>
                                                        <textarea style="height: 100px;resize: none;" name="card2_mailing4" type="text" class="form-control" ></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="profile" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="float-e-margins marg-bot10">
                                                <div class="ibox-title">
                                                    <h5>Personal Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Occupation</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_occupation" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Employer</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" name="card_employer" class="form-control" autocomplete="off"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Profile Picture</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="file" name="profilepic" id="profilepic" class="form-control" accept="image/*" />
                                                            <span style="color:red">Max Upload size 5Mb</span>
                                                            <input type="hidden" name="hiddenimg" id="hiddenimg" value="" >
                                                            <span id="imgerror" style="color:red"></span>
                                                        </div>
                                                        <div id="profilepicDisp" style="display:none">
                                                            <div class="form-group">
                                                                <img id="dispProfile" height="50" width="50">
                                                            </div>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <input type="hidden" id="new_case" name="new_case" />
                        <input type="hidden" id="prospect_id" name="prospect_id" />
                        <input type="hidden" id="newRolodexCard" name="newRolodexCard" value="0"/>
                        <input type="hidden" id="oldCardValue" name="oldCardValue" value="0"/>
                        <input type="hidden" id="oldCaseNo" name="oldCaseNo" value="0"/>
                        <button type="button" id="addContact" class="btn btn-md btn-primary">Save</button>
                        <button type="button" id="cancelContact" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Rolodex add/edit popup END-->

<?php
$session_Arr = $this->session->userdata('user_data');
?>
<!--Add Case Activity START-->
<div id="caseSpecific" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Party Specific Information</h4>
            </div>
            <form method="post" name="parties_case_specific_data" id="parties_case_specific_data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins">
                                <div class="">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="">Type</label>
                                            <select class="form-control" name="parties_case_specific" id="parties_case_specific">
                                                <?php
                                                if (isset($types)) {
                                                    foreach ($types as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $val->type; ?>"><?php echo $val->type; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="">Side</label>
                                            <select class="form-control" name="parites_side" id="parties_side">
                                                <?php
                                                if (isset($side)) {
                                                    foreach ($side as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="">Their Office Number</label>
                                            <input type="text" name="parties_office_number" id="parties_office_number" class="form-control contact" autocomplete="off">
                                        </div>

                                        <div class="form-group">
                                            <label class="">Show the name on the Party Sheet
                                                <input type="checkbox" value="1" name="parties_party_sheet" id="parties_party_sheet" class="i-checks"></lable>
                                                </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="">Party Notes</label>
                                                    <textarea class="form-control" rows="9" id="parties_notes" name="parties_notes"></textarea>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-sm-12 text-center">
                                <div class="">
                                    <?php $caseNo = $this->uri->segment(3); ?>
                                    <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo; ?>">
                                    <input id="partiesCardCode" name="partiesCardCode" type="hidden" value="">
                                    <button id="save_case_specific" type="submit" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
        <!-- Edit case specific information  -->


        <div id="duplicateData" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Possible Duplicate</h4>
                    </div>
                    <form method="post" name="parties_case_specific" id="parties_case_specific">
                        <div class="modal-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <table id="duplicateEntries" class="table table-bordered table-hover table-striped dataTables-example" style="width: 100%">
                                                <thead>
                                                <th>Case No</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>Date Entered</th>
                                                <th>SS No</th>
                                                <th>Card Type</th>
                                                <th>Caption</th>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input id="last_name" name="last_name" type="hidden" class="last_name" value="">
                            <input id="first_name" name="first_name" type="hidden" class="first_name"  value="">
                        </div>
                        <div class="modal-footer">
                            <div class="col-sm-12 text-center">
                                <div class="">

                                    <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $this->uri->segment(3); ?>">
                                    <input id="duplicateCardCode" name="duplicateCardCode" type="hidden" value="">
                                    <button id="add_name_case" type="submit" class="btn btn-primary">Add Name & Case</button>
                                    <button id="attach_name" type="button" class="btn btn-primary" disabled>Attach Name</button>
                                    <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                                    <button id="edit_new_case" type="button" class="btn btn-primary pull-right" disabled>Edit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- venue list end-->
        <div id="venulist_model" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Venue List</h4>
                    </div>
                    <div class="modal-body" style="display:inline-block; width:100%;">
                        <form>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive party-table1">
                                            <table class="table table-bordered table-hover table-striped" id="venue_table">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Firm</th>
                                                        <th>city</th>
                                                        <th >Address</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (isset($case_cards) && $case_cards != '') {
                                                        foreach ($case_cards as $key => $details) {
                                                            $name = $details->first . "" . $details->middle . "" . $details->last;
                                                            echo '<tr id="' . $details->cardcode . '">' .
                                                            '<td class="card_details" title="' . ($name) . '">' . $name . '</td>' .
                                                            '<td>' . $details->firm . '</td>' .
                                                            '<td>' . $details->city . '</td>' .
                                                            '<td>' . $details->address1 . '</td>' .
                                                            '</tr>';
                                                        }
                                                    } else {
                                                        ?>
                                                        <tr>
                                                            <td colspan="4">Data not found.</td>
                                                            <td style="display: none;"></td>
                                                            <td style="display: none;"></td>
                                                            <td style="display: none;"></td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 text-center">
                            <button type="button" class="btn btn-md btn-primary" id="set_venue" >Attach</button>
                            <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end venue list -->
        <!-- start firm companylist  -->
        <div id="firmcompanylist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog modal-md firmcompanylist">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Firm Company List</h4>
                    </div>
                    <div class="modal-body" style="display:inline-block; width:100%;">
                        <div class="form-group">
                            <form action="#" role="form" method="post" id="companylistForm" name="companylistForm">
                                <label>Enter Firm or Company Name</label>
                                <input type="text" class="form-control" name="rolodex_search_text_name" id="rolodex_search_text_name"/>
                                <input type="hidden" class="form-control" name="rolodex_search_parties_name" id="rolodex_search_parties_name" value="rolodex-company" />
                            </form>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped dataTables-example" id="rolodexsearchResult_data">
                                            <thead>
                                                <tr>
                                                    <th>Firm/Company</th>
                                                    <th>City</th>
                                                    <th>Address</th>
                                                    <th>Address2</th>
                                                    <th>Phone</th>
                                                    <th>Name</th>
                                                    <th>State</th>
                                                    <th>Zip</th>

                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 text-center">
                            <button type="button" class="btn btn-md btn-primary" id="viewSearchedPartieDetails" >Attach</button>
                            <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  end firmcompanylist -->
        <!-- casepartyDetaillist  -->
        <div id="casepartyDetaillist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Select A Party To Attach</h4>
                    </div>
                    <div class="modal-body" style="display:inline-block; width:100%;">
                        <!-- <form id="add_caseactivity_note" name="add_caseactivity_note"> -->
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="table-responsive rolodexsearch">
                                                        <table class="table table-bordered table-hover table-striped dataTables-example" id="casePartylist" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Firm</th>
                                                                    <th>City</th>
                                                                    <th>Address</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="text-center">
                                            <button type="button" class="btn btn-md btn-primary" id="casepartysave" >Process</button>
                                            <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                        </form>-->
                    </div>
                </div>
            </div>
        </div>
        <!---  End  casepartyDetaillist  -->
        <div id="Case-list" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span id="record_count"> </span>Found Case</h4>
                    </div>
                    <div class="modal-body">
                        <div class="ibox float-e-margins">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="ibox-content marg-bot15 party-in">
                                        <form>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="table-responsive party-table">
                                                            <table id="caselist"  class="table table-bordered table-hover table-striped" style="width:100%;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Case No</th>
                                                                        <th>Name</th>
                                                                        <th>AttyR</th>
                                                                        <th>AttyH</th>
                                                                        <th>Case Type</th>
                                                                        <th>Card Type</th>
                                                                        <th>Status</th>
                                                                        <th>File No</th>
                                                                        <th>SS No</th>
                                                                        <th>View</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="ibox-content marg-bot15 party">
                                        <form>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-12 party_showhide main_address_drp">
                                                        <div class="table-responsive">
                                                            <table class="table no-bottom-margin">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Name :</strong></td>
                                                                        <td id="case_party_name" data-cardcodeWarning=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Firm Name :</strong></td>
                                                                        <td id="case_firm_name" data-cardcodeWarning=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Card Type :</strong></td>
                                                                        <td id="case_card_type"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Phone :</strong></td>
                                                                        <td id="case_phone"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>S S No :</strong></td>
                                                                        <td id="case_ss_no"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Type of Case :</strong></td>
                                                                        <td id="case_type"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Case Status :</strong></td>
                                                                        <td id="case_status"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Follow Up :</strong></td>
                                                                        <td id="case_follow"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Date Entered :</strong></td>
                                                                        <td id="case_entered"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Your File No :</strong></td>
                                                                        <td id="case_fileno"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Staff :</strong></td>
                                                                        <td id="case_staff"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-left" width="100"><strong>Dates of Injury :</strong></td>
                                                                        <td id="case_injury"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-sm-12 text-center">
                            <div class="">
                                <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <div id="eamslist" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md eamsreflist">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EAMS List</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-eamsLookup" >
                                <thead>
                                    <tr>
                                        <th>Eamsref</th>
                                        <th>Name</th>
                                        <th>Add1</th>
                                        <th>Add2</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Zip</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn btn-md btn-primary" id="eamsrefDetails" >Attach</button>
                    <button type="button" class="btn btn-md btn-primary" id="eamsrefDetach" >Detach</button>
                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>

                </div>
            </div>
        </div>
    </div>
</div>

        <div id="duplicateroloData" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Possible Duplicate</h4>
                    </div>
                    <form method="post" name="parties_rolo_specific" id="parties_rolo_specific">
                        <div class="modal-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <table id="duplicateRoloEntries" class="table table-bordered table-hover table-striped dataTables-example" style="width: 100%">
                                                <thead>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>Social Sec No</th>
                                                <th>Firm/Company</th>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col-sm-12 text-center">
                                <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Close</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var uri_seg = '<?php echo (isset($url_seg)) ? $url_seg : ''; ?>';
            var myDropzone = "";
            var selectedArray = [];
            var rolodexSearchDatatable = '';
            var rolodexSearch = '';
            var categoryeventlist = '';
            var casepartylist_data = '';
            var venue_table_data = '';
            var caseactTable = '';
            var swalFunction = '';
            var CaseHID = $("#hdnCaseId").val();
            var ProspectListDatatable;
            var duplicateRoloDatatable = '';
            var searchBy1 = {'prosno':<?php echo isset($record[0]->proskey) ? $record[0]->proskey : 'null'; ?>};
            $(document).ready(function () {
                    $('#sfl').tooltip();
                    $('input.numberinput').bind('keypress', function (e) {
                        return !(e.which != 8 && e.which != 0 &&
                                (e.which < 48 || e.which > 57) && e.which != 46);
                    });

                    var calYearRange = $('#calYearRange').val();
                    duplicateRoloDatatable = $('#duplicateRoloEntries').DataTable();
                    eamsData = $('.dataTables-eamsLookup').DataTable({
                "processing": true,
                "serverSide": true,
                stateSave: true,
                            stateSaveParams: function (settings, data) {
                                    data.search.search = "";
                                    data.start = 0;
                                    delete data.order;
                            },
                "bFilter": true,
                "sScrollX": "100%",
                "sScrollXInner": "150%",
                "ajax": {
                    url: "<?php echo base_url(); ?>eams_lookup/getEamsAjaxData",
                    type: "POST"
                }
            });

                venue_table_data = $('#venue_table').DataTable();
                casepartylist_data = $('#casePartylist').DataTable();
                /* rolodaxdataTable = $('.dataTables-rolodex').DataTable(); */
                $('#datepicker').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                $('.contact').mask("(000) 000-0000");
                $('.social_sec').mask("000-00-0000");
                $('#addcontact.modal').on('show.bs.modal', function () {
                    document.getElementById('imgerror').innerHTML = '';
                    document.getElementById('hiddenimg').value = '';
                    $(".chosen-language").chosen();
                });
                $('#addRolodexForm a[data-toggle="tab"]').on('click', function (e) {
                    setValidation();
                    e.preventDefault();
                });
                $('#addcontact.modal').on('hidden.bs.modal', function () {
                    $('#addcontact.modal-title').text('Add New Contact');
                    $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                        this.value = '';
                    });
                    document.getElementById('profilepicDisp').style.display = 'none';
                    document.getElementById('dispProfile').src = '';
                    $('#addcontact a[href="#name"]').tab('show');
                    var addContactValidator = setValidation();
                    addContactValidator.resetForm();
                });
                $('#addContact').on('click', function () {
                    var cardcodeVal = $('input[name="card_cardcode"]').val();
                    if (cardcodeVal == '') {
                        if (!$('#addRolodexForm').valid()) {
                            return false;
                        }
                    }
                    if (!$('#addRolodexForm').valid()) {
                        return false;
                    }
                    var myImg = $('#profilepic').prop('files')[0];
                    if(myImg){
                        var sizeKB = myImg.size / 1024; ;
                        if(sizeKB > 5000)
                        {
                            swal("Failure !", "The file size exceeds the limit allowed.", "error");
                            return false;
                        }
                    }
                    $.blockUI();
                    var url = '<?php echo base_url() . "rolodex/addCardData" ?>';
                    if ($('input[name="card_cardcode"]').val() != '') {
                        url = '<?php echo base_url() . "rolodex/editCardData" ?>';
                    }
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: $('#addRolodexForm').serialize(),
                        success: function (result) {
                            var data = $.parseJSON(result);
                            var editCardcode = data.editcardid;
                            if ($('#profilepic').val() != '')
                            {
                                var old_img = document.getElementById('hiddenimg').value;
                                var images = '';
                                var file_data = $('#profilepic').prop('files')[0];
                                var form_data = new FormData();
                                form_data.append('file', file_data);
                                $.ajax({
                                    url: '<?php echo base_url(); ?>rolodex/updateprofilepic/' + editCardcode + '/' + old_img,
                                    dataType: 'text',
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    data: form_data,
                                    type: 'post',
                                    success: function (response) {
                                        if (response)
                                        {
                                            images = response;
                                            var plink = '<?php echo base_url() ?>assets/rolodexprofileimages/' + images;
                                            setTimeout(function ()
                                            {
                                                if (images != null) {
                                                    if ($('#applicantImage').length == 0) {
                                                        $('.client-img').find('img.img-circle').attr("src", plink);
                                                    } else
                                                    {
                                                        $('#applicantImage').remove();
                                                        $('.client-img').append('<center><img class="img-circle" height="67px" src="' + plink + '" /></center>');
                                                    }
                                                } else {

                                                }
                                            }, 500);
                                        }
                                    },
                                    error: function (response) {
                                    }
                                });
                            }
                            $.unblockUI();
                            if (data.status == '1') {
                                $('#addRolodexForm')[0].reset();
                                $('#addcontact').modal('hide');
                                $('#addcontact .modal-title').html('Add New Contact');

                                if ((data.caseid != 0 || data.caseid != 'undefined')) {
                                    var caseid = $("#hdnCaseId").val();
                                    if (data.caseid != 0 && $("#new_case").val() == 1) {
                                        var url = "<?php echo base_url() ?>cases/case_details/" + data.caseid;
                                        setTimeout(function ()
                                        {
                                            window.location.href = url;
                                        }, 3000);
                                    }
                                    if (cardcodeVal != '' || $("#new_case").val() == 'parties') {

                                        $.ajax({
                                            url: '<?php echo base_url() . "cases/case_details" ?>',
                                            method: "POST",
                                            data: {'caseId': $("#hdnCaseId").val()},
                                            success: function (result) {
                                                var res = $.parseJSON(result);
                                                setTimeout(function ()
                                                {
                                                    $(".applicant_name").html("<b>" + res.display_details.salutation + " " + res.display_details.first + " " + res.display_details.last + "</b>");
                                                    if (res.display_details.popular)
                                                    {
                                                        if ((res.display_details.popular).length < 10) {
                                                            var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular + '</b></span><br/>';
                                                        } else
                                                        {
                                                            var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + res.display_details.popular.substr(1, 10) + '..</b></span><br/>';
                                                        }
                                                        if ($('.popular').length > 0) {
                                                            $(".popular").replaceWith(htm);
                                                        } else
                                                        {
                                                            $(".applicant_name").after(htm);
                                                        }

                                                    }
                                                    $(".applicant_address").html(res.display_details.address1);
                                                    $(".applicant_address2").html(res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);

                                                    $('#address_map').attr('href', "https://maps.google.com/?q=" + res.display_details.address1 + '' + res.display_details.address2 + " " + res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);
                                                    $(".home").html(res.display_details.home);
                                                    if (res.display_details.car == "")
                                                        $(".business").html(res.display_details.business);
                                                    else
                                                        $(".business").html(res.display_details.car);

                                                    $(".email").html(res.display_details.email);
                                                    var dob = new Date(res.display_details.birth_date);
                                                    var today = new Date();
                                                    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                                                    $(".birth_date").html('Born on ' + moment(res.display_details.birth_date).format('MM/DD/YYYY') + ' ,' + age + 'years old');
                                                    $("#party_name").html(res.display_details.first + " " + res.display_details.last);
                                                    $("#party_beeper").val(res.display_details.beeper);
                                                    var p_tr_id = $('table#party-table-data').find('.table-selected');
                                                    p_tr_id = p_tr_id.attr('id');
                                                    $("#" + p_tr_id).find(".parties_name").html(res.display_details.first + ", " + res.display_details.last);
                                                }, 1000);
                                            }
                                        });
                                    }

                                }

                                rolodaxdataTable.draw(false);

                            } else {
                                swal("Oops...", data.message, "error");
                            }

                        }
                    });

                    return false;
                });
                $('.datepickerClass').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                /* rolodexSearchDatatable = $('#rolodexPartiesResult').DataTable({
                 "lengthMenu": [5, 10, 25, 50, 100]
                 }); */
                rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                    "scrollX": true,
                    stateSave: true,
					stateSaveParams: function (settings, data) {
						data.search.search = "";
						data.start = 0;
						delete data.order;
					},
                    "sScrollX": "100%",
                    "sScrollXInner": "150%",
                    "lengthMenu": [5, 10, 25, 50, 100]
                });
                $("#rolodex_search_parties_form").validate({
                    rules: {
                        rolodex_search_text: {
                            required: true
                        },
                        rolodex_search_parties: {
                            required: true
                        }
                    },
                    messages: {
                        rolodex_search_text: {
                            required: "Please provide search text"
                        },
                        rolodex_search_parties: {
                            required: "Please provide search by"
                        }
                    },
                    submitHandler: function (form, event) {
                        event.preventDefault();
                        var alldata = $("form#rolodex_search_parties_form").serialize();
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/searchForRolodex',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                                var text = $("#rolodex_search_text").val();
                                var substring = text.substr(0, 1);
                                var radioVal = $("input[name='rolodex_search_parties']:checked").val();
                                if (substring == "<") {

                                    var res = $.parseJSON(data);
                                    $("#searched_party_fullname").val(res[0].fullname);
                                    $("#searched_party_firmname").val(res[0].firm);
                                    $('#searched_party_type').html(res[0].type);
                                    $('#searched_party_cardnumber').html(res[0].cardcode);
                                    $(".addSearchedParties").trigger("click");

                                }

                                rolodexSearchDatatable.clear();
                                var obj = $.parseJSON(data);
                                $.each(obj, function (index, item) {
                                    var html = '';
                                    html += "<tr class='viewPartiesDetail' data-cardcode=" + item.cardcode + "><td>" + item.name + "</td>";
                                    html += "<td>" + item.firm + "</td>";
                                    html += "</tr>";
                                    rolodexSearchDatatable.row.add($(html));
                                });
                                rolodexSearchDatatable.draw();
                            }
                        });

                    }
                });
                $("#companylistForm").validate({
                    rules: {
                        rolodex_search_text_name: {
                            required: true,
                            noBlankSpace: true
                        }
                    },
                    messages: {
                        rolodex_search_text_name: {
                            required: "Please enter Firm/Company name "
                        }
                    },
                    submitHandler: function (form, event) {
                        event.preventDefault();
                    }
                });
            });
            $(document.body).on('click', '.dataTables-eamsLookup tbody tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                    $('.dataTables-eamsLookup tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click', '#eamsrefDetails', function () {
                var selectedrows = eamsData.rows('.table-selected').data();
                var caseNo = $("#caseNo").val();
                var cardCode = $('#caseCardCode').val();
                /*alert(selectedrows[0][0]); return false;*/
                if (selectedrows.length > 0) {
                    $('#eamslist').modal('hide');
                    swal({
                        title: 'EAMS',
                        text: 'Do you want to attach '+selectedrows[0][1]+' to the rolodex?',
                        showCancelButton: true,
                        confirmButtonColor: "#1ab394",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            swal.close();
                            $.ajax({
                                url: '<?php echo base_url(); ?>eams_lookup/eamsrefDetails',
                                method: 'POST',
                                dataType: 'json',
                                data: {
                                    eamsref: selectedrows[0][0],caseno: caseNo,cardCode:cardCode
                                },
                                beforeSend: function (xhr) {
                                    $.blockUI();
                                },
                                complete: function (jqXHR, textStatus) {
                                    $.unblockUI();
                                },
                                success: function (data) {
                                    if (data == false) {
                                        swal({
                                            title: "Alert",
                                            text: "No Party Selected.",
                                            type: "warning"
                                        });
                                    } else {

                                        $('input[name="card2_eamsref"]').val(data.eamsref);
                                        $('input[name="eamsref_name"]').val(data.name);
                                    }
                                }
                            });
                        }
                    });

                } else {
                    swal({
                        title: "Alert",
                        text: "No Party Selected.",
                        type: "warning"
                    });
                }
            });
            function setValidation() {
                var validator = $('#addRolodexForm').validate({
                    ignore: [],
                    rules: {
                        card_first: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() == '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card_type: {
                            required: true
                                    /*function (element) {
                                     if ($("input[name=card_type]").val() != '') {
                                     return true;
                                     }
                                     return false;
                                     }*/
                        },
                        card2_firm: {
                            required: function (element) {
                                if ($("input[name=card_first]").val() == '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_address1: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_city: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_state: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            },
                            stateUS: true
                        },
                        card2_zip: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            },
                            zipcodeUS: true
                        },

                        /*card_middle: {letterwithspace: true},
                        card_last: {letterwithspace: true},*/
                        card_business: {phoneUS: true},
                        card_home: {phoneUS: true},
                        card_fax: {phoneUS: true, maxlength: 15},
                        card_email: {email: true},
                        card2_phone1: {phoneUS: true},
                        card2_fax: {phoneUS: true},
                        card2_phone2: {phoneUS: true},
                        card2_fax2: {phoneUS: true},
                        card2_tax_id: {alphanumeric: true, maxlength: 15},
                        card_social_sec: {ssId: true},
                        card_speciality: {letterswithbasicpunc: true},
                    },
                    messages: {
                        card_first: {required: "Please Fill First Name or Firm Name"},
                        card2_firm: {required: "Please Fill First Name or Firm Name"},
                        card2_fax: {phoneUS: "Please specify a valid fax number"},
                        card2_fax2: {phoneUS: "Please specify a valid fax number"},
                        card_fax: {phoneUS: "Please specify a valid fax number"},
                    },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');

                        $('#addcontact a[href="#' + tab + '"]').tab('show');
                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "card_type") {
                            error.insertAfter(element.next());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

                return validator;
            }
            function caseDetails(cardCode) {
                $.ajax({
                    url: "<?php echo base_url() . 'cases/getCasrcardDetails' ?>",
                    method: "POST",
                    data: {
                        cardCode: cardCode
                    },
                    success: function (result) {
                        $('#viewAllCases').html(result);
                        $("#caseCardDetails").modal('show');
                        $("#caseCardDetails table").dataTable();

                    }
                });
            }
            function editContact(cardCode) {
                $.blockUI();
                $.ajax({
                    url: "<?php echo base_url() . "rolodex/getEditRolodex" ?>",
                    method: "POST",
                    data: {'cardcode': cardCode},
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments = '';
                        if (data.card_comments) {
                            if(isJson(data.card_comments)) {
                                notes_comments = $.parseJSON(data.card_comments);
                            } else {
                                notes_comments = data.card_comments;
                            }
                        }
                        $('#addcontact .modal-title').html('Edit Contact');
                        $('#card_salutation').val(data.salutation);
                        $('#card_suffix').val(data.suffix);
                        $('select[name=card_title]').val(data.title);
                        $('#card_type').val(data.type);
                        $('input[name=card_first]').val(data.first);
                        $('input[name=card_middle]').val(data.middle);
                        $('input[name=card_last]').val(data.last);
                        $('input[name=card_popular]').val(data.popular);
                        $('input[name=card_letsal]').val(data.letsal);
                        $('input[name=card_occupation]').val(data.occupation);
                        $('input[name=card_employer]').val(data.employer);
                        $('input[name=hiddenimg]').val(data.picture);
                        if (data.picture)
                        {
                            document.getElementById('profilepicDisp').style.display = 'block';
                            document.getElementById('dispProfile').src = data.picturePath;
                        } else
                        {
                            document.getElementById('dispProfile').src = '';
                        }
                        $('input[name=card2_firm]').val(data.firm);
                        $('input[name=card2_address1]').val(data.address1);
                        $('input[name=card2_address2]').val(data.address2);
                        $('input[name=card2_city]').val(data.city);
                        $('input[name=card2_state]').val(data.state);
                        $('input[name=card2_zip]').val(data.zip);
                        $('input[name=card2_venue]').val(data.venue);
                        $('input[name=card2_eamsref]').val(data.eamsref);
                        $('input[name=eamsref_name]').val(data.eamsname);
                        $('input[name=card_business]').val(data.business);
                        $('input[name=card_fax]').val(data.card_fax);
                        $('input[name=card_home]').val(data.home);
                        $('input[name=card_car]').val(data.car);
                        $('input[name=card_email]').val(data.email);
                        if (notes_comments != '')
                            $('input[name=card_notes]').val(notes_comments[0].notes);
                        else
                            $('input[name=card_notes]').val();
                        $('input[name=card2_phone1]').val(data.phone1);
                        $('input[name=card2_phone2]').val(data.phone2);
                        $('input[name=card2_tax_id]').val(data.tax_id);
                        $('input[name=card2_fax]').val(data.card2_fax);
                        $('input[name=card2_fax2]').val(data.fax2);
                        $('input[name=card_social_sec]').val(data.social_sec);
                        if (data.birth_date == 'NA') {
                            $('input[name=card_birth_date]').val('');
                        } else {
                            $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                        }

                        $('input[name=card_licenseno]').val(data.licenseno);
                        $('select[name=card_specialty]').val(data.specialty);
                        $('input[name=card_mothermaid]').val(data.mothermaid);
                        $('input[name=card_cardcode]').val(data.cardcode);
                        $('input[name=card_firmcode]').val(data.firmcode);
                        $('select[name=card_interpret]').val(data.interpret);
                        $('select[name=card_language]').val(data.language).trigger("change");

                        $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                        $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                        $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                        $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                        if (notes_comments != '')
                            $('textarea[name=card_comments]').val(notes_comments[1].comments);
                        else
                            $('textarea[name=card_comments]').val();
                        $('textarea[name=card2_comments]').val(data.card2_comments);
                        $('textarea[name=card2_mailing1]').val(data.mailing1);
                        $('textarea[name=card2_mailing2]').val(data.mailing2);
                        $('textarea[name=card2_mailing3]').val(data.mailing3);
                        $('textarea[name=card2_mailing4]').val(data.mailing4);
                        $('#new_case').val('parties');
                        $('input[name="card_cardcode"]').val(data.cardcode);

                        $('#addcontact').modal({backdrop: 'static', keyboard: false});
                        $("#addcontact").modal('show');

                        $("#addRolodexForm select").trigger('change');

                        $.unblockUI();
                    }
                });
            }
            $(document.body).on('click', '.rolodex-company', function () {
                $("#firmcompanylist").attr("style", "z-index:9999 !important");
                $("#rolodexsearchResult_data tbody").html('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $('#firmcompanylist').modal('show');
                rolodexSearch.clear();
                return false;
            });
            $(document.body).on('keyup', '#rolodex_search_text_name', function (e) {
                if (e.keyCode == '13') {
                    $('#rolodex_search_text_name').trigger("blur");
                    $('#rolodex_search_text_name').trigger("focus");
                }
            });
            $(document.body).on('blur', '#rolodex_search_text_name', function (e) {
                if (e.type == 'focusout' || e.keyCode == '13')
                {
                    if ($('#companylistForm').valid())
                    {
                        if ($('#rolodex_search_text_name').val().trim().length > 0)
                        {
                            rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                                processing: true,
                                rolodexsearchAddress: true,
                                serverSide: true,
                                stateSave: true,
                                "searching" : true,
                                stateSaveParams: function (settings, data) {
                                        data.search.search = "";
                                        data.start = 0;
                                        delete data.order;
                                        data.order = [0, "ASC"];
                                },
                                bFilter: false,
                                scrollX: true,
                                sScrollY: 400,
                                bScrollCollapse: true,
                                destroy: true,
                                autoWidth: true,
                                "lengthMenu": [5, 10, 25, 50, 100],
                                order: [[0, "ASC"]],
                                ajax: {
                                    url: '<?php echo base_url(); ?>cases/searchForRolodexByfirm',
                                    method: 'POST',
                                    data: {rolodex_search_parties: $('#rolodex_search_parties_name').val(), rolodex_search_text: $(this).val()},
                                },
                                fnRowCallback: function (nRow, data) {
                                    $(nRow).attr("cardcode", '' + data[8]);
                                    $(nRow).addClass('viewsearchPartiesDetail1');
                                },
                                fnDrawCallback: function (oSettings) {
                                },
                                initComplete: function (settings, json) {

                                },
                                select: {
                                    style: 'single'
                                }
                            });
                            /*$.ajax({
                             url: '<?php //echo base_url();      ?>cases/searchForRolodex',
                             method: 'POST',
                             data: {rolodex_search_parties:$('#rolodex_search_parties_name').val(),rolodex_search_text:$(this).val()},
                             beforeSend: function (xhr) {
                             $.blockUI();
                             },
                             complete: function (jqXHR, textStatus) {
                             $.unblockUI();
                             },
                             success: function (data) {
                             rolodexSearch.clear();
                             
                             var obj = $.parseJSON(data);
                             $.each(obj, function (index, item) {
                             var html = '';
                             html += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + item.cardcode + "><td>" + item.firm + "</td>";
                             html += "<td>" + item.city + "</td>";
                             html += "<td>" + item.address + "</td>";
                             html += "<td>" + item.address2 + "</td>";
                             html += "<td>" + item.phone + "</td>";
                             html += "<td>" + item.name + "</td>";
                             html += "<td>" + item.state + "</td>";
                             html += "<td>" + item.zip + "</td>";
                             html += "</tr>";
                             rolodexSearch.row.add($(html));
                             });
                             rolodexSearch.draw();
                             }
                             });*/
                        } else
                        {
                            swal({
                                title: "Alert",
                                text: "Please enter Firm name.",
                                type: "warning"
                            });
                        }
                    }
                }
            });
            $(document.body).on('click', '.viewPartiesDetail', function () {
                $("#rolodexPartiesResult tr").removeClass("highlight");
                $(this).addClass('highlight');
                var caseNo = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: $(this).data('cardcode'),
                        caseno: caseNo
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data == false) {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has been occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        } else {
                            $("#searched_party_fullname").val(data.fullname);
                            $("#searched_party_firmname").val(data.firm);
                            $('#searched_party_type').html(data.type);
                            $('#searched_party_speciality').html(data.speciality);
                            $('#searched_party_address').html(data.address1);
                            $('#searched_party_city').html(data.city);
                            $('#searched_party_cardnumber').html(data.cardcode);
                            $('#searched_party_ssno').html(data.social_sec);
                            $('#searched_party_email').html(data.email);
                            $('#searched_party_licenseno').html(data.licenseno);
                            $('#searched_party_homephone').html(data.home);
                            $('#searched_party_business').html(data.business);
                            $('.addSearchedParties').attr("disabled", false);
                            $('.editPartyCard').attr("disabled", false);
                        }
                    }
                });



            });
            $(document.body).on('click', '.addSearchedParties', function () {
                var caseNo = $("#caseNo").val();
                var cardcode = $("#searched_party_cardnumber").html();
                var type = $("#searched_party_type").html();
                var fullname = $("#searched_party_fullname").val();
                var firm = $("#searched_party_firmname").val();
                var nameofParty = '';

                if (firm != "") {
                    nameofParty = firm;
                } else {
                    nameofParty = fullname;
                }

                var radioVal = $("input[name='rolodex_search_parties']:checked").val();
                if (radioVal == "rolodex-cardno") {
                    nameofParty = fullname;
                }

                $("#addparty").modal('hide');

                $.ajax({
                    url: '<?php echo base_url(); ?>cases/addSearchedPartyToCase',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        caseno: caseNo,
                        type: type
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if ($('#parties table tr#tr-0').length < 1) {
                            location.reload();
                        }
                        var vount_r = $('#party-table-data tr').length;
                        if ($(".party-in table td").html() == "<h4>No Parties Found</h4>") {
                            $(".party-in table td").remove();
                        }
                        var html = '';
                        html += '<tr class="" id="tr-' + vount_r + '">';
                        html += "<td>" + vount_r + "</td>";
                        html += "<td title=" + nameofParty + "><a class='view_party' href='javascript:void(0);' data-card=" + cardcode + " data-case=" + caseNo + ">" + nameofParty + "</a> </td>";
                        html += "<td>" + type + "</td>";
                        html += "<td>";
                        html += "<a class='view_party' href='javascript:void(0);' data-card=" + cardcode + " data-case=" + caseNo + ">View</a> ";
                        html += "<a class='remove_party' href='javascript:void(0);' data-card=" + cardcode + " data-case=" + caseNo + ">Delete</a>";
                        html += "</td></tr>";
                        table.row.add($(html));
                        table.draw();
                        if ($('table#party-table-data tr').hasClass('table-selected'))
                        {
                            $('table#party-table-data tr').removeClass('table-selected')
                        }
                        $('tr.last').addClass('table-selected');

                        setPartyViewData(cardcode, caseNo);


                    }
                });
            });
            function setPartyViewData(cardcode, caseNo)
            {
                $('.edit-party').show();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getAjaxCardDetails',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        caseno: caseNo
                    },
                    success: function (data) {
                        if (data == false) {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has been occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        } else {
                            var notes_comments = '';
                            if (data.comment_per) {
                                if(isJson(data.comment_per)) {
                                    notes_comments = $.parseJSON(data.comment_per);
                                } else {
                                    notes_comments = data.comment_per;
                                }
                            }
                            $('.edit-party').attr('data-casecard', cardcode);
                            $('.edit-party').attr('data-case', caseNo);
                            $('#party_name').html(data.fullname);
                            $('#party_name').data('cardcodeWarning', data.cardcode);
                            $('#party_type').data('cardcodeWarning', data.cardcode);
                            $('#partiesCardCode').val(data.cardcode);
                            $('#party_type').html(data.type);
                            $('#firm_name').html(data.firm);
                            $('.addressVal').attr('href', "https://maps.google.com/?q=" + data.address1 + data.address2);
                            $('#party_address1').html(data.address1);
                            $('#party_address2').html(data.address2);
                            $('#party_city').html(data.city);
                            $('#party_phn_home').html(data.home);
                            $('#party_phn_office').html(data.business);
                            $('#party_fax').html(data.fax);
                            $('#party_email').html(data.email);
                            $('#party_side').html(data.side);
                            $('#party_offc_no').html(data.officeno);
                            $('#party_notes').html('');
                            if (data.eamsref)
                                $('#party_eams').html(data.name + ' ' + data.eamsref);
                            else
                                $('#party_eams').html('');
                            $('#party_cell').html(data.car);
                            $('#party_beeper').html(data.beeper);
                            $('#party_notes').html(data.notes);


                            if (data.comment_bus != '') {
                                $('.busComment').css({'visibility': 'visible'});
                                $('#comment_bus').html(data.comment_bus);
                            } else {
                                $('.busComment').css({'visibility': 'hidden'});
                                $('#comment_bus').html(data.comment_bus);
                            }

                            if (data.comment_per && notes_comments[1]) {
                                $('.busComment').css({'visibility': 'visible'});
                                $('#comment_per').html(notes_comments[1].comments);
                            } else {
                                $('.busComment').css({'visibility': 'hidden'});
                                $('#comment_per').html('');
                            }
                            if (data.atty_hand)
                            {
                                if ($('#case_act_atty option:contains(' + data.atty_hand + ')').length) {
                                    $('select[name=case_act_atty]').val(data.atty_hand);
                                } else
                                {
                                    $('#case_act_atty').append("<option value='" + data.atty_hand + "' selected='seleceted'>" + data.atty_hand + "</option>");
                                }
                            }

                        }
                    }
                });
            }
            $(document.body).on('click', '#change_name_warning', function () {
                $("#partiesNameChangeWarning").modal("hide");
                editContact($('#party_name').data('cardcodeWarning'));
            });
            $(document.body).on('click', '.editPartyCard', function () {
                var cardcode = $('#searched_party_cardnumber').html();
                if (cardcode == "" || cardcode == 'undefinded') {
                    swal({
                        title: "Alert",
                        text: "The card does not exist in the Rolodex or cannot be edited at this time. Please try searching again.",
                        type: "warning"
                    });
                } else {
                    $("#addcontact").attr("style", "z-index:9999 !important");
                    editContact(cardcode);

                }
            });
            $(document.body).on('click', '#attach_name', function () {
                var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                var first_name = $(".first_name").val();
                var last_name = $(".last_name").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/attach_name',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        first_name: first_name,
                        last_name: last_name
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (result) {
                        var url = "<?php echo base_url() ?>cases/case_details/" + result.caseno;
                        $(location).attr('href', url);
                    }
                });

            });
            $(document.body).on('click', '.selectDuplicateData', function () {
                $("#duplicateEntries tr").removeClass("highlight");
                $(this).addClass('highlight');
                var numItems = $('.highlight').length;
                if (numItems == '1')
                {
                    $('#edit_new_case').prop('disabled', false);
                    $('#attach_name').prop('disabled', false);
                }
            });
            $(document.body).on('click', '.addparty', function () {
                $('.addSearchedParties').attr("disabled", true);
                $('.editPartyCard').attr("disabled", true);
                $("#addparty").modal('show');
            });
            $(document.body).on('click', '#rolodexsearchResult_data tbody tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {

                    $('#rolodexsearchResult_data tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click', '#viewSearchedPartieDetails', function () {
                var selectedrows = rolodexSearch.rows('.table-selected').data();
                var caseNo = $("#caseNo").val();
                if (selectedrows.length > 0) {
                    $('#firmcompanylist').modal('hide');
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            cardcode: selectedrows[0][8],
                            caseno: caseNo
                        },
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data == false) {
                                swal({
                                    title: "Alert",
                                    text: "Please attach the existing  firm from the firm list to continue...",
                                    type: "warning"
                                });
                            } else {
                                var city = data.city;
                                if (city != null)
                                {
                                    var strarray = city.split(',');
                                    $('input[name="card2_city"]').val($.trim(strarray[0]));
                                    $('input[name="card2_state"]').val($.trim(strarray[1]));
                                    $('input[name="card2_zip"]').val($.trim(strarray[2]));
                                }
                                $('input[name="card2_firm"]').val(data.firm);
                                $('input[name="card2_address1"]').val(data.address1);
                                $('input[name="card2_address2"]').val(data.address2);

                                $('input[name="card2_venue"]').val(data.venue);
                                $('.addSearchedParties').attr("disabled", false);
                                $('.editPartyCard').attr("disabled", false);
                            }
                        }
                    });
                } else {
                    swal({
                        title: "Alert",
                        text: "Please attach the existing  firm from the firm list to continue...",
                        type: "warning"
                    });
                }
            });
            $('#firmcompanylist').on('hidden.bs.modal', function () {
                $("#rolodex_search_text_name").val("");
                $('tr.table-selected').removeClass('table-selected');
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });
                $('#rolodexsearchResult_data').dataTable().fnClearTable();
            });
            $('#addparty').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });

            });
            function attachParty(tab)
            {
                var caseno = $('#hdnCaseId').val();
                $('#casepartyDetaillist').modal('show');
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getcasepartyDetails',
                    method: 'POST',
                    data: {caseno: caseno},
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        casepartylist_data.clear();
                        var obj = $.parseJSON(data);
                        if (obj.length < 1) {
                            location.reload();
                        }
                        $.each(obj, function (index, item) {
                            var name = item.salutation + ' ' + item.first + ' ' + item.middle + ' ' + item.last;
                            if (item.state)
                            {
                                var istate = item.state;
                            } else
                            {
                                var istate = '';
                            }
                            if (item.zip)
                            {
                                var izip = item.zip;
                            } else
                            {
                                var izip = '';
                            }
                            if (item.city)
                            {
                                var icity = item.city;
                            } else
                            {
                                var icity = '';
                            }
                            if (item.address1)
                            {
                                var iadd1 = item.address1;
                            } else
                            {
                                var iadd1 = '';
                            }
                            if (item.firm)
                            {
                                var ifirm = item.firm;
                            } else
                            {
                                var ifirm = '';
                            }
                            if (item.fax)
                            {
                                var ifax = item.fax;
                            } else
                            {
                                var ifax = '';
                            }
                            if (item.phone1)
                            {
                                var iph1 = item.phone1;
                            } else
                            {
                                var iph1 = '';
                            }
                            var html = '';
                            html += '<tr class="casepartydetails"  data-first="' + item.first + '" data-state="' + istate + '" data-zip="' + izip + '"data-last="' + item.last + '" data-active_data="' + tab + '"data-social_sec="' + item.social_sec + '"data-fax="' + ifax + '"data-phone="' + iph1 + '"data-salutation="' + item.salutation + '" ><td id="name">' + name + '</td>';
                            html += '<td id="firm">' + ifirm + '</td>';
                            html += '<td>' + icity + '</td>';
                            html += '<td>' + iadd1 + '</td>';
                            html += '</tr>';
                            casepartylist_data.row.add($(html));
                        });
                        casepartylist_data.draw();
                    }
                });
            }
            $('#addparty.modal').on('show.bs.modal', function () {
                if ($('table#rolodexPartiesResult tr').hasClass('highlight') == false)
                {
                    $('.addSearchedParties').prop('disabled', true);
                    $('.editPartyCard').prop('disabled', true);
                }
            });
            $('#firmcompanylist').on('show.bs.modal', function () {
                $(".dataTables-example").removeAttr("style");
                $(".dataTables_scrollHeadInner").removeAttr("style");
                $(".dataTables-example").css("width", "100% !important;");
                $(".dataTables_scrollHeadInner").css("width", "100% !important;");
            });
            $('#firmcompanylist').on('hide.bs.modal', function () {
                $('#rolodex_search_text_name-error').remove();
                $('#rolodex_search_text_name').removeClass('error');
            });
            var regExp = /[a-z]/i;
            function validate(e) {
                var value = String.fromCharCode(e.which) || e.key;
                if (regExp.test(value)) {
                    e.preventDefault();
                    return false;
                }
            }
            $(document.body).on('click', '#eamsrefDetach', function () {
            var caseNo = $("#caseNo").val();
            var cardCode = $('#caseCardCode').val();
            /*alert(selectedrows[0][0]); return false;*/
            $('#eamslist').modal('hide');
            swal({
                title: 'EAMS',
                text: 'Are you sure you want to detach any EAMS uniform references to this rolodex card and use defaults?',
                showCancelButton: true,
                confirmButtonColor: "#1ab394",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    swal.close();
                    $.ajax({
                        url: '<?php echo base_url(); ?>eams_lookup/eamsrefDetached',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            caseno: caseNo,cardCode:cardCode
                        },
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (data) {
                            /*if (data == false) {
                                swal({
                                    title: "Alert",
                                    text: "No Party Selected.",
                                    type: "warning"
                                });
                            } else {*/

                                $('input[name="card2_eamsref"]').val('');
                                $('input[name="eamsref_name"]').val('');
                            /*}*/
                        }
                    });
                }
            });
    
    
    });
    
    $('#eamslist').on('shown.bs.modal', function (e) {
        var eams = $('input[name="card2_eamsref"]').val();
        if(eams && eams != 0){
            $('#eamsrefDetach').prop('disabled', false);
        }
        else{
            $('#eamsrefDetach').prop('disabled', true);
        }
        eamsData.columns.adjust();
    });

    $('#eamslist').on('hide.bs.modal', function (e) {
        $('.input-sm').val('');
        eamsData.search('').draw(); 
        eamsData.ajax.reload();
    });
    
    $('#card_last').on('blur', function () {
        var fname = $('#card_first').val();
        var lname = $('#card_last').val();
        $.ajax({
            url: '<?php echo base_url(); ?>rolodex/checkDuplicateRolodex',
            method: "POST",
            data: {
                fname: fname,lname:lname
            },
            success: function (result) {
                var obj = $.parseJSON(result);
                if (obj.status == '1') {
                    $("#duplicateroloData").modal("show");
                    duplicateRoloDatatable.rows().remove().draw();
                    $.each(obj.data, function (index, item) {
                        var html = '';
                        html += "<tr>";
                        html += "<td>" + item.name + "</td>";
                        html += "<td>" + item.address1 + "</td>";
                        html += "<td>" + item.city + "</td>";
                        html += "<td>" + item.social_sec + "</td>";
                        html += "<td>" + item.firm + "</td>";
                        html += "</tr>";
                        duplicateRoloDatatable.row.add($(html));
                    });
                    duplicateRoloDatatable.draw();
                }
            }
        });
    });
        </script>



