<!DOCTYPE html>
<html lang="en" class="no-scroll">
<?php
$url_segment1 = $this->uri->segment(1);
$url_segment2 = $this->uri->segment(2);

?>
    <head>
        <meta charset="utf-8">
       <!--  <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
       <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />

        <title>
            <?php 
                if($this->uri->segment(3) != '') {
                    echo $this->uri->segment(3)." | ";
                } elseif(is_numeric($this->uri->segment(2))) {
                    echo $this->uri->segment(2)." | ";
                }
            ?>
            <?php echo $pageTitle; ?> | <?php echo APPLICATION_NAME; ?>        
        </title>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets'); ?>/img/favicon.png">

        <script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>
         <script src="<?php echo base_url('assets'); ?>/js/plugins/converse/converse.js"></script>

        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Plugins -->
        <!--<link href="<?php echo base_url('assets'); ?>/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">-->

        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet"> -->



        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">

        <?php if ($url_segment1 != 'dashboard') {?>
        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/cropper/cropper.min.css" rel="stylesheet"> -->
        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/switchery/switchery.css" rel="stylesheet"> -->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet"> -->

        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/mdtimepicker.css" rel="stylesheet"> -->

        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/basic.css" rel="stylesheet"> -->
        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet"> -->
        <?php }?>

        <?php if ($url_segment1 == 'dashboard') {?>
            <link href="<?php echo base_url('assets'); ?>/css/plugins/jquery-highlighttextarea-master/jquery.highlighttextarea.min.css" rel="stylesheet">
        <?php }?>
        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/datapicker/datepicker3.css" rel="stylesheet"> -->

        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet"> -->
        <?php //if ($url_segment1 != 'calendar') {?>
        <link href="<?php echo base_url('assets'); ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
        <?php //}?>
        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet"> -->



        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">-->


       <link href="<?php echo base_url('assets'); ?>/css/plugins/select2/select2.css" rel="stylesheet">
       <link href="<?php echo base_url('assets'); ?>/css/plugins/editbleselect/jquery-editable-select.min.css" rel="stylesheet">


        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet"> -->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote.css" rel="stylesheet"> -->
        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">-->

        <!-- <link href="<?php echo base_url('assets'); ?>/css/animate.css" rel="stylesheet"> -->
        <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
        <!--<link href="<?php echo base_url('assets'); ?>/css/dev_styles.css" rel="stylesheet">-->

        <!-- <link href="<?php echo base_url('assets'); ?>/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet"> -->

        <link href="<?php echo base_url('assets'); ?>/js/plugins/converse/converse.css" rel="stylesheet">
         <link href="<?php echo base_url('assets'); ?>/css/plugins/chosen/chosen.css" rel="stylesheet">
<?php if ($url_segment2 == 'case_details') {?>
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/lightbox.min.css">
<?php }?>
    
    <!-- File included for new time picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/uikit.min.css" />
    <script src="<?php echo base_url('assets'); ?>/js/uikit.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/components/autocomplete.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/components/timepicker.min.js"></script>
    <!-- End -->
    </head>
    <style>
     #profileImage {
        background: #512DA8;
        font-size: 28px;
        color: #fff;
        width: 42px;
        height: 42px;
        text-align: center;
        line-height: 38px;
        display: inline-block;
      }
      #page-wrapper .nav > li > a.count-info i {
    color: #4d7ab1;
    }
    </style>
    <body>
        <script type="text/javascript">
            var message = '<?php echo $this->session->flashdata('message') ?>';
            var HTTP_PATH = '<?php echo base_url(); ?>';
            var menu = '<?php echo $menu; ?>';
            $('body').addClass(localStorage.getItem('mini'));
        </script>
        <div id="wrapper">

            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <center><span>
                                    <?php
$url = FCPATH . 'assets/profileimages/' . $this->session->userdata('user_data')['profilepic'];
/*echo 'exist =>' . file_exists($url);
exit; */
if (file_exists($url) && $this->session->userdata('user_data')['profilepic'] != '') {
    ?>

                                    <a href="<?php echo base_url() . 'profile' ?>"> <img alt="image" height="42" width="42" class="img-circle" src="<?php echo base_url('assets/profileimages') . "/" . $this->session->userdata('user_data')['profilepic']; ?>" /></a>
                                    <?php
} else {
    ?>
                                            <a href="<?php echo base_url() . 'profile' ?>"> <div id="profileImage" height="42" width="42" class="img-circle"></div></a>
                                        <?php
}
?>
<!--                                    <a href="<?php echo base_url() . 'profile' ?>" title="Edit Profile" class="pull-right" style=" float: right; margin-top: 10px;"><i class="fa fa-edit"></i></a>-->
                                </span></center>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="cursor: default !important;">
                                    <span class="clear">
                                        <span class="block m-t-xs">
                                           <center><strong class="font-bold"><?php echo $this->session->userdata('user_data')['fname'] . " " . $this->session->userdata('user_data')['lname']; ?></strong></center>
                                        </span>
                                        <span class="text-muted text-xs block"><center><?php echo $this->session->userdata('user_data')['title']; ?></center></span>
                                    </span>
                                </a>
                                <!-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="#">Profile</a></li>
                                    <li><a href="#">Contacts</a></li>
                                    <li><a href="#">Mailbox</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" onclick="logout()">Logout</a></li>
                                </ul>-->
                            </div>
                            <div class="logo-element">
                                <?php
$url = FCPATH . 'assets/profileimages/' . $this->session->userdata('user_data')['profilepic'];
/*echo 'exist =>' . file_exists($url);
exit; */
if (file_exists($url) && $this->session->userdata('user_data')['profilepic'] != '') {
    ?>

                                    <a href="<?php echo base_url() . 'profile' ?>"><img alt="image" height="42" width="42" class="img-circle" src="<?php echo base_url('assets/profileimages') . "/" . $this->session->userdata('user_data')['profilepic']; ?>" /></a>
                                    <?php
} else {
    ?>
                                            <a href="<?php echo base_url() . 'profile' ?>"> <div id="profileImage" height="42" width="42" class="img-circle"><?php echo substr($this->session->userdata('user_data')['initials'], 0, 1); ?></div></a>
                                        <?php
}
?>
                            </div>
                        </li>
                        <li data-active='dashboard'>
                            <a href="<?php echo base_url('dashboard'); ?>" title="Dashboard"><i class="fa fa-th-large" ></i> <span class="nav-label">Dashboard</span> <!--<span class="fa arrow"></span>--></a>
                        </li>
                        <li data-active='cases'>
                            <a href="<?php echo base_url('cases'); ?>" title="Cases"><i class="fa fa-diamond"></i> <span class="nav-label">Cases</span></a>
                        </li>
                        <li data-active='calendar'>
                            <a href="<?php echo base_url('calendar'); ?>" title="Calendar"><i class="fa fa-calendar"></i> <span class="nav-label">Calendar</span></a>
                        </li>
                        <li data-active='email'>
                            <a href="<?php echo base_url('email'); ?>" title="E-mails"><i class="fa fa-envelope"></i> <span class="nav-label">E-mails </span><span class="emailchk label label-warning email_count pull-right"></span></a>
                        </li>
                        <li data-active='rolodex'>
                            <a href="<?php echo base_url('rolodex'); ?>" title="Rolodex"><i class="fa fa-files-o"></i><span class="nav-label">Rolodex</span>  </a>
                        </li>
                        <li data-active='tasks'>
                            <a href="<?php echo base_url('tasks'); ?>" title="Tasks"><i class="fa fa-tasks"></i> <span class="nav-label">Tasks</span></a>
                        </li>
                        <li data-active='eams_lookup'>
                            <a href="<?php echo base_url('eams_lookup'); ?>" title="EAMS Lookup"><i class="fa fa-eye"></i> <span class="nav-label">EAMS Lookup</span>  </a>
                        </li>
                        <?php $snslink = "http://108.222.150.121:8082/snsweb/user/login?username=" . $this->session->userdata('user_data')['username'] . "&password=" . base64_encode($this->session->userdata('user_data')['password']) . "&snsusersunqid=" . $this->session->userdata('user_data')['initials'];?>
                        <li data-active='reminders' class="not-focus">
                            <a href="<?php echo $snslink; ?>" target="_blank" title="Reminders"><i class="fa fa-bell"></i> <span class="nav-label">Reminders</span></a>
                        </li>
                        <!-- <li data-active='tools'>
                            <a href="javascript:;"><i class="fa fa-edit"></i> <span class="nav-label">Tools</span></a>
                        </li>
                        <li data-active='programs'>
                            <a href="javascript:;"><i class="fa fa-desktop"></i> <span class="nav-label">Programs</span></a>
                        </li> -->
                        <li data-active='reports'>
                            <a href="<?php echo base_url('reports'); ?>" title="Reports"><i class="fa fa-pie-chart"></i> <span class="nav-label">Reports</span></a>
                        </li>
                        <!-- <li data-active='broadcast'>
                            <a href="<?php echo base_url('broadcast'); ?>"><i class="fa fa-wechat"></i> <span class="nav-label">Broadcasts</span></a>
                        </li> -->
<!--                        <li data-active='chats'>
                            <a href="<?php echo base_url('chat'); ?>" title="Broadcasts"><i class="fa fa-wechat"></i> <span class="nav-label">Broadcasts</span></a>
                        </li>
                        <li data-active='prospect'>
                            <a href="<?php echo base_url('prospects'); ?>" title="Prospects"><i class="fa fa-users"></i> <span class="nav-label">Potential New Client (PNC)</span></a>
                        </li>-->
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row">

                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                            <img class="logo_Law" src="<?php echo base_url('assets'); ?>/img/logo.jpg">
                            <!-- <form role="search" class="navbar-form-custom" action="">
                                <div class="form-group">
                                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                                </div>
                            </form> -->
                        </div>
                        <input type="hidden" name="check_click_unread" id="check_click_unread" value="0">
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                            <center><span class="m-r-sm welcome-message text-navy" ><strong>Welcome, <?php echo $this->session->userdata('user_data')['initials']; ?>!</strong></span></center>

                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-envelope"></i>
                                    <span class="label label-warning email_count"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-messages">
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="<?php echo base_url('email'); ?>">
                                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="logout()">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                            <!--<li>
                                <a class="right-sidebar-toggle">
                                    <i class="fa fa-tasks"></i>
                                </a>
                            </li>-->
                        </ul>
                    </nav>
                </div>
                <script>
                        $(document).ready(function(){
                            var firstName = '<?php echo $this->session->userdata('user_data')['initials'] ?>';
                            var intials = firstName.charAt(0);
                            var profileImage = $('#profileImage').text(intials);
                          });
                </script>