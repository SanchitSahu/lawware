 <script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>
        <script type="text/javascript">
            var uri_seg = '<?php echo (isset($url_seg)) ? $url_seg : '';?>';
            var myDropzone = "";
            var selectedArray = [];
            var rolodexSearchDatatable = '';
            var rolodexSearch='';
            var categoryeventlist='';
            var casepartylist_data='';
            var venue_table_data = '';
            var caseactTable= '';
            var swalFunction = '';
            var CaseHID = $("#hdnCaseId").val();
            var ProspectListDatatable;
            var searchBy1 = {'prosno':<?php echo isset($record[0]->proskey)?$record[0]->proskey:'null';?>};


 $(document.body).on('ifToggled','input#field_ct1', function (){
                if($(this).is(':checked'))
                {
                    $('#field_doi2').removeAttr('readonly');
                    $('#field_adj1c').mask('CT - 99/99/9999 ',{placeholder:"CT - mm/dd/yyyy"});
                }
                else
                {
                   $("#field_doi2").prop('readonly', true);
                   $('#field_adj1c').val('');
                }
            });

            $(document.body).on('change','input#field_doi', function (){
                if($('#field_ct1').is(':checked'))
                {
                    $('#field_doi2').removeAttr('readonly');
                    $('#field_adj1c').mask('CT - 99/99/9999 ',{placeholder:"CT - mm/dd/yyyy"});
                }
                else
                {
                    var value = $('#field_doi').val();
                    $('#field_adj1c').val(value);
                }
            });

            $(function() {
                $( "#relatedcasenumber,.caseno" ).autocomplete({
                    source: '<?php echo base_url(); ?>cases/autosearch/'+CaseHID
                });
            });




            $(document).ready(function () {

                ProspectListDatatable = $('#ProspectListDatatable').DataTable({
                    scrollX:true
                });
                $('input.numberinput').bind('keypress', function (e) {
                    return !(e.which != 8 && e.which != 0 &&
                            (e.which < 48 || e.which > 57) && e.which != 46);
                });

                var calYearRange = $('#calYearRange').val();

                venue_table_data = $('#venue_table').DataTable();
                categoryeventlist=$('#caseEventlist').DataTable();
                casepartylist_data=$('#casePartylist').DataTable();
                //caseactTable=$('#case_activity_list').dataTable();
                rolodaxdataTable = $('.dataTables-rolodex').DataTable();
                $('#print_datepicker').hide();

                /*var partdataTable = $("#party-table-data").dataTable({
                    "processing": true,
                    "serverSide": true,
                    "bFilter": true,
                    "sScrollX": "100%",
                    "sScrollXInner": "150%",
                    //"bStateSave": true,
                    "order": [[ 0, "desc" ]],
                    "ajax": {
                        url: "<?php echo base_url(); ?>case/getPartyAjaxData",
                        type: "POST",
                        data: {'caseno': $('#caseNo').val()},

                        }
                });*/

                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
                // End Task Table

                $('.datepicker').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date(),
                });

                $('#datepicker').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date(),
                });

                $('.dob').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: '+0d',
                });


                $('#taskdatepicker').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date(),
                });

                $('#taskdatepicker2').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date(),
                });


                $('#caldate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    /*endDate: "+"+calYearRange+"y",*/
                    startDate: new Date()
                });

                $('.rdate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    /*endDate: "+"+calYearRange+"y",*/
                    startDate: new Date()
                });


                var d = new Date();
                var hours = d.getHours();
                var minutes = d.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                var currenttime = d;

                if(d.getHours() > 12) {
                    d.setHours(d.getHours() - 12);
                }
                if(d.getHours() < 10) {
                    hours = '0' + d.getHours();
                } else {
                    hours = d.getHours();
                }
                if(d.getMinutes() < 10) {
                    minutes = '0' + d.getMinutes();
                } else {
                    minutes = d.getMinutes();
                }
                var currenttime = hours+":"+minutes+" "+ampm;
                $('#caltime').val(currenttime)

               /*var input1 = $('#caltime');
                input1.mdtimepicker({
                     autoclose: true,
                 });

                 $('.rtime').mdtimepicker({
                     autoclose: true,
                 });

                 $('#oletime').mdtimepicker({
                    autoclose: true,
                    'default': currenttime,
                });*/
                $('#oletime').val(currenttime);
   $('#tickledate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date()
                });

                /*$('#tickletime').mdtimepicker({
                    autoclose: true
                });*/

                $('.preinjurydate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });

                $('.injurydate').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date()
                });

                $('#field_adj1a').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date(),
                });
                // Task Table
                $('#start_printData').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                }).on('changeDate', function (ev) {
                    $('#end_printData').datetimepicker('setStartDate', ev.date);
                });

                $('#end_printData').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    //endDate: new Date()
                }).on('changeDate', function (ev) {
                    $('#start_printData').datetimepicker('setEndDate', ev.date);
                });

                $('select[name=print_type]').on('change', function () {
                    if (this.value == 4 || this.value == 5) {
                        $('#print_datepicker').show();

                    } else {
                        $('#start_printData').val('').datetimepicker("update");
                        $('#end_printData').val('').datetimepicker("update");
                    }
                });

                $('#casedate_followup').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date(),
                });

                //console.log(searchBy);
                //var status = $('ul#task-navigation li a').parent().attr('id');

                //alert(uri_seg);
               /* if(uri_seg !='rolodex' && uri_seg !='dashboard' && uri_seg !='email' && uri_seg !='case'){
                taskTable = $('.task-dataTable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "bFilter": true,
                    // "sScrollX": "100%",
                    // "sScrollXInner": "150%",
                    "order": [[0, "DESC"]],
                    "autoWidth": false,
                    "ajax": {
                        url: "<?php echo base_url(); ?>tasks/getTasksData",
                        type: "POST",
                        "data": function (ae) {
                            var status = $('ul#task-navigation').find('li.active').attr('id');
                            if($('#task_reminder').is(':checked') == true)
                            {
                                var reminder = 1;
                            }
                            else
                            {
                                var reminder = '';
                            }
                            var searchBy = {'caseno': $('#caseNo').val()};
                            //var User = { : '<?php echo $this->session->userdata('user_data')['username']  ?>'};
                            searchBy['taskBy'] = status;
                            searchBy['reminder'] = reminder;

                            //console.log(searchBy);
                            $.extend(ae, searchBy);
                            //$.extend(ae, User);
                        }
                    },
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).attr('id', aData[9]);
                        if (aData[aData.length - 2] != '') {
                            var color = aData[aData.length - 2].split('-');
                            if (color[0] != '') {
                                $(nRow).css({'background-color': color[0], 'color': color[1]});
                            } else {
                                $(nRow).css({'background-color': '#ffffff', 'color': '#000000'});
                            }
                        }
                    },
                    "columnDefs": [{
                            "render": function (data, type, row) {
                                if (data != null) {
                                    var button = '';
                                    var button1='';
                                    if (row[8] == '') {
                                        if(row[1]==='<?php echo isset($username)?$username:$this->session->userdata('user_data')['username']; ?>') {
                                            button1 ='<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + data + ')"><i class="fa fa-trash"></i></button>';
                                                }
                                        button = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + data + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;'+button1;
                                        button += '&nbsp;&nbsp;<button class="btn btn-primary btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + data + ')"><i class="fa fa-check"></i></button>';
                                        if(row[11] != 0)
                                        {
                                           button += '&nbsp;&nbsp;<a class="btn btn-primary btn-circle-action btn-circle" title="PullCase" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/'+row[11]+'"><i class="fa fa-folder-open"></i></a>';
                                        }
                                    }
                                }
                                return button;
                            },
                            "targets": 9,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        {
                            targets: 6,
                            className: 'textwrap-line'
                        }],
                });
                }*/
                if(uri_seg !='rolodex' && uri_seg !='dashboard' && uri_seg !='email' && uri_seg !='case'){
                taskTable1 = $('.task-dataTable1').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "bFilter": true,
                    /*"sScrollX": "100%",
                    "sScrollXInner": "150%",*/
                    "order": [[3, "asc"]],
                    "autoWidth": false,
                    "ajax": {
                        url: "<?php echo base_url(); ?>tasks/getTasksData",
                        type: "POST",
                        "data": function (ae) {

                            searchBy=searchBy1;
                            if($('#task_reminder').is(':checked') == true)
                            {
                                var reminder = 1;
                            }
                            else
                            {
                                var reminder = '';
                            }
                            searchBy['reminder'] = reminder;
                            var status = $('ul#task-navigation').find('li.active').attr('id');
                            searchBy['taskBy'] = status;
                            $.extend(ae, searchBy);
                        }
                    },
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        $(nRow).attr('id', aData[9]);
                        if (aData[aData.length - 2] != '') {
                            var color = aData[aData.length - 2].split('-');
                            if (color[0] != '') {
                                $(nRow).css({'background-color': color[0], 'color': color[1]});
                            } else {
                                $(nRow).css({'background-color': '#ffffff', 'color': '#000000'});
                            }
                        }
                    },
                    "columnDefs": [{
                            "render": function (data, type, row) {
                                if (data != null) {
                                    var button = '';
                                    if (row[8] == '') {
                                        button = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + data + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + data + ')"><i class="fa fa-trash"></i></button>';
                                        button += '&nbsp;&nbsp;<button class="btn btn-info btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + data + ')"><i class="fa fa-check"></i></button>';
                                        if(row[11] != 0)
                                        {
                                           button += '&nbsp;&nbsp;<a class="btn btn-info btn-circle-action btn-circle" title="PullCase" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/'+row[11]+'"><i class="fa fa-folder-open"></i></a>';
                                        }
                                    }
                                }
                                return button;
                            },
                            "targets": 9,
                            "bSortable": false,
                            "bSearchable": false
                        },
                        {
                            targets: 6,
                            className: 'textwrap-line'
                        }],
                });
                }
                    
               $('.task-dataTable tbody').on('click', 'tr', function () {

                    if ($(this).hasClass('table-selected')) {
                        $(this).removeClass('table-selected');
                        selectedArray.splice($.inArray(this.id, selectedArray), 1);
                    } else {
                        //table.$('tr.selected').removeClass('selected');
                        selectedArray.push(this.id);
                        //$(this).addClass('table-selected');
                    }
                });
                /*$('.confirm').on('click',function(){
                    $('#saveCompletedTask').removeAttr('disabled');
                });*/
                /* Task Table End*/
                setCalendarValidation();

                $.fn.modal.Constructor.prototype.enforceFocus = function () {};

                $('.contact').mask("(000) 000-0000");
                $('.social_sec').mask("000-00-0000");

                $('#addcontact.modal').on('show.bs.modal', function () {
                    document.getElementById('imgerror').innerHTML = '';
                    document.getElementById('hiddenimg').value = '';
                    //document.getElementById('dispProfile').src= '';

                    $(".chosen-language").chosen();
                });

                $('#addRolodexForm a[data-toggle="tab"]').on('click', function (e) {
                    /*if (!$('#addRolodexForm').valid() && $('input[name="card_cardcode"]').val() == '') {
                        e.preventDefault();
                        return false;
                    }*/
                    setValidation();
                    e.preventDefault();
                });

                $('#addcontact.modal').on('hidden.bs.modal', function () {
                    $('#addcontact.modal-title').text('Add New Contact');
                    $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                        this.value = '';
                    });
                    document.getElementById('profilepicDisp').style.display = 'none';
                    document.getElementById('dispProfile').src= '';
                    $('#addcontact a[href="#name"]').tab('show');
                    var addContactValidator = setValidation();
                    addContactValidator.resetForm();
                });

                /* Start Calendar Modal Hide/Show */
                //Stop tab change when error occured
                $('#addcalendar a[data-toggle="tab"]').on('click', function (e) {
                    if($('#cal_typ').length > 0)
                    {
                        var sel_val = $('#cal_typ').val();
                        $("#todo").val(sel_val).trigger('change');
                    }
                    if (!$('#calendarForm').valid() && $('input[name="eventno"]').val() == '0') {
                        e.preventDefault();
                        return false;
                    }
                });


                $('#addcalendar.modal').on('show.bs.modal', function () {
                    var atrid = $('#CalAddEvt').attr('data-attr');
                    /*if(atrid == 0)
                    {
                       $('.claender_event').attr("disabled", "disabled");
                    }*/
                });
                //set Default Tab when open the Calendar Modal
                $('#addcalendar.modal').on('hidden.bs.modal', function () {
                    $('#addcalendar h4.modal-title').text('Add Appointment/Event To Calendar');
                    $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                        this.value = '';
                    });
                    var validator = setCalendarValidation();
                    validator.resetForm();
                    $('input[name=eventno]').val(0);
                    $('#addcalendar a[href="#general1"]').tab('show');

                    $('#addcalendar .tab-content .tab-pane').not('#general1').removeClass('active');
                     $('#addcalendar .tab-content div#general1').addClass('active');
                });
                /*End Calendar Modal Hide/Show*/
                //Add and Edit Rolodex
                $('#addContact').on('click', function () {
                   // alert("hi");
                    var cardcodeVal = $('input[name="card_cardcode"]').val();

                    if (cardcodeVal == '') {
                        if (!$('#addRolodexForm').valid()) {
                            return false;
                        }
                    }

                    if (!$('#addRolodexForm').valid()) {
                        return false;
                    }
                    $.blockUI();




                    var url = '<?php echo base_url() . "rolodex/addCardData" ?>';
                    if ($('input[name="card_cardcode"]').val() != '') {
                        url = '<?php echo base_url() . "rolodex/editCardData" ?>';
                    }
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: $('#addRolodexForm').serialize(),
                        success: function (result) {
                            var data = $.parseJSON(result);
                            var editCardcode = data.editcardid;
                            if($('#profilepic').val() != '')
                            {
                                var old_img = document.getElementById('hiddenimg').value;
                                var images='';
                                var file_data = $('#profilepic').prop('files')[0];
                                var form_data = new FormData();
                                   form_data.append('file', file_data);
                                   $.ajax({
                                       url: '<?php echo base_url(); ?>rolodex/updateprofilepic/'+editCardcode+'/'+old_img, // point to server-side controller method
                                       dataType: 'text', // what to expect back from the server
                                       cache: false,
                                       contentType: false,
                                       processData: false,
                                       data: form_data,
                                       type: 'post',
                                       success: function (response) {

                                       if(response)
                                        {
                                            images=response;
                                            var plink= '<?php echo base_url() ?>assets/rolodexprofileimages/'+images;
                                                setTimeout(function()
                                                {
                                                    if(images != null){
                                                        if($('#applicantImage').length ==0){
                                                        $('.client-img').find('img.img-circle').attr("src",plink);
                                                     }
                                                   else
                                                   { $('#applicantImage').remove();
                                                   $('.client-img').append('<center><img class="img-circle" height="67px" src="'+plink+'" /></center>'); }
                                                    }
                                                    else{

                                                     }
                                                 }, 500);
                                        }
                                      },
                                       error: function (response) {
                                          //alert(response);
                                       }
                                   });
                            }
                            $.unblockUI();


                            if (data.status == '1') {
                                //swal("Success !", data.message, "success");
                                $('#addRolodexForm')[0].reset();
                                $('#addcontact').modal('hide');
                                $('#addcontact .modal-title').html('Add New Contact');

                                //$("#new_case").val()
                        if((data.caseid != 0 || data.caseid != 'undefined')){
                                var caseid = $("#hdnCaseId").val();
                                if (data.caseid != 0 && $("#new_case").val() == 1) {
                                    var url = "<?php echo base_url() ?>cases/case_details/" + data.caseid;
                                    setTimeout(function()
                                    {
                                        window.location.href = url;
                                    }, 3000);
                                }
                                //alert($("#hdnCaseId").val());
                                 //alert(cardcodeVal);
                                 //alert($("#new_case").val());
                                 if (cardcodeVal != '' || $("#new_case").val() == 'parties') {

                                    $.ajax({
                                        url: '<?php echo base_url() . "cases/case_details" ?>',
                                        method: "POST",
                                        data: {'caseId': $("#hdnCaseId").val()},
                                        success: function (result) {
                                            var res = $.parseJSON(result);
                                            setTimeout(function()
                                            {
                                           // if (res.display_details.firm == "") {
                                                    $(".applicant_name").html("<b>"+res.display_details.salutation + " "+res.display_details.first + " " + res.display_details.last+"</b>");
                                                    //} else {
                                                      //  $(".applicant_name").html(res.display_details.firm);
                                                    //}
                                                    if(res.display_details.popular)
                                                    {
                                                       if((res.display_details.popular).length < 10) {
                                                           var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by '+res.display_details.popular+'</b></span><br/>';
                                                        }
                                                        else
                                                        {
                                                          var htm = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by '+ res.display_details.popular.substr(1, 10)+'..</b></span><br/>';
                                                        }
                                                        //alert(htm);
                                                        if ($('.popular').length > 0) {
                                                            $( ".popular" ).replaceWith(htm);
                                                        }
                                                        else
                                                        {
                                                            $( ".applicant_name" ).after(htm);
                                                        }

                                                    }
                                                    $(".applicant_address").html(res.display_details.address1);
                                                    $(".applicant_address2").html(res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);

                                                    $('#address_map').attr('href', "https://maps.google.com/?q=" + res.display_details.address1 +''+res.display_details.address2+" "+res.display_details.city + " " + res.display_details.state + " " + res.display_details.zip);
                                                    $(".home").html(res.display_details.home);
                                                    if (res.display_details.car == "")
                                                        $(".business").html(res.display_details.business);
                                                    else
                                                        $(".business").html(res.display_details.car);

                                                    $(".email").html(res.display_details.email);
                                                    var dob = new Date(res.display_details.birth_date);
                                                    var today = new Date();
                                                    var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                                                   // console.log(age);
                                                    $(".birth_date").html('Born on ' +  moment(res.display_details.birth_date).format('MM/DD/YYYY')+' ,'+  age  + 'years old');
                                                    //parties tab
                                                    $("#party_name").html(res.display_details.first + " " + res.display_details.last);
                                                    $("#party_beeper").val(res.display_details.beeper);
                                                    var p_tr_id= $('table#party-table-data').find('.table-selected');
                                                    p_tr_id= p_tr_id.attr('id');
                                                    $("#"+p_tr_id).find(".parties_name").html(res.display_details.first + ", " + res.display_details.last);
                                             }, 1000);
                                        }
                                    });
                                }

                                }

                                rolodaxdataTable.draw(false);

                            } else {
                                swal("Oops...", data.message, "error");
                              //  window.location.reload();
                            }

                        }
                    });

                    return false;
                });


                // Calendar Filter options
                $('#getPriority').on('change', function () {
                    var selectedValue = $(this).val();
                    taskTable.columns(5).search(JSON.stringify({'priority': selectedValue})).draw();
                });

                $('#gettaskStatus').on('change', function () {
                    var taskStatus = $(this).val();
                    //console.log(taskStatus);
                    taskTable.columns(8).search(JSON.stringify({'task-status': taskStatus})).draw();
                });



                $('#btn-taskPrint').on('click', function () {

                    //if($('input[name=print_type]').val() == 3) {}

                    $.ajax({
                        url: '<?php echo base_url() . 'tasks/printTask' ?>',
                        method: "POST",
                        //dataType: 'json',
                        data: $('form#taskPrint').serialize() + '&searchBy=' + JSON.stringify(searchBy) + '&selectedData=' + selectedArray,
                        success: function (result) {

                            $(result).printThis({
                                debug: false,
                                header: "<h2><center>"<?php echo APPLICATION_NAME; ?>" | Tasks </center></h2>"
                            });
                        }
                    });

                });



                //Validations for billing defaults
                $("#billing_defaults").validate({
                    submitHandler: function (form, event) {
                        event.preventDefault();
                        $("#caseNo").val($("#hdnCaseId").val());
                        var alldata = $("form#billing_defaults").serialize();
                        //console.log(alldata);
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/setBillingDefaults',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (res) {
                                //console.log(data);
                                $("#adddefault").modal("hide");
                                var data = $.parseJSON(res);
                                if (data != 0) {
                                    var actno = $("#activity_no").val();
                                    //alert(actno);
                                    if (actno == "") {
                                        if (data.freezedt != '1970-01-01 05:30:00' && data.freezedt != '1899-12-30 00:00:00' && data.freezedt != '0000-00-00 00:00:00') {
                                            $('.freezeUntil').html(data.default_freeze_until);
                                            $('.freezeUntil').parent().removeClass('hidden');
                                        }
                                        $("#activity_entry_type").val(data.default_entry_type).trigger("change");
                                        var activityText = $("#activity_entry_type option:selected").text();
                                        $("#activity_short_desc").html(activityText);
                                        $("#activity_event_desc").html(activityText);
                                        $('#activity_teamno').val(data.default_teamno);
                                        $('#activity_staff').val(data.default_staff).trigger("change");
                                       // $('.dateoflastpayment').html(data.default_payment_dt);
                                        if (data.default_payment_dt != '1970-01-01 05:30:00' && data.default_payment_dt != '1899-12-30 00:00:00' && data.default_payment_dt != '0000-00-00 00:00:00') {
                                            $('.dateoflastpayment').html(data.default_payment_dt);
                                            $('.dateoflastpayment').parent().removeClass('hidden');
                                        }
                                        $('#activity_latefee').html(data.default_late_fee);
                                        $('#activity_bi_rate').val(data.default_hr_rate);
                                    }
                                    /*$("#activity_entry_type").val(data.default_entry_type).trigger('change');
                                     $("#activity_teamno").html(data.default_teamno);*/


                                    swal("Success !", "Billing defaults set successfuly", "success");
                                } else {
                                    swal("Failure !", "Fail to set billing defaults", "error");
                                }
                            }
                        });

                    }
                });

                /*jQuery.validator.addMethod("greaterThan",
                    function(value, element, params) {
                        if($(params).val() != '')
                        {
                            if (!/Invalid|NaN/.test(new Date(value))) {
                                return new Date(value) > new Date($(params).val());
                            }

                            return isNaN(value) && isNaN($(params).val())
                                || (Number(value) > Number($(params).val()));
                        }
                        else
                        {
                            return true;
                        }
                    },'Must be greater than Reminder Date.');*/

                 /* var validator = $('#addTaskForm1').validate({
                    ignore: [],
                    rules: {
                        whofrom: {required: true},
                        whoto3: {required: true},
                        datereq: {required: true},
                        event: {required: true, noBlankSpace: true},
                        datereq: { greaterThan: ".rdate" },
                    },
                });*/
                $('#add_new_case_activity').validate({
                    ignore: [],

                    rules: {
                        activity_event: {
                            required: true
                        },

                    },
                    messages: {
                        activity_event: {required: "Event Description is Required"},

                    },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');
                        //alert(tab);
                         $('#addcaseact a[href="#' + tab + '"]').tab('show');
                    }
                });
                 $.validator.addMethod("phoneUSA", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
       return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
    }, "Please specify a valid phone number");
    $("#prospectForm").validate({
                    ignore: [],
                    rules: {
                        pro_first:{
                            required: true,
                        },
                        pro_last:{
                            required: true,
                        },
                        pro_type: {
                            required: true,
                         },
                        pro_business: {phoneUSA: true},
                        pro_home: {phoneUSA: true},
                        pro_cell: {phoneUSA: true},
                        pro_social_sec: {ssId: true},
                        pro_state:{stateUS: true},
                        pro_zip:{zipcodeUS: true},
                        pro_caseno:{
                            remote: {
                                url:'<?php echo base_url(); ?>prospects/CheckCaseno',
                                type: "post",
                                data: {
                                   pro_caseno: function() {
                                    return $('#prospectForm :input[name="pro_caseno"]').val();
                                   }
                                }
                            } ,
                            digits:true
                        },
                    },
                    messages: {
                        pro_first: {
                            required: "Please provide first name"
                        },
                        pro_last: {
                            required: "Please provide last name"
                        },
                        pro_type: {
                            required: "Please select Category"
                        },
                        pro_caseno:{
                            remote:"This caseno is not exist"
                        },
                        pro_state:{stateUS: "Invalid USA state abbreviation"},

                    },
                    submitHandler: function (form) {
                        return false;
                    },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');

                        $('#proceedprospects a[href="#' + tab + '"]').tab('show');
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "pro_type") {
                            error.insertAfter(element.next());
                        } else {
                            error.insertAfter(element);
                        }
                    }

    });
                $('.dtpickDisDayofWeek').datetimepicker({
                    daysOfWeekDisabled: [0],
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    startDate: new Date(),
                });

                $('.datepickerClass').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date(),
                });
                $('#casedate_closed').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date(),
                });
                $('#add_case_time').mdtimepicker({
                    autoclose: true,
                    twelvehour: true
                });

                $('#activity_entry_type').change(function () {
                    var data_key = $('#activity_entry_type option:selected').data('key');
                    $(".billingtab").addClass("hidden");
                    $("#" + data_key).removeClass("hidden");
                });

                var data_key = $('#activity_entry_type option:selected').data('key');
                $(".billingtab").addClass("hidden");
                $("#" + data_key).removeClass("hidden");

                //Dropzone.autoDiscover = false;
                Dropzone.options.addNewCaseActivity = {
                    autoProcessQueue: false,
                    uploadMultiple: true,
                    parallelUploads: 11,
                    maxFiles: 11,
                    addRemoveLinks: true,
                    uploadprogress: true,
                    enqueueForUpload: false,
                    paramName: 'document',
                    acceptedFiles: "image/jpeg,application/docx,application/pdf,text/plain,application/msword",


                    // Dropzone settings
                    init: function () {
                        myDropzone = this;

                     this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                            $.blockUI();

                            $("#caseActNo").val($("#hdnCaseId").val());

                            e.preventDefault();
                            e.stopPropagation();

                            var form = $(this).closest('#add_new_case_activity');
                            if (form.valid() == true) {
                              //  $(this).prop('disabled', true);

                                if (myDropzone.getQueuedFiles().length > 0) {
                                    myDropzone.processQueue();
                                } else {
                                    myDropzone.uploadFiles([]); //send empty
                                }
                            } else {
                                $.unblockUI();
                            }

                        });//}

                        this.on("maxfilesreached", function () {
                            swal("Failure !", "Max file allowed exceeded", "error");
                            if (this.files[1] != null) {
                                this.removeFile(this.files[0]);
                            }
                        });

                        this.on("addedfile", function (file) {
                          /*  if(!confirm("Do you want to upload the file?")){
                             this.removeFile(file);
                             return false;
                             }*/
                            if (this.files.length) {
                                if($('#activity_event').val() == '') {
                                    $('#activity_event').val(file.name.replace(".msg", ""));
                                }
                                var _i, _len;
                                for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
                                    if (this.files[_i].name === file.name) {
                                        this.removeFile(file);
                                    }
                                }
                            }
                        }),
                                this.on("success", function (file, responseText) {
                                });
                        this.on("sendingmultiple", function () {
                        });
                        this.on("successmultiple", function (file, responseText) {

                            $.unblockUI();
                            var html = '';
                            var view = '';
                            $("#addcaseact").modal("hide");
                            var data = JSON.parse(responseText);
                            var link ='<?php echo base_url()?>'+'assets/clients/' + $('#caseNo').val() +'/'+data.file_name;
                            if (file.length == 0) {

                                if ((file.length + data.fileExist) > 1) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0);"><i class = "icon fa fa-eye"></i> View All</a>';
                                } else if ((file.length + data.fileExist) > 0) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_attach"  download='+data.file_name+'  data-actid=' + data.actno + ' href="'+link+'"><i class = "icon fa fa-eye"></i> View</a>';
                                } else {
                                    view = '';
                                }
                            } else {
                                if (data.fileExist > 1) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0);"><i class = "icon fa fa-eye"></i> View All</a>';
                                } else if (data.fileExist > 0) {
                                    view = '<a class="btn btn-xs btn-primary marginClass view_attach" download='+data.file_name+' data-actid=' + data.actno + ' href="'+link+'"><i class = "icon fa fa-eye"></i> View</a>';
                                } else {
                                    view = '';
                                }
                            }



                            html += "<tr id=caseAct" + data.actno + "><td>" + data.actData.add_case_cal + "</td>";
                            html += "<td>" + data.actData.add_case_time + "</td>";
                            html += "<td>" + data.actData.activity_event + "</td>";
                            html += "<td>" + data.actData.last_ini + "</td>";
                            html += "<td>" + data.actData.orignal_ini + "</td>";
                            html += "<td>" + view + "</td>";
                            html += "<td>";
                            html += "<a class='btn btn-xs btn-info marginClass activity_edit' data-actid=" + data.actno + " href = 'javascript:;'><i class = 'icon fa fa-paste'></i> Edit</a>";
                            html += "<a class='btn btn-xs btn-danger marginClass activity_delete' data-actid=" + data.actno + " href = 'javascript:;'><i class = 'icon fa fa-times'></i> Delete</a>";
                            html += "</td>";
                            html += "</tr>";
                            //swal("Success !", "New activity added successfuly", "success");
                            swal({
                                    title: "Success!",
                                    text: "Activity List updated successfuly",
                                    type: "success"
                                },
                                  function()
                                  {
                                      location.reload();
                                  }
                                );

                            if (data.status == 2) {
                                if (caseactTable.rows('[id=caseAct' + data.actno + ']').any()) {
                                    caseactTable.rows($('tr#caseAct' + data.actno)).remove();
                                }
                                caseactTable.row.add($(html)).draw();

                                //swal("Success !", "Activity updated successfuly", "success");
                                swal({
                                    title: "Success!",
                                    text: "Activity updated successfuly",
                                    type: "success"
                                },
                                  function()
                                  {
                                      location.reload();
                                  }
                                );
                            }else if (data.status == 1) {
                                caseactTable.row.add($(html)).draw();
                            } else {
                                swal("Failure !", "Fail to add activity", "error");
                            }
                            //location.reload();
                            document.getElementById("add_new_case_activity").reset();
                        });
                        this.on("errormultiple", function (files, response) {
                        });
                        this.on("completemultiple", function (file) {
                            this.removeAllFiles(true);
                        });
                    }

                }


                rolodexSearchDatatable = $('#rolodexPartiesResult').DataTable({
                    "lengthMenu": [5, 10, 25, 50, 100]
                });
                rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                    "scrollX": true,
                    //"destroy": true,
                    "lengthMenu": [5, 10, 25, 50, 100]
                });


                duplicateDatatable = $('#duplicateEntries').DataTable();

                $("#rolodex_search_parties_form").validate({
                    //place all errors in a <div id="errors"> element
                    /*errorPlacement: function(error, element) {
                     error.appendTo("div#errors");
                     },*/
                    rules: {
                        rolodex_search_text: {
                            required: true
                        },
                        rolodex_search_parties: {
                            required: true
                        }
                    },
                    messages: {
                        rolodex_search_text: {
                            required: "Please provide search text"
                        },
                        rolodex_search_parties: {
                            required: "Please provide search by"
                        }
                    },
                    submitHandler: function (form, event) {
                        event.preventDefault();

                        var alldata = $("form#rolodex_search_parties_form").serialize();
                        //console.log(alldata);
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/searchForRolodex',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                                var text = $("#rolodex_search_text").val();
                                var substring = text.substr(0, 1);
                                var radioVal = $("input[name='rolodex_search_parties']:checked").val();
                                //alert(radioVal);alert(substring);
                                if (substring == "<") { //radioVal == "rolodex-cardno" &&

                                    var res = $.parseJSON(data);
                                    $("#searched_party_fullname").val(res[0].fullname);
                                    $("#searched_party_firmname").val(res[0].firm);
                                    $('#searched_party_type').html(res[0].type);
                                    $('#searched_party_cardnumber').html(res[0].cardcode);
                                    $(".addSearchedParties").trigger("click");

                                }

                                rolodexSearchDatatable.clear();

                                //rolodexSearchDatatable
                                var obj = $.parseJSON(data);
                                $.each(obj, function (index, item) {
                                    var html = '';
                                    html += "<tr class='viewPartiesDetail' data-cardcode=" + item.cardcode + "><td>" + item.name + "</td>";
                                    html += "<td>" + item.firm + "</td>";
                                    html += "</tr>";
                                    rolodexSearchDatatable.row.add($(html));
                                });
                                rolodexSearchDatatable.draw();
                            }
                        });

                    }
                });
                //company validation
                $("#companylistForm").validate({
                 rules: {
                 rolodex_search_text_name: {
                 required: true,
                 noBlankSpace: true
                 }
                 },
                 messages: {
                 rolodex_search_text_name: {
                 required: "Please enter rolodex name "
                 }
                 },
                 submitHandler: function (form, event) {
                 event.preventDefault();
                 }
                 });
                $("#addNewCase").validate({
                    rules: {
                        first_name: {
                            required: true,
                            noBlankSpace: true
                        },
                        last_name: {
                            required: true,
                            noBlankSpace: true
                        }
                    },
                    messages: {
                        first_name: {
                            required: "Please provide first name"
                        },
                        last_name: {
                            required: "Please provide last name"
                        }
                    },
                    submitHandler: function (form, event) {
                        // console.log('In');
                        //return false;
                        setValidation();
                        event.preventDefault();
                        $("#caseNo").val($("#hdnCaseId").val());
                        var alldata = $("form#addNewCase").serialize();
                        //console.log(alldata);
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/checkDuplicateCase',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (result) {
                                var obj = $.parseJSON(result);
                                duplicateDatatable.rows().remove().draw();
                                if (obj.status == '1') {
                                         $("#addcase").modal("hide");
                                    $.each(obj.data, function (index, item) {
                                     //   console.log(item);
                                        var html = '';
                                        html += "<tr class='selectDuplicateData' data-cardcode=" + item.cardcode + ">";
                                        html += "<td>" + item.caseno + "</td>";
                                        html += "<td>" + item.type + "</td>";
                                        html += "<td>" + item.casestat + "</td>";
                                        html += "<td>" + item.dateenter + "</td>";
                                        html += "<td>" + item.social_sec + "</td>";
                                        html += "<td>" + item.casetype + "</td>";
                                        html += "<td>" + item.caption1 + "</td>";
                                        html += "</tr>";
                                        duplicateDatatable.row.add($(html));

                                    });

                                    duplicateDatatable.draw();
                                    if (obj.data.length == 1) {
                                        $("#duplicateEntries tr.selectDuplicateData").addClass("highlight");
                                        $('#edit_new_case').prop('disabled', false);
                                        $('#attach_name').prop('disabled', false);
                                    }
                                     $('input[name=first_name]').val($("#first_name").val());
                                    $('input[name=last_name]').val($("#last_name").val());
                                    $("#duplicateData").modal("show");
                                } else {
                                    if($("#prospect_id").val()!='')
                                    {
                                       $.ajax({
                                                url: '<?php echo base_url(); ?>prospects/getProspectsdata',
                                                method: "POST",
                                                data:{prospect_id:$("#prospect_id").val()},
                                                beforeSend: function (xhr) {
                                                    $.blockUI();
                                                },
                                                complete: function (jqXHR, textStatus) {
                                                    $.unblockUI();
                                                },
                                                success: function(result) {
                                                    var data = $.parseJSON(result);


                                                        $('#card_salutation').val(data.data.salutation);
                                                        $('#card_suffix').val(data.data.suffix);
                                                        $('#card_type').val(data.data.type);
                                                        $('input[name=card_first]').val(data.data.first);
                                                        $('input[name=card_last]').val(data.data.last);
                                                        $('input[name=card2_address1]').val(data.data.address1);
                                                        $('input[name=card2_city]').val(data.data.city);
                                                        $('input[name=card2_state]').val(data.data.state);
                                                        $('input[name=card2_zip]').val(data.data.zip);

                                                        //Tab - 2 Data
                                                        $('input[name=card_business]').val(data.data.business);
                                                        $('input[name=card_home]').val(data.data.home);
                                                        $('input[name=card_car]').val(data.data.cell);
                                                        $('input[name=card_email]').val(data.data.email);

                                                        ////Tab - 3 Data
                                                        $('input[name=card_social_sec]').val(data.data.social_sec);
                                                        if (data.data.birthdate == 'NA') {
                                                            $('input[name=card_birth_date]').val('');
                                                        } else {
                                                            $('input[name=card_birth_date]').val(moment(data.data.birthdate).format('MM/DD/YYYY') );
                                                        }

                                                        $("#new_case").val(1);
                                                        $("#prospect_id").val($("#prospect_id").val());

                                                        $("#addcase").modal("hide");
                                                        $("#addRolodexForm select").trigger('change');
                                                        $('select[name=card_language]').val('English').trigger("change");
//                                                        $("select[name=card_language]").trigger('chosen:updated');
                                                        $("#addcontact").modal("show");
                                                }
                                        });


                                    }
                                    else
                                    {
                                            $('input[name=card_first]').val($("#first_name").val());
                                            $('input[name=card_last]').val($("#last_name").val());
                                            $("#new_case").val(1);
                                            $("#addcase").modal("hide");
                                            $("#addRolodexForm select").trigger('change');
                                            $('select[name=card_language]').val('English').trigger("change");
//                                            $("select[name=card_language]").trigger('chosen:updated');
                                            $("#addcontact").modal("show");
                                    }
                                }
                            }
                        });


                    }
                });

                //Edit Case
                $('#savecase').on('click', function () {

                    var alldata = $("form#edit_case_details").serialize();
                    var rbName = $("#case_reffered_by option:selected").text();
                    var nwCaption = $("#editcase_caption").text();
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/editCase',
                        method: 'POST',
                        data: alldata,
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (result) {
                            var data = $.parseJSON(result);
                            $.unblockUI();
                            if (data.status == '1') {
                                swal("Success !", data.message, "success");
                                $('#edit_case_details')[0].reset();
                                $('#editcasedetail').modal('hide');
                                $('#editcasedetail .modal-title').html('Edit Case');
                            } else if (data.status == '0') {
                                swal("Warning !", data.message, "error");
                                $('#edit_case_details')[0].reset();
                                $('#editcasedetail').modal('hide');
                                $('#editcasedetail .modal-title').html('Edit Case');
                            }
                            $.ajax({
                                url: '<?php echo base_url() . "cases/case_details" ?>',
                                method: "POST",
                                data: {'caseId': $("#hdnCaseId").val()},
                                success: function (result) {
                                    var res = $.parseJSON(result);

                                    $(".casetype").html(res.display_details.casetype);
                                    //$(".casesubtype").html(res.display_details.casetype);
                                    $(".location").html(res.display_details.location);
                                    $(".casestat").html(res.display_details.casestat);
                                    $(".yourfileno").html(res.display_details.yourfileno);
                                    $(".venue").html(res.display_details.first);

                                    $(".atty_resp").html(res.display_details.atty_resp);
                                    $(".para_hand").html(res.display_details.para_hand);
                                    $(".atty_hand").html(res.display_details.atty_hand);
                                    $(".sec_hand").html(res.display_details.sec_hand);
                                    if(res.display_details.caption1.length > 55)
                                        $(".caption #caption_details").html(res.display_details.caption1.substring(0,55)+'...');
                                    else
                                        $(".caption #caption_details").html(res.display_details.caption1);
                                    $(".casesubtype").html(res.display_details.udfall);

                                    if ($('div.sticky.caption').hasClass('caption-closed')) {
                                        $('div.sticky.caption').html(res.display_details.caption1 + ' - ' + 'CLOSED');
                                    }

                                    var dateenter = 'NA';
                                    var dateopen = 'NA';
                                    var followup = 'NA';
                                    var dateclosed = 'NA';
                                    var dr = 'NA';
                                    var psdate = 'NA';

                                    if (res.display_details.dateenter == '1899-12-31 00:00:00' || res.display_details.dateenter == '1899-12-30 00:00:00' || res.display_details.dateenter == '1970-01-01 00:00:00') {
                                        dateenter = 'NA';
                                    } else {
                                        dateenter = moment(res.display_details.dateenter).format('MM/DD/YYYY');
                                    }

                                    if (res.display_details.dateopen == '1899-12-31 00:00:00' || res.display_details.dateopen == '1899-12-30 00:00:00' || res.display_details.dateopen == '1970-01-01 00:00:00') {
                                        dateopen = 'NA';
                                    } else {
                                        dateopen = moment(res.display_details.dateopen).format('MM/DD/YYYY');
                                    }

                                    if (res.display_details.followup == '1899-12-31 00:00:00' || res.display_details.followup == '1899-12-30 00:00:00' || res.display_details.followup == '1970-01-01 00:00:00') {
                                        followup = 'NA';
                                    } else {
                                        followup = moment(res.display_details.followup).format('MM/DD/YYYY');
                                    }


                                    if (res.display_details.dateclosed == '1899-12-31 00:00:00' || res.display_details.dateclosed == '1899-12-30 00:00:00' || res.display_details.dateclosed == '1970-01-01 00:00:00') {
                                        dateclosed = 'NA';
                                    } else {
                                        dateclosed = moment(res.display_details.dateclosed).format('MM/DD/YYYY');
                                    }

                                    if (res.display_details.d_r == '1899-12-31 00:00:00' || res.display_details.d_r == '1899-12-30 00:00:00' || res.display_details.d_r == '1970-01-01 00:00:00') {
                                        dr = 'NA';
                                    } else {
                                        dr = moment(res.display_details.d_r).format('MM/DD/YYYY');
                                    }

                                    if (res.display_details.psdate == '1899-12-31 00:00:00' || res.display_details.psdate == '1899-12-30 00:00:00' || res.display_details.psdate == '1970-01-01 00:00:00' || res.display_details.psdate == '') {
                                        psdate = 'NA';
                                    } else {
                                        psdate = moment(res.display_details.psdate).format('MM/DD/YYYY');
                                    }

                                    $(".dateenter").html(dateenter);
                                    $(".dateopen").html(dateopen);
                                    $(".followup").html(followup);
                                    $(".d_r").html(dr);
                                    $(".rb").html(rbName);
                                    $(".ps").html(psdate);
                                    $(".dateclosed").html(dateclosed);
                                    //$(".").html(psdate);
                                    $(".notes").html(res.category_array.quickNote);
                                }
                            });
                        }
                    });
                });


                $("#parties_case_specific_data").validate({

                    submitHandler: function (form, event) {
                        event.preventDefault();
                        $("#caseNo").val($("#hdnCaseId").val());
                        var alldata = $("form#parties_case_specific_data").serialize();
                       //console.log(alldata);
                         $('#partiesCardCode').val();
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/setPartiesCaseSpecific',
                            method: 'POST',
                            data: alldata,
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (res) {
                                var data = $.parseJSON(res);
                                //console.log(data);
                                if (data.status == '1') {

                                    $.ajax({
                    url: '<?php echo base_url(); ?>cases/getCaseSpecific',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: $('#partiesCardCode').val(),
                        caseno: $('#caseNo').val(),
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {

                     // alert(data[0].notes);
                        $("#party_type").html(data[0].type);
                        $("#party_side").html(data[0].side);
                        $("#party_offc_no").html(data[0].officeno);
                        $("#party_notes").html(data[0].notes);
                     //   (data[0].flags != '1') ? $('#parties_party_sheet').iCheck('uncheck') : $('#parties_party_sheet').iCheck('check');
                    }
                });
                swal("Success !", data.message, "success");

                                } else {
                                    swal("Alert !", data.message, "warning");
                                }

                                $('#caseSpecific').modal('hide');
                            }
                        });

                    }
                });

//                swalFunction = function () {
//                    swal({
//                        title: "If you make changes in the rolodex,it may affect several cases",
//                        type: "warning",
//                        text: "Click Yes : To make changes for this case only" + "\n" + "Click No : To make changes in the rolodex which may affect several cases" + "\n" + "Click Cancel :To Cancel and not make any changes at all",
//                        showCancelButton: true,
//                        confirmButtonColor: "#DD6B55",
//                        confirmButtonText: "Yes",
//                        CancelButtonText: "No",
//                        closeOnConfirm: false
//                    });
//
//                };
                  $('#sfl').tooltip();
                     $("#addInjuryForm").validate({
                                rules: {
                                    field_first: {
                                        required: true,
                                     },
                                   field_last: {
                                        required: true,
                                     },
                                    field_doi2:{required: function(){
                                        if($('#field_doi').val()=="")//return value based on textbox1 status
                                             return true;
                                        else
                                             return false;
                                       }
                                     }
                                 },
                                messages: {
                                    field_first: {
                                        required: "First name required"
                                    },
                                    field_last:{
                                        remote:"Last name required"
                                    },
                                    field_doi2:{
                                        required: "CT Dates is required."
                                    },
                                },
                            });
                  //email validation
        $('#send_mail_form1').validate({
            rules: {
               /* whoto1: {
                    required: function (element) {
                        if ($("input[name=emails1]").val() == '') {
                            return true;
                        } else {
                            validate_ext_email($("input[name=emails1]").val());
                        }
                        return false;
                    }

                },
                emails1: {
                    required: function (element) {
                        if ($('input[name=whoto1]').val() == '') {
                            return false;
                        }
                        return true;
                    }
                },*/
                whoto1:{required: function(){
                    if($('#emails1').val()=="")//return value based on textbox1 status
                         return true;
                    else
                         return false;
                   }
                 },
                mail_subject1: {
                    required: true
                },

            },
            messages: {
                whoto1: {
                    required: 'Please Select a User to send mail to'
                },
                /*emails1: {
                    required: 'Please add email address to send email'
                },*/
                mail_subject1: {
                    required: 'Please enter a Mail Subject'
                },

            },
            errorPlacement: function (error, element) {
                if ($(element).attr('name') == 'whoto1') {
                    $(element).parent().append(error);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
               // alert($('#send_mail_form1').serialize());
                var whoto = [];
                whoto = $('select[name="whoto1"]').val();
                var subject = $('#mail_subject1').val();
                var urgent = $('input[name="mail_urgent1"]:checked').val();
                var ext_emails = $('input[name="emails1"]').val();

                var mailbody = CKEDITOR.instances.mailbody1.getData();
                var filenameVal = $("#filenameVal").val();
                var mailType = $("#mailType1").val();
                var case_no = $("#case_no1").val();
                //var att_file = ;
                var form_data = new FormData();
                form_data.append("whoto", whoto);
                form_data.append("subject", subject);
                form_data.append("urgent", urgent);
                form_data.append("ext_emails", ext_emails);
                form_data.append("mailbody", mailbody);
                form_data.append("mailType", mailType);
                form_data.append("filenameVal", filenameVal);
                form_data.append("caseNo", case_no);
                //var ins = $('input#afiles1')[0].files.length;
                var p =0;
                $.each($('input[name="afiles1[]"]'),function(i,obj){
                    $.each(obj.files,function(x,file){
                    vaild_data = validateAttachFileExtension(file.name);
                    form_data.append("files_"+p, file);
                    p=p+1;
                    })
                });
                /*for (var x = 0; x < ins; x++) {
                    form_data.append("files_" + x, $('#afiles1')[0].files[x]);
                }*/
                //alert(form_data);
                //form_data.append("files",$('input#afiles')[].files);
                $.ajax({
                    url: '<?php echo base_url(); ?>email/send_email',
                    method: 'POST',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,

                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.result == "true") {
                            swal({
                                title: "Success!",
                                text: "Mail is sent successfully.",
                                type: "success"
                            });

                        } else if (data.result == "true" && data.ext_party == '0')
                        {
                            swal({
                                title: "Alert",
                                text: "Oops! Some went wrong. Email wasn't send to External Party.",
                                type: "warning"
                            });
                        } else if (data.result == "false" && data.attachment_issue != '')
                        {
                            swal({
                                title: "Alert",
                                text: "Oops! " + data.attachment_issue,
                                type: "warning"
                            });
                            return false;
                        } else {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again later.",
                                type: "warning"
                            });
                        }
                        $('#CaseEmailModal').modal('hide');
                        caseactTable.draw();
                    }
                });

                return false;
            }
        });
       var adarr='<?php echo  isset($system_caldata[0]->calattyass)?$system_caldata[0]->calattyass:'' ?>';
       if(adarr==3){
               $("select[name=attyass]").attr("disabled", true);
        }
        else{  $("select[name=attyass]").removeAttr("disabled"); }
        });

                $(document.body).on('click', '.activity_edit', function () {
               var id= $(this).data('actid');
                caseActEdit(id);
            });



        //function for case act
        function caseActEdit(id){
            //alert(id);

            var caseNo = $("#caseNo").val();
            $.ajax({
                    url: '<?php echo base_url(); ?>cases/getAjaxActivityDetails',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        actno: id,
                        caseno: caseNo,
                    },
                    success: function (data) {
                        $('#add_new_case_activity')[0].reset();
                        if (data == false) {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        } else {
                                if(data.access=='2'){
                                if(data.initials=='<?php echo $username ?>'){
                                    $('#add_new_case_activity .btn-primary').prop('disabled', false);
                                    $('#addcaseact .modal-title').html('Edit Case Activity');

                                }else {
                                     $('#add_new_case_activity .btn-primary').prop('disabled', true);
                                     $('#addcaseact .modal-title').html('Edit Case Activity      <small>(This Activity is Restricted to be change by '+data.initials0+')</small>');
                                }
                            }
                            else{
                                $('#add_new_case_activity .btn-primary').prop('disabled', false);
                                $('#addcaseact .modal-title').html('Edit Case Activity');
                            }

                            myDropzone.removeAllFiles(true);
                            if (data.document != "") {
                                $.each(data.document, function (index, item) {

                                    var baseUrl = '<?php echo base_url(); ?>' + item;
                                    var mockFile = {name: item, size: 12345, status: 'success'};
                                    myDropzone.emit("addedfile", mockFile);
                                    myDropzone.emit("thumbnail", mockFile, baseUrl);
                                    myDropzone.files.push(mockFile); // file must be added manually

                                    /*var mockFile = { name: item, size: '12345' };
                                     myDropzone.options.addedfile.call(myDropzone, mockFile);
                                     myDropzone.options.thumbnail.call(myDropzone, mockFile, baseUrl);*/
                                });
                            }

                            $('#original_ini').val(data.initials0);
                            var datefrmt = moment(data.date).format('MM/DD/YYYY');
                            var datetime = moment(data.date).format('hh:mm a');
                            var datebicyclefrmt = moment(data.bicycle).format('MM/DD/YYYY');
                            var datepmtdate = moment(data.bipmtdate).format('MM/DD/YYYY');
                            var datepmduedate = moment(data.bipmtduedt).format('MM/DD/YYYY');


                            /*$(".documentAttch").removeClass("hidden");
                             $(".activityNo").html(data.actno);
                             var filePath = "f" + data.actno + ".* (Default Document )";
                             $(".documentPath").html(filePath);*/

                            $("#activity_no").val(data.actno);
                            $('#add_case_cal').val(datefrmt);
                            $('#add_case_time').val(datetime);
                            $('#activity_event').html(data.event);
                            $('#last_ini').val(data.initials);
                            //$('#case_act_atty').val(data.atty).trigger("change");
                            if ($('#case_act_atty option:contains('+ data.atty +')').length) {
                            $('select[name=case_act_atty]').val(data.atty);
                            }
                            else
                            {
                                $('#case_act_atty').append("<option value='" + data.atty + "' selected='seleceted'>" + data.atty + "</option>");
                            }
                            $('#case_act_time').val(data.minutes);
                            $('#case_act_hour_rate').val(data.rate);
                            $('#case_act_cost_amt').val(data.cost);
                            $('#activity_title').val(data.title).trigger("change");


                            var str1 = data.type;
                            if(str1!=null){
                            if(str1.indexOf('OTHER-') != -1){
                            var ret = data.type.replace('OTHER-','');
                             $('#activity_fee').val('OTHER').trigger('change');
                              $('#activity_fee1').val(ret);
                            }}else{
                             $('#activity_fee').val(data.type).trigger('change');
                            }
                            $("input[name=activity_classification][value=" + data.arap + "]").parent('div').addClass('checked');

                            $('#case_category').val(data.category).trigger("change");
                            $('#color_codes').val(data.color).trigger("change");
                            $('#security').val(data.access).trigger("change");
                            if (data.mainnotes != 0) {
                                $('#activity_mainnotes').parent('div').addClass('checked');
                                //$( "#activity_mainnotes" ).trigger( "change" );
                                $('input[name=activity_mainnotes]').prop('checked', true).triggerHandler('click');
                            }
                            if (data.redalert != 0) {
                                $('#activity_redalert').parent('div').addClass('checked');
                                //$( "#activity_redalert" ).trigger( "change" );
                                $('input[name=activity_redalert]').prop('checked', true).triggerHandler('click');
                            }
                            if (data.upontop != 0) {
                                $('#activity_upontop').parent('div').addClass('checked');
                                //$( "#activity_upontop" ).trigger( "change" );
                                $('input[name=activity_upontop]').prop('checked', true).triggerHandler('click');
                            }

                            $('#activity_typeact').val(data.typeact).trigger("change");
                            $('#activity_doc_type').val(data.wordstyle).trigger("change");
                            $('#activity_style').val(data.ole3style).trigger("change");
                            //$('#activity_o_init').val(data.event);
                            //$('#activity_l_init').val(data.event);
                            //$('#activity_last_init').val(data.event);
                            $('#case_act_TM').val(data.frmtm);
                            $('#case_act_BM').val(data.frmbm);
                            $('#case_act_LM').val(data.frmlm);
                            $('#case_act_RM').val(data.frmrm);
                            $('#case_act_PW').val(data.frmwidth);
                            $('#case_act_PH').val(data.frmheight);
                            $('#case_act_orientation').val(data.orient).trigger("change");
                            $('#case_act_copies').val(data.copies);
                            $('#activity_entry_type').val(data.bistyle).trigger("change");
                            $('#activity_teamno').val(data.biteamno);
                            $('#activity_staff').html(data.biowner);
                            $('#activity_short_desc').html(data.bidesc);
                            $('#activity_event_desc').html(data.bicost);
                            $('#activity_biling_cycle').val(datebicyclefrmt);
                            $('#activity_postmark_date').val(datepmtdate);
                            $('#activity_payment_due').val(datepmduedate);
                            //$('#activity_amount').html(data.event);
                            $('#activity_latefee').val(data.bilatefee);
                            $('#activity_check_no').val(data.bicheckno);
                            $('#activity_bi_hour').html(data.event);
                            $('#activity_bi_min').html(data.bitime);
                            $('#activity_bi_rate').html(data.bihourrate);
                            $('#activity_bi_fee').html(data.bifee);
                            $('#activity_bi_latefee').html(data.bilatefee);
                            $('#activity_bi_creditac').html(data.bipmt);
                           // $('#addcaseact .modal-title').html('Edit Case Activity');
                            $('#addcaseact').modal('show');

                        }
                    }
                });
        }

            //Set Rolodex Modal Validation Function
            function setValidation() {
                var validator = $('#addRolodexForm').validate({
                    ignore: [],
                    rules: {
                        card_first: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() == '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_firm: {
                            required: function (element) {
                                if ($("input[name=card_first]").val() == '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_address1: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_city: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        card2_state: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            },
                            stateUS: true
                        },
                        card2_zip: {
                            required: function (element) {
                                if ($("input[name=card2_firm]").val() != '') {
                                    return true;
                                }
                                return false;
                            },
                            zipcodeUS: true
                        },
                        card_type: {
                            required: true
                            /*function (element) {
                                if ($("input[name=card_first]").val() != '') {
                                    return true;
                                }
                                return false;
                            }*/
                        },
                        /*card_middle: {letterwithspace: true},
                        card_last: {letterwithspace: true},*/
                        card_business: {phoneUS: true},
                        card_home: {phoneUS: true},
                        card_fax: {phoneUS: true, maxlength: 15},
                        //card_beeper: {phoneUS: true},
                        card_email: {email: true},
                        card2_phone1: {phoneUS: true},
                        card2_fax: {phoneUS: true},
                        card2_phone2: {phoneUS: true},
                        //                card_licenseno: {usLicenceNo: true},
                        card2_fax2: {phoneUS: true},
                        card2_tax_id: {alphanumeric: true, maxlength: 15},
                        card_social_sec: {ssId: true},
                        card_speciality: {letterswithbasicpunc: true},
                    },
                    messages: {
                        card_first: {required: "Please Fill First Name or Firm Name"},
                        card2_firm: {required: "Please Fill First Name or Firm Name"},
                        card2_fax: {phoneUS:"Please specify a valid fax number"},
                        card2_fax2: {phoneUS: "Please specify a valid fax number"},
                        card_fax: {phoneUS: "Please specify a valid fax number"},
                    },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');

                        $('#addcontact a[href="#' + tab + '"]').tab('show');
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "card_type") {
                            error.insertAfter(element.next());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

                return validator;
            }

            // Set Calendar Modal Validation Function
            function setCalendarValidation() {
                var validator = $('#calendarForm').validate({
                    ignore: [],
                    rules: {
                        calstat: {letterwithspace: true},
                        first: {
                            required: true,
                            letterswithbasicpunc: true
                        },
                        last: {
                            required: true,
                            letterswithbasicpunc: true
                        },
                        defendant: {letterswithbasicpunc: true},
                        judge: {letterswithbasicpunc: true},
                        venue: {letterswithbasicpunc: true},
                        caldate: {required: true},
                        caltime: {required: true},
                        event: {required: true},
                        /*whofrom: {
                            required: function (element) {
                                if ($('input[name=tickledate]').val() != '') {
                                    return true;
                                }
                                return false;
                            }, lettersonly: true, maxlength: 3
                        },
                        whoto: {
                            required: function (element) {
                                if ($('input[name=tickledate]').val() != '') {
                                    return true;
                                }
                                return false;
                            }, lettersonly: true, maxlength: 3
                        },*/
                        caseno: {number: true,
                                remote: {
                            url: '<?php echo base_url(); ?>prospects/CheckCaseno',
                            type: "post",
                            data: {
                                pro_caseno: function () {
                                    return $('#calendarForm :input[name="caseno"]').val();
                                }
                            }
                        },
                        },
                        /*tickledate: {
                            required: function (element) {
                                if ($('input[name=whofrom]').val() != '' || $('input[name=whoto]').val() != '' || $('input[name=tickletime]').val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },
                        tickletime: {
                            required: function (element) {
                                if ($('input[name=tickledate]').val() != '') {
                                    return true;
                                }
                                return false;
                            }
                        },*/
                    },
                    messages: {
                         caseno:{
                            remote:"This caseno is not exist"
                        }
                     },
                    invalidHandler: function (form, validator) {
                        var element = validator.invalidElements().get(0);
                        var tab = $(element).closest('.tab-pane').attr('id');

                        $('#addcalendar a[href="#' + tab + '"]').tab('show');
                    },
                });
                return validator;
            }
            // Add Event in Calendar
            function getCalendarDataByEventno(eventno) {
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>calendar/getCalendarData',
                    dataType: 'json',
                    method: 'post',
                    data: {
                        eventno: eventno,
                    },
                    success: function (res) {
                        var data = res;
                        ///console.log(data);
                        //var data = $.parseJSON(res);

                        $('#addcalendar .modal-title').html('Edit Calendar');
                        $('#lastDate').prop("disabled", false);
                        $("#lastDate").css("background-color", "");
                        $('input[name=eventno]').val(data.eventno);
                        // For General Tab
                        $('input[name=calstat]').val(data.calstat);
                       // $('select[name=atty_hand]').val(data.atty_hand);
                       if($("#atty_hand").hasClass("select2Class"))
                        {
                            $('select[name=atty_hand]').val(data.atty_hand);
                        }
                        else if($("#atty_hand").hasClass("cstmdrp"))
                        {
                            $('#atty_hand').val(data.atty_hand);
                        }
                        var addmin_attyass='<?php echo isset($system_caldata[0]->calattyass)?$system_caldata[0]->calattyass:''?>';

                        if(addmin_attyass=='1') {
                            $("select[name=attyass]").removeAttr("disabled");
                            data.attyass = (data.attyass == 'NA') ? '' : data.attyass;
                            //$('select[name=attyass]').val(data.attyass);
                            if($("#attyass").hasClass("select2Class"))
                            {
                                $('select[name=attyass]').val(data.attyass);
                            }
                            else if($("#attyass").hasClass("cstmdrp"))
                            {
                                $('#attyass').val(data.attyass);
                            }
                       }
                       else{ $("select[name=attyass]").attr("disabled", true); }
                        //$('select[name=card_salutation]').val(data.salutation);
                       // alert(data.judge);
                        $('#judge').val(data.judge);
                        $('input[name=first]').val(data.first);
                        $('input[name=last]').val(data.last);
                        $('input[name=defendant]').val(data.defendant);
                        $('#venue').val(data.venue);
                        $('input[name=location]').val(data.location);
                        $('input[name=caldate]').val(data.caldate);
                        var clocktime = moment(data.caltime,'hh:mm A');
                        //$('input[name=caltime]').val(clocktime.format('hh:mm A'));
                        var eveTime = clocktime.format('hh:mm A');
                        $('#caltime').val(eveTime);
                       /*$('#caltime').mdtimepicker({
                           'default': eveTime
                       });*/
                        //alert(clocktime.format('hh:mm A'));

                        $('input[name=totime]').val(data.tottime);
                        $('#event').val(data.event);
                        // For Notes Tab
                        $('textarea[name=notes]').val(data.notes);
                        //For Ticklers/Alerts Tab
                        $('select[name=tickle]').val(data.tickle);
                        $('input[name=tickledate]').val(data.tickdate);
                        $('input[name=tickletime]').val(data.ticktime);
                        $('select[name=whofrom]').val(data.whofrom);
                        $('select[name=whoto]').val(data.whoto);
                        // For Properties Tab
                        if(data.caseno != 0)
                            $('input[name=caseno]').val(data.caseno);
                        else
                            $('input[name=caseno]').val('');
                        $('input[name=initials0]').val(data.initials0);
                        $('input[name=initials]').val(data.initials);
                        $('input[name=chgwho]').val(data.chgwho);
                        $('input[name=chgdate]').val(data.chgdate);
                        $('input[name=chgtime]').val(data.chgtime);

                        (data.protected == '1') ? $('input[name=protected]').iCheck('check') : $('input[name=protected]').iCheck('uncheck');
                        (data.rollover == '1') ? $('input[name=rollover]').iCheck('check') : $('input[name=rollover]').iCheck('uncheck');

                        $('select[name=todo]').val(data.todo);
                        $('select[name=color]').val(data.color);
                      //  $("#addcalendar select").trigger('change');
                        $("#addcalendar select").trigger('change');
                        $.unblockUI();
                    }
                });
            }
            // End Add Calendar Data in Model
            function editContact(cardCode) {
            //alert("hi");
                $.blockUI();

                $.ajax({
                    url: "<?php echo base_url() . "rolodex/getEditRolodex" ?>",
                    method: "POST",
                    data: {'cardcode': cardCode},
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments = '';
                        if (data.card_comments) {
                            if(isJson(data.card_comments)) {
                                notes_comments = $.parseJSON(data.card_comments);
                            } else {
                                notes_comments = data.card_comments;
                            }
                        }
                        $('#addcontact .modal-title').html('Edit Contact');
                        //Tab - 1 Data
                        $('#card_salutation').val(data.salutation);
                        $('#card_suffix').val(data.suffix);
                        $('select[name=card_title]').val(data.title);
                        $('#card_type').val(data.type);
                        $('input[name=card_first]').val(data.first);
                        $('input[name=card_middle]').val(data.middle);
                        $('input[name=card_last]').val(data.last);
                        $('input[name=card_popular]').val(data.popular);
                        $('input[name=card_letsal]').val(data.letsal);
                        $('input[name=card_occupation]').val(data.occupation);
                        $('input[name=card_employer]').val(data.employer);
                        $('input[name=hiddenimg]').val(data.picture);
                        if(data.picture)
                        {
                            document.getElementById('profilepicDisp').style.display = 'block';
                            document.getElementById('dispProfile').src= data.picturePath;
                        }
                        else
                        {
                            document.getElementById('dispProfile').src='';
                        }
                        $('input[name=card2_firm]').val(data.firm);
                        $('input[name=card2_address1]').val(data.address1);
                        $('input[name=card2_address2]').val(data.address2);
                        $('input[name=card2_city]').val(data.city);
                        $('input[name=card2_state]').val(data.state);
                        $('input[name=card2_zip]').val(data.zip);
                        $('input[name=card2_venue]').val(data.venue);
                        $('input[name=card2_eamsref]').val(data.eamsref);
                        //Tab - 2 Data
                        $('input[name=card_business]').val(data.business);
                        $('input[name=card_fax]').val(data.card_fax);
                        //$('input[name=card_beeper]').val(data.beeper);
                        $('input[name=card_home]').val(data.home);
                        $('input[name=card_car]').val(data.car);
                        $('input[name=card_email]').val(data.email);
                        //$('input[name=card_notes]').val(notes_comments[0].notes);
                        if(notes_comments != '')
                                $('input[name=card_notes]').val(notes_comments[0].notes);
                            else
                                $('input[name=card_notes]').val();
                        $('input[name=card2_phone1]').val(data.phone1);
                        $('input[name=card2_phone2]').val(data.phone2);
                        $('input[name=card2_tax_id]').val(data.tax_id);
                        $('input[name=card2_fax]').val(data.card2_fax);
                        $('input[name=card2_fax2]').val(data.fax2);
                        ////Tab - 3 Data
                        $('input[name=card_social_sec]').val(data.social_sec);
                        if (data.birth_date == 'NA') {
                            $('input[name=card_birth_date]').val('');
                        } else {
                            $('input[name=card_birth_date]').val(moment(data.birth_date).format('MM/DD/YYYY'));
                        }

                        $('input[name=card_licenseno]').val(data.licenseno);
                        $('select[name=card_specialty]').val(data.specialty);
                        $('input[name=card_mothermaid]').val(data.mothermaid);
                        $('input[name=card_cardcode]').val(data.cardcode);
                        $('input[name=card_firmcode]').val(data.firmcode);
                        $('select[name=card_interpret]').val(data.interpret);
                        $('select[name=card_language]').val(data.language).trigger("change");

                        $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                        $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                        $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                        $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                        //Tab - 4 Data  $('input[name=card_notes]').val(notes_comments[0].note);
                         if(notes_comments != '')
                               $('textarea[name=card_comments]').val(notes_comments[1].comments);
                           else
                               $('textarea[name=card_comments]').val();
                        //$('textarea[name=card_comments]').val(notes_comments[1].comments);
                        $('textarea[name=card2_comments]').val(data.card2_comments);
                        //Tab - 5 Data
                        $('textarea[name=card2_mailing1]').val(data.mailing1);
                        $('textarea[name=card2_mailing2]').val(data.mailing2);
                        $('textarea[name=card2_mailing3]').val(data.mailing3);
                        $('textarea[name=card2_mailing4]').val(data.mailing4);



                        //parties tab call
                        $('#new_case').val('parties');
                        $('input[name="card_cardcode"]').val(data.cardcode);

                        $('#addcontact').modal({backdrop: 'static', keyboard: false});
                        $("#addcontact").modal('show');

                        $("#addRolodexForm select").trigger('change');

                        $.unblockUI();
                    }
                });
            }

            function editCaseDetails(caseNo, selectTab) {
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getAjaxCaseDetail',
                    data: 'caseno=' + caseNo,
                    method: 'POST',
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        data = data[0];

                        if (data.dateenter != '1970-01-01 05:30:00' && data.dateenter != '1899-12-30 00:00:00' && data.dateenter != '0000-00-00 00:00:00' && data.dateenter != null) {
                            var dateenter = moment(data.dateenter).format('MM/DD/YYYY');
                            $('#casedate_entered').val(dateenter);
                        }

                        if (data.dateopen != '1970-01-01 05:30:00' && data.dateopen != '1899-12-30 00:00:00' && data.dateopen != '0000-00-00 00:00:00' && data.dateopen != null) {
                            var dateopen = moment(data.dateopen).format('MM/DD/YYYY');
                            $('#casedate_open').val(dateopen);
                        }

                        if (data.followup != '1970-01-01 05:30:00' && data.followup != '1899-12-30 00:00:00' && data.followup != '0000-00-00 00:00:00' && data.followup != null) {
                            var followup = moment(data.followup).format('MM/DD/YYYY');
                            $('#casedate_followup').val(followup);
                        }

                        if (data.dateclosed != '1970-01-01 05:30:00' && data.dateclosed != '1899-12-30 00:00:00' && data.dateclosed != '0000-00-00 00:00:00' && data.dateclosed != null) {
                            var dateclosed = moment(data.dateclosed).format('MM/DD/YYYY');
                            $('#casedate_closed').val(dateclosed);
                        }

                        if (data.psdate != '1970-01-01 05:30:00' && data.psdate != '1899-12-30 00:00:00' && data.psdate != '0000-00-00 00:00:00' && data.psdate != null) {
                            var psdate = moment(data.psdate).format('MM/DD/YYYY');
                            $('#casedate_psdate').val(psdate);
                        }

                        if (data.d_r != '1970-01-01 05:30:00' && data.d_r != '1899-12-30 00:00:00' && data.d_r != '0000-00-00 00:00:00' && data.d_r != null) {
                            var d_r = moment(data.d_r).format('MM/DD/YYYY');
                            $('#casedate_referred').val(d_r);
                        }

                        if(data.udfall2 != null)
                        {
                        var res = data.udfall2.split(",");
                        //console.log(res[0],res[1]);
                        $('#closedfileno').val(res[0]);
                        $('select[name=editOtherClass]').val(res[1]);
                         }
                        //$('#editCaseType').val(data.casetype);
                        //$('#editCaseStat').val(data.casestat);
                        $('#fileno').val(data.yourfileno);
                        $('#editCaseVenue1').val(data.first);
                        $('#editCaseVenue').val(data.venue);
                        $('select[name=editCaseSubType]').val(data.udfall);
                        //$('select[name=editCaseAttyResp]').val(data.atty_resp);

                        /*if ($('#editCaseType option:contains('+ data.casetype +')').length) {
                            $('select[name=editCaseType]').val(data.casetype);
                         }
                         else
                         {
                              $('#editCaseType').append("<option value='" + data.casetype + "' selected='seleceted'>" + data.casetype + "</option>");
                         }*/
                        if($("#editCaseType").hasClass("select2Class"))
                        {
                            if ($('#editCaseType option:contains('+ data.casetype +')').length) {
                                $('select[name=editCaseType]').val(data.casetype);
                             }
                             else
                             {
                                  if(data.casetype)
                                    $('#editCaseType').append("<option value='" + data.casetype + "' selected='seleceted'>" + data.casetype + "</option>");
                             }
                        }
                        else if($("#editCaseType").hasClass("cstmdrp"))
                        {
                             if(data.casetype)
                                $('#editCaseType').val(data.casetype);
                        }

                        if($("#editCaseStat").hasClass("select2Class"))
                        {
                            if ($('#editCaseStat option:contains('+ data.casetype +')').length) {
                                $('select[name=editCaseStat]').val(data.casetype);
                             }
                             else
                             {
                                  if(data.casestat)
                                      $('#editCaseStat').append("<option value='" + data.casestat + "' selected='seleceted'>" + data.casestat + "</option>");
                             }
                        }
                        else if($("#editCaseStat").hasClass("cstmdrp"))
                        {
                               if(data.casestat)
                                $('#editCaseStat').val(data.casestat);
                        }

                        if($("#editCaseAttyResp").hasClass("select2Class"))
                        {
                                if ($('#editCaseAttyResp option:contains('+ data.atty_resp +')').length) {
                                    $('select[name=editCaseAttyResp]').val(data.atty_resp);
                                 }
                                 else
                                 {
                                      if(data.atty_resp)
                                          $('#editCaseAttyResp').append("<option value='" + data.atty_resp + "' selected='seleceted'>" + data.atty_resp + "</option>");
                                 }
                        }
                        else if($("#editCaseAttyResp").hasClass("cstmdrp"))
                        {
                             if(data.atty_resp)
                                 $('#editCaseAttyResp').val(data.atty_resp);
                        }

                        if($("#editCaseAttyHand").hasClass("select2Class"))
                        {
                            if ($('#editCaseAttyHand option:contains('+ data.atty_hand +')').length) {
                               $('select[name=editCaseAttyHand]').val(data.atty_hand);
                            }
                            else
                            {
                               if(data.atty_hand)
                                   $('#editCaseAttyHand').append("<option value='" + data.atty_hand + "' selected='seleceted'>" + data.atty_hand + "</option>");
                            }
                        }
                        else if($("#editCaseAttyHand").hasClass("cstmdrp"))
                        {
                            if(data.atty_hand)
                                $('#editCaseAttyHand').val(data.atty_hand);
                        }

                        if($("#editCaseAttyPara").hasClass("select2Class"))
                        {
                            if ($('#editCaseAttyPara option:contains('+ data.para_hand +')').length) {
                               $('select[name=editCaseAttyPara]').val(data.para_hand);
                            }
                            else
                            {
                                if(data.para_hand)
                                    $('#editCaseAttyPara').append("<option value='" + data.para_hand + "' selected='seleceted'>" + data.para_hand + "</option>");
                            }
                        }
                        else if($("#editCaseAttyPara").hasClass("cstmdrp"))
                        {
                             if(data.para_hand)
                                $('#editCaseAttyPara').val(data.para_hand);
                        }

                        if($("#editCaseAttySec").hasClass("select2Class"))
                        {
                            if ($('#editCaseAttySec option:contains('+ data.sec_hand +')').length) {
                               $('select[name=editCaseAttySec]').val(data.sec_hand);
                            }
                            else
                            {
                                if(data.sec_hand)
                                    $('#editCaseAttySec').append("<option value='" + data.sec_hand + "' selected='seleceted'>" + data.sec_hand + "</option>");
                            }
                         }
                        else if($("#editCaseAttySec").hasClass("cstmdrp"))
                        {
                             if(data.sec_hand)
                                 $('#editCaseAttySec').val(data.sec_hand);
                        }



                        //$('#editCaseAttyPara').val(data.para_hand);
                        //$('#editCaseAttySec').val(data.sec_hand);
                        $('#case_ps').val(data.ps);
                        $('#fileloc').val(data.location);
                        $('select[name=case_reffered_by]').val(data.rb);

                        if (typeof data.category_events !== 'undefined') {
                            if (typeof data.category_events.quickNote !== 'undefined') {
                                $('#editcase_quicknote').val(data.category_events.quickNote);
                            }
                            if (typeof data.category_events.message1 !== 'undefined') {
                                $('#caseedit_message1').val(data.category_events.message1);
                            }
                            if (typeof data.category_events.message2 !== 'undefined') {
                                $('#caseedit_message2').val(data.category_events.message2);
                            }
                            if (typeof data.category_events.message3 !== 'undefined') {
                                $('#caseedit_message3').val(data.category_events.message3);
                            }
                        }

                        $('#editcase_caption').val(data.caption1);

                        $('#editcasedetail').modal('show');
                         $("#edit_case_details select").trigger('change');
                        $("a[href='#" + selectTab + "']").trigger("click");
                    }
                });
            }


            $(document.body).on('change', '#default_freeze', function () {
                if ($('#default_freeze:checked').length) {
                    $("#default_feeze_until").attr("disabled", true);
                } else {
                    $("#default_feeze_until").attr("disabled", false);
                }
            });
            $(document.body).on('click','.complete_task',function(){
               var id=$(this).attr('data-id');
               var event=$(this).attr('data-event');
               var caseno=$(this).attr('data-caseno');
              // alert(caseno);
                if(caseno <= 0 ){
                   completeTask(id);
                }
                else{
                    $('#case_event').val(event);
                    $('#caseTaskId').val(id);
                     $('#caseno').val(caseno);
                    $("#TaskCompleted").modal("show");
                }
            });
            $(document.body).on('click','#saveCompletedTask',function(){
                /*$('#saveCompletedTask').attr('disabled','disabled');*/
                var id=$('#caseTaskId').val();
                var case_event=$('#case_event').val();
                var case_extra_event=$('#case_extra_event').val();
                var case_category=$('#case_category').val();
                var caseno=$('#caseno').val();
                 $.ajax({
                        url: '<?php echo base_url(); ?>tasks/completedTaskWithCase',
                        data: {taskId:id,case_event:case_event,case_extra_event:case_extra_event,case_category:case_category,caseno:caseno},
                        method: "POST",
                        success: function (result) {
                            $.unblockUI();
                            var data = $.parseJSON(result);
                            if (data.status == 1) {
                                swal("Success !", data.message, "success");
                                 $("#TaskCompleted").modal("hide");
                                taskTable.ajax.reload(null, false);

                               window.reload();

                            } else {
                                swal("Oops...", data.message, "error");
                            }
                        }
                    });
            });

            $(document.body).on('click', '.viewPartiesDetail', function () {
                //alert('in function');
                $("#rolodexPartiesResult tr").removeClass("highlight");
                $(this).addClass('highlight');
                var caseNo = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: $(this).data('cardcode'),
                        caseno: caseNo
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data == false) {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        } else {
                            $("#searched_party_fullname").val(data.fullname);
                            $("#searched_party_firmname").val(data.firm);
                            $('#searched_party_type').html(data.type);
                            $('#searched_party_speciality').html(data.speciality);
                            $('#searched_party_address').html(data.address1);
                            $('#searched_party_city').html(data.city);
                            $('#searched_party_cardnumber').html(data.cardcode);
                            $('#searched_party_ssno').html(data.social_sec);
                            $('#searched_party_email').html(data.email);
                            $('#searched_party_licenseno').html(data.licenseno);
                            $('#searched_party_homephone').html(data.home);
                            $('#searched_party_business').html(data.business);
                            $('.addSearchedParties').attr("disabled", false);
                            $('.editPartyCard').attr("disabled", false);
                        }
                    }
                });



            });

            $(document.body).on('click', '.addSearchedParties', function () {
                var caseNo = $("#caseNo").val();
                var cardcode = $("#searched_party_cardnumber").html();
                var type = $("#searched_party_type").html();
                var fullname = $("#searched_party_fullname").val();
                var firm = $("#searched_party_firmname").val();
                var nameofParty = '';

                if (firm != "") {
                    nameofParty = firm;
                } else {
                    nameofParty = fullname;
                }

                var radioVal = $("input[name='rolodex_search_parties']:checked").val();
                if (radioVal == "rolodex-cardno") {
                    nameofParty = fullname;
                }

                $("#addparty").modal('hide');

                $.ajax({
                    url: '<?php echo base_url(); ?>cases/addSearchedPartyToCase',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        caseno: caseNo,
                        type: type
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                       //console.log(data);
                        if ($('#parties table tr#tr-0').length < 1) {
                            location.reload();
                        }
                       var vount_r= $('#party-table-data tr').length;
                     if ($(".party-in table td").html() == "<h4>No Parties Found</h4>") {
                            $(".party-in table td").remove();
                        }
                        var html = '';
                        html += '<tr class="" id="tr-'+vount_r+'">';
                        html += "<td>" + vount_r + "</td>";
                        html += "<td title=" + nameofParty + "><a class='view_party' href='javascript:void(0);' data-card=" + cardcode + " data-case=" + caseNo + ">"+ nameofParty + "</a> </td>";
                        html += "<td>" + type + "</td>";
                        html += "<td>";
                        html += "<a class='view_party' href='javascript:void(0);' data-card=" + cardcode + " data-case=" + caseNo + ">View</a> ";
                        html += "<a class='remove_party' href='javascript:void(0);' data-card=" + cardcode + " data-case=" + caseNo + ">Delete</a>";
                        html += "</td></tr>";
                       //  $('table#party-table-data').find('.table-selected').not('tr').removeClass('table-selected');

                        //$(".party-in table").append(html);
                        table.row.add($(html));
                        table.draw();
                        // $('.party-in table').DataTable().ajax.reload();
                        //$("#party-table-data")
                        if($('table#party-table-data tr').hasClass('table-selected'))
                        {
                            $('table#party-table-data tr').removeClass('table-selected')
                        }
                           $('tr.last').addClass('table-selected');

                         setPartyViewData(cardcode,caseNo);


                    }
                });
            });
            function setPartyViewData(cardcode,caseNo)
            {
             $('.edit-party').show();
               $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxCardDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    cardcode: cardcode,
                    caseno: caseNo
                },
                 beforeSend: function (xhr) {
                        //$.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        //$.unblockUI();
                    },
                success: function (data) {
                    if (data == false) {
                        swal({
                            title: "Alert",
                            text: "Oops! Some error has occured while fetching details. Please try again later.",
                            type: "warning"
                        });
                    } else {
                        var notes_comments = '';
                        if (data.comment_per) {
                            if(isJson(data.comment_per)) {
                                notes_comments = $.parseJSON(data.comment_per);
                            } else {
                                notes_comments = data.comment_per;
                            }
                        }
                        $('.edit-party').attr('data-casecard', cardcode);
                        $('.edit-party').attr('data-case', caseNo);
                        $('#party_name').html(data.fullname);
                        $('#party_name').data('cardcodeWarning', data.cardcode);
                        $('#party_type').data('cardcodeWarning', data.cardcode);
                        $('#partiesCardCode').val(data.cardcode);
                        $('#party_type').html(data.type);
                        $('#firm_name').html(data.firm);
                        $('.addressVal').attr('href', "https://maps.google.com/?q=" + data.address1 + data.address2);
                        $('#party_address1').html(data.address1);
                        $('#party_address2').html(data.address2);
                        $('#party_city').html(data.city);
                        $('#party_phn_home').html(data.home);
                        $('#party_phn_office').html(data.business);
                        $('#party_fax').html(data.fax);
                        $('#party_email').html(data.email);
                        $('#party_side').html(data.side);
                        $('#party_offc_no').html(data.officeno);
                        $('#party_notes').html('');
                        if(data.eamsref)
                            $('#party_eams').html(data.name + ' ' + data.eamsref);
                        else
                            $('#party_eams').html('');
                        $('#party_cell').html(data.car);
                        $('#party_beeper').html(data.beeper);
                        //$('#comment_per').html(notes_comments[1].comments);
                        $('#party_notes').html(data.notes);


                        if (data.comment_bus != '') {
                            $('.busComment').css({'visibility': 'visible'});
                            $('#comment_bus').html(data.comment_bus);
                        } else {
                            $('.busComment').css({'visibility': 'hidden'});
                            $('#comment_bus').html(data.comment_bus);
                        }

                        if (data.comment_per && notes_comments[1]) {
                            $('.busComment').css({'visibility': 'visible'});
                            $('#comment_per').html(notes_comments[1].comments);
                        } else {
                            $('.busComment').css({'visibility': 'hidden'});
                            $('#comment_per').html('');
                        }
                        if(data.atty_hand)
                        {
                            if ($('#case_act_atty option:contains('+ data.atty_hand +')').length) {
                            $('select[name=case_act_atty]').val(data.atty_hand);
                            }
                            else
                            {
                                $('#case_act_atty').append("<option value='" + data.atty_hand + "' selected='seleceted'>" + data.atty_hand + "</option>");
                            }
                        }

                    }
                }
            });
            }
            $(document.body).on('click', '#change_name_warning', function () {
                $("#partiesNameChangeWarning").modal("hide");
                editContact($('#party_name').data('cardcodeWarning'));
            });

            $(document.body).on('click', '.typeLink', function () {
                var cardcode = $('#party_type').data('cardcodeWarning');
                var caseno = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/getCaseSpecific',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        caseno: caseno,
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                       // console.log(data);
                        $("#parties_case_specific").val(data[0].type);
                        $("#parties_side").val(data[0].side);
                        $("#parties_office_number").val(data[0].officeno);
                        $("#parties_notes").val(data[0].notes);
                        (data[0].flags != '1') ? $('#parties_party_sheet').iCheck('uncheck') : $('#parties_party_sheet').iCheck('check');
                    }
                });

                $("#caseSpecific").modal("show");
                //editContact($('#party_name').data('cardcodeWarning'));
            });

            $(document.body).on('click', '.editPartyCard', function () {
                var cardcode = $('#searched_party_cardnumber').html();
                if (cardcode == "" || cardcode == 'undefinded') {
                    swal({
                        title: "Alert",
                        text: "That card does not exist in the rolodex or cannot be edited at this time. Please try searching again.",
                        type: "warning"
                    });
                } else {
                  // $('#addparty').modal('hide');
                   $("#addcontact").attr("style", "z-index:9999 !important");
                    editContact(cardcode);

                }
            });

            $(document.body).on('click', '#attach_name', function () {
                //add card entry
                var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                var first_name = $(".first_name").val();
                var last_name = $(".last_name").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/attach_name',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        cardcode: cardcode,
                        first_name: first_name,
                        last_name: last_name
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (result) {
                        //var data = $.parseJSON(result);
                        var url = "<?php echo base_url() ?>cases/case_details/" + result.caseno;
                        $(location).attr('href', url);
                    }
                });

            });

            $(document.body).on('click', '.selectDuplicateData', function () {
                $("#duplicateEntries tr").removeClass("highlight");
                $(this).addClass('highlight');
                var numItems = $('.highlight').length;
               if(numItems=='1')
               {
                  $('#edit_new_case').prop('disabled', false);
                  $('#attach_name').prop('disabled', false);
               }
            });

            $(document.body).on('click', '#add_name_case', function () {
                //add card entry
                $('input[name=card_first]').val($(".first_name").val());
                $('input[name=card_last]').val($(".last_name").val());
                $("#new_case").val(1);
                $("#addcase").modal("hide");
                $("#duplicateData").modal("hide");
                $("#addcontact").modal("show");
                return false;
            });

            $(document.body).on('click', '#edit_new_case', function () {
                $("#duplicateData").modal("hide");
                var cardcode = $("#duplicateEntries .highlight").data('cardcode');
                editContact(cardcode);

            });

            $(document.body).on('click', '.addparty', function () {
                $('.addSearchedParties').attr("disabled", true);
                $('.editPartyCard').attr("disabled", true);
                $("#addparty").modal('show');
            });

            $(document.body).on('click', '.venuelist', function () {
                $("#venulist_model").modal("show");
            });


            $('#venue_table tbody').on('click', 'tr', function () {
                // $(this).toggleClass('selected');
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                    venue_table_data.$('tr.table-selected').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });

            $(document.body).on('click', '#set_venue', function () {
                var contentArray = [];
                $.map(venue_table_data.rows('.table-selected').data(), function (item) {
                    contentArray = item;
                    return contentArray;
                });
                if(contentArray[1].lengh > 0){
                     $("#editCaseVenue1").val(contentArray[1]);
                }else{
                   $("#editCaseVenue1").val(contentArray[0]);
               }
                $("#editCaseVenue").val(contentArray.DT_RowId);
                $("#venulist_model").modal("hide");
            });
            $(document.body).on('click', '.edit-party', function () {

                $('#show_conform_model').modal('show');

            });
            $(document.body).on('click', '#relodex_yes', function () {
                var cardcode = $('#edit-party').attr('data-casecard');
                var caseno = $('#edit-party').attr('data-case');
             //  alert(cardcode);
                $('#show_conform_model').modal('hide');
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>rolodex/getEditRolodex',
                    data: {cardcode: cardcode, caseNO: caseno,status:'Yes'},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments = '';
                        if (data.card_comments) {
                            if(isJson(data.card_comments)) {
                                notes_comments = $.parseJSON(data.card_comments);
                            } else {
                                notes_comments = data.card_comments;
                            }
                        }
                       //alert(data);
                        swal.close();
                        if (data.status == '0')
                        {
                            var name = '';
                         if (data.data.type == 'APPLICANT')
                            {
                                 name = data.data.last + ', ' + data.data.first;
                                 $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                           }
                            else if (data.data.type == 'LIEN' || data.data.type == 'BOARD' || data.data.type == 'INSURANCE' || data.data.type == 'ATTORNEY' || data.data.type == 'EMPLOYER')
                            {
                                 name = data.data.firm;
                                 $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                            } else if (data.data.type == 'DR') {
                                if (data.data.firm.length == 0) {
                                    name = data.data.last + ', ' + data.data.first;
                                    $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
                                } else {
                                    name = data.data.firm;
                                    $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
                                }
                            }
                            else
                            {
                                name = data.data.last + ', ' + data.data.first;
                                 $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');

                            }
                            //alert(data.data.firm);
//                            if (data.data.firm.length > 0)
//                            {
//                                 $('input:radio[name=rolodex_search_parties][value=rolodex-company]').iCheck('check');
//                            } else {
//                                 $('input:radio[name=rolodex_search_parties][value=rolodex-first-last]').iCheck('check');
//                            }

                            $('#rolodex_search_text').val(name);
                            $("#rolodex_search_parties_form").submit();
                            $("#addparty").modal('show');
                        } else
                        {

                            $('#addcontact .modal-title').html('Edit Contact');
                            $('#card_salutation').val(data.salutation);
                            $('#card_suffix').val(data.suffix);
                            $('select[name=card_title]').val(data.title);
                            $('#card_type').val(data.type);
                            $('input[name=card_first]').val(data.first);
                            $('input[name=card_middle]').val(data.middle);
                            $('input[name=card_last]').val(data.last);
                            $('input[name=card_popular]').val(data.popular);
                            $('input[name=card_letsal]').val(data.letsal);
                            $('input[name=card_occupation]').val(data.occupation);
                            $('input[name=card_employer]').val(data.employer);
                            $('input[name=hiddenimg]').val(data.picture);
                            if(data.picture)
                            {
                                document.getElementById('profilepicDisp').style.display = 'block';
                                document.getElementById('dispProfile').src= data.picturePath;
                            }
                            else
                            {
                                document.getElementById('dispProfile').src='';
                            }
                            $('input[name=card2_firm]').val(data.firm);
                            $('input[name=card2_address1]').val(data.address1);
                            $('input[name=card2_address2]').val(data.address2);
                            $('input[name=card2_city]').val(data.city);
                            $('input[name=card2_state]').val(data.state);
                            $('input[name=card2_zip]').val(data.zip);
                            $('input[name=card2_venue]').val(data.venue);
                            $('input[name=card2_eamsref]').val(data.eamsref);
                            //Tab - 2 Data
                            $('input[name=card_business]').val(data.business);
                            $('input[name=card_fax]').val(data.card_fax);
                            //$('input[name=card_beeper]').val(data.beeper);
                            $('input[name=card_home]').val(data.home);
                            $('input[name=card_car]').val(data.car);
                            $('input[name=card_email]').val(data.email);
                            if(notes_comments != '')
                                $('input[name=card_notes]').val(notes_comments[0].notes);
                            else
                                $('input[name=card_notes]').val();
                            $('input[name=card2_phone1]').val(data.phone1);
                            $('input[name=card2_phone2]').val(data.phone2);
                            $('input[name=card2_tax_id]').val(data.tax_id);
                            $('input[name=card2_fax]').val(data.card2_fax);
                            $('input[name=card2_fax2]').val(data.fax2);
                            ////Tab - 3 Data
                            $('input[name=card_social_sec]').val(data.social_sec);
                            if (data.birth_date == 'NA') {
                                $('input[name=card_birth_date]').val('');
                            } else {
                                $('input[name=card_birth_date]').val(data.birth_date);
                            }

                            $('input[name=card_licenseno]').val(data.licenseno);
                            $('select[name=card_specialty]').val(data.specialty);
                            $('input[name=card_mothermaid]').val(data.mothermaid);
                            $('input[name=card_cardcode]').val(data.cardcode);
                            $('input[name=card_firmcode]').val(data.firmcode);
                            $('select[name=card_interpret]').val(data.interpret);
                            $('select[name=card_language]').val(data.language).trigger("change");

                            $('#form_datetime1').val(data.card_origchg + ' - ' + data.card_origdt);
                            $('#form_datetime2').val(data.card_lastchg + ' - ' + data.card_lastdt);
                            $('#form_datetime3').val(data.card2_origchg + ' - ' + data.card2_origdt);
                            $('#form_datetime4').val(data.card2_lastchg + ' - ' + data.card2_lastdt);
                            //Tab - 4 Data
                           if(notes_comments != '')
                               $('textarea[name=card_comments]').val(notes_comments[1].comments);
                           else
                               $('textarea[name=card_comments]').val();
                            $('textarea[name=card2_comments]').val(data.card2_comments);
                            //Tab - 5 Data
                            $('textarea[name=card2_mailing1]').val(data.mailing1);
                            $('textarea[name=card2_mailing2]').val(data.mailing2);
                            $('textarea[name=card2_mailing3]').val(data.mailing3);
                            $('textarea[name=card2_mailing4]').val(data.mailing4);



                            //parties tab call
                            $('#new_case').val('parties');
                            $('input[name="card_cardcode"]').val(data.cardcode);

                            $('#addcontact').modal({backdrop: 'static', keyboard: false});
                            $("#addcontact").modal('show');
                            $("#addRolodexForm select").trigger('change');
                            $.unblockUI();
                        }
                    }
                });


            });
            $(document.body).on('click', '#relodex_no', function () {
                $('#show_conform_model').modal('hide');
                var cardcode = $('#edit-party').attr('data-casecard');
                var caseno = $('#edit-party').attr('data-case');
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>rolodex/editRolodexdetail',
                    data: {cardcode: cardcode, caseNO: caseno,status:'NO'},
                    method: "POST",
                    success: function (result) {
                        var data = $.parseJSON(result);
                        var notes_comments =$.parseJSON(data.data.card_comments);
                        //alert(notes_comments);
                       if (data.status == '0')
                        {
                             $.unblockUI();
                             swal({
                                title:'Are you sure you want to make changes to this rolodex card?',
                                text:' '+data.message ,
                                showCancelButton: true,
                                confirmButtonColor: "#1ab394",
                                confirmButtonText: "Yes",
                                closeOnConfirm: false,
                                showLoaderOnConfirm: true,
                             },function(isConfirm){
                             if (isConfirm) {
                                // console.log(data.data.salutation,"jk");
                             swal.close();
                            $('#addcontact .modal-title').html('Edit Contact');
                            $('#card_salutation').val(data.data.salutation);
                            $('#card_suffix').val(data.data.suffix);
                            $('select[name=card_title]').val(data.data.title);
                            $('#card_type').val(data.data.type);
                            $('input[name=card_first]').val(data.data.first);
                            $('input[name=card_middle]').val(data.data.middle);
                            $('input[name=card_last]').val(data.data.last);
                            $('input[name=card_letsal]').val(data.data.letsal);
                            $('input[name=card_occupation]').val(data.data.occupation);
                            $('input[name=card_employer]').val(data.data.employer);
                            $('input[name=hiddenimg]').val(data.data.picture);
                            if(data.data.picture)
                            {
                                document.getElementById('profilepicDisp').style.display = 'block';
                                document.getElementById('dispProfile').src= data.data.picturePath;
                            }
                            else
                            {
                                document.getElementById('dispProfile').src='';
                            }
                            $('input[name=card2_firm]').val(data.data.firm);
                            $('input[name=card2_address1]').val(data.data.address1);
                            $('input[name=card2_address2]').val(data.data.address2);
                            $('input[name=card2_city]').val(data.data.city);
                            $('input[name=card2_state]').val(data.data.state);
                            $('input[name=card2_zip]').val(data.data.zip);
                            $('input[name=card2_venue]').val(data.data.venue);
                            $('input[name=card2_eamsref]').val(data.data.eamsref);
                            //Tab - 2 Data
                            $('input[name=card_business]').val(data.data.business);
                            $('input[name=card_fax]').val(data.data.card_fax);
                            //$('input[name=card_beeper]').val(data.data.beeper);
                            $('input[name=card_home]').val(data.data.home);
                            $('input[name=card_car]').val(data.data.car);
                            $('input[name=card_email]').val(data.data.email);
                            //$('input[name=card_notes]').val(notes_comments[0].notes);
                            if(notes_comments != '')
                                $('input[name=card_notes]').val(notes_comments[0].notes);
                            else
                                $('input[name=card_notes]').val();
                            $('input[name=card2_phone1]').val(data.data.phone1);
                            $('input[name=card2_phone2]').val(data.data.phone2);
                            $('input[name=card2_tax_id]').val(data.data.tax_id);
                            $('input[name=card2_fax]').val(data.data.card2_fax);
                            $('input[name=card2_fax2]').val(data.data.fax2);
                            ////Tab - 3 Data
                            $('input[name=card_social_sec]').val(data.data.social_sec);
                            if (data.data.birth_date == 'NA') {
                                $('input[name=card_birth_date]').val('');
                            } else {
                                $('input[name=card_birth_date]').val(data.data.birth_date);
                            }

                            $('input[name=card_licenseno]').val(data.data.licenseno);
                            $('select[name=card_specialty]').val(data.data.specialty);
                            $('input[name=card_mothermaid]').val(data.data.mothermaid);
                            $('input[name=card_cardcode]').val(data.data.cardcode);
                            $('input[name=card_firmcode]').val(data.data.firmcode);
                            $('select[name=card_interpret]').val(data.data.interpret);
                            $('select[name=card_language]').val(data.data.language).trigger("change");

                            $('#form_datetime1').val(data.data.card_origchg + ' - ' + data.data.card_origdt);
                            $('#form_datetime2').val(data.data.card_lastchg + ' - ' + data.data.card_lastdt);
                            $('#form_datetime3').val(data.data.card2_origchg + ' - ' + data.data.card2_origdt);
                            $('#form_datetime4').val(data.data.card2_lastchg + ' - ' + data.data.card2_lastdt);
                            //Tab - 4 Data
                            //alert('card_comments =>'+data.data.card_comments);
                            //$('textarea[name=card_comments]').val(notes_comments[1].comments);
                            if(notes_comments != '')
                               $('textarea[name=card_comments]').val(notes_comments[1].comments);
                           else
                               $('textarea[name=card_comments]').val();
                            //$('textarea[name=card_comments]').val(data.data.card_comments);
                            $('textarea[name=card2_comments]').val(data.data.card2_comments);
                            //Tab - 5 Data
                            $('textarea[name=card2_mailing1]').val(data.data.mailing1);
                            $('textarea[name=card2_mailing2]').val(data.data.mailing2);
                            $('textarea[name=card2_mailing3]').val(data.data.mailing3);
                            $('textarea[name=card2_mailing4]').val(data.data.mailing4);

                            $('#new_case').val('parties');
                            $('input[name="card_cardcode"]').val(data.data.cardcode);

                            $('#addcontact').modal({backdrop: 'static', keyboard: false});
                            $("#addcontact").modal('show');
                            $("#addRolodexForm select").trigger('change');
                          $.unblockUI();
                         }
                        });

                        } else
                        {
                            // console.log(data.data.salutation);
                            $('#addcontact .modal-title').html('Edit Contact');
                            $('#card_salutation').val(data.data.salutation);
                            $('#card_suffix').val(data.data.suffix);
                            $('select[name=card_title]').val(data.data.title);
                            $('#card_type').val(data.data.type);
                            $('input[name=card_first]').val(data.data.first);
                            $('input[name=card_middle]').val(data.data.middle);
                            $('input[name=card_last]').val(data.data.last);
                            $('input[name=card_letsal]').val(data.data.letsal);
                            $('input[name=card_occupation]').val(data.data.occupation);
                            $('input[name=card_employer]').val(data.data.employer);
                            $('input[name=hiddenimg]').val(data.data.picture);
                            if(data.data.picture)
                            {
                                document.getElementById('profilepicDisp').style.display = 'block';
                                document.getElementById('dispProfile').src= data.data.picturePath;
                            }
                            else
                            {
                                document.getElementById('dispProfile').src='';
                            }
                            $('input[name=card2_firm]').val(data.data.firm);
                            $('input[name=card2_address1]').val(data.data.address1);
                            $('input[name=card2_address2]').val(data.data.address2);
                            $('input[name=card2_city]').val(data.data.city);
                            $('input[name=card2_state]').val(data.data.state);
                            $('input[name=card2_zip]').val(data.data.zip);
                            $('input[name=card2_venue]').val(data.data.venue);
                            $('input[name=card2_eamsref]').val(data.data.eamsref);
                            //Tab - 2 Data
                            $('input[name=card_business]').val(data.data.business);
                            $('input[name=card_fax]').val(data.data.card_fax);
                            //$('input[name=card_beeper]').val(data.data.beeper);
                            $('input[name=card_home]').val(data.data.home);
                            $('input[name=card_car]').val(data.data.car);
                            $('input[name=card_email]').val(data.data.email);
                            $('input[name=card2_phone1]').val(data.data.phone1);
                            $('input[name=card2_phone2]').val(data.data.phone2);
                            $('input[name=card2_tax_id]').val(data.data.tax_id);
                            $('input[name=card2_fax]').val(data.data.card2_fax);
                            $('input[name=card2_fax2]').val(data.data.fax2);
                            ////Tab - 3 Data
                            $('input[name=card_social_sec]').val(data.data.social_sec);
                            if (data.data.birth_date == 'NA') {
                                $('input[name=card_birth_date]').val('');
                            } else {
                                $('input[name=card_birth_date]').val(data.data.birth_date);
                            }

                            $('input[name=card_licenseno]').val(data.data.licenseno);
                            $('select[name=card_specialty]').val(data.data.specialty);
                            $('input[name=card_mothermaid]').val(data.data.mothermaid);
                            $('input[name=card_cardcode]').val(data.data.cardcode);
                            $('input[name=card_firmcode]').val(data.data.firmcode);
                            $('select[name=card_interpret]').val(data.data.interpret);
                            $('select[name=card_language]').val(data.data.language).trigger("change");

                            $('#form_datetime1').val(data.data.card_origchg + ' - ' + data.data.card_origdt);
                            $('#form_datetime2').val(data.data.card_lastchg + ' - ' + data.data.card_lastdt);
                            $('#form_datetime3').val(data.data.card2_origchg + ' - ' + data.data.card2_origdt);
                            $('#form_datetime4').val(data.data.card2_lastchg + ' - ' + data.data.card2_lastdt);
                            //Tab - 4 Data
                            $('textarea[name=card_comments]').val(data.data.card_comments);
                            $('textarea[name=card2_comments]').val(data.data.card2_comments);
                            //Tab - 5 Data
                            $('textarea[name=card2_mailing1]').val(data.data.mailing1);
                            $('textarea[name=card2_mailing2]').val(data.data.mailing2);
                            $('textarea[name=card2_mailing3]').val(data.data.mailing3);
                            $('textarea[name=card2_mailing4]').val(data.data.mailing4);

                            $('#new_case').val('parties');
                            $('input[name="card_cardcode"]').val(data.data.cardcode);

                            $('#addcontact').modal({backdrop: 'static', keyboard: false});
                            $("#addcontact").modal('show');
                            $("#addRolodexForm select").trigger('change');
                              $.unblockUI();
                        }
                    }
                });

            });
            $(document.body).on('click','.rolodex-company',function(){
                $("#firmcompanylist").attr("style", "z-index:9999 !important");
                $("#rolodexsearchResult_data  tbody").remove();
                rolodexSearch.destroy();
                $('#firmcompanylist').modal('show');
                $('#rolodexsearchResult_data').DataTable().clear();
            });
            $(document.body).on('blur','#rolodex_search_text_name',function(e) {
            //console.log(e.type)
            if (e.type == 'focusout' || e.keyCode == '13')
             {
             if ($('#companylistForm').valid())
             {
            if($('#rolodex_search_text_name').val().trim().length > 0)
            {
                    $.ajax({
                            url: '<?php echo base_url(); ?>cases/searchForRolodex',
                            method: 'POST',
                            data: {rolodex_search_parties:$('#rolodex_search_parties_name').val(),rolodex_search_text:$(this).val()},
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                                 rolodexSearch.clear();

                                //rolodexSearchDatatable
                                var obj = $.parseJSON(data);
                             // console.log(obj);
                                $.each(obj, function (index, item) {
                                    var html = '';
                                    html += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + item.cardcode + "><td>" + item.firm + "</td>";
                                    html += "<td>" + item.city + "</td>";
                                    html += "<td>" + item.address + "</td>";
                                    html += "<td>" + item.phone + "</td>";
                                    html += "</tr>";
                                    rolodexSearch.row.add($(html));
                                });
                                rolodexSearch.draw();
                            }
                        });
            }
            else
            {
                swal({
                                title: "Alert",
                                text: "Please enter Firm name.",
                                type: "warning"
                            });
            }
            }
            }
 });
        
            $(document.body).on('click', '#rolodexsearchResult_data tbody tr', function () {
                // $(this).toggleClass('selected');
              if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {

                $('#rolodexsearchResult_data tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click','#viewSearchedPartieDetails',function() {
            // $("#rolodexPartiesResult_data tr").removeClass("highlight");

             var selectedrows = rolodexSearch.rows('.table-selected').data();
                var caseNo = $("#caseNo").val();
                if(selectedrows > 0){
                    $('#firmcompanylist').modal('hide');
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/viewSearchedPartieDetails',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            cardcode: selectedrows[0][8],
                            caseno: caseNo
                        },
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (data) {
                            if (data == false) {
                                swal({
                                    title: "Alert",
                                    text: "Oops! Some error has occured while fetching details. Please try again later.",
                                    type: "warning"
                                });
                            } else {
                                var city= data.city;
                                if(city!= null)
                                {
                                    var strarray = city.split(',');
                                    $('input[name="card2_city"]').val($.trim(strarray[0]));
                                    $('input[name="card2_state"]').val($.trim(strarray[1]));
                                    $('input[name="card2_zip"]').val($.trim(strarray[2]));
                                 }
                                $('input[name="card2_firm"]').val(data.firm);
                                $('input[name="card2_address1"]').val(data.address1);
                                $('input[name="card2_address2"]').val(data.address2);

                                $('input[name="card2_venue"]').val(data.venue);
                                $('.addSearchedParties').attr("disabled", false);
                                $('.editPartyCard').attr("disabled", false);
                            }
                        }
                    });
                } else {
                    swal({
                        title: "Alert",
                        text: "Oops! Some error has occured while fetching details. Please try again later.",
                        type: "warning"
                    });
                }
            });
            $(document.body).on('click','.event_note_list',function(){
              $('#eventnotelist').modal('show');
            });
            $('#eventnotelist').on('shown.bs.modal', function (e) {
               category_list($('#case_category_event').val(),$('#caseNo').val());
            });
            $(document.body).on('change','.category_event_list',function(){
                category_list($('#case_category_event').val(),$('#caseNo').val());
            });
            $('#activity_list').on('ifClicked', function(event){
                 category_list($('#case_category_event').val(),$('#caseNo').val());
            });
            function category_list(category,caseno)
            {
                var status='';
                if($('#activity_list').prop("checked") == true){
                    status=1;
                } else {
                     status=0;
                }
                $.ajax({
                            url: '<?php echo base_url(); ?>cases/getcategoryeventlist',
                            method: 'POST',
                            data: {category_list:category,case_id:caseno,status:status},
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                                 categoryeventlist.clear();

                                //rolodexSearchDatatable
                                var obj = $.parseJSON(data);
                             // console.log(obj);
                                $.each(obj, function (index, item) {
                                    var html = '';
                                    html += '<tr class="categorylistevent" data-event="'+(item.event)+'" data-category=' + item.category + '><td>' + item.event + '</td>';
                                    html += '</tr>';
                                    categoryeventlist.row.add($(html));
                                });
                                categoryeventlist.draw();
                            }
                    });
            }
            $('#caseEventlist tbody').on('click', 'tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                $('#caseEventlist tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click','#caseactivityevent',function(){
                var row = categoryeventlist.row( '.table-selected' ).node();
                $('#eventnotelist').modal('hide');
                 $('#activity_event').val($(row).attr('data-event'));
            });

            $('#addcase.modal').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });
            });

            $('#firmcompanylist').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });
                $('#rolodexsearchResult_data').dataTable().fnClearTable();
            });

            $('#addparty').on('hidden.bs.modal', function () {
                $(this).find('input[type="text"]').each(function () {
                    this.value = '';
                });

            });

            $(document.body).on('click','.casepartydetails',function(){

                });
            $('#casePartylist tbody').on('click', 'tr', function () {
                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                } else {
                $('#casePartylist tr').removeClass('table-selected');
                    $(this).addClass('table-selected');
                }
            });
            $(document.body).on('click','#casepartysave',function(){
                      var row = casepartylist_data.row( '.table-selected' ).node();

                     // var res = $(row).find('td:eq(0)').text().split(" ");

                    $('#casepartyDetaillist').modal('hide');
                    if($(row).attr('data-active_data')=='general')
                    {
                        $('input[name=field_first]').val($(row).attr('data-first'));
                        $('input[name=field_last]').val($(row).attr('data-last'));
                        if(data-state)
                            $('input[name=field_state]').val($(row).attr('data-state'));
                        else
                            $('input[name=field_state]').val('');
                        $('input[name=field_zip_code]').val($(row).attr('data-zip'));
                        $('input[name=field_social_sec]').val($(row).attr('data-social_sec'));
                        $('input[name=field_aoe_coe_status]').val($(row).attr('data-aoe_coe_status'));
                        $('input[name=field_address]').val($(row).find('td:eq(3)').text());
                        $('input[name=field_city]').val($(row).find('td:eq(2)').text());
                    }
                    else if($(row).attr('data-active_data')=='employers1')
                    {
                        $('input[name=field_e_name]').val($(row).attr('data-first'));
                        $('input[name=field_e_address]').val($(row).find('td:eq(3)').text());
                        $('input[name=field_e_city]').val($(row).find('td:eq(2)').text());
                        $('input[name=field_e_state]').val($(row).attr('data-state'));
                        $('input[name=field_e_zip]').val($(row).attr('data-zip'));
                        $('input[name=field_e_phone]').val($(row).attr('data-phone'));
                        $('input[name=field_e_fax]').val($(row).attr('data-fax'));
                    }
                    else if($(row).attr('data-active_data')=='employers2')
                    {
                        $('input[name=field_e2_name]').val($(row).attr('data-first'));
                        $('input[name=field_e2_address]').val($(row).find('td:eq(3)').text());
                        $('input[name=field_e2_city]').val($(row).find('td:eq(2)').text());
                        $('input[name=field_e2_state]').val($(row).attr('data-state'));
                        $('input[name=field_e2_zip]').val($(row).attr('data-zip'));
                        $('input[name=field_e2_phone]').val($(row).attr('data-phone'));
                        $('input[name=field_e2_fax]').val($(row).attr('data-fax'));
                    }
                    else if($(row).attr('data-active_data')=='carriers1')
                    {

                        $('select[name=field_i_adjsal]').val($(row).attr('data-salutation'));
                        $('input[name=field_i_adjfst]').val($(row).attr('data-first'));
                        $('input[name=field_i_adjuster]').val($(row).attr('data-last'));
                        $('input[name=field_i_name]').val($(row).find('td:eq(1)').text());
                        $('input[name=field_i_address]').val($(row).find('td:eq(3)').text());
                        $('input[name=field_i_city]').val($(row).find('td:eq(2)').text());
                        $('input[name=field_i_state]').val($(row).attr('data-state'));
                        $('input[name=field_i_zip]').val($(row).attr('data-zip'));
                        $('input[name=field_i_phone]').val($(row).attr('data-phone'));
                        $('input[name=field_i_fax]').val($(row).attr('data-fax'));
                    }
                    else if($(row).attr('data-active_data')=='carriers2')
                    {

                        $('select[name=field_i2_adjsal]').val($(row).attr('data-salutation'));
                        $('input[name=field_i2_adjfst]').val($(row).attr('data-first'));
                        $('input[name=field_i2_adjuste]').val($(row).attr('data-last'));
                        $('input[name=field_i2_name]').val($(row).find('td:eq(1)').text());
                        $('input[name=field_i2_address]').val($(row).find('td:eq(3)').text());
                        $('input[name=field_i2_city]').val($(row).find('td:eq(2)').text());
                        $('input[name=field_i2_state]').val($(row).attr('data-state'));
                        $('input[name=field_i2_zip]').val($(row).attr('data-zip'));
                        $('input[name=field_i2_phone]').val($(row).attr('data-phone'));
                        $('input[name=field_i2_fax]').val($(row).attr('data-fax'));
                    }
                    else if($(row).attr('data-active_data')=='attorneys1')
                    {
                        $('input[name=field_d1_first]').val($(row).attr('data-first'));
                        $('input[name=field_d1_last]').val($(row).attr('data-last'));
                        $('input[name=field_d1_firm]').val($(row).find('td:eq(1)').text());
                        $('input[name=field_d1_address]').val($(row).find('td:eq(3)').text());
                        $('input[name=field_d1_city]').val($(row).find('td:eq(2)').text());
                        $('input[name=field_d1_state]').val($(row).attr('data-state'));
                        $('input[name=field_d1_zip]').val($(row).attr('data-zip'));
                        $('input[name=field_d1_phone]').val($(row).attr('data-phone'));
                        $('input[name=field_d1_fax]').val($(row).attr('data-fax'));
                    }
                    else
                    {
                        $('input[name=field_d2_first]').val($(row).attr('data-first'));
                        $('input[name=field_d2_last]').val($(row).attr('data-last'));
                        $('input[name=field_d2_firm]').val($(row).find('td:eq(1)').text());
                        $('input[name=field_d2_address]').val($(row).find('td:eq(3)').text());
                        $('input[name=field_d2_city]').val($(row).find('td:eq(2)').text());
                        $('input[name=field_d2_state]').val($(row).attr('data-state'));
                        $('input[name=field_d2_zip]').val($(row).attr('data-zip'));
                        $('input[name=field_d2_phone]').val($(row).attr('data-phone'));
                        $('input[name=field_d2_fax]').val($(row).attr('data-fax'));
                    }
                    $("#addInjuryForm select").trigger('change');

                });
            $(document.body).on('click','.injury_print',function(){
                var injuryId = $(this).attr('id');
                $('#injury_Print').modal('show');
                $('#injuryId').val(injuryId);
            });
            $(document.body).on('click','.print_injury_data',function(){
                var form_data_type = $('input[name=form_data_type]:checked').val();
                var injuryId =  $('#injuryId').val();
                var caseno=$('#hdnCaseId').val();
               // var print_option = $('input[name=print_option]:checked').val();
              //  var form_style = $('input[name=form_style]:checked').val();
                $.ajax({
                    url: '<?php echo base_url() . 'cases/printInjuryDetails' ?>',
                    method: "POST",
                    //dataType: 'json',
                    data: {
                            form_data_type: form_data_type,
                            injuryId:injuryId,
                            caseno:caseno
                          },
                    success: function (result) {
                                $('#print_injures_form').modal('hide');
                      $(result).printThis({
                            debug: false,
                           // header: "<h2><center>View Message</center></h2>"
                        });
                  }
              });
    });
    function attachParty(tab)
    {
         var caseno=$('#hdnCaseId').val();
                $('#casepartyDetaillist').modal('show');
                    $.ajax({
                            url: '<?php echo base_url(); ?>cases/getcasepartyDetails',
                            method: 'POST',
                            data: {caseno:caseno},
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                               // console.log(data);
                                 casepartylist_data.clear();


                                var obj = $.parseJSON(data);
                               if (obj.length < 1) {
                                 location.reload();
                                }
                                 $.each(obj, function (index, item) {
                                 var name=item.salutation +' '+item.first+' '+item.middle+' '+item.last;
                                // console.log($(this).attr('data-tab'));
                                if(item.state)
                                {
                                    var istate = item.state;
                                }
                                else
                                {
                                    var istate = '';
                                }
                                if(item.zip)
                                {
                                    var izip = item.zip;
                                }
                                else
                                {
                                    var izip = '';
                                }
                                if(item.city)
                                {
                                    var icity = item.city;
                                }
                                else
                                {
                                    var icity = '';
                                }
                                if(item.address1)
                                {
                                    var iadd1 = item.address1;
                                }
                                else
                                {
                                    var iadd1 = '';
                                }
                                if(item.firm)
                                {
                                    var ifirm = item.firm;
                                }
                                else
                                {
                                    var ifirm = '';
                                }
                                if(item.fax)
                                {
                                    var ifax = item.fax;
                                }
                                else
                                {
                                    var ifax = '';
                                }
                                if(item.phone1)
                                {
                                    var iph1 = item.phone1;
                                }
                                else
                                {
                                    var iph1 = '';
                                }
                                    var html = '';
                                    html += '<tr class="casepartydetails"  data-first="' + item.first+ '" data-state="' + istate +'" data-zip="' + izip + '"data-last="'+ item.last + '" data-active_data="'+tab+'"data-social_sec="'+item.social_sec+'"data-fax="'+ifax +'"data-phone="'+iph1+'"data-salutation="'+item.salutation +'" ><td id="name">' + name + '</td>';
                                    html+='<td id="firm">'+ifirm+ '</td>';
                                    html+='<td>'+icity+ '</td>';
                                    html+='<td>'+iadd1+ '</td>';
                                    html += '</tr>';
                                    casepartylist_data.row.add($(html));
                                });
                                casepartylist_data.draw();
                            }
                    });
    }


    function caseDetails(cardCode)
    {
            $.ajax({
                    url: '<?php echo base_url() . 'cases/getCasrcardDetails' ?>',
                    method: "POST",
                    //dataType: 'json',
                    data: {
                            cardCode:cardCode
                          },
                    success: function (result) {
                    $('#viewAllCases').html(result);
                    $("#caseCardDetails").modal('show');
                    }
              });
    }


    function fileCheck(obj) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1)
            {
                 document.getElementById('imgerror').innerHTML = "Only '.jpeg','.jpg', '.png', '.gif', '.bmp' formats are allowed.";
                 return false;
            }

    }
    $(document.body).on('click','.calendar_clone',function(){
        var atty_hand=$('select[name=atty_hand]').val();
        var attyass=$('select[name=attyass]').val();
        var name=$('#first').val()+' '+$('#last').val();
        var event=$('#event').val();

        $('#atty_hand_clone').val(atty_hand);
        $('#attyass_clone').val(attyass);
        $('#event_clone').val(event);
        $('#firstname_clone').val(name);
        $('#first_clone').val($('#first').val());
        $('#last_clone').val($('#last').val());
        if ($('#calendarForm').valid()) {
               // $.blockUI();
         $('#clonecalendar').modal('show');
     }
    });
    $(document.body).on('change','#entry_date',function(){
     //  var moment().format('M/D/YYYY');
        var array = [];
        var array1 = [];
       var option=$('#entry_date').val();
        if(option==2){
            for (var i = 0; i < 14; i++) {
                var d=moment().add(i, 'days');
                var d1=d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));

            }
        }
        else if(option==3){
            for (var i = 0; i < 30; i++) {
                var d=moment().add(i, 'days');
                var d1=d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        }
        else if(option==4){
            for (var i = 0; i < 4; i++) {
                var d=moment().add(i, 'weeks');
                var d1=d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        }
        else if(option==5){
            for (var i = 0; i < 24; i++) {
                var d=moment().add(i, 'weeks');
                var d1=d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        }
        else if(option==6){
            for (var i = 0; i < 52; i++) {
                var d=moment().add(i, 'weeks');
                var d1=d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        }
        else if(option==7){
            for (var i = 0; i < 6; i++) {
                var d=moment().add(i, 'month');
                var d1=d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        }
        else if(option==8){
            for (var i = 0; i < 12; i++) {
                var d=moment().add(i, 'month');
                var d1=d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        }
        else
        {
              array.push(moment().format('M/D/YYYY , dddd'));
              array1.push(moment(d1).format('YYYY-M-D'));
        }
         // display the result in myDiv
          $('#Date_copy').text('');
       //console.log(array1);
          $('#startend1').val(array1);
    for(var i=0;i<array.length;i++){
        $('#Date_copy').append(array[i]+'\n');
    }
       // console.log(array.join(','));
   // alert(array);
    });       // Add/Edit Calendar Addclonecase

        $('#clonecalender').on('click', function () {
//           alert($('#calendarForm').serialize());
             //   var =$('#calendarForm').serialize()
            var url = '<?php echo base_url()?>calendar/addCalendarCloneData';
            $.blockUI();
                $.ajax({
                    url: url,
                    dataType: 'json',
                    method: 'post',
                    data: $('#calendarForm').serialize()+"&startend1=" + $('#startend1').val()+"&attyass=" +$('#attyass_clone').val()+"&first="+$('#firstname_clone').val(),
                  //  data: $('#myForm').serialize() + "&moredata=" + morevalue
                     complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        //alert(data[0].status);
                      // console.log(data.length);return false;
                         $.unblockUI();
                        if (data[0].status == '1') {
                            swal("Success !", data.message, "success");
                            $('#Addclonecase')[0].reset();
                            $('#calendarForm')[0].reset();
                            $('#addcalendar').modal('hide');
                            $('#clonecalendar').modal('hide');
                            $('#addcalendar .modal-title').html('Add Calendar');
                                createCalendarRow1(data);
                                  $('#calendar').fullCalendar('refetchEvents');
                        } else {
                            swal("Oops...", data.message, "error");
                        }


                    }
                });

        });
        function createCalendarRow1(data) {
          var html = '';
          var arr=[];
            var json_color = jQuery.parseJSON('<?php echo color_codes ?>');
            if (json_color.hasOwnProperty(data.color)) {
                    style = "background-color:" + json_color[data.color][2] + ";color:" + json_color[data.color][1];
                } else {
                    style = "background-color:#FFFFFF;color: #000000";
                }
              for (var i = 0; i < data.length; i++){
                //console.log(data[i].calendarData.last);
               // $.each(data[i].calendarData, function (key ,data) {
                html = "<tr class='gradeX' id='calEvent_" + data.eventno + "' style='" + style + "'>";
                html += "<td>" + moment(data[i].calendarData.date).format('DD/MM/YYYY HH:mm') + "</td>";
                html += "<td>" + data[i].calendarData.atty_hand + "</td>";
                html += "<td>" + data[i].calendarData.attyass + "</td>";
                html += "<td>" + data[i].calendarData.first + ' ' + data[i].calendarData.last + ' vs ' + data[i].calendarData.defendant + "</td>";
                html += "<td>" + data[i].calendarData.event + "</td>";
                html += "<td>" + data[i].calendarData.venue + "</td>";
                html += "<td>" + data[i].calendarData.judge + "</td>";
                html += "<td>" + data[i].calendarData.notes + "</td>";
                html += "<td>";
                html += "<a class='btn btn-xs btn-info mar-right4 event_edit' data-eventno=" + data.eventno + " href = 'javascript:;'><i class = 'icon fa fa-paste'></i> Edit</a>";
                html += "<a class='btn btn-xs btn-danger event_delete' data-eventno=" + data.eventno + " href = 'javascript:;'><i class = 'icon fa fa-times'></i> Delete</a>";
                html += "</td>";
                html += "</tr>";
                arr.push(html);
              }
             for(var i = 0; i < arr.length; i++){
                  caseDetailCalendarTable.row.add($(arr[i])).draw();
              }
         }
      /*  $(document.body).on('click','.claender_event',function(){
             var caldate=$('#caldate').val();
             var caltime=$('#caltime').val();
             var judge=$('select[name=judge]').val();
            // var judge=$('select[name=judge]').val();
             var defendant=$('input[name=defendant]').val();
             var atty_hand=$('select[name=atty_hand]').val();
             $('#activity_event').val('<Calender>'+','+caldate+caltime+',Judge:'+judge+',Defendant:'+defendant+',Atty Assigned:'+atty_hand);
             $('#activity_event_desc').val('<Calender>'+','+caldate+caltime+',Judge:'+judge+',Defendant:'+defendant+',Atty Assigned:'+atty_hand);
            $('#addcaseact').modal('show');
         }); */

        $('#addparty.modal').on('show.bs.modal', function () {
                if($('table#rolodexPartiesResult tr').hasClass('highlight') == false)
                {
                    $('.addSearchedParties').prop('disabled', true);
                    $('.editPartyCard').prop('disabled', true);
                }
        });

        $(document.body).on('click','.clear_form',function(){//alert("bvn");
             // $('#calendarForm')[0].reset();
              $('#calendarForm').find('input,textarea,select').each(function () {
                    this.value = '';
                    $(this).iCheck('uncheck');
                    $(this).trigger('change');
                });
        });

        $('#firmcompanylist').on('show.bs.modal', function () {
                //$(".dataTables_scrollHeadInner").removeAttr("style");
                $(".dataTables-example").removeAttr("style");
                 $(".dataTables_scrollHeadInner").removeAttr("style");
                $(".dataTables-example").css("width","100% !important;");
                $(".dataTables_scrollHeadInner").css("width","100% !important;");
        });
        $('#addcaseact.modal').on('hidden.bs.modal', function () {
                    $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                        this.value = '';
                    });
                    $('#addcaseact a[href="#tab-26"]').tab('show');
        });
        $(document.body).on('click','.CaseActEmail',function(){
            var activity_event=$('#activity_event').val();
            if (CKEDITOR.instances.mailbody1) CKEDITOR.instances.mailbody1.destroy();
           CKEDITOR.replace('mailbody1');
            $('#addcaseact').modal('hide');
           /* var body=$('.emailDetails').html();
                    modal = $('#CaseEmailModal');
                    modal.find('.modal-body').html(body);*/
                    $('#CaseEmailModal').modal('show');
                    $('#mailbody1').val(activity_event);

         });
        $(document.body).on('click','.CaseActPrint',function(){
             $.ajax({
                   url: '<?php echo base_url() . 'cases/printpde' ?>',
                    method: "POST",
                    data: {caseno: $('#hdnCaseId').val()},
                    success: function (result) {
                        //var link = $.trim('<?php echo base_url(); ?>'+ 'assets/clients/'+$('#caseNo').val()+'/'+$.trim(data));

                       var based="<?php echo base_url();?>"+ 'assets/printCaseAct/'+result;
                      window.open(based,'_blank');
                    }
                });

        });

        $('#ProspectListDatatable tbody').on('click', 'tr', function () {

                if ($(this).hasClass('table-selected')) {
                    $(this).removeClass('table-selected');
                }
                else
                {
                   ProspectListDatatable.$('tr.table-selected').removeClass('table-selected');
                    $(this).addClass('table-selected');
                     var row = ProspectListDatatable.row( '.table-selected' ).node();
                    prospect_detail(row);

                }
});


   $('#proceedprospects.modal').on('hidden.bs.modal', function () {
        $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
                       this.value = '';
  });
  $('#prospectForm').validate().resetForm();
     $('#prospectForm').find('.error').removeClass('error');
              $('#proceedprospects .tab-content div#tab-6').addClass('active');
   });

$('#CaseEmailModal.modal').on('hidden.bs.modal', function () {

                     $(this).find('input[type="email"],textarea,select,file').each(function () {
                       this.value = '';
                    });
                    $('#case_category1').val('1');
                    $('#mail_subject1').val('');
                    $('#emails1').val('');
                    $("#send_mail_form1 select").trigger('change');
                     var $el = $('#afiles1');
                       $el.wrap('<form>').closest('form').get(0).reset();
                       $el.unwrap();
                });

    $(document.body).on('change','#activity_fee',function(){
     if($(this).val()=='OTHER'){
           $("#activity_fee1").show();
     }
     else{
          $("#activity_fee1").hide();
           //$("#activity_fee1").val('');
          $("#activity_fee1").val('');
     }

    });
     $(document.body).on('blur','#case_act_time',function(){
       //  alert($(this).val());
         var a = eval($(this).val());
          $('#case_act_time').val(a);
        var hour=$('#case_act_hour_rate').val();
        if(hour> 0){
           var  cost=parseInt(a*hour)/parseInt(60);
          $('#case_act_cost_amt').val(cost);
        }
    });
    $(document.body).on('blur','#case_act_hour_rate',function(){
       //  alert($(this).val());
         var a = eval($(this).val());

        var hour=$('#case_act_time').val();//minutes
        if(hour> 0){
           var  cost=parseInt(a*hour)/parseInt(60);

            $('#case_act_cost_amt').val(cost);
        }
    });
     var regExp = /[a-z]/i;
    function validate(e) {
  var value = String.fromCharCode(e.which) || e.key;

    // No letters
    if (regExp.test(value)) {
      e.preventDefault();
      return false;
    }
}

function remove_error(id)
{
    var myElem = document.getElementById(id+'-error');
    if (myElem === null)
    {

    }
    else
    {
        document.getElementById(id+'-error').innerHTML =  '';
    }
}

function SubmitForm()
{
    if ($("#add_new_case_activity").valid()) {
        return true;
    }
    else{
        return false;
    }
}
$('#addcaseact').on('hidden.bs.modal', function() {
    var $alertas = $('#add_new_case_activity');
    $alertas.validate().resetForm();
    $alertas.find('.error').removeClass('error');
});
$(document.body).on('click','.calnote',function(){
 $('textarea[name=notes]').val('<?php echo isset($system_caldata[0]->calnotes)?$system_caldata[0]->calnotes:''; ?>');

});
 /*$(document.body).on('click','#printProspectdetails',function(){
     $('#printProspectmodal').modal('show');
    });*/
$(document.body).on('change click','.weekendsalert',function(){
    var weekalert="<?php echo isset($holidayset->weekends)?$holidayset->weekends:0?>";
   // alert(weekalert);
    if(weekalert!=0 && weekalert=='on' ){
        var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        var a = new Date($(this).val());
        if(weekday[a.getDay()]=='Saturday' ||weekday[a.getDay()]== 'Sunday')
        {
            swal({
                title:"",
                text: "This day is "+weekday[a.getDay()],
                customClass: 'swal-wide',
                confirmButtonColor: "#1ab394",
                confirmButtonText: "OK",
            });
        }
    }
    //alert("hi"); alert(date('l',)$(this).val());
});
$(document.body).on('change click','.alerttonholiday',function(){
   var aletholi=$(this).val();
    var aftholiday="<?php echo isset($holidayset->aftholiday)?$holidayset->aftholiday:0?>";
    if(aftholiday!=0 && aftholiday=='on'  && $(this).val()!=''){
        var holidaylist = '<?php echo isset($holodaylist)?json_encode($holodaylist):0; ?>';
        if(JSON.parse(holidaylist).length > 0){
            $.each(JSON.parse(holidaylist), function( key, value ) {
             var temp = new Array();
             temp = value.split(",");
              var c = new Date(temp[0]);
              var b=new Date(aletholi);
               if(c.getTime()==b.getTime()){

               swal({
                    title:"",
                    text: "This day is "+temp[1],
                    customClass: 'swal-wide',
                    confirmButtonColor: "#1ab394",
                    confirmButtonText: "OK",
                });
            }

            });
        }
    }

});

$(document.body).on('change click','.birthcalalert',function(){
    var bialaddcalevent="<?php echo  isset($main1->bialaddcalevent)?$main1->bialaddcalevent:0?>";
    if(bialaddcalevent!=0 && bialaddcalevent=='on'){
        var birthdate="<?php echo isset($display_details->birth_date)?date('m/d/Y',strtotime($display_details->birth_date)):0?>";
        if(birthdate != '' && birthdate!= 0)
        {  if($(this).val()!=''){
                var b=new Date(birthdate);
                var y=new Date();
                var year = y.getFullYear();
                var month = b.getMonth();
                var day = b.getDate();
                var c = new Date(year, month, day);
                var b=new Date($(this).val());
                if(b.getTime() === c.getTime())
                {
                    swal({
                        title:"Notice",
                        text: "You are scheduling this appointment on the client's birthday",
                        confirmButtonColor: "#1ab394",
                        confirmButtonText: "OK",
                    });
                }

            }
        }

    }

});
$(document.body).on('change click','.staffvacation',function(){
    var Uservacation = '<?php echo isset($Uservacation)?json_encode($Uservacation):0; ?>';

    if($('.staffTO').val()!="" && JSON.parse(Uservacation).length > 0 && $('.staffTO').val()=='<?php echo $this->session->userdata('user_data')['username'] ?>' && $('.sfinishby').val()!=''){
        $.each(JSON.parse(Uservacation), function( key, value ) {
            var temp = new Array();
            temp = value.split(",");
            if(temp[0].includes("-")==true){
                    var result = temp[0].split('-');
                    var dateFrom = result[0];
                    var dateTo = result[1];
                    var dateCheck = $('.sfinishby').val();
                    var from = Date.parse(dateFrom);
                    var to   = Date.parse(dateTo);
                    var check = Date.parse(dateCheck );

                    if((check <= to && check >= from))     // alert("date contained");
                     swal({
                        title:"",
                        text: temp[1],
                        customClass: 'swal-wide',
                        confirmButtonColor: "#1ab394",
                        confirmButtonText: "OK",
                        timer: 2000
                    });


            }else {
                var c = new Date(temp[0]);
                var b=new Date($('.sfinishby').val());
                if(c.getTime()==b.getTime()){
                    swal({
                        title:"",
                        text: temp[1],
                        customClass: 'swal-wide',
                        confirmButtonColor: "#1ab394",
                        confirmButtonText: "OK",
                    });
                }
            }
            });

        }


});

$(document.body).on('click','#save_prospect',function(){
         //   alert($('#prospectId').val());return false;
                    if ($("#prospectForm").valid()) {
                        $.blockUI();
                        var url = '';
                        if($('#prospectId').val() != '' && $('#prospectId').val() != 0){
                            url = '<?php echo base_url(); ?>prospects/editProspects';
                        }else{
                            url = '<?php echo base_url(); ?>prospects/addProspects';
                        }
                       /* $.ajax({
                            url: url,
                            method: "POST",
                            data: $('#prospectForm').serialize(),
                            success: function(result) {  //var data = $.parseJSON(result);

                            $.unblockUI();
                            var data = $.parseJSON(result);
                            if (data.status == '1') {
                                 //alert(data.prospect_id_last);return false;
                                swal("Success !", data.message, "success");
                            // if($('#prospectId').val() != '' && $('#prospectId').val() != 0){
                                    $.ajax({
                                        url: '<?php echo base_url(); ?>prospects/getProspectsdata',
                                        method: "POST",
                                        data:{prospect_id:data.prospect_id_last},
                                        beforeSend: function (xhr) {
                                        },
                                        complete: function (jqXHR, textStatus) {
                                        // $.unblockUI();
                                        },
                                        success: function(result) {
                                        var data_new = $.parseJSON(result);
                                        //console.log(data_new);return false;
                                        // console.log(data_new);
                                        ProspectAjaxData=data_new; }
                                    });
                              // }
                                    var name=$('#pro_salutation').val()+' '+$('#pro_first').val()+' '+$('#pro_last').val();
                                    $('.pro_first1').text($('#pro_first').val());
                                    $('.pro_last1').text($('#pro_last').val());
                                    $('.pro_first').html(name);
                                    $('.pro_cell').html($('#pro_cell').val());
                                    $('.pro_business').html($('#pro_business').val());
                                    $('.pro_home').html($('#pro_home').val());
                                    $('.pro_address').html($('#pro_address1').val());
                                    $('.pro_city_st_zip').html($('#pro_city').val()+" "+$('#pro_state').val()+" "+$('#pro_zip').val());
                                    $('.pro_social_sec').html($('#pro_social_sec').val());
                                    $('.pro_birthdate').html($('#pro_birthdate').val());
                                    $('.pro_email').html($('#pro_email').val());

                                    //section 2
                                    $('.pro_type').html($('#pro_type').val());
                                    $('.pro_casestat').html($('#pro_casestat').val());
                                    $('.pro_daterefin').html($('#pro_daterefin').val());
                                    $('.pro_datelastcn').html($('#pro_datelastcn').val());
                                    $('.pro_initials0').html($('#pro_initials0').val());
                                    $('.pro_pendwith').html($('#pro_pendwith').val());
                                    $('.pro_atty').html($('#pro_atty').val());
                                    if($('#mailingh').val()=='Y'||$('#mailingh').val()=='1')
                                    { $('.pro_mailingh').html('Y');  }else { $('.pro_mailingh').html("");}
                                    //section 3
                                    $('.pro_refcode').html($('#pro_refcode').val());
                                    $('.pro_refsource').html($('#pro_refsource').val());
                                    $('.pro_refby').html($('#pro_refby').val());
                                    //section 4
                                    $('.pro_assciation').html($('#pro_accociation').val());
                                    $('.pro_refto').html($('#pro_refto').val());
                                    $('.pro_dateref').html($('#pro_dateref').val());
                                    $('.pro_outrefstat').html($('#pro_outrefstat').val());
                                    $('.pro_ref_fee').html($('#pro_reffee').val()+" "+$('#pro_followup_date').val());
                                    $('.pro_amount').html($('#pro_amount').val());
                                    $('.date_follow_paid').html($('#pro_datepaid').val()+"/"+$('#pro_followup_date').val());

                                    //tab 1 notes
                                    $('.pro_notes').text($('#pro_notes').val());
                                    //tab 3 injury
                                    $('.pro_injn_emp').html($('#pro_injn_emp').val());
                                    $('.pro_injn_date').html($('#pro_injn_date').val());
                                    $('.pro_injn_body').html($('#pro_injn_body').val());
                                    $('.edit_prospect').attr('data-prospect_id',data.prospect_id_last);
                                    $('.delete_prospect').attr('data-pros_id',data.prospect_id_last);
                            }
                                   $('#prospectForm')[0].reset();
                                   $('#proceedprospects').modal('hide');
                            },

                    });
                                    return false; */
                }

        });

$(document.body).on('click', '.activity_view', function () {
    var fname= $(this).data('actid');
    viewActDoc(fname)
});
 $('.discard_message').click(function (e) {
            swal({
                title: "Are you sure?",
                text: "The changes you made will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, discard it!",
                closeOnConfirm: true
            }, function () {
               // $('.inbox').trigger("click");
                $('#CaseEmailModal').find('.att-add-file').not(':first').remove();
                $('#CaseEmailModal').modal("hide");
            });
        });
$(document).on('click','.ca-addfile',function(){
$('.att-add-file:last').after('<label class="col-sm-2 control-label"></label><div class="col-sm-9 att-add-file marg-top5 clearfix"> <input type="file" name="afiles1[]" id="afiles1" multiple ><i class="fa fa-plus-circle marg-top5 ca-addfile" aria-hidden="true"></i> <i class="fa fa-times marg-top5 removefile" aria-hidden="true"></i></div>');
});
$(document).on('click','.removefile',function(){
$(this).closest('.att-add-file').prev('label').remove();
$(this).closest('.att-add-file').remove();
});
function viewActDoc(fname){
            //alert(id);
            var caseNo = $("#caseNo").val();
            $.ajax({
                    url: '<?php echo base_url(); ?>cases/viewcaseactdoc',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        filename: fname,
                        caseno: caseNo,
                    },
                    success: function (data) {

                    }

                });
        }
</script>



