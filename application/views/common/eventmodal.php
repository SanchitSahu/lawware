<?php
include('js/calendarjs.php');
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '')
            unset($holodaylist[$key]);
    }
}
if (isset($staff_details)) {
    $Uservacation = isset($this->data['staff_details'][0]->vacation) ? (explode("##", trim($this->data['staff_details'][0]->vacation))) : '';
    if (!empty($Uservacation)) {
        $Uservacation = array_map('trim', $Uservacation);
        foreach ($Uservacation as $key => $value) {
            if (is_null($value) || $value == '')
                unset($Uservacation[$key]);
        }
    }
}
?>
<style>
    .chosen-container {
        width: 100% !important;
    }
    .pac-container{z-index: 9999;}
    .uk-dropdown {
        width: 98%;
    }
</style>

<div id="addcalendar" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Appointment/Event To Calendar </h4>
            </div>
            <div class="modal-body">
                <form role="form" id="calendarForm" name="calendarForm" method="post" >
                    <input type="hidden" id="mode" name="mode" value="add">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general1">General</a></li>
                            <li class=""><a data-toggle="tab" href="#notes">Notes</a></li>
                            <li class=""><a data-toggle="tab" href="#ticklers">Ticklers/Alerts</a></li>
                            <li class=""><a data-toggle="tab" href="#properties">Properties</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>General Information</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <input type="hidden" name="eventno" value="0"/>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Attorney Handling</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <select class="form-control cstmdrp cstmdrp-placeholder" id="atty_hand" placeholder="Select Attorney Handling" name="atty_hand" value="" >
                                                                <option value="" disabled="disabled">Select Attorney Handling</option>
                                                                <?php
                                                                if (isset($atty_hand)) {
                                                                    foreach ($atty_hand as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val ?>" <?php
                                                                        if (isset($display_details->atty_resp) && $val == $display_details->atty_resp) {
                                                                            echo "selected='selected'";
                                                                        }
                                                                        ?> ><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Assigned</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <select class="form-control cstmdrp cstmdrp-placeholder" id="attyass" name="attyass" placeholder="Select Attorney Assigned" value="" >
                                                                <option value="" disabled="disabled">Select Attorney Assigned</option>
                                                                <?php
																if (isset($atty_resp)) {
                                                                    foreach ($atty_resp as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val ?>" <?php
                                                                        if (isset($display_details->atty_hand) && $val == $display_details->atty_hand) {
                                                                            echo "selected='selected'";
                                                                        }
                                                                        ?> ><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Judge</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <select  class="form-control cstmdrp cstmdrp-placeholder" name="judge" id="judge" placeholder="Select Judge">
                                                                <?php
                                                                if (isset($JudgeName)) {
                                                                    foreach ($JudgeName as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Location</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="text" class="form-control" name="location" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">First Name</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="text" class="form-control" value="<?php echo isset($display_details->first) ? $display_details->first : ''; ?>" name="first" id="first" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Name</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="text" class="form-control" value="<?php echo isset($display_details->last) ? $display_details->last : ''; ?>" name="last" id="last" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Defendant</label>
                                                        <?php
                                                        /*echo $defendant;
                                                         if (isset($client_defendant) && !empty($client_defendant)) {
                                                          $defendant = $client_defendant[0]->first . ' ' . $client_defendant[0]->last;
                                                          } else {
                                                          $defendant = '';
                                                          } */
                                                        ?>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="text" class="form-control" name="defendant" value="<?php echo $defendant; ?>" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Venue</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <select class="form-control cstmdrp cstmdrp-placeholder" name="venue" id="venue" placeholder="Select Venue">
                                                                <option value="" disabled>Select Venue</option>
                                                                <?php
                                                                if (isset($Venue)) {
                                                                    foreach ($Venue as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="text" class="form-control injurydate date-field birthcalalert alerttonholiday" pattern="\d{1,2}/\d{1,2}/\d{4}" name="caldate" id="caldate" value="<?php echo date('m/d/Y'); ?>" data-mask="99/99/9999" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Time</label>
                                                        <div class="col-sm-7 no-paging uk-autocomplete">
                                                            <input class="form-control" name="caltime" id="caltime"  pattern="\d{1,2}:\d{2}( ?[apAP][mM])?" value="" data-uk-timepicker="{format:'12h'}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-md btn-primary btn-danger clear_form" tabindex="-1">Clear</button>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Event <span class="asterisk">*</span></label>
                                                        <div class="col-sm-7 no-paging"> <select  class="form-control cstmdrp cstmdrp-placeholder" name="event" id="event" style="" placeholder="Select Event">
                                                                <?php
                                                                $calevent = isset($main1->event) ? $main1->event : '';
                                                                $calevent = explode(',', $calevent);
                                                                foreach ($calevent as $val) {
                                                                    ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Total Time in Minutes</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="number" class="form-control" name="totime" min="0" value="0"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="notes" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5><a data-toggle="tab" href="#" class="calnote">Notes</a></h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group">
                                                <textarea class="form-control" name="notes" style="height: 300px;width: 100%;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="ticklers" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Tickler / Alerts</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row marg-bot5">
                                                        <label class="col-sm-5 no-left-padding marg-bot0">Type of Appointment</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <select class="form-control select2Class" name="tickle">
                                                                <option value="">Normal</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clarfix"></div>
                                                    <div class="row marg-bot5">
                                                        <label class="col-sm-5 no-left-padding">Date</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="text" name="tickledate" class="form-control injurydate date-field" id="tickledate" readonly />
                                                        </div>
                                                    </div>
                                                    <div class="clarfix"></div>
                                                    <div class="row marg-bot5">
                                                        <label class="col-sm-5 no-left-padding">Time</label>
                                                        <div class="col-sm-7 no-paging uk-autocomplete">
                                                            <input type="text" name="tickletime" class="form-control injurytime date-field" id="tickletime" data-uk-timepicker="{format:'12h'}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="clarfix"></div>
                                                   
                                                    <div class="clarfix"></div>
                                                    <div class="row marg-bot5">
                                                        <button type="button" id="ticklerClear" class="btn btn-md btn-primary btn-danger">Clear</button>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="row marg-bot5">
                                                        <label class="col-sm-5 no-left-padding">From</label>
                                                        <div class="col-sm-7 no-paging"><select class="form-control select2Class" name="whofrom" id="whofrom">
                                                                <?php
                                                                if (isset($staffList)) {
                                                                    foreach ($staffList as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['username']) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row marg-bot5">
                                                        <label class="col-sm-5 no-left-padding">Tickler Assigned to</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <select class="form-control select2Class" name="whoto" id="whoto">
                                                                <?php
                                                                if (isset($staffList)) {
                                                                    foreach ($staffList as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="ibox-content">
                                                 <div class="row marg-bot5">
                                                    <label class="container-checkbox pull-left">Reminder
                                                        <input type="hidden" name="calreminder" id="calreminder">
                                                        <input type="checkbox" class="treminder" name="reminder">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    </div>
                                               
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row marg-bot5">
                                                        <label class="col-sm-5 no-left-padding">Events</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <select class="form-control select2Class" name="tevent" id="tevent">
                                                                <?php
                                                                if (isset($ticklerEvent)) {
                                                                    foreach ($ticklerEvent as $val) {
                                                                        ?>
                                                                        <option value="<?php echo $val->title; ?>"><?php echo $val->title; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row marg-bot5">
                                                        <label class="col-sm-5 no-left-padding">Doctor</label>
                                                        <div class="col-sm-7 no-paging">
                                                            <input type="text" name="doctor" class="form-control" id="doctor"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="properties" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>General Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Category</label>
                                                        <div class="col-sm-7 no-paging marginbottom-5"><select class="form-control select2Class" name="todo" id="todo">

                                                                <?php
                                                                if (isset($system_caldata[0]->calendars))
                                                                    $calendars = trim($system_caldata[0]->calendars);
                                                                else
                                                                    $calendars = '';
                                                                if ($calendars == null) {
                                                                    ?>  <option value="">Calendars</option>
                                                                    <?php
                                                                } else {
                                                                    $calcate = explode(',', $system_caldata[0]->calendars);
                                                                    ?>
                                                                    <?php foreach ($calcate as $key => $val) { ?>
                                                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Color</label>
                                                        <div class="col-sm-7 no-paging marginbottom-5"><select class="form-control select2Class" name="color">
                                                                <option value="0">Select Color</option>
                                                                <?php
                                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                                foreach ($jsoncolor_codes as $k => $color) {
                                                                    ?>
                                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Case No</label>
                                                        <div class="col-sm-7 no-paging"><input type="text" class="form-control  caseno" name="caseno" value="<?php echo $case_no; ?>" />
                                                        </div></div><div class="clearfix"></div>
                                                    <!--                                                    <div class="form-group">
                                                                                                            <span><label>Protected <input type="checkbox" class="i-checks" name="check_protected" value="1"></label></span>
                                                                                                        </div>
                                                                                                        <div class="form-group">
                                                                                                            <span><label>Rollover <input type="checkbox" class="i-checks" name="rollover" value="1"></label></span>
                                                                                                        </div>-->
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Last Modification Details</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Origin Initials</label>
                                                        <div class="col-sm-7 no-paging"> <input type="text" name="initials0" class="form-control" readonly/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Initials</label>
                                                        <div class="col-sm-7 no-paging"> <input type="text" name="initials" class="form-control" readonly/>
                                                        </div></div><div class="clearfix"></div>
                                                    <!--                                                    <div class="form-group">
                                                                                                            <span><label>Override <input type="checkbox" class="i-checks"></label></span>
                                                                                                        </div>-->
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Initials</label>
                                                        <div class="col-sm-7 no-paging"> <input type="text" class="form-control" readonly name="chgwho"/>
                                                        </div></div><div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Date</label>
                                                        <div class="col-sm-7 no-paging"><input type="text" class="form-control injurydate clonedate date-field" data-mask="99/99/9999" readonly id="lastDate" style="background-color: #eee !important" disabled="disabled" name="chgdate"/>
                                                        </div></div><div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Time</label>
                                                        <div class="col-sm-7 no-paging uk-autocomplete"><input type="text" class="form-control injurytime" readonly name="chgtime" data-uk-timepicker="{format:'12h'}"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-group text-center ">
                    <button type="submit" class="btn btn-md btn-primary pull-left calendar_clone">Clone</button>
                    <button type="submit" id="btnSaveCalendar" class="btn btn-md btn-primary">Save</button>
                    <button type="button" id="btnCloseCalendar" data-dismiss="modal" class="btn btn-md btn-primary btn-danger mar-right7">Cancel</button>
					<span id="pullcaseevent"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="clonecalendar" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">

    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Clone Calendar Appointments </h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <form id="Addclonecase" name="Addclonecase" novalidate="novalidate">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-6 ">
                                        <div class="form-group">
                                            <label>Attorney/Handling</label>
                                            <input type="text" class="form-control" name="atty_hand_clone" id="atty_hand_clone" readonly="readonly">

                                        </div>
                                        <div class="form-group">
                                            <label>Assigned</label>
                                            <select class="form-control cstmdrp" id="attyass_clone" name="attyass_clone" value="">
                                                <option value="">Select Attorney Assigned</option>
                                                <?php
                                                if (isset($atty_resp)) {
                                                    foreach ($atty_resp as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $val ?>" <?php
                                                        if (isset($display_details->atty_resp) && $val == $display_details->atty_resp) {
                                                            echo "selected='selected'";
                                                        }
                                                        ?>><?php echo $val; ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text"  class="form-control" name="firstname_clone"id="firstname_clone"/>
                                            <input type="hidden"  class="form-control" name="first_clone"id="first_clone"/>
                                            <input type="hidden"  class="form-control" name="last_clone"id="last_clone"/>

                                        </div>
                                        <div class="form-group">
                                            <label>Event</label>
                                            <input type="text" class="form-control" name="event_clone" id="event_clone" readonly="readonly">


                                        </div>
                                        <div class="form-group" id="cloneDate">
                                            <label>Date</label>
                                            <select class="form-control" name="entry_date" id="entry_date">
                                                <option value="1"><?php echo date('m/d/Y') ?></option>
                                                <option value="2"><?php echo date('m/d/Y') . ' 14 Days (daily)' ?></option>
                                                <option value="3"><?php echo date('m/d/Y') . ' 30 Days (daily)' ?></option>
                                                <option value="4"><?php echo date('m/d/Y') . ' 4 Weeks (weekly)' ?></option>
                                                <option value="5"><?php echo date('m/d/Y') . ' 24 Weeks(weekly)' ?></option>
                                                <option value="6"><?php echo date('m/d/Y') . ' 52 Weeks(weekly)' ?></option>
                                                <option value="7"><?php echo date('m/d/Y') . ' 6 Months(monthly)' ?></option>
                                                <option value="8"><?php echo date('m/d/Y') . ' 12 Months(monthly)' ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group appointments"id="appointments">
                                            <label>Dates to copy appointments to</label>
                                            <?php
                                            $d = date('m/d/Y');
                                            $day = date("l", strtotime($d));
                                            ?>
                                            <textarea class="form-control" style="height: 320px;width: 100%;" id="Date_copy" name="Date_copy" readonly><?php echo $d . ',' . $day; ?></textarea>
                                            <input type="hidden" name="startend1" id="startend1" value="<?php echo date('Y-m-d') ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <!--span class="marg-top5 pull-right"><label>&nbsp;&nbsp;<div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" class="i-checks" name="notify" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></label></span-->
                                            <label class="container-checkbox pull-right">Clone the notes <input type="checkbox" name="notify" id="notify"> <span class="checkmark"></span> </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary" id="clonecalender">Clone</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="cloneCaseWarning" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <span>Duplicate Entry on same date. Proceed?</span>
                                <br /><br />
                                <table class="" width="100%">
                                    <tr>
                                        <td align="left">Click Yes </td>
                                        <td align="left">
                                            <span class="">To copy event with same date.</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">Click No </td>
                                        <td align="left">
                                            <span class="">To copy event without same date.</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">Click Cancel </td>
                                        <td align="left">
                                            <span class="">To cancel and not make changes at all.</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <button id="clone_event_warning" type="button" class="btn btn-primary">Yes</button>
                        <button id="clone_event_warning_no" type="button" class="btn btn-primary">No</button>
                        <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var venue_table_data = '';
    var swalFunction = '';
    var CaseHID = $("#hdnCaseId").val();
    var Clonetoday = 0;
    $(document).ready(function () {
        $('input.numberinput').bind('keypress', function (e) {
            return !(e.which != 8 && e.which != 0 &&
                    (e.which < 48 || e.which > 57) && e.which != 46);
        });
        var calYearRange = $('#calYearRange').val();
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
        /*$('#caltime').val(currenttime);*/
        /*var input1 = $('#caltime');
        input1.mdtimepicker({
            autoclose: true
        });*/
        /*$('.injurytime').mdtimepicker({
            autoclose: true
        });*/
        $('.injurydate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        setCalendarValidation();
        $.fn.modal.Constructor.prototype.enforceFocus = function () {};
        $('.contact').mask("(000) 000-0000");
        $('.social_sec').mask("000-00-0000");
        $('#addcontact.modal').on('show.bs.modal', function () {
            document.getElementById('imgerror').innerHTML = '';
            document.getElementById('hiddenimg').value = '';
            $(".chosen-language").chosen();
        });
        /* Start Calendar Modal Hide/Show */
        $('#addcalendar a[data-toggle="tab"]').on('click', function (e) {
            /*if ($('#cal_typ').length > 0)
            {
                var sel_val = $('#cal_typ').val();
                $("#todo").val(sel_val).trigger('change');
            }*/
            if (!$('#calendarForm').valid() && $('input[name="eventno"]').val() == '0') {
                e.preventDefault();
                return false;
            }
        });

        $('#addcalendar.modal').on('show.bs.modal', function () {

            var adarr = '<?php echo isset($system_caldata[0]->calattyass) ? $system_caldata[0]->calattyass : '' ?>';
            if (adarr == '3') {
                $("#attyass").attr("disabled", true);
            } else if(adarr == '1') {
                $("#attyass").removeAttr("disabled");
            }

            var d = new Date();
            var hours = d.getHours();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            var currenttime = d;
            var currentMinute = d.getMinutes();

            if(currentMinute <=30){
                d.setMinutes(30);
            } else {
                d.setMinutes(00);
                if(d.getHours() == 11){
                    ampm = (ampm == "PM") ? "AM" : "PM";
                }
                (d.getHours() == 12) ? d.setHours("01") : d.setHours(d.getHours()+1);
            }

            (d.getHours() > 12) ? (d.setHours(d.getHours()-12)) : d.getHours();
            var currenttime = (d.getHours().toString().length < 2 ? "0"+d.getHours() : d.getHours()) + ":" + (d.getMinutes() == 0 ? "00" : d.getMinutes()) + " " + ampm;

            $('#caltime').val(currenttime);

            if ($('#cal_typ').length > 0) {
                var sel_val = $('#cal_typ').val();
                $("#todo").val(sel_val).trigger('change');
            }

        });

        $('#addcalendar.modal').on('hidden.bs.modal', function () {
            $('#attyass').removeAttr("disabled");
            $('#addcalendar h4.modal-title').text('Add Appointment/Event To Calendar');
			$('#calendarForm')[0].reset();
            $('.select2Class').trigger('change');
            var validator = setCalendarValidation();
            validator.resetForm();
            $('input[name=eventno]').val(0);
            if ($('#calendarForm :input[name="caseno"]').val() != 0) {
                $('#calendarForm :input[name="atty_hand"]').val('<?php echo $display_details->atty_resp ?>');
                $('#calendarForm :input[name="cal_atty_hand"]').val('<?php echo $display_details->atty_resp ?>');
                $('#calendarForm :input[name="attyass"]').val('<?php echo $display_details->atty_hand ?>');
            }
            $('#addcalendar a[href="#general1"]').tab('show');
            $('#addcalendar .tab-content .tab-pane').not('#general1').removeClass('active');
            $('#addcalendar .tab-content div#general1').addClass('active');
        });
        /*End Calendar Modal Hide/Show*/
    });
    $(document.body).on('click', '#clone_event_warning', function () {
        $("#cloneCaseWarning").modal("hide");
        Clonetoday = 1;
        CloneEventC(Clonetoday);
    });
    function CloneEventC(Clonetoday = 0) {
        var Ddays = $('#entry_date').val();
        var nDayas = 1;
        if (Ddays == 2) {
            nDayas = 14;
        }
        if (Ddays == 3) {
            nDayas = 30;
        }
        if (Ddays == 4) {
            nDayas = 4;
        }
        if (Ddays == 5) {
            nDayas = 24;
        }
        if (Ddays == 6) {
            nDayas = 52;
        }
        if (Ddays == 7) {
            nDayas = 6;
        }
        if (Ddays == 8) {
            nDayas = 12;
        }
        if ($('input[type="checkbox"][name="notify"]').is(":checked") == true) {
            var addnotes = 1;
        } else {
            var addnotes = 0;
        }
        if (Clonetoday == 0) {
            nDayas = nDayas - 1;
        }
        var msg = 'Are you sure you want to copy this appointment to ' + nDayas + ' appointments';
        swal({
            title: '',
            text: msg,
            showCancelButton: true,
            confirmButtonColor: "#1ab394",
            confirmButtonText: "Yes",
            closeOnConfirm: true,
            showLoaderOnConfirm: false
        }, function (isConfirm) {
            if (isConfirm) {
                swal.close();
                var url = '<?php echo base_url() ?>calendar/addCalendarCloneData';
                $.blockUI();
                $.ajax({url: url, dataType: 'json', method: 'post', data: $('#calendarForm').serialize() + "&startend1=" + $('#startend1').val() + "&attyass=" + $('#attyass_clone').val() + "&first=" + $('#first_clone').val() + "&last=" + $('#last_clone').val() + "&addNotes=" + addnotes + "&addToday=" + Clonetoday,
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        $.unblockUI();
                        if (data[0].status == '1') {
                            swal("Success !", data.message, "success");
                            $('#Addclonecase')[0].reset();
                            $('#calendarForm')[0].reset();
                            $('#addcalendar').modal('hide');
                            $('#clonecalendar').modal('hide');
                            $('#cloneCaseWarning').modal('hide');
                            $('#addcalendar .modal-title').html('Add Calendar');
					        $('#calendar').fullCalendar('refetchEvents');
                        } else {
                            swal("Oops...", data.message, "error");
                        }
                    }
                });
            }
        });
    }
	$(document.body).on('click','#CalAddEvt', function () {
		$('.calender-pullcase').css('display','none');
	});
    $(document.body).on('click', '#clone_event_warning_no', function () {
        Clonetoday = 0;
        $("#cloneCaseWarning").modal("hide");
        CloneEventC(Clonetoday);
    });
    function setCalendarValidation() {
        var validator = $('#calendarForm').validate({
            ignore: [],
            rules: {
                calstat: {letterwithspace: true},
                /*first: {
                 required: true,
                 letterswithbasicpunc: true
                 },
                 last: {
                 required: true,
                 letterswithbasicpunc: true
                 },
                defendant: {letterswithbasicpunc: true},*/
                judge: {letterswithbasicpunc: true},
                venue: {letterswithbasicpunc: true},
                caldate: {required: true},
                caltime: {required: true},
                event: {required: true},
                /*whofrom: {
                 required: function (element) {
                 if ($('input[name=tickledate]').val() != '') {
                 return true;
                 }
                 return false;
                 }, lettersonly: true, maxlength: 3
                 },
                 whoto: {
                 required: function (element) {
                 if ($('input[name=tickledate]').val() != '') {
                 return true;
                 }
                 return false;
                 }, lettersonly: true, maxlength: 3
                 },*/
                caseno: {number: true,
                    remote: {
                        url: '<?php echo base_url(); ?>prospects/CheckCaseno',
                        type: "post",
                        data: {
                            pro_caseno: function () {
                                return $('#calendarForm :input[name="caseno"]').val();
                            }
                        }
                    }
                }
            },
            messages: {
                caseno: {
                    remote: "This caseno is not exist"
                }
            },
            invalidHandler: function (form, validator) {
                var element = validator.invalidElements().get(0);
                var tab = $(element).closest('.tab-pane').attr('id');

                $('#addcalendar a[href="#' + tab + '"]').tab('show');
            }
        });
        return validator;
    }
    function getCalendarDataByEventno(eventno) {
        $.blockUI();
        $.ajax({
            url: '<?php echo base_url(); ?>calendar/getCalendarData',
            dataType: 'json',
            method: 'post',
            data: {
                eventno: eventno
            },
            success: function (res) {
                var data = res;

				$('#addcalendar .modal-title').html('Edit Calendar');
				if(data.caseno != '0' && data.caseno != '')
				{
					$('#pullcaseevent').html('<button class="btn btn-md btn-primary calender-pullcase" data-caseno="'+ data.caseno +'">Pull Case</button>');		
                }else{ $('#pullcaseevent').html(''); }
				$('#lastDate').prop("disabled", false);
                $("#lastDate").css("background-color", "");
                $('input[name=eventno]').val(data.eventno);
                $('input[name=calstat]').val(data.calstat);
                if ($("#atty_hand").hasClass("select2Class"))
                {
                    $('select[name=atty_hand]').val(data.atty_hand);
                } else if ($("#atty_hand").hasClass("cstmdrp"))
                {
                    $('#atty_hand').val(data.atty_hand);
                }
                var addmin_attyass = '<?php echo isset($system_caldata[0]->calattyass) ? $system_caldata[0]->calattyass : '' ?>';

                if (addmin_attyass == '1') {
                    $('#attyass').removeAttr("disabled");
                    data.attyass = (data.attyass == 'NA') ? '' : data.attyass;
                    if ($("#attyass").hasClass("select2Class"))
                    {
                        $('select[name=attyass]').val(data.attyass);
                    } else if ($("#attyass").hasClass("cstmdrp"))
                    {
                        $('#attyass').val(data.attyass);
                    }
                } else {
					if ($("#attyass").hasClass("select2Class"))
					{
						$('select[name=attyass]').val(data.attyass);
					} else if ($("#attyass").hasClass("cstmdrp"))
					{
						$('#attyass').val(data.attyass);
					}
                    if (addmin_attyass == '3') {
                        $('#attyass').attr("disabled", true);
                    } else if(addmin_attyass == '2' && data.eventno != '0') {
                        $('#attyass').attr("disabled", true);
                    } else {
                        $('#attyass').removeAttr("disabled");
                    }
                    
                }
                $('#judge').val(data.judge);
                $('input[name=first]').val(data.first);
                $('input[name=last]').val(data.last);
                $('input[name=defendant]').val(data.defendant);
                $('#venue').val(data.venue);
                $('input[name=location]').val(data.location);
                $('input[name=caldate]').val(data.caldate);
                var clocktime = moment(data.caltime, 'hh:mm A');
                var eveTime = clocktime.format('hh:mm A');
                $('#caltime').val(eveTime);
                /*$('#caltime').mdtimepicker({
                    'default': eveTime
                });*/
                $('input[name=totime]').val(data.tottime);
                $('#event').val(data.event);
                $('textarea[name=notes]').val(data.notes);
                $('select[name=tickle]').val(data.tickle);
                $('input[name=tickledate]').val(data.tickdate);
                $('input[name=tickletime]').val(data.ticktime);
                $('select[name=whofrom]').val(data.whofrom);
                $('select[name=whoto]').val(data.whoto);
                if (data.caseno != 0) {
                    $('input[name=caseno]').val(data.caseno);
                } else {
                    $('input[name=caseno]').val('');
                }
                $('input[name=initials0]').val(data.initials0);
                $('input[name=initials]').val(data.initials);
                $('input[name=chgwho]').val(data.chgwho);
                $('input[name=chgdate]').val(data.chgdate);
                $('input[name=chgtime]').val(data.chgtime);
                $('input[name=doctor]').val(data.doctor);
                (data.protected == '1') ? $('input[name=protected]').iCheck('check') : $('input[name=protected]').iCheck('uncheck');
                (data.rollover == '1') ? $('input[name=rollover]').iCheck('check') : $('input[name=rollover]').iCheck('uncheck');
                if(data.reminder == '1')
                    $('input[type="checkbox"][name="reminder"]').prop("checked", true).change();
                $('select[name=todo]').val(data.todo);
                $('select[name=color]').val(data.color);
                $('select[name=tevent]').val(data.tevent);
                $("#addcalendar select").trigger('change');
                $.unblockUI();
            }
        });
    }

    function category_list(category, caseno) {
        var status = '';
        if ($('#activity_list').prop("checked") == true) {
            status = 1;
        } else {
            status = 0;
        }
        $.ajax({
            url: '<?php echo base_url(); ?>cases/getcategoryeventlist',
            method: 'POST',
            data: {category_list: category, case_id: caseno, status: status},
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (data) {
                categoryeventlist.clear();
                var obj = $.parseJSON(data);
                $.each(obj, function (index, item) {
                    var html = '';
                    html += '<tr class="categorylistevent" data-event="' + (item.event) + '" data-category=' + item.category + '><td>' + item.event + '</td>';
                    html += '</tr>';
                    categoryeventlist.row.add($(html));
                });
                categoryeventlist.draw();
            }
        });
    }

    function caseDetails(cardCode) {
        $.ajax({
            url: '<?php echo base_url() . 'cases/getCasrcardDetails' ?>',
            method: "POST",
            data: {
                cardCode: cardCode
            },
            success: function (result) {
                $('#viewAllCases').html(result);
                $("#caseCardDetails").modal('show');
            }
        });
    }

    $('#clonecalender').on('click', function () {
        var Ddays = $('#entry_date').val();
        if (Ddays > 1) {
            $("#cloneCaseWarning").modal("show");
        } else {
            Clonetoday = 1;
            CloneEventC(Clonetoday);
        }
    });

    function createCalendarRow1(data) {
        var html = '';
        var arr = [];
        var json_color = jQuery.parseJSON('<?php echo color_codes ?>');
        if (json_color.hasOwnProperty(data.color)) {
            style = "background-color:" + json_color[data.color][2] + ";color:" + json_color[data.color][1];
        } else {
            style = "background-color:#FFFFFF;color: #000000";
        }
        for (var i = 0; i < data.length; i++) {
            html = "<tr class='gradeX' id='calEvent_" + data.eventno + "' style='" + style + "'>";
            html += "<td>" + moment(data[i].calendarData.date).format('DD/MM/YYYY HH:mm') + "</td>";
            html += "<td>" + data[i].calendarData.atty_hand + "</td>";
            html += "<td>" + data[i].calendarData.attyass + "</td>";
            html += "<td>" + data[i].calendarData.first + ' ' + data[i].calendarData.last + ' vs ' + data[i].calendarData.defendant + "</td>";
            html += "<td>" + data[i].calendarData.event + "</td>";
            html += "<td>" + data[i].calendarData.venue + "</td>";
            html += "<td>" + data[i].calendarData.judge + "</td>";
            html += "<td>" + data[i].calendarData.notes + "</td>";
            html += "<td>";
            html += "<a class='btn btn-xs btn-info mar-right4 event_edit' data-eventno=" + data.eventno + " href = 'javascript:;' title='Edit'><i class = 'icon fa fa-edit'></i> Edit</a>";
            html += "<a class='btn btn-xs btn-danger event_delete' data-eventno=" + data.eventno + " href = 'javascript:;' title='Delete'><i class = 'icon fa fa-times'></i> Delete</a>";
            html += "</td>";
            html += "</tr>";
            arr.push(html);
        }
        for (var i = 0; i < arr.length; i++) {
            caseDetailCalendarTable.row.add($(arr[i])).draw();
        }
    }
    var regExp = /[a-z]/i;

    function validate(e) {
        var value = String.fromCharCode(e.which) || e.key;
        if (regExp.test(value)) {
            e.preventDefault();
            return false;
        }
    }

    function remove_error(id) {
        var myElem = document.getElementById(id + '-error');
        if (myElem === null)
        {

        } else
        {
            document.getElementById(id + '-error').innerHTML = '';
        }
    }

    $(document.body).on('click', '.calnote', function () {
        $('textarea[name=notes]').val('<?php echo isset($system_caldata[0]->calnotes) ? $system_caldata[0]->calnotes : ''; ?>');

    });

    $(document.body).on('click', '.calendar_clone', function () {
        /*var atty_hand = $('select[name=atty_hand]').val();
         var attyass = $('select[name=attyass]').val();*/
        var atty_hand = $('#atty_hand').val();
        var attyass = $('#attyass').val();
        var name = $('#first').val() + ' ' + $('#last').val();
        var event = $('#event').val();
        $('#atty_hand_clone').val(atty_hand);
        $('#attyass_clone').val(attyass);
        $('#event_clone').val(event);
        $('#firstname_clone').val(name);
        $('#first_clone').val($('#first').val());
        $('#last_clone').val($('#last').val());
        var dt = $('input[name=caldate]').val();
        $('#startend1').val($('input[name=caldate]').val());
        var html = '';
        html += '<div class="form-group" id="cloneDate"><label>Date</label>';
        html += '<select class="form-control" name="entry_date" id="entry_date">';
        html += '<option value="1" selected>' + dt + '</option>';
        html += '<option value="2">' + dt + ' 14 Days (daily)</option>';
        html += '<option value="3">' + dt + ' 30 Days (daily)</option>';
        html += '<option value="4">' + dt + ' 4 Weeks (weekly)</option>';
        html += '<option value="5">' + dt + ' 24 Weeks(weekly)</option>';
        html += '<option value="6">' + dt + ' 52 Weeks(weekly)</option>';
        html += '<option value="7">' + dt + ' 6 Months(monthly)</option>';
        html += '<option value="8">' + dt + ' 12 Months(monthly)</option>';
        html += '</select></div>';
        $("#cloneDate").replaceWith(html);

        $('#Date_copy').text(moment($('#caldate').val()).format('M/D/YYYY ,dddd'));
        if ($('#calendarForm').valid()) {
            $('#clonecalendar').modal('show');
        }
    });

    $(document.body).on('change', '#entry_date', function () {
        var array = [];
        var array1 = [];
        var option = $('#entry_date').val();
        if (option == 2) {
            for (var i = 0; i < 14; i++) {
                var d = moment($('#caldate').val()).add(i, 'days');
                var d1 = d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        } else if (option == 3) {
            for (var i = 0; i < 30; i++) {
                var d = moment($('#caldate').val()).add(i, 'days');
                var d1 = d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        } else if (option == 4) {
            for (var i = 0; i < 4; i++) {
                var d = moment($('#caldate').val()).add(i, 'weeks');
                var d1 = d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        } else if (option == 5) {
            for (var i = 0; i < 24; i++) {
                var d = moment($('#caldate').val()).add(i, 'weeks');
                var d1 = d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        } else if (option == 6) {
            for (var i = 0; i < 52; i++) {
                var d = moment($('#caldate').val()).add(i, 'weeks');
                var d1 = d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        } else if (option == 7) {
            for (var i = 0; i < 6; i++) {
                var d = moment($('#caldate').val()).add(i, 'month');
                var d1 = d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        } else if (option == 8) {
            for (var i = 0; i < 12; i++) {
                var d = moment($('#caldate').val()).add(i, 'month');
                var d1 = d._d;
                array.push(moment(d1).format('M/D/YYYY ,dddd'));
                array1.push(moment(d1).format('YYYY-M-D'));
            }
        } else
        {
            array.push(moment($('#caldate').val()).format('M/D/YYYY , dddd'));
            array1.push(moment(d1).format('YYYY-M-D'));
        }
        $('#Date_copy').text('');
        $('#startend1').val(array1);
        for (var i = 0; i < array.length; i++) {
            $('#Date_copy').append(array[i] + '\n');
        }
    });

    $(document.body).on('change click', '.weekendsalert', function () {
        var weekalert = "<?php echo isset($holidayset->weekends) ? $holidayset->weekends : 0 ?>";
        if (weekalert != 0 && weekalert == 'on') {
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            var a = new Date($(this).val());
            if (weekday[a.getDay()] == 'Saturday' || weekday[a.getDay()] == 'Sunday')
            {
                swal({
                    title: "",
                    text: "This day is " + weekday[a.getDay()],
                    customClass: 'swal-wide',
                    confirmButtonColor: "#1ab394",
                    confirmButtonText: "OK",
                });
            }
        }
    });

    $(document.body).on('change click', '.alerttonholiday', function () {
        var aletholi = $(this).val();
        var aftholiday = "<?php echo isset($holidayset->aftholiday) ? $holidayset->aftholiday : 0 ?>";
        if (aftholiday != 0 && aftholiday == 'on' && $(this).val() != '') {
            var holidaylist = '<?php echo isset($holodaylist) ? json_encode($holodaylist) : 0; ?>';
            if (JSON.parse(holidaylist).length > 0) {
                $.each(JSON.parse(holidaylist), function (key, value) {
                    var temp = new Array();
                    temp = value.split(",");
                    var c = new Date(temp[0]);
                    var b = new Date(aletholi);
                    if (c.getTime() == b.getTime()) {

                        swal({
                            title: "",
                            text: "This day is " + temp[1],
                            customClass: 'swal-wide',
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "OK"
                        });
                    }

                });
            }
        }

    });

    $(document.body).on('change click', '.birthcalalert', function () {
        var bialaddcalevent = "<?php echo isset($main1->bialaddcalevent) ? $main1->bialaddcalevent : 0 ?>";
        if (bialaddcalevent != 0 && bialaddcalevent == 'on') {
            var birthdate = "<?php echo isset($display_details->birth_date) ? date('m/d/Y', strtotime($display_details->birth_date)) : 0 ?>";
            if (birthdate != '' && birthdate != 0)
            {
                if ($(this).val() != '') {
                    var b = new Date(birthdate);
                    var y = new Date();
                    var year = y.getFullYear();
                    var month = b.getMonth();
                    var day = b.getDate();
                    var c = new Date(year, month, day);
                    var b = new Date($(this).val());
                    if (b.getTime() === c.getTime())
                    {
                        swal({
                            title: "Notice",
                            text: "You are scheduling this appointment on the client's birthday",
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "OK"
                        });
                    }

                }
            }

        }

    });

    $(document.body).on('change click', '.staffvacation', function () {
        var Uservacation = '<?php echo isset($Uservacation) ? json_encode($Uservacation) : 0; ?>';

        if ($('.staffTO').val() != "" && JSON.parse(Uservacation).length > 0 && $('.staffTO').val() == '<?php echo $this->session->userdata('user_data')['username'] ?>' && $('.sfinishby').val() != '') {
            $.each(JSON.parse(Uservacation), function (key, value) {
                var temp = new Array();
                temp = value.split(",");
                if (temp[0].includes("-") == true) {
                    var result = temp[0].split('-');
                    var dateFrom = result[0];
                    var dateTo = result[1];
                    var dateCheck = $('.sfinishby').val();
                    var from = Date.parse(dateFrom);
                    var to = Date.parse(dateTo);
                    var check = Date.parse(dateCheck);
                    if ((check <= to && check >= from))
                        swal({
                            title: "",
                            text: temp[1],
                            customClass: 'swal-wide',
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "OK",
                            timer: 2000
                        });


                } else {
                    var c = new Date(temp[0]);
                    var b = new Date($('.sfinishby').val());
                    if (c.getTime() == b.getTime()) {
                        swal({
                            title: "",
                            text: temp[1],
                            customClass: 'swal-wide',
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "OK"
                        });
                    }
                }
            });

        }


    });

    $(document.body).on('click', '.clear_form', function () {
        $('#calendarForm').find('input,textarea,select').each(function () {
            this.value = '';
            $('#calendarForm :input[name="caseno"]').val('<?php echo $case_no; ?>');
            $(this).iCheck('uncheck');
            $(this).trigger('change');
        });
    });

    $(document.body).on('click', '#ticklerClear', function () {
        $("#whofrom").select2("val", "");
        $('#tickledate').val('');
        $("#whoto").select2("val", "");
        $('#tickletime').val('');
    });
</script>



