<?php include 'js/documentsjs.php'; ?>
<style>
    div#dropzonePreview {
        left: 0px;
    }
    #add_new_case_activity .tabs-container .panel-body {
        padding: 5px;
    }

    #formletters20 .modal-dialog,#formletters22 .modal-dialog{
        margin-top:60px;
    }

    #formletters26 .modal-dialog,#formletters1 .modal-dialog,#formletters25 .modal-dialog{
        margin-top:140px;
    }

    #formletters27 .modal-dialog,#formletters7 .modal-dialog ,#formletters30 .modal-dialog, #formletters14 .modal-dialog, #formletters32 .modal-dialog , #formletters10 .modal-dialog , #formletters128 .modal-dialog , #formletters129 .modal-dialog , #formletters130 .modal-dialog , #formletters131 .modal-dialog {
        margin-top:140px;
    }
    .table-responsive th .container-checkbox .checkmark {
        right: 0;
        margin: 0 auto;
        top: -7px;
    }
    #injuryTable td{overflow: visible;}
    .uk-dropdown {
        width: 90%;
    }

    #add_case_cal {
        position: relative !important;
    }
</style>
<!-- Form Modal Start-->
<div id="form_modal" class="modal fade" role="dialog">
    <input type="hidden" id="depo_date" value="">
    <input type="hidden" id="depo_time" value="0">
    <input type="hidden" id="fee_amt" value="">
    <input type="hidden" id="enum" value="">
    <input type="hidden" id="doc_name" value="">
    <input type="hidden" id="doc_number" value="0">
    <input type="hidden" id="defen_att" value="">
    <input type="hidden" id="cc_party" value="">
    <input type="hidden" id="pl_title" value="">
    <input type="hidden" id="my_pl_title" value="">
    <input type="hidden" id="date_App" value="">
    <input type="hidden" id="date_Tri" value="">
    <input type="hidden" id="time_Tri" value="">
    <input type="hidden" id="board_City" value="">
    <input type="hidden" id="dr_splt" value="">
    <input type="hidden" id="e_type" value="">
    <input type="hidden" id="e_day" value="">
    <input type="hidden" id="e_date" value="">
    <input type="hidden" id="e_time" value="">
    <input type="hidden" id="judgeNM" value="">
    <input type="hidden" id="Client_suffix" value="<?php echo $display_details->suffix; ?>">
    <input type="hidden" id="my_middle" value="<?php echo $display_details->middle; ?>">
    <input type="hidden" id="my_email" value="<?php echo $display_details->email; ?>">
    <input type="hidden" id="licenceNo" value="<?php echo $display_details->licenseno; ?>">
    <input type="hidden" id="Client_fname" value="<?php echo $display_details->first; ?>">
    <input type="hidden" id="Client_lname" value="<?php echo $display_details->last; ?>">
    <input type="hidden" id="dunname" value="">
    <input type="hidden" id="selected_injury" value="">
    <input type="hidden" id="default_party" value="<?php echo $case_cards[0]->cardcode; ?>">
    <input type="hidden" id="selected_ins_party" value="">
    <input type="hidden" id="selected_defatt_party" value="">
    <input type="hidden" id="selected_phy_party" value="">
    <input type="hidden" id="selected_emp_party" value="">
    <input type="hidden" id="total_mileage" value="">
    <input type="hidden" id="total_mileage_amount" value="">
    <input type="hidden" id="travel_time" value="">
    <input type="hidden" id="depo_prep_time" value="">
    <input type="hidden" id="total_time" value="">
    <input type="hidden" id="total_request" value="">
    <input type="hidden" id="daily_wage" value="">
    <input type="hidden" id="depo_time_hours" value="">
    <input type="hidden" id="depo_time_minutes" value="">
    <input type="hidden" id="attorney_fees" value="">
    <input type="hidden" id="trip_mileage" value="">
    <input type="hidden" id="dr_speciality" value="">
    <input type="hidden" id="body_partexamined" value="">
    <div class="modal-dialog modal-lg" data-backdrop="static" data-keyboard="false">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forms</h4>
            </div>
<!--            <form method="post" name="Formlist" id="Formlist">
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#print-letters">Print Letters</a></li>
                                            <li class=""><a data-toggle="tab" href="#edit-letters">Edit Letters</a></li>
                                            <li class=""><a data-toggle="tab" href="#saved-documents">Saved Documents</a></li>
                                            <li class=""><a data-toggle="tab" href="#a1court-forms">Court Forms</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="print-letters" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Print Letters</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="table-responsive">
                                                                <table id="form-letter" class="table table-striped table-bordered dt-responsive nowrap dataTables-example" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="10%">Number</th>
                                                                            <th width="10%">Group</th>
                                                                            <th width="75%" >Title</th>
                                                                            <th width="15%">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach ($print_letters as $key => $val) { ?>
                                                                            <tr>
                                                                                <td><?php echo $val['number']; ?></td>
                                                                                <td><?php echo $val['group']; ?></td>
                                                                                <td><?php echo $val['title']; ?></td>
                                                                                <td><?php if ($val['modalType'] != '') { ?>
                                                                                        <a href="#" class="viewDocForm" data-toggle="modal" id="<?php echo $val['filename']; ?>" data-target="#<?php echo $val['modalType']; ?>">View</a>
                                                                                    <?php } else { ?>
                                                                                        <a href="#" class="createDoc" id="<?php echo $val['filename']; ?>" data-cardcode="<?php echo $display_details->cardcode; ?>">View</a>
                                                                                    <?php } ?>

                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="edit-letters" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Edit Letters</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="table-responsive">
                                                                <table id="form-letter1" class="table table-striped table-bordered dt-responsive nowrap dataTables-example" cellspacing="0" width="100% !important;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="10%">Number</th>
                                                                            <th width="10%">Group</th>
                                                                            <th width="75%" >Title</th>
                                                                            <th width="15%">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach ($print_letters as $key => $val) { ?>
                                                                            <tr>
                                                                                <td><?php echo $val['number']; ?></td>
                                                                                <td><?php echo $val['group']; ?></td>
                                                                                <td><?php echo $val['title']; ?></td>
                                                                                <td><a href="#" class="viewDocForm" data-toggle="modal" id="<?php echo $val['filename']; ?>" data-target="#<?php echo $val['modalType']; ?>">View</a></td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="saved-documents" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Saved Documents</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="a1court-forms" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Court Forms</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <select class="form-control" name="isCC">
                                                                            <option value="y">Y</option>
                                                                            <option value="n">N</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <select class="form-control" name="isCC">
                                                                            <option value="y">Y</option>
                                                                            <option value="n">N</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>-->
        </div>
    </div>
</div>
<!-- Form Modal End -->

<div id="formletters1" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Date of Depo</label>
                        <input type="text" class="form-control injurydate1" id="dateOfDep"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time of Depo</label>
                        <input type="text" class="form-control timepckr" id="timeOfDep" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form1-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters136" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Defense Attorney Or Insurance Company To Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select31" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select32" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-form136-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters2" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ccParties Prompt</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Are there parties to CC on this letter?</label>
                        <select class="form-control" name="isCC" id="allowCC">
                            <option value="y">Y</option>
                            <option value="n">N</option>
                        </select>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form2-process">Proceed</a>
                        <button type="button" class="btn btn-md btn-primary btn-danger btn-form2-cancel " data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters3" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title formletters3-Title" id="modal_title">Select Party to Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select1" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select2" class="whatever"></select>


                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form-process">Proceed</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters127" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title formletters127-Title" id="modal_title">Select Party to Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select23" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select24" class="whatever"></select>


                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="submit" class="btn btn-md btn-primary btn-form127-process"  data-toggle="modal" data-target="#formletters128" data-dismiss="modal">Proceed</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters4" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ccParties Prompt</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Are there parties to CC to this letter?</label>
                        <select class="form-control" name="isCC" id="ISCC">
                            <option value="y">Y</option>
                            <option value="n">N</option>
                        </select>
                    </div>
                    <div class="form-group" id="divjudge">
                        <input type="text" class="form-control" placeholder="judge" name="judge" id="judge" value="">
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form2-process">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters5" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select A Dr. To Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select7" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select8" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form5-process" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters6" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select (other) Party To Deliver Records To Or Click Cancel</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select3" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select4" class="whatever"></select>


                    </div>

                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form6-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters7" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Date of Depo</label>
                        <input type="text" class="form-control injurydate" id="dateOfDep1"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time of Depo</label>
                        <input type="text" class="form-control timepckr" id="timeOfDep1" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group">
                        <label>Amount of Witness Fee Included $</label>
                        <input type="text" class="form-control" id="amountOfFee"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form7-process" href="#" data-toggle="modal" data-target="#formletters8" data-dismiss="modal" >Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters8" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Party to CC</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select5" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select6" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form8-process">Proceed</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters9" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>USE CAPS Enter TITLE of Document(s) Being Served</label>
                        <input type="text" class="form-control" id="pleadingTitle"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form9-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters9_for411" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>USE CAPS Enter TITLE of Pleading</label>
                        <input type="text" class="form-control" id="my_pleadingTitle"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form9-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters10" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Choose Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Date of Appearance</label>
                        <input type="text" class="form-control injurydate" id="dateOfApp"/>
                    </div>
                    <div class="form-group">
                        <label>Date of Trial</label>
                        <input type="text" class="form-control injurydate" id="dateOfTri"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time of Trial</label>
                        <input type="text" class="form-control timepckr" id="timeOfTri" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group">
                        <label>Board City</label>
                        <input type="text" class="form-control" id="boardCity"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form10-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters11" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Defense Attorney Or Insurance Company To Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select9" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select10" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-form11-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters12" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select A Dr. Being Deposed</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select11" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select12" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15"><!--href="#" data-toggle="modal" data-target="#formletters13" data-dismiss="modal"-->
                        <a class="btn btn-md btn-primary btn-form12-process" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters13" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Date of Depo</label>
                        <input type="text" class="form-control injurydate" id="dateOfDep2"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time of Depo</label>
                        <input type="text" class="form-control timepckr" id="timeOfDep2" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group">
                        <label>Amount of Witness Fee Included $</label>
                        <input type="text" class="form-control" id="amountOfFee2"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form13-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters14" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Date of Fee Sent to Dr.</label>
                        <input type="text" class="form-control injurydate1" id="dateOffee"/>
                    </div>
                    <div class="form-group">
                        <label>Amount of Witness Fee Sent to Dr $</label>
                        <input type="text" class="form-control" id="amountOfWFee"/>
                    </div>
                    <div class="form-group">
                        <label>Full Name Deposed AME</label>
                        <input type="text" class="form-control" id="AMEName"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form14-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters15" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>AME is Dr._________</label>
                        <input type="text" class="form-control" id="drAME"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form15-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters16" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Enter Venue for Filing</label>
                        <input type="text" class="form-control" id="fval"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form16-process" href="#" data-toggle="modal" data-target="#formletters31" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters17" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select The Ame Dr. For this Appointment</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select13" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select14" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form17-process" href="#" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters18" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Dr.'s Specialty(Psychiatry,Orthopaedic, etc ) </label>
                        <input type="text" class="form-control" id="drspeciality"/>
                    </div>
                    <div class="form-group" id="etypediv">
                        <label>Exam Type(QME,4600/Treating Dr., ETC.)</label>
                        <input type="text" class="form-control" id="eType"/>
                    </div>
                    <div class="form-group">
                        <label>Day</label>
                        <input type="text" class="form-control" id="eday"/>
                    </div>
                    <div class="form-group">
                        <label>Date </label>
                        <input type="text" class="form-control injurydate" id="edate"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time </label>
                        <input type="text" class="form-control timepckr" id="etime" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group text-center marg-top15">

                        <a class="btn btn-md btn-primary btn-form18-process" href="#" data-toggle="modal" data-target="#formletters8" data-dismiss="modal">Proceed</a>

                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters19" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Party to Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select15" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select16" class="whatever"></select>
                        <?php foreach ($case_cards as $key => $details) { ?>
                            <input type="hidden" id="<?php echo $details->cardcode; ?>" value="<?php
                            if ($details->firm != '') {
                                echo $details->firm;
                            } else {
                                if ($details->suffix != '') {
                                    echo $details->first . ' ' . $details->last . ',' . $details->suffix;
                                } else {
                                    echo $details->first . ' ' . $details->last;
                                }
                            }
                            ?>">
                               <?php } ?>

                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form19-process" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters20" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Judge</label>
                        <input type="text" class="form-control" id="hjudge"/>
                    </div>
                    <div class="form-group">
                        <label>Date</label>
                        <input type="text" class="form-control injurydate" id="dateOfDeph"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time</label>
                        <input type="text" class="form-control timepckr" id="timeOfDeph" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group">
                        <label>Hearing Type</label>
                        <input type="text" class="form-control" id="typeofh"/>
                    </div>

                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form20-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters21" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Judge</label>
                        <input type="text" class="form-control" id="hjudge1"/>
                    </div>
                    <div class="form-group">
                        <label>Original Date</label>
                        <input type="text" class="form-control injurydate1" id="dateOfDeph1"/>
                    </div>
                    <div class="form-group">
                        <label>New Date</label>
                        <input type="text" class="form-control injurydate" id="dateOfnDeph1"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>New Time</label>
                        <input type="text" class="form-control timepckr" id="timeOfDeph1" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group">
                        <label>Hearing Type</label>
                        <input type="text" class="form-control" id="typeofh1"/>
                    </div>

                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form21-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters22" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Deponent's Name </label>
                        <input type="text" class="form-control" id="dname"/>
                    </div>
                    <div class="form-group">
                        <label>Date</label>
                        <input type="text" class="form-control injurydate1" id="ddate"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time</label>
                        <input type="text" class="form-control timepckr" id="dtime" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form22-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters23" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Location For This Depo</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select17" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select18" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form23-process" href="#" data-toggle="modal" data-target="#formletters22">Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters24" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_title1">Select Defense Attorney Or Insurance Company To Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select19" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select20" class="whatever"></select>


                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form24-process">Proceed</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters25" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>MSC Date</label>
                        <input type="text" class="form-control injurydate" id="mscdate"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>MSC Time</label>
                        <input type="text" class="form-control timepckr" id="msctime" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group">
                        <label>Board City</label>
                        <input type="text" class="form-control" id="mscboard"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form25-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters26" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Deposition Date</label>
                        <input type="text" class="form-control injurydate" id="depodate"/>
                    </div>
                    <div class="form-group">
                        <label>Attorney Name</label>
                        <input type="text" class="form-control" id="attName"/>
                    </div>
                    <div class="form-group">
                        <label>Fee Advanced: $</label>
                        <input type="text" class="form-control" id="feeadv"/>
                    </div>
                    <div class="form-group">
                        <label>Location:</label>
                        <input type="text" class="form-control" id="loctn"/>
                    </div>
                    <div class="form-group">
                        <label>Depo Time:</label>
                        <input type="text" class="form-control" id="depotime"/>
                    </div>
                    <div class="form-group">
                        <label>Physician:</label>
                        <input type="text" class="form-control" id="physc"/>
                    </div>
                    <div class="form-group">
                        <label>Fee Requested: $</label>
                        <input type="text" class="form-control" id="reqfee"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form26-process" href="#" data-toggle="modal" data-target="#formletters3" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters27" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Petition Filed Dated</label>
                        <input type="text" class="form-control injurydate1" id="pedate"/>
                    </div>
                    <div class="form-group">
                        <label>Physician Witness Fee$</label>
                        <input type="text" class="form-control" id="phyfee"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form27-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters28" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select the Addressee</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select21" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select22" class="whatever"></select>


                    </div>

                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form28-process" href="#" data-toggle="modal" data-target="#formletters29" data-dismiss="modal">Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters29" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>PAGES(not Including cover): </label>
                        <input type="text" class="form-control" id="pages"/>
                    </div>
                    <div class="form-group">
                        <label>Our Reference Number: </label>
                        <input type="text" class="form-control" id="our_ref"/>
                    </div>
                    <div class="form-group">
                        <label>Their Reference Number: </label>
                        <input type="text" class="form-control" id="their_ref"/>
                    </div>
                    <div class="form-group">
                        <label>Comments: </label>
                        <textarea class="form-control" id="comment"></textarea>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form29-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters30" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Date of Depo</label>
                        <input type="text" class="form-control injurydate" id="dateOfDep3"/>
                    </div>
                    <div class="form-group uk-autocomplete">
                        <label>Time of Depo</label>
                        <input type="text" class="form-control timepckr" id="timeOfDep3" data-uk-timepicker="{format:'12h'}"/>
                    </div>
                    <div class="form-group">
                        <label>Amount of Witness Fee Included $</label>
                        <input type="text" class="form-control" id="amountOfFee1"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form30-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters31" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Injuries</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <form id="injlistfrm">
                        <table id="injuryTable" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="container-checkbox">
                                            <input name="select_all" value="1" id="someCheckbox-all" type="checkbox" />
                                            <span class="checkmark"></span>
                                        </label>
                                    </th>
                                    <th>No</th>
                                    <th>Date of Injury</th>
                                    <th>Claim no</th>
                                    <th>Case no</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($injury_details as $key => $data) {
                                    if (strtolower($data->app_status) != 'closed') {
                                        if ($data->eamsno) {
                                            $eamsno = $data->eamsno;
                                        } elseif ($data->case_no) {
                                            $eamsno = $data->case_no;
                                        } else {
                                            $eamsno = '-';
                                        }
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <label class="container-checkbox">
                                                    <input type="checkbox" class="singlecheck" id="relatedinj" name="relatedinj" value="<?php echo $data->injury_id; ?>">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </td>
                                            <td class="text-center"><?php echo $i; ?></td>
                                            <td class="text-left"><?php echo $data->adj1c; ?></td>
                                            <td class="text-center"><?php echo $data->i_claimno; ?></td>
                                            <td class="text-center"><?php echo $eamsno; ?></td>
                                            <td class="text-center"><?php echo $data->app_status; ?></td>
                                        </tr>

                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </form>
                    <div class="form-group text-center marg-top15"><!-- id="createDoc12" -->
                        <button type="submit"  class="btn btn-md btn-primary multipleInjuryProcess btn-form31-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="formletters134" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title formletters134-Title" id="modal_title">Select A Dr To Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select29" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select30" class="whatever"></select>


                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="submit" class="btn btn-md btn-primary btn-form134-process"  data-toggle="modal" data-target="#formletters135" data-dismiss="modal">Proceed</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters135" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Enter the Dr. Speciality : Orthopedics,Psyschiatry,Etc</label>
                        <input type="text" class="form-control " id="dr_speciality_f"/>
                    </div>
                    <div class="form-group">
                        <label>Enter Body Parts to be Examined</label>
                        <input type="text" class="form-control" id="body_partexamined_f"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form135-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="formletters130" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Petition Filed Dated</label>
                        <input type="text" class="form-control injurydate1" id="pedate2"/>
                    </div>
                    <div class="form-group">
                        <label>Reasonable Attorney's Fees $</label>
                        <input type="text" class="form-control" id="attorneyfees2"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form130-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters129" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Petition Filed Dated</label>
                        <input type="text" class="form-control injurydate1" id="pedate1"/>
                    </div>
                    <div class="form-group">
                        <label>Reasonable Attorney's Fees $</label>
                        <input type="text" class="form-control" id="attorneyfees1"/>
                    </div>
                    <div class="form-group">
                        <label>Applicant's Daily Wage $</label>
                        <input type="text" class="form-control" id="dailywage"/>
                    </div>
                    <div class="form-group">
                        <label>Total Round Trip Mileage</label>
                        <input type="text" class="form-control" id="tripmileage1"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form129-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters131" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Deposition Date</label>
                        <input type="text" class="form-control injurydate" id="depodate3"/>
                    </div>
                    <div class="form-group">
                        <label>Attorney Name</label>
                        <input type="text" class="form-control" id="attName1"/>
                    </div>
                    <div class="form-group">
                        <label>Travel Time:</label>
                        <input type="text" class="form-control" id="traveltime"/>
                    </div>
                    <div class="form-group">
                        <label>Depo Prep Time:</label>
                        <input type="text" class="form-control" id="depopreptime"/>
                    </div>
                    <div class="form-group">
                        <label>Depo Time:</label>
                        <input type="text" class="form-control" id="depotime1"/>
                    </div>
                    <div class="form-group">
                        <label>Total Time:</label>
                        <input type="text" class="form-control" id="totaltime"/>
                    </div>
                    <div class="form-group">
                        <label>Total Requested: $</label>
                        <input type="text" class="form-control" id="totalrequest"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form131-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters128" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Deposition Date</label>
                        <input type="text" class="form-control injurydate" id="depodate1"/>
                    </div>
                    <div class="form-group">
                        <label>Depo & Prep Time - Total Hours</label>
                        <input type="text" class="form-control" id="depotimehours"/>
                    </div>
                    <div class="form-group">
                        <label>Depo & Prep Time - Total Miniutes</label>
                        <input type="text" class="form-control" id="depotimeminutes"/>
                    </div>
                    <div class="form-group">
                        <label>Total Attorney's Fees Requested $</label>
                        <input type="text" class="form-control" id="attorneyfees"/>
                    </div>
                    <div class="form-group">
                        <label>Applicant's Round - Trip Mileage</label>
                        <input type="text" class="form-control" id="tripmileage"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-form128-process" id="createDoc" href="#" data-toggle="modal" data-dismiss="modal">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="envelope" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title formletters3-Title" id="modal_title">Select Party to Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select27" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select28" class="whatever"></select>
                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createEnvelope" class="btn btn-md btn-primary btn-form-process">Proceed</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="formletters132" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title formletters132-Title" id="modal_title">Select Defence Attorney Or Insurance Company To Address</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select25" class="whatever">

                            <?php
                            foreach ($case_cards as $key => $details) {
                                $name = "";
                                $cardno = "";
                                
                                if ($details->first != '') {
                                    $name .= $details->first;
                                }
                                if ($details->middle != '') {
                                    $name .= ' ' . $details->middle;
                                }
                                if ($details->last != '') {
                                    $name .= ' ' . $details->last;
                                }
                                if ($details->firm != '') {
                                    $name .= $details->firm;
                                }
                                if ($details->cardcode == 745) {
                                    $name = 'Straussner & Sherman ' . $details->firm;
                                }
                                ?>

                                <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                            <?php } ?>
                        </select>

                        <label>Parties to Merge</label>
                        <select multiple id="select26" class="whatever"></select>


                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="submit" class="btn btn-md btn-primary btn-form132-process"  data-toggle="modal" data-target="#formletters133" data-dismiss="modal">Proceed</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="formletters133" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Total Mileage</label>
                        <input type="text" class="form-control " id="total_mileage_f"/>
                    </div>
                    <div class="form-group">
                        <label>@.34 = $__</label>
                        <input type="text" class="form-control" id="total_mileage_amount_f"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form133-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="addcaseact"  class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class=" modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Case Activity</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="add_new_case_activity" class="dropzone" enctype="multipart/form-data" name="add_new_case_activity" method="post" action="<?php echo base_url(); ?>cases/addEditActivity" onSubmit="">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-26">General</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-28">Properties</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-29">OLE</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-26" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>General</h5>
                                            <input type="hidden" value="<?php echo $display_details->caseno; ?>" name="caseActNo" id="caseActNo">
                                            <input type="hidden" value="" name="activity_no" id="activity_no">
                                        </div>
                                        <div class="ibox-content">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control add_case_cal datepickerClass" name="add_case_cal" id="add_case_cal" value="<?php echo date('m/d/Y'); ?>" readonly/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Time</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" name="add_case_time" id="add_case_time" data-uk-timepicker="{format:'12h'}"/>
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding marg-bot5">Origin Initials</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" value="<?php echo $this->session->userdata('user_data')['initials']; ?>" name="orignal_ini" id="original_ini" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding marg-bot5">Last Initials</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" value="<?php echo $this->session->userdata('user_data')['initials']; ?>" name="last_ini" id="last_ini" class="form-control" />
                                                        </div></div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15 inline-block-100">
                                                        <!--                                                        <button type="button" class="btn btn-md btn-primary pull-right marg-left10">Save</button>-->
                                                        <button type="button" class="autorecover btn btn-md btn-primary pull-right">Auto Recover</button>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Event <span class="asterisk">*</span></label>
                                                        <textarea class="form-control" style="height: 150px;width: 100%;" name="activity_event" id="activity_event"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-27" class="tab-pane">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Checks, Fees and Costs</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 300px;">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Attorney/parralegal</label>
                                                    <select class="form-control select2Class" name="case_act_atty" id="case_act_atty">
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Time/Minutes</label>
                                                    <input class="form-control" type="text"  name="case_act_time"  id="case_act_time" onkeypress='validate(event)'/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Hourly Rate</label>
                                                    <input class="form-control" type="number" min="0"  value="0" name="case_act_hour_rate" id="case_act_hour_rate"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Costs / Check Amount</label>
                                                    <input class="form-control" type="number"  min="0" value="0.00" name="case_act_cost_amt" id="case_act_cost_amt"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <select class="form-control select2Class" name="activity_title" id="activity_title">
                                                        <?php
                                                        $jsonactivity_type = json_decode(activity_title);
                                                        foreach ($jsonactivity_type as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Type of Fee or Cost</label>
                                                    <select name="activity_fee" id="activity_fee" class="form-control select2Class">
                                                        <?php
                                                        $jsonfee_cost = json_decode(fee_cost);
                                                        foreach ($jsonfee_cost as $key => $option) {
                                                            echo '<option value=' . $key . '>' . $option . '</option>';
                                                        }
                                                        ?>
                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="activity_fee1" id="activity_fee1"  style="display: none"> </div>
                                                <div class="form-group">
                                                    <label>Fee Classification</label>
                                                </div>
                                                <div class="form-group">
                                                    <span><label><input type="radio" value="1" class="i-checks" name="activity_classification"> None </label></span>
                                                    <span><label><input type="radio" value="2" class="i-checks" name="activity_classification"> Account Payable </label></span>
                                                    <span><label><input type="radio" value="3" class="i-checks" name="activity_classification"> Account Receivable </label></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-28" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="float-e-margins marg-bot25  inline-block-100">

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Genaral Note Properties</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Category</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select id="case_category" name="case_category" class="form-control select2Class">
                                                               <?php foreach ($caseactcategory as $k => $v) { ?>
                                                                    <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Color</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select id="color_codes" name="color_codes" class="form-control select2Class">
                                                                <?php
                                                                $jsoncolor_codes = json_decode(color_codes);
                                                                foreach ($jsoncolor_codes as $key => $option) {
                                                                    echo '<option value=' . $key . '>' . $option[0] . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Security</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="security" id="security" class="form-control select2Class">
                                                                <?php
                                                                $jsonsecurity = json_decode(security);
                                                                foreach ($jsonsecurity as $key => $option) {
                                                                    echo '<option value=' . $key . '>' . $option . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="ibox-title">
                                                    <h5>Type of Note</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <span><label><input type="checkbox" name="activity_mainnotes" id="activity_mainnotes" class="i-checks" value="1"> Show In All </label></span>
                                                        <span><label><input type="checkbox" name="activity_redalert" id="activity_redalert" class="i-checks" value="1"> Red Alert </label></span>
                                                        <span><label><input type="checkbox" name="activity_upontop" id="activity_upontop" class="i-checks" value="1"> Up On Top </label></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Type of activity</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" id="activity_typeact" name="activity_typeact">
                                                                <?php
                                                                $jsonactivity_type = json_decode(type_of_activity);
                                                                foreach ($jsonactivity_type as $key => $option) {
                                                                    echo '<option value=' . $key . '>' . $option . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="dropzonePreview" class="dz-default dz-message">
                                            <span class="label label-info">Click here to attach file</span>
                                        </div>
                                        <div class="row documentAttch hidden">
                                            <div class="col-sm-12">

                                                <div class="col-sm-2"><div class="col-sm-2">Document:</div> <div class="documentPath col-sm-10"></div>Activity No: </div><div class="activityNo col-sm-10"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-29" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="ibox-title">
                                                    <h5>General Information</h5>
                                                </div>
                                                <div class="ibox-content caseact-generalinfo">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Document Type</label>
                                                        <input type="hidden" class="" id="caseact_uploaddocDefault" name="caseact_uploaddocDefault">
                                                        <div class="col-sm-7 no-padding fileContainer upload-button marg-top15">
                                                            <input type="file" class="" id="uploadBtn3">
                                                            <button type="button" id="uploadFile3" class="btn btn-sm btn-primary">Upload</button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Document Type</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="activity_doc_type" id="activity_doc_type"/>
                                                            <?php
                                                            $jsonactivity_doc_type = json_decode(activity_doc_type);
                                                            foreach ($jsonactivity_doc_type as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>
                                                            </select>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Style</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="activity_style" id="activity_style"/>
                                                            <?php
                                                            $jsonactivity_style = json_decode(activity_style);
                                                            foreach ($jsonactivity_style as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-sm-6">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Origin Initials</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" name="activity_o_init" id="activity_o_init" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Initials</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" name="activity_l_init" id="activity_l_init"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <span><label>Override <input type="checkbox" class="i-checks" name="activity_override" id="activity_override"></label></span>
                                                    </div>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Initials</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" name="activity_last_init" id="activity_last_init"/>
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control injurydate" data-mask="99/99/9999" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Time</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" id="oletime" class="form-control injurytime" />
                                                        </div></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-30" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Law Form Letter Margins</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 300px;">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Top</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_TM" id="case_act_TM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Bottom</label>
                                                    <input type="number" step="0.10" min="-1.00" value="0.00" name="case_act_BM" id="case_act_BM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Left</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_LM" id="case_act_LM" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Right</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_RM" id="case_act_RM" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Page Width</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_PW" id="case_act_PW" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Page Height</label>
                                                    <input type="number" step="0.10" min="-1.00"  value="0.00" name="case_act_PH" id="case_act_PH" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Orientation</label>
                                                    <select name="case_act_orientation" id="case_act_orientation" class="form-control select2Class">
                                                        <option value="0"></option>
                                                        <option value="1">Portrait</option>
                                                        <option value="2">Landscape</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Copies</label>
                                                    <input type="number" value="0" name="case_act_copies" id="case_act_copies" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-31" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Billing</h5>
                                        </div>
                                        <div class="ibox-content" style="min-height: 415px;">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Entry Type</label>
                                                    <select class="form-control select2Class" name="activity_entry_type" id="activity_entry_type">
                                                        <option value="">Select Entry Type</option>
                                                        <?php
                                                        $jsonactivity_entry_type = json_decode(activity_entry_type);
                                                        foreach ($jsonactivity_entry_type as $key => $option) {
                                                            echo '<option data-key=' . $option[1] . ' value=' . $key . '>' . $option[0] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group hidden">
                                                    <label>Freeze Until</label>
                                                    <span class="freezeUntil"></span>
                                                </div>
                                            </div>


                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Team no</label>
                                                    <input type="number" class="form-control" value="0" min='0'id="activity_teamno" name="activity_teamno" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Attorney/Staff</label>
                                                    <select class="form-control select2Class" id="activity_staff" name="activity_staff">
                                                        <?php
                                                        $username = isset($username) ? $username : $this->session->userdata('user_data')['username'];
                                                        if (isset($staffList)) {
                                                            foreach ($staffList as $val) {
                                                                ?>
                                                                <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $username) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Short Description</label>
                                                    <textarea class="form-control" style="width: 100%;height: 100px;" id="activity_short_desc" name="activity_short_desc"></textarea>
                                                </div>

                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Billing Cycle</label>
                                                    <input type="text" readonly name="activity_biling_cycle" id="activity_biling_cycle" value="<?php echo date('m/d/Y'); ?>" class="form-control datepickerClass" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of Last Payment <span class="dateoflastpayment"></span></label>
                                                    <a class="btn btn-sm btn-primary pull-right no-bottom-margin" href="#adddefault" data-toggle="modal">Defaults</a>
                                                </div>
                                                <div class="form-group">
                                                    <label>Event/Detailed Description</label>
                                                    <textarea class="form-control" style="width: 100%;height: 150px;" id="activity_event_desc" name="activity_event_desc"></textarea>
                                                </div>


                                                <div id="payment" class="billingtab tab1 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Payment Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 370px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Check postmark Date</label>
                                                                        <input type="text" readonly class="form-control datepickerClass" name="activity_postmark_date" id="activity_postmark_date"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Payment due Date</label>
                                                                        <input type="text" readonly class="form-control datepickerClass" name="activity_payment_due" id="activity_payment_due"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Amount Paid</label>
                                                                        <input type="number" class="form-control" step="5" value="0" name="activity_amount" id="activity_amount"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Late Fee</label>
                                                                        <input type="number" class="form-control" step="5" value="0" name="activity_latefee" id="activity_latefee"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Check no</label>
                                                                        <input type="text" class="form-control numberinput" name="activity_check_no" id="activity_check_no" />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="fee" class="billingtab tab2 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 300px; height: 100%;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Total Time(Hours)</label>
                                                                        <input type="number" value="0.1000"  min='0' class="form-control" name="activity_bi_hour" id="activity_bi_hour" step="0.1000"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Total Time(minutes)</label>
                                                                        <input type="number" value="0"  min='0' class="form-control" name="activity_bi_min" id="activity_bi_min"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Hourly Rate</label>
                                                                        <input type="number" value="0"  min='0' class="form-control" name="activity_bi_rate" id="activity_bi_rate"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Total Fee</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" min='0' step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="cost" class="billingtab tab3 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Cost Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 155px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Cost</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_cost" id="activity_bi_cost" step="5"/>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Your Check No</label>
                                                                        <input type="text" maxlength="20" class="form-control" name="activity_check_no" id="activity_check_no"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="flat-fee" class="billingtab tab4 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Flat Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Flat Fee Amount</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="late-fee" class="billingtab tab5 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Late Fee Information Screen</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Late Fee Amount</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_latefee" id="activity_bi_latefee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="credit-account" class="billingtab tab6 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins">
                                                            <div class="ibox-title">
                                                                <h5>Adjustment/Credit Account</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Credit Account</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_creditac" id="activity_bi_creditac" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="debit-account" class="billingtab tab7 hidden">
                                                    <div class="">
                                                        <div class="ibox float-e-margins" >
                                                            <div class="ibox-title">
                                                                <h5>Adjustment/Debit Account</h5>
                                                            </div>
                                                            <div class="ibox-content" style="min-height: 110px;">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Debit Account</label>
                                                                        <input type="number" value="0" class="form-control" name="activity_bi_fee" id="activity_bi_fee" step="5"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="none" class="billingtab tab8 hidden" >

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center marg-top15">
                                <button type="submit" class="btn btn-md btn-primary btn-act-submit" style="margin-bottom:0px;">Save</button>

                                <button type="button" class="btn btn-md btn-primary btn-danger btn-act" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-md btn-primary CaseActEmail">Email</button>
                                <button type="button" class="btn btn-md btn-primary CaseActPrint">Print</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="CaseEmailModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Write mail: Unattached Documents</h4>
            </div>
            <div class="modal-body">
                <div class="ibox-content marg-bot15">
                    <form class="form-horizontal" id="send_mail_form1"  name="send_mail_form1" method="post" enctype="multipart/form-data">
                        <div class="form-group"><label class="col-sm-2 control-label">Internal User:</label>
                            <div class="col-sm-10">
                                <select name="whoto1" id="whoto1" style="width:100%" multiple  placeholder="To" class="form-control select2Class">
                                    <?php
                                    if (isset($staff_list)) {
                                        foreach ($staff_list as $staff) {
                                            ?>
                                            <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">External Parties:</label>
                            <div class="col-sm-10">
                                <input type="text" name="emails1" id="emails1" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Subject :</label>
                            <div class="col-sm-10">
                                <input type="text" name="mail_subject1" id="mail_subject1" class="form-control">
                                <input type="hidden" name="filenameVal1" id="filenameVal1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group inline-block marg-right40 col-sm-3" align="center">
                                <label style="margin-left:15px !important;">Urgent</label>
<!--                                <input type="checkbox" class="js-switch" class="margin-right10" sty name="mail_urgent1" id="mail_urgent1"  />-->
                                 <input type="checkbox" name="mail_urgent1" id="mail_urgent1" value="Y" class="i-checks">
                            </div>
                            <div class="form-inline col-sm-5" align="center">
                                <label class="marg-right10">Case No:</label>
                                <input type="text" name='caseno1' id="case_no1" class="form-control" readonly value="<?php echo $display_details->caseno; ?>">
                            </div>
                            <div class="form-inline col-sm-4 pull-right">
                                <select id="case_category1" name="case_category1" class="form-control select2Class">
                                    <?php foreach ($caseactcategory as $k => $v) { ?>
                                        <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class=" form-group mail-text h-200" style="padding-right:10px; padding-left:10px;">
                            <textarea id="mailbody1"  name="mailbody1"></textarea>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group text-right">
                            <div class="col-sm-8 text-left">
                                <!-- <label class="col-sm-2 control-label">Attachment :</label> -->
                                <div id="selectedloader1"></div>
                                <div id="selectedFiles1"></div>
                                <div class="attatchmentset" style="display: inline-block !important;width: 100% !important;overflow: hidden !important;overflow-y: auto !important;">
                                    <div class="col-sm-12 att-add-file marg-top5"> <input type="file" name="afiles3[]" id="afiles3" multiple > <!-- <i class="fa fa-plus-circle marg-top5 ca-addfile" aria-hidden="true"></i> --></div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send"><i class="fa fa-reply"></i> Send</button>
                                <a href="javascript:;" class="btn btn-danger btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                            </div>
                        </div>
                        <div class="col-sm-12 supported-files"><b>Note: </b> File size upload upto 20 MB <b>Supported Files :</b> PNG, JPG, JPEG, BMP,TXT, ZIP,RAR,DOC,DOCX,PDF,XLS,XLSX,PPT,PTTX,MP4 </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="totalFileSize" id="totalFileSize" value="0">
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>

<div id="injuryselectionPopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Injuries</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <form id="injlistfrm-single">
                        <table id="injuryTable" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No</th>
                                    <th>Date of Injury</th>
                                    <th>Claim no</th>
                                    <th>Case no</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($injury_details as $key => $data) {
                                    if (strtolower($data->app_status) != 'closed') {
                                        if ($data->eamsno) {
                                            $eamsno = $data->eamsno;
                                        } elseif ($data->case_no) {
                                            $eamsno = $data->case_no;
                                        } else {
                                            $eamsno = '-';
                                        }
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <div class="i-checks">
                                                    <label> <input type="radio" class="singlleinjuryselect" id="singlleinjuryselect-<?php echo $i; ?>" value="<?php echo $data->injury_id; ?>"  name="singlleinjuryselect"> <i></i></label></div>
                                            </td>
                                            <td class="text-center"><?php echo $i; ?></td>
                                            <td class="text-left"><?php echo $data->adj1c; ?></td>
                                            <td class="text-center"><?php echo $data->i_claimno; ?></td>
                                            <td class="text-center"><?php echo $eamsno; ?></td>
                                            <td class="text-center"><?php echo $data->app_status; ?></td>
                                        </tr>

                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </form>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc1" class="btn btn-md btn-primary injuryselectionPopup-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Popup for select Defence Attorney -->
<div id="Add-DefenceAttyPopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select The Defense Attorney</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select9" class="whatever">
                            <?php
                            if (!empty($case_cards)) {
                                foreach ($case_cards as $key => $details) :
                                    $name = "";
                                    $cardno = "";
                                    if ($details->first != '') {
                                        $name .= $details->first;
                                    }
                                    if ($details->middle != '') {
                                        $name .= ' ' . $details->middle;
                                    }
                                    if ($details->last != '') {
                                        $name .= ' ' . $details->last;
                                    }
                                    if ($details->firm != '') {
                                        $name .= $details->firm;
                                    }
                                    if ($details->cardcode == 745) {
                                        $name = 'Straussner & Sherman ' . $details->firm;
                                    }
                                    ?>
                                    <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select10" class="whatever"></select>
                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-form11-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Popup for select insurance party -->
<div id="Add-InsurancePopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select The Insurance Party</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select-i1" class="whatever">
                            <?php
                            if (!empty($case_cards)) {
                                foreach ($case_cards as $key => $details) :
                                    $name = "";
                                    $cardno = "";
                                    if ($details->first != '') {
                                        $name .= $details->first;
                                    }
                                    if ($details->middle != '') {
                                        $name .= ' ' . $details->middle;
                                    }
                                    if ($details->last != '') {
                                        $name .= ' ' . $details->last;
                                    }
                                    if ($details->firm != '') {
                                        $name .= $details->firm;
                                    }
                                    if ($details->cardcode == 745) {
                                        $name = 'Straussner & Sherman ' . $details->firm;
                                    }
                                    ?>
                                    <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select-i2" class="whatever"></select>
                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-selectinsparty-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Popup for select Employer PArty -->
<div id="Add-EmployerPopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select The Employer</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select-e1" class="whatever">
                            <?php
                            if (!empty($case_cards)) {
                                foreach ($case_cards as $key => $details) :
                                    $name = "";
                                    $cardno = "";
                                    if ($details->first != '') {
                                        $name .= $details->first;
                                    }
                                    if ($details->middle != '') {
                                        $name .= ' ' . $details->middle;
                                    }
                                    if ($details->last != '') {
                                        $name .= ' ' . $details->last;
                                    }
                                    if ($details->firm != '') {
                                        $name .= $details->firm;
                                    }
                                    if ($details->cardcode == 745) {
                                        $name = 'Straussner & Sherman ' . $details->firm;
                                    }
                                    ?>
                                    <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select-e2" class="whatever"></select>
                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-selectiemployer-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Popup for select Defense Att -->
<div id="Add-DefenseAttyPopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select The Defense Attorney</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select-d1" class="whatever">
                            <?php
                            if (!empty($case_cards)) {
                                foreach ($case_cards as $key => $details) :
                                    $name = "";
                                    $cardno = "";
                                    if ($details->first != '') {
                                        $name .= $details->first;
                                    }
                                    if ($details->middle != '') {
                                        $name .= ' ' . $details->middle;
                                    }
                                    if ($details->last != '') {
                                        $name .= ' ' . $details->last;
                                    }
                                    if ($details->firm != '') {
                                        $name .= $details->firm;
                                    }
                                    if ($details->cardcode == 745) {
                                        $name = 'Straussner & Sherman ' . $details->firm;
                                    }
                                    ?>
                                    <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select-d2" class="whatever"></select>
                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-selectdefatt-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Popup for document listing-->
<div id="documentPopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select The Defense Attorney</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select id="select-d1" class="whatever">
                            <?php
                            if (!empty($case_cards)) {
                                foreach ($case_cards as $key => $details) :
                                    $name = "";
                                    $cardno = "";
                                    if ($details->first != '') {
                                        $name .= $details->first;
                                    }
                                    if ($details->middle != '') {
                                        $name .= ' ' . $details->middle;
                                    }
                                    if ($details->last != '') {
                                        $name .= ' ' . $details->last;
                                    }
                                    if ($details->firm != '') {
                                        $name .= $details->firm;
                                    }
                                    if ($details->cardcode == 745) {
                                        $name = 'Straussner & Sherman ' . $details->firm;
                                    }
                                    ?>
                                    <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select-d2" class="whatever"></select>
                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-selectdefatt-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Popup for select insurance party -->
<div id="Add-Physician" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Physician</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group locationdepo">
                        <label>Parties</label>
                        <select multiple id="select-p1" class="whatever">
                            <?php
                            if (!empty($case_cards)) {
                                foreach ($case_cards as $key => $details) :
                                    $name = "";
                                    $cardno = "";
                                    if ($details->first != '') {
                                        $name .= $details->first;
                                    }
                                    if ($details->middle != '') {
                                        $name .= ' ' . $details->middle;
                                    }
                                    if ($details->last != '') {
                                        $name .= ' ' . $details->last;
                                    }
                                    if ($details->firm != '') {
                                        $name .= $details->firm;
                                    }
                                    if ($details->cardcode == 745) {
                                        $name = 'Straussner & Sherman ' . $details->firm;
                                    }
                                    ?>
                                    <option value="<?php echo $details->cardcode; ?>"><?php echo $details->cardtype . "-" . $name; ?></option>
                                    <?php
                                endforeach;
                            }
                            ?>
                        </select>
                        <label>Parties to Merge</label>
                        <select multiple id="select-p2" class="whatever"></select>
                    </div>
                    <div class="form-group text-center marg-top15"><!--   -->
                        <a class="btn btn-md btn-primary btn-selectphys-process" href="javascript: void(0);" >Proceed</a>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="formletters32" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Date</label>
                        <input type="text" class="form-control injurydate1" id="lcra_dt"/>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form32-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="formletters33" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter All Information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Product Delivery Unit</label>
                        <select class="select2Class form-control" id="pro_unit" name="pro_unit" style="width: 100%;">
                            <option value="none"></option>
                            <?php foreach ($product_delivery_unit as $key => $val) { ?>
                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Document Type</label>
                        <select class="select2Class form-control" id="pro_doc_typ" name="pro_doc_typ" style="width: 100%;">
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Document Title</label>
                         <select class="select2Class form-control" id="doc_title" name="doc_title" style="width: 100%;">
                            
                        </select>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="submit" id="createDoc" class="btn btn-md btn-primary btn-form33-process">Proceed</button>
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
$url_segment = $this->uri->segment(2);
if ($url_segment == 'documents_New' || $url_segment == 'documents_new') {
?>
<input type="hidden" name="formmodule" id="formmodule" value="documents_New">
<?php }else if($url_segment == 'documents'){ ?>
<input type="hidden" name="formmodule" id="formmodule" value="documents">
<?php } ?>
<script>
    var myDropzone = "";
    let send_mail_form1_validate = "";
    var DOCUMENT_AUTOSAVE_ENABLED = "<?php echo DOCUMENT_AUTOSAVE_ENABLED; ?>";
    function validate_ext_email(v_ext_emails) {
        var emails = '';
        var count_e = 0;
        if (v_ext_emails.indexOf(',') !== -1) {
            emails = v_ext_emails.split(",");
            count_e = emails.length;
        }
        var pattern = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (count_e > 0) {
            for (i = 0; i < emails.length; i++) {
                if (!pattern.test(emails[i].trim())) {
                    swal({
                        title: "Alert",
                        text: "Oops! Please enter valid email ID.\n" + emails[i].trim(),
                        type: "warning"
                    });
                    return false;
                }
            }
        } else {
            if (!pattern.test(v_ext_emails.trim())) {
                swal({
                    title: "Alert",
                    text: "Oops! Please enter valid email ID.\n" + v_ext_emails.trim(),
                    type: "warning"
                });
                return false;
            }
        }
        return true;
    }
    $(function() {
        $('#afiles3').bind("change", function() {
            var fileSize = parseInt($('#totalFileSize').val());
            var formData = new FormData();
            var fileName = document.getElementById('afiles3').files;
            var fi = document.getElementById('afiles3');
            var fsize = '';
            for (var i = 0, len = document.getElementById('afiles3').files.length; i < len; i++) {
            var fsize = fi.files.item(i).size / 1024;
            fileSize = fileSize + fsize;
            
            if(fsize < 20001)
            {
                formData.append("file" + i, document.getElementById('afiles3').files[i]);
            }else
            {
                var filenameerror = fi.files.item(i).name;
                swal({
                        title: "Alert",
                        text: "Your " + filenameerror + " file size " + Math.round(fsize/1024) + " MB. Please upload file size up to 20 MB",
                        type: "warning"
                    });
                var input = $("#afiles3");
                var fileName = input.val();
                if(fileName) {
                    input.val('');
                }
            }
            
            }
            if(fileSize >= 20001) {
                swal({
                        title: "Alert",
                        text: "Total size of all files including the current file which you have selected is " + Math.round(fileSize/1024) + " MB. Please upload files which are in total of size up to 20 MB",
                        type: "warning"
                    });
                fileSize = fileSize - fsize;
                return false;
            }
            $('#totalFileSize').val(fileSize);
            $.ajax({
                url: "../../email/emailattaupload",
                type: 'post',
                data: formData,
                dataType: 'html',
                async: true,
                processData: false,
                contentType: false,
                beforeSend: function () { $('#selectedloader1').addClass('loader');},
                complete: function () {
                $('#selectedloader1').removeClass('loader');
                },
                success : function(data) {
                    var input = $('input[name="afiles3[]"]');
                    console.log(input);
                     var fileName = input.val();
                     console.log(fileName);
        
                    if(fileName) { 
                        input.val('');
                    }
                    $('#selectedFiles1').append(data);
                     
                },
                error : function(request) {
                    console.log(request.responseText);
                }
            });
        });
    });

    $(document).ready(function () {
        
        var is_iPad = navigator.userAgent.match(/iPad/i) != null;
        var changeEvent = 'change';
        if(is_iPad){
            changeEvent = "blur";
        }
        $('#add_case_cal').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            /*endDate: new Date()*/
        });

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });

        send_mail_form1_validate =$('#send_mail_form1').validate({
            rules: {
                whoto1: {required: function () {
                        if ($('#emails1').val() == "")
                            return true;
                        else
                            return false;
                    }
                }
            },
            messages: {
                whoto1: {
                    required: 'Please Select a User to send mail to'
                },
            },
            errorPlacement: function (error, element) {
                if ($(element).attr('name') == 'whoto1') {
                    $(element).parent().append(error);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                var whoto = [];
                /*whoto = $('select[name="whoto1"]').val();*/
                if($('select[name="whoto1"]').val()){
                    whoto = $('select[name="whoto1"]').val();
                }
                var subject = $('#mail_subject1').val();
                var urgent = $('input[name="mail_urgent1"]:checked').val();
                var ext_emails = $('input[name="emails1"]').val();
                var mailbody = CKEDITOR.instances.mailbody1.getData();
                var filenameVal = $("#filenameVal").val();
                var mailType = $("#mailType1").val();
                var case_no = $("#case_no1").val();
                var case_category1 = $("#case_category1").val();
                var form_data = new FormData();
                
                var ext_emails = $('input[name="emails1"]').val();
                let valid_data = true;
                if (ext_emails != '') {
                    valid_data = validate_ext_email($("input[name=emails1]").val());
                }
                if(valid_data == false) {
                    return false;
                }



                form_data.append("whoto", whoto);
                form_data.append("subject", subject);
                form_data.append("urgent", urgent);
                form_data.append("ext_emails", ext_emails);
                form_data.append("mailbody", mailbody);
                form_data.append("mailType", mailType);
                form_data.append("filenameVal", filenameVal);
                form_data.append("caseNo", case_no);
                form_data.append("caseCategory", case_category1);
                var p = 0;
                var filelength = $("input[name='afiles1[]']");
                var fileGlobal = [];
                var tmp_fileGlobal = $("input[name='files_global_var[]']").map(function(){return $(this).val();}).get();
                $.each(tmp_fileGlobal, function(k, v) {
                    var array = $.map(JSON.parse(v), function(value, index) {
                        return [ index + ':' + value];
                    });
                    fileGlobal[k] = array;
                });
                var files_arry =[];
                form_data.append("fileGlobalVariable",JSON.stringify(fileGlobal));
                $.each($('input[name="afiles1[]"]'), function (i, obj) {
                    $.each(obj.files, function (x, file) {
                        vaild_data = validateAttachFileExtension(file.name);
                        form_data.append("files_" + p, file);
                        p = p + 1;
                    });
                    files_arry.push(obj.defaultValue);
                    if(filelength.length == files_arry.length)
                    {
                        form_data.append("files" ,files_arry);
                    }
                });
                var chkmodulef = $('#formmodule').val();
                var newfUrl = '<?php echo base_url('email/send_email'); ?>';
                
                $.ajax({
                    url: newfUrl,
                    method: 'POST',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.result == "true") {
                            swal({
                                title: "Success!",
                                text: "Mail is successfully sent.",
                                type: "success"
                            });
                        } else if (data.result == "true" && data.ext_party == '0')
                        {
                            swal({
                                title: "Alert",
                                text: "Oops! Something went wrong. Email wasn't send to External Party.",
                                type: "warning"
                            });
                        } else if (data.result == "false" && data.attachment_issue != '')
                        {
                            swal({
                                title: "Alert",
                                text: "Oops! " + data.attachment_issue,
                                type: "warning"
                            });
                            return false;
                        } else {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again.",
                                type: "warning"
                            });
                        }
                        $('#CaseEmailModal').modal('hide');
                        $('.fileremove').remove();
                        $('input[name="afiles1[]"]').remove();
                        $('#totalFileSize').val('0');
                        $('input[name="files_global_var[]"]').remove();
                    }
                });
                return false;
            }
        });
        
        $('#CaseEmailModal.modal').on('hidden.bs.modal', function () {
            $(this).find('input[type="email"],textarea,select,file').each(function () {
                this.value = '';
            });
            /*$('#CaseEmailModal').find('#mail_urgent1').prop('checked', false);*/
            $('#CaseEmailModal').find('#mail_urgent1').iCheck("uncheck");
            $('#CaseEmailModal').find('.att-add-file').not(':first').remove();
            $('input[name="afiles1[]"]').remove();
            $('input[name="files_global_var[]"]').remove();
            $('.fileremove').html('');
            $('#case_category1').val('1');
            $('#whoto1').val('0').trigger('change');
            $('#mail_subject1').val('');
            $('#emails1').val('');
            $("#send_mail_form1 select").trigger('change');
            var $el = $('#afiles2');
            $el.unwrap();
            send_mail_form1_validate.resetForm();
        });

        $('.discard_message').click(function (e) {
            swal({
                title: "Are you sure?",
                text: "The changes you made will be lost",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, discard it!",
                closeOnConfirm: true
            }, function () {
                $('#CaseEmailModal').find('.att-add-file').not(':first').remove();
                $('#CaseEmailModal').modal("hide");
            });
        });

        Dropzone.options.addNewCaseActivity = {
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 11,
            maxFiles: 11,
            addRemoveLinks: true,
            uploadprogress: true,
            enqueueForUpload: false,
            paramName: 'document',
            acceptedFiles: "image/jpeg,application/docx,application/pdf,text/plain,application/msword",
            init: function () {
                myDropzone = this;
                this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                    $.blockUI();
                    e.preventDefault();
                    e.stopPropagation();
                    var form = $(this).closest('#add_new_case_activity');
                    if (form.valid() == true) {
                        if (myDropzone.getQueuedFiles().length > 0) {
                            myDropzone.processQueue();
                        } else {
                            myDropzone.uploadFiles([]);
                        }
                    } else {
                        $.unblockUI();
                    }
                });
                this.on("maxfilesreached", function () {
                    swal("Failure !", "Max file allowed exceeded", "error");
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);
                    }
                });
                this.on("addedfile", function (file) {
                    if (this.files.length) {
                        /*if($('#activity_event').val() == '') {
                            $('#activity_event').val(file.name.replace(".msg", ""));
                        }*/
                        var _i, _len;
                        for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
                            if (this.files[_i].name === file.name) {
                                this.removeFile(file);
                            }
                        }
                    }
                }),
                        this.on("success", function (file, responseText) {
                        });
                this.on("sendingmultiple", function (data, xhr, formData) {
                    if ($('#uploadBtn3').prop('files').length > 0) {
                        var caseactUploaddocData = $('#uploadBtn3').prop('files')[0];
                        formData.append("caseact_uploaddoc", caseactUploaddocData);
                    }
                });
                this.on("successmultiple", function (file, responseText) {
                    $.unblockUI();
                    var html = '';
                    var view = '';
                    $("#addcaseact").modal("hide");
                    var data = JSON.parse(responseText);
                    var link = '<?php echo base_url() ?>' + 'assets/clients/' + $('#caseNo').val() + '/' + data.file_name;
                    if (file.length == 0) {
                        if ((file.length + data.fileExist) > 1) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0)"><i class="icon fa fa-eye"></i> View All</a>';
                        } else if ((file.length + data.fileExist) > 0) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_attach" download=' + data.file_name + ' data-actid=' + data.actno + ' href="' + link + '"><i class="icon fa fa-eye"></i> View</a>';
                        } else {
                            view = '';
                        }
                    } else {
                        if (data.fileExist > 1) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + data.actno + ' href="javascript:void(0)"><i class="icon fa fa-eye"></i> View All</a>';
                        } else if (data.fileExist > 0) {
                            view = '<a class="btn btn-xs btn-primary marginClass view_attach" download=' + data.file_name + ' data-actid=' + data.actno + ' href="' + link + '"><i class="icon fa fa-eye"></i> View</a>';
                        } else {
                            view = '';
                        }
                    }
                    html += "<tr id=caseAct" + data.actno + "><td>" + data.actData.add_case_cal + "</td>";
                    html += "<td>" + data.actData.add_case_time + "</td>";
                    html += "<td>" + data.actData.activity_event + "</td>";
                    html += "<td>" + data.actData.last_ini + "</td>";
                    html += "<td>" + data.actData.orignal_ini + "</td>";
                    html += "<td>" + view + "</td>";
                    html += "<td>";
                    html += "<a class='btn btn-xs btn-info marginClass activity_edit' data-actid=" + data.actno + " href='javascript:' title='Edit'><i class='icon fa fa-edit'></i> Edit</a>";
                    html += "<a class='btn btn-xs btn-danger marginClass activity_delete' data-actid=" + data.actno + " href='javascript:' title='Delete'><i class='icon fa fa-times'></i> Delete</a>";
                    html += "</td>";
                    html += "</tr>";
                    swal({
                        title: "Success!",
                        text: "Activity List updated successfuly",
                        type: "success"
                    },
                           );

                    document.getElementById("add_new_case_activity").reset();
                });
                this.on("errormultiple", function (files, response) {
                });
                this.on("completemultiple", function (file) {
                    this.removeAllFiles(true);
                });
            }
        };

        $('#add_new_case_activity').validate({
            ignore: [],
            rules: {
                activity_event: {
                    required: true
                }
            },
            messages: {
                activity_event: {required: "Event Description is Required"}
            },
            invalidHandler: function (form, validator) {
                var element = validator.invalidElements().get(0);
                var tab = $(element).closest('.tab-pane').attr('id');
                $('#addcaseact a[href="#' + tab + '"]').tab('show');
            }
        });

        function SubmitForm() {
            if ($("#add_new_case_activity").valid()) {
                return true;
            } else {
                return false;
            }
        }

        $("#select1, #select2").on(changeEvent, function () {
            var a = $(this);
           
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select1, #select2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select3, #select4").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select3, #select4", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);

            });
        });

        $("#select5, #select6").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select5, #select6", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select7, #select8").on(changeEvent, function (){
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select7, #select8", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select9, #select10").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select9, #select10", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select11, #select12").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select11, #select12", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select13, #select14").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select13, #select14", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select15, #select16").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select15, #select16", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select17, #select18").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select17, #select18", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select19, #select20").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select19, #select20", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select21, #select22").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select21, #select22", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select23, #select24").on(changeEvent, function () {
            var a = $(this);
           
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select23, #select24", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });
        $("#select27, #select28").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select27, #select28", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select29, #select30").on(changeEvent, function () {
            var a = $(this);
           
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select29, #select30", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });
       
        $("#select25, #select26").on(changeEvent, function () {
            var a = $(this);
           
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select25, #select26", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $("#select31, #select32").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select31, #select32", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
        });

        $('#formletters127.modal').on("hidden.bs.modal", function () {
            $("#select23 option:selected").prop("selected", false);
            var a = $('#select24');
            a.siblings("select").append(a.find("option"));
            $("#select23, #select24", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select23 option:selected").prop("selected", false);
        });

        $('#formletters128.modal').on("hidden.bs.modal", function () {
            $("#depodate1").val('');
            $("#depotimehours").val('');
            $("#depotimeminutes").val('');
            $("#attorneyfees").val('');
            $("#tripmileage").val('');
        });

        $('#formletters129.modal').on("hidden.bs.modal", function () {
            $("#pedate1").val('');
            $("#attorneyfees1").val('');
            $("#dailywage").val('');
            $("#tripmileage1").val('');
        });

        $('#formletters130.modal').on("hidden.bs.modal", function () {
            $("#pedate2").val('');
            $("#attorneyfees2").val('');
        });

        $('#formletters131.modal').on("hidden.bs.modal", function () {
            $("#depodate3").val('');
            $("#attName1").val('');
            $("#traveltime").val('');
            $("#depopreptime").val('');
            $("#depotime1").val('');
            $("#totaltime").val('');
            $("#totalrequest").val('');
        });
        $('#formletters134.modal').on("hidden.bs.modal", function () {
            $("#select29 option:selected").prop("selected", false);
            var a = $('#select30');
            a.siblings("select").append(a.find("option"));
            $("#select29, #select30", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select25 option:selected").prop("selected", false);
        });

        $('#formletters132.modal').on("hidden.bs.modal", function () {
            $("#select25 option:selected").prop("selected", false);
            var a = $('#select26');
            a.siblings("select").append(a.find("option"));
            $("#select25, #select26", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select25 option:selected").prop("selected", false);
        });

        $('#formletters136.modal').on("hidden.bs.modal", function () {
            $("#select31 option:selected").prop("selected", false);
            var a = $('#select32');
            a.siblings("select").append(a.find("option"));
            $("#select31, #select32", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select31 option:selected").prop("selected", false);
        });

        
        $('#formletters133.modal').on("hidden.bs.modal", function () {
            $("#total_mileage_f").val('');
            $("#total_mileage_amount_f").val('');
        });

        $("#select27, #select28").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select27, #select28", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select25 option:selected").prop("selected", false);
        });

        $('#formletters135.modal').on("hidden.bs.modal", function () {
            $("#dr_speciality_f").val('');
            $("#body_partexamined_f").val('');
        });

        $("#formletters24").on('shown.bs.modal', function (e) {
            var doc_no = $("#doc_number").val();
            if (doc_no == 475)
            {
                $("#modal_title1").html('Select Party Served With Original');
            }
        });

        $("#formletters2").on('hide.bs.modal', function (e) {
            $("#allowCC").val('y');
        });
        
        $("#formletters4").on('shown.bs.modal', function (e) {
            $("#ISCC").val('y');
            $("#judge").val('');
        });

        $("#form-letter").DataTable({
            scrollY: 250,
            scrollX: true,
            stateSave: true,
            stateSaveParams: function (settings, data) {
                data.search.search = "";
                data.start = 0;
                delete data.order;
            }
        });

        $("#form-letter1").DataTable({
            scrollY: 300,
            scrollX: true,
            stateSave: true,
            stateSaveParams: function (settings, data) {
                data.search.search = "";
                data.start = 0;
                delete data.order;
            }
        });

        $("#formletters1.modal, #formletters7.modal,#formletters9.modal,#formletters9_for411.modal,#formletters10.modal , #formletters12.modal,#formletters13.modal,#formletters14.modal,#formletters15.modal,#formletters16.modal,#formletters18.modal,#formletters20.modal,#formletters21.modal,#formletters22.modal,#formletters25.modal,#formletters26.modal,#formletters27.modal,#formletters29.modal,#formletters30.modal").on("hidden.bs.modal", function () {
            $(this).find('input[type="text"],textarea').each(function () {
                this.value = "";
            });
        });

        $('#formletters31.modal').on("hidden.bs.modal", function () {
            $(this).find('input[type="checkbox"]').each(function () {
                $("#formletters31 .modal-body").find('input:checkbox').prop('checked', false);
            });
        });
        
        $('#formletters33.modal').on("hidden.bs.modal", function () {
            $("#pro_unit").select2('val', 'none');
            $("#pro_doc_typ").select2('val', 'none');
            $("#doc_title").select2('val', 'none');
        });
        
        $('#formletters32.modal').on("hidden.bs.modal", function () {
            $("#lcra_dt").val('');
        });

        $('#someCheckbox-all').on('click', function () {
            if ($(this).prop('checked'))
            {
                $(".singlecheck").prop('checked', true);
            } else {
                $(".singlecheck").prop('checked', false);
            }
        });
        $(document.body).on("click", ".btn-form134-process", function () {
            $("#select30 option").prop('selected', true);
            if ($("#select30").val()) {
                var values = $("#select30").val();
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
                // $("#defen_att").val(nw[nw.length - 1]);
            } else {
                $("#default_party").val('');
                // $("#defen_att").val('');
            }
            var doc_no = $("#doc_number").val();
            $('#formletters134').modal('hide');
        });

        $("#formletters3.modal").on("show.bs.modal , hide.bs.modal", function () {
            if ($("#doc_number").val() == '10006') {
                $(".formletters3-Title").html('Select Locations');
            }
            $("#select1 option:selected").prop("selected", false);
            var a = $('#select2');
            a.siblings("select").append(a.find("option"));
            $("#select1, #select2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select1 option:selected").prop("selected", false);
        });
        $("#formletters3.modal").on("hide.bs.modal", function () {
            $(".formletters3-Title").html('Select Party to Address');
        });

        $("#formletters5.modal").on("show.bs.modal", function () {
            $("#select7 option:selected").prop("selected", false);
            var a = $('#select8');
            a.siblings("select").append(a.find("option"));
            $("#select7, #select8", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select7 option:selected").prop("selected", false);
        });
        
        $("#formletters5.modal").on("show.bs.modal", function () {
            $("#select7 option:selected").prop("selected", false);
            var a = $('#select8');
            a.siblings("select").append(a.find("option"));
            $("#select7, #select8", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select7 option:selected").prop("selected", false);
        });

        $("#formletters6.modal").on("show.bs.modal", function () {
            $("#select3 option:selected").prop("selected", false);
            var a = $('#select4');
            a.siblings("select").append(a.find("option"));
            $("#select3, #select4", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select3 option:selected").prop("selected", false);
        });
        
        $("#formletters8.modal").on("show.bs.modal", function () {
            $("#select5 option:selected").prop("selected", false);
            var a = $('#select6');
            a.siblings("select").append(a.find("option"));
            $("#select5, #select6", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select5 option:selected").prop("selected", false);
        });

        $("#formletters11.modal").on("show.bs.modal", function () {
            $("#select9 option:selected").prop("selected", false);
            var a = $('#select10');
            a.siblings("select").append(a.find("option"));
            $("#select9, #select10", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select9 option:selected").prop("selected", false);
        });

        $("#formletters12.modal").on("show.bs.modal", function () {
            $("#select11 option:selected").prop("selected", false);
            var a = $('#select12');
            a.siblings("select").append(a.find("option"));
            $("#select11, #select12", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select11 option:selected").prop("selected", false);
        });

        $("#formletters17.modal").on("show.bs.modal", function () {
            $("#select13 option:selected").prop("selected", false);
            var a = $('#select14');
            a.siblings("select").append(a.find("option"));
            $("#select13, #select14", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select13 option:selected").prop("selected", false);
        });

        $("#formletters19.modal").on("show.bs.modal", function () {
            $("#select15 option:selected").prop("selected", false);
            var a = $('#select16');
            a.siblings("select").append(a.find("option"));
            $("#select15, #select16", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select15 option:selected").prop("selected", false);
        });

        $("#formletters23.modal").on("show.bs.modal", function () {
            $("#select17 option:selected").prop("selected", false);
            var a = $('#select18');
            a.siblings("select").append(a.find("option"));
            $("#select17, #select18", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select17 option:selected").prop("selected", false);
        });

        $("#formletters24.modal").on("show.bs.modal", function () {
            $("#select19 option:selected").prop("selected", false);
            var a = $('#select20');
            a.siblings("select").append(a.find("option"));
            $("#select19, #select20", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select19 option:selected").prop("selected", false);
        });

        $("#formletters28.modal").on("show.bs.modal", function () {
            $("#select21 option:selected").prop("selected", false);
            var a = $('#select22');
            a.siblings("select").append(a.find("option"));
            $("#select21, #select22", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select21 option:selected").prop("selected", false);
        });
        $(document.body).on("click", ".btn-form127-process", function () {
            $("#select24 option").prop('selected', true);
            if ($("#select24").val()) {
                var values = $("#select24").val();
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
                $("#defen_att").val(nw[nw.length - 1]);
            } else {
                $("#default_party").val('');
                $("#defen_att").val('');
            }
            var doc_no = $("#doc_number").val();
            $('#formletters127').modal('hide');
        });

        $("#Add-InsurancePopup.modal").on("show.bs.modal", function () {
            $("#select-i1 option:selected").prop("selected", false);
            var a = $('#select-i2');
            a.siblings("select").append(a.find("option"));
            $("#select-i1, #select-i2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
            $("#select-i1 option:selected").prop("selected", false);
        });
        
        $("#Add-Physician.modal").on("show.bs.modal", function () {
            $("#select-p1 option:selected").prop("selected", false);
            var a = $('#select-p2');
            a.siblings("select").append(a.find("option"));
            $("#select-p1, #select-p2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
            $("#select-p1 option:selected").prop("selected", false);
        });

        $("#Add-opposingattPopup.modal").on("show.bs.modal", function () {
            $("#select-o1 option:selected").prop("selected", false);
            var a = $('#select-o2');
            a.siblings("select").append(a.find("option"));
            $("#select-o1, #select-o2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
            $("#select-o1 option:selected").prop("selected", false);
        });

        $("#Add-EmployerPopup.modal").on("show.bs.modal", function () {
            $("#select-e1 option:selected").prop("selected", false);
            var a = $('#select-e2');
            a.siblings("select").append(a.find("option"));
            $("#select-e1, #select-e2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
            $("#select-e1 option:selected").prop("selected", false);
        });

        $("#Add-DefenseAttyPopup.modal").on("show.bs.modal", function () {
            $("#select-d1 option:selected").prop("selected", false);
            var a = $('#select-d2');
            a.siblings("select").append(a.find("option"));
            $("#select-d1, #select-d2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
            $("#select-d1 option:selected").prop("selected", false);
        });

        $('.injurydate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            /*startDate: new Date()*/
        });

        $('.injurydate1').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true
        });

        $("#uploadBtn3").change(function () {
            if ($(this).val() != "") {
                var docFileData = $(this).prop('files')[0];
                $("#uploadFile3").html(docFileData.name);
            } else {
                $("#uploadFile3").html("Upload File");
            }
        });

        $(document.body).on("click", ".btn-form-process", function () {
            $("#select2 option").prop('selected', true);
            $("#cc_party").val($("#select2").val());
        });

        $(document.body).on("click", ".btn-form135-process", function () {
            $("#dr_speciality").val($("#dr_speciality_f").val());
            $("#body_partexamined").val($("#body_partexamined_f").val());
        });

        /*$(document.body).on("click", ".btn-act", function () {
            $(".btn-act-submit").trigger("click");
        });*/

        $(document.body).on("click", ".btn-form5-process", function () {
            if ($("#doc_number").val() == 409) {
                $('#formletters5').modal('hide');
                $('#formletters7').modal('show');
            }

        });
        /*$('#addcaseact').on('hidden.bs.modal', function () {
            var $alertas = $('#add_new_case_activity');
            $alertas.validate().resetForm();
            $('#uploadFile3').html('Upload');
            $alertas.find('.error').removeClass('error');
        });*/
        $('#addcaseact.modal').on('hidden.bs.modal', function () {
            $(this).find('input[type="text"],input[type="email"],textarea').each(function () {
                this.value = '';
                $('#activity_mainnotes').prop('checked',false).iCheck('update');
                $("#activity_redalert").prop('checked',false).iCheck('update');
                $("#activity_upontop").prop('checked',false).iCheck('update');
                $("#activity_override").prop('checked',false).iCheck('update');
                $('.select2Class#case_category').val('1').trigger("change");
                $('.select2Class#color_codes').val('1').trigger("change");
                $('.select2Class#security').val('1').trigger("change");
                $('.select2Class#activity_doc_type').val('0').trigger("change");
                $('.select2Class#activity_style').val('0').trigger("change");
                $("#uploadFile3").html("Upload");
                $('input[name=activity_no]').val('');
                var $alertas = $('#add_new_case_activity');
                $alertas.validate().resetForm();
                $alertas.find('.error').removeClass('error');

            });
            myDropzone.removeAllFiles(true);
            $('#addcaseact a[href="#tab-26"]').tab('show');
        });

        $("#formletters30 , #formletters22").on("show.bs.modal", function () {
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 3000);
        });

        $(document.body).on("click", ".btn-form1-process", function () {
            var doc_no = $("#doc_number").val();
            $("#depo_date").val($("#dateOfDep").val());
            $("#depo_time").val($("#timeOfDep").val());
            if (doc_no == 408 || doc_no == 400 || doc_no == 401)
            {
                $("#modal_title").html('Select Location For This Depo');
            }
        });

        $(document.body).on("click", ".btn-form7-process", function () {
            $("#depo_date").val($("#dateOfDep1").val());
            $("#depo_time").val($("#timeOfDep1").val());
            $("#fee_amt").val($("#amountOfFee").val());
        });

        $(document.body).on("click", ".btn-form30-process", function () {
            $("#depo_date").val($("#dateOfDep3").val());
            $("#depo_time").val($("#timeOfDep3").val());
            $("#fee_amt").val($("#amountOfFee1").val());
        });

        $(document.body).on("click", ".btn-form128-process", function () {
            $("#depo_date").val($("#depodate1").val());
            $("#depo_time_hours").val($("#depotimehours").val());
            $("#depo_time_minutes").val($("#depotimeminutes").val());
            $("#attorney_fees").val($("#attorneyfees").val());
            $("#trip_mileage").val($("#tripmileage").val());
        });
        
        $(document.body).on("click", ".btn-form32-process", function () {
            $("#depo_date").val($("#lcra_dt").val());
            $("#createDoc").trigger("click");
        });
        
        $(document.body).on("click", ".btn-form33-process", function () {
            $("#pro_unit").val($("#pro_unit").val());
            $("#pro_doc_typ").val($("#pro_doc_typ").val());
            $("#doc_title").val($("#doc_title").val());
        });
        
        $(document.body).on("click", ".btn-form13-process", function () {
            $("#depo_date").val($("#dateOfDep2").val());
            $("#depo_time").val($("#timeOfDep2").val());
            $("#fee_amt").val($("#amountOfFee2").val());
        });

        $(document.body).on("click", ".btn-form9-process", function () {
            $("#pl_title").val($("#pleadingTitle").val());
            $("#my_pl_title").val($("#my_pleadingTitle").val());
            var doc_no = $("#doc_number").val();
            if (doc_no == 410 || doc_no == 411)
            {
                $("#modal_title").html('Select Parties To Serve');
            }
        });

        $(document.body).on("click", ".btn-form10-process", function () {
            $("#date_App").val($("#dateOfApp").val());
            $("#date_Tri").val($("#dateOfTri").val());
            $("#time_Tri").val($("#timeOfTri").val());
            $("#board_City").val($("#boardCity").val());
        });

        $(document.body).on("click", ".btn-form6-process", function () {
            $("#select4 option").prop('selected', true);
            $("#defen_att").val($("#select4").val());
        });

        $(document.body).on("click", ".btn-form24-process", function () {
            $("#select20 option").prop('selected', true);
            if ($("#select20").val()) {
                var values = $("#select20").val();
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
                $("#defen_att").val(nw[nw.length - 1]);
            } else {
                $("#default_party").val('');
                $("#defen_att").val('');
            }
            var doc_no = $("#doc_number").val();

            $('#formletters24').modal('hide');
        });

        $(document.body).on("click", ".btn-form132-process", function () {
            $("#select26 option").prop('selected', true);
            if ($("#select26").val()) {
                var values = $("#select26").val();
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
                $("#defen_att").val(nw[nw.length - 1]);
            } else {
                $("#default_party").val('');
                $("#defen_att").val('');
            }
            var doc_no = $("#doc_number").val();
            $('#formletters132').modal('hide');
        });

        $(document.body).on("click", ".btn-form136-process", function () {
            var doc_no = $("#doc_number").val();
            $("#select32 option").prop('selected', true);
            $("#defen_att").val($("#select32").val());
            $('#formletters136').modal('hide');
            $('#formletters2').modal('show');
        });

        $(document.body).on("click", ".btn-form28-process", function () {
            $("#select22 option").prop('selected', true);
            if ($("#select22").val()) {
                var values = $("#select22").val();
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
            } else
            {
                $("#default_party").val('');
            }
        });

        $(document.body).on("click", ".btn-form11-process", function () {
            var doc_no = $("#doc_number").val();
            if (doc_no == '414' || doc_no == '419')
            {
                $('#formletters12').modal('show');
            } else if (doc_no == '415')
            {
                $('#formletters14').modal('show');
            }
            $("#select10 option").prop('selected', true);
            $("#defen_att").val($("#select10").val());
            $('#formletters11').modal('hide');
        });

        $(document.body).on("click", ".btn-form23-process", function () {
            $('#formletters23').modal('hide');
            $("#select18 option").prop('selected', true);
            $("#defen_att").val($("#select18").val());
        });

        $(document.body).on("click", ".btn-form12-process", function () {
            var doc_no = $("#doc_number").val();
            if (doc_no == '419')
            {
                $('#formletters8').modal('show');
            } else if (doc_no == '415')
            {
                $('#formletters13').modal('show');
            } else if (doc_no == '414')
            {
                $('#formletters30').modal('show');
            }
            $('#formletters12').modal('hide');
            $("#select12 option").prop('selected', true);
            if ($("#select12").val()) {
                var values = $("#select12").val();
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
            } else {
                $("#default_party").val('');
            }
        });

        $(document.body).on("click", ".btn-form17-process", function () {
            $("#select14 option").prop('selected', true);
            if ($("#select14").val()) {
                $('#formletters18').modal('show');
                var doc_no = $("#doc_number").val();
                var values = $("#select14").val();
                if (doc_no == '437')
                {
                    $("#etypediv").css('display', 'none');
                }
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
                $('#formletters17').modal('hide');
            } else {
                $('#formletters18').modal('show');
                if (doc_no == '437')
                {
                    $("#etypediv").css('display', 'none');
                }
                $("#default_party").val('');
                $('#formletters17').modal('hide');
            }
        });

        $(document.body).on("click", ".btn-form19-process", function () {
            var last_value = $('#select16').closest('select').find('option').filter(':last').val();
            var last_name = $('#' + last_value).val();
            $('#dunname').val(last_name);
            $('#formletters19').modal('hide');
            $('#formletters2').modal('show');
            $('.btn-form2-process').attr('data-target', '#formletters8');
            $("#select16 option").prop('selected', true);
            $("#defen_att").val($("#select16").val());

            if ($("#select16").val()) {
                var values = $("#select16").val();
                var nw = values.toString().split(',');
                $("#default_party").val(nw[nw.length - 1]);
            } else {
                $("#default_party").val('');
            }
        });

        $(document.body).on("click", ".btn-form18-process", function () {

            $("#dr_splt").val($("#drspeciality").val());
            $("#e_type").val($("#eType").val());
            $("#e_day").val($("#eday").val());
            $("#e_date").val($("#edate").val());
            $("#e_time").val($("#etime").val());
        });

        $(document.body).on("click", ".btn-form20-process", function () {
            $("#modal_title").html('Select Parties for Proof of Service');
            $("#judgeNM").val($("#hjudge").val());
            $("#depo_date").val($("#dateOfDeph").val());
            $("#depo_time").val($("#timeOfDeph").val());
            $("#pl_title").val($("#typeofh").val());
        });

        $(document.body).on("click", ".btn-form21-process", function () {
            $("#modal_title").html('Select Parties for Proof of Service');
            $("#judgeNM").val($("#hjudge1").val());
            $("#depo_date").val($("#dateOfDeph1").val());
            $("#e_date").val($("#dateOfnDeph1").val());
            $("#depo_time").val($("#timeOfDeph1").val());
            $("#pl_title").val($("#typeofh1").val());
        });

        $(document.body).on("click", ".btn-form26-process", function () {
            $("#modal_title").html('Select Parties for Proof of Service');
            $("#judgeNM").val($("#attName").val());
            $("#depo_date").val($("#depodate").val());
            $("#fee_amt").val($("#feeadv").val());
            $("#depo_time").val($("#depotime").val());
            $("#pl_title").val($("#loctn").val());
            $("#e_type").val($("#physc").val());
            $("#e_day").val($("#reqfee").val());

        });

        $(document.body).on("click", ".btn-form29-process", function () {
            $("#judgeNM").val($("#pages").val());
            $("#fee_amt").val($("#our_ref").val());
            $("#pl_title").val($("#their_ref").val());
            $("#e_type").val($("#comment").val());
        });

        $(document.body).on("click", ".btn-form22-process", function () {
            $("#depo_date").val($("#ddate").val());
            $("#depo_time").val($("#dtime").val());
            $("#pl_title").val($("#dname").val());
            $("#modal_title").html('Select Parties for Proof of Service');
        });

        $(document.body).on("click", ".btn-form14-process", function () {
            $("#depo_date").val($("#dateOffee").val());
            $("#fee_amt").val($("#amountOfWFee").val());
            $("#pl_title").val($("#AMEName").val());
        });

        $(document.body).on("click", ".btn-form15-process", function () {
            $("#pl_title").val($("#drAME").val());
        });

        $(document.body).on("click", ".btn-form8-process", function () {
            $("#select6 option").prop('selected', true);
            $("#cc_party").val($("#select6").val());
        });

        $(document.body).on("click", ".btn-form2-process", function () {
            if ($("#doc_number").val() == 406) {
                var show = $("#ISCC").val();
            } else {
                var show = $("#allowCC").val();
            }

            $("#enum").val($("select[name=isCC]").val());
            $("#modal_title").html('Select Parties To Cc');
            if ($("#judge").val())
                $("#judgeNM").val($("#judge").val());
            if (show == 'n')
            {
                $("#cc_party").val('');
                $('#createDoc').trigger('click');
            } else if (show == 'y') {
                $('#formletters2').modal('hide');
                $('#formletters4').modal('hide');
                $('#formletters3').modal('show');
            }


        });

        $(document.body).on("click", ".btn-form129-process", function () {
            $("#depo_date").val($("#pedate1").val());
            $("#attorney_fees").val($("#attorneyfees1").val());
            $("#daily_wage").val($("#dailywage").val());
            $("#trip_mileage").val($("#tripmileage1").val());
        });

        $(document.body).on("click", ".btn-form25-process", function () {
            $("#depo_date").val($("#mscdate").val());
            $("#depo_time").val($("#msctime").val());
            $("#board_City").val($("#mscboard").val());
        });

        $(document.body).on("click", ".btn-form27-process", function () {
            $("#depo_date").val($("#pedate").val());
            $("#fee_amt").val($("#phyfee").val());
        });
        $(document.body).on("click", ".btn-form133-process", function () {
            $("#total_mileage").val($("#total_mileage_f").val());
            $("#total_mileage_amount").val($("#total_mileage_amount_f").val());
        });

        $(document.body).on("click", ".btn-form131-process", function () {
            $("#depo_date").val($("#depodate3").val());
            $("#judgeNM").val($("#attName1").val());
            $("#travel_time").val($("#traveltime").val());
            $("#depo_prep_time").val($("#depopreptime").val());
            $("#depo_time").val($("#depotime1").val());
            $("#total_time").val($("#totaltime").val());
            $("#total_request").val($("#totalrequest").val());
        });
        
        $(document.body).on("click", ".btn-form130-process", function () {
            $("#depo_date").val($("#pedate2").val());
            $("#attorney_fees").val($("#attorneyfees2").val());
        });

        $(document.body).on("click", ".viewDocForm", function () {
            if ($("ul.list0001 li").hasClass("label-success")) {
                $("ul.list0001 li").removeClass("label-success");
            }
            $(this).closest("li").addClass("label-success");
            var a = $(this).closest("li");
            var c = a.find("a").attr("id");
            var n = a.find("a").data("number");
            var prefix = a.find("a").data("prefix");
            var d = a.find("a").text();
            var d = a.find("a").text();

            $("#A1forms1").text("Form #:" + n + ", " + prefix + " " + d);
            var b = this.id.split(".");
            $("#doc_number").val(b[0]);
            $("#doc_name").val(this.id);
        });

        $(document.body).on("click", "#createDoc", function () {
            var addinj = [];

            if ($("#doc_number").val() == 434 || $("#doc_number").val() == 10861 || $("#doc_number").val() == 10078)
            {
                $(".singlecheck:checked").each(function () {
                    addinj.push($(this).val());
                });
                $("#selected_injury").val(addinj);
                if ($("#default_party").val())
                    var b = [$("#default_party").val()];
                else
                    var b = '';

            } else if ($("#doc_number").val() == 10874 || $("#doc_number").val() == 10031 || $("#doc_number").val() == 10886 || $("#doc_number").val() == 10048 || $("#doc_number").val() == 10074 || $("#doc_number").val() == 10076 || $("#doc_number").val() == 10422 || $("#doc_number").val() == 10540 || $("#doc_number").val() == 10873 || $("#doc_number").val() == 10863 || $("#doc_number").val() == 10025 || $("#doc_number").val() == 10406 || $("#doc_number").val() == 10581 || $("#doc_number").val() == 10891 || $("#doc_number").val() == 11216 || $("#doc_number").val() == 10032 || $("#doc_number").val() == 10875 || $("#doc_number").val() == 10089 || $("#doc_number").val() == 10091 || $("#doc_number").val() == 10210 || $("#doc_number").val() == 10051 || $("#doc_number").val() == 10099 || $("#doc_number").val() == 10075 || $("#doc_number").val() == 11224 || $("#doc_number").val() == 11209 || $("#doc_number").val() == 10214 || $("#doc_number").val() == 10058 || $("#doc_number").val() == 10876 || $("#doc_number").val() == 10501 || $("#doc_number").val() == 10039 || $("#doc_number").val() == 11217) {
                addinj.push($("input[name=singlleinjuryselect]:checked").val());
                 var b = '';
            } else if ($("#doc_number").val() == 404) {
                if ($("#default_party").val())
                    var b = [$("#default_party").val()];
                else
                    var b = '';
                $("#select2 option").prop('selected', true);
                $("#cc_party").val($("#select2").val());
            } else if ($("#doc_number").val() == 409) {
                $("#select8 option").prop('selected', true);
                if ($("#select8").val()) {
                    var b = $("#select8").val();
                } else {
                    var b = '';
                }
            } else if ($("#doc_number").val() == 475) {
                $("#select20 option").prop('selected', true);
                if ($("#select20").val())
                    var b = $("#select20").val();
                else
                    var b = '';
            }  else if ($("#doc_number").val() == 403 ||$("#doc_number").val() == 407 || $("#doc_number").val() == 412 || $("#doc_number").val() == 414 || $("#doc_number").val() == 415 || $("#doc_number").val() == 419 || $("#doc_number").val() == 430 || $("#doc_number").val() == 436 || $("#doc_number").val() == 437 || $("#doc_number").val() == 441 || $("#doc_number").val() == 442 || $("#doc_number").val() == 425 || $("#doc_number").val() == 469 || $("#doc_number").val() == 474 || $("#doc_number").val() == 486 || $("#doc_number").val() == 488 || $("#doc_number").val() == 492 || $("#doc_number").val() == 502 || $("#doc_number").val() == 504 || $("#doc_number").val() == 541) {
                if ($("#default_party").val())
                    var b = [$("#default_party").val()];
                else
                    var b = '';
            } else {
                $("#select2 option").prop('selected', true);
                if ($("#select2").val()) {
                    var b = $("#select2").val();
                } else {
                    var b = '';
                }
            }
            var f = $("#defen_att").val();
            var d = this.id.split(".");
            c = '';
            if (b.length > 0) {

                var c = b[b.length - 1];
                if (f.length > 0) {
                    c = c + "," + f;
                }
            }
            var a = {};
            a.depo_date = $("#depo_date").val();
            a.depo_time = $("#depo_time").val();
            a.fee_amt = $("#fee_amt").val();
            a.pl_title = $("#pl_title").val();
            a.my_pl_title = $("#my_pl_title").val();
            a.date_App = $("#date_App").val();
            a.date_Tri = $("#date_Tri").val();
            a.time_Tri = $("#time_Tri").val();
            a.board_City = $("#board_City").val();
            a.dr_splt = $("#dr_splt").val();
            a.e_type = $("#e_type").val();
            a.e_day = $("#e_day").val();
            a.e_date = $("#e_date").val();
            a.e_time = $("#e_time").val();
            a.cc_party = $("#cc_party").val();
            a["enum"] = $("#enum").val();
            a.judgeNM = $("#judgeNM").val();
            a.Client_suffix = $("#Client_suffix").val();
            a.my_middle = $("#my_middle").val();
            a.my_email = $("#my_email").val();
            a.Client_fname = $("#Client_fname").val();
            a.Client_lname = $("#Client_lname").val();
            a.party = c;
            a.party1 = b;
            a.lastParty = $("#defen_att").val();
            a.caseno = $("#caseNo").val();
            a.fileName = $("#doc_name").val();
            a.clientName = $(".client-personal span.applicant_name").text();
            a.clientAddress = $(".client-personal span.applicant_address").text();
            a.clientAddress2 = $(".client-personal span.applicant_address2").text();
            a.docNumber = $("#doc_number").val();
            a.selected_ins_party = $("#selected_ins_party").val();
            a.selected_defatt_party = $("#selected_defatt_party").val();
            a.selected_phy_party = $("#selected_phy_party").val();
            a.selected_emp_party = $("#selected_emp_party").val();
            a.addinj = $("#selected_injury").val();
            a.pro_unit = $("#pro_unit").val();
            a.pro_doc_typ = $("#pro_doc_typ").val();
            a.doc_title = $("#doc_title").val();
            a.total_mileage = $("#total_mileage").val();
            a.total_mileage_amount = $("#total_mileage_amount").val();
            a.travel_time = $("#travel_time").val();
            a.depo_prep_time = $("#depo_prep_time").val();
            a.total_time = $("#total_time").val();
            a.total_request = $("#total_request").val();
            a.daily_wage = $("#daily_wage").val();
            a.depo_time_hours = $("#depo_time_hours").val();
            a.depo_time_minutes = $("#depo_time_minutes").val();
            a.attorney_fees = $("#attorney_fees").val();
            a.trip_mileage = $("#trip_mileage").val();
            a.dr_speciality = $("#dr_speciality").val();
            a.body_partexamined = $("#body_partexamined").val();
            $("#formletters3").modal("hide");
            $("#formletters2").modal("hide");
            $("#formletters8").modal("hide");
            $("#formletters10").modal("hide");
            $("#formletters13").modal("hide");
            $("#formletters14").modal("hide");
            $("#formletters15").modal("hide");
            $("#formletters16").modal("hide");
            $("#formletters25").modal("hide");
            $("#formletters27").modal("hide");
            $("#formletters29").modal("hide");
            $("#formletters30").modal("hide");
            $("#formletters31").modal("hide");
            $("#formletters32").modal("hide");
            $("#formletters33").modal("hide");
            $("#formletters128").modal("hide");
            $("#formletters129").modal("hide");
            $("#formletters130").modal("hide");
            $("#formletters131").modal("hide");
            $("#formletters133").modal("hide");
            $("#formletters135").modal("hide");
            
            $("#injuryselectionPopup").modal("hide");
            $.blockUI();
            $.ajax({
                url: "<?php echo base_url(); ?>cases/createDoc",
                type: "post",
                data: a,
                success: function (i) {
                    var filepath = i.split('/');
                    var filenm = filepath[1].split('.');
                    filename = $.trim(filenm[0]).substring(1, filenm[0].length);
                    $('#activity_no').val(filename);
                    $.unblockUI();
                    if (i == false) {
                        swal("Oops...", "Something went Wrong !!", "error");
                    } else {
                        if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                            var g = document.createElement("a");
                            g.target = "_blank";
                            g.href = $.trim("<?php echo base_url(); ?>document/doceditor.php?fileID=" + filepath[1] + "&user=" + "<?php echo $this->session->userdata('user_data')['initials']; ?>" + "&filePath=" + i);
                            document.body.appendChild(g);
                            g.click();
                        } else {
                            var g = document.createElement("a");
                            g.href = $.trim("<?php echo base_url(); ?>assets/clients/" + $.trim(i));
                            document.body.appendChild(g);
                            g.click();
                        }
                    }
                    if ($("#doc_number").val() == 407) {
                        $("textarea#activity_event").val("Ltr to Insurance RE:");
                        $("#addcaseact").modal("show");
                    }

                    if ($("#doc_number").val() == 405) {
                        $("textarea#activity_event").val("Ltr to Board RE:");
                        $("#addcaseact").modal("show");
                    }

                    if ($("#doc_number").val() == 403) {
                        $("#addcaseact").modal("show");
                        $("textarea#activity_event").val("Ltr to Defense Attorney RE: settlement demand");
                    }

                    if ($("#doc_number").val() == 404) {
                        $("textarea#activity_event").val("Ltr to " + $('#dunname').val());
                        $("#addcaseact").modal("show");
                    }

                    if ($("#doc_number").val() == 402) {
                        $("textarea#activity_event").val("Ltr to Client RE:");
                        $("#addcaseact").modal("show");
                    }

                    if ($("#doc_number").val() == 406) {
                        $("textarea#activity_event").val("Ltr to Judge " + $('#judge').val());
                        $("#addcaseact").modal("show");
                    }

                    if ($("#doc_number").val() == 469) {
                        $("textarea#activity_event").val("Dun Ltr to:" + $('#dunname').val());
                        $("#addcaseact").modal("show");
                    }

                    $("#depo_date").val("");
                    $("#depo_time").val("0");
                    $("#fee_amt").val('');
                    $("#pl_title").val('');
                    $("#my_pl_title").val('');
                    $("#date_App").val('');
                    $("#date_Tri").val('');
                    $("#time_Tri").val('');
                    $("#board_City").val('');
                    $("#dr_splt").val('');
                    $("#e_day").val('');
                    $("#e_type").val('');
                    $("#e_date").val('');
                    $("#e_time").val('');
                    $("#doc_name").val("");
                    $("#doc_number").val("0");
                    $("#judgeNM").val('');
                    $("#cc_party").val();
                    $("#form_modal").modal("hide");
                    $("#injlistfrm-single")[0].reset();
                    $("#selected_injury").val('');
                }
            });
            return false;
        });

        $(document.body).on("click", "#createEnvelope", function () {
            $("#select28 option").prop('selected', true);
            if ($("#select28").val()) {
                var b = $("#select28").val();
            } else {
                var b = '';
            }

            var f = $("#defen_att").val();
            if (b.length > 0) {
                var c = b[b.length - 1];
                if (f.length > 0) {
                    c = c + "," + f;
                }
            }
            var a = {};
            a.parties = b;
            a.defen_att = f;
            a.lastParty = c;
            a.caseNo = $('#caseNo').val();
            
            $.ajax({
                url: "<?php echo base_url(); ?>cases/createEnvelope",
                type: "post",
                data: a,
                success: function (i) {
                    if (i == false) {
                        swal("Oops...", "Select Party to Generate Envelope", "error");
                    } else {
                        var filepath = i.split('/');
                    var filenm = filepath[1].split('.');
                    filename = $.trim(filenm[0]).substring(1, filenm[0].length);
                    if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                            var g = document.createElement("a");
                            g.target = "_blank";
                            g.href = $.trim("<?php echo base_url(); ?>document/doceditor.php?fileID=" + filepath[1] + "&user=" + "<?php echo $this->session->userdata('user_data')['initials']; ?>" + "&filePath=" + i);
                            document.body.appendChild(g);
                            g.click();
                        } else {
                            var g = document.createElement("a");
                            g.href = $.trim("<?php echo base_url(); ?>assets/clients/" + $.trim(i));
                            document.body.appendChild(g);
                            g.click();
                        }
                        $("#envelope").modal("hide");
                    return false;
                    }
                }
            });
            return false;
        });

        $('#envelope.modal').on('hidden.bs.modal', function () {
            $("#select27 option:selected").prop("selected", false);
            var a = $('#select28');
            a.siblings("select").append(a.find("option"));
            $("#select27, #select28", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                $(this).html(b);
            });
            $("#select27 option:selected").prop("selected", false);
        });
        
        $('#injuryselectionPopup').on('hidden.bs.modal', function () {
            $("#injlistfrm-single")[0].reset();
            $("#singlleinjuryselect-1").iCheck('check');

        });

        $('#injuryselectionPopup').on('show.bs.modal', function () {
            $("#injlistfrm-single")[0].reset();
            $("#singlleinjuryselect-1").iCheck('check');

        });

        $(document.body).on("click", ".viewDocForm", function () {
            var currentTab = $('ul.nav.nav-tabs li.active a').text();
            if (currentTab == 'Print LettersPrint LettersGeneral')
            {
                $("tr").removeClass("label-success");
                $(this).parent().parent().addClass("label-success");

            }
        });

        $(document.body).on("click", ".createDoc", function (){
            if ($("#form-letter2 tr").hasClass("label-success")) {
                $("#form-letter2 tr").removeClass("label-success");
            }
            $(this).closest("tr").addClass("label-success");
            if ($("ul.list0001 li").hasClass("label-success")) {
                $("ul.list0001 li").removeClass("label-success");
            }
            $(this).closest("li").addClass("label-success");
            var b = $(this).closest("li");
            var e = b.find("a").attr("id");
            var n = b.find("a").data("number");
            var prefix = b.find("a").data("prefix");
            var f = b.find("a").text();
            $("#A1forms1").text("Form #:" + n + ", " + prefix + " " + f);
            
        });
        
        $(document.body).on("click", ".createDoc", function () {

            var d = this.id.split(".");
            $("#doc_number").val(d[0]);
            $("#doc_name").val(this.id);
            var c = $(this).attr("data-cardcode");
            if (d[0] == 10859) {
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/countemployerparty',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        caseno: $("#caseNo").val()
                    },
                    success: function (data) {
                        if (data.count >= 2) {
                            $("#Add-EmployerPopup").modal('show');
                            $("#selected_emp_party").val("");
                            $("#select-e2").val("");
                        } else {
                            $("#createDoc").trigger("click");
                        }
                    }
                });
            } else if (d[0] == 'Lacera_amendment') {
                    $.ajax({
                    url: '<?php echo base_url(); ?>cases/countdoc',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        caseno: $("#caseNo").val()
                    },
                    success: function (data) {
                        if (data.count >= 2) {
                            $("#Add-Physician").modal('show');
                            $("#selected_phy_party").val("");
                            $("#select-p2").val("");
                        } else {
                            $("#createDoc").trigger("click");
                        }
                    }
                });
            }  
            else {
                $("#createDoc").trigger("click");
            }
        });

        $("#form_modal").on("show.bs.modal", function () {
            $(".dataTables-example").removeAttr("style");
            $(".dataTables_scrollHeadInner").removeAttr("style");
            $(".dataTables-example").css("width", "100% !important;");
            $(".dataTables_scrollHeadInner").css("width", "100% !important;");
        });

        $(document.body).on('click', '.activity_delete', function () {
            var actId = $(this).data('actid');
            swal({
                title: "Are you sure to Remove this Activity?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                var caseNo = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/deleteActivity',
                    method: 'POST',
                    data: {
                        actno: actId,
                        caseno: caseNo
                    },
                    success: function (data) {
                        if (data == 1) {
                            swal("Deleted!", "Activity Deleted Successfully !!", "success");
                            $('.activity_delete[data-actid=' + actId + ']').parents('tr').remove();
                        } else {
                            swal("Oops...", "Oops! Something went wrong.", "error");
                        }
                    }
                });
            });
        });
        
        $(document.body).on('click', '.activity_edit', function () {
            var id = $(this).data('actid');
            caseActEdit(id);
        });

        function caseActEdit(id){
            var caseNo = $("#caseNo").val();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxActivityDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    actno: id,
                    caseno: caseNo,
                },
                success: function (data) {
                    $('#add_new_case_activity')[0].reset();
                    if (data == false) {
                        swal({
                            title: "Alert",
                            text: "Oops! Some error has occured while fetching details. Please try again later.",
                            type: "warning"
                        });
                    } else {
                        if(data.access=='2'){
                            if(data.initials=='<?php echo $username ?>'){
                                $('#add_new_case_activity .btn-primary').prop('disabled', false);
                                $('#addcaseact .modal-title').html('Edit Case Activity');

                            }else {
                                    $('#add_new_case_activity .btn-primary').prop('disabled', true);
                                    $('#addcaseact .modal-title').html('Edit Case Activity      <small>(This Activity is Restricted to be change by '+data.initials0+')</small>');
                            }
                        }
                        else{
                            $('#add_new_case_activity .btn-primary').prop('disabled', false);
                            $('#addcaseact .modal-title').html('Edit Case Activity');
                        }

                        myDropzone.removeAllFiles(true);
                        if (data.document != "") {
                            $.each(data.document, function (index, item) {

                                var baseUrl = '<?php echo base_url(); ?>' + item;
                                var mockFile = {name: item, size: 12345, status: 'success'};
                                myDropzone.emit("addedfile", mockFile);
                                myDropzone.emit("thumbnail", mockFile, baseUrl);
                                myDropzone.files.push(mockFile); // file must be added manually

                                /*var mockFile = { name: item, size: '12345' };
                                    myDropzone.options.addedfile.call(myDropzone, mockFile);
                                    myDropzone.options.thumbnail.call(myDropzone, mockFile, baseUrl);*/
                            });
                        }

                        $('#original_ini').val(data.initials0);
                        var datefrmt = moment(data.date).format('MM/DD/YYYY');
                        var datetime = moment(data.date).format('hh:mm a');
                        var datebicyclefrmt = moment(data.bicycle).format('MM/DD/YYYY');
                        var datepmtdate = moment(data.bipmtdate).format('MM/DD/YYYY');
                        var datepmduedate = moment(data.bipmtduedt).format('MM/DD/YYYY');


                        /*$(".documentAttch").removeClass("hidden");
                            $(".activityNo").html(data.actno);
                            var filePath = "f" + data.actno + ".* (Default Document )";
                            $(".documentPath").html(filePath);*/

                        $("#activity_no").val(data.actno);
                        $('#add_case_cal').val(datefrmt);
                        $('#add_case_time').val(datetime);
                        $('#activity_event').html(data.event);
                        $('#last_ini').val(data.initials);
                        //$('#case_act_atty').val(data.atty).trigger("change");
                        if ($('#case_act_atty option:contains('+ data.atty +')').length) {
                        $('select[name=case_act_atty]').val(data.atty);
                        }
                        else
                        {
                            $('#case_act_atty').append("<option value='" + data.atty + "' selected='seleceted'>" + data.atty + "</option>");
                        }
                        $('#case_act_time').val(data.minutes);
                        $('#case_act_hour_rate').val(data.rate);
                        $('#case_act_cost_amt').val(data.cost);
                        $('#activity_title').val(data.title).trigger("change");


                        var str1 = data.type;
                        if(str1!=null){
                        if(str1.indexOf('OTHER-') != -1){
                        var ret = data.type.replace('OTHER-','');
                            $('#activity_fee').val('OTHER').trigger('change');
                            $('#activity_fee1').val(ret);
                        }}else{
                            $('#activity_fee').val(data.type).trigger('change');
                        }
                        $("input[name=activity_classification][value=" + data.arap + "]").parent('div').addClass('checked');

                        $('#case_category').val(data.category).trigger("change");
                        $('#color_codes').val(data.color).trigger("change");
                        $('#security').val(data.access).trigger("change");
                        if (data.mainnotes != 0) {
                            $('#activity_mainnotes').parent('div').addClass('checked');
                            //$( "#activity_mainnotes" ).trigger( "change" );
                            $('input[name=activity_mainnotes]').prop('checked', true).triggerHandler('click');
                        }
                        if (data.redalert != 0) {
                            $('#activity_redalert').parent('div').addClass('checked');
                            //$( "#activity_redalert" ).trigger( "change" );
                            $('input[name=activity_redalert]').prop('checked', true).triggerHandler('click');
                        }
                        if (data.upontop != 0) {
                            $('#activity_upontop').parent('div').addClass('checked');
                            //$( "#activity_upontop" ).trigger( "change" );
                            $('input[name=activity_upontop]').prop('checked', true).triggerHandler('click');
                        }

                        $('#activity_typeact').val(data.typeact).trigger("change");
                        $('#activity_doc_type').val(data.wordstyle).trigger("change");
                        $('#activity_style').val(data.ole3style).trigger("change");
                        //$('#activity_o_init').val(data.event);
                        //$('#activity_l_init').val(data.event);
                        //$('#activity_last_init').val(data.event);
                        $('#case_act_TM').val(data.frmtm);
                        $('#case_act_BM').val(data.frmbm);
                        $('#case_act_LM').val(data.frmlm);
                        $('#case_act_RM').val(data.frmrm);
                        $('#case_act_PW').val(data.frmwidth);
                        $('#case_act_PH').val(data.frmheight);
                        $('#case_act_orientation').val(data.orient).trigger("change");
                        $('#case_act_copies').val(data.copies);
                        $('#activity_entry_type').val(data.bistyle).trigger("change");
                        $('#activity_teamno').val(data.biteamno);
                        $('#activity_staff').html(data.biowner);
                        $('#activity_short_desc').html(data.bidesc);
                        $('#activity_event_desc').html(data.bicost);
                        $('#activity_biling_cycle').val(datebicyclefrmt);
                        $('#activity_postmark_date').val(datepmtdate);
                        $('#activity_payment_due').val(datepmduedate);
                        //$('#activity_amount').html(data.event);
                        $('#activity_latefee').val(data.bilatefee);
                        $('#activity_check_no').val(data.bicheckno);
                        $('#activity_bi_hour').html(data.event);
                        $('#activity_bi_min').html(data.bitime);
                        $('#activity_bi_rate').html(data.bihourrate);
                        $('#activity_bi_fee').html(data.bifee);
                        $('#activity_bi_latefee').html(data.bilatefee);
                        $('#activity_bi_creditac').html(data.bipmt);
                        // $('#addcaseact .modal-title').html('Edit Case Activity');
                        $('#addcaseact').modal('show');
                    }
                }
            });
        }   

        //function for case act
        $(document.body).on('click', '.injuryselectionPopup-process', function () {
            $("#injuryselectionPopup").modal('hide');
            var addinj = [];
            addinj.push($("input[name=singlleinjuryselect]:checked").val());
            $("#selected_injury").val(addinj);
            if ($("#doc_number").val() == 10891 || $("#doc_number").val() == 11209 || $("#doc_number").val() == 10873 || $("#doc_number").val() == 10875 || $("#doc_number").val() == 10876 || $("#doc_number").val() == 10882 || $("#doc_number").val() == 10886 || $("#doc_number").val() == 11216 || $("#doc_number").val() == 10885 || $("#doc_number").val() == 10874 || $("#doc_number").val() == 10871 || $("#doc_number").val() == 10865 || $("#doc_number").val() == 10857 || $("#doc_number").val() == 10863) {

                $.ajax({
                    url: '<?php echo base_url(); ?>cases/countdefenseatt',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        caseno: $("#caseNo").val()
                    },
                    success: function (data) {
                        if (data.count >= 2) {
                            $("#Add-DefenseAttyPopup").modal('show');
                            if ($("#doc_number").val() == 10863)
                            {
                                $('#Add-DefenseAttyPopup .modal-title').text('Select The Opposing Attorney Firm');
                            } else
                            {
                                $('#Add-DefenseAttyPopup .modal-title').text('Select The Defense Attorney');
                            }
                            $("#selected_defatt_party").val("");
                            $("#select-e2").val("");

                        } else {
                            /*Insurance popup form number set*/
                            if (($("#doc_number").val() == 10891 || $("#doc_number").val() == 11209 || $("#doc_number").val() == 10873 || $("#doc_number").val() == 10875 || $("#doc_number").val() == 10876 || $("#doc_number").val() == 10882 || $("#doc_number").val() == 10886 || $("#doc_number").val() == 11216 || $("#doc_number").val() == 10885 || $("#doc_number").val() == 10874 || $("#doc_number").val() == 10871 || $("#doc_number").val() == 10865 || $("#doc_number").val() == 10857) && $("#doc_number").val() != 10863) {
                                $.ajax({
                                    url: '<?php echo base_url(); ?>cases/countinsuranceparty',
                                    method: 'POST',
                                    dataType: "json",
                                    data: {
                                        caseno: $("#caseNo").val()
                                    },
                                    success: function (data) {
                                        if (data.count >= 2) {
                                            $("#Add-InsurancePopup").modal('show');
                                            $("#selected_ins_party").val("");
                                            $("#select-i2").val("");
                                        } else {
                                            $("#createDoc").trigger("click");
                                        }
                                    }
                                });
                            } else {
                                $("#createDoc").trigger("click");
                            }
                        }
                    }
                });
            } 
            else if($("#doc_number").val() == 10006) 
            {
        $('#Add-InsurancePopup .modal-title').text('Select The Carrier');
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/countinsuranceparty',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        caseno: $("#caseNo").val()
                    },
                    success: function (data) {
                        if (data.count >= 2) {
                            $("#Add-InsurancePopup").modal('show');
                            $("#selected_ins_party").val("");
                            $("#select-i2").val("");
                        } else {
                            $.ajax({
                                url: '<?php echo base_url(); ?>cases/countdefenseatt',
                                method: 'POST',
                                dataType: "json",
                                data: {
                                    caseno: $("#caseNo").val()
                                },
                                success: function (data) {
                                    if (data.count >= 2) {
                                        $("#Add-DefenseAttyPopup").modal('show');
                                        $("#selected_defatt_party").val("N");
                                        $("#select-e2").val("");
                                    } else {
                                        $("#formletters6").modal('show');
                                    }
                                }
                            });
                        }
                    }
                });
            }
                /*Pop up for the Employer for Form 10889*/
               else if ($("#doc_number").val() == '10889') {
                     $.ajax({
                         url: '<?php echo base_url(); ?>cases/countemployerparty',
                         method: 'POST',
                         dataType: "json",
                         data: {
                             caseno: $("#caseNo").val()
                         },
                         success: function (data) {
                             if (data.count >= 2) {
                                 $("#Add-EmployerPopup").modal('show');
                                 $("#selected_emp_party").val("");
                                 $("#select-e2").val("");
                             } else {
                                 $("#createDoc").trigger("click");
                             }
                         }
                     });      
                  }  

               else {
                   $("#createDoc").trigger("click");
               }     
        });

        $(document.body).on('click', '.multipleInjuryProcess', function () {
            $("#formletters31").modal('hide');
            var addinj = [];
            $(".singlecheck:checked").each(function () {
                addinj.push($(this).val());
            });
            $("#selected_injury").val(addinj);
            if ($("#doc_number").val() == 10890 || $("#doc_number").val() == 10864 || $("#doc_number").val() == 10857 || $("#doc_number").val() == 10856 || $("#doc_number").val() == 10871) {
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/countdefenseatt',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        caseno: $("#caseNo").val()
                    },
                    success: function (data) {
                        if (data.count >= 2) {
                            $("#Add-DefenseAttyPopup").modal('show');
                            $("#selected_defatt_party").val("");
                            $("#select-e2").val("");
                        } else {
                            if ($("#doc_number").val() == 10890 || $("#doc_number").val() == 10864 || $("#doc_number").val() == 10857 || $("#doc_number").val() == 10856 || $("#doc_number").val() == 10885 || $("#doc_number").val() == 10871) {
                                $.ajax({
                                    url: '<?php echo base_url(); ?>cases/countinsuranceparty',
                                    method: 'POST',
                                    dataType: "json",
                                    data: {
                                        caseno: $("#caseNo").val()
                                    },
                                    success: function (data) {
                                        if (data.count >= 2) {
                                            $("#Add-InsurancePopup").modal('show');
                                            $("#selected_ins_party").val("");
                                            $("#select-i2").val("");
                                        } else {
                                            $("#createDoc").trigger("click");
                                        }
                                    }
                                });
                            } else {
                                $("#createDoc").trigger("click");
                            }
                        }
                    }
                });
            } else {
                $("#createDoc").trigger("click");
            }
        });

        $("#select-i1, #select-i2").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select-i1, #select-i2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                // b = b.sort(function (f, e) {
                //     return f.value - e.value;
                // });
                $(this).html(b);
            });
        });
        
        $("#select-p1, #select-p2").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select-p1, #select-p2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
        });

        $("#select-e1, #select-e2").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select-e1, #select-e2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                // b = b.sort(function (f, e) {
                //     return f.value - e.value;
                // });
                $(this).html(b);
            });
        });

        $("#select-d1, #select-d2").on(changeEvent, function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected").attr("selected", false));
            $("#select-d1, #select-d2", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
        });

        $(document.body).on("click", ".btn-selectinsparty-process", function () {
            $("#select-i2 option").prop('selected', true);

            var values = $("#select-i2").val();
            var inspartySelected = "";
            if (values !== null) {
                inspartySelected = values.toString().split(',');

                $("#selected_ins_party").val(inspartySelected[inspartySelected.length - 1]);
            } else {
                $("#selected_ins_party").val('N');
            }
            $("#Add-InsurancePopup").modal('hide');

            if ($("#doc_number").val() == 10006) {
                
                $('#Add-DefenseAttyPopup .modal-title').text('Select The Opposing Attorney Firm');
                  
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/countdefenseatt',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        caseno: $("#caseNo").val()
                    },
                    success: function (data) {
                        if (data.count >= 2) {
                            $("#Add-DefenseAttyPopup").modal('show');
                            $("#selected_defatt_party").val("");
                            $("#select-e2").val("");
                        } else {
                            $("#formletters6").modal('show');
                        }
                    }
                });
            } else {
                $("#createDoc").trigger("click");
            }


        });
        
        
        $(document.body).on("click", ".btn-selectphys-process", function () {
            $("#select-p2 option").prop('selected', true);

            var values = $("#select-p2").val();
            var phySelected = "";
            if (values !== null) {
                phySelected = values.toString().split(',');

                $("#selected_phy_party").val(phySelected[phySelected.length - 1]);
            } else {
                $("#selected_phy_party").val('N');
            }
            $("#Add-Physician").modal('hide');
            $("#createDoc").trigger("click");
        });

        $(document.body).on("click", ".btn-selectdefatt-process", function () {
            $("#select-d2 option").prop('selected', true);

            var values = $("#select-d2").val();

            var defattSelected = "";
            if (values !== null) {
                defattSelected = values.toString().split(',');
                $("#selected_defatt_party").val(defattSelected[defattSelected.length - 1]);
                $("#Add-DefenseAttyPopup").modal('hide');
            } else {
                /*swal({
                 title: "You have not selected any parties to merge.",
                 type: "warning",
                 showCancelButton: false,
                 customClass: "sweet-alert-warning",
                 });*/
                $("#selected_defatt_party").val('N');
                $("#Add-DefenseAttyPopup").modal('hide');
            }

            /*send all data about popup  form number set*/
            if ($("#doc_number").val() == 10890 || $("#doc_number").val() == 10864 || $("#doc_number").val() == 10857 || $("#doc_number").val() == 10865 || $("#doc_number").val() == 10856 || $("#doc_number").val() == 10891 || $("#doc_number").val() == 11209 || $("#doc_number").val() == 10873 || $("#doc_number").val() == 10875 || $("#doc_number").val() == 10876 || $("#doc_number").val() == 10882 || $("#doc_number").val() == 10886 || $("#doc_number").val() == 10856 || $("#doc_number").val() == 11216 || $("#doc_number").val() == 10885 || $("#doc_number").val() == 10874 || $("#doc_number").val() == 10871) {
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/countinsuranceparty',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        caseno: $("#caseNo").val()
                    },
                    success: function (data) {
                        if (data.count >= 2) {
                            $("#Add-InsurancePopup").modal('show');
                            $("#selected_ins_party").val("");
                            $("#select-i2").val("");
                        } else {
                            $("#createDoc").trigger("click");
                        }
                    }
                });
            } else if ($("#doc_number").val() == 10006) {
                $("#formletters6").modal('show');
            } else {
                $("#createDoc").trigger("click");
            }
        });

        $(document.body).on("click", ".btn-selectiemployer-process", function () {
            $("#select-e2 option").prop('selected', true);
            var values = $("#select-e2").val();
            var inspartySelected = "";
            if (values !== null) {
                inspartySelected = values.toString().split(',');
                $("#selected_emp_party").val(inspartySelected[inspartySelected.length - 1]);
                $("#Add-EmployerPopup").modal('hide');
                $("#createDoc").trigger("click");
            } else {
                $("#selected_emp_party").val('N');
                $("#Add-EmployerPopup").modal('hide');
                $("#createDoc").trigger("click");
                
            }
        });
    });
    
    $(document.body).on('change', '#pro_unit', function (event) {
        last_valid_selection = $(this).val();
        pro_doc_typ = <?php echo json_encode(doc_type) ?>;
        //str = JSON.stringify(pro_doc_typ);
        $('#pro_doc_typ').empty();
        var optn = '';
        optn = optn + '<option value="none"></option>';
        $.each(JSON.parse(pro_doc_typ), function (key, value) {
           if (last_valid_selection == value['category']) {
               optn = optn + '<option value="'+value["title"]+'">'+value["title"]+'</option>';
           }
        });
        $('#pro_doc_typ').append(optn);
        //console.log(optn);
    });
    
    $(document.body).on('change', '#pro_doc_typ', function (event) {
        last_valid_selection = $(this).val();
        pro_doc_typ = $('#pro_unit').val();
        doc_title = <?php echo json_encode(doc_title) ?>;
        //str = JSON.stringify(pro_doc_typ);
        $('#doc_title').empty();
        var optn = '';
        optn = optn + '<option value="none"></option>';
         $.each(JSON.parse(doc_title), function (key, value) {
            if (last_valid_selection == value['title'] && pro_doc_typ == value['category']) {
                optn = optn + '<option value="'+value["val"]+'">'+value["val"]+'</option>';
           }
        });
        $('#doc_title').append(optn);
        //console.log(optn);
    });
    $(document.body).on('click', '.CaseActEmail', function () {
        var activity_event = $('#activity_event').val();
        if (CKEDITOR.instances.mailbody1)
            CKEDITOR.instances.mailbody1.destroy();
        CKEDITOR.replace('mailbody1');
        $('#addcaseact').modal('hide');
        $('input[name=activity_no]').val('');
        $('#CaseEmailModal').modal('show');
        $('#mailbody1').val(activity_event);
    });
    $(document.body).on('click', '.CaseActPrint', function () {
        $.ajax({
            url: '<?php echo base_url() . 'cases/printpde' ?>',
            method: "POST",
            data: {caseno: $("#caseNo").val()},
            success: function (result) {
                var based = "<?php echo base_url(); ?>" + 'assets/printCaseAct/' + result;
                window.open(based, '_blank');
            }
        });
    });
    
    $('#addcaseact').on('shown.bs.modal', function (e) {
        var d = new Date();
        var hours = d.getHours();
        var minutes = d.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        var currenttime = d;

        if(d.getHours() > 12) {
            d.setHours(d.getHours() - 12);
        }
        if(d.getHours() < 10) {
            hours = '0' + d.getHours();
        } else {
            hours = d.getHours();
        }
        if(d.getMinutes() < 10) {
            minutes = '0' + d.getMinutes();
        } else {
            minutes = d.getMinutes();
        }
        var currenttime = hours+":"+minutes+" "+ampm;
        if($('#add_case_time').val() == '') {
            $('#add_case_time').val(currenttime);
        }
        notesLocalStorageTimer = setInterval(function () {
            var eval = $('textarea#activity_event').val();
            if (eval)
                localStorage.setItem('autorecoverevent', eval);
        }, 60000);
        $('#original_ini').val("<?php echo $this->session->userdata('user_data')['initials']; ?>");
        $('#last_ini').val("<?php echo $this->session->userdata('user_data')['initials']; ?>");
        if($('#add_case_cal').val() == '') {
            $('#add_case_cal').val("<?php echo date('m/d/Y');?>");
        }
    });
    
    $(document).on('click',".remove",function (e) {
        var a = $(this).parent().css('display','none');
        var imagename = $(this).data('image');
        var chkmodulef = $('#formmodule').val();
        if(chkmodulef == 'documents_New' || chkmodulef == 'documents_new'){
            var newfUrl = '<?php echo base_url(); ?>email_new/emailattaremove';
        }else{
            var newfUrl = '<?php echo base_url('email/emailattaremove'); ?>';
        }
        $.ajax({
            url: newfUrl,
            type: 'post',
            data: {'imagename': imagename},
            success : function(data) {
                removedFileSize = data.split(':')[1];
                totalFileSize = $('#totalFileSize').val() - removedFileSize;
                console.log(totalFileSize);
                $('#totalFileSize').val(totalFileSize);
            },
            error : function(request) { }
        });
    });
</script>