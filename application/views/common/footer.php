<?php
$url_segment1 = $this->uri->segment(1);
$url_segment2 = $this->uri->segment(2);
?>

<div class="footer">
    <div>
        <strong><?php echo APPLICATION_NAME; ?></strong> &copy; 2016-<?php echo Date('Y'); ?>
    </div>
</div>

<script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/blockUI.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/select2/select2.full.js" rel="stylesheet"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/editbleselect/jquery-editable-select.min.js" rel="stylesheet"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/toastr/toastr.min.js"></script>

<?php //if ($url_segment1 != 'calendar') { ?>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/moment/moment.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/moment/moment-timezone.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/buttons.print.min.js"></script>
<?php //} ?>

<script src="<?php echo base_url('assets'); ?>/js/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/additional-methods.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/jquery.mask.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/default.js"></script>

<script src="<?php echo base_url('assets'); ?>/js/common_js.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/chosen/chosen.jquery.js"></script>

<?php if ($url_segment1 == 'dashboard') { ?>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-highlighttextarea-master/jquery.highlighttextarea.js"></script>
<?php } ?>


<?php if ($url_segment2 == 'case_details') { ?>
    <script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/rowReordering.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/lightbox.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/notification.js"></script>
<?php } ?>
<input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url('tasks'); ?>">

<input type="hidden" name="isEmailUrgent" id="isEmailUrgent" value="">
<?php
$url_segment = $this->uri->segment(1);
if ($url_segment == 'email_old') {
?>
<input type="hidden" name="module" id="module" value="email_old">
<script src="<?php echo base_url('assets'); ?>/js/email_dash_new.js"></script>
<input type="hidden" name="urgentEmailBaseUrl" id="urgentEmailBaseUrl" value="<?php echo base_url('email_new'); ?>">
<?php }else if($url_segment == 'email'){ ?>
<input type="hidden" name="module" id="module" value="email">
<script src="<?php echo base_url('assets'); ?>/js/email_new.js"></script>
<input type="hidden" name="urgentEmailBaseUrl" id="urgentEmailBaseUrl" value="<?php echo base_url('email_new'); ?>">
<?php } ?>
<?php 
if($url_segment2 == 'case_details_new' || $url_segment2 == 'case_details_New' || $url_segment2 == 'documents_new' || $url_segment2 == 'documents_New') { ?>
    <input type="hidden" name="module" id="module" value="email_new">
    <input type="hidden" name="urgentEmailBaseUrl" id="urgentEmailBaseUrl" value="<?php echo base_url   ('email_new'); ?>">
<?php } elseif($url_segment2 == 'case_details' || $url_segment2 == 'case_details' || $url_segment2 == 'documents' || $url_segment2 == 'documents') { ?>
    <input type="hidden" name="module" id="module" value="email_new">
    <input type="hidden" name="urgentEmailBaseUrl" id="urgentEmailBaseUrl" value="<?php echo base_url   ('email_new'); ?>">
<?php } ?>
<script>
    $(".navbar-header,.fa-bars").on('click', function () {
        if ($("body").hasClass("mini-navbar")) {
            localStorage.setItem('mini', 'mini-navbar');
        } else {
            localStorage.setItem('mini', '');
        }
    });

    var isAppleDevice = navigator.userAgent.match(/(iPod|iPhone|iPad)/) != null;
    
    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    
    $(document).ready(function () {
      var inputs = jQuery("input:not(.es-input)");
      if ( isAppleDevice ){
        $(document).on('touchstart','body', function (event) {
          var targetTouches = event.originalEvent.targetTouches;
          if ( !$(event.target).hasClass("es-visible")){
            inputs.context.activeElement.blur();
          }
        });
      }

        checkNewEmail();
       var ConverseSession = converse.initialize({
           bosh_service_url: '<?php echo XMPP_BOSH_URL; ?>',
           authentication: 'prebind',
           prebind: true,
           show_desktop_notifications: false,
           play_sounds: true,
           allow_muc: true,
           jid: "<?php echo strtolower($this->session->userdata('user_data')['initials']) . "@" . XMPP_HOST; ?>",
           auto_away: 60,
           auto_login: true,
           prebind_url: '<?php echo base_url('xmpp'); ?>',
           keepalive: true,
           message_carbons: true,
           roster_groups: false,
           show_controlbox_by_default: false,
           allow_contact_requests: false,
           allow_non_roster_messaging: true,
           sounds_path: '<?php echo base_url(); ?>assets/sounds/',
           allow_contact_removal: false,
           hide_muc_server: true,
           allow_logout: true,
           auto_reconnect: true,
           allow_otr: false
       });

        var checkInternetConnectionTimer = setInterval(function () {
            var state = navigator.onLine ? "online" : "offline";
            if (state == 'offline') {
                Command: toastr["error"]("OffLine: Please Check Your Internet Connection");
                setTimeout(function () {
                    window.location = '<?php echo base_url(); ?>login';
                }, 1000);
                clearInterval(checkInternetConnectionTimer);
            }
        }, 1000);

        jQuery.event.special.dblclick = {
            setup: function (data, namespaces) {
                var agent = navigator.userAgent.toLowerCase();
                if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0 || agent.indexOf('ipod') >= 0) {
                    var elem = this, $elem = jQuery(elem);
                    $elem.bind('touchend.dblclick', jQuery.event.special.dblclick.handler);
                } else {
                    var elem = this, $elem = jQuery(elem);
                    $elem.bind('click.dblclick', jQuery.event.special.dblclick.handler);
                }
            },

            teardown: function (namespaces) {
                var agent = navigator.userAgent.toLowerCase();
                if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0 || agent.indexOf('ipod') >= 0) {
                    var elem = this, $elem = jQuery(elem);
                    $elem.unbind('touchend.dblclick');
                } else {
                    var elem = this, $elem = jQuery(elem);
                    $elem.unbind('click.dblclick', jQuery.event.special.dblclick.handler);
                }
            },

            handler: function (event) {
                var elem = event.target, $elem = jQuery(elem), lastTouch = $elem.data('lastTouch') || 0, now = new Date().getTime();
                var delta = now - lastTouch;
                if (delta > 20 && delta < 500) {
                    $elem.data('lastTouch', 0);
                    $elem.trigger('dblclick');
                } else {
                    $elem.data('lastTouch', now);
                }
            }
        };

       
		sessioncheck();
		function sessioncheck()
		{	
			$.ajax({
				   url: '<?php echo base_url('script/checkAjaxSession'); ?>',
				   dataType: 'json',
				   type: 'POST',
				   success: function (data) {
					   
					   if (data.status == 'session_expire') {
						   clearInterval(checkInternetConnectionTimer);
						  
						   Command: toastr["success"]("Your Login Session expire.");
						   setTimeout(function () {
							   window.location = '<?php echo base_url(); ?>login';
						   }, 1000);
					   } else if (data.status == 'force_logout') {
						   clearInterval(checkInternetConnectionTimer);

						   toastr.success("", "The System will be going down for maintenance. You will be logged off in approximately 10 seconds.", {
							   "timeOut": "10000"
						   });
						   setTimeout(function (data) {
							   clearloginsession();
						   }, 10000);
					   } else { 
              sessioncheck();
            }
				   }
			   });
		}
    });

    function clearloginsession() {
        $.ajax({
            url: '<?php echo base_url('script/clearloginsession'); ?>',
            type: 'POST',
            success: function (result) {
                window.location = '<?php echo base_url(); ?>login';
            }
        });
    }

    function logout() {
        swal({
            title: "Are you sure to Logout From <?php echo APPLICATION_NAME; ?>? ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, I am!",
            closeOnConfirm: false
        }, function () {
            $.blockUI();
            $('#conversejs .logout').trigger('click');
            setTimeout(() => {
                window.location.href = "<?php echo base_url('login/logout') ?>";
            }, 1000);
        });
    }

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        /* route: 'long_name', */
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        
        var fulladdress = '';
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                if(i== 0){
                    var val_new = place.address_components[i+1][componentForm[addressType]];
                    var val = place.address_components[i][componentForm[addressType]] + ', ' + val_new;
                    document.getElementById(addressType).value = val;
                }
                else
                {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
                }
            }
        }
        /* var x ='';
         if (document.getElementById('check_add').checked) {
         x = document.getElementById('check_add').value;
         }
         
         if(x == 1){
         document.getElementById('fulladdress1').value = fulladdress;
         }
         else{
         document.getElementById('fulladdress2').value = fulladdress;
         } */
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    function checkNewEmail() {
        var pervious_ucount = $('.dropdown .email_count').text();
        var chkmodule = $('#module').val();
        var newUrl = '<?php echo base_url('email/checkUnreadEmails'); ?>';
        var NewUrl = HTTP_PATH + "email/get_unread_count";
        
        $.ajax({
            url: newUrl,
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                $('.email_count').text(data.ucount);
                if (pervious_ucount != "" && data.ucount > pervious_ucount) {
                    $('.total_mail_count').html(data.ucount).show();
                    $('.mail-box-header .total_mail_count').html('(' + data.ucount + ')').show();
                    $('.dropdown .email_count').text(data.ucount).show();
                    $('.email_count').text(data.ucount).show();
                    $('#isEmailUrgent').val(data.mail_category);
                    <?php if ($url_segment1 == 'email' || $url_segment1 == 'email_new') { ?>
                        grab_emails();
                    <?php } ?>
                    if($("#check_click_unread").val() == "0") {
                        if($('#isEmailUrgent').val() == 'Y' || $('#isEmailUrgent').val() == 'y') {
                            toastr.options = {
                                timeOut: 0,
                                extendedTimeOut: 0,
                                "closeButton": true
                            };
                            Command: toastr["success"]("New Urgent Email Received, Please check your inbox..");
                            var ue = $("#urgentEmailBaseUrl").val();
                            notifyUrgentEmailBrowser(1, "Urgent Email Received", "Click to open", ue, "");
                        } else {
                            toastr.options = {
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                            };
                            Command: toastr["success"]("New Email Received, Please check your inbox..");
                        }
                        $.ajax({
                            url: NewUrl,
                            dataType: "json",
                            type: "POST",
                            success: function (e) {
                                e.count > 0 ? $(".email_count").html(e.count).removeClass("hidden") : $(".email_count").html(0).addClass("hidden");
                                var t = "",
                                    s = "";
                                $.each(e.emails, function (e, o) {
                                    s = null != o.whofrom_name && "" != o.whofrom_name ? o.whofrom_name : o.whofrom, t += "<li>", t += '<a href="' + HTTP_PATH + 'email" class="forMessageDrpdown">', t += '<div class="dropdown-messages-box">', t += '<div class="media-body">', t += '<small class="pull-right">' + /*moment(o.datesent).fromNow()+*/ "</small>", t += "New email from <strong>" + s + "</strong>. <br>", t += '<small class="text-muted">' + (o.caseno > 0 ? "<strong>Case No - " + o.caseno + "</strong>&nbsp;" : "") + /*moment(o.datesent).fromNow()+*/ " at " + moment(o.datesent).format("HH:mm a") + " - " + moment(o.datesent).format("DD.MM.YYYY") + "</small>", t += "</div>", t += "</div>", t += "</a>", t += "</li>", t += '<li class="divider"></li>'
                                }), t += "<li>", t += '<div class="text-center link-block">', t += '<a href="' + HTTP_PATH + 'email">', t += '<i class="fa fa-envelope"></i> <strong>Read All Messages</strong>', t += "</a>", t += "</div>", t += "</li>", $("ul.dropdown-messages").html(t)
                            }
                        });
                    } else {
                      $("#check_click_unread").val("0");
                    }
                }
                setTimeout(function () {
                   checkNewEmail();
                }, 4000);
            }
        });
    }

function isValidDate(dateStr) {
    var datePat = /^(\d{2,2})(\/)(\d{2,2})\2(\d{4}|\d{4})$/;

    var matchArray = dateStr.match(datePat);
    if (matchArray == null) {
        return false;
    }

    month = matchArray[1];
    day = matchArray[3];
    year = matchArray[4];
    if (month < 1 || month > 12) {
        return false;
    }
    if (day < 1 || day > 31) {
        return false;
    }
    if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
        return false;
    }
    if (month == 2) {
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isleap)) {
            return false;
        }
    }
    return true;
}


</script>
<script src="<?php echo base_url('assets'); ?>/js/notification.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgpQo9AEdow9TVwvI20si1T4P0s-EMIV8&libraries=places&callback=initAutocomplete" async defer></script>
</div>
</div>
</body>
</html>
