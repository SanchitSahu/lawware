<?php
include 'js/casecardjs.php';
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '') {
            unset($holodaylist[$key]);
        }
    }
}
?>
<style>
    .ui-datepicker-trigger{
        border:none;
        background:none;
        position: absolute;
        right: 6px;
        top: 5px;
    }
	.rdateerror
	{
		color:red;
	}
    .uk-dropdown {
        width: 100%;
    }
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .chosen-container {
        width: 100% !important;
    }
    .pac-container{z-index: 9999;}
    #addTaskForm label{font-weight:normal;}
    #addTaskForm1 label{font-weight: normal;}
    #addTaskForm1 .form-group {margin-bottom: 15px;}
    .container-checkbox{padding-bottom: 20px;}
</style>
<?php
$session_Arr = $this->session->userdata('user_data');
?>

<div id="addtask" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body">
                <form id="addTaskForm" name="addTaskForm" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <input type="hidden" name="caseno" id="caseNo" class="case_No" value="<?php echo $caseNo; ?>"/>
                                        <label class="col-sm-4 no-padding">From</label>
                                        <div class="col-sm-8 no-padding">
                                            <select class="form-control select2Class" name="whofrom" id="whofrom">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected="selected"' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-2 no-padding">To</label>
                                        <div class="col-sm-10 no-padding">
                                            <select class="form-control select2Class staffTO staffvacation" name="whoto2" onchange="return remove_error('whoto2')" id="whoto2">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected="selected"' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Finish By<span class="asterisk">*</span></label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" onchange="return remove_error('taskdatepicker')" id="taskdatepicker" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Color</label>
                                        <div class="col-sm-8 no-padding">
                                            <select class="form-control select2Class" name="color" id="color">
                                                <?php
                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                foreach ($jsoncolor_codes as $k => $color) {
                                                    ?>
                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Task Type</label>
                                        <div class="col-sm-8 no-padding">
                                            <select class="form-control select2Class" name="typetask">
                                                <option value="" selected="selected">Select Task Type</option>
                                                <option value=""></option>
                                                <option value="follow up">Follow Up</option>
                                                <option value="statute">Statute</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-padding">Priority</label>
                                        <div class="col-sm-8 no-padding">
                                            <select class="form-control select2Class" name="priority" id="priority">
                                                <option value="3">Low</option>
                                                <option value="2">Medium</option>
                                                <option value="1">High</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-padding">Priority</label>
                                        <div class="col-sm-9 no-padding">
                                            <select class="form-control select2Class" name="priority" id="priority">
                                                <option value="L">Low</option>
                                                <option value="M">Medium</option>
                                                <option value="H">High</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>-->
                                <!-- <div class="col-sm-6">
                                     <div class="form-group">
                                         <label class="col-sm-3 no-left-padding">Phase</label>
                                         <div class="col-sm-9 no-padding"><select class="form-control select2Class" name="phase">
                                                 <option value="">Select Phase</option>
                                                 <option value=""></option>
                                                 <option value="begining">Beginning</option>
                                                 <option value="middle">Middle</option>
                                                 <option value="final">Final</option>
                                             </select>
                                         </div>
                                     </div>
                                 </div>-->
                                <div class="clearfix">&nbsp;</div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task <span class="asterisk">*</span></label>
                                        <textarea class="form-control" name="event" style="height: 150px;resize: none;"></textarea>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <!-- <label class="">Reminder&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="reminder" ></label> -->
                                        <label class="container-checkbox pull-left">Reminder
                                            <input type="checkbox" class="treminder" name="reminder" onchange="showhidedt();">
                                            <span class="checkmark"></span> <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="You will get notified on your screen prior to 10 minutes of your scheduled task. To enable calender reminder notification in your browser to receive all notifications, allow notification in site settings." id="taskreminder"></i>&nbsp;&nbsp;<span class="rdateerror"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group">
                                        <label class="col-sm-3">Date</label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" name="rdate" class="rdate form-control" data-mask="99/99/9999" placeholder class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group">
                                        <label class="col-sm-3">Time</label>
                                        <div class="col-sm-9 no-padding uk-autocomplete">
                                            <input type="text" name="rtime" class="rtime form-control" class="form-control"  data-uk-timepicker="{format:'12h'}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <!-- <button type="button" class="btn btn-md btn-primary pull-left" id="saveToGroup">Save to Group</button> -->
                                        <button type="button" class="btn btn-md btn-primary" id="addTask" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
<!--                                        <span class="marg-top5 pull-right"><label>Email<input type="checkbox" class="i-checks" name="notify" ></label></span>-->
                                        <label class="container-checkbox pull-right marg-top5">&nbsp;Email
                                            <input type="checkbox" class="" name="notify" >
                                            <span class="checkmark"  style="padding-right: 5px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Add/Edit Tasks Modal -->

<!-- Edit case specific information -->

<div id="TaskCompleted" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Task Completed</h4>
            </div>
            <div class="modal-body">
                <form id="edit_case_details" method="post">
                    <div class="panel-body">
                        <div class="float-e-margins">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox-content">
                                        <div class="form-group">
                                            <textarea class="form-control" name="case_event" id="case_event" style="height: 100px;width: 100%;"></textarea>
                                            <input type='hidden' id='caseTaskId' name='caseTaskId'>
                                            <input type='hidden' id='caseno' class='caseNo' name='caseno' value="<?php echo $caseno; ?>">
                                            <input type='hidden' id='addnew' class='addnew' value="0">
                                        </div>
                                        <div class="form-group">
                                            <label>Enter any information to add to the  task to be posted to case activity</label>
                                            <textarea class="form-control" name="case_extra_event" id="case_extra_event" style="height: 100px;width: 100%;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class='col-sm-6'>
                    <select id="case_category" name="case_category" class="form-control select2Class">
                        <?php
                        /* $jsonCategory = json_decode(category);
                          foreach ($jsonCategory as $key => $option) {
                          echo '<option value=' . $key . '>' . $option . '</option>';
                          } */
                        ?>
                        <?php foreach ($caseactcategory as $k => $v) { ?>
                            <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class='col-sm-6'>
                    <div class="form-btn text-right">
                        <button type="submit" id="saveCompletedTask" class="btn btn-primary btn-md">Complete</button>
                        <button type="submit" id="saveCompletedTask" class="btn btn-primary btn-md">Complete and Add New</button>
                        <button type="button" class="btn btn-primary btn-md btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="Task-list" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span id="record_count"> </span>Task For PNC </h4>
                <a class="btn pull-right btn-primary btn-sm marg-left5" href="#addtask1" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Task</a>
                <a class="btn pull-right btn-primary btn-sm marg-left5" href="#printtask" data-backdrop="static" data-keyboard="false" data-toggle="modal">Print</a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="table-responsive">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content" style="overflow:auto;">
                                <table class="table table-bordered task-dataTable1">
                                    <thead>
                                        <tr>
                                            <th id="th-date" width="10%">Date</th>
                                            <th id="th-time" width="10%">Time</th>
                                            <th id="th-from" width="5%">From</th>
                                            <th id="th-to" width="5%">To</th>
                                            <th id="th-finishby" width="10%">Finish By</th>
                                            <th id="th-type" width="5%">Type</th>
                                            <th id="th-priority" width="5%">Priority</th>
                                            <th id="th-event" width="30%" class="caseact_event">Event</th>
                                            <th id="th-phase" width="5%">Phase</th>
                                            <th id="th-completed" width="10%">Completed</th>
                                            <th id="th-action" width="10%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="addtask1" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body" style="display:block;">
                <form id="addTaskForm1" name="addTaskForm1" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-4 col-xs-6">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <input type="hidden" name="caseno" id="caseNo" class="case_No" value="<?php echo $caseno; ?>"/>
                                        <label class="col-sm-4 no-padding">From</label>
                                        <div class="col-sm-8 no-padding">
                                            <select class="form-control select2Class" name="whofrom" id="whofrom">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-sm-2 no-padding">To</label>
                                        <div class="col-sm-10 no-padding">
                                            <select class="form-control select2Class" name="whoto3" id="whoto3">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Finish By <span class="asterisk">*</span></label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" id="taskdatepicker2" />
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Color</label>
                                        <div class="col-sm-8">
                                            <select class="form-control select2Class" name="color" id="color">
                                                <?php
                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                foreach ($jsoncolor_codes as $k => $color) {
                                                    ?>
                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 no-padding">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-padding">Task Type</label>
                                        <div class="col-sm-8 no-left-padding">
                                            <select class="form-control select2Class" name="typetask">
                                                <option value="">Select Task Type</option>
                                                <option value=""></option>
                                                <option value="follow up">Follow up</option>
                                                <option value="statute">Statute</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
								<div class="col-sm-4 no-padding">
                                    <div class="form-group">
                                        <label class="col-sm-4">Priority</label>
                                        <div class="col-sm-8 no-left-padding">
                                            <select class="form-control select2Class" name="priority" id="priority">
                                                <option value="3">Low</option>
                                                <option value="2">Medium</option>
                                                <option value="1">High</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-sm-4 no-padding">
                                    <div class="form-group">
                                        <label class="col-sm-3">Priority</label>
                                        <div class="col-sm-9 no-left-padding">
                                            <select class="form-control select2Class" name="priority" id="priority">
                                                <option value="L">Low</option>
                                                <option value="M">Medium</option>
                                                <option value="H">High</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>-->
                                <!--<div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Phase</label>
                                        <div class="col-sm-9 no-padding">
                                            <select class="form-control select2Class" name="phase">
                                                <option value="">Select Phase</option>
                                                <option value=""></option>
                                                <option value="begining">Beginning</option>
                                                <option value="middle">Middle</option>
                                                <option value="final">Final</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>-->

                                <div class="clearfix">&nbsp;</div>


                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task <span class="asterisk">*</span></label>
                                        <textarea class="form-control" name="event" style="height: 150px;resize: none;"></textarea>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <!--label class="">Reminder&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="reminder" ></label-->
                                            <label class="container-checkbox pull-left">Reminder
                                                <input type="checkbox" class="treminder" name="reminder1" onchange="showhidedt1();">
                                                <span class="checkmark"></span><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="You will get notified on your screen prior to 10 minutes of your scheduled task. To enable calender reminder notification in your browser to receive all notifications, allow notification in site settings." id="taskreminderedit"></i> &nbsp;&nbsp;<span class="rdateerror"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="row form-group">
                                            <label class="col-sm-3">Date</label>
                                            <div class="col-sm-9 no-padding">
                                                <input type="text" name="rdate" class="rdate1 form-control" data-mask="99/99/9999" placeholder class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="row form-group">
                                            <label class="col-sm-3">Time</label>
                                            <div class="col-sm-9 no-padding uk-autocomplete">
                                                <input type="text" name="rtime" id="rtimeup" class="rtime form-control" data-uk-timepicker="{format:'12h'}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <!-- <button type="button" class="btn btn-md btn-primary pull-left">Save to Group</button> -->
                                        <button type="button" class="btn btn-md btn-primary" id="addTask1" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
										<span class="marg-left5" id="pullcasebtn"></span>
<!--                                        <span class="marg-top5 pull-right"><label>Email&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="notify" ></label></span>-->
                                        <label class="container-checkbox pull-right marg-top5">&nbsp;Email
                                            <input type="checkbox" class="" name="notify" >
                                            <span class="checkmark"  style="padding-right: 5px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div></div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

<!-- End Add/Edit Tasks Modal -->
<div id="printtask" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Task</h4>
            </div>
            <div class="modal-body" style="display:inline-block;">
                <form id="taskPrint" name="taskPrint">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12" id="print_datepicker">
                                    <label>Days to Print</label>
                                    <div class="input-group date-rangepicker">
                                        <input type="text" id="start_printData" class="input-small form-control" name="start_printData" placeholder="From Date" readonly>
                                        <span class="input-group-addon">To</span>
                                        <input type="text" id="end_printData" class="input-small form-control" name="end_printData" placeholder="To Date" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Type of Report</label>
                                        <select class="form-control" name="print_type">
                                            <?php
                                            $repType = json_decode(print_type);
                                            foreach ($repType as $key => $val) {
                                                ?>
                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <span>
                                            <label>
                                                <input type="checkbox" name="namecheck" class="i-checks" />Include the name before each task
                                            </label>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center marg-top15">
                                        <button type="button" id="btn-taskPrint" class="btn btn-md btn-primary">Proceed</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="taskCompletePopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <span>Are you sure to Set Task as Completed?</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <?php $caseNo = $this->uri->segment(3); ?>
                        <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo; ?>">
                        <input id="taskNo" name="taskNo" type="hidden" value="">
                        <button id="complete" type="button" class="btn btn-primary">Complete</button>
                        <button id="completewithnew" type="button" class="btn btn-primary">Complete and Add New</button>
                        <button type="button" id="cncl" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start of Save to Group Modal -->
<div id="saveToGroupModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Save to Group</h4>
            </div>
            <div class="modal-body">
                <form id="saveToGroupForm" name="saveToGroupForm" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <input type="hidden" name="caseno" id="caseNo" class="case_No" value="<?php echo $caseNo; ?>"/>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Group</label>
                                        <div class="col-sm-8 no-padding">
                                            <select class="form-control select2Class" name="typeGroup" id="typeGroup">
                                                <option value="" selected="selected">Select Group</option>
                                                <?php foreach($groups as $key => $val) { ?>
                                                    <option value="<?php echo $val->group_id; ?>"><?php echo $val->group_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Group Memebers </label>
                                        <textarea class="form-control" name="groupMembers" id="groupMembers" style="height: 150px;resize: none;" disabled="disabled"></textarea>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary" id="addGroup" >Proceed</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End of Save to Group Modal -->

<div id="copyemail">
    <textarea id="copytoemail" STYLE="display:none;"></textarea>
</div>
<?php
$url_segment = $this->uri->segment(2);
if ($url_segment == 'tasks_new' || $url_segment == 'tasks_New') {
?>
<input type="hidden" name="taskmodule" id="taskmodule" value="task_new">
<?php }else if($url_segment == 'tasks'){ ?>
<input type="hidden" name="taskmodule" id="taskmodule" value="task">
<?php } ?>
<script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>
<script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $('#taskreminder').tooltip();
                                                        $('#taskreminderedit').tooltip();

                                                        var selectedArray = [];
                                                        $(".rdate1").datetimepicker({
                                                            format: "mm/dd/yyyy",
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });
                                                        $('.datepicker').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });

                                                        $('#datepicker').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: new Date()
                                                        });

                                                        $('.dob').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            endDate: '+0d'
                                                        });

                                                        $("#taskdatepicker").datepicker({
                                                            dateformat: "mm/dd/yyyy",
                                                            showOn: "button",
                                                            minDate: 0,
                                                            buttonImage: false,
                                                            buttonText: '<i class="fa fa-calendar" aria-hidden="true"></i>',
                                                            buttonImageOnly: false
                                                        });

                                                        $('#taskdatepicker2').datepicker({
                                                            dateformat: "mm/dd/yyyy",
                                                            showOn: "button",
                                                            minDate: 0,
                                                            buttonImage: false,
                                                            buttonText: '<i class="fa fa-calendar" aria-hidden="true"></i>',
                                                            buttonImageOnly: false
                                                        });

                                                        $('#caldate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            /*endDate: "+"+calYearRange+"y",*/
                                                            startDate: new Date()
                                                        });

                                                        $('.rdate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });

                                                        $('#start_printData').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true
                                                        }).on('changeDate', function (ev) {
                                                            $('#end_printData').datetimepicker('setStartDate', ev.date);
                                                        });

                                                        $('#end_printData').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true
                                                        }).on('changeDate', function (ev) {
                                                            $('#start_printData').datetimepicker('setEndDate', ev.date);
                                                        });

                                                        $('#taskdatepicker').on('blur', function () {
                                                            var today = new Date();
                                                            var ad = $(this).val();
                                                            if (isValidDate(ad)) {
                                                                $('#taskdatepicker').val(moment(ad).format('MM/DD/YYYY'));
                                                                $('.weekendsalert').trigger( "click" );
                                                            } else {
                                                                var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                                                                $('#taskdatepicker').val(moment(new_date).format('MM/DD/YYYY'));
                                                                /*$('.weekendsalert').trigger("change");*/
                                                            }

                                                        });

                                                        $('#taskdatepicker2').on('blur', function () {
                                                            var today = new Date();
                                                            var ad = $(this).val();
                                                            if (isValidDate(ad))
                                                            {
                                                                $('#taskdatepicker2').val(moment(ad).format('MM/DD/YYYY'));
                                                                $('.weekendsalert').trigger( "click" );
                                                            } else
                                                            {
                                                                var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                                                                $('#taskdatepicker2').val(moment(new_date).format('MM/DD/YYYY'));
                                                                /*$('.weekendsalert').trigger("change");*/
                                                            }
                                                        });

                                                        $('select[name=print_type]').on('change', function () {
                                                            if (this.value == 4 || this.value == 5) {
                                                                $('#print_datepicker').show();
                                                            } else {
                                                                $('#start_printData').val('').datetimepicker("update");
                                                                $('#end_printData').val('').datetimepicker("update");
                                                            }
                                                        });

                                                        $('#btn-taskPrint').on('click', function () {
                                                            $.ajax({
                                                                url: '<?php echo base_url() . 'tasks/printTask' ?>',
                                                                method: "POST",
                                                                data: $('form#taskPrint').serialize() + '&searchBy=' + JSON.stringify(searchBy) + '&selectedData=' + selectedArray,
                                                                success: function (result) {
                                                                    $(result).printThis({
                                                                        debug: false,
                                                                        header: "<h2 style='margin-top: 20px;'><center><?php echo APPLICATION_NAME; ?> | Tasks </center></h2>"
                                                                    });
                                                                }
                                                            });
                                                        });

                                                        var d = new Date();
                                                        var hours = d.getHours();
                                                        var minutes = d.getMinutes();
                                                        var ampm = hours >= 12 ? 'PM' : 'AM';
                                                        var currenttime = d;

                                                        if(d.getHours() > 12) {
                                                            d.setHours(d.getHours() - 12);
                                                        }
                                                        if(d.getHours() < 10) {
                                                            hours = '0' + d.getHours();
                                                        } else {
                                                            hours = d.getHours();
                                                        }
                                                        if(d.getMinutes() < 10) {
                                                            minutes = '0' + d.getMinutes();
                                                        } else {
                                                            minutes = d.getMinutes();
                                                        }
                                                        var currenttime = hours+":"+minutes+" "+ampm;
                                                        $('#caltime').val(currenttime);

                                                        /*var input1 = $('#caltime');
                                                        input1.mdtimepicker({
                                                            autoclose: true
                                                        });*/

                                                        /*$('.rtime').mdtimepicker({
                                                            autoclose: true
                                                        });*/

                                                        /*$('#oletime').mdtimepicker({
                                                            autoclose: true,
                                                            'default': currenttime
                                                        });*/
                                                        $('#oletime').val(currenttime);
                                                        $('#tickledate').datetimepicker({
                                                            format: 'mm/dd/yyyy',
                                                            minView: 2,
                                                            autoclose: true,
                                                            startDate: new Date()
                                                        });

                                                        /*$('#tickletime').mdtimepicker({
                                                            autoclose: true
                                                        });*/

                                                        jQuery.validator.addMethod("greaterThan", function (c, b, d) {
                                                            if ($(d).val() != "") {
                                                                if (!/Invalid|NaN/.test(new Date(c))) {
                                                                    return new Date(c) >= new Date($(d).val());
                                                                }
                                                                return isNaN(c) && isNaN($(d).val()) || (Number(c) >= Number($(d).val()));
                                                            } else {
                                                                return true;
                                                            }
                                                        }, "Must be greater than Reminder Date.");
										
                                                        var a = $("#addTaskForm1").validate({
                                                            ignore: [],
                                                            rules: {
                                                                whofrom: {
                                                                    required: true
                                                                },
                                                                whoto3: {
                                                                    required: true
                                                                },
                                                                datereq: {
                                                                    required: true,
                                                                    greaterThan: ".rdate1"
                                                                },
                                                                event: {
                                                                    required: true,
                                                                    noBlankSpace: true
                                                                }
                                                            }
                                                        });
                                                        /* taskTable();*/

                                                        $("#addtask").on("show.bs.modal", function () {
                                                            <?php if (!empty($caseNo)) { ?>
                                                                $(".case_No").val("<?php echo $caseNo; ?>");
                                                            <?php } ?>
                                                            /*$(".select2Class#whoto2").val('<?php echo $this->session->userdata('user_data')['initials'];?>').trigger("change");*/
                                                            $(".rtime").val('');
                                                            $(".select2Class#whoto").val("AEB").trigger("change");
                                                            $(".select2Class#priority").val("2").trigger("change");
                                                            $(".select2Class#color").val("1").trigger("change");
                                                            
                                                        });

                                                        $("#addtask.modal").on("hidden.bs.modal", function () {
                                                            $("#addtask h4.modal-title").text("Add a Task");

                                                            $("#addTaskForm")[0].reset();
                                                            $(".select2Class#priority").val("2").trigger("change");
                                                            $(".select2Class#color").val("1").trigger("change");
                                                            /*$("select[name=color]").select2('val', '1');*/

                                                            $("select[name=typetask]").select2('val', 'Task Type');
                                                            $("select[name=typetask]").val('').trigger("change");

                                                            $("select[name=phase]").select2('val', 'Select Phase');
                                                            $("select[name=phase]").val('').trigger("change");

                                                            $("select[name=phase]").val('').trigger("change");

                                                          /* $(".select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");*/
                                                            $(".select2Class#whoto2").val('<?php echo $this->session->userdata('user_data')['initials'];?>').trigger("change");
                                                            var b = setTaskValidation();
                                                            $("input[name=taskid]").val(0);
                                                            $('#addtask a[href="#name"]').tab("show");
                                                        });

                                                        setTaskValidation();
                                                        $(".task-dataTable tbody").on("click", "tr", function () {
                                                            if ($(this).hasClass("table-selected")) {
                                                                $(this).removeClass("table-selected");
                                                                selectedArray.splice($.inArray(this.id, selectedArray), 1);
                                                            } else {
                                                                selectedArray.push(this.id);
                                                            }
                                                        });
                                                    });

                                                    $(document.body).on('click', '#saveToGroup', function() {
                                                        $('#saveToGroupModal').modal("show");
                                                    });

                                                    $(document.body).on('change', '#typeGroup', function() {
                                                        var selected_group = $('#typeGroup').val();
                                                        $.ajax({
                                                            url: "<?php echo base_url(); ?>tasks/getGroupMembers",
                                                            dataType: 'json',
                                                            type: 'POST',
                                                            data: {
                                                                group_id: selected_group
                                                            },
                                                            beforeSend: function (xhr) {
                                                                $.blockUI();
                                                            },
                                                            complete: function (jqXHR, textStatus) {
                                                                $.unblockUI();
                                                            },
                                                            success: function (result) {
                                                                var html = '';
                                                                for(var i = 0; i < result.length; i++) {
                                                                    html += result[i].initials;
                                                                    if(result.length > 1 && i != result.length-1) {
                                                                        html += ','
                                                                    }
                                                                }
                                                                $('#groupMembers').text(html);
                                                            }
                                                        });
                                                    });

                                                    $(document.body).on('click', '#complete', function () {
                                                        var taskId = $('#taskNo').val();
                                                        var c = $('#' + taskId).attr("data-taskid");
                                                        var d = $('#' + taskId).attr("data-event");
                                                        var b = $('#' + taskId).attr("data-caseno");
                                                        $("#taskCompletePopup").modal("hide");
                                                        $("#case_event").val(d);
                                                        $("#caseTaskId").val(taskId);
                                                        $("#caseno").val(b);
                                                        $("#addnew").val(0);
                                                        newCompletedTask();
                                                    });

                                                    $('#TaskCompleted.modal').on('show.bs.modal', function () {
                                                        $('#case_extra_event').val('');
                                                    });
                                                    $(document.body).on('click', '#completewithnew', function () {
                                                        $("#taskCompletePopup").modal("hide");
                                                        var taskId = $('#taskNo').val();
                                                        var c = $('#' + taskId).attr("data-taskid");
                                                        var d = $('#' + taskId).attr("data-event");
                                                        var b = $('#' + taskId).attr("data-caseno");
                                                        $("#case_event").val(d);
                                                        $("#caseTaskId").val(taskId);
                                                        $("#caseno").val(b);
                                                        $("#addnew").val(1);
                                                        newCompletedTask();
                                                    });

                                                    $('#TaskCompleted.modal').on('show.bs.modal', function () {
                                                        $('#case_extra_event').val('');
														 $(".select2Class#case_category").val("1").trigger("change");
													
                                                    });

                                                    $('#getPriority').on('change', function () {
                                                        var selectedValue = $(this).val();
                                                        taskTable.columns(5).search(JSON.stringify({'priority': selectedValue})).draw();
                                                    });

                                                    $('#gettaskStatus').on('change', function () {
                                                        var taskStatus = $(this).val();
                                                        taskTable.columns(8).search(JSON.stringify({'task-status': taskStatus})).draw();
                                                    });

                                                    $(document.body).on("click", "#saveCompletedTask", function () {
                                                        if ($(this).text() == 'Complete and Add New')
                                                            $("#addnew").val(1);
                                                        else
                                                            $("#addnew").val(0);
                                                        /*$('#saveCompletedTask').attr('disabled', 'disabled');*/
                                                        var e = $("#caseTaskId").val();
                                                        var a = $("#case_event").val();
                                                        var d = $("#case_extra_event").val();
                                                        var b = $("#case_category").val();
                                                        var c = $("#caseno").val();
                                                        var n = $("#addnew").val();
                                                        $(".case_No").val(c);
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: "<?php echo base_url(); ?>tasks/completedTaskWithCase",
                                                            data: {
                                                                taskId: e,
                                                                case_event: a,
                                                                case_extra_event: d,
                                                                case_category: b,
                                                                caseno: c
                                                            },
                                                            method: "POST",
                                                            success: function (f) {
                                                                $.unblockUI();
                                                                var g = $.parseJSON(f);
                                                                taskTable.ajax.reload(null, false);
                                                                if (g.status == 1) {
                                                                    swal({
                                                                        title: "Success!",
                                                                        text: g.message,
                                                                        type: "success"
                                                                    }, function () {
                                                                        if (n == 1) {
                                                                            $('#addtask').modal("show");
                                                                        }
                                                                    });
                                                                    $("#addnew").val('0');
                                                                    $('#saveCompletedTask').removeAttr("disabled");
                                                                } else if (g.status == 2) {
                                                                    if (n == 0) {
                                                                        swal({
                                                                            title: "Warning!",
                                                                            text: '<?php echo AddNewTaskCase; ?>',
                                                                            type: "warning",
                                                                            showCancelButton: true,
                                                                            confirmButtonColor: "#DD6B55",
                                                                            confirmButtonText: "Add new",
                                                                            closeOnConfirm: true
                                                                        }, function () {
                                                                            $('#addtask').modal("show");
                                                                        });
                                                                    } else if (n == 1) {
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: g.message,
                                                                            type: "success"
                                                                        }, function () {
                                                                            if (n == 1) {
                                                                                $('#addtask').modal("show");
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                                $("#TaskCompleted").modal("hide");
                                                            }
                                                        });
                                                    });

                                                    function newCompletedTask() {
                                                        var e = $("#caseTaskId").val();
                                                        var a = $("#case_event").val();
                                                        var d = $("#case_extra_event").val();
                                                        var b = $("#case_category").val();
                                                        var c = $("#caseno").val();
                                                        var n = $("#addnew").val();
                                                        $(".case_No").val(c);
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: "<?php echo base_url(); ?>tasks/completedTaskWithCase",
                                                            data: {
                                                                taskId: e,
                                                                case_event: a,
                                                                case_extra_event: d,
                                                                case_category: b,
                                                                caseno: c
                                                            },
                                                            method: "POST",
                                                            success: function (f) {
                                                                $.unblockUI();
                                                                taskTable.ajax.reload(null, false);

                                                                var g = $.parseJSON(f);
                                                                if (g.status == 1) {
                                                                    swal({
                                                                        title: "Success!",
                                                                        text: g.message,
                                                                        type: "success"
                                                                    }, function () {
                                                                        if (n == 1) {
                                                                            $('#addtask').modal("show");
                                                                        }
                                                                    });
                                                                    $("#addnew").val('0');
                                                                    $('#saveCompletedTask').removeAttr("disabled");
                                                                } else if (g.status == 2) {
                                                                    if (n == 0) {
                                                                        swal({
                                                                            title: "Warning!",
                                                                            text: '<?php echo AddNewTaskCase; ?>',
                                                                            type: "warning",
                                                                            showCancelButton: true,
                                                                            confirmButtonColor: "#DD6B55",
                                                                            confirmButtonText: "Add new",
                                                                            closeOnConfirm: true
                                                                        }, function () {
                                                                            $('#addtask').modal("show");
                                                                        });
                                                                    } else if (n == 1) {
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: g.message,
                                                                            type: "success"
                                                                        }, function () {
                                                                            if (n == 1) {
                                                                                $('#addtask').modal("show");
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                                $("#TaskCompleted").modal("hide");
                                                            }
                                                        });
                                                    }

                                                    function setTaskValidation() {
                                                        var a = $("#addTaskForm").validate({
                                                            ignore: [],
                                                            rules: {
                                                                whofrom: {
                                                                    required: true
                                                                },
                                                                whoto2: {
                                                                    required: true
                                                                },
                                                                datereq: {
                                                                    required: true,
                                                                    greaterThan: ".rdate"
                                                                },
                                                                event: {
                                                                    required: true,
                                                                    noBlankSpace: true
                                                                }
                                                            }
                                                        });
                                                        return a;
                                                    }

                                                    function deleteTask(a) {
                                                        swal({
                                                            title: "Alert",
                                                            text: "Are you sure to Remove this Task? ",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, delete it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            $.blockUI();
                                                            $.ajax({
                                                                url: "<?php echo base_url(); ?>tasks/deleteRec",
                                                                data: {
                                                                    taskId: a
                                                                },
                                                                method: "POST",
                                                                success: function (b) {
                                                                    $.unblockUI();
                                                                    var c = $.parseJSON(b);
                                                                    if (c.status == 1) {
                                                                        taskTable.ajax.reload(null, false);
                                                                        swal("Success !", c.message, "success");
                                                                    } else {
                                                                        swal("Oops...", c.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }

                                                    function completeTask(a) {
														var taskId = $('#' + a).attr("data-id");
                                                        var c = $('#' + taskId).attr("data-taskid");
                                                        var d = $('#' + taskId).attr("data-event");
                                                        var b = $('#' + taskId).attr("data-caseno");
                                                        /*$("#taskCompletePopup").modal("hide");*/
                                                        $("#case_event").val(d);
                                                        $("#caseTaskId").val(taskId);
                                                        $("#caseno").val(b);
                                                        $('#taskNo').val(taskId);
                                                        $("#TaskCompleted").modal("show");
                                                        /* $("#taskCompletePopup").modal("show");*/
                                                        return false;
                                                    }

                                                    function completeTask1(a) {
                                                        var taskId = a;
                                                        var c = $('#' + taskId).attr("data-taskid");
                                                        var d = $('#' + taskId).attr("data-event");
                                                        var b = $('#' + taskId).attr("data-caseno");
                                                        /*$("#taskCompletePopup").modal("hide");*/
                                                        $("#case_event").val(d);
                                                        $("#caseTaskId").val(taskId);
                                                        $("#caseno").val(b);
                                                        $('#taskNo').val(taskId);
                                                        /*$("#TaskCompleted").modal("show");*/
                                                        $("#taskCompletePopup").modal("show");
                                                        return false;
                                                    }

                                                    function editTask(a) {
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: "<?php echo base_url(); ?>tasks/getTask",
                                                            data: {
                                                                taskId: a
                                                            },
                                                            method: "POST",
                                                            success: function (b) {
                                                                var c = $.parseJSON(b);
                                                                $.unblockUI();
                                                                if (c.status == 0) {
                                                                    swal("Oops...", c.message, "error");
                                                                } else {
                                                                    $("#addtask1 .modal-title").html("Edit Task");
                                                                    $("input[name=taskid]").val(a);
                                                                    $("input[name=caseno]").val(c.caseno);
                                                                    $("select[name=whofrom]").val(c.whofrom);
                                                                    $("select[name=whoto3]").val(c.whoto);
                                                                    $("input[name=datereq]").val(c.datereq);
                                                                    $("textarea[name=event]").val(c.event);
                                                                    $("select[name=color]").val(c.color);
                                                                    $("#addtask1 select[name=priority]").val(c.priority);
                                                                    $("select[name=typetask]").val(c.typetask);
                                                                    $("select[name=phase]").val(c.phase);
                                                                    if(c.caseno > 0 && c.caseno != '')
                                                                    { $('#pullcasebtn').html('<a title="Pull Case" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + c.caseno + '"><button type="button" class="btn btn-md btn-primary">Pull Case</button></a>'); }
                                                                    else{ $('#pullcasebtn').html('');}
                                                                    if (c.notify == 1) {
                                                                        $("input[name=notify]").iCheck("check");
                                                                    } else {
                                                                        $("input[name=notify]").iCheck("uncheck");
                                                                    }
                                                                    if (c.reminder == 1) {
                                                                        $('input[type="checkbox"][name="reminder1"]').prop("checked", true).change();
                                                                        $("input[name=rdate]").val(c.popupdate);
                                                                        $("input[name=rtime]").val(c.popuptime);
                                                                        $(".rmdt").show();
                                                                    } else {
                                                                        $('input[type="checkbox"][name="reminder1"]').prop("checked", false).change();
                                                                        $(".rmdt").hide();
                                                                    }
                                                                    $("#addtask1").modal({
                                                                        backdrop: "static",
                                                                        keyboard: false
                                                                    });
                                                                    $("#addtask1").modal("show");
                                                                    $('.rmdt').css('display','none');
                                                                    $("#addTaskForm1 select").trigger("change");
                                                                }
                                                            }
                                                        });
                                                    }

                                                    $("#addtask").on("hidden.bs.modal", function () {
                                                        $(this).find("input,textarea").each(function () {
                                                            this.value = "";
                                                            $(this).iCheck("uncheck");
                                                            $(this).trigger("change");
                                                        });
                                                    });

                                                    $(document.body).on("click", "#addtaskModal", function () {
                                                        $(".select2Class#whofrom").val("<?php echo isset($loginUser) ? $loginUser : ''; ?>").trigger("change");
                                                        $(".select2Class#whoto").val("AEB").trigger("change");
                                                        $(".select2Class#priority").val("2").trigger("change");
                                                        $(".select2Class#color").val("1").trigger("change");
                                                        $("#addtask").modal({
                                                            backdrop: "static",
                                                            keyboard: false
                                                        });
                                                        $("#addtask").modal("show");
                                                    });

                                                    $(document).on("click", ".task_list", function () {
                                                        var a = $(this).attr("data-proskey_id");
                                                        searchBy1.prosno = a;
                                                        $("#Task-list").modal("show");
                                                    });

                                                    $(document.body).on("click", "#addTask1", function () {
                                                        if ($('input[type="checkbox"][name="reminder1"]').is(":checked") == true) {
                                                        if($('.rdate1').val()=='' || $('#rtimeup').val()=='')
                                                            {
                                                                    $('.rdateerror').text('Please select Reminder date and time');
                                                                    return false;
                                                            }
                                                        } 
                                                        if ($("#addTaskForm1").valid()) {
                                                            $.blockUI();
                                                            var a = "";
                                                            var chktmodule = $('#taskmodule').val();
                
                                                            if ($('input[name=taskid]').val() != '' && $('input[name=taskid]').val() != 0) {
                                                                if(chktmodule == 'task_new'){
                                                                    a = '<?php echo base_url('tasks_new/editTask'); ?>';
                                                                }else{
                                                                    a = '<?php echo base_url('tasks/editTask'); ?>';
                                                                }
                                                            } else {
                                                                if(chktmodule == 'task_new'){
                                                                    a = '<?php echo base_url('tasks_new/addTask'); ?>';
                                                                }else{
                                                                    a = '<?php echo base_url('tasks/addTask'); ?>';
                                                                }
                                                            }
                                                            
                                                            $.ajax({
                                                                url: a,
                                                                method: "POST",
                                                                data: $("#addTaskForm1").serialize() + "&prosno=" + $(".task_list").attr("data-proskey_id"),
                                                                success: function (b) {
                                                                    var c = $.parseJSON(b);
                                                                    $.unblockUI();
                                                                    if (c.status == "1") {
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: c.message,
                                                                            type: "success"
                                                                        }, function () {
                                                                            $("#addTaskForm1")[0].reset();
                                                                            $("#addtask1").modal("hide");
                                                                            $("#addtask1 .modal-title").html("Add a Task");
                                                                            /*taskTable.draw(false);*/
                                                                            taskTable.draw();
                                                                        });
                                                                        /*$('.whoto').removeClass("active");
                                                                        $('.whofrom').addClass("active");*/
                                                                    } else {
                                                                        swal("Oops...", c.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });

                                                    $("#addtask1.modal").on("hidden.bs.modal", function () {
                                                        $("#addtask1 h4.modal-title").text("Add a Task");
                                                        $("#addTaskForm1")[0].reset();
                                                        var a = setTaskValidation();
                                                        $("label.error").remove();
                                                        $(this).find('input[type="text"],input[type="email"],textarea').each(function () {
                                                            this.value = "";
                                                            $(this).removeClass("error");
                                                        });
                                                        $(this).find("input,textarea").each(function () {
                                                            this.value = "";
                                                            $(this).iCheck("uncheck");
                                                            $(this).trigger("change");
                                                        });
                                                        $("#addTaskForm")[0].reset();
                                                        $("input[name=taskid]").val(0);
                                                        $('#addtask1 a[href="#name"]').tab("show");
                                                    });

                                                    function remove_error(id) {
                                                        var myElem = document.getElementById(id + '-error');
                                                        if (myElem === null) {

                                                        } else {
                                                            document.getElementById(id + '-error').innerHTML = '';
                                                        }
                                                    }

                                                    function showhidedt1() {
                                                        if ($('input[type="checkbox"][name="reminder1"]').is(":checked") == true) {
                                                            $('input[type="checkbox"][name="reminder1"]').val("on");
                                                            $(".rmdt1").show();
                                                        } else {
                                                            $(".rmdt1").hide();
                                                            $(".rdate1").val("");
															$('.rdateerror').text('');
                                                            $(".rtime").val("");
                                                        }
                                                    }

                                                    function showhidedt() {
                                                        if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
                                                            $('input[type="checkbox"][name="reminder"]').val("on");
                                                            $(".rmdt").show();
                                                        } else {
                                                            $(".rmdt").hide();
                                                            $('.rdateerror').text('');
                                                            $(".rdate").val("");
                                                            $(".rtime").val("");
                                                        }
                                                    }

                                                    function taskTable() {
                                                        taskTable = $(".task-dataTable").DataTable({
                                                            processing: true,
                                                            serverSide: true,
                                                            stateSave: true,
															stateSaveParams: function (settings, data) {
																data.search.search = "";
																data.start = 0;
																delete data.order;
																data.order = [4, "ASC"];
															},
                                                            bFilter: true,
                                                            order: [
                                                                [4, "ASC"]
                                                            ],
                                                            autoWidth: false,
                                                            ajax: {
                                                                url: "<?php echo base_url(); ?>tasks/getTasksData",
                                                                type: "POST",
                                                                data: function (d) {
                                                                    var c = $("ul#task-navigation").find("li.active").attr("id");
                                                                    if ($("#task_reminder").is(":checked") == true) {
                                                                        var b = 1;
                                                                    } else {
                                                                        var b = "";
                                                                    }
                                                                    var e = {
                                                                        caseno: $(".case_No").val()
                                                                    };
                                                                    e.taskBy = c;
                                                                    e.reminder = b;
                                                                    $.extend(d, e);
                                                                }
                                                            },
                                                            fnRowCallback: function (f, e, d, c) {
                                                                $(f).attr("id", e[10]);
                                                                $(f).attr("data-id", e[10]);
                                                                $(f).attr("data-event", e[1]);
                                                                $(f).attr("data-caseno", e[12]);

                                                                if (e[e.length - 2] != "") {
                                                                    var b = e[e.length - 2].split("-");
                                                                    if (b[0] != "") {
                                                                        $(f).css({
                                                                            "background-color": b[0],
                                                                            color: b[1]
                                                                        });
                                                                    } else {
                                                                        $(f).css({
                                                                            "background-color": "#ffffff",
                                                                            color: "#000000"
                                                                        });
                                                                    }
                                                                }
                                                            },
                                                            columnDefs: [{
                                                                    render: function (e, c, f) {
                                                                        if (e != null) {
                                                                            var b = "";
                                                                            var d = "";
                                                                            if (f[8] == "") {
                                                                                if (f[3] === "<?php echo isset($username) ? $username : $this->session->userdata('user_data')['username']; ?>") {
                                                                                    d = '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + e + ')"><i class="fa fa-trash"></i></button>';
                                                                                }
                                                                                b = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + e + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;' + d;
                                                                                if (f[12] != 0) {
                                                                                    b += '<button class="btn btn-primary btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + e + ')"><i class="fa fa-check"></i></button>';
                                                                                    b += '&nbsp;&nbsp;<a class="btn btn-primary btn-circle-action btn-circle" title="Pull Case" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + f[11] + '"><i class="fa fa-folder-open"></i></a>';
                                                                                }
                                                                                if (f[12] == 0)
                                                                                {
                                                                                    b += '&nbsp;&nbsp;<button class="btn btn-primary btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask1(' + e + ')"><i class="fa fa-check"></i></button>';
                                                                                }
                                                                            }
                                                                        }
                                                                        return b;
                                                                    },
                                                                    targets: 9,
                                                                    bSortable: false,
                                                                    bSearchable: false
                                                                },
                                                                {
                                                                    "targets": 1,
                                                                    "className": "text-center",
                                                                },
                                                                {
                                                                    "targets": 3,
                                                                    "className": "text-center",
                                                                },
                                                                {
                                                                    targets: 7,
                                                                    className: "textwrap-line"
                                                                }]
                                                        });
                                                    }

                                                    $(document.body).on('change', '.weekendsalert', function () {
                                                        var weekalert = "<?php echo isset($holidayset->weekends) ? $holidayset->weekends : 0 ?>";
                                                        var aletholi = $(this).val();
                                                        var aftholiday = "<?php echo isset($holidayset->aftholiday) ? $holidayset->aftholiday : 0 ?>";
                                                        if (weekalert != 0 && weekalert == 'on') {
                                                            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                                            var a = new Date();
                                                            var ad = $(this).val();
                                                            if(isValidDate(ad) == false) {
                                                                a.setDate(parseInt(a.getDate())+parseInt($(this).val()));
                                                            } else {
                                                                a = new Date($(this).val());
                                                            }
                                                            if (weekday[a.getDay()] == 'Saturday' || weekday[a.getDay()] == 'Sunday')
                                                            {
                                                                swal({
                                                                    title: "",
                                                                    text: "You are trying to add a task on non business day. Are you sure?",
                                                                    customClass: 'swal-wide',
                                                                    confirmButtonColor: "#1ab394",
                                                                    confirmButtonText: "OK"
                                                                });
                                                            }
                                                        } else if (aftholiday != 0 && aftholiday == 'on' && $(this).val() != '') {
                                                            var holidaylist = '<?php echo isset($holodaylist) ? json_encode($holodaylist) : 0; ?>';
                                                            if (JSON.parse(holidaylist).length > 0) {
                                                                $.each(JSON.parse(holidaylist), function (key, value) {
                                                                    var temp = new Array();
                                                                    temp = value.split(",");
                                                                    var c = new Date(temp[0]);
                                                                    var b = new Date(aletholi);
                                                                    if (c.getTime() == b.getTime()) {
                                                                        swal({
                                                                            title: "",
                                                                            text: "You are trying to add a task on non business day. Are you sure?",
                                                                            customClass: 'swal-wide',
                                                                            confirmButtonColor: "#1ab394",
                                                                            confirmButtonText: "OK"
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });

$('.rdate').on('show.bs.modal', function (e) {
    if (e.stopPropagation) e.stopPropagation();
});
</script>
