<?php
if (isset($Prospect_data->birthdate) && $Prospect_data->birthdate != '0000-00-00 00:00:00') {
    $birthDate = date('d/m/Y', strtotime(isset($Prospect_data->birthdate) ? $Prospect_data->birthdate : 0));
    $birthDate = explode("/", $birthDate);
    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
} else {
    $birthDate = '';
    $age = '';
}
$filesudf = isset($Prospect_data->fieldsudf) ? $Prospect_data->fieldsudf : '';
$filesudf = explode(',', $filesudf);

if (isset($caseId) && !empty($caseId)) {
    $styl = 'style="display:none"';
} else {
    $styl = '';
}
?>

<style type="text/css" media="print">
    #new_table th{
        font-size: 16px;
        border-bottom: 1px solid #000;
    }
    .preload {
        position : absolute;
        top      : -9999px;
        height   : 1px;
        width    : 1px;
        overflow : hidden;
    }

    #Task-list {padding-top: 0px;}
    #Task-list .modal-dialog{margin-top: 5px;}
    #Task-list .modal-body{padding: 10px 30px 0px;}
</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Potential New Client (PNC) </span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-xs-9">
                            <h5 style="margin-right: 10px;">Potential New Client (PNC) <span id="pnctopinfo">- <?php if($record[0]->caseno){ echo 'Case '.$record[0]->caseno;}?> - <?php echo isset($Prospect_data->salutation) ? $Prospect_data->salutation : '' ?> <?php echo isset($Prospect_data->first) ? $Prospect_data->first : ''; ?> <?php echo isset($Prospect_data->last) ? $Prospect_data->last : ''; ?></span></h5>
                        </div>
                        <div class="col-xs-3 text-right marg-bot5">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#" <?php echo $styl; ?> class="btn btn-primary btn-sm marg-right5 next_privous_list Privous" title="Previous" data-type="previous" data-id="<?php echo isset($record[0]->prospect_id) ? $record[0]->prospect_id : ''; ?>"><span class="icon fa fa-angle-left"></span></a>
                            <a href="#" <?php echo $styl; ?> class="btn btn-primary btn-sm marg-right5 next_privous_list Next" title="Next" data-type="next" data-id="<?php echo isset($record[1]->prospect_id) ? $record[1]->prospect_id : ''; ?>" ><span class="icon fa fa-angle-right"></span></a>
                            <a href="#printProspectmodal" <?php echo $styl; ?> class="btn btn-primary btn-sm  marg-right5" data-toggle="modal" title="Search" onclick="changePrintProspectModalHeader('search')"><i class="icon fa fa-search fa-1x"></i></a>
                            <a href="#" class="btn btn-primary btn-sm  marg-right5 task_list" data-proskey_id="<?php echo isset($record[0]->proskey) ? $record[0]->proskey : ''; ?>" data-toggle="modal" title="Tasks">Tasks</a>
                            <span class="prospect_status"  style="color:red"><?php echo isset($record[0]->status) ? $record[0]->status == 1 ? 'Deleted' : "" : ''; ?></span>
                        </div>
                        <div class="col-xs-6">
                            <a href="#addprospects" <?php echo $styl; ?> class="btn btn-primary btn-sm pull-right" data-toggle="modal" title="Add PNC">PNC</a>
                            <a href="#printenvelope" class="btn btn-primary btn-sm pull-right marg-right5" data-toggle="modal" title="Print Envelope">Print Envelope</a>
                            <a id="printProspectdetails" <?php echo $styl; ?>  class=" btn btn-primary  btn-sm pull-right marg-right5" data-toggle="modal" title="Print"  onclick="changePrintProspectModalHeader('print')"><i class="icon fa fa-print fa-1x"></i>  Print</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-content pnc-list">
                    <div class="row ">
                        <div class="col-lg-6">
                            <div class="ibox-title">
                                <h5>PNC Details</h5>
                                <?php if (isset($record[0]->prospect_id) && $record[0]->prospect_id > 0) { ?>
                                    <a href="#" data-status="<?php echo isset($record[0]->status) ? $record[0]->status : ''; ?>"  title="Delete" data-pros_id="<?php echo isset($record[0]->prospect_id) ? $record[0]->prospect_id : ''; ?>" class="pull-right delete_prospect btn btn-sm btn-danger marg-left5">Delete</a>
                                    <a href="#" data-case_no="<?php echo isset($record[0]->caseno) ? $record[0]->caseno : ''; ?>" title="Edit" data-prospect_id="<?php echo isset($record[0]->prospect_id) ? $record[0]->prospect_id : ''; ?>" class="pull-right marg-left5 edit_prospect btn btn-sm btn-primary marg-left5">Edit&nbsp;&nbsp;</a>
                                <?php } ?>
                            </div>
                            <div class="ibox-content marg-bot15 party">
                                <form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12 prospect_detail">
                                                <div class="table-responsive ">
                                                    <table class="table no-bottom-margin" >
                                                        <tbody>
                                                            <tr >
                                                                <td class="text-right" width="120"><strong>Name :</strong></td>
                                                                <td class="pro_first"><?php echo isset($Prospect_data->salutation) ? $Prospect_data->salutation : '' ?> <?php echo isset($Prospect_data->first) ? $Prospect_data->first : ''; ?> <?php echo isset($Prospect_data->last) ? $Prospect_data->last : ''; ?></td>
                                                                <td class="pro_first1" hidden><?php echo $Prospect_data->first; ?></td>
                                                                <td class="pro_last1" hidden><?php echo $Prospect_data->last; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Cell Phone :</strong></td>
                                                                <td class="pro_cell"><?php echo isset($Prospect_data->cell) ? $Prospect_data->cell : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Home Phone :</strong></td>
                                                                <td class="pro_home"><?php echo isset($Prospect_data->home) ? $Prospect_data->home : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Work Phone :</strong></td>
                                                                <td class="pro_business"><?php echo isset($Prospect_data->business) ? $Prospect_data->business : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Address :</strong></td>
                                                                <td class="pro_address"><?php echo isset($Prospect_data->address1) ? $Prospect_data->address1 : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>City,St,Zip :</strong></td>
                                                                <td class="pro_city_st_zip"><?php echo isset($Prospect_data->city) ? $Prospect_data->city : ''; ?>&nbsp;&nbsp;<?php echo isset($Prospect_data->state) ? $Prospect_data->state : ''; ?>&nbsp;&nbsp;<?php echo isset($Prospect_data->zip) ? $Prospect_data->zip : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Social Sec :</strong></td>
                                                                <td class="pro_social_sec"><?php echo isset($Prospect_data->social_sec) ? $Prospect_data->social_sec : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Birth Date :</strong></td>
                                                                <td class="pro_birthdate">
                                                                    <?php
                                                                    if (isset($Prospect_data->birthdate) && $Prospect_data->birthdate != '0000-00-00 00:00:00') {
                                                                        echo isset($Prospect_data->birthdate) && $Prospect_data->birthdate != '0000-00-00 00:00:00' ? date('m/d/Y', strtotime($Prospect_data->birthdate)) : '';
                                                                        ?>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120" style="border-bottom: 1px solid #e7eaec;"><strong>Email :</strong></td>
                                                                <td style="border-bottom: 1px solid #e7eaec;" class="pro_email"><?php echo isset($Prospect_data->email) ? $Prospect_data->email : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Type :</strong></td>
                                                                <td class="pro_type"><?php echo isset($Prospect_data->type) ? $Prospect_data->type : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Status :</strong></td>
                                                                <td class="pro_casestat"><?php echo isset($Prospect_data->casestat) ? $Prospect_data->casestat : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Date Ent :</strong></td>
                                                                <td class="pro_daterefin"><?php echo isset($Prospect_data->daterefin) && $Prospect_data->daterefin != '0000-00-00 00:00:00' ? date('m/d/Y', strtotime($Prospect_data->daterefin)) : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Last Cont :</strong></td>
                                                                <td class="pro_datelastcn"><?php echo isset($Prospect_data->datelastcn) && $Prospect_data->datelastcn != '0000-00-00 00:00:00' ? date('m/d/Y', strtotime($Prospect_data->datelastcn)) : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Init Contact :</strong></td>
                                                                <td class="pro_initials0"><?php echo isset($Prospect_data->initials0) ? $Prospect_data->initials0 : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Pend with :</strong></td>
                                                                <td class="pro_pendwith"><?php echo isset($Prospect_data->pendwith) ? $Prospect_data->pendwith : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Attorney :</strong></td>
                                                                <td class="pro_atty"><?php echo isset($Prospect_data->atty) ? $Prospect_data->atty : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Ref Code :</strong></td>
                                                                <td class="pro_refcode"><?php echo isset($Prospect_data->refcode) ? $Prospect_data->refcode : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Ref Source :</strong></td>
                                                                <td class="pro_refsource"><?php echo isset($Prospect_data->refsource) ? $Prospect_data->refsource : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120" style="border-bottom: 1px solid #e7eaec;"><strong>Ref By :</strong></td>
                                                                <td class="pro_refby" style="border-bottom: 1px solid #e7eaec;"><?php echo isset($Prospect_data->refby) ? $Prospect_data->refby : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Association :</strong></td>
                                                                <td class="pro_assciation"><?php echo isset($filesudf[4]) ? $filesudf[4] : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Ref To :</strong></td>
                                                                <td class="pro_refto"><?php echo isset($Prospect_data->refto) ? $Prospect_data->refto : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Date Ref :</strong></td>
                                                                <td class="pro_dateref"><?php echo isset($Prospect_data->dateref) && $Prospect_data->dateref != '0000-00-00 00:00:00' ? date('m/d/Y', strtotime($Prospect_data->dateref)) : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Out Ref Stat :</strong></td>
                                                                <td class="pro_outrefstat"><?php echo isset($Prospect_data->outrefstat) ? $Prospect_data->outrefstat : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Ref Fee :</strong></td>
                                                                <td class="pro_ref_fee">
                                                                    <?php
                                                                    if (isset($filesudf) && !empty($filesudf[0])) {
                                                                        echo $filesudf[0];
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Amount :</strong></td>
                                                                <td class="pro_amount"><?php echo isset($Prospect_data->amount) ? $Prospect_data->amount : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right" width="120"><strong>Date Paid - f/u  :</strong></td>
                                                                <td class="date_follow_paid">
                                                                    <?php
                                                                    $ofset6 = isset($filesudf[5]) && $filesudf[5] != '' ? date('m/d/Y', strtotime($filesudf[5])) : "";
                                                                    if (isset($Prospect_data->datepaid) && $Prospect_data->datepaid != '0000-00-00 00:00:00') {
                                                                        $dpd = date('m/d/Y', strtotime($Prospect_data->datepaid));
                                                                    } else {
                                                                        $dpd = '';
                                                                    }
                                                                    echo $dpd . " - " . $ofset6;
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <form action="" role="form">
                                <div class="tabs-container">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#tab-1">Notes</a></li>
                                        <li class=""><a data-toggle="tab" href="#tab-3">Injury</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tab-1" class="tab-pane active">
                                            <div class="panel-body">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-content">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p class="pro_notes"> <?php echo isset($Prospect_data->notes) ? $Prospect_data->notes : ''; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-3" class="tab-pane">
                                            <div class="panel-body">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-content">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p class=""><strong>Employer </strong>:<span class="pro_injn_emp"><?php echo $filesudf[1]; ?></span></p>
                                                                <p class=""><strong>Date of Injury</strong>:<span class="pro_injn_date"><?php echo $filesudf[2]; ?></span></p>
                                                                <p class=""><strong>Part of Body</strong>:<span class="pro_injn_body"><?php echo $filesudf[3]; ?></span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addprospects" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">PNC - Add</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Add</th>
                                    <th width="120">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Add PNC</td>
                                    <td><a href="#"class="add_prospect"  data-toggle="modal" data-dismiss="modal">Proceed</a></td>
                                </tr>
                                <tr>
                                    <td>Add / Accept This Case</td>
                                    <td><a href="#" data-toggle="modal" class="AddAsCase" >Proceed</a></td>
                                </tr>
                                <tr>
                                    <td>Add a Task</td>
                                    <td><a href="#addtask" data-toggle="modal">Proceed</a></td>
                                </tr>
                                <tr>
                                    <td>Clone this PNC</td>
                                    <td><a href="#" data-toggle="modal" class="clone_prospect">Proceed</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary btn-danger" href="#" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="proceedprospects" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="titlemodal">Add </span>a Potential New Client</h4>
            </div>
            <div class="modal-body">
                <form id="prospectForm" method="POST">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-6">General</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-7">Marketing</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-8">Notes</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-9">Injuries</a></li>
                            <li class=""><a data-toggle="tab" href="#advance_tab">Advanced</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-6" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Title</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <select class="form-control select2Class" name="pro_salutation" id="pro_salutation">
                                                                        <option value="">Select Salutation</option>
                                                                        <?php foreach ($saluatation as $val) { ?>
                                                                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Suffix</label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <select name="pro_suffix" class="form-control select2Class">
                                                                        <option value="">Select Suffix</option>
                                                                        <?php foreach ($suffix as $val) { ?>
                                                                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">First Name <span style="color:red">*</span></label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" class="form-control duplicate" name="pro_first" id="pro_first"  autocomplete="off"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Cell Phone</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control contact" placeholder="(xxx) xxx-xxxx" name="pro_cell"  id="pro_cell" maxlength="14" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>        
                                                        
                                                    </div>
                                                </div>
                                                 <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Last Name <span style="color:red">*</span></label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" class="form-control duplicate" name="pro_last" id="pro_last"  autocomplete="off"/>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Work Phone</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control contact" placeholder="(xxx) xxx-xxxx" name="pro_business" id="pro_business" maxlength="14" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Home Phone</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control contact" placeholder="(xxx) xxx-xxxx" name="pro_home" id="pro_home" maxlength="14" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Address</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control "  name="pro_address1" id="pro_address1" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">City</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control" name="pro_city" id="pro_city" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">State</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control" name="pro_state" id="pro_state" placeholder="NV" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Zip</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control" name="pro_zip" placeholder="10022" id="pro_zip" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Social Sec No.</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control social_sec" name="pro_social_sec" id="pro_social_sec" placeholder="234-56-7898" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Email</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="email" class="form-control" name="pro_email" id="pro_email" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Birth Date</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control  date-field" name="pro_birthdate" id="pro_birthdate" data-mask="99/99/9999" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Category <span style="color:red">*</span></label>
                                                        <div class="col-sm-7 no-padding marg-bot10">
                                                            <select class="form-control select2Class" name="pro_type" id="pro_type">
                                                                <?php if(isset($category)) { foreach ($category as $val) { ?>
                                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                            <?php } } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Type of Case  <?php
                                                            if (isset($casetypevalidate) && $casetypevalidate == 1) {
                                                                echo '<span style="color:red">*</span>';
                                                            }
                                                            ?></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control <?php
                                                            if ($casetypedrp == 1) {
                                                                echo 'cstmdrp';
                                                            }
                                                            ?> " name="pro_casetype" id="pro_casetype" <?php
                                                                    if ($casetypevalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?>   >
                                                                <option value="">select Case Type</option>
                                                                <?php foreach ($casetype as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Case Status</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <select name="pro_casestat" id="pro_casestat"  placeholder="Select Case Status" class="form-control <?php
                                                            if ($casestatusdrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" <?php
                                                            if ($casestatvalidate == 1) {
                                                                echo 'required';
                                                            }
                                                            ?>>
                                                                <option value="">Select Case Status</option>
                                                                <?php foreach ($casestat as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date Entered</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control injurydate" name="pro_daterefin" id="pro_daterefin" data-mask="99/99/9999" value="<?php echo date('m/d/Y'); ?>" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Last Contact Date</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control preinjurydate" name="pro_datelastcn" id="pro_datelastcn" data-mask="99/99/9999" value="<?php echo date('m/d/Y'); ?>" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">To Do Next</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control" name="pro_todonext" id="pro_todonext" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group " >
                                                        <label class="col-sm-5 no-left-padding">Initial Contact</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="pro_initials0" id="pro_initials0">
                                                                <option value="">Select...</option>
                                                                <?php foreach ($atty_resp as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Pending with</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control select2Class" name="pro_pendwith" id="pro_pendwith">
                                                                <option value="">select... </option>
                                                                <?php foreach ($para_hand as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Attorney</label>
                                                        <div class="col-sm-7 no-padding">

                                                            <select class="form-control select2Class" name="pro_atty" id="pro_atty">
                                                                <option value="">select Attorney</option>
                                                                <?php foreach ($sec_hand as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix">&nbsp;</div>
                                                    <?php
                                                    if ($mtitle1 != '' || $mtitle2 != '' || $mtitle3 != '' || $mtitle4 != '' || $mtitle5 != '' || $mtitle6 != '') {
                                                        ?>
                                                        <div class="form-group">
                                                            <div class="col-sm-5"></div>
                                                            <?php
                                                            if ($mtitle1 != '') {
                                                                if (isset($mtitleval1) && $mtitleval1 == 1) {
                                                                    $chkm = 'checked';
                                                                } else {
                                                                    $chkm = '';
                                                                }
                                                                echo '<span><label><input type="checkbox" name="pro_mailingh" class="i-checks" ' . $chkm . '>' . $mtitle1 . '</label></span>&nbsp;&nbsp;';
                                                            }
                                                            if ($mtitle2 != '') {

                                                                if (isset($mtitleval2) && $mtitleval2 == 1) {
                                                                    $chkm2 = 'checked';
                                                                } else {
                                                                    $chkm2 = '';
                                                                }
                                                                echo '<span><label><input type="checkbox" name="pro_mailing1" class="i-checks" ' . $chkm2 . ' >' . $mtitle2 . '</label></span>&nbsp;&nbsp;';
                                                            }
                                                            if ($mtitle3 != '') {
                                                                if (isset($mtitleval3) && $mtitleval3 == 1) {
                                                                    $chkm3 = 'checked';
                                                                } else {
                                                                    $chkm3 = '';
                                                                }
                                                                echo '<span><label><input type="checkbox" name="pro_mailing2" class="i-checks" ' . $chkm3 . ' >' . $mtitle3 . '</label></span>&nbsp;&nbsp;';
                                                            }
                                                            if ($mtitle4 != '') {
                                                                if (isset($mtitleval4) && $mtitleval4 == 1) {
                                                                    $chkm4 = 'checked';
                                                                } else {
                                                                    $chkm4 = '';
                                                                }
                                                                echo '<span><label><input type="checkbox" name="pro_mailing3" class="i-checks" ' . $chkm4 . '>' . $mtitle4 . '</label></span>&nbsp;&nbsp;';
                                                            }
                                                            if ($mtitle5 != '') {
                                                                if (isset($mtitleval5) && $mtitleval5 == 1) {
                                                                    $chkm5 = 'checked';
                                                                } else {
                                                                    $chkm5 = '';
                                                                }
                                                                echo '<span><label><input type="checkbox" name="pro_mailing4" class="i-checks" ' . $chkm5 . '>' . $mtitle5 . '</label></span>&nbsp;&nbsp;';
                                                            }
                                                            if ($mtitle6 != '') {
                                                                echo '<span><label><input type="checkbox" name="pro_mailing5" class="i-checks">' . $mtitle6 . '</label></span>';
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <div class="form-group">
                                                            <span><label><input type="checkbox" name="pro_mailingh" class="i-checks"> Mailing - Holiday List</label></span>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-7" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referral Code <?php
                                                            if (isset($referralvalidate) && $referralvalidate == 1) {
                                                                echo '<span style="color:red">*</span>';
                                                            }
                                                            ?></label>
                                                        <div class="col-sm-7 no-padding">
                                                            <select class="form-control <?php
                                                            if ($casereferraldrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>"  name="pro_refcode" id="pro_refcode" <?php
                                                                    if ($referralvalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?> placeholder="Select Referral Code">
                                                                <option value="" disabled>Select Referral Code</option>
                                                                <?php foreach ($referral as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referral Source <?php
                                                            if (isset($referralsrcvalidate) && $referralsrcvalidate == 1) {
                                                                echo '<span style="color:red">*</span>';
                                                            }
                                                            ?></label>
                                                        <div class="col-sm-7 no-padding">
                                                            <select class="form-control <?php
                                                            if ($referralsrcdrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_refsource" id="pro_refsource" <?php
                                                                    if ($referralsrcvalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?> placeholder="Select Referral Source">
                                                                <option value="" disabled>Select Referral Source</option>
                                                                <?php foreach ($referralsrc as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referred By <?php
                                                            if (isset($referredbyvalidate) && $referredbyvalidate == 1) {
                                                                echo '<span style="color:red">*</span>';
                                                            }
                                                            ?></label>
                                                        <div class="col-sm-7 no-padding">
                                                            <select class="form-control <?php
                                                            if ($referredbydrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_refby" id="pro_refby" <?php
                                                                    if ($referredbyvalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?> placeholder="Select Referred By">
                                                                <option value="" disabled>Select Referred By</option>
                                                                <?php foreach ($referredby as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Association <?php
                                                            if (isset($associationvalidate) && $associationvalidate == 1) {
                                                                echo '<span style="color:red">*</span>';
                                                            }
                                                            ?></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control  <?php
                                                            if ($associationdrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_accociation" id="pro_accociation" <?php
                                                                    if ($associationvalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?>>
                                                                <option value="" disabled>Select Association</option>
                                                                <?php foreach ($association as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Out Ref Status <?php
                                                            if (isset($rstatusvalidate) && $rstatusvalidate == 1) {
                                                                echo '<span style="color:red">*</span>';
                                                            }
                                                            ?></label>
                                                        <div class="col-sm-7 no-padding">
                                                            <select class="form-control <?php
                                                            if ($rstatusdrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_outrefstat" name="pro_outrefstat" <?php
                                                                    if ($rstatusvalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?>>
                                                                <option value="" disabled>Select Ref Status</option>
                                                                <?php foreach ($referalstatus as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Referred To <?php
                                                            if (isset($referredtovalidate) && $referredtovalidate == 1) {
                                                                echo '<span style="color:red">*</span>';
                                                            }
                                                            ?></label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control <?php
                                                            if ($referredtodrp == 1) {
                                                                echo 'cstmdrp';
                                                            } else {
                                                                echo 'select2Class';
                                                            }
                                                            ?>" name="pro_refto" id="pro_refto" <?php
                                                                    if ($referredtovalidate == 1) {
                                                                        echo 'required';
                                                                    }
                                                                    ?>>
                                                                <option value="" disabled>Select Referred To</option>
                                                                <?php foreach ($referredto as $key => $val) { ?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Date Referred</label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" class="form-control injurydate" id="pro_dateref" name="pro_dateref" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">% of Ref Fee</label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" class="form-control" id="pro_per_reffee" name="pro_per_reffee" autocomplete="off"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Ref Fee</label>
                                                                <div class="col-sm-7 no-padding marg-bot5">
                                                                    <select class="form-control select2Class" id="pro_reffee" name="pro_reffee">
                                                                        <option value="" disabled>Select Ref Fee</option>
                                                                        <option value="Yes">YES</option>
                                                                        <option value="NO">NO</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label class="col-sm-5 no-left-padding">Amount</label>
                                                                <div class="col-sm-7 no-padding">
                                                                    <input type="text" class="form-control" name="pro_amount" id="pro_amount" autocomplete="off"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Date Paid</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control injurydate" name="pro_datepaid" id="pro_datepaid" data-mask="99/99/9999" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Follow up Date</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control injurydate" name="pro_followup_date" id="pro_followup_date" data-mask="99/99/9999" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-8" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <span><label><input type="checkbox" class="i-checks"> Make Notes a Red Alert </label></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="pro_notes"  id="pro_notes"style="height: 200px;"><?php echo $this->session->userdata('user_data')['initials'] . "-" . date('m/d/Y h:i:s a'); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-9" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Employer</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" name="pro_injn_emp" id="pro_injn_emp" class="form-control" autocomplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">DOI</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text"  name="pro_injn_date" id="pro_injn_date"  class="form-control" data-mask="99/99/9999" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">POB</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" name="pro_injn_body" id="pro_injn_body"  class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="advance_tab" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Link to Case Number</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" id="pro_caseno" name="pro_caseno"  class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">PNC ID Number</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" id="pro_prosno" name="pro_prosno" value="<?php echo isset($Prospect_mainkey) ? $Prospect_mainkey : ''; ?>" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">PNC Main ID</label>
                                                        <div class="col-sm-7 no-padding">
                                                            <input type="text" class="form-control"  id="pro_proskey" name="pro_proskey" value="<?php echo isset($Prospect_mainkey) ? $Prospect_mainkey : ''; ?>" readonly autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center marg-top15">
                                <input type="hidden" id="prospectId" name="prospectId">
                                <button type="submit" class="btn btn-md btn-primary" id="save_prospect">Save</button>
                                <button type="button" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="printenvelope" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print Envelopes or Labels</h4>
            </div>
            <div class="modal-body">
                <form action="" id="removelabel" role="form" >
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-11">General</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-12">Return Address</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-13">Bin Override</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-11" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>To</label>
                                                        <textarea class="form-control" style="height: 150px;" name="env_address" id="env_address"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="form-group marg-bot5">
                                                        <span><label><input type="checkbox" class="i-checks" name="re_check" id="re_check"> Return Address </label></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <span><label><input type="checkbox" class="i-checks"> Collate copies </label></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-7">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 no-left-padding text-right">Copies</label>
                                                        <div class="col-sm-9 no-padding marg-bot5">
                                                            <input type="text" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 no-left-padding text-right">Style</label>
                                                        <div class="col-sm-9 no-padding marg-bot5">
                                                            <select class="form-control">
                                                                <option>Normal</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15 marg-bot0">
                                                        <button type="button" class="btn btn-md btn-primary printenvelopes">Print Envelopes</button>
                                                        <!-- <button type="button" class="btn btn-md btn-primary">Print 2 Col Labels</button>
                                                         <button type="button" class="btn btn-md btn-primary">Print 3 Col Labels</button>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-12" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Return address to show on the envelope</label>
                                                        <textarea class="form-control" style="height: 150px;" id="env_return_address" name="env_return_address"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <span class="marg-bot10">Return Address</span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Top Margin</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Left Margin</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <span class="marg-bot10">Addressed To</span>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Top Margin</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Left Margin</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <input type="text" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Bin</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Format</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select class="form-control">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-13" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h4>Enter zero to use the default settings</h4>
                                                    <div class="form-group">
                                                        <label class="col-sm-6 no-left-padding">Printer Bin Override (-1 to show)</label>
                                                        <div class="col-sm-6 no-padding">
                                                            <input type="text" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <h4>&nbsp;</h4>
                                                    <div class="form-group">
                                                        <label class="col-sm-6 no-left-padding">Paper Size</label>
                                                        <div class="col-sm-6 no-padding">
                                                            <input type="text" class="form-control" autocomplete="off"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="duplicateProspect" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span id="record_count"> </span>    Possible Duplicate - By Name</h4>
            </div>
            <form method="post" name="parties_case_specific" id="parties_case_specific">
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <table id="duplicateProspectEntries" class="table table-bordered table-hover table-striped dataTables-example" style="width: auto;">
                                        <thead>
                                        <th>Date Refrered</th>
                                        <th>Name</th>
                                        <th>Home</th>
                                        <th>Business</th>
                                        <th>Address</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Atty</th>
                                        <th>Pendwith</th>
                                        <th>Todo Next</th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <div class="">
                            <button id="attach_link" type="button" class="btn btn-primary" disabled>Link</button>
                            <button type="button" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    var taskTable = '';
    var ProspectAjaxData;
    var ProspectAjaxSearchData;
    var duplicateProspectDatatable;
    var ProspectprintListDatatable;

    var searchBy = {'prosno':<?php echo isset($record[0]->proskey) ? $record[0]->proskey : 'null'; ?>};

    var Prospect_data = JSON.parse('<?php echo addslashes(json_encode($Prospect_data)); ?>');

    function changePrintProspectModalHeader(modalType) {
        if (modalType == "search") {
            $("#printProspectmodal .modal-title").html("Search For a PNC");
        } else if (modalType == "print") {
            $("#printProspectmodal .modal-title").html("Find prospect to print");
        }
    }

    $(document).ready(function () {

        taskTable = $(".task-dataTable").DataTable({
            processing: true,
            serverSide: true,
            bFilter: true,
            order: [[0, "DESC"]],
            autoWidth: false,
            ajax: {
                url: "<?php echo base_url(); ?>tasks/getTasksData",
                type: "POST",
                data: function (d) {
                    var c = $("ul#task-navigation").find("li.active").attr("id");
                    if ($("#task_reminder").is(":checked") == true) {
                        var b = 1
                    } else {
                        var b = ""
                    }
                    var e = {
                        caseno: $("#caseNo").val()
                    };
                    e.taskBy = c;
                    e.reminder = b;
                    $.extend(d, e);
                }
            },
            fnRowCallback: function (f, e, d, c) {
                $(f).attr("id", e[10]);
                if (e[e.length - 2] != "") {
                    var b = e[e.length - 2].split("-");
                    if (b[0] != "") {
                        $(f).css({
                            "background-color": b[0],
                            color: b[1]
                        });
                    } else {
                        $(f).css({
                            "background-color": "#ffffff",
                            color: "#000000"
                        });
                    }
                }
            },
            columnDefs: [{
                    render: function (e, c, f) {
                        if (e != null) {
                            var b = "";
                            var d = "";
                            if (f[8] == "") {
                                if (f[2] === "<?php echo isset($username) ? $username : $this->session->userdata('user_data')['username']; ?>") {
                                    d = '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + e + ')"><i class="fa fa-trash"></i></button>'
                                }
                                b = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + e + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;' + d;
                                b += '&nbsp;&nbsp;<button class="btn btn-primary btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + e + ')"><i class="fa fa-check"></i></button>';
                                if (f[11] != 0) {
                                    b += '&nbsp;&nbsp;<a class="btn btn-primary btn-circle-action btn-circle" title="PullCase" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + f[11] + '"><i class="fa fa-folder-open"></i></a>';
                                }
                            }
                        }
                        return b;
                    },
                    targets: 9,
                    bSortable: false,
                    bSearchable: false
                }, {
                    targets: 6,
                    className: "textwrap-line"
                }]
        });

        $("#pro_social_sec").keydown(function (e) {
            if ($.inArray(e.keyCode, [189, 46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('#addTask').on('click', function () {
            var rdate = $(".rdate").val();
            var taskdatepicker = $('#taskdatepicker').val();
            if (taskdatepicker >= rdate) {
                console.log('Yes');
            } else {
                $('.pncerror').html('Must be greater than Reminder Date.');
                return false;
            }

            if ($('#addTaskForm').valid()) {
                $.blockUI();

                var url = '';
                if ($('input[name=taskid]').val() != '' && $('input[name=taskid]').val() != 0) {
                    url = '<?php echo base_url(); ?>tasks/editTask';
                } else {
                    url = '<?php echo base_url(); ?>tasks/addTask';
                }

                $.ajax({
                    url: url,
                    method: "POST",
                    data: $('#addTaskForm').serialize(),
                    success: function (result) {
                        var data = $.parseJSON(result);
                        $.unblockUI();
                        if (data.status == '1') {
                            swal({
                                title: "Success!",
                                text: data.message,
                                type: "success"
                            }, function () {
                                $('#addTaskForm1')[0].reset();
                                $('#addTaskForm')[0].reset();

                                $("select[name=typetask]").select2('val', 'Task Type');
                                $("select[name=typetask]").val('').trigger("change");

                                $("select[name=phase]").select2('val', 'Select Phase');
                                $("select[name=phase]").val('').trigger("change");

                                $("select[name=phase]").val('').trigger("change");

                                $(".select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");

                                $(".select2Class#color").val($(".select2Class#color option:eq(0)").val()).trigger("change");

                                $(".select2Class#priority").val($(".select2Class#priority option:eq(0)").val()).trigger("change");

                                $('#addtask').modal('hide');
                                $('#addtask .modal-title').html('Add a Task');

                                taskTable1.draw(false);
                            });
                        } else {
                            swal("Oops...", data.message, "error");
                        }
                    }
                });
            }
        });

        $('#env_address').text(Prospect_data.salutation + " " + Prospect_data.first + " " + Prospect_data.last + ',' + Prospect_data.suffix + "\n" + Prospect_data.address1 + "\n" + Prospect_data.city + " " + Prospect_data.state + " " + Prospect_data.zip);
        duplicateProspectDatatable = $('#duplicateProspectEntries').DataTable();
        ProspectprintListDatatable = $('#ProspectprintListDatatable').DataTable();

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
        $('.input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
        $('#pro_birthdate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        });
        $('#pro_injn_date').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        });

        $('.injurytime').mdtimepicker({
            autoclose: true
        });
        $('#uploadBtn3').change(function () {
            if ($(this).val() != '') {
                $('#uploadFile3').html($(this).val());
            } else {
                $('#uploadFile3').html('Upload File');
            }
        });

        function setTaskValidation() {
            var validator = $('#addTaskForm').validate({
                ignore: [],
                rules: {
                    whofrom: {required: true},
                    whoto2: {required: true},
                    datereq: {required: true, greaterThan: ".rdate"},
                    event: {required: true, noBlankSpace: true}
                }
            });
            return validator;
        }
    });

    $('#proceedprospects').on('hidden.bs.modal', function () {
        $(this).find('input,textarea,select').each(function () {
            this.value = '';
            $(this).iCheck('uncheck');
            $(this).trigger('change');
        });
        $('.titlemodal').html('Add ');
        $('#prospectForm')[0].reset();
    });

    $(document.body).on('click', '.next_privous_list', function () {
        var record = '<?php echo json_encode($record); ?>';
        var current = $(this).attr('data-id');
        if (current != '') {
            record = JSON.parse(record);
            var arr = new Array();
            var privous = '';
            var next = '';
            $.each(record, function (k, v) {
                arr.push(v.prospect_id);
            });

            if ($(this).attr('data-type') == 'previous') {
                $.each(arr, function (index, value) {
                    if (current == value) {
                        if (index == 0) {
                            swal({
                                title: "Alert",
                                text: "You Are At Current Prospect",
                                type: "success"
                            });
                        } else {
                            privous = parseInt(index - 1);
                            next = parseInt(index);
                        }
                    }
                });
            } else {
                $.each(arr, function (index, value) {
                    if (current == value) {
                        if (arr.length == parseInt(index) + 1) {
                            swal({
                                title: "Alert",
                                text: "You Are At Last Prospect",
                                type: "success"
                            });
                        } else {
                            privous = parseInt(index);
                            next = parseInt(index) + 1;
                        }
                    }
                });
            }
            $('.Privous').attr("data-id", arr[privous]);
            $('.Next').attr("data-id", arr[next]);
            $.ajax({
                url: '<?php echo base_url(); ?>prospects/getProspectsdata',
                method: "POST",
                data: {prospect_id: current},
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (result) {
                    var data = $.parseJSON(result);
                    ProspectAjaxData = data;
                    var fieldsudf = data.data.fieldsudf.split(',');
                    var name = data.data.salutation + ' ' + data.data.first + ' ' + data.data.last;
                    var st_zip = data.data.city + " " + data.data.state + " " + data.data.zip;
                    $('.edit_prospect').attr("data-prospect_id", current);
                    $('.edit_prospect').attr("data-case_no", data.data.caseno);
                    $('.delete_prospect').attr("data-pros_id", current);
                    $('.delete_prospect').attr("data-status", data.data.status);
                    if (data.data.status == '1') {
                        $('.prospect_status').html("Deleted");
                    } else {
                        $('.prospect_status').html("");
                    }
                    $('.task_list').attr("data-proskey_id", data.data.proskey);
                    $('.pro_first1').text(data.data.first);
                    $('.pro_last1').text(data.data.last);
                    $('.pro_first').html(name);
                    if(data.data.caseno!='0')
                    {
                        $('#pnctopinfo').text(' - Case ' + data.data.caseno + ' - '+ name);
                    }
                    else
                    {
                        $('#pnctopinfo').text(' - '+ name);
                    }
                    $('.pro_cell').html(data.data.cell);
                    $('.pro_home').html(data.data.home);
                    $('.pro_business').html(data.data.business);
                    $('.pro_address').html(data.data.address1);
                    $('.pro_city_st_zip').html(st_zip);
                    $('.pro_social_sec').html(data.data.social_sec);
                    var dob = new Date(data.data.birthdate);
                    var today = new Date();
                    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                    if (data.data.birthdate != '0000-00-00 00:00:00') {
                        $('.pro_birthdate').html(moment(data.data.birthdate).format('MM/DD/YYYY'));
                    } else {
                        $('.pro_birthdate').html('');
                    }
                    $('.pro_email').html(data.data.email);
                    $('.pro_type').html(data.data.type);
                    $('.pro_casestat').html(data.data.casestat);
                    $('.pro_daterefin').html(moment(data.data.daterefin).format('MM/DD/YYYY'));
                    $('.pro_datelastcn').html(moment(data.data.datelastcn).format('MM/DD/YYYY'));
                    $('.pro_initials0').html(data.data.initials0);
                    $('.pro_pendwith').html(data.data.pendwith);
                    $('.pro_atty').html(data.data.atty);
                    if (data.data.mailingh == 'Y' || data.data.mailingh == '1') {
                        $('.pro_mailingh').html('Y');
                    } else {
                        $('.pro_mailingh').html("");
                    }
                    $('.pro_refcode').html(data.data.refcode);
                    $('.pro_refsource').html(data.data.refsource);
                    $('.pro_refby').html(data.data.refby);
                    $('.pro_assciation').html(fieldsudf[5]);
                    $('.pro_refto').html(data.data.refto);
                    if (data.data.dateref != '0000-00-00 00:00:00') {
                        $('.pro_dateref').html(moment(data.data.dateref).format('MM/DD/YYYY'));
                    } else {
                        $('.pro_dateref').html('');
                    }
                    $('.pro_outrefstat').html(data.data.outrefstat);
                    $('.pro_ref_fee').html(fieldsudf[0]);
                    $('.pro_amount').html(data.data.amount);
                    if (data.data.datepaid != '0000-00-00 00:00:00') {
                        $('.date_follow_paid').html(moment(data.data.datepaid).format('MM/DD/YYYY') + " - " + fieldsudf[5]);
                    } else {
                        $('.date_follow_paid').html('');
                    }
                    $('.pro_notes').text(data.data.notes);
                    $('.pro_injn_emp').html(fieldsudf[1]);
                    $('.pro_injn_date').html(fieldsudf[2]);
                    $('.pro_injn_body').html(fieldsudf[3]);
                    $('#env_address').text(name + ',' + data.data.suffix + "\n" + data.data.address1 + "\n" + st_zip);
                    return false;
                }
            });
        }
    });

    $(document.body).on('focusout', '.duplicate', function () {
        var first_name = $("#pro_first").val();
        var last_name = $("#pro_last").val();

        if ((first_name != undefined || first_name == null) && (last_name != undefined || last_name == null)) {
            $.ajax({
                url: '<?php echo base_url(); ?>prospects/checkDuplicateProspect',
                method: 'POST',
                data: {first_name: first_name, last_name: last_name},
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (result) {
                    var obj = $.parseJSON(result);
                    duplicateProspectDatatable.rows().remove().draw();
                    if (obj.status == '1') {
                        $.each(obj.data, function (index, item) {
                            var html = '';
                            html += "<tr class='selectProspectDuplicateData' data-prospect_id=" + item.prospect_id + ">";
                            html += "<td>" + item.dateref + "</td>";
                            html += "<td>" + item.first + " " + item.last + "</td>";
                            html += "<td>" + item.home + "</td>";
                            html += "<td>" + item.business + "</td>";
                            html += "<td>" + item.address1 + "</td>";
                            html += "<td>" + item.type + "</td>";
                            html += "<td>" + item.casestat + "</td>";
                            html += "<td>" + item.atty + "</td>";
                            html += "<td>" + item.pendwith + "</td>";
                            html += "<td>" + item.todonext + "</td>";
                            html += "</tr>";
                            duplicateProspectDatatable.row.add($(html));
                        });
                        duplicateProspectDatatable.draw();
                        $('#record_count').text(obj.data.length);
                        if (obj.data.length == 1) {
                            $("#duplicateProspectEntries tr.selectProspectDuplicateData").addClass("table-selected");
                        }
                        $("#duplicateProspect").modal("show");
                    }
                }
            });
        }
    });

    $(document.body).on('click', '.selectProspectDuplicateData', function () {
        $("#duplicateProspectEntries tr").removeClass("table-selected");
        $(this).addClass('table-selected');
        var numItems = $('.table-selected').length;
        if (numItems == '1') {
            $('#attach_link').prop('disabled', false);
        }
    });
    $(document.body).on('click', '#attach_link', function () {
        var row = duplicateProspectDatatable.row('.table-selected').node();

        $('#duplicateProspect').modal('hide');
        $.ajax({
            url: '<?php echo base_url(); ?>prospects/getProspectsdata',
            method: "POST",
            data: {prospect_id: $(row).attr('data-prospect_id')},
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (result) {
                var data = $.parseJSON(result);
                var fieldsudf = data.data.fieldsudf.split(',');
                $('select[name=pro_salutation]').val(data.data.salutation);
                $('#pro_first').val(data.data.first);
                $('#pro_last').val(data.data.last);
                $('select[name=pro_suffix]').val(data.data.suffix);
                $('#pro_cell').val(data.data.cell);
                $('#pro_business').val(data.data.business);
                $('#pro_home').val(data.data.home);
                $('#pro_address1').val(data.data.address1);
                $('#pro_city').val(data.data.city);
                $('#pro_state').val(data.data.state);
                $('#pro_zip').val(data.data.zip);
                $('#pro_social_sec').val(data.data.social_sec);
                $('#pro_email').val(data.data.email);
                var dob = new Date(data.data.birthdate);
                var today = new Date();
                var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                if (data.data.birthdate != '0000-00-00 00:00:00') {
                    $('#pro_birthdate').val(moment(data.data.birthdate).format('MM/DD/YYYY'));
                } else {
                    $('#pro_birthdate').val('');
                }
                $('select[name=pro_type]').val(data.data.type);
                $('select[name=pro_casetype]').val(data.data.casetype);
                $('select[name=pro_initials0]').val(data.data.initials0);
                $('select[name=pro_casestat]').val(data.data.casestat);
                $('select[name=pro_pendwith]').val(data.data.pendwith);
                $('#pro_daterefin').val(moment(data.data.daterefin).format('MM/DD/YYYY'));
                $('#pro_datelastcn').val(moment(data.data.datelastcn).format('MM/DD/YYYY'));
                $('select[name=pro_atty]').val(data.data.atty);
                if (data.data.mailingh == 1) {
                    $('input[name=pro_mailingh]').iCheck('check');
                } else {
                    $('input[name=pro_mailingh]').iCheck('uncheck');
                }
                $('#pro_todonext').val(data.data.todonext);
                $('select[name=pro_refcode]').val(data.data.refcode);
                $('select[name=pro_refto]').val(data.data.refto);
                $('select[name=pro_refsource]').val(data.data.refsource);
                if (data.data.dateref != '0000-00-00 00:00:00') {
                    $('#pro_dateref').val(moment(data.data.dateref).format('MM/DD/YYYY'));
                } else {
                    $('#pro_dateref').val('');
                }
                $('#pro_per_reffee').val(fieldsudf[0]);
                $('select[name=pro_refby]').val(data.data.refby);
                $('select[name=pro_reffee]').val(fieldsudf[0]);
                $('#pro_amount').val(data.data.amount);
                $('select[name=pro_accociation]').val(fieldsudf[4]);
                if (data.data.datepaid != '0000-00-00 00:00:00') {
                    $('#pro_datepaid').val(moment(data.data.datepaid).format('MM/DD/YYYY'));
                } else {
                    $('#pro_datepaid').val('');
                }
                $('select[name=pro_outrefstat]').val(data.data.outrefstat);
                $('#pro_followup_date').val(fieldsudf[5]);
                $('#pro_notes').val(data.data.notes);
                $('#pro_injn_emp').val(fieldsudf[1]);
                $('#pro_injn_date').val(fieldsudf[2]);
                $('#pro_injn_body').val(fieldsudf[3]);
                if (data.data.caseno != 0) {
                    $('#pro_caseno').val(data.data.caseno);
                } else {
                    $('#pro_caseno').val('');
                }
                $('#pro_prosno').val(data.Prospect_mainkey);
                $('#pro_proskey').val(data.Prospect_mainkey);

                $('#proceedprospects').modal('show');
                $("#prospectForm select").trigger('change');
                return false;
            }
        });
    });
    $(document.body).on('click', '.add_prospect ', function () {
        $('#pro_prosno').val('<?php echo isset($Prospect_mainkey) ? $Prospect_mainkey : ''; ?>');
        $('#pro_proskey').val('<?php echo isset($Prospect_mainkey) ? $Prospect_mainkey : ''; ?>');
        $('.nav-tabs a[href="#tab-6"]').tab('show');
        $('#proceedprospects').modal('show');
    });

    $(document.body).on('click', '.clone_prospect', function () {
        setProspectvalue(ProspectAjaxData, 'clone');
        $('.nav-tabs a[href="#tab-6"]').tab('show');
    });
    $(document.body).on('click', '.AddAsCase', function () {
        $('#addprospects').modal('hide');
        var caseno = $('.edit_prospect').attr('data-case_no');
        if (caseno > 0) {
            swal({
                title: "That Case has already been accepted.\n  Case Number " + caseno + "\n Would you like to pull the case?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, function () {
                window.open("<?php echo base_url() ?>cases/case_details/" + caseno);
            });
        } else {
            $('#first_name').val($('.pro_first1').text());
            $('#last_name').val($('.pro_last1').text());
            $('#prospect_id').val($('.edit_prospect').attr('data-prospect_id'));
            $('#first_name').attr("readonly", true);
            $('#last_name').attr("readonly", true);
            $('#addcase').modal('show');
        }
    });

    $('#addtask.modal').on('hidden.bs.modal', function () {
        $('#addtask h4.modal-title').text('Add a Task');
        $("select[name=typetask]").select2('val', 'Task Type');
        $("select[name=typetask]").val('').trigger("change");
        $("select[name=phase]").select2('val', 'Select Phase');
        $("select[name=phase]").val('').trigger("change");
        $("select[name=phase]").val('').trigger("change");
        $(".select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");
        $(".select2Class#color").val($(".select2Class#color option:eq(0)").val()).trigger("change");
        $(".select2Class#priority").val($(".select2Class#priority option:eq(0)").val()).trigger("change");
        $('#taskdatepicker').val("");
        $("select[name=event]").val('');
        $("select[name=rdate]").val('');
        $('#addTaskForm1')[0].reset();
        $('#addTaskForm')[0].reset();

        var taskVal = setTaskValidation();
        taskVal.resetForm();
        $('input[name=taskid]').val(0);
        $('#addtask a[href="#name"]').tab('show');
    });

    $('#proceedprospects a[data-toggle="tab"]').on('click', function (e) {
        if (!$('#prospectForm').valid()) {
            e.preventDefault();
            return false;
        }
    });

    $(document.body).on('click', '.print_prospect', function () {
        $('#printoption').modal('show');
    });

    $(document.body).on('click', '.print_prospect_data', function () {
        var print_option = $('select[name=print_option]').val();
        var count = 1;
        var table = '';
        table += "<table  id='new_table' style='width:100%; margin:0px auto;>";
        if (print_option == "1") {
            table += "<thead><tr>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>No</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Pending</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>DateEntered</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>LastContact</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Name</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Todonext</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Type</th>";
            table += " </tr></thead>";
            table += "  <tbody>";
            $.each(ProspectAjaxSearchData.data, function (index, item) {
                table += "<tr>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + count + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.pendwith + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + moment(item.daterefin).format('MM/DD/YYYY');
                +"</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + moment(item.datelastcn).format('MM/DD/YYYY');
                +"</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.first + "  " + item.last + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.todonext + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.type + "</td>";
                table += "</tr></tbody>";
                count++;
            });
        } else if (print_option == "2") {
            table += "<thead><tr>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>No</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Name</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Phone(C.Cell,B.Business,H.Home)</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Type</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>DateEntered</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Pending</th>";
            table += " </tr></thead>";
            table += "  <tbody>";
            $.each(ProspectAjaxSearchData.data, function (index, item) {
                if (item.cell != '') {
                    var cell = "C. " + item.cell;
                } else {
                    var cell = '';
                }
                if (item.business != '') {
                    var business = "B." + item.business;
                } else {
                    var business = '';
                }
                if (item.home != '') {
                    var home = "H." + item.home;
                } else {
                    var home = '';
                }
                table += "<tr class='selectedData'  data-prospect=" + item.prospect_id + ">";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + count + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.first + "  " + item.last + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + cell + " <br>" + business + "<br> " + home + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' class='initials0'>" + item.type + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + moment(item.daterefin).format('MM/DD/YYYY');
                +"</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.pendwith + "</td>";
                table += "</tr></tbody>";
                count++;
            });

        } else if (print_option == "3") {
            table += "<thead><tr>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>No</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Name <br/>Fee</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Cell Phone<br/>Work Phone</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>DateEntered<br/>LastContact</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Initials co<br/>Pending with</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Attoney<br/>Mailing </th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Refer by<br/>Refer to </th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Type<br/>Status</th>";
            table += " </tr></thead>";
            table += "  <tbody>";
            $.each(ProspectAjaxSearchData.data, function (index, item) {
                var mailingh;
                var fieldsudf = item.fieldsudf.split(',');
                if (item.mailingh == 1) {
                    mailingh = "Y";
                } else {
                    mailingh = " ";
                }
                table += "<tr class='selectedData'  data-prospect=" + item.prospect_id + ">";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + count + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.first + "  " + item.last + "<br/>" + fieldsudf[0] + "</td>";

                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.cell + "</br>" + item.business + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + moment(item.daterefin).format('MM/DD/YYYY') + "<br/>" + moment(item.datelastcn).format('MM/DD/YYYY') + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.initials0 + "<br/>" + item.pendwith + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.atty + "<br/>" + mailingh + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.refby + "<br/>" + item.refto + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.type + "<br/>" + item.casestat + "</td>";
                table += "</tr></tbody>";
                count++;
            });
        } else if (print_option == "4" || print_option == "4b" || print_option == "5") {
            table += "<thead><tr>";
            table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>No</th>";
            if (print_option == "4") {
                table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>Name</th>";
            }
            if (print_option == "4b") {
                table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>Name <br/>Email</th>";
            }
            if (print_option == "5") {
                table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>Name <br/>Cell Phone</br>To DoNext</th>";
            }
            table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>Type<br/>Status</br>DateEntered<br/>LastContact</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>Initials co<br/>Pending with<br/>Attoney<br/>Mailing </th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>Refreral code<br/>Refreral Source<br/>Date Refered</br>Refer by</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:9px;padding: 5px;'>Refer to <br/>Out Refral stat,fee</br>Amount</br>Date paid </th>";
            table += " </tr></thead>";
            table += "  <tbody>";
            $.each(ProspectAjaxSearchData.data, function (index, item) {
                var mailingh;
                var fieldsudf = item.fieldsudf.split(',');
                if (item.mailingh == 1) {
                    mailingh = "Y";
                } else {
                    mailingh = " ";
                }
                table += "<tr class='selectedData'  data-prospect=" + item.prospect_id + ">";
                table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + count + "</td>";
                if (print_option == "4") {
                    table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + item.first + "  " + item.last + "</td>";
                }
                if (print_option == "4b") {
                    table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + item.first + "  " + item.last + "<br/>" + item.email + "</td>";
                }
                if (print_option == "5") {
                    table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + item.first + "  " + item.last + "<br/>" + item.cell + "<br/>" + item.todonext + "</td>";
                }
                table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + item.type + "</br>" + item.casestat + "<br/>" + moment(item.daterefin).format('MM/DD/YYYY') + "<br/>" + moment(item.datelastcn).format('MM/DD/YYYY') + "</td>";
                table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + item.initials0 + "<br/>" + item.pendwith + "<br/>" + item.atty + "<br/>" + mailingh + "</td>";
                table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + item.refcode + "<br/>" + item.refsource + "<br>" + moment(item.dateref).format("MM/DD/YYYY");
                +"<br/>" + item.refby + "</td>";
                table += "<td style='text-align: center;font-size:9px;padding: 5px;' >" + item.refto + "<br/>" + item.outrefstat + "," + fieldsudf[0] + "<br/>" + item.amount + "<br/>" + moment(item.datepaid).format('MM/DD/YYYY') + "</td>";
                table += "</tr></tbody>";
                count++;
            });
        } else if (print_option == "6") {
            var css = '@page {size: landscape} body { writing-mode: tb-rl; }',
                    head = document.head || document.getElementsByTagName('head')[0],
                    style = document.createElement('style');
            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);
            table += "<thead><tr>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>No</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>No</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Type</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Stutus</th>";

            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Referred To</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Date Referred</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Name</th>";

            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Pending</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Atty</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Referral fee</th>";

            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Out Ref status</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Amount</th>";
            table += "<th style='border-bottom: 1px solid #000;font-size:12px;padding: 5px;'>Date Paid</th>";
            table += " </tr></thead>";
            table += "  <tbody>";
            $.each(ProspectAjaxSearchData.data, function (index, item) {
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + count + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.type + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.casestat + "</td>";

                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.refto + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + moment(item.dateref).format('MM/DD/YYYY');
                +"</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.salutation + "  " + item.first + "  " + item.last + "</td>";

                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.pendwith + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.atty + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.reffee + "</td>";

                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + item.outrefstat + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;'>" + item.amount + "</td>";
                table += "<td style='text-align: center;font-size:12px;padding: 5px;' >" + moment(item.datepaid).format('MM/DD/YYYY');
                +"</td>";

                table += "</tr></tbody>";
                count++;
            });

        } else if (print_option == "7" || print_option == "8" || print_option == "9" || print_option == "10") {
            if (print_option == "7") {
                table += "  <tbody>";
                $.each(ProspectAjaxSearchData.data, function (index, item) {
                    table += "<tr><td>" + item.first + "  " + item.last + "</td></tr>";
                    table += "<tr><td>" + item.address1 + "</td></tr>";
                    table += "<tr><td>" + item.city + " " + item.state + " " + item.zip + "</td></tr></tbody>";
                });
            }
            if (print_option == "8") {
                var i = 1;
                table += "  <tbody>";
                $.each(ProspectAjaxSearchData.data, function (index, item) {
                    if (i % 2 != 0) {
                        table += "<tr><td width='70%'>" + item.first + "  " + item.last + "</br>" + item.address1 + "<br>" + item.city + " " + item.state + " " + item.zip + "</td>";
                    } else {
                        table += "<td>" + item.first + "  " + item.last + "</br>" + item.address1 + "<br>" + item.city + " " + item.state + " " + item.zip + "</td></tr></tbody>";
                    }
                    i++;
                });
            }

            if (print_option == "9") {
                var i = 0;
                var trEnd = 0;
                table += "  <tbody>";
                $.each(ProspectAjaxSearchData.data, function (index, item) {
                    if (i == 0) {
                        table += '<tr>';
                    }
                    table += "<td width='50%'>" + item.first + "  " + item.last + "</br>" + item.address1 + "<br>" + item.city + " " + item.state + " " + item.zip + "</td>";
                    if (i == 2) {
                        i = 0;
                        trEnd = 1;
                    } else {
                        trEnd = 0;
                        i++;
                    }
                    if (trEnd == 1) {
                        table += '</tr>';
                    }
                });
                if (trEnd == 0) {
                    table += '</tr></tbody>';
                }
            }
            if (print_option == "10") {
                var i = 1;
                table += "<tbody>";
                $.each(ProspectAjaxSearchData.data, function (index, item) {
                    if (i % 2 != 0) {
                        table += "<tr><td>" + item.first + "  " + item.last + "</br>" + item.address1 + "<br>" + item.city + " " + item.state + " " + item.zip + "</td>";
                    } else {
                        table += "<td>" + item.first + "  " + item.last + "</br>" + item.address1 + "<br>" + item.city + " " + item.state + " " + item.zip + "</td></tr></tbody>";
                    }
                    i++;
                });
            }
        }

        $('#new_table tbody').append(table);
        table += "</table>";
        if (print_option != '11') {
            if (print_option == "6") {
                $(table).printThis({
                    debug: true,
                    header: "<h3><center>Intake/PNC Report</center></h3>",
                    importCSS: true,
                    loadCSS: '<?php echo base_url('assets') . '/css/landscape.css'; ?>',
                    printContainer: false

                });
            } else {
                $(table).printThis({
                    debug: true,
                    header: "<h3><center>Intake/Prospect Report</center></h3>",

                });
            }
        }
        var count = 0;
        if (print_option == '11') {
            var email_arr = [];
            $.each(ProspectAjaxSearchData.data, function (index, item) {
                if (item.email.length > 0) {
                    count++;
                    email_arr.push(item.email);
                }
            });
            $('#copytoemail').text(email_arr);

            swal({
                text: "Would you like to copy all Emails in this report to the windows cilpboard?",
                title: "Copy to Cilpboard",
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                showCancelButton: true,
                closeOnConfirm: false
            }, function () {
                swal({
                    text: "Total record in the PNC:" + ProspectAjaxSearchData.data.length + "\n Total PNC with an email that ware copied to cilpboard:" + count,
                    title: "Success!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            text: 'Successfully copied to cilpboard, you can paste these cilpboard data',
                            title: "Success!",
                            timer: 5000
                        });
                    }

                    var aux = document.createElement("input");
                    aux.setAttribute("value", document.getElementById('copytoemail').innerHTML);
                    document.body.appendChild(aux);
                    aux.select();
                    document.execCommand("copy");
                });
            });
        }
    });
    $(document.body).on('click', '.printenvelopes', function () {
        return_address = '';
        if ($("#re_check").prop('checked') == true) {
            var return_address = $('#env_return_address').val();
        }
        var addrss = '';

        addrss += "<div style='margin: 150px auto;display: table;' class='test'><span style='margin-left: -50px;'>" + return_address + "</span></br>" + $('.pro_first').html() + "</br>" + $('.pro_address').html() + "</br>" + $('.pro_city_st_zip').html() + "</div>";
        $(addrss).printThis({
            debug: false,
            header: null,
            footer: null
        });
    });

    $('#addcase.modal').on('hidden.bs.modal', function () {
        $(this).find('input,textarea,select').each(function () {
            this.value = '';
            $(this).iCheck('uncheck');
            $(this).trigger('change');
        });
    });

    $(document.body).on('click', '.delete_prospect', function () {
        var prospect_id = $('.delete_prospect').data('pros_id');
        if ($('.delete_prospect').attr('data-status') == 1) {
            swal({
                text: "This prospect is already deleted",
                type: "success",
                title: "Alert",
                closeOnConfirm: false
            }, function () {
                swal({
                    title: "Would  you like to recall this prospect",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes,recall it!",
                    closeOnConfirm: false
                }, function () {
                    $.blockUI();
                    $.ajax({
                        url: '<?php echo base_url(); ?>prospects/deleteRec',
                        data: {ProspectId: prospect_id, status: 0},
                        method: "POST",
                        success: function (result) {
                            $.unblockUI();
                            var data = $.parseJSON(result);
                            if (data.status == 1) {
                                $('.prospect_status').text('');
                                $('.delete_prospect').attr('data-status', 0);
                                swal("Success !", 'Prospect successfully recalled.', "success");
                            } else {
                                swal("Oops...", data.message, "error");
                            }
                        }
                    });
                });
            });
        } else {
            swal({
                title: "Are you sure to Remove this prospect?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>prospects/deleteRec',
                    data: {ProspectId: prospect_id, status: 1},
                    method: "POST",
                    success: function (result) {
                        $.unblockUI();
                        var data = $.parseJSON(result);
                        if (data.status == 1) {
                            $('.prospect_status').text('Deleted');
                            $('.delete_prospect').attr('data-status', 1);
                            swal("Success !", data.message, "success");
                        } else {
                            swal("Oops...", data.message, "error");
                        }
                    }
                });
            });
        }
    });
    $(document.body).on('click', '#printProspectdetails', function () {
        $('#printProspectmodal').modal('show');
    });


    $(document.body).on('click', '#search_prospect', function () {
        var alldata = $("form#search_prospectForm").serialize();
        $.ajax({
            url: '<?php echo base_url(); ?>prospects/searchForProspects',
            method: 'POST',
            data: alldata,
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (data) {
                ProspectListDatatable.clear();
                var res = $.parseJSON(data);
                ProspectAjaxSearchData = res;
                if (res.status == '0') {
                    swal({
                        title: "No record found",
                        type: "warning"
                    });
                } else {
                    ProspectListDatatable.clear();
                    var pro_i = 0;
                    $.each(res.data, function (index, item) {
                        pro_i = pro_i + 1;
                        var fieldsudf = item.fieldsudf.split(',');
                        var html = '';
                        var caseno = '';
                        if (typeof item.caseno !== 'undefined') {
                            caseno = item.caseno;
                        } else {
                            caseno = "";
                        }
                        html += "<tr class='selectedData' id='" + pro_i + "' data-prospect='" + item.prospect_id + "' data-caseno='" + caseno + "' data-prosno='" + item.prosno + "' data-proskey='" + item.proskey + "'>";
                        if (item.dateref != '0000-00-00 00:00:00') {
                            html += "<td class='dateref'>" + moment(item.dateref).format('MM/DD/YYYY');
                            +"</td>";
                        } else {
                            html += "<td class='dateref'></td>";
                        }
                        html += "<td class='pendwith'>" + item.pendwith + "</td>";
                        html += "<td class='atty'>" + item.atty + "</td>";
                        html += "<td class='first'>" + item.first + "</td>";
                        html += "<td class='last'>" + item.last + "</td>";
                        html += "<td class='initials0'>" + item.initials0 + "</td>";
                        html += "<td class='casetype'>" + item.casetype + "</td>";
                        html += "<td class='casestat'>" + item.casestat + "</td>";
                        html += "<td class='datelastcn'>" + moment(item.datelastcn).format('MM/DD/YYYY');
                        +"</td>";
                        html += "<td class='city'>" + item.city + "</td>";
                        html += "<td class='state'>" + item.state + "</td>";
                        html += "<td class='zip'>" + item.zip + "</td>";
                        html += "<td class='refcode'>" + item.refcode + "</td>";
                        html += "<td class='refsource'>" + item.refsource + "</td>";
                        html += "<td class='refby' >" + item.refby + "</td>";
                        html += "<td class='refto'>" + item.refto + "</td>";
                        html += "<td class='reffee'>" + fieldsudf[0] + "</td>";
                        html += "<td class='outrefstat'>" + item.outrefstat + "</td>";
                        html += "<td class='amount'>" + item.amount + "</td>";
                        if (item.datepaid != '0000-00-00 00:00:00') {
                            html += "<td class='datepaid'>" + moment(item.datepaid).format('MM/DD/YYYY');
                            +"</td>";
                        } else {
                            html += "<td class='datepaid'></td>";
                        }

                        html += "<td class='todonext'>" + item.todonext + "</td>";
                        html += "<td hidden class='mailingh'>" + item.mailingh + "</td>";
                        html += "<td hidden class='mailing1'>" + item.mailing1 + "</td>";
                        html += "<td hidden class='mailing2'>" + item.mailing2 + "</td>";
                        html += "<td hidden class='mailing3'>" + item.mailing3 + "</td>";
                        html += "<td hidden class='mailing4'>" + item.mailing4 + "</td>";
                        html += "<td hidden class='mailing5'>" + item.mailing5 + "</td>";
                        html += "<td hidden class='email'>" + item.email + "</td>";
                        html += "<td hidden class='cell'>" + item.cell + "</td>";
                        html += "<td hidden class='home'>" + item.home + "</td>";
                        html += "<td hidden class='business'>" + item.business + "</td>";
                        html += "<td hidden class='address'>" + item.address1 + "</td>";
                        html += "<td hidden class='social_sec'>" + item.social_sec + "</td>";
                        var dob = new Date(item.birthdate);
                        var today = new Date();
                        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                        if (item.birthdate != '0000-00-00 00:00:00') {
                            html += "<td hidden class='birth_date'>" + moment(item.birthdate).format('MM/DD/YYYY') + "</td>";
                        } else {
                            html += "<td hidden class='birth_date'></td>";
                        }
                        html += "<td hidden class='type'>" + item.type + "</td>";
                        html += "<td hidden class='daterefin'>" + moment(item.daterefin).format('MM/DD/YYYY');
                        +"</td>";
                        html += "<td hidden class='salutation'>" + item.salutation + "</td>";
                        html += "<td hidden class='suffix'>" + item.suffix + "</td>";
                        html += "<td hidden class='emp_inj'>" + fieldsudf[1] + "</td>";
                        html += "<td hidden class='emp_inj_date'>" + fieldsudf[2] + "</td>";
                        html += "<td hidden class='emp_body'>" + fieldsudf[3] + "</td>";
                        html += "<td hidden class='followup_datel'>" + fieldsudf[5] + "</td>";
                        html += "<td hidden class='accociation'>" + fieldsudf[4] + "</td>";

                        html += "</tr>";
                        console.log(html);
                        ProspectListDatatable.row.add($(html));
                    });
                    ProspectListDatatable.draw();
                    $('#ProspectListDatatable tbody tr:eq(0)').addClass("table-selected");
                    var row = ProspectListDatatable.row('.table-selected').node();
                    prospect_detail(row);
                    $('#ListProspects').modal('show');
                }
            }
        });
        return false;
    });
    $(document.body).on('click', '.edit_prospect', function () {
        var prospect_id = $(this).data('prospect_id');

        setProspectvalue(ProspectAjaxData, 'edit');
        $('.nav-tabs a[href="#tab-6"]').tab('show');
    });


    function prospect_detail(row) {
        $('.pro_first_new').html($(row).closest('tr').find('.first').text() + " " + $(row).closest('tr').find('.last').text());
        $('.pro_cell_new').html($(row).closest('tr').find('.cell').text());
        $('.pro_home_new').html($(row).closest('tr').find('.home').text());
        $('.pro_business_new').html($(row).closest('tr').find('.business').text());
        $('.pro_address_new').html($(row).closest('tr').find('.address').text());
        $('.pro_city_st_zip_new').html($(row).closest('tr').find('.city').text() + " " + $(row).closest('tr').find('.state').text() + " " + $(row).closest('tr').find('.zip').text());
        $('.pro_social_sec_new').html($(row).closest('tr').find('.social_sec').text());
        $('.pro_birthdate_new').html($(row).closest('tr').find('.birth_date').text());
        $('.pro_email_new').html($(row).closest('tr').find('.email').text());
        $('.pro_type_new').html($(row).closest('tr').find('.type').text());
        $('.pro_casestat_new').html($(row).closest('tr').find('.casestat').text());
        $('.pro_daterefin_new').html($(row).closest('tr').find('.daterefin').text());
        $('.pro_datelastcn_new').html($(row).closest('tr').find('.datelastcn').text());
        $('.pro_initials0_new').html($(row).closest('tr').find('.initials0').text());
        $('.pro_pendwith_new').html($(row).closest('tr').find('.pendwith').text());
        $('.pro_atty_new').html($(row).closest('tr').find('.atty').text());
        $('.pro_refcode_new').html($(row).closest('tr').find('.refcode').text());
        $('.pro_refsource_new').html($(row).closest('tr').find('.refsource').text());
        $('.pro_refby_new').html($(row).closest('tr').find('.refby').text());
        $('.pro_assciation_new').html($(row).closest('tr').find('.accociation').text());
        $('.pro_refto_new').html($(row).closest('tr').find('.refto').text());
        $('.pro_dateref_new').html($(row).closest('tr').find('.dateref').text());
        $('.pro_outrefstat_new').html($(row).closest('tr').find('.outrefstat').text());
        $('.pro_ref_fee_new').html($(row).closest('tr').find('.reffee').text());
        $('.pro_amount_new').html($(row).closest('tr').find('.amount').text());
        $('.date_follow_paid_new').html($(row).closest('tr').find('.datepaid').text() + " - " + $(row).closest('tr').find('.followup_datel').text());
    }
    $(document.body).on('click', '.edit_prospect_select', function () {
        $('.nav-tabs a[href="#tab-6"]').tab('show');
        var row = ProspectListDatatable.row('.table-selected').node();
        $('#printProspectmodal').modal('hide');
        $('#ListProspects').modal("hide");
        $('#prospectId').val($(row).closest('tr').attr('data-prospect'));
        $('select[name=pro_salutation]').val($(row).closest('tr').find('.salutation').text());
        $('#pro_first').val($(row).closest('tr').find('.first').text());
        $('#pro_last').val($(row).closest('tr').find('.last').text());
        $('select[name=pro_suffix]').val($(row).closest('tr').find('.suffix').text());
        $('#pro_cell').val($(row).closest('tr').find('.cell').text());
        $('#pro_business').val($(row).closest('tr').find('.business').text());
        $('#pro_home').val($(row).closest('tr').find('.home').text());
        $('#pro_address1').val($(row).closest('tr').find('.address').text());
        $('#pro_city').val($(row).closest('tr').find('.city').text());
        $('#pro_state').val($(row).closest('tr').find('.state').text());
        $('#pro_zip').val($(row).closest('tr').find('.zip').text());
        $('#pro_social_sec').val($(row).closest('tr').find('.social_sec').text());
        $('#pro_email').val($(row).closest('tr').find('.email').text());
        if (($(row).closest('tr').find('.birth_date').text()) != '') {
            $('#pro_birthdate').val(moment($(row).closest('tr').find('.birth_date').text()).format('MM/DD/YYYY'));
        } else {
            $('#pro_birthdate').val('');
        }
        if (($(row).closest('tr').find('.dateref').text()) != '') {
            $('#pro_dateref').val(moment($(row).closest('tr').find('.dateref').text()).format('MM/DD/YYYY'));
        } else {
            $('#pro_dateref').val('');
        }
        $('select[name=pro_type]').val($(row).closest('tr').find('.type').text());
<?php if (isset($casetypedrp) && $casetypedrp == 1) { ?>
            $('#pro_casetype').val($(row).closest('tr').find('.casetype').text());
<?php } else { ?>
            $('select[name=pro_casetype]').val($(row).closest('tr').find('.casetype').text());
<?php } ?>
        $('select[name=pro_initials0]').val($(row).closest('tr').find('.initials0').text());
<?php if (isset($casestatusdrp) && $casestatusdrp == 1) { ?>
            $('#pro_casestat').val($(row).closest('tr').find('.casestat').text());
<?php } else { ?>
            $('select[name=pro_casestat]').val($(row).closest('tr').find('.casestat').text());
<?php } ?>
        $('select[name=pro_pendwith]').val($(row).closest('tr').find('.pendwith').text());
        $('#pro_daterefin').val(moment($(row).closest('tr').find('.daterefin').text()).format('MM/DD/YYYY'));

        $('#pro_datelastcn').val(moment($(row).closest('tr').find('.datelastcn').text()).format('MM/DD/YYYY'));
        $('select[name=pro_atty]').val($(row).closest('tr').find('.atty').text());
        if ($(row).closest('tr').find('.mailingh').text() == 1) {
            $('input[name=pro_mailingh]').iCheck('check');
        } else {
            $('input[name=pro_mailingh]').iCheck('uncheck');
        }

        if ($(row).closest('tr').find('.mailing1').text() == 1) {
            $('input[name=pro_mailing1]').iCheck('check');
        } else {
            $('input[name=pro_mailing1]').iCheck('uncheck');
        }

        if ($(row).closest('tr').find('.mailing2').text() == 1) {
            $('input[name=pro_mailing2]').iCheck('check');
        } else {
            $('input[name=pro_mailing2]').iCheck('uncheck');
        }

        if ($(row).closest('tr').find('.mailing3').text() == 1) {
            $('input[name=pro_mailing3]').iCheck('check');
        } else {
            $('input[name=pro_mailing3]').iCheck('uncheck');
        }

        if ($(row).closest('tr').find('.mailing4').text() == 1) {
            $('input[name=pro_mailing4]').iCheck('check');
        } else {
            $('input[name=pro_mailing4]').iCheck('uncheck');
        }

        if ($(row).closest('tr').find('.mailing5').text() == 1) {
            $('input[name=pro_mailing5]').iCheck('check');
        } else {
            $('input[name=pro_mailing5]').iCheck('uncheck');
        }
        $('#pro_todonext').val($(row).closest('tr').find('.todonext').text());
<?php if (isset($casereferraldrp) && $casereferraldrp == 1) { ?>
            $('#pro_refcode').val($(row).closest('tr').find('.refcode').text());
<?php } else { ?>
            $('select[name=pro_refcode]').val($(row).closest('tr').find('.refcode').text());
<?php } ?>
<?php if (isset($referredtodrp) && $referredtodrp == 1) { ?>
            $('#pro_refto').val($(row).closest('tr').find('.refto').text());
<?php } else { ?>
            $('select[name=pro_refto]').val($(row).closest('tr').find('.refto').text());
<?php } ?>
<?php if (isset($referralsrcdrp) && $referralsrcdrp == 1) { ?>
            $('#pro_refsource').val($(row).closest('tr').find('.refsource').text());
<?php } else { ?>
            $('select[name=pro_refsource]').val($(row).closest('tr').find('.refsource').text());
<?php } ?>
        $('#pro_per_reffee').val($(row).closest('tr').find('.reffee').text());
<?php if (isset($referredbydrp) && $referredbydrp == 1) { ?>
            $('#pro_refby').val($(row).closest('tr').find('.refby').text());
<?php } else { ?>
            $('select[name=pro_refby]').val($(row).closest('tr').find('.refby').text());
<?php } ?>

<?php if (isset($referredbydrp) && $referredbydrp == 1) { ?>
            $('#pro_accociation').val($(row).closest('tr').find('.accociation').text());
<?php } else { ?>
            $('select[name=pro_accociation]').val($(row).closest('tr').find('.accociation').text());
<?php } ?>

        $('select[name=pro_reffee]').val($(row).closest('tr').find('.reffee').text());
        $('#pro_amount').val($(row).closest('tr').find('.amount').text());

        if (($(row).closest('tr').find('.datepaid').text()) != '') {
            $('#pro_datepaid').val($(row).closest('tr').find('.datepaid').text());
        } else {
            $('#pro_datepaid').val('');
        }
        $('select[name=pro_outrefstat]').val($(row).closest('tr').find('.outrefstat').text());

        $('select[name=pro_outrefstat]').val($(row).closest('tr').find('.outrefstat').text());
        $('#pro_followup_date').val($(row).closest('tr').find('.followup_date').text());

        $('#pro_injn_emp').val($(row).closest('tr').find('.emp_inj').text());
        $('#pro_injn_date').val($(row).closest('tr').find('.emp_inj_date').text());
        $('#pro_injn_body').val($(row).closest('tr').find('.emp_body').text());
        $('#pro_caseno').val($(row).closest('tr').attr('data-caseno'));
        $('#pro_prosno').val($(row).closest('tr').attr('data-prosno'));
        $('#pro_proskey').val($(row).closest('tr').attr('data-proskey'));
        $('.titlemodal').html('Edit ');
        $('#proceedprospects').modal('show');
        $("#prospectForm select").trigger('change');
    });

    $(document.body).on('click', '.print_prospect', function () {
        $('#printoption').modal('show');
    });

    $(document.body).on('click', '#save_prospect', function () {
        if ($("#prospectForm").valid()) {
            $.blockUI();
            var url = '';
            if ($('#prospectId').val() != '' && $('#prospectId').val() != 0) {
                url = '<?php echo base_url(); ?>prospects/editProspects';
            } else {
                url = '<?php echo base_url(); ?>prospects/addProspects';
            }
            $.ajax({
                url: url,
                method: "POST",
                data: $('#prospectForm').serialize(),
                success: function (result) {
                    $.unblockUI();
                    var data = $.parseJSON(result);
                    if (data.status == '1') {
                        location.reload();
                    }

                    $('#prospectForm')[0].reset();
                    $('#proceedprospects').modal('hide');
                }
            });
            return false;
        }
    });

    function setProspectvalue(data, type) {
        if (typeof (data) == 'undefined') {
            if (type == 'edit') {
                $('.titlemodal').html('Edit ');
                $('#prospectId').val('<?php echo isset($record[0]->prospect_id) ? $record[0]->prospect_id : ''; ?>');
            }
            var fieldsudf = Prospect_data.fieldsudf;
            var fieldsudf = fieldsudf.split(',');

            $('select[name=pro_salutation]').val(Prospect_data.salutation);
            $('#pro_first').val(Prospect_data.first);
            $('#pro_last').val(Prospect_data.last);
            $('select[name=pro_suffix]').val(Prospect_data.suffix);
            $('#pro_cell').val(Prospect_data.cell);
            $('#pro_business').val(Prospect_data.business);
            $('#pro_home').val(Prospect_data.home);
            $('#pro_address1').val(Prospect_data.address1);
            $('#pro_city').val(Prospect_data.city);
            $('#pro_state').val(Prospect_data.state);
            $('#pro_zip').val(Prospect_data.zip);
            $('#pro_social_sec').val(Prospect_data.social_sec);
            $('#pro_email').val(Prospect_data.email);
            if (Prospect_data.birthdate != '0000-00-00 00:00:00') {
                $('#pro_birthdate').val(moment(Prospect_data.birthdate).format('MM/DD/YYYY'));
            } else {
                $('#pro_birthdate').val('');
            }
            $('select[name=pro_type]').val(Prospect_data.type);
            $('select[name=pro_casetype]').val(Prospect_data.casetype);
            $('select[name=pro_initials0]').val(Prospect_data.initials0);
            $('select[name=pro_casestat]').val(Prospect_data.casestat);
            $('select[name=pro_pendwith]').val(Prospect_data.pendwith);
            if (Prospect_data.daterefin != '0000-00-00 00:00:00') {
                $('#pro_daterefin').val(moment(Prospect_data.daterefin).format('MM/DD/YYYY'));
            } else {
                $('#pro_daterefin').val('');
            }
            $('#pro_datelastcn').val(moment(Prospect_data.datelastcn).format('MM/DD/YYYY'));
            $('select[name=pro_atty]').val(Prospect_data.atty);
<?php if ($casetypedrp == 1) { ?>
                $('#pro_casetype').val(Prospect_data.casetype);
<?php } else { ?>
                $('select[name=pro_casetype]').val(Prospect_data.casetype);
<?php } ?>
            $('select[name=pro_initials0]').val(Prospect_data.initials0);
<?php if ($casestatusdrp == 1) { ?>
                $('#pro_casestat').val(Prospect_data.casestat);
<?php } else { ?>
                $('select[name=pro_casestat]').val(Prospect_data.casestat);
<?php } ?>
            if (Prospect_data.mailingh == 1) {
                $('input[name=pro_mailingh]').iCheck('check');
            } else {
                $('input[name=pro_mailingh]').iCheck('uncheck');
            }
            $('#pro_todonext').val(Prospect_data.todonext);

<?php if ($casereferraldrp == 1) { ?>
                $('#pro_refcode').val(Prospect_data.refcode);
<?php } else { ?>
                $('select[name=pro_refcode]').val(Prospect_data.refcode);
<?php } ?>
<?php if ($referredtodrp == 1) { ?>
                $('#pro_refto').val(Prospect_data.refto);
<?php } else { ?>
                $('select[name=pro_refto]').val(Prospect_data.refto);
<?php } ?>
<?php if ($referralsrcdrp == 1) { ?>
                $('#pro_refsource').val(Prospect_data.refsource);
<?php } else { ?>
                $('select[name=pro_refsource]').val(Prospect_data.refsource);
<?php } ?>
            if (Prospect_data.dateref != '0000-00-00 00:00:00') {
                $('#pro_dateref').val(moment(Prospect_data.dateref).format('MM/DD/YYYY'));
            } else {
                $('#pro_dateref').val('');
            }
            $('#pro_per_reffee').val(fieldsudf[6]);
<?php if ($referredbydrp == 1) { ?>
                $('#pro_refby').val(Prospect_data.refby);
<?php } else { ?>
                $('select[name=pro_refby]').val(Prospect_data.refby);
<?php } ?>
            $('select[name=pro_reffee]').val(fieldsudf[0]);
            $('#pro_amount').val(Prospect_data.amount);
<?php if ($associationdrp == 1) { ?>
                $('#pro_accociation').val(fieldsudf[4]);
<?php } else { ?>
                $('select[name=pro_accociation]').val(fieldsudf[4]);
<?php } ?>

            if (Prospect_data.datepaid != '0000-00-00 00:00:00') {
                $('#pro_datepaid').val(moment(Prospect_data.datepaid).format('MM/DD/YYYY'));
            } else {
                $('#pro_datepaid').val('');
            }
            $('#pro_followup_date').val(fieldsudf[5]);
            $('#pro_notes').val(Prospect_data.notes);
            $('#pro_injn_emp').val(fieldsudf[1]);
            $('#pro_injn_date').val(fieldsudf[2]);
            $('#pro_injn_body').val(fieldsudf[3]);
            if (Prospect_data.caseno != 0) {
                $('#pro_caseno').val(Prospect_data.caseno);
            } else {
                $('#pro_caseno').val('');
            }
            if (type == 'clone') {
                $('#pro_prosno').val('<?php echo isset($record[0]->proskey) ? $record[0]->proskey : ''; ?>');
                $('#pro_proskey').val('<?php echo isset($record[0]->proskey) ? $record[0]->proskey : ''; ?>');
                $('.titlemodal').html('Add ');
            } else {
                $('#pro_prosno').val(Prospect_data.prosno);
                $('#pro_proskey').val(Prospect_data.proskey);
            }
        } else {
            $('#prospectId').val(data.data.prospect_id);
            var fieldsudf = data.data.fieldsudf.split(',');

            $('select[name=pro_salutation]').val(data.data.salutation);
            $('#pro_first').val(data.data.first);
            $('#pro_last').val(data.data.last);
            $('select[name=pro_suffix]').val(data.data.suffix);
            $('#pro_cell').val(data.data.cell);
            $('#pro_business').val(data.data.business);
            $('#pro_home').val(data.data.home);
            $('#pro_address1').val(data.data.address1);
            $('#pro_city').val(data.data.city);
            $('#pro_state').val(data.data.state);
            $('#pro_zip').val(data.data.zip);
            $('#pro_social_sec').val(data.data.social_sec);
            $('#pro_email').val(data.data.email);
            if (data.data.birthdate != '0000-00-00 00:00:00') {
                $('#pro_birthdate').val(moment(data.data.birthdate).format('MM/DD/YYYY'));
            } else {
                $('#pro_birthdate').val('');
            }
            $('select[name=pro_type]').val(data.data.type);
            $('select[name=pro_casetype]').val(data.data.casetype);
            $('select[name=pro_initials0]').val(data.data.initials0);
            $('select[name=pro_casestat]').val(data.data.casestat);
            $('select[name=pro_pendwith]').val(data.data.pendwith);
            if (data.data.daterefin != '0000-00-00 00:00:00') {
                $('#pro_daterefin').val(moment(data.data.daterefin).format('MM/DD/YYYY'));
            } else {
                $('#pro_daterefin').val('');
            }
            $('#pro_datelastcn').val(moment(data.data.datelastcn).format('MM/DD/YYYY'));
            $('select[name=pro_atty]').val(data.data.atty);
<?php if ($casetypedrp == 1) { ?>
                $('#pro_casetype').val(data.data.casetype);
<?php } else { ?>
                $('select[name=pro_casetype]').val(data.data.casetype);
<?php } ?>
            $('select[name=pro_initials0]').val(data.data.initials0);
<?php if ($casestatusdrp == 1) { ?>
                $('#pro_casestat').val(data.data.casestat);
<?php } else { ?>
                $('select[name=pro_casestat]').val(data.data.casestat);
<?php } ?>
            if (data.data.mailingh == 1) {
                $('input[name=pro_mailingh]').iCheck('check');
            } else {
                $('input[name=pro_mailingh]').iCheck('uncheck');
            }
            $('#pro_todonext').val(data.data.todonext);
<?php if ($casereferraldrp == 1) { ?>
                $('#pro_refcode').val(data.data.refcode);
<?php } else { ?>
                $('select[name=pro_refcode]').val(data.data.refcode);
<?php } ?>
<?php if ($referredtodrp == 1) { ?>
                $('#pro_refto').val(data.data.refto);
<?php } else { ?>
                $('select[name=pro_refto]').val(data.data.refto);
<?php } ?>
<?php if ($referralsrcdrp == 1) { ?>
                $('#pro_refsource').val(data.data.refsource);
<?php } else { ?>
                $('select[name=pro_refsource]').val(data.data.refsource);
<?php } ?>
            if (data.data.dateref != '0000-00-00 00:00:00') {
                $('#pro_dateref').val(moment(data.data.dateref).format('MM/DD/YYYY'));
            } else {
                $('#pro_dateref').val('');
            }
            $('#pro_per_reffee').val(fieldsudf[6]);
<?php if ($referredbydrp == 1) { ?>
                $('#pro_refby').val(data.data.refby);
<?php } else { ?>
                $('select[name=pro_refby]').val(data.data.refby);
<?php } ?>
            $('select[name=pro_reffee]').val(fieldsudf[0]);
            $('#pro_amount').val(data.data.amount);
<?php if ($associationdrp == 1) { ?>
                $('#pro_accociation').val(fieldsudf[4]);
<?php } else { ?>
                $('select[name=pro_accociation]').val(fieldsudf[4]);
<?php } ?>

            if (data.data.datepaid != '0000-00-00 00:00:00') {
                $('#pro_datepaid').val(moment(data.data.datepaid).format('MM/DD/YYYY'));
            } else {
                $('#pro_datepaid').val('');
            }
            $('select[name=pro_outrefstat]').val(data.data.outrefstat);
            $('#pro_followup_date').val(fieldsudf[5]);
            $('#pro_notes').val(data.data.notes);
            $('#pro_injn_emp').val(fieldsudf[1]);
            $('#pro_injn_date').val(fieldsudf[2]);
            $('#pro_injn_body').val(fieldsudf[3]);
            if (data.data.caseno != 0) {
                $('#pro_caseno').val(data.data.caseno);
            } else {
                $('#pro_caseno').val('');
            }
            if (type == 'clone') {
                $('#pro_prosno').val('<?php echo isset($record[0]->proskey) ? $record[0]->proskey : ''; ?>');
                $('#pro_proskey').val('<?php echo isset($record[0]->proskey) ? $record[0]->proskey : ''; ?>');
                $('.titlemodal').html('Add ');
            } else {
                $('#pro_prosno').val(data.data.prosno);
                $('#pro_proskey').val(data.data.proskey);
                $('.titlemodal').html('Edit ');
            }
        }
        $('#proceedprospects').modal('show');
        $("#prospectForm select").trigger('change');
    }

    $(document.body).on('click', '#printProspectdetails', function () {
        $('#printProspectmodal').modal('show');
    });

    $(document).on('dblclick', '#ProspectListDatatable tbody tr', function () {
        $('#ListProspects').modal('hide');
        $('#printProspectmodal').modal('hide');
        var record = '<?php echo json_encode($record); ?>';
        var current = $(this).data('prospect');
        if (current != '') {
            record = JSON.parse(record);
            var arr = new Array();
            var privous = '';
            var next = '';
            $.each(record, function (k, v) {
                arr.push(v.prospect_id);
            });
            if ($(this).attr('data-type') == 'previous') {
                $.each(arr, function (index, value) {
                    if (current == value) {
                        if (index == 0) {
                            swal({
                                title: "Alert",
                                text: "You Are At Current Prospect",
                                type: "success"
                            });
                        } else {
                            privous = parseInt(index - 1);
                            next = parseInt(index);
                        }
                    }
                });
            } else {
                $.each(arr, function (index, value) {
                    if (current == value) {
                        if (arr.length == parseInt(index) + 1) {
                            swal({
                                title: "Alert",
                                text: "You Are At Last Prospect",
                                type: "success"
                            });
                        } else {
                            privous = parseInt(index);
                            next = parseInt(index) + 1;
                        }
                    }
                });
            }
            $('.Privous').attr("data-id", arr[privous]);
            $('.Next').attr("data-id", arr[next]);
            $.ajax({
                url: '<?php echo base_url(); ?>Prospects/getProspectsdata',
                method: "POST",
                data: {prospect_id: current},
                beforeSend: function (xhr) {
                },
                complete: function (jqXHR, textStatus) {
                },
                success: function (result) {
                    var data = $.parseJSON(result);
                    ProspectAjaxData = data;
                    var fieldsudf = data.data.fieldsudf.split(',');
                    var name = data.data.salutation + ' ' + data.data.first + ' ' + data.data.last;
                    var st_zip = data.data.city + " " + data.data.state + " " + data.data.zip;
                    $('.edit_prospect').attr("data-prospect_id", current);
                    $('.edit_prospect').attr("data-case_no", data.data.caseno);
                    $('.delete_prospect').attr("data-pros_id", current);
                    $('.delete_prospect').attr("data-status", data.data.status);
                    if (data.data.status == '1') {
                        $('.prospect_status').html("Deleted");
                    } else {
                        $('.prospect_status').html("");
                    }
                    $('.task_list').attr("data-proskey_id", data.data.proskey);
                    $('.pro_first1').text(data.data.first);
                    $('.pro_last1').text(data.data.last);
                    $('.pro_first').html(name);
                    $('.pro_cell').html(data.data.cell);
                    $('.pro_home').html(data.data.home);
                    $('.pro_business').html(data.data.business);
                    $('.pro_address').html(data.data.address1);
                    $('.pro_city_st_zip').html(st_zip);
                    $('.pro_social_sec').html(data.data.social_sec);
                    var dob = new Date(data.data.birthdate);
                    var today = new Date();
                    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
                    if (data.data.birthdate != '0000-00-00 00:00:00') {
                        $('.pro_birthdate').html(moment(data.data.birthdate).format('MM/DD/YYYY'));
                    } else {
                        $('.pro_birthdate').html('');
                    }
                    $('.pro_email').html(data.data.email);
                    $('.pro_type').html(data.data.type);
                    $('.pro_casestat').html(data.data.casestat);
                    $('.pro_daterefin').html(moment(data.data.daterefin).format('MM/DD/YYYY'));
                    $('.pro_datelastcn').html(moment(data.data.datelastcn).format('MM/DD/YYYY'));
                    $('.pro_initials0').html(data.data.initials0);
                    $('.pro_pendwith').html(data.data.pendwith);
                    $('.pro_atty').html(data.data.atty);
                    if (data.data.mailingh == 'Y' || data.data.mailingh == '1') {
                        $('.pro_mailingh').html('Y');
                    } else {
                        $('.pro_mailingh').html("");
                    }
                    $('.pro_refcode').html(data.data.refcode);
                    $('.pro_refsource').html(data.data.refsource);
                    $('.pro_refby').html(data.data.refby);
                    $('.pro_assciation').html(fieldsudf[5]);
                    $('.pro_refto').html(data.data.refto);
                    if (data.data.dateref != '0000-00-00 00:00:00') {
                        $('.pro_dateref').html(moment(data.data.dateref).format('MM/DD/YYYY'));
                    } else {
                        $('.pro_dateref').html('');
                    }
                    $('.pro_outrefstat').html(data.data.outrefstat);
                    $('.pro_ref_fee').html(fieldsudf[0]);
                    $('.pro_amount').html(data.data.amount);
                    if (data.data.datepaid != '0000-00-00 00:00:00') {
                        $('.date_follow_paid').html(moment(data.data.datepaid).format('MM/DD/YYYY') + " - " + fieldsudf[5]);
                    } else {
                        $('.date_follow_paid').html('');
                    }

                    $('.pro_notes').text(data.data.notes);
                    $('.pro_injn_emp').html(fieldsudf[1]);
                    $('.pro_injn_date').html(fieldsudf[2]);
                    $('.pro_injn_body').html(fieldsudf[3]);
                    $('#env_address').text(name + ',' + data.data.suffix + "\n" + data.data.address1 + "\n" + st_zip);
                    return false;
                }
            });
        }
    });
</script>