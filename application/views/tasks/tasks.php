<input type="hidden" id="caseNo" value="<?php echo $caseNo; ?>"/>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <?php if ($caseNo) { ?>
                    <a href="<?php echo base_url() . 'dashboard'; ?>">Home</a> /
                    <a href="<?php echo base_url() . 'cases'; ?>">Cases</a> /
                    <a href="<?php echo base_url('cases/case_details/' . $caseNo); ?>"> <?php echo $caseNo; ?></a> /
                    <span>Tasks</span>
                <?php } else { ?>
                    <a href="<?php echo base_url() . 'dashboard'; ?>">Home</a> / <span>Tasks</span>
                <?php } ?>
            </div>
            <?php if (isset($caseNo) && empty($caseNo)) { ?>
                <ul id="task-navigation" class="nav nav-pills">
                    <li id="whoto" class="whoto active"><a href="javascript: void(0);" ><span class="glyphicon glyphicon-home"></span>  Task For <?php echo $this->session->userdata('user_data')['username']; ?></a></li>
                    <li id="whofrom" class="whofrom"><a href="javascript: void(0);" ><span class="glyphicon glyphicon-user"></span> Task From <?php echo $this->session->userdata('user_data')['username']; ?></a></li>
                </ul>
            <?php } ?>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if (isset($caseNo) && !empty($caseNo)) { ?>
                        <h5>Task For Case <?php echo $caseNo; ?></h5>
                    <?php } else { ?>
                        <h5>Task Delegated <span id="changeforFrom">For</span> <?php echo $this->session->userdata('user_data')['username']; ?></h5>
                    <?php } ?>
                    <a class="btn pull-right btn-primary btn-sm marg-left5" title="Print" href="#printtask" data-backdrop="static" data-keyboard="false" data-toggle="modal"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                    <a class="btn pull-right btn-primary btn-sm" id="addtaskModal" title="Add Task">Add Task</a>
                </div>
                <div class="ibox-content marg-bot15">
                    <div class="filters">
                        <div class="form-group">
                            <label>Priority</label>
                            <select class="form-control" id="getPriority" name="priority">
                                <option value="">All</option>
                                <option value="3">Low</option>
                                <option value="2">Medium</option>
                                <option value="1">High</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Task Status</label>
                            <select class="form-control" id="gettaskStatus" name="completed">
                                <option value="all">All</option>
                                <option selected value="pending">Pending</option>
                                <option value="completed">Completed</option>
                            </select>
                        </div>
                        <label class="container-checkbox pull-right">Reminder
                            <input type="checkbox" id="task_reminder" onchange="return getremindertasks();">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="">
                        <table class="table table-bordered task-dataTable table-striped table-hover">
                            <thead>
                                <tr>
                                    <th id="th-date text-center" width="10%">Date</th>
                                    <th id="th-event text-center" width="20%" class="caseact_event">Event</th>
                                    <th id="th-from text-center" width="5%">From</th>
                                    <th id="th-to text-center" width="5%">To</th>
                                    <th id="th-finishby text-center" width="10%">Finish By</th>
                                    <th id="th-type text-center" width="5%">Type</th>
                                    <th id="th-priority text-center" width="5%">Priority</th>
                                    <th id="th-event text-center" width="20%">Reminder</th>
									<th id="th-completed text-center" width="10%">Completed</th>
                                    <th id="th-action text-center" width="10%">Actions</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$url_segment = $this->uri->segment(1);
if ($url_segment == 'tasks_new' || $url_segment == 'tasks_New') {
?>
<input type="hidden" name="taskmodule" id="taskmodule" value="task_new">
<?php }else if($url_segment == 'email'){ ?>
<input type="hidden" name="taskmodule" id="taskmodule" value="task">
<?php } ?>
<script type="text/javascript">
    var taskTable = '';

    function getremindertasks() {
        taskTable.ajax.reload();
    }
    var searchBy = {'taskBy': 'whoto'};
    $(document).ready(function () {
        var current_active_tab = $("ul#task-navigation").find("li.active").attr("id");
        $('a.navbar-minimalize.minimalize-styl-2.btn.btn-primary').click(function () {
            setTimeout(function () {
                taskTable.columns.adjust();
            }, 100)
        });

        taskTable = $(".task-dataTable").DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [4, "ASC"];
			},
            scrollX: true,
            bFilter: true,
            order: [
                [4, "ASC"]
            ],
            autoWidth: true,
            ajax: {
                url: "<?php echo base_url(); ?>tasks/getTasksData",
                type: "POST",
                data: function (d) {
                    var tab_status = '';
                    var c = $("ul#task-navigation").find("li.active").attr("id");
                    /*if(current_active_tab != c) {
                        d.start = 0;
                    }*/
                    if ($("#task_reminder").is(":checked") == true) {
                        var b = 1;
                    } else {
                        var b = "";
                    }
                    var e = {
                        caseno: $("#caseNo").val(),
                        /*tab_status: tab_status*/
                    };
                    if ($("#caseNo").val() == '')
                    {
                        e.taskBy = c;
                    }

                    e.reminder = b;
                    $.extend(d, e);
                }
            },
            fnRowCallback: function (f, e, d, c) {
                /*$('#DataTables_Table_0_paginate').attr('data-dt-idx', '1').trigger();*/
                $(f).attr("id", e[9]);
                $(f).attr("data-id", e[9]);
		        $(f).attr("data-event", e[1]);
                $(f).attr("data-caseno", e[11]);
                if (e[e.length - 2] != "") {
                    var b = e[e.length - 2].split("-");
                    if (b[0] != "") {
                        $(f).css({
                            "background-color": b[0],
                            color: b[1]
                        });
                    } else {
                        $(f).css({
                            "background-color": "#ffffff",
                            color: "#000000"
                        });
                    }
                }
            },
            columnDefs: [{
                    render: function (e, c, f) {
						
                        if (e != null) {
                            var b = "";
                            var d = "";
							
                            if (f[8] == "") {
                                if (f[3] === "<?php echo isset($username) ? $username : $this->session->userdata('user_data')['username']; ?>") {
                                    d = '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + e + ')"><i class="fa fa-trash"></i></button>&nbsp;';
                                }
                                b = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + e + ')"><i class="fa fa-edit"></i></button>&nbsp;' + d;
                                if (f[11] != 0) {
                                    b += '<button class="btn btn-info btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + e + ')"><i class="fa fa-check"></i></button>&nbsp;';
                                    b += '<a class="btn btn-info btn-circle-action btn-circle" title="Pull Case" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + f[11] + '"><i class="fa fa-folder-open"></i></a>&nbsp;';
                                }
                                if (f[11] == 0)
                                {
                                    b += '<button class="btn btn-info btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask1(' + e + ')"><i class="fa fa-check"></i></button>';
                                }
                            }
                        }
                        return b;
                    },
					
                    targets: 9,
                    bSortable: false,
                    bSearchable: false
					
                },
                {
                    "targets": 0,
					"width": "2%",
					"className": "text-center cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
					$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                },
				{
                    "targets": 1,
					"width": "70%",
					"className": "textwrap-line cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
					$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					
					}
                },
				{
                    "targets": 2,
					"width": "2%",
					"className": "text-center cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
							$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                },
                {
                    "targets": 3,
					"width": "2%",
                    "className": "text-center cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
							$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                },{
                    "targets": 4,
					"width": "2%",
                    "className": "text-center cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
							$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                },{
                    "targets": 5,
					"width": "3%",
                    "className": "text-center cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
							$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                },{
                    "targets": 6,
					"width": "1%",
                    "className": "text-center cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
							$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                },{
                    "targets": 7,
					"width": "2%",
                    "className": "text-center cursorspointer",
					"width": "10%",
					'createdCell':  function (td, cellData, rowData, row, col) {
							$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                }, {
                    "targets": 8,
					"width": "10%",
                    "className": "text-center cursorspointer",
					'createdCell':  function (td, cellData, rowData, row, col) {
							$(td).attr('onclick', "editTask(" + rowData[9] + ")");
					
					}
                }, {
                    "targets": 9,
					"width": "6%"
                    
                }]
        });
        $("#whofrom, #whoto").on('click', function() {
            taskTable.ajax.reload();
        });

		$("select[name=completed]").val('pending').trigger("change");
		$("select[name=priority]").val('').trigger("change");

        $('#addTask').on('click', function () {
			if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
                if($('.rdate').val()=='' || $('.rtime').val()=='')
				{
					$('.rdateerror').text('Please select Reminder date and time');
					return false;
				}
			} 
														
            if ($('#addTaskForm').valid()) {
                $.blockUI();
                var url = '';
                var chktmodule = $('#taskmodule').val();
                
                if ($('input[name=taskid]').val() != '' && $('input[name=taskid]').val() != 0) {
                    if(chktmodule == 'task_new'){
                        var url = '<?php echo base_url('tasks_new/editTask'); ?>';
                    }else{
                        var url = '<?php echo base_url('tasks/editTask'); ?>';
                    }
                } else {
                    if(chktmodule == 'task_new'){
                        var url = '<?php echo base_url('tasks_new/addTask'); ?>';
                    }else{
                        var url = '<?php echo base_url('tasks/addTask'); ?>';
                    }
                }
                $.ajax({
                    url: url,
                    method: "POST",
                    data: $('#addTaskForm').serialize(),
                    success: function (result) {
                        var data = $.parseJSON(result);
                        $.unblockUI();
                        if (data.status == '1') {
                            swal({
                                title: "Success!",
                                text: data.message,
                                type: "success"
                            },
                                    function ()
                                    {
                                        $('#addTaskForm')[0].reset();

                                        $("select[name=typetask]").select2('val', 'Task Type');
                                        $("select[name=typetask]").val('').trigger("change");

                                        $("select[name=phase]").select2('val', 'Select Phase');
                                        $("select[name=phase]").val('').trigger("change");
                                        $(".select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");
										$('#addtask').modal('hide');
                                        $('#addtask .modal-title').html('Add a Task');

                                        /*taskTable.draw(false);*/
                                        taskTable.draw();
                                    }
                            );

                            /*$('.whoto').removeClass("active");
                            $('.whofrom').addClass("active");*/
                        } else {
                            swal("Oops...", data.message, "error");
                        }
                    }
                });
            }
        });
    });

    $('ul#task-navigation li a').on('click', function (e) {
        var status = $(this).parent().attr('id');
        if (status == 'whofrom')
        {
            $('#changeforFrom').html('From');
        } else
        {
            $('#changeforFrom').html('For');
        }
        $('ul#task-navigation').find('li:not(.' + status + ')').removeClass('active');
        $('ul#task-navigation').find('li.' + status).addClass('active');
        taskTable.ajax.reload(null, false);
    });
    
    $('.rdate').on('show.bs.modal', function (e) {
    if (e.stopPropagation) e.stopPropagation();
});
</script>
