<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
<script src="<?php echo base_url() . 'assets/js/plugins/print/printThis.js' ?>"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script><style>a[disabled] { pointer-events: none;cursor: default;}#applicantImage { background: #1ab394;font-size: 35px;color: #fff;width: 100px;height: 100px;text-align: center;line-height: 100px;display: inline-block;}.staffimg{ background: #1ab394;font-size: 20px;color: #fff;width: 30px;height: 30px;text-align: center;display: inline-block;}.icon-wrench{ display:none !important;}#myProgress{ width: 100%;background-color: #ddd;}#myBar{ width: 0%;height: 30px;background-color: #1ab394;text-align: center;line-height: 30px;color: white;}#myProgress1{ width: 100%;background-color: #ddd;}#myBar1{ width: 0%;height: 30px;background-color: #4CAF50;text-align: center;line-height: 30px;color: white;}#myProgress3 { width: 100%;background-color: #ddd;margin-bottom: 5px;margin-top: 10px;}#myBar3 { width: 0%;height: 30px;background-color: #4CAF50;text-align: center;line-height: 30px;color: white;}.sidebar, .toggle { transition:all .5s ease-in-out;-webkit-transition:all .5s ease-in-out;}.sidebar { background:#fefefe;width:0px;height:387px;position:absolute;top:0;right:0;z-index:9999;padding: 0;box-shadow: 0 0 5px rgba(0,0,0,0.2);}.toggle { position:absolute;left: -24px;top:0px;width:25px;height:25px;background:red;box-shadow: -2px 0 5px rgba(0,0,0,0.2);}.sidebar-collapsed { width:400px;}.sidebar-collapsed .toggle { left: -25px;}.sidebar .sidebar-row{width: 100%;overflow: hidden;height: 100%;padding-bottom: 10px;}.sidebar .sidebar-wrapper{ width: 400px !important;padding: 15px;max-height: 100%;overflow-y: auto;}.caseDates{ height: 280px;}.case-details{ font-size: 14px;}table.caseDateTbl td{ border: 1px solid black;height: 30px;padding-left: 5px;}.bnoti{ background-color: #1ab394;color: #fff;padding: 5px;}.notreadyet{font-weight: 700!important;}.addfile,.removefile,.ca-addfile {top:0px;cursor: pointer;position: absolute;}.removefile {margin-left: 26px;color: #ee5d5c;}.addfile,.ca-addfile {color: #18a689;}.tab-pane .ibox-content {padding: 15px;}.loader { position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url('<?php echo base_url(); ?>/assets/img/loader.gif') 50% 50% no-repeat rgb(255, 255, 255);opacity: .8;}.padding-left-none{ padding-left:0px !important;}.padding-right-none{ padding-right:0px !important;}#injurysummary .ibox{ background-color: #f5f5f5;}#injurysummary hr{ margin-top: 0px !important;}.padding-ten{ padding:10px;}.colorgrey{ color:#3a3a3a;}.colorblack{ color:#000;font-weight: 600;}#injurysummary .ibox-content{ border:none;}div#dropzonePreview{left: 0px;}.modal .ibox-title h5{margin-top:9px;width: calc(100% - 70px);}.modal .ibox-content .ibox-title,.modal .tab-pane .ibox-title{min-height: 50px !important;}.modal .tab-pane .ibox-content {display: inline-block; width: 100%;float: left; }.modal .tabs-container .panel-body{ padding: 0px 15px 0px;}.modal .tabs-container .panel-body .ibox{margin-bottom: 0px;}.form-group input, .form-group select {margin-bottom: 5px !important;}.chosen-container{ width: 100% !important;}.pac-container{z-index: 9999;}.injuryalignment .panel-body{ padding: 0px;}.addinjuryalignment .modal-body { padding: 0px 20px 0px 20px !important;}.injuryaddalignment input{ width: auto !important;}label{font-weight:normal !important;}.select2-container, textarea {margin-bottom: 5px;}.modal-dialog{    margin: 15px auto;}.padd-bot5{padding-bottom: 5px !important;}.padd-top5{padding-top: 5px !important;}#injury .ibox-title .btn-sm{ font-size:12px !important;}#field_adj6a.xs-field{width: 58px !important; display: inline-block !important;}.ibox-title.legend-title fieldset{border: 1px solid;border-left: 0px;border-right: 0px;border-bottom: 0px;margin-bottom: 0px;padding-left: 15px;}.ibox-title.legend-title fieldset legend{width: auto;border-bottom: 0px;margin-bottom: 0px;}.modal .legend-title h5{margin-top:0px;}</style>
<input type="hidden" value="<?php echo $case_no; ?>" id="caseNo">
<input type="hidden" name="hdnCaseId" id="hdnCaseId" value="<?php echo $case_no; ?>" />
<input type="hidden" id="viewtext" value="0">
<input type="hidden" id="viewtext2" value="1">
<style>
    #injuryTable1_wrapper .btn-group{display: block; text-align: right; }
    #injuryTable1_wrapper .btn-group .btn{ float: right; }
    input#field_adj1d { width: 36%;} tr.injury_view1 td{cursor:pointer;}
    #injury_div{z-index: 1;}
    .injDocs .btn-group{
        margin-right: 10px;
        margin-bottom: 10px;
    }
</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="breadcrumbs col-lg-3">
                    <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                    <a href="<?php echo base_url('cases'); ?>">Cases</a> /
                    <a href="<?php echo base_url('cases/case_details/' . $case_no); ?>"> <?php echo $case_no; ?></a> /
                    <span>Injury</span>
                </div>
            </div>
            <div class = "tabs-container">
                <ul class = "nav nav-tabs injury-details">
                    <li class="active"><a data-toggle="tab" href="#injuryviews" id="">Injuries (<span id="count_injury"><?php echo $injury_cnt; ?></span>)</a></li>
                    <li class=""><a data-toggle="tab" href="#injurysummary">Summary</a></li>
                </ul>
                <div class="tab-content">
                    <div id="injuryviews" class="tab-pane active">
                        <div class="panel-body" style="margin-top: 10px;">
                            <ul class = "nav nav-tabs injury-details">
                                <li class="">
                                    <a data-toggle="tab" href="#injuryviewone" id="injuryview1" onclick="tabcheck('viewone')">View 1</a>
                                </li>
                                <li class="active">
                                    <a data-toggle = "tab" href="#injuryviewtwo" id="injuryview2" onclick="tabcheck('viewtwo')">View 2</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div id="injuryviewone" class="tab-pane ">
                                    <div class="ibox">
                                        <div class="ibox-title">
                                            <h5>Injuries</h5>
                                            <a class="pull-right btn btn-info marg-left5 btn-sm AddInjurymodal" href="#addinjury" data-toggle="modal" ondblclick="off" dblclick="off">Add</a>
                                        </div>
                                        <div class="ibox-content marg-bot15 ">
                                            <form>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="table-responsive">
                                                                <table id="injuryTable1" class="table table-bordered table-hover table-striped" style="width:100% !important;width:100% !important;">
                                                                    <thead>
                                                                        <tr>
<!--                                                                            <th>No</th>-->
                                                                            <th>Date of Injury</th>
                                                                            <th>Claim No</th>
                                                                            <th>Status2</th>
                                                                            <th>EAMS No</th>
                                                                            <th>Employer</th>
                                                                            <th>Insurance co.</th>
                                                                            <th>Attorney</th>
                                                                            <th>Adjuster</th>
                                                                            <!-- <th>Attorney2</th> -->
                                                                            <th>Part of Body Injured</th>
                                                                            <th>Status</th>
                                                                            <!-- <th>Carrier #2</th>
                                                                            <th>Adjuster #2</th>
                                                                            <th>Claim #2</th> -->
                                                                            <th>WCAB No</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div id="injuryviewtwo" class="tab-pane active">
                                    <div class="float-e-margins">
                                        <div class="row">
                                            <div id="injuryview">
                                                <div class="col-lg-7">
                                                    <div class="ibox">
                                                        <div class="ibox-title">
                                                            <h5>Injuries</h5>
                                                            <a class="pull-right btn btn-info marg-left5 btn-sm AddInjurymodal" href="#addinjury" data-toggle="modal" ondblclick="off" dblclick="off">Add</a>
                                                            <a class="btn btn-info pull-right marg-left5 btn-sm" <?php echo ($injury_cnt == 0) ? 'disabled' : '' ?> href="#" id="copyinjury">Copy</a>
                                                        </div>
                                                        <div class="ibox-content marg-bot15 ">
                                                            <form>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="table-responsive">
                                                                                <table id="injuryTable" class="table table-bordered table-hover table-striped">
                                                                                    <thead>
                                                                                        <tr>
<!--                                                                                            <th>No</th>-->
                                                                                            <th>Date of Injury</th>
                                                                                            <th>Claim no</th>
                                                                                            <th>Case no</th>
                                                                                            <th>Actions</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody></tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5" id="injury_div">
                                                    <div class="ibox">
                                                        <div class="ibox-title" style="padding-left: 5px;padding-right: 5px;">
                                                            <h5 id="injury_status">Status:<span class="red inline-block" style="font-size: 12px;"></span><span id="injury_status2" class="inline-block green" style="font-size: 11px;"></span></h5>
                                                            <a class="pull-right marg-left5 injury_delete btn btn-sm btn-danger" id="" href="#" style="display:none;">Delete</a>
                                                            <a class="pull-right marg-left5  injury_edit btn btn-sm btn-primary" id="" data-blockid href="#" style="display:none;">Edit</a>
                                                            <a class="pull-right injury_print btn btn-sm btn-primary"  id=""  href="#"  style="display:none;"><i class="icon fa fa-print"></i> Print</a>
                                                        </div>
                                                        <div class="ibox-content marg-bot15 party">
                                                            <form>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="table-responsive">
                                                                                <table class="table">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Date of Injury :</strong></td>
                                                                                            <td id="td_adj1c"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>POB :</strong></td>
                                                                                            <td id="td_adj1e"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>How :</strong></td>
                                                                                            <td id="td_adj2a"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Occupation :</strong></td>
                                                                                            <td id="td_adj1a"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table class="table">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Employer :</strong></td>
                                                                                            <td id="td_e_name"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Carrier :</strong></td>
                                                                                            <td id="td_i_name"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Adjuster :</strong></td>
                                                                                            <td id="td_i_adjuster"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Phone :</strong></td>
                                                                                            <td id="td_i_phone"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Claim No :</strong></td>
                                                                                            <td id="td_i_claimno"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table class="table">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Attorney :</strong></td>
                                                                                            <td id="td_d1_first"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Firm Name :</strong></td>
                                                                                            <td id="td_d1_firm"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Phone :</strong></td>
                                                                                            <td id="td_d1_phone"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table class="table">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Employer 2 :</strong></td>
                                                                                            <td id="td_e2_name"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Carrier 2 :</strong></td>
                                                                                            <td id="td_i2_name"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Adjuster 2 :</strong></td>
                                                                                            <td id="td_i2_first"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>Phone :</strong></td>
                                                                                            <td id="td_i2_phone"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <table class="table">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>WCAB No :</strong></td>
                                                                                            <td id="td_my1"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="text-right" width="150"><strong>EAMS No :</strong></td>
                                                                                            <td id="td_my2"></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--View 2-->
                                
                                <!--View2 End-->
                            </div>
                             <div class='row'>
                                            <div class="ibox">
                                                <div class="ibox-title">
                                                    <h5>Injuries Related Documents</h5>
                                                    <div class='fileContainer upload-button injurydocumentupload'>
                                                        <input type="file" class=""  id="uploadinjDoc" accept="" name="uploadinjDoc[]" multiple >
                                                        <button type="button" id="uploadDoc" class="btn btn-sm btn-primary pull-right">Upload Injury Documents</button>
                                                    </div>
                                                    <div id="myProgress3" style="display:none">
                                                        <div id="myBar3"></div>
                                                    </div>
                                                </div>
                                                <div class="ibox-content marg-bot15 ">
                                                    <div class="injDocs">

                                                    </div>
                                                    <div id="injurymessage"></div>
                                                </div>
                                            </div>
                                        </div>
                        </div>
                    </div>



                    <div id="injurysummary" class="tab-pane"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addinjury" data-backdrop="static" data-keyboard="false" class="modal fade addinjuryalignment" role="dialog">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Injury</h4>
            </div>
            <div class="modal-body">
                <form action="" role="form" id="addInjuryForm">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general">General</a></li>
                            <li><a data-toggle="tab" href="#court">Court</a></li>
                            <li class=""><a data-toggle="tab" href="#employers">Employers</a></li>
                            <li class=""><a data-toggle="tab" href="#carriers">Carriers</a></li>
                            <li class=""><a data-toggle="tab" href="#attorneys">Attorneys</a></li>
                            <li class=""><a data-toggle="tab" href="#1">1</a></li>
                            <li class=""><a data-toggle="tab" href="#2-6">2-6</a></li>
                            <li class=""><a data-toggle="tab" href="#7-10">7-10</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-22">Important Dates</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-23">Add'l Info</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">

                                        <div class="row no-margin">
                                            <div class="col-sm-6 no-left-padding">
                                                <div class="ibox-title ">
                                                    <h5>Application Information</h5>
                                                    <a href="#" class="btn btn-md btn-primary pull-right" onclick="attachParty('general');" >Attach</a>
                                                </div>
                                                <div class="ibox-content">
                                                    <input type="hidden" name="field_injury_id" class="form-control" value="0"/>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">First Name</label>
                                                        <div class="col-sm-7  no-padding"><input type="text" autocomplete="off" name="field_first" class="form-control" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->first;
                                                        }
                                                        ?>" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Last Name</label>
                                                        <div class="col-sm-7  no-padding"><input type="text" name="field_last" autocomplete="off" value="<?php
                                                            if (isset($display_details)) {
                                                                echo $display_details->last;
                                                            }
                                                            ?>" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Address</label>
                                                        <div class="col-sm-7  no-padding"><input type="text" autocomplete="off" name="field_address" value="<?php
                                                        if (isset($display_details)) {
                                                            echo $display_details->address1;
                                                        }
                                                        ?>" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">City</label>
                                                        <div class="col-sm-7  no-padding"><input type="text" autocomplete="off" name="field_city" value="<?php
                                                            if (isset($display_details)) {
                                                                echo $display_details->city;
                                                            }
                                                            ?>" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">State</label>
                                                        <div class="col-sm-7  no-padding"><input type="text" autocomplete="off" name="field_state" value="<?php
                                                            if (isset($display_details)) {
                                                                echo $display_details->state;
                                                            }
                                                            ?>" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Zip</label>
                                                        <div class="col-sm-7  no-padding"><input type="text" autocomplete="off" name="field_zip_code" value="<?php
                                                            if (isset($display_details)) {
                                                                echo $display_details->zip;
                                                            }
                                                            ?>" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">SS No.</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_social_sec" class="form-control social_sec" value="<?php
                                                            if (isset($display_details)) {
                                                                echo $display_details->social_sec;
                                                            }
                                                            ?>" placeholder="234-56-7898"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">AOE/COE Status</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_aoe_coe_status" class="form-control" value="" />
                                                        </div></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 no-padding">
                                                <div class="ibox-title">
                                                    <h5>Other Dates for this Injury</h5>
                                                </div>
                                                <div class="ibox-content marg-bot15">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">DOR Field</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_dor_date" class="form-control injurydate date-field" data-mask="99/99/9999" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">S & W</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_s_w" class="form-control injurydate date-field" data-mask="99/99/9999" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Liab</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_liab" class="form-control injurydate date-field" data-mask="99/99/9999" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Other SOL</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_other_sol" class="form-control injurydate date-field" data-mask="99/99/9999" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Follow up</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_follow_up" class="form-control injurydate date-field" data-mask="99/99/9999" />
                                                        </div></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 no-padding">
                                                <div class="ibox-title">
                                                    <h5>General Information</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-left-padding">Status</label>
                                                        <div class="col-sm-7 no-padding marg-bot5">
                                                            <select name="field_app_status" id="field_app_status" class="form-control select2Class">
                                                                <option value="OPEN">OPEN</option>
                                                                <option value="CLOSED">CLOSED</option>
                                                                <option value="REOPEN">REOPEN</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!--<div class="form-group">
                                                        <label class="col-sm-5 no-padding">Status</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" name="field_app_status" value="OPEN" class="form-control" />
                                                        </div>
                                                    </div>-->
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Office No</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_office" class="form-control" />
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="court" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="ibox-content">
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Status</label>
                                                    <div class="col-sm-7 no-padding"><select name="field_status2" class="form-control select2Class">
                                                            <option value="">Select Status</option>
                                                            <?php
                                                                $court_status = json_decode(injuryStatus);
                                                                foreach ($court_status as $key => $value) {
                                                                    ?>
                                                                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                            <?php }?>
                                                        </select></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Amended Application</label>
                                                    <div class="col-sm-7 no-padding"><select name="field_amended" class="form-control select2Class">
                                                            <option value="">Select Amended</option>
                                                            <?php
                                                                $amendedApp = json_decode(amendedApplication);
                                                                foreach ($amendedApp as $key => $value) {
                                                                    ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">WCAB No.</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_case_no" class="form-control" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">EAMS ADJ NO (Numbers Only)</label>
                                                    <div class="col-sm-7 no-padding"><input type="number" min="1" name="field_eamsno" class="form-control " />
                                                    </div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="employers" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6 no-left-padding">
                                                <div class="ibox-title">
                                                    <h5>Employer Information</h5>
                                                    <a href="#" class="pull-right btn btn-md btn-primary " onclick="attachParty('employers1');"  >Attach</a>

                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Employer</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e_name" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Address</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e_address" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">City</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e_city" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">State</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e_state" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Zip</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e_zip" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Phone</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e_phone" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e_fax" class="form-control contact"/>
                                                        </div></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 no-padding">
                                                <div class="ibox-title">
                                                    <h5>Employer 2 Information</h5>
                                                    <a href="#" class="btn btn-md btn-primary pull-right"  onclick="attachParty('employers2');" >Attach</a>

                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Employer</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e2_name" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Address</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e2_address" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">City</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e2_city" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">State</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e2_state" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Zip</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e2_zip" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Phone</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e2_phone" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_e2_fax" class="form-control contact" />
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="carriers" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6 no-left-padding">
                                                <div class="ibox-title">
                                                    <h5>Carrier Information</h5>
                                                    <a href="#" class="btn btn-md btn-primary pull-right" onclick="attachParty('carriers1');" >Attach</a>

                                                </div>
                                                <div class="ibox-title legend-title">
                                                <fieldset>
                                                    <legend><h5>Adjuster</h5></legend>
                                                </fieldset>
                                                </div>
                                                
                                                <div class="ibox-content padd-bot5 padd-top5">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Salutation</label>
                                                        <div class="col-sm-7 no-padding"><select name="field_i_adjsal" class="form-control select2Class" >
                                                                <option value="">Select Salutation</option>
                                                                <?php foreach ($saluatation as $val) {?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">First</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_adjfst" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Last</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_adjuster" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Claim No.</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_claimno" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Carrier</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_name" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Address</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_address" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">City</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_city" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">State</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_state" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Zip</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" name="field_i_zip" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Phone</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_phone" class="form-control contact" placeholder="(xxx) xxx-xxxx"/>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i_fax" class="form-control contact" placeholder="(xxx) xxx-xxxx" />
                                                        </div></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 no-padding">

                                                <div class="ibox-title">
                                                    <h5>Carrier 2 Information</h5>
                                                    <a href="#" class="btn btn-md btn-primary pull-right" onclick="attachParty('carriers2');">Attach</a>
                                                </div>
                                                <div class="ibox-title legend-title">
                                                <fieldset>
                                                    <legend><h5>Adjuster</h5></legend>
                                                </fieldset>
                                                </div>
                                                <div class="ibox-content padd-bot5 padd-top5">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Salutation</label>
                                                        <div class="col-sm-7 no-padding"><select name="field_i2_adjsal" class="form-control select2Class" >
                                                                <option value="">Select Salutation</option>
                                                                <?php foreach ($saluatation as $val) {?>
                                                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">First</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_adjfst" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Last</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_adjuste" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Claim No.</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_claimno" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Carrier</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_name" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Address</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_address" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">City</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_city" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">State</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_state" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Zip</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_zip" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Phone</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_phone" class="form-control contact" placeholder="(xxx) xxx-xxxx" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_i2_fax" class="form-control contact" placeholder="(xxx) xxx-xxxx" />
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="attorneys" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="row">
                                            <div class="col-sm-6 no-left-padding">

                                                <div class="ibox-title">
                                                    <h5>Defence Attorney Information</h5>
                                                    <a href="#" class="btn btn-md btn-primary pull-right"  onclick="attachParty('attorneys1');">Attach</a>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">First</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_first" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Last</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_last" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Firm</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_firm" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Address</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_address" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">City</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_city" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">State</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_state" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Zip</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_zip" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Phone</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_phone" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d1_fax" class="form-control contact" />
                                                        </div></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 no-padding">
                                                <div class="ibox-title">
                                                    <h5>Defence Attorney Information</h5>
                                                    <a href="#" class="btn btn-md btn-primary pull-right"  onclick="attachParty('attorneys2');">Attach</a>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">First</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_first" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Last</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_last" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Firm</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_firm" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Address</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_address" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">City</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_city" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">State</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_state" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Zip</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_zip" class="form-control" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Phone</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_phone" class="form-control contact" />
                                                        </div></div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 no-padding">Fax</label>
                                                        <div class="col-sm-7 no-padding"><input type="text" autocomplete="off" name="field_d2_fax" class="form-control contact" />
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="1" class="tab-pane injuryalignment">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content injuryone">
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Date of Injury for EAMS and all new Injuries</label>
                                                    <div class="col-sm-7 no-padding"><div class="i-checks"><label> <input type="checkbox" name="field_ct1" id="field_ct1" value=""> <i></i>CT</label></div>
                                                        <div class="input-daterange" id="datepicker1">
                                                            <input type="text" autocomplete="off" class="input-sm form-control injurydate1 date-field" data-mask="99/99/9999"  name="field_doi" id="field_doi"> <!-- name="start" -->
                                                            <input type="text" autocomplete="off" class="input-sm form-control injurydate1 date-field" data-mask="99/99/9999" disabled name="field_doi2" id="field_doi2"><!-- name="end" -->
                                                        </div>
                                                        <label id="field_doi2-error" class="error" for="field_doi2"></label>
                                                    </div></div>

                                                <div class="form-group no-bottom-margin">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <p><b>1.</b>&nbsp;The Injured employee, born <input type="text" id="field_adj1b" name="field_adj1b"  class="form-control bg-field date-field" data-mask="99/99/9999" value="<?php if (!empty($display_details->birth_date)) {
                                                                    echo date('m/d/Y', strtotime(isset($display_details->birth_date) ? $display_details->birth_date : ''));
                                                                }
                                                                ?>"/>
                                                                while employed as a&nbsp;&nbsp; <input type="text" name="field_adj1a" id="" class="form-control bg-field" />
                                                                on <input type="text" autocomplete="off" name="field_adj1c" id="field_adj1c"  class="form-control  bg-field" />
                                                                at <input type="text" autocomplete="off" name="field_adj1d" id="field_adj1d"  class="form-control" />
                                                                City <input type="text" autocomplete="off" name="field_adj1d2"  class="form-control  bg-field" />
                                                                State <input type="text" autocomplete="off" name="field_adj1d3"  class="form-control" />
                                                                Zip&nbsp; <input type="text" autocomplete="off" class="form-control bg-field" name="field_adj1d4" />
                                                            </p>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="ibox-content injuryone">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group no-bottom-margin">
                                                            <label>Body Parts (EAMS App Only)</label>
                                                            <?php $bodyParts = json_decode(bodyParts);?>
                                                            <select class="form-control select2Class" name="field_pob1" >
                                                                <option value="">Select Body Part 1</option>
                                                                <?php foreach ($bodyParts as $key => $val) {?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                                <?php }?>
                                                            </select>
                                                            <select class="form-control select2Class" name="field_pob2" >
                                                                <option value="">Select Body Part 2</option>
                                                                <?php foreach ($bodyParts as $key => $val) {?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                                <?php }?>
                                                            </select>
                                                            <select class="form-control select2Class" name="field_pob3" >
                                                                <option value="">Select Body Part 3</option>
                                                                <?php foreach ($bodyParts as $key => $val) {?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                                <?php }?>
                                                            </select>
                                                            <select class="form-control select2Class" name="field_pob4" >
                                                                <option value="">Select Body Part 4</option>
                                                                <?php foreach ($bodyParts as $key => $val) {?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                                <?php }?>
                                                            </select>
                                                            <select class="form-control select2Class" name="field_pob5" >
                                                                <option value="">Select Body Part 5</option>
                                                                <?php foreach ($bodyParts as $key => $val) {?>
                                                                    <option value="<?php echo $key; ?>"><?php echo $key . " " . $val; ?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Other Body Parts (Legacy and EAMS App)</label>
                                                            <textarea class="form-control" name="field_adj1e"  style="width: 100%;height: 150px;"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div></div>
                            </div>
                            <div id="2-6" class="tab-pane twosix">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="ibox float-e-margins marg-bot10">
                                            <div class="ibox-content injuryone">
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">2. The Injury occured as follows:</label>
                                                    <div class="col-sm-7 no-padding"><textarea class="form-control" name="field_adj2a"  style="width: 100%;height: 150px;"></textarea>
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">3. Actual earnings at the time were</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_adj3a"  class="form-control full-width">
                                                    </div></div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">4. The Injury caused disabilty as follows:</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_adj4a" class="form-control full-width">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="form-group">
                                                        <p>5.&nbsp;Comp pd <input type="text" style="text-transform:uppercase;width:35px !important;" id="field_adj5a" name="field_adj5a" maxlength="1" class="form-control xs-field"> Total pd <input type="text" name="field_adj5b" class="form-control"> Weekly rate <input type="text" name="field_adj5c" class="form-control"> <br />Date last pmt <input type="text" name="field_adj5d" class="form-control injurydate1 date-field" data-mask="99/99/9999"></p>
                                                    </div>
                                                        <!--<input type="text" name="field_adj6a" maxlength="1" class="form-control xs-field">-->
                                                    <div class="form-group no-bottom-margin">
                                                        <p class="no-bottom-margin">6. Unemployment Insurance or unemployment compensetion disability benefits have been received since the date of injury
                                                            <select name="field_adj6a" id="field_adj6a" class="form-control xs-field">
                                                                <option value="Y">Y</option>
                                                                <option value="N">N</option>
                                                            </select>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></div>
                                </div>
                            </div>
                            <div id="7-10" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="ibox float-e-margins marg-bot10">
                                            <div class="ibox-content injuryone no-bottom-padding">
                                                <div class="form-group">
                                                    <label>7. The Injury occured as follows:</label>
                                                    <p>Medical treatment was received <input type="text" name="field_adj7a" maxlength="1" style="text-transform:uppercase" class="form-control xs-field"> Date of last treatment <input type="text" name="field_adj7b" id="field_adj7b" class="form-control date-field" data-mask="99/99/9999"> All treatment was furnished by the employer or insurance company <input type="text" style="text-transform:uppercase" name="field_adj7c" maxlength="1" class="form-control xs-field">
                                                        Other treatment was provided or paid for by <input type="text" name="field_adj7d" class="form-control"> Did Medi-cal pay for any healthcare related to this claim <input type="text" name="field_adj7e" style="text-transform:uppercase" maxlength="1" class="form-control xs-field">
                                                        Doctors not providing or paid for by employer or insurance company who treated or examined for this injury are <input type="text" name="field_doctors" class="form-control bg-field"></p>
                                                </div>
                                                <div class="form-group">
                                                    <label>8. Other Cases have been field for industrial Injuries by this employee as follows:</label>
                                                    <input type="text" name="field_adj8a" class="form-control full-width">
                                                </div>
                                                <div class="form-group">
                                                    <label>9. This application is field beacuse of a disagreement regarding liabilty for:</label>
                                                    <p> TD indemnity <input type="text" name="field_adj9a" style="text-transform:uppercase" value="<?php echo isset($injury9->tdind) ? $injury9->tdind : 'Y' ?>" maxlength="1" class="form-control xs-field">
                                                        PD indemnity <input type="text" value="<?php echo isset($injury9->pdind) ? $injury9->pdind : 'Y' ?>" style="text-transform:uppercase" name="field_adj9b" maxlength="1" class="form-control xs-field">
                                                        Reimbursement for medical expanse <input type="text" style="text-transform:uppercase" name="field_adj9c" value="<?php echo isset($injury9->Reformeexpense) ? $injury9->Reformeexpense : 'Y'; ?>" class="form-control xs-field">
                                                        Medical treatment <input type="text" style="text-transform:uppercase" name="field_adj9d" value="<?php echo isset($injury9->meditreatment) ? $injury9->meditreatment : 'Y'; ?>" maxlength="1" class="form-control xs-field">
                                                        Compansation at proper rate <input type="text" style="text-transform:uppercase" name="field_adj9e" value="<?php echo isset($injury9->comatprorate) ? $injury9->comatprorate : 'Y'; ?>"class="form-control xs-field">
                                                        Rehabilitation <input type="text" style="text-transform:uppercase" name="field_adj9f" value="<?php echo isset($injury9->rehabilitation) ? $injury9->rehabilitation : 'Y'; ?>" maxlength="1" class="form-control xs-field">
                                                        Other Specify <input type="text" name="field_adj9g" value="<?php echo isset($injury9->otherspecify) ? $injury9->otherspecify : '' ?>" class="form-control"> </p>
                                                </div>
                                                <div class="form-group no-bottom-margin">
                                                    <p class="no-bottom-margin">10.&nbsp;Dated at <input type="text" name="field_adj10city"  class="form-control field_adj10city" value="Van Nuys"> Date <input type="text" name="field_adj10a" id="field_adj10a" class="form-control field_adj10a date-field" data-mask="99/99/9999" value="<?php echo date('m/d/Y'); ?>"> Applicant Attorney <input type="text" name="field_firmatty1" class="form-control bg-field" value="<?php echo $attorneyname; ?>"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-22" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="ibox float-e-margins marg-bot10">
                                            <div class="ibox-content injuryone">
                                                <div class="ibox-title">
                                                    <h5>User Define Fields</h5>
                                                </div>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Injury Status</label>
                                                    <div class="col-sm-7 no-padding"> <select name="field_fld1" class="form-control select2Class" >
                                                            <option value="">Select Status</option>
                                                            <option value="Accepted as of">Accepted as of</option>
                                                            <option value="Delayed as of">Delayed as of</option>
                                                            <option value="Denied as of">Denied as of</option>
                                                        </select>
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">5402 Date (POS+93)</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld2" class="form-control injurydate date-field" data-mask="99/99/9999" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">4850 Start Date</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld3" class="form-control injurydate date-field" data-mask="99/99/9999"" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">4658(d) Date (P&S+60)</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld4" class="form-control injurydate date-field" data-mask="99/99/9999"" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Notice of Offer Date</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld5" class="form-control injurydate date-field" data-mask="99/99/9999"" />
                                                    </div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-23" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="ibox float-e-margins marg-bot10">
                                            <div class="ibox-content injuryone">
                                                <div class="ibox-title">
                                                    <h5>User Define Fields</h5>
                                                </div>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Current PTP</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld11" class="form-control" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Prior PTP</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld12" class="form-control" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Prior Awards</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld13" class="form-control" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Prior Attorney</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld14" class="form-control" />
                                                    </div></div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 no-padding">Add'I Notes</label>
                                                    <div class="col-sm-7 no-padding"><input type="text" name="field_fld20" class="form-control" />
                                                    </div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="submit" id="addInjury" class="btn btn-md btn-primary">Save</button>
                    <button data-dismiss="modal" id="closeInjury" class="btn btn-md btn-primary btn-danger">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="casepartyDetaillist" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select A Party To Attach</h4>
            </div>
            <div class="modal-body" style="display:inline-block; width:100%;">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover table-striped" id="casePartylist" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Firm</th>
                                                            <th>City</th>
                                                            <th>Address</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <button type="button" class="btn btn-md btn-primary" id="casepartysave" >Process</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-md btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="injury_Print" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Print the Application</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="" role="form" name="print_injures_form" id="print_injures_form">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="injuryId" id="injuryId">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="i-checks"><label> <input type="radio" value="form-data" name="form_data_type" checked> <i></i>Form and Data</label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="i-checks"><label> <input type="radio" value="data-only" name="form_data_type"> <i></i> Data only </label></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="i-checks"><label> <input type="radio" value="form-only" name="form_data_type"> <i></i> Form only </label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-primary print_injury_data">Proceed</button>
                        <button data-toggle="modal" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var injurytable = ""; /*view 2*/
    var injurytable1 = ""; /*view 1*/
    var casepartylist_data = '';
    var clickFlag = false;
    $(document).ready(function () {
         /*$("#injuryview1").trigger('click');*/

        $('a.navbar-minimalize.minimalize-styl-2.btn.btn-primary').click(function () {
            setTimeout(function () {
                injurytable.columns.adjust();
                injurytable1.columns.adjust();
            }, 100);
        });
        injuryDatatableInitialize();
        $('.contact').mask("(000) 000-0000");
        $('.social_sec').mask("000-00-0000");
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
        $(document.body).on('ifToggled', 'input#field_ct1', function () {

            if ($(this).is(':checked'))
            {
                $('#field_doi2').removeAttr('disabled');
                 $('#field_adj1c').val($('#field_doi').val());
                /* $('#field_adj1c').mask('CT - 99/99/9999 ', {placeholder: "CT - mm/dd/yyyy"});*/
            } else
            {
                $("#field_doi2").prop('disabled', true);
                $('#field_doi2').val('');
                $('#field_adj1c').val($('#field_doi').val());
            }
        });
        $(document.body).on('change', 'input#field_doi', function () {
            if ($('#field_ct1').is(':checked'))
            {
                $('#field_doi2').removeAttr('disabled');
                /*$('#field_adj1c').mask('CT - 99/99/9999 ', {placeholder: "CT - mm/dd/yyyy"});*/
            } else
            {
                var value = $('#field_doi').val();
                /*  $('#field_adj1c').val(value);*/
            }
        });

        $('.injurydate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('.field_adj10city').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });
        $('.field_adj10a').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });

        $('.injurydate1, #field_adj7b').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        });
        $('#field_adj1a, #field_adj1b').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            endDate: new Date()
        });

        casepartylist_data = $('#casePartylist').DataTable({
            scrollX: true,
            stateSave: true,
            stateSaveParams: function (settings, data) {
                data.search.search = "";
                data.start = 0;
                delete data.order;
            },
        });

        $('.injury-details a[data-toggle="tab"]').on('click', function (e) {
            if ($(this).attr('href') == '#injurysummary') {
                var caseno = $("#caseNo").val();
                $.ajax({
                    url: '<?php echo base_url('cases/case_injurysummary'); ?>',
                    dataType: 'html',
                    type: 'POST',
                    data: {
                        caseno: caseno
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {

                        $('#injurysummary').html(data);
                    }
                });
            } else if ($(this).attr('href') == '#injuryviews') {
                if (injurytable != "") {
                    /*injurytable.draw()*/
                } else {
                    injuryDatatableInitialize();
                }
            }
        });

        $(document.body).on('click', '.injury_view', function () {
            var caseNo = $("#caseNo").val();
            var injuryId = $(this).data('id');
            $('div#injury_div').block();
            $('table#injuryTable').find('.table-selected').not('tr#injury-' + injuryId).removeClass('table-selected');
            $('#injuryTable #injury-' + injuryId).addClass('table-selected');
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    injury_id: injuryId,
                    caseno: caseNo
                },
                success: function (data) {
                    $('div#injury_div').unblock();
                     $(this).addClass('table-selected');

                $.ajax({
                    url: '<?php echo base_url(); ?>cases/injurydoc',
                    method: 'POST',
                    data: {
                        injury_id: injuryId,
                        caseno: caseNo,
                    },
                    success: function (data) {

                        if(data.length > 1)
                        {
                            $('.injDocs').html(data);
                            $('#injurymessage').html('');
                        }else
                        {
                            $('#injurymessage').html('No Injury Document');
                            $('.injDocs').html('');
                        }
                    }
                });


                    if (data != false) {
                        setInjuryViewData(data);
                    }
                }
            });
            return false;
        });
        $(document.body).on('click', '.injury_view1', function () {

            var injuryId = $(this).data('id');
            $('injury_view').addClass('table-selected');
            $('.injury_view1').removeClass('table-selected');
            $(this).addClass('table-selected');

            $.ajax({
                url: '<?php echo base_url(); ?>cases/injurydoc',
                method: 'POST',
                data: {
                    injury_id: injuryId,
                    caseno: <?php echo $case_no; ?>,
                },
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (data) {
                    if(data.length > 1)
                    {
                        $('.injDocs').html(data);
                        $('#injurymessage').html('');

                    }
                    else
                    {
                        $('#injurymessage').html('No Injury Document');
                        $('.injDocs').html('');

                    }
                }
            });
            /*$('.injury_edit').trigger('click');*/
        });

        $(document.body).on('click', '.injury_delete', function () {
            var injuryId = this.id;

            var injurydefaultcount = $('#count_injury').text();
            var injurydefaultcount1 = $('#count_injury1').text();
            var injurycount = parseInt(injurydefaultcount) - 1;
            var injurycount1 = parseInt(injurydefaultcount) - 1;

            swal({
                title: "Are you sure to Remove this Injury?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Remove it!",
                closeOnConfirm: true
            }, function () {
                /*$('div#injury_div').block();*/
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/RemoveInjury',
                    data: {
                        injury_id: injuryId
                    },
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    method: "POST",
                    dataType: 'json',
                    success: function (result) {
                        /*$('div#injury_div').unblock();*/
                        if (result.status == 1) {
                            setInjuryViewData('');
                            if(injurytable != '') {
                                injurytable.rows('tr#injury-' + injuryId).remove().draw();
                            }
                            if(injurytable1 != '') {
                                injurytable1.rows('tr#injury-' + injuryId).remove().draw();
                            }
                            $("#count_injury").text(injurycount);
                            $("#count_injury1").text(injurycount1);
                            if ($('#injuryTable tbody tr').length < 1) {
                                $('#injuryTable tbody').html('<tr id="noRecord"><td colspan="4"><h4>No Injury Found</h4></td></tr>');
                            }
                            if ($('#injuryTable1 tbody tr').length < 1) {
                                $('#injuryTable1 tbody').html('<tr id="noRecord"><td colspan="4"><h4>No Injury Found</h4></td></tr>');
                            }
                            swal("Success !", result.message, "success");
                        } else {
                            swal("Oops...", result.message, "error");
                        }
                        var iId = $('#injuryTable tbody tr:eq(0)').attr('data-id');
            $('#injuryTable #injury-' + iId).addClass('table-selected');
            $('#injuryTable #injury-' + iId).trigger('click');
                    }
                });
            });
            return false;
        });

        $('#addinjury.modal').on('hidden.bs.modal', function () {
            $('#addinjury h4.modal-title').text('Add Injury');
            /*$('.select2Class').trigger('change');*/
            $(".select2Class").val(['']).trigger("change");
            $("#field_app_status").val(['OPEN']).trigger("change");
            $('input[type=checkbox]').val('').iCheck('uncheck');
            $('#addInjuryForm')[0].reset();
            $('input[name=field_injury_id]').val('');
        });

        $(document.body).on('click', '.injury_edit', function () {
            var caseNo = $("#caseNo").val();
            var injuryId = $(this).attr('id');
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetailsEdit',
                method: 'POST',
                dataType: 'json',
                data: {
                    injury_id: injuryId,
                    caseno: caseNo
                },
                success: function (data) {

                    if (data != false) {
                        setDataForAJAXSuccess(data);
                    }
                }
            });
            return false;
        });

        $(document.body).on('click', '#copyinjury', function () {
            var caseNo = $("#caseNo").val();
            var injuryId = $('.injury_edit').attr('id');
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetailsEdit',
                method: 'POST',
                dataType: 'json',
                data: {
                    injury_id: injuryId,
                    caseno: caseNo
                },
                success: function (data) {

                    if (data != false) {
                        $('#addinjury .modal-title').html('Edit Injury');
                        $('input[name=field_injury_id]').val(0);
                        $('input[name=field_first]').val(data.first);
                        $('input[name=field_last]').val(data.last);
                        $('input[name=field_address]').val(data.address);
                        $('input[name=field_city]').val(data.city);
                        $('input[name=field_state]').val(data.state);
                        $('input[name=field_zip_code]').val(data.zip_code);
                        $('input[name=field_social_sec]').val(data.social_sec);
                        $('input[name=field_aoe_coe_status]').val(data.aoe_coe_status);
                        $('input[name=field_dor_date]').val(data.dor_date);
                        $('input[name=field_s_w]').val(data.s_w);
                        $('input[name=field_liab]').val(data.liab);
                        $('input[name=field_other_sol]').val(data.other_sol);
                        $('input[name=field_follow_up]').val(data.follow_up);
                        $('input[name=field_app_status]').val(data.app_status);
                        $('input[name=field_office]').val(data.office);
                        $('select[name=field_status2]').val(data.status2);
                        $('select[name=field_amended]').val(data.amended);
                        $('input[name=field_case_no]').val(data.case_no);
                        $('input[name=field_eamsno]').val(data.eamsno);
                        $('input[name=field_e_name]').val(data.e_name);
                        $('input[name=field_e_address]').val(data.e_address);
                        $('input[name=field_e_city]').val(data.e_city);
                        $('input[name=field_e_state]').val(data.e_state);
                        $('input[name=field_e_zip]').val(data.e_zip);
                        $('input[name=field_e_phone]').val(data.e_phone);
                        $('input[name=field_e_fax]').val(data.e_fax);
                        $('input[name=field_e2_name]').val(data.e2_name);
                        $('input[name=field_e2_address]').val(data.e2_address);
                        $('input[name=field_e2_city]').val(data.e2_city);
                        $('input[name=field_e2_state]').val(data.e2_state);
                        $('input[name=field_e2_zip]').val(data.e2_zip);
                        $('input[name=field_e2_phone]').val(data.e2_phone);
                        $('input[name=field_e2_fax]').val(data.e2_fax);
                        $('select[name=field_i_adjsal]').val(data.i_adjsal);
                        $('input[name=field_i_adjfst]').val(data.i_adjfst);
                        $('input[name=field_i_adjuster]').val(data.i_adjuster);
                        $('input[name=field_i_claimno]').val(data.i_claimno);
                        $('input[name=field_i_name]').val(data.i_name);
                        $('input[name=field_i_address]').val(data.i_address);
                        $('input[name=field_i_city]').val(data.i_city);
                        $('input[name=field_i_state]').val(data.i_state);
                        $('input[name=field_i_zip]').val(data.i_zip);
                        $('input[name=field_i_phone]').val(data.i_phone);
                        $('input[name=field_i_fax]').val(data.i_fax);
                        $('select[name=field_i2_adjsal]').val(data.i2_adjsal);
                        $('input[name=field_i2_adjfst]').val(data.i2_adjfst);
                        $('input[name=field_i2_adjuste]').val(data.i2_adjuste);
                        $('input[name=field_i2_claimno]').val(data.i2_claimno);
                        $('input[name=field_i2_name]').val(data.i2_name);
                        $('input[name=field_i2_address]').val(data.i2_address);
                        $('input[name=field_i2_city]').val(data.i2_city);
                        $('input[name=field_i2_state]').val(data.i2_state);
                        $('input[name=field_i2_zip]').val(data.i2_zip);
                        $('input[name=field_i2_phone]').val(data.i2_phone);
                        $('input[name=field_i2_fax]').val(data.i2_fax);
                        $('input[name=field_d1_first]').val(data.d1_first);
                        $('input[name=field_d1_last]').val(data.d1_last);
                        $('input[name=field_d1_firm]').val(data.d1_firm);
                        $('input[name=field_d1_address]').val(data.d1_address);
                        $('input[name=field_d1_city]').val(data.d1_city);
                        $('input[name=field_d1_state]').val(data.d1_state);
                        $('input[name=field_d1_zip]').val(data.d1_zip);
                        $('input[name=field_d1_phone]').val(data.d1_phone);
                        $('input[name=field_d1_fax]').val(data.d1_fax);
                        $('input[name=field_d2_first]').val(data.d2_first);
                        $('input[name=field_d2_last]').val(data.d2_last);
                        $('input[name=field_d2_firm]').val(data.d2_firm);
                        $('input[name=field_d2_address]').val(data.d2_address);
                        $('input[name=field_d2_city]').val(data.d2_city);
                        $('input[name=field_d2_state]').val(data.d2_state);
                        $('input[name=field_d2_zip]').val(data.d2_zip);
                        $('input[name=field_d2_phone]').val(data.d2_phone);
                        $('input[name=field_d2_fax]').val(data.d2_fax);
                        if (data.ct1 == 1) {
                            $('input[name=field_ct1]').iCheck('check');
                        }
                        $('input[name=field_doi]').val(data.doi);
                        $('input[name=field_doi2]').val(data.doi2);
                        $('input[name=field_adj1a]').val(data.adj1a);
                        $('input[name=field_adj1b]').val(data.adj1b);
                        $('input[name=field_adj1c]').val(data.adj1c);
                        $('input[name=field_adj1d]').val(data.adj1d);
                        $('input[name=field_adj1d2]').val(data.adj1d2);
                        $('input[name=field_adj1d3]').val(data.adj1d3);
                        $('input[name=field_adj1d4]').val(data.adj1d4);
                        $('textarea[name=field_adj1e]').val(data.adj1e);
                        $('select[name=field_pob1]').val(data.pob1);
                        $('select[name=field_pob2]').val(data.pob2);
                        $('select[name=field_pob3]').val(data.pob3);
                        $('select[name=field_pob4]').val(data.pob4);
                        $('select[name=field_pob5]').val(data.pob5);
                        $('textarea[name=field_adj2a]').val(data.adj2a);
                        $('input[name=field_adj3a]').val(data.adj3a);
                        $('input[name=field_adj4a]').val(data.adj4a);
                        $('input[name=field_adj5a]').val(data.adj5a);
                        $('input[name=field_adj5b]').val(data.adj5b);
                        $('input[name=field_adj5c]').val(data.adj5c);
                        $('input[name=field_adj5d]').val(data.adj5d);
                        /*$('input[name=field_adj6a]').val(data.adj6a);*/
                        $('select[name=field_adj6a]').val(data.adj6a).trigger("change");
                        $('input[name=field_adj7a]').val((data.adj7a).toUpperCase());
                        $('input[name=field_adj7b]').val(data.adj7b);
                        $('input[name=field_adj7c]').val((data.adj7c).toUpperCase());
                        $('input[name=field_adj7d]').val(data.adj7d);
                        $('input[name=field_adj7e]').val((data.adj7e).toUpperCase());
                        $('input[name=field_doctors]').val(data.doctors);
                        $('input[name=field_adj8a]').val(data.adj8a);
                        $('input[name=field_adj9a]').val((data.adj9a).toUpperCase());
                        $('input[name=field_adj9b]').val((data.adj9b).toUpperCase());
                        $('input[name=field_adj9c]').val((data.adj9c).toUpperCase());
                        $('input[name=field_adj9d]').val((data.adj9d).toUpperCase());
                        $('input[name=field_adj9e]').val((data.adj9e).toUpperCase());
                        $('input[name=field_adj9f]').val((data.adj9f).toUpperCase());
                        $('input[name=field_adj9g]').val(data.adj9g);
                        $('input[name=field_adj10city]').val(data.adj10city);
                        $('input[name=field_adj10a]').val(data.adj10a);
                        $('input[name=field_firmatty1]').val(data.firmatty1);
                        $('select[name=field_fld1]').val(data.fld1);
                        $('input[name=field_fld2]').val(data.fld2);
                        $('input[name=field_fld3]').val(data.fld3);
                        $('input[name=field_fld4]').val(data.fld4);
                        $('input[name=field_fld5]').val(data.fld5);
                        $('input[name=field_fld11]').val(data.fld11);
                        $('input[name=field_fld12]').val(data.fld12);
                        $('input[name=field_fld13]').val(data.fld13);
                        $('input[name=field_fld14]').val(data.fld14);
                        $('input[name=field_fld20]').val(data.fld20);

                        swal({
                            title: 'Copy Injury!',
                            text: "Are you sure you want to copy injury dated " + $('#td_adj1c').html() + "?",
                            showCancelButton: true,
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                swal.close();
                                $('#addinjury').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                $("#addinjury").modal('show');
                                $("#addInjuryForm select").trigger('change');
                                $.unblockUI();
                            }
                        });

                    }
                }
            });
            return false;
        });

        $(document.body).on('click', '#injuryclone', function () {
            var caseNo = $("#caseNo").val();
            var injurycloneId = $(this).data("id");
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetailsEdit',
                method: 'POST',
                dataType: 'json',
                data: {
                    injury_id: injurycloneId,
                    caseno: caseNo
                },
                success: function (data) {

                    if (data != false) {
                        $('#addinjury .modal-title').html('Edit Injury');
                        $('input[name=field_injury_id]').val(0);
                        $('input[name=field_first]').val(data.first);
                        $('input[name=field_last]').val(data.last);
                        $('input[name=field_address]').val(data.address);
                        $('input[name=field_city]').val(data.city);
                        $('input[name=field_state]').val(data.state);
                        $('input[name=field_zip_code]').val(data.zip_code);
                        $('input[name=field_social_sec]').val(data.social_sec);
                        $('input[name=field_aoe_coe_status]').val(data.aoe_coe_status);
                        $('input[name=field_dor_date]').val(data.dor_date);
                        $('input[name=field_s_w]').val(data.s_w);
                        $('input[name=field_liab]').val(data.liab);
                        $('input[name=field_other_sol]').val(data.other_sol);
                        $('input[name=field_follow_up]').val(data.follow_up);
                        $('input[name=field_app_status]').val(data.app_status);
                        $('input[name=field_office]').val(data.office);
                        $('select[name=field_status2]').val(data.status2);
                        $('select[name=field_amended]').val(data.amended);
                        $('input[name=field_case_no]').val(data.case_no);
                        $('input[name=field_eamsno]').val(data.eamsno);
                        $('input[name=field_e_name]').val(data.e_name);
                        $('input[name=field_e_address]').val(data.e_address);
                        $('input[name=field_e_city]').val(data.e_city);
                        $('input[name=field_e_state]').val(data.e_state);
                        $('input[name=field_e_zip]').val(data.e_zip);
                        $('input[name=field_e_phone]').val(data.e_phone);
                        $('input[name=field_e_fax]').val(data.e_fax);
                        $('input[name=field_e2_name]').val(data.e2_name);
                        $('input[name=field_e2_address]').val(data.e2_address);
                        $('input[name=field_e2_city]').val(data.e2_city);
                        $('input[name=field_e2_state]').val(data.e2_state);
                        $('input[name=field_e2_zip]').val(data.e2_zip);
                        $('input[name=field_e2_phone]').val(data.e2_phone);
                        $('input[name=field_e2_fax]').val(data.e2_fax);
                        $('select[name=field_i_adjsal]').val(data.i_adjsal);
                        $('input[name=field_i_adjfst]').val(data.i_adjfst);
                        $('input[name=field_i_adjuster]').val(data.i_adjuster);
                        $('input[name=field_i_claimno]').val(data.i_claimno);
                        $('input[name=field_i_name]').val(data.i_name);
                        $('input[name=field_i_address]').val(data.i_address);
                        $('input[name=field_i_city]').val(data.i_city);
                        $('input[name=field_i_state]').val(data.i_state);
                        $('input[name=field_i_zip]').val(data.i_zip);
                        $('input[name=field_i_phone]').val(data.i_phone);
                        $('input[name=field_i_fax]').val(data.i_fax);
                        $('select[name=field_i2_adjsal]').val(data.i2_adjsal);
                        $('input[name=field_i2_adjfst]').val(data.i2_adjfst);
                        $('input[name=field_i2_adjuste]').val(data.i2_adjuste);
                        $('input[name=field_i2_claimno]').val(data.i2_claimno);
                        $('input[name=field_i2_name]').val(data.i2_name);
                        $('input[name=field_i2_address]').val(data.i2_address);
                        $('input[name=field_i2_city]').val(data.i2_city);
                        $('input[name=field_i2_state]').val(data.i2_state);
                        $('input[name=field_i2_zip]').val(data.i2_zip);
                        $('input[name=field_i2_phone]').val(data.i2_phone);
                        $('input[name=field_i2_fax]').val(data.i2_fax);
                        $('input[name=field_d1_first]').val(data.d1_first);
                        $('input[name=field_d1_last]').val(data.d1_last);
                        $('input[name=field_d1_firm]').val(data.d1_firm);
                        $('input[name=field_d1_address]').val(data.d1_address);
                        $('input[name=field_d1_city]').val(data.d1_city);
                        $('input[name=field_d1_state]').val(data.d1_state);
                        $('input[name=field_d1_zip]').val(data.d1_zip);
                        $('input[name=field_d1_phone]').val(data.d1_phone);
                        $('input[name=field_d1_fax]').val(data.d1_fax);
                        $('input[name=field_d2_first]').val(data.d2_first);
                        $('input[name=field_d2_last]').val(data.d2_last);
                        $('input[name=field_d2_firm]').val(data.d2_firm);
                        $('input[name=field_d2_address]').val(data.d2_address);
                        $('input[name=field_d2_city]').val(data.d2_city);
                        $('input[name=field_d2_state]').val(data.d2_state);
                        $('input[name=field_d2_zip]').val(data.d2_zip);
                        $('input[name=field_d2_phone]').val(data.d2_phone);
                        $('input[name=field_d2_fax]').val(data.d2_fax);
                        if (data.ct1 == 1) {
                            $('input[name=field_ct1]').iCheck('check');
                        }
                        $('input[name=field_doi]').val(data.doi);
                        $('input[name=field_doi2]').val(data.doi2);
                        $('input[name=field_adj1a]').val(data.adj1a);
                        $('input[name=field_adj1b]').val(data.adj1b);
                        $('input[name=field_adj1c]').val(data.adj1c);
                        $('input[name=field_adj1d]').val(data.adj1d);
                        $('input[name=field_adj1d2]').val(data.adj1d2);
                        $('input[name=field_adj1d3]').val(data.adj1d3);
                        $('input[name=field_adj1d4]').val(data.adj1d4);
                        $('textarea[name=field_adj1e]').val(data.adj1e);
                        $('select[name=field_pob1]').val(data.pob1);
                        $('select[name=field_pob2]').val(data.pob2);
                        $('select[name=field_pob3]').val(data.pob3);
                        $('select[name=field_pob4]').val(data.pob4);
                        $('select[name=field_pob5]').val(data.pob5);
                        $('textarea[name=field_adj2a]').val(data.adj2a);
                        $('input[name=field_adj3a]').val(data.adj3a);
                        $('input[name=field_adj4a]').val(data.adj4a);
                        $('input[name=field_adj5a]').val(data.adj5a);
                        $('input[name=field_adj5b]').val(data.adj5b);
                        $('input[name=field_adj5c]').val(data.adj5c);
                        $('input[name=field_adj5d]').val(data.adj5d);
                        /*$('input[name=field_adj6a]').val(data.adj6a);*/
                        $('select[name=field_adj6a]').val(data.adj6a).trigger("change");
                        $('input[name=field_adj7a]').val((data.adj7a).toUpperCase());
                        $('input[name=field_adj7b]').val(data.adj7b);
                        $('input[name=field_adj7c]').val((data.adj7c).toUpperCase());
                        $('input[name=field_adj7d]').val(data.adj7d);
                        $('input[name=field_adj7e]').val((data.adj7e).toUpperCase());
                        $('input[name=field_doctors]').val(data.doctors);
                        $('input[name=field_adj8a]').val(data.adj8a);
                        $('input[name=field_adj9a]').val((data.adj9a).toUpperCase());
                        $('input[name=field_adj9b]').val((data.adj9b).toUpperCase());
                        $('input[name=field_adj9c]').val((data.adj9c).toUpperCase());
                        $('input[name=field_adj9d]').val((data.adj9d).toUpperCase());
                        $('input[name=field_adj9e]').val((data.adj9e).toUpperCase());
                        $('input[name=field_adj9f]').val((data.adj9f).toUpperCase());
                        $('input[name=field_adj9g]').val(data.adj9g);
                        $('input[name=field_adj10city]').val(data.adj10city);
                        $('input[name=field_adj10a]').val(data.adj10a);
                        $('input[name=field_firmatty1]').val(data.firmatty1);
                        $('select[name=field_fld1]').val(data.fld1);
                        $('input[name=field_fld2]').val(data.fld2);
                        $('input[name=field_fld3]').val(data.fld3);
                        $('input[name=field_fld4]').val(data.fld4);
                        $('input[name=field_fld5]').val(data.fld5);
                        $('input[name=field_fld11]').val(data.fld11);
                        $('input[name=field_fld12]').val(data.fld12);
                        $('input[name=field_fld13]').val(data.fld13);
                        $('input[name=field_fld14]').val(data.fld14);
                        $('input[name=field_fld20]').val(data.fld20);

                        swal({
                            title: 'Copy Injury!',
                            text: "Are you sure you want to copy injury dated " + $('#td_adj1c').html() + "?",
                            showCancelButton: true,
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                swal.close();
                                $('#addinjury').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                $("#addinjury").modal('show');
                                $("#addInjuryForm select").trigger('change');
                                $.unblockUI();
                            }
                        });

                    }
                }
            });
            return false;
        });

        $(document.body).on('click', '#addInjury', function (e) {
            $('#copyinjury').removeAttr('disabled', 'disabled');
            if ($('#addInjuryForm').valid()) {
                if (clickFlag == true) {
                    return false;
                }
                clickFlag = true;
                var injuryId = $('input[name=field_injury_id]').val();
                var caseNo = $("#caseNo").val();

                if (injuryId == 0)
                {
                    var injurydefaultcount = $('#count_injury').text();
                    var injurycount = parseInt(injurydefaultcount) + 1;
                    var injurydefaultcount1 = $('#count_injury1').text();
                    var injurycount1 = parseInt(injurydefaultcount1) + 1;
                } else
                {
                    var injurydefaultcount = $('#count_injury').text();
                    var injurycount = parseInt(injurydefaultcount);
                    var injurydefaultcount1 = $('#count_injury1').text();
                    var injurycount1 = parseInt(injurydefaultcount1);

                }
                e.preventDefault();
                $('#addInjury').addClass("disabled");
                e.preventDefault();
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/addInjuries',
                    method: 'POST',
                    dataType: 'json',
                    data: $('form#addInjuryForm').serialize() + '&field_caseno=' + caseNo,
                    success: function (data) {
                        if (data.status == 1) {
                            swal({
                                title: "Success!",
                                text: data.message,
                                type: "success"
                            },
                                    function () {
                                        $("#addinjury").modal('hide');
                                        $.ajax({
                                            url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                                            method: 'POST',
                                            dataType: 'json',
                                            data: {
                                                injury_id: data.injury_id,
                                                caseno: caseNo
                                            },
                                            success: function (data) {
                                                $('div#injury_div').unblock();
                                                if (data != false) {
                                                    injurytable.draw();
                                                    if (injurytable1 != "") {
                                                        injurytable1.draw();
                                                    }
                                                    setInjuryViewData(data);
                                                    $("#count_injury").text(injurycount);
                                                    $("#count_injury1").text(injurycount1);

                                                }
                                                var iId = $('#injuryTable tbody tr:eq(0)').attr('data-id');
            $('#injuryTable #injury-' + iId).addClass('table-selected');
            $('#injuryTable #injury-' + iId).trigger('click');
                                            }
                                        });
                                    }
                            );

                        } else {
                            swal("Oops...", data.message, "error");
                        }
                        $('#addInjury').removeClass("disabled");
                        clickFlag = false;
                    }
                });
            }
        });

        $(document.body).on('hidden.bs.modal', '#addinjury.modal', function () {
            $('#addinjury a[href="#general"]').tab('show');
        });

        $.validator.addMethod("checkNumber", function (element) {
            if(!isNaN($('input[name=field_adj5c]').val()))
                return true;
            else
                return false;
        }, "Please specify valid Weekly Rate");
        $("#addInjuryForm").validate({
            rules: {
                field_first: {
                    required: true
                },
                field_last: {
                    required: true
                },
                field_doi2: {required: function () {
                        if ($('#field_doi').val() == "")
                            return true;
                        else
                            return false;
                    }
                },
                field_adj5c: {
                    checkNumber: true
                }
            },
            messages: {
                field_first: {
                    required: "First name required"
                },
                field_last: {
                    remote: "Last name required"
                },
                field_doi2: {
                    required: "CT Dates is required."
                },
                field_adj5c: {
                    checkNumber: "Please specify valid Weekly Rate"
                }
            }
        });

        $('#casePartylist tbody').on('click', 'tr', function () {
            if ($(this).hasClass('table-selected')) {
                $(this).removeClass('table-selected');
            } else {
                $('#casePartylist tr').removeClass('table-selected');
                $(this).addClass('table-selected');
            }
        });

        $(document.body).on('click', '#casepartysave', function () {
            var row = casepartylist_data.row('.table-selected').node();
            $('#casepartyDetaillist').modal('hide');
            if ($(row).attr('data-active_data') == 'general')
            {
                $('input[name=field_first]').val($(row).attr('data-first1'));
                $('input[name=field_last]').val($(row).attr('data-last'));
                if ($(row).attr('data-state'))
                    $('input[name=field_state]').val($(row).attr('data-state'));
                else
                    $('input[name=field_state]').val('');
                $('input[name=field_zip_code]').val($(row).attr('data-zip'));
                $('input[name=field_social_sec]').val($(row).attr('data-social_sec'));
                $('input[name=field_aoe_coe_status]').val($(row).attr('data-aoe_coe_status'));
                $('input[name=field_address]').val($(row).find('td:eq(3)').text());
                $('input[name=field_city]').val($(row).find('td:eq(2)').text());
            } else if ($(row).attr('data-active_data') == 'employers1')
            {
                if($(row).attr('data-firm'))
                {
                    var firmname = $(row).attr('data-firm');
                }
                else
                {
                    var firmname = $(row).attr('data-first');
                }
                $('input[name=field_e_name]').val(firmname);
                $('input[name=field_e_address]').val($(row).find('td:eq(3)').text());
                $('input[name=field_adj1d]').val($(row).find('td:eq(3)').text());
                $('input[name=field_e_city]').val($(row).find('td:eq(2)').text());
                $('input[name=field_adj1d2]').val($(row).find('td:eq(2)').text());
                $('input[name=field_e_state]').val($(row).attr('data-state'));
                $('input[name=field_adj1d3]').val($(row).attr('data-state'));
                $('input[name=field_e_zip]').val($(row).attr('data-zip'));
                $('input[name=field_adj1d4]').val($(row).attr('data-zip'));
                $('input[name=field_e_phone]').val($(row).attr('data-phone'));
                $('input[name=field_e_fax]').val($(row).attr('data-fax'));
            } else if ($(row).attr('data-active_data') == 'employers2')
            {
                if($(row).attr('data-firm'))
                {
                    var firmname1 = $(row).attr('data-firm');
                }
                else
                {
                    var firmname1 = $(row).attr('data-first');
                }

                $('input[name=field_e2_name]').val(firmname1);
                $('input[name=field_e2_address]').val($(row).find('td:eq(3)').text());
                $('input[name=field_e2_city]').val($(row).find('td:eq(2)').text());
                $('input[name=field_e2_state]').val($(row).attr('data-state'));
                $('input[name=field_e2_zip]').val($(row).attr('data-zip'));
                $('input[name=field_e2_phone]').val($(row).attr('data-phone'));
                $('input[name=field_e2_fax]').val($(row).attr('data-fax'));
            } else if ($(row).attr('data-active_data') == 'carriers1')
            {
                $('select[name=field_i_adjsal]').val($(row).attr('data-salutation'));
                $('input[name=field_i_adjfst]').val($(row).attr('data-first1'));
                $('input[name=field_i_adjuster]').val($(row).attr('data-last'));
                $('input[name=field_i_name]').val($(row).find('td:eq(1)').text());
                $('input[name=field_i_address]').val($(row).find('td:eq(3)').text());
                $('input[name=field_i_city]').val($(row).find('td:eq(2)').text());
                $('input[name=field_i_state]').val($(row).attr('data-state'));
                $('input[name=field_i_zip]').val($(row).attr('data-zip'));
                $('input[name=field_i_phone]').val($(row).attr('data-phone'));
                $('input[name=field_i_fax]').val($(row).attr('data-fax'));
            } else if ($(row).attr('data-active_data') == 'carriers2')
            {
                $('select[name=field_i2_adjsal]').val($(row).attr('data-salutation'));
                $('input[name=field_i2_adjfst]').val($(row).attr('data-first1'));
                $('input[name=field_i2_adjuste]').val($(row).attr('data-last'));
                $('input[name=field_i2_name]').val($(row).find('td:eq(1)').text());
                $('input[name=field_i2_address]').val($(row).find('td:eq(3)').text());
                $('input[name=field_i2_city]').val($(row).find('td:eq(2)').text());
                $('input[name=field_i2_state]').val($(row).attr('data-state'));
                $('input[name=field_i2_zip]').val($(row).attr('data-zip'));
                $('input[name=field_i2_phone]').val($(row).attr('data-phone'));
                $('input[name=field_i2_fax]').val($(row).attr('data-fax'));
            } else if ($(row).attr('data-active_data') == 'attorneys1')
            {
                $('input[name=field_d1_first]').val($(row).attr('data-first1'));
                $('input[name=field_d1_last]').val($(row).attr('data-last'));
                $('input[name=field_d1_firm]').val($(row).find('td:eq(1)').text());
                $('input[name=field_d1_address]').val($(row).find('td:eq(3)').text());
                $('input[name=field_d1_city]').val($(row).find('td:eq(2)').text());
                $('input[name=field_d1_state]').val($(row).attr('data-state'));
                $('input[name=field_d1_zip]').val($(row).attr('data-zip'));
                $('input[name=field_d1_phone]').val($(row).attr('data-phone'));
                $('input[name=field_d1_fax]').val($(row).attr('data-fax'));
            } else
            {
                $('input[name=field_d2_first]').val($(row).attr('data-first1'));
                $('input[name=field_d2_last]').val($(row).attr('data-last'));
                $('input[name=field_d2_firm]').val($(row).find('td:eq(1)').text());
                $('input[name=field_d2_address]').val($(row).find('td:eq(3)').text());
                $('input[name=field_d2_city]').val($(row).find('td:eq(2)').text());
                $('input[name=field_d2_state]').val($(row).attr('data-state'));
                $('input[name=field_d2_zip]').val($(row).attr('data-zip'));
                $('input[name=field_d2_phone]').val($(row).attr('data-phone'));
                $('input[name=field_d2_fax]').val($(row).attr('data-fax'));
            }
            $("#addInjuryForm select").trigger('change');
        });

        $('#uploadinjDoc').click(function(){
            var view1 = $('#viewtext').val();
            var view2 = $('#viewtext2').val();
            var fld_value = '';
            var injuryno = '';

            if(view1 == '1') {
                var injuryrow=injurytable1.rows('.table-selected').data();
                if(injuryrow.length == '0')
                {
                    swal({
                        title: "Alert",
                        text: "Please select Injury from view 1",
                        type: "error"
                    });
                    return false;
                }
            } else if(view2 == '1') {
                var injuryrow2=injurytable.rows('.table-selected').data();
                if(injuryrow2.length == '0')
                {
                    swal({
                        title: "Alert",
                        text: "Please select Injury from view 2",
                        type: "error"
                    });
                    return false;
                }
            }

        });

        $(document.body).on("change", "#uploadinjDoc", function() {
            var files = $("#uploadinjDoc")[0].files;
            var form_data = new FormData();
            var ins = $('input#uploadinjDoc')[0].files.length;
            var image_uploded = '';
            var valid_file = true;
            var cnt = 0;
            var Cno = $('#hdnCaseId').val();

            var view1 = $('#viewtext').val();
            var view2 = $('#viewtext2').val();

            if(view1 == '1') {
                var injuryrow=injurytable1.rows('.table-selected').data();
                if(injuryrow.length != '0') {
                    var injuryno = injuryrow[0][12];
                }
            } else if(view2 == '1') {
                var injuryrow2=injurytable.rows('.table-selected').data();
                if(injuryrow2.length != '0') {
                    var injuryno = injuryrow2[0][4];
                }
            }

            for (var x = 0; x < ins; x++) {
                var valid_extension = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.gif|\.png|\.txt|\.xlsx|\.xls|\.rtf|\.html|\.htm|\.wpd|\.wps|\.csv|\.ppt|\.pptx|\.zip|\.tar|\.xml|\.XLR)$/i;
                ;
                var fld_value = $('#uploadinjDoc')[0].files[x].name;
                form_data.append('caseno', $('#hdnCaseId').val());
                form_data.append('injuryno',injuryno)
                if (!valid_extension.test(fld_value)) {
                    swal({
                        title: "Alert",
                        text: "Oops! Please provide valid file format.\n" + fld_value + ' is not valid file format',
                        type: "warning"
                    });
                } else {
                    form_data.append("files_" + x, $('#uploadinjDoc')[0].files[x]);
                    cnt++;
                }
            }
            if (cnt > 0) {
                document.getElementById('myProgress3').style.display = 'block';
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/UploadinjDocs',
                    type: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function (xhr) {},
                    complete: function (jqXHR, textStatus) {},
                    success: function (data) {

                        var elem = document.getElementById("myBar3");
                        var width = 10;
                        var id = setInterval(frame, 10);

                        function frame() {
                            if (width >= 100) {
                                var res = $.parseJSON(data);
                                if (res.status == 1) {
                                    var injdoc_uploded = '';
                                    var newDiv = '';
                                    swal({
                                        title: "Success!",
                                        text: res.message,
                                        type: "success"
                                    });
                                    $.each(res.file, function(i, val) {
                                        newDiv = '<div class="btn-group"> <a class="btn btn-outline btn-success btn-sm case_video" href="<?php echo base_url() ?>assets/clients/' + Cno + '/' + val + '">' + val + '</a><button class="btn btn-outline btn-danger btn-sm" id="del_injDoc" data-injuryid="'+ injuryno +'" data-id="<?php echo DOCUMENTPATH ?>clients/' + Cno + '/' + val + '"><i class="fa fa-close"></i></button></div>';
                                        $('.injDocs').append(newDiv);
                                    });
                                    $('#injurymessage').html('');
                                    $("#myBar3").text("");
                                    $("#myBar3").removeAttr("style");
                                    document.getElementById('myProgress3').style.display = 'none';
                                    /*window.setTimeout(function(){
                                        location.reload();
                                    },2000);*/
                                }
                                window.clearInterval(id);
                            } else {
                                width++;
                                elem.style.width = width + '%';
                                elem.innerHTML = width * 1 + '%';
                            }
                        }
                    }
                });
            }
            ;
        });

        $(".injDocs").on('click', '#del_injDoc', function () {
            var link = $(this).attr('data-id');
            var injuryidno = $(this).attr('data-injuryid');
            var $this = $(this);
            swal({
                title: 'Injury Document',
                text: "Are you sure to delete?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }, function () {
                if (link != '') {
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/delete_injury_doc',
                        method: 'POST',
                        data: {
                            link: link
                        },
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (result) {
                            var data = $.parseJSON(result);
                            if (data.status == 1) {
                                swal({
                                    title: "Injury Document",
                                    text: "Removed Successfully",
                                    type: "success"
                                });
                                $this.prev("a").remove();
                                $this.remove();

                $('#injury-' + injuryidno).addClass('table-selected');
                $('#injury-' + injuryidno).trigger('click');

                            } else {
                                swal("Oops...", "Oops! Something went wrong.", "error");
                            }
                        }
                    });
                }
            });
        });

        $(document.body).on('click', '.injury_print', function () {
            var injuryId = $(this).attr('id');
            $('#injury_Print').modal('show');
            $('#injuryId').val(injuryId);
        });

        $(document.body).on('click', '.buttons-print', function () {
            var injuryId = $('#injuryTable1').find('.table-selected').data('id');
            $('#injury_Print').modal('show');
            $('#injuryId').val(injuryId);
        });

        $(document.body).on('click', '.print_injury_data', function () {
            var form_data_type = $('input[name=form_data_type]:checked').val();
            var injuryId = $('#injuryId').val();
            var caseno = $('#hdnCaseId').val();
            $.ajax({
                url: '<?php echo base_url() . 'cases/printInjuryDetails' ?>',
                method: "POST",
                data: {
                    form_data_type: form_data_type,
                    injuryId: injuryId,
                    caseno: caseno
                },
                success: function (result) {
                    $('#injury_Print').modal('hide');
                    $(result).printThis({
                        debug: false,
                        header: "<h2 style='margin-top:20px !important;'><center>Injury Details</center></h2>"
                    });
                }
            });
        });

        $("#field_doi2").change(function () {
            if ($("#field_doi").val() == '')
            {
                $("#field_adj1c").val("CT: " + $("#field_doi").val() + "" + $("#field_doi2").val());
            } else
            {
                $("#field_adj1c").val("CT: " + $("#field_doi").val() + "-" + $("#field_doi2").val());
            }
        });

        $("#field_doi").change(function () {
            if ($("#field_doi2").val() == '')
            {
                $("#field_adj1c").val($("#field_doi").val() + "" + $("#field_doi2").val());
            } else
            {
                $("#field_adj1c").val("CT: " + $("#field_doi").val() + "-" + $("#field_doi2").val());
            }

        });
    });

    function tabcheck(str) {

        $('#viewtext').val(0);
        $('#viewtext2').val(0);
        if(str == 'viewone') {
            $('#viewtext').val(1);

        } else if(str == 'viewtwo') {
            $('#viewtext2').val(1);
        }

    }
    function setInjuryViewData(data) {

        if (typeof data.app_status !== 'undefined') {
            if (data.app_status.toLowerCase() == 'closed') {
                $('h5#injury_status span').addClass('red');
                $('h5#injury_status span').removeClass('green');
            } else {
                $('h5#injury_status span').addClass('green');
                $('h5#injury_status span').removeClass('red');
            }
            if (data.status2 == '')
            {
                $('h5#injury_status span').html(data.app_status.toUpperCase());
                $('span#injury_status2').html("");
            } else
            {
                $('h5#injury_status span').html(data.app_status.toUpperCase());
                $('span#injury_status2').html('-' + data.status2);

            }
            $('#injury_div .ibox-title .injury_edit').show();
            $('#injury_div .ibox-title .injury_delete').show();
            $('#injury_div .ibox-title .injury_print').show();
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_edit.marg-left5').attr('id', data.injury_id) : $('a.injury_edit').attr('id', '');
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_edit.marg-left5').attr('data-blockid', data.injury_id) : $('a.injury_edit').attr('data-blockid', '');
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_delete.marg-left5').attr('id', data.injury_id) : $('a.injury_delete').attr('id', '');
            (typeof data.injury_id !== 'undefined') ? $('div#injury_div .ibox-title a.injury_print').attr('id', data.injury_id) : $('a.injury_print').attr('id', '');
            if (typeof data.injury_id !== 'undefined') {
                var injuryId = data.injury_id;
                $('table#injuryTable').find('.table-selected').not('tr#injury-' + injuryId).removeClass('table-selected');
                $('#injury-' + injuryId).addClass('table-selected');
            }
        } else {
            $('h5#injury_status span').html('');
            $('#injury_div .ibox-title .injury_edit').hide();
            $('#injury_div .ibox-title .injury_delete').hide();
            $('#injury_div .ibox-title .injury_print').hide();
        }
        (typeof data.case_no !== 'undefined') ? $('#td_my1').html(data.case_no) : $('#td_my1').html('');
        (typeof data.eamsno !== 'undefined') ? $('#td_my2').html(data.eamsno) : $('#td_my2').html('');
        (typeof data.adj1c !== 'undefined') ? $('#td_adj1c').html(data.adj1c) : $('#td_adj1c').html('');
        (typeof data.adj1e !== 'undefined') ? $('#td_adj1e').html(data.adj1e) : $('#td_adj1e').html('');
        (typeof data.adj2a !== 'undefined') ? $('#td_adj2a').html(data.adj2a) : $('#td_adj2a').html('');
        (typeof data.adj1b !== 'undefined') ? $('#td_adj1a').html(data.adj1a) : $('#td_adj1a').html('');
        (typeof data.e_name !== 'undefined') ? $('#td_e_name').html(data.e_name) : $('#td_e_name').html('');
        (typeof data.i_name !== 'undefined') ? $('#td_i_name').html(data.i_name) : $('#td_i_name').html('');
        (typeof data.adjustername !== 'undefined') ? $('#td_i_adjuster').html(data.adjustername) : $('#td_i_adjuster').html('');
        (typeof data.i_phone !== 'undefined') ? $('#td_i_phone').html(data.i_phone) : $('#td_i_phone').html('');
        (typeof data.i_claimno !== 'undefined') ? $('#td_i_claimno').html(data.i_claimno) : $('#td_i_claimno').html('');
        (typeof data.d1name !== 'undefined') ? $('#td_d1_first').html(data.d1name) : $('#td_d1_first').html('');
        (typeof data.d1_firm !== 'undefined') ? $('#td_d1_firm').html(data.d1_firm) : $('#td_d1_firm').html('');
        (typeof data.d1_phone !== 'undefined') ? $('#td_d1_phone').html(data.d1_phone) : $('#td_d1_phone').html('');
        (typeof data.e2_name !== 'undefined') ? $('#td_e2_name').html(data.e2_name) : $('#td_e2_name').html('');
        (typeof data.i2_name !== 'undefined') ? $('#td_i2_name').html(data.i2_name) : $('#td_i2_name').html('');
        (typeof data.i2name !== 'undefined') ? $('#td_i2_first').html(data.i2name) : $('#td_i2_first').html('');
        (typeof data.i2_phone !== 'undefined') ? $('#td_i2_phone').html(data.i2_phone) : $('#td_i2_phone').html('');
    }
    function attachParty(tab) {
        var caseno = $('#hdnCaseId').val();
        $('#casepartyDetaillist').modal('show');
        $.ajax({
            url: '<?php echo base_url(); ?>cases/getcasepartyDetails',
            method: 'POST',
            data: {caseno: caseno},
            beforeSend: function (xhr) {
                $.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                $.unblockUI();
            },
            success: function (data) {
                casepartylist_data.clear();
                var obj = $.parseJSON(data);
                if (obj.length < 1) {
                    location.reload();
                }
                $.each(obj, function (index, item) {
                    var name = item.salutation + ' ' + item.first + ' ' + item.middle + ' ' + item.last;
                    if (item.state)
                    {
                        var istate = item.state;
                    } else
                    {
                        var istate = '';
                    }
                    if (item.zip)
                    {
                        var izip = item.zip;
                    } else
                    {
                        var izip = '';
                    }
                    if (item.city)
                    {
                        var icity = item.city;
                    } else
                    {
                        var icity = '';
                    }
                    if (item.address1)
                    {
                        var iadd1 = item.address1;
                    } else
                    {
                        var iadd1 = '';
                    }
                    if (item.firm)
                    {
                        var ifirm = item.firm;
                    } else
                    {
                        var ifirm = '';
                    }
                    if (item.fax)
                    {
                        var ifax = item.fax;
                    } else
                    {
                        var ifax = '';
                    }
                    if (item.phone1)
                    {
                        var iph1 = item.phone1;
                    } else
                    {
                        var iph1 = '';
                    }
                    var html = '';
                    html += '<tr class="casepartydetails" data-first="' + item.first + ' ' + item.last + '" data-first1="' + item.first + '" data-firm="' + item.firm + '" data-state="' + istate + '" data-zip="' + izip + '"data-last="' + item.last + '" data-active_data="' + tab + '"data-social_sec="' + item.social_sec + '"data-fax="' + ifax + '"data-phone="' + iph1 + '"data-salutation="' + item.salutation + '" ><td id="name">' + name + '</td>';
                    html += '<td id="firm">' + ifirm + '</td>';
                    html += '<td>' + icity + '</td>';
                    html += '<td>' + iadd1 + '</td>';
                    html += '</tr>';
                    casepartylist_data.row.add($(html));
                });
                casepartylist_data.draw();
            }
        });
    }

    function injuryDatatableInitialize() {
        injurytable = $('#injuryTable').DataTable({
            retrieve: true,
            scrollX: true,
            stateSave: true,
            stateSaveParams: function (settings, data) {
                data.search.search = "";
                data.start = 0;
                delete data.order;
                data.order = [0, "desc"];
            },
            "serverSide": true,
            "bFilter": true,
            "rowReorder": true,
            "order": [
                [0, "desc"]
            ],
            "ajax": {
                url: "<?php echo base_url(); ?>cases/getAllInjuries",
                type: "POST",
                data: {
                    'caseId': $("#caseNo").val()
                },
                async: false
            },
            "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [3]
                },
                {
                    "bSearchable": false,
                    "aTargets": [3]
                }
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $(nRow).attr('id', 'injury-' + aData[4]);
                $(nRow).attr('data-id', aData[4]);
                $(nRow).addClass("injury_view");
                $('td', nRow).css('cursor', 'pointer');
            },
            fnRowCallback: function (nRow, data) {
                if (data[5] == "CLOSED") {
                    $('td', nRow).css('color', 'rgba(0, 0, 0, 0.42)');
                }
            },
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty()).on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
                getInjuryDocs("injuryTable");
            }
        });
        injurytable.on('row-reorder', function (e, diff, edit) {
            var ids1 = new Array();
            var ids3 = new Array();
            for (var i = 0, ien = diff.length; i < ien; i++) {
                var rowData = injurytable.row(diff[i].node).data();
                var ids = new Array();
                var ids2 = new Array();
                if (diff[i].newData == 1) {
                    var newid = diff[i].newData;
                    var injuryid = $(rowData[4]).attr('id');
                    ids2.push(newid);
                    ids2.push(injuryid);
                    ids3.push(ids2);
                    $.ajax({
                        url: '<?php echo base_url(); ?>cases/updateinjuryorderno',
                        type: "POST",
                        data: {
                            position: ids3,
                            caseno: $('#hdnCaseId').val()
                        },
                        success: function (data) {
                            if (data == 1) {
                                swal("Success !", "Injury is successfully reordered.", "success");
                                injurytable.draw();
                            }
                        }
                    });
                } else {
                    var injuryid = $(rowData[4]).attr('id');
                    ids.push(diff[i].newData);
                    ids.push(injuryid);
                    ids1.push(ids);
                }
            }
            if (ids1.length > 0) {
                $.ajax({
                    url: '<?php echo base_url(); ?>cases/updateinjuryorderno',
                    type: "POST",
                    data: {
                        position: ids1,
                        caseno: $('#hdnCaseId').val()
                    },
                    success: function (data) {
                        if (data > 0) {
                        }
                    }
                });
            }
        });
    }

    $('#injuryview1').on('click', function () {

        if (injurytable1 != "") {
            injurytable1.draw();
            var iId = $('#injuryTable1 tbody tr:eq(0)').attr('data-id');
            $('#injuryTable1 #injury-' + iId).addClass('table-selected');
            $('#injuryTable1 #injury-' + iId).trigger('click');

            setTimeout(function () {
                /*injurytable.columns.adjust();*/
                injurytable1.columns.adjust();
            }, 100);
        } else {
            var iId = $('#injuryTable1 tbody tr:eq(0)').attr('data-id');
            $('#injuryTable1 #injury-' + iId).addClass('table-selected');
            $('#injuryTable1 #injury-' + iId).trigger('click');

            injurytable1 = $('#injuryTable1').DataTable({
                "retrieve": true,
                "scrollX": true,
                stateSave: true,
                stateSaveParams: function (settings, data) {
                    data.search.search = "";
                    data.start = 0;
                    delete data.order;
                    data.order = [0, "desc"];
                },
                "serverSide": true,
                "bFilter": true,
                "rowReorder": true,
                "order": [
                    [0, "desc"]
                ],
                dom: 'l<"html5buttons"B>tr<"col-sm-5 no-left-padding"i><"col-sm-7 no-padding"p>',
                buttons: [
                    {
                        extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                        },
                        customize: function (win) {
                            $(win.document.body)
                                    .css('font-size', '10pt');
                            $(win.document.body).find('td')
                                    .css('white-space', 'normal');
                            $(win.document.body).find('td')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],
                "ajax": {
                    url: "<?php echo base_url(); ?>cases/getAllInjuriesview2",
                    type: "POST",
                    data: {
                        'caseId': $("#caseNo").val()
                    },
                    async: false
                },
                "aoColumnDefs": [{
                        "bSortable": false,
                        "aTargets": [3]
                    },
                    {
                        "bSearchable": false,
                        "aTargets": [3]
                    },
                    {
                        "render": function (data, type, row) {
                            return '<a href="javascript:void(0)" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-popover-content="#data" onclick="open_edit_popup($(this).parent().parent().attr(`id`))">' + data + '</a>';
                        },
                        "aTargets": [0]
                    }
                ],
                initComplete: function () {
                    setTimeout(function () {
                        /*injurytable.columns.adjust();*/
                        injurytable1.columns.adjust();
                    }, 100);
                    getInjuryDocs("injuryTable1");
                },
                fnRowCallback: function (nRow, data) {
                    $(nRow).attr('id', 'injury-' + data[12]);
                    $(nRow).attr('data-id', data[12]);
                    $(nRow).addClass('injury_view1');
                    if (data[9] == "CLOSED") {
                        $('td', nRow).css('color', 'rgba(0, 0, 0, 0.42)');
                    }
                },
            });
        }
    });

    $('#injuryview2').on('click', function () {
        if (injurytable != "") {
            injurytable.draw();
            var iId = $('#injuryTable tbody tr:eq(0)').attr('data-id');
            $('#injuryTable #injury-' + iId).addClass('table-selected');
            $('#injuryTable #injury-' + iId).trigger('click');
            setTimeout(function () {
                injurytable.columns.adjust();
                /*injurytable1.columns.adjust();*/
            }, 100);
        } else {
            injurytable = $('#injuryTable').DataTable({
                "retrieve": true,
                "scrollX": true,
                "serverSide": true,
                stateSave: true,
                stateSaveParams: function (settings, data) {
                    data.search.search = "";
                    data.start = 0;
                    delete data.order;
                    data.order = [0, "desc"];
                },
                "bFilter": true,
                "rowReorder": true,
                "order": [
                    [0, "desc"]
                ],

                "ajax": {
                    url: "<?php echo base_url(); ?>cases/getAllInjuries",
                    type: "POST",
                    data: {
                        'caseId': $("#caseNo").val()
                    },
                    async: false
                },

                "aoColumnDefs": [{
                        "bSortable": false,
                        "aTargets": [3]
                    },
                    {
                        "bSearchable": false,
                        "aTargets": [3]
                    },
                ],
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    $(nRow).attr('id', 'injury-' + aData[5]);
                },
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                    });

                    setTimeout(function () {
                        injurytable.columns.adjust();
                        /*injurytable1.columns.adjust();*/
                    }, 100);

                    getInjuryDocs("injuryTable");
                }
            });
        }
    });

    $(document.body).on('click', '.AddInjurymodal', function () {
        var caseno = $("#hdnCaseId").val();
        $.ajax({
            url: '<?php echo base_url(); ?>cases/getmainpartydetail',
            method: 'POST',
            dataType: 'json',
            data: {"caseno": caseno},
            success: function (data) {
                if (data.status == 1) {
                    $("input[name=field_adj1a]").val(data.result.occupation);
                    /*$("input[name=field_adj1d]").val(data.result.address1+', '+data.result.address2);
                    $("input[name=field_adj1d2]").val(data.result.city);
                    $("input[name=field_adj1d3]").val(data.result.state);
                    $("input[name=field_adj1d4]").val(data.result.zip);*/
                }
            }
        });
    });

    function open_edit_popup(data) {
        var injury_id = data.split('-')[1];
        var caseNo = $("#caseNo").val();
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetailsEdit',
                method: 'POST',
                dataType: 'json',
                data: {
                    injury_id: injury_id,
                    caseno: caseNo
                },
                success: function (data) {
                    if (data != false) {
                        setDataForAJAXSuccess(data);
                    }
                }
            });
            return false;
    }

    function getInjuryDocs(tableId) {
        var caseNo = $("#caseNo").val();

        var injuryId = $('#' + tableId + ' tbody tr:eq(0)').attr('id');
        if (injuryId != 'noRecord' && typeof injuryId != typeof undefined) {
            injuryId = $('#' + tableId + ' tbody tr:eq(0)').attr('id').split('-');
            injuryId = injuryId[1];
            $('div#injury_div').block();
            $('table#' + tableId + '').find('.table-selected').not('tr#injury-' + injuryId).removeClass('table-selected');
            $('#' + tableId + ' #injury-' + injuryId).addClass('table-selected');
            $.ajax({
                url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    injury_id: injuryId,
                    caseno: caseNo,
                },
                success: function (data) {
                    $('div#injury_div').unblock();
                    if (data != false) {
                        setInjuryViewData(data);

                    }
                    var iId = $('#' + tableId + ' tbody tr:eq(0)').attr('data-id');
                    $('#' + tableId + ' #injury-' + iId).addClass('table-selected');
                    
                    if(injuryId != "" && typeof injuryId != "undefined") {
                        $.ajax({
                            url: '<?php echo base_url(); ?>cases/injurydoc',
                            method: 'POST',
                            data: {
                                injury_id: injuryId,
                                caseno: $("#caseNo").val(),
                            },
                            beforeSend: function (xhr) {
                                $.blockUI();
                            },
                            complete: function (jqXHR, textStatus) {
                                $.unblockUI();
                            },
                            success: function (data) {
                                if(data.length > 1)
                                {
                                    $('.injDocs').html(data);
                                    $('#injurymessage').html('');

                                }
                                else
                                {
                                    $('#injurymessage').html('No Injury Document');
                                    $('.injDocs').html('');

                                }
                            }
                        });
                    }
                }
            });
        }
    }

    function setDataForAJAXSuccess(data) {
        $('#addinjury .modal-title').html('Edit Injury');
        $('input[name=field_injury_id]').val(data.injury_id);
        $('input[name=field_first]').val(data.first);
        $('input[name=field_last]').val(data.last);
        $('input[name=field_address]').val(data.address);
        $('input[name=field_city]').val(data.city);
        $('input[name=field_state]').val(data.state);
        $('input[name=field_zip_code]').val(data.zip_code);
        $('input[name=field_social_sec]').val(data.social_sec);
        $('input[name=field_aoe_coe_status]').val(data.aoe_coe_status);
        $('input[name=field_dor_date]').val(data.dor_date);
        $('input[name=field_s_w]').val(data.s_w);
        $('input[name=field_liab]').val(data.liab);
        $('input[name=field_other_sol]').val(data.other_sol);
        $('input[name=field_follow_up]').val(data.follow_up);
        $('select[name=field_app_status]').val(data.app_status).trigger('change');
        $('input[name=field_office]').val(data.office);
        $('select[name=field_status2]').val(data.status2);
        $('select[name=field_amended]').val(data.amended);
        $('input[name=field_case_no]').val(data.case_no);
        $('input[name=field_eamsno]').val(data.eamsno);
        $('input[name=field_e_name]').val(data.e_name);
        $('input[name=field_e_address]').val(data.e_address);
        $('input[name=field_e_city]').val(data.e_city);
        $('input[name=field_e_state]').val(data.e_state);
        $('input[name=field_e_zip]').val(data.e_zip);
        $('input[name=field_e_phone]').val(data.e_phone);
        $('input[name=field_e_fax]').val(data.e_fax);
        $('input[name=field_e2_name]').val(data.e2_name);
        $('input[name=field_e2_address]').val(data.e2_address);
        $('input[name=field_e2_city]').val(data.e2_city);
        $('input[name=field_e2_state]').val(data.e2_state);
        $('input[name=field_e2_zip]').val(data.e2_zip);
        $('input[name=field_e2_phone]').val(data.e2_phone);
        $('input[name=field_e2_fax]').val(data.e2_fax);
        $('select[name=field_i_adjsal]').val(data.i_adjsal);
        $('input[name=field_i_adjfst]').val(data.i_adjfst);
        $('input[name=field_i_adjuster]').val(data.i_adjuster);
        $('input[name=field_i_claimno]').val(data.i_claimno);
        $('input[name=field_i_name]').val(data.i_name);
        $('input[name=field_i_address]').val(data.i_address);
        $('input[name=field_i_city]').val(data.i_city);
        $('input[name=field_i_state]').val(data.i_state);
        $('input[name=field_i_zip]').val(data.i_zip);
        $('input[name=field_i_phone]').val(data.i_phone);
        $('input[name=field_i_fax]').val(data.i_fax);
        $('select[name=field_i2_adjsal]').val(data.i2_adjsal);
        $('input[name=field_i2_adjfst]').val(data.i2_adjfst);
        $('input[name=field_i2_adjuste]').val(data.i2_adjuste);
        $('input[name=field_i2_claimno]').val(data.i2_claimno);
        $('input[name=field_i2_name]').val(data.i2_name);
        $('input[name=field_i2_address]').val(data.i2_address);
        $('input[name=field_i2_city]').val(data.i2_city);
        $('input[name=field_i2_state]').val(data.i2_state);
        $('input[name=field_i2_zip]').val(data.i2_zip);
        $('input[name=field_i2_phone]').val(data.i2_phone);
        $('input[name=field_i2_fax]').val(data.i2_fax);
        $('input[name=field_d1_first]').val(data.d1_first);
        $('input[name=field_d1_last]').val(data.d1_last);
        $('input[name=field_d1_firm]').val(data.d1_firm);
        $('input[name=field_d1_address]').val(data.d1_address);
        $('input[name=field_d1_city]').val(data.d1_city);
        $('input[name=field_d1_state]').val(data.d1_state);
        $('input[name=field_d1_zip]').val(data.d1_zip);
        $('input[name=field_d1_phone]').val(data.d1_phone);
        $('input[name=field_d1_fax]').val(data.d1_fax);
        $('input[name=field_d2_first]').val(data.d2_first);
        $('input[name=field_d2_last]').val(data.d2_last);
        $('input[name=field_d2_firm]').val(data.d2_firm);
        $('input[name=field_d2_address]').val(data.d2_address);
        $('input[name=field_d2_city]').val(data.d2_city);
        $('input[name=field_d2_state]').val(data.d2_state);
        $('input[name=field_d2_zip]').val(data.d2_zip);
        $('input[name=field_d2_phone]').val(data.d2_phone);
        $('input[name=field_d2_fax]').val(data.d2_fax);
        if (data.ct1 == 1) {
            $('input[name=field_ct1]').iCheck('check');
        }
        $('input[name=field_doi]').val(data.doi);
        $('input[name=field_doi2]').val(data.doi2);
        $('input[name=field_adj1a]').val(data.adj1a);
        $('input[name=field_adj1b]').val(data.adj1b);
        $('input[name=field_adj1c]').val(data.adj1c);
        $('input[name=field_adj1d]').val(data.adj1d);
        $('input[name=field_adj1d2]').val(data.adj1d2);
        $('input[name=field_adj1d3]').val(data.adj1d3);
        $('input[name=field_adj1d4]').val(data.adj1d4);
        $('textarea[name=field_adj1e]').val(data.adj1e);
        $('select[name=field_pob1]').val(data.pob1);
        $('select[name=field_pob2]').val(data.pob2);
        $('select[name=field_pob3]').val(data.pob3);
        $('select[name=field_pob4]').val(data.pob4);
        $('select[name=field_pob5]').val(data.pob5);
        $('textarea[name=field_adj2a]').val(data.adj2a);
        $('input[name=field_adj3a]').val(data.adj3a);
        $('input[name=field_adj4a]').val(data.adj4a);
        $('input[name=field_adj5a]').val(data.adj5a);
        $('input[name=field_adj5b]').val(data.adj5b);
        $('input[name=field_adj5c]').val(data.adj5c);
        $('input[name=field_adj5d]').val(data.adj5d);
        /* $('input[name=field_adj6a]').val(data.adj6a);*/
        $('select[name=field_adj6a]').val(data.adj6a).trigger("change");
        $('input[name=field_adj7a]').val((data.adj7a).toUpperCase());
        $('input[name=field_adj7b]').val(data.adj7b);
        $('input[name=field_adj7c]').val((data.adj7c).toUpperCase());
        $('input[name=field_adj7d]').val(data.adj7d);
        $('input[name=field_adj7e]').val((data.adj7e).toUpperCase());
        $('input[name=field_doctors]').val(data.doctors);
        $('input[name=field_adj8a]').val(data.adj8a);
        $('input[name=field_adj9a]').val((data.adj9a).toUpperCase());
        $('input[name=field_adj9b]').val((data.adj9b).toUpperCase());
        $('input[name=field_adj9c]').val((data.adj9c).toUpperCase());
        $('input[name=field_adj9d]').val((data.adj9d).toUpperCase());
        $('input[name=field_adj9e]').val((data.adj9e).toUpperCase());
        $('input[name=field_adj9f]').val((data.adj9f).toUpperCase());
        $('input[name=field_adj9g]').val(data.adj9g);
        $('input[name=field_adj10city]').val(data.adj10city);
        $('input[name=field_adj10a]').val(data.adj10a);
        $('input[name=field_firmatty1]').val(data.firmatty1);
        $('select[name=field_fld1]').val(data.fld1);
        $('input[name=field_fld2]').val(data.fld2);
        $('input[name=field_fld3]').val(data.fld3);
        $('input[name=field_fld4]').val(data.fld4);
        $('input[name=field_fld5]').val(data.fld5);
        $('input[name=field_fld11]').val(data.fld11);
        $('input[name=field_fld12]').val(data.fld12);
        $('input[name=field_fld13]').val(data.fld13);
        $('input[name=field_fld14]').val(data.fld14);
        $('input[name=field_fld20]').val(data.fld20);
        $('#addinjury').modal({
            backdrop: 'static',
            keyboard: false
        });
        $("#addinjury").modal('show');
        $("#addInjuryForm select").trigger('change');
        $.unblockUI();
    }
</script>