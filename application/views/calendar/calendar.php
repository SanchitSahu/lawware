<style>
    label.error { color: #cc5965 !important; margin-left: 5px !important; left: 0 !important;/*position: absolute !important;*/bottom: -24px !important; }
    .form-group { margin-bottom: 40px !important;}
    .modal .modal-body { padding: 0px 15px 0px; }
    .modal .tabs-container .panel-body{padding:0px !important;}
    .modal .ibox{margin-bottom: 0px !important;}
    .modal .form-group input{margin-bottom:5px !important;}
    .marginbottom-5{margin-bottom: 5px !important;}
    #calendarForm label{font-weight: normal;}
    #basicDayView .fa.fa-trash{ padding-top: 5px; padding-right: 2px; }
    #basicDayView .btn-circle-action, .historyDayView .btn-circle-action{ padding: 0px 0 !important; }
    .fc-history-view{padding-top: 10px;}
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {bottom: 3px;}
    .fc-basicWeek-view .fc-today {background: #4d7ab1;}
    .fc-basicWeek-view .fc-today a, .fc-basicWeek-view .fc-today span {color: white;}
    .cday {
        font-weight: bold;
        border: 1px solid #ddd;
        padding: 5px 0px;
    }
</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <?php if ($case_no) { ?>
                    <a href="<?php echo base_url() . 'dashboard'; ?>">Home</a> /
                    <a href="<?php echo base_url() . 'cases'; ?>">Cases</a> /
                    <a href="<?php echo base_url('cases/case_details/' . $case_no); ?>"> <?php echo $case_no; ?></a> /
                    <span>Calendar</span>
                <?php } else { ?>
                    <a href="<?php echo base_url(); ?>">Home</a> /
                    <span>Calendar</span>
                <?php } ?>
            </div>
            <input type='hidden' id='showevent' value='<?php echo urldecode($this->uri->segment(3)); ?>'>
            <input type='hidden' id='case_no' value='<?php
            if (!empty($case_no)) {
                echo $case_no;
            } else {
                '0';
            }
            ?>'>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Calendar</h5>
                    <a class="btn pull-right btn-primary btn-sm" href="#addcalendar" id="CalAddEvt" title="Add Event To Calendar" data-backdrop="static" data-toggle="modal" data-attr="0" data-keyboard="false">Add Event</a>
                    <!-- <a href="#" class="btn btn-primary" style="margin-right: 10px; float: right;" id="ExportCalEvt">Export Calendar</a> -->
                </div>
                <div class="ibox-content" style="width:100%; display: inline-block;">
                    <div class="row">
                        <div class="col-lg-12 marg-bot40">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label>Calendar Type: </label>
                                    <?php
                                    if (isset($system_caldata[0]->calendars)) {
                                        $calendars = trim($system_caldata[0]->calendars);
                                    } else {
                                        $calendars = '';
                                    }
                                    if ($calendars == null) {
                                        ?>
                                        <select name="cal_typ" id="cal_typ" class="form-control select2Class" onchange="get_cal_evt()">
                                            <option value="Calenders">Calendars</option>
                                        </select>
                                        <?php
                                    } else {
                                        $calcate = explode(',', $system_caldata[0]->calendars);
                                        ?>
                                        <select name="cal_typ" id="cal_typ" class="form-control select2Class" onchange="get_cal_evt()">
                                            <?php foreach ($calcate as $key => $val) { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } ?>
                                </div>

                                <div class="col-lg-3 mt-md-15">
                                    <label>AttyH: </label>
                                    <select class="form-control cstmdrp" name="cal_atty_hand" id="cal_atty_hand" onBlur="get_cal_evt()" >
                                        <option value="" selected>Select Attorney Handling</option>
                                        <?php
                                        if (isset($atty_hand)) {
                                            foreach ($atty_hand as $key => $val) {
                                                ?>
                                                <option value="<?php echo $val ?>" <?php
                                                /* if (isset($display_details->atty_hand) && $val == $display_details->atty_hand) {
                                                  echo "selected='selected'";
                                                  } */
                                                ?> ><?php echo $val; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>

                                <div class="col-lg-3 mt-md-15">
                                    <label>AttyA: </label>
                                    <select class="form-control cstmdrp" name="cal_attyass" id="cal_attyass" onBlur="get_cal_evt()" value="">
                                        <option value="" selected>Select Attorney Assigned</option>
                                        <?php
                                        if (isset($atty_hand)) {
                                            foreach ($atty_hand as $key => $val) {
                                                ?>
                                                <option value="<?php echo $val ?>" <?php
                                                /* if (isset($display_details->atty_resp) && $val == $display_details->atty_resp) {
                                                  echo "selected='selected'";
                                                  } */
                                                ?>><?php echo $val; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>

                                <div class="col-lg-2 mt-md-15">
                                    <br />
                                    <!--button class="btn btn-white" title="Search" onclick="get_cal_evt()">Search</button-->
                                    <button class="btn btn-primary btn-danger" style="margin-top: 5px;" title="Clear all filtering parameters" onclick="reset_calcal_evt()">Clear</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
                <a href="<?php if($this->uri->segment(2)) { echo "../"; }?>cal_event.ics" id="downloadICS" download></a>
            </div>
        </div>
    </div>
</div>

<!--<link href="<?php echo base_url('assets'); ?>/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>
<script src="<?php echo base_url('assets'); ?>/js/plugins/fullcalendar/moment.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/fullcalendar/fullcalendar.min.js"></script>-->

<link href="<?php echo base_url('assets'); ?>/js/plugins/fullcalendar-3.9.0/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/plugins/fullcalendar-3.9.0/fullcalendar.print.css" rel='stylesheet' media='print'>
<script src="<?php echo base_url('assets'); ?>/js/plugins/fullcalendar-3.9.0/lib/moment.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/fullcalendar-3.9.0/fullcalendar.js"></script>
<script>
                                        var historyViewDataTable = "";
                                        jQuery(document).ready(function () {

                                            /*As per client feedback, current date should not be visible when viewing History tab*/
                                            $(document).on('click', '.fc-state-default', function(){
                                                    /*var tabactive = $(this).text();*/
                                                    if($(this).hasClass('fc-history-button')) {
                                                        $('.fc-center').css('display', 'none');
                                                    } else {
                                                        $('.fc-center').css('display', 'inline-block');
                                                    }

                                                    $('.fc-today-button').on('click', function() {
                                                        $('.fc-today-button').removeClass('fc-state-disabled');
                                                        $('.fc-today-button').removeAttr('disabled'); 
                                                        $('.fc-today-button').css('background', '#4d7ab1');
                                                        $('.fc-today-button').css('color', 'white');
                                                    });

                                                     $('.fc-prev-button, .fc-next-button').on('click', function() {
                                                        $('.fc-today-button').removeClass('fc-state-disabled');
                                                        $('.fc-today-button').removeAttr('disabled'); 
                                                        $('.fc-today-button').css('background', 'white');
                                                        $('.fc-today-button').css('color', 'black');
                                                    });
                                                });
                                            $(window).on('load', function() {
                                                if($('.fc-state-active').hasClass('fc-history-button')) {
                                                    $('.fc-center').css('display', 'none');
                                                } else {
                                                    $('.fc-center').css('display', 'inline-block');
                                                }
                                            });
                                            $(document).on('click', '.fc-month-button, .fc-basicWeek-button, .fc-basicDay-button, .fc-history-button', function() {
                                                    $('.fc-today-button').css('background', 'white');
                                                    $('.fc-today-button').css('color', 'black');
                                            });

                                            $('a.navbar-minimalize.minimalize-styl-2.btn.btn-primary').click(function () {
                                                if(historyViewDataTable != "") {
                                                     setTimeout(function () {
                                                        historyViewDataTable.columns.adjust();
                                                    }, 100);
                                                }
                                            });
                                            var date = new Date();
                                            var d = date.getDate();
                                            var m = date.getMonth();
                                            var y = date.getFullYear();
                                            var NOW = moment();
                                            var infinity = 9999;
                                            var TOINFINITY = moment().add(infinity, "year");
                                            var defaultView = ($('#case_no').val() != 0) ? 'history' : '<?php echo isset($system_caldata[0]->calview) ? $system_caldata[0]->calview : 'basicDay' ?>';

                                            var calendar = $('#calendar').fullCalendar({
                                                header: {
                                                    left: 'today,prev,next',
                                                    center: 'title',
                                                    right: 'month,basicWeek,basicDay,history'
                                                },
                                                editable: true,
                                                lazyFetching: false,
                                                timeFormat: 'hh:mm A',
                                                defaultView: defaultView, /*'<?php echo isset($system_caldata[0]->calview) ? $system_caldata[0]->calview : 'basicDay' ?>',*/
                                                dayClick: function (date, allDay, jsEvent, view) {
                                                    var myDate = new Date();
                                                    var isLocked = document.getElementById("locked");
                                                    /*var a = new Date(date.format('MM/DD/YYYY'));
                                                     var today = new Date(date.format());
                                                     var dd = today.getDate();
                                                     var mm = today.getMonth() + 1;
                                                     var yyyy = today.getFullYear();
                                                     a = mm + '/' + dd + '/' + yyyy;*/
                                                    $('#caldate').val(date.format('MM/DD/YYYY'));
                                                    console.log(allDay.target);
                                                    if (!$(allDay.target).hasClass("cal-delete") && !$(allDay.target).hasClass("fa-trash") && !$(allDay.target).hasClass("calender-pullcase") && !$(allDay.target).hasClass("cal-pullcase")) {
                                                        $('#attyass').removeAttr('disabled');
                                                        $('#addcalendar').modal('show');
                                                    }
                                                },
                                                navLinks: true,
                                                eventLimit: 3,
                                                views: {
                                                    history: {
                                                        type: 'list',
                                                        duration: TOINFINITY.diff(NOW, 'days'),
                                                        buttonText: 'History'
                                                    },
                                                    week: {
                                                        eventLimit: false,
                                                        buttonText: 'Week'
                                                    },
                                                    agenda: {
                                                        eventLimit: false
                                                    },
                                                    basic: {
                                                        eventLimit: false
                                                    },
                                                    basicWeek: {
                                                        eventLimit: false
                                                    },
                                                    month: {
                                                        buttonText: 'Month'
                                                    },
                                                    day: {
                                                        buttonText: 'Day'
                                                    }

                                                },
                                                eventRender: function (event, element, view) {
                                                    if (view.name == 'basicDay') {
                                                        
                                                        var Innerhtml = "";
                                                        var oldHtml = $("#basicDayView tbody").html();
                                                        if(element.length > 0){
                                                            if (event.caseno != "" && event.caseno != 0) {
                                                                Innerhtml = '<tr style="background-color:' + event.color + '" class="fc-content" >' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" class="" style="color:' + event.textColor + '">' + event.eventTime + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" class="" style="color:' + event.textColor + '">' + event.attyH + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.attyA + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.first + ' ' + event.last + ' v. ' + event.defendant + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.title + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.venue + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.judge + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.notes + '</td>' +
                                                                        '<td>' +
                                                                        '<a class="btn btn-danger btn-circle-action btn-circle cal-delete" title="Delete"><i class="fa fa-trash closeon" id="' + event.eventno + '"></i></a>' +
                                                                        '<a class="btn btn-default btn-circle-action btn-circle cal-pullcase" data-caseno="' + event.caseno + '" title="Pull Case"><i class="fa fa-folder-open calender-pullcase" data-caseno="' + event.caseno + '" style="margin: 6px 6px; color: #4D7AB1;"></i></a>' +
                                                                        '</td>' +
                                                                        '</tr>';
                                                            } else {
                                                                Innerhtml = '<tr style="background-color:' + event.color + '" class="fc-content" >' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" class="" style="color:' + event.textColor + '">' + event.eventTime + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.attyH + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.attyA + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.first + ' ' + event.last + ' v. ' + event.defendant + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.title + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.venue + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.judge + '</td>' +
                                                                        '<td onclick="javascript: return open_event(' + event.eventno + ')" style="color:' + event.textColor + '">' + event.notes + '</td>' +
                                                                        '<td>' +
                                                                        '<a class="btn btn-danger btn-circle-action btn-circle" title="Delete"><i class="fa fa-trash closeon" id="' + event.eventno + '"></i></a>' +
                                                                        '</td>' +
                                                                        '</tr>';
                                                            }
                                                            $("#basicDayView tbody").append(Innerhtml);
                                                        }else{
                                                            html = '<tr style="background-color: white; text-align: center;" class="fc-content">';
                                                            html += '<td valign="top" colspan="9" class="dataTables_empty">No matching records found</td>';
                                                            html += '</tr>';
                                                            $("#basicDayView tbody").append(html);
                                                        }
                                                    } else if (view.name == "month" || view.name == "basicWeek") {
                                                        if (event.caseno != "" && event.caseno != 0) {
                                                            $('#addcalendar').modal('hide');
                                                            element.find('div.fc-content').prepend("<span class='calender-pullcase event-caseno' data-caseno='" + event.caseno + "'>" + event.caseno + "</span>");
                                                        }
                                                    }
                                                    element.append("<span class='closeon'>X</span>");
                                                    element.append("<span>" + event.first + " " + event.last + "</span>");
                                                    element.find(".closeon").click(function () {
                                                        swal({
                                                            title: "Are you sure Want to Delete this Event?",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, Delete it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>calendar/removeEvent',
                                                                dataType: 'json',
                                                                method: 'post',
                                                                beforeSend: function (xhr) {
                                                                    $.blockUI();
                                                                },
                                                                complete: function (jqXHR, textStatus) {
                                                                    $.unblockUI();
                                                                },
                                                                data: {eventno: event.eventno},
                                                                success: function (res) {
                                                                    if (res.status == 1) {
                                                                        /*swal("Success !", res.message, "success");*/
                                                                        swal({
                                                                            title: "Success !",
                                                                            text: res.message,
                                                                            type: "success"
                                                                        }, function() {
                                                                            $('.historyDayView').DataTable().draw();
                                                                        });
                                                                        $('#calendar').fullCalendar('removeEvents', event._id);
                                                                    } else {
                                                                        swal("Oops...", res.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        });
                                                        return false;
                                                    });
                                                },
                                                eventAfterAllRender: function (event, element, view) {
                                                    $("#basicDayView").find("tbody:gt(0)").remove();
                                                },
                                                viewRender: function (view, element) {
                                                    if (view.name == "basicDay") {
                                                        var html = "";
                                                        $(".fc-basicDay-view").html('');
                                                        html += '<div class="cday d-block text-center"></div>';
                                                        html += '<table id="basicDayView" width="100%" style="border:1px; background-color:#f5f5f5">';
                                                        html += '<thead style="border:1px solid;"><tr>';
                                                        html += '<th style="padding:5px; width:9.5%;">Time</th><th style="padding:5px; width:8.5%;" >AttyH</th><th style="padding:5px; width:8.5%;">AttyA</th><th style="padding:5px; width: 19.75%;">Name v. Defendant</th><th style="padding:5px; width:8.5%;">Event</th><th style="padding:5px; width:8.5%;">Venue</th><th style="padding:5px; width:8.5%;">Judge</th><th style="padding:5px; width:19.75%;">Notes</th><th style="padding:5px; width:8.5%;">Action</th>';
                                                        html += '</tr></thead><tbody></tbody>';
                                                        html += '</table>';
                                                        $(".fc-basicDay-view").append(html);
                                                    } else if (view.name == 'history') {
                                                        var html = "";
                                                        html += '<table class="historyDayView table table-striped table-bordered table-hover dataTables-example" style="width:100%;" width="100%" style="border:1px;">';
                                                        html += '<thead style="border:1px solid;"><tr>';
                                                        html += '<th text-center style="padding:8px;text-align:left;">Time</th><th style="padding:8px;text-align:left;">AttyH</th><th style="padding:8px;text-align:left;">AttyA</th><th style="padding:8px;text-align:left;">Name <span class="display_block"></span> v. <span class="display_block"></span> Defendant</th><th style="padding:8px;text-align:left;">Event</th><th style="padding:8px;text-align:left;">Venue</th><th style="padding:8px;text-align:left;">Judge</th><th style="padding:8px;text-align:left;">Notes</th><th style="padding:8px;text-align:left;">Action</th>';
                                                        html += '</tr></thead><tbody></tbody>';
                                                        html += '</table>';
                                                        $(".fc-history-view").html(html);
                                                        setHistoryViewDatatable(true);


                                                    }
                                                },
                                                events: function (start, end, timezone, callback) {
                                                    var attyh = $('#cal_atty_hand').val();
                                                    var attya = $('#cal_attyass').val();
                                                    var caseNo = $('#case_no').val();
                                                    if (attyh == 'Select Attorney Handling') {
                                                        attyh = '';
                                                    }
                                                    if (attya == 'Select Attorney Assigned') {
                                                        attya = '';
                                                    }
                                                    var calTyp = $('#cal_typ').val();
                                                    $.ajax({
                                                        url: '<?php echo base_url(); ?>calendar/getCalendarEvents',
                                                        dataType: 'json',
                                                        method: 'post',
                                                        beforeSend: function (xhr) {
                                                            $.blockUI();
                                                        },
                                                        data: {
                                                            start: moment(start._d).add(1, 'days').format('YYYY-MM-DD'), 
                                                            end: moment(end._d).format('YYYY-MM-DD'), 
                                                            attyh: attyh, 
                                                            attya: attya, 
                                                            caltyp: calTyp, 
                                                            caseno: caseNo
                                                        },
                                                        success: function (res) {
                                                            $.unblockUI();
                                                            $("#basicDayView tbody").html('');
                                                            if(res.length == 0) {
                                                                html = '<tr style="background-color: white; text-align: center;" class="fc-content">';
                                                                html += '<td valign="top" colspan="9" class="dataTables_empty">No Event Found</td>';
                                                                html += '</tr>';
                                                                $("#basicDayView tbody").append(html);
                                                            } else {
                                                                callback(res);
                                                            }
                                                            callback(res);
                                                        },
                                                        complete: function (res) {
                                                            var tmp_day = moment(start._d).format('dddd');
                                                            $('.cday').html(tmp_day);
                                                        }
                                                    });
                                                },
                                                eventClick: function (calEvent, jsEvent, view) {
                                                    var eventno = calEvent.eventno;
													
                                                    if (eventno != '' || eventno != 0) {
                                                        $('#addcalendar').modal({backdrop: 'static', keyboard: false});
                                                        getCalendarDataByEventno(eventno);
                                                    }
                                                }
                                            });
                                            $('#btnSaveCalendar').on('click', function () {
                                                var url = '<?php echo base_url(); ?>calendar/addCalendarData';
                                                if ($('input[name=eventno]').val() != 0) {
                                                    url = '<?php echo base_url(); ?>calendar/editCalendarData';
                                                }
                                                
                                                if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
                                                    $('#calreminder').val('1');
                                                }else{
                                                    $('#calreminder').val("0");
                                                }

                                                if ($('#calendarForm').valid()) {
                                                    $.blockUI();
                                                    $.ajax({
                                                        url: url,
                                                        dataType: 'json',
                                                        method: 'post',
                                                        data: $('#calendarForm').serialize(),
                                                        success: function (data) {
                                                            if (data.status == '1') {
                                                                /*swal("Success !", data.message, "success");*/
                                                                swal({
                                                                    title: "Success !",
                                                                    text: data.message,
                                                                    type: "success"
                                                                }, function() {
                                                                    $('.historyDayView').DataTable().draw();
                                                                });
                                                                $('#calendarForm')[0].reset();
                                                                $('#addcalendar').modal('hide');
                                                                $('#addcalendar .modal-title').html('Add Calendar');
                                                                var view = $('#calendar').fullCalendar('getView');
                                                                if (view.name == 'basicDay') {
                                                                    var html = "";
                                                                    $(".fc-basicDay-view").html('');
                                                                    html += '<div class="cday d-block text-center"></div>';
                                                                    html += '<table id="basicDayView" width="100%" style="border:1px; background-color:#f5f5f5">';
                                                                    html += '<thead style="border:1px solid;"><tr>';
                                                                    html += '<th style="padding:5px; width:9.5%;">Time</th><th style="padding:5px; width:8.5%;" >AttyH</th><th style="padding:5px; width:8.5%;">AttyA</th><th style="padding:5px; width: 19.75%;">Name v. Defendant</th><th style="padding:5px; width:8.5%;">Event</th><th style="padding:5px; width:8.5%;">Venue</th><th style="padding:5px; width:8.5%;">Judge</th><th style="padding:5px; width:19.75%;">Notes</th><th style="padding:5px; width:8.5%;">Action</th>';
                                                                    html += '</tr></thead><tbody></tbody>';
                                                                    html += '</table>';
                                                                    $(".fc-basicDay-view").append(html);
                                                                }
                                                                $('#calendar').fullCalendar('refetchEvents');
                                                            } else {
                                                                var url = '<?php echo base_url(); ?>calendar/addCalendarDataD';
                                                                if ($('input[name=eventno]').val() != 0) {
                                                                    url = '<?php echo base_url(); ?>calendar/editCalendarDataD';
                                                                }
                                                                swal({
                                                                    title: "Current event overlaps with another event for the attorney.",
                                                                    type: "warning",
                                                                    showCancelButton: true,
                                                                    confirmButtonColor: "#DD6B55",
                                                                    confirmButtonText: "Yes, add it!",
                                                                    closeOnConfirm: false
                                                                }, function () {
                                                                    $.ajax({
                                                                        url: url,
                                                                        dataType: 'json',
                                                                        method: 'post',
                                                                        data: $('#calendarForm').serialize(),
                                                                        success: function (data) {
                                                                            if (data.status == '1') {
                                                                                swal("Success !", data.message, "success");
                                                                                $('#calendarForm')[0].reset();
                                                                                $('#addcalendar').modal('hide');
                                                                                $('#addcalendar .modal-title').html('Add Calendar');
                                                                                var view = $('#calendar').fullCalendar('getView');
                                                                                if (view.name == 'basicDay')
                                                                                {
                                                                                    var html = "";
                                                                                    $(".fc-basicDay-view").html('');
                                                                                    html += '<div class="cday d-block text-center"></div>';
                                                                                    html += '<table id="basicDayView" width="100%" style="border:1px; background-color:#f5f5f5">';
                                                                                    html += '<thead style="border:1px solid;"><tr>';
                                                                                    html += '<th text-center style="padding:5px;">Time</th><th style="padding:5px;">AttyH</th><th style="padding:5px;">AttyA</th><th style="padding:5px;">Name v. Defendant</th><th style="padding:5px;">Event</th><th style="padding:5px;">Venue</th><th style="padding:5px;">Judge</th><th style="padding:5px;">Notes</th><th style="padding:5px;">Action</th>';
                                                                                    html += '</tr></thead><tbody></tbody>';
                                                                                    html += '</table>';
                                                                                    $(".fc-basicDay-view").append(html);
                                                                                }
                                                                                $('#calendar').fullCalendar('refetchEvents');
                                                                            }
                                                                        }
                                                                    });
                                                                });
                                                            }
                                                            $.unblockUI();
                                                        }
                                                    });
                                                } else {

                                                }
                                                return false;
                                            });
                                            $(document.body).on("change", "#cal_typ", function () {
                                                if ($(".fc-history-button").hasClass("fc-state-active")) {
                                                    $('.fc-center').css('display', 'none');
                                                    setHistoryViewDatatable();
                                                } else {
                                                    $('.fc-center').css('display', 'display-inline');
                                                }
                                            });
                                            $('#cal_atty_hand').editableSelect({
                                                filter: !1
                                            }).on('select.editable-select', function (e, li) {
                                                if ($(".fc-history-button").hasClass("fc-state-active"))
                                                    setHistoryViewDatatable();
                                            });
                                            $('#cal_attyass').editableSelect({
                                                filter: !1
                                            }).on('select.editable-select', function (e, li) {
                                                if ($(".fc-history-button").hasClass("fc-state-active"))
                                                    setHistoryViewDatatable();
                                            });
                                            $(document.body).on("click", ".calender-pullcase, .cal-pullcase", function () {
                                                $('#addcalendar').modal('hide');
                                                window.open('<?php echo base_url(); ?>cases/case_details/' + $(this).data('caseno'), '_blank');
                                                return false;
                                            });
                                            $(document.body).on("click", ".cal-delete, .fa-trash", function (event) {
                                                event.stopPropagation();
                                                var eventID = '';
                                                if ($(this).hasClass('cal-delete')) {
                                                    eventID = $(this).find('i').attr('id');
                                                } else if ($(this).hasClass('fa-trash')) {
                                                    eventID = $(this).attr('id');
                                                }
                                                if (eventID != "") {
                                                    swal({
                                                        title: "Are you sure Want to Delete this Event?",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Yes, Delete it!",
                                                        closeOnConfirm: false
                                                    }, function () {
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>calendar/removeEvent',
                                                            dataType: 'json',
                                                            method: 'post',
                                                            beforeSend: function (xhr) {
                                                                $.blockUI();
                                                            },
                                                            complete: function (jqXHR, textStatus) {
                                                                $.unblockUI();
                                                            },
                                                            data: {eventno: eventID},
                                                            success: function (res) {
                                                                if (res.status == 1) {
                                                                    /*swal("Success !", res.message, "success");*/
                                                                    swal({
                                                                        title: "Success !",
                                                                        text: res.message,
                                                                        type: "success"
                                                                    }, function() {
                                                                        $('.historyDayView').DataTable().draw();
                                                                    });
                                                                    $('#calendar').fullCalendar('refetchEvents');
                                                                    historyViewDataTable.ajax.reload();
                                                                } else {
                                                                    swal("Oops...", res.message, "error");
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                            $(document.body).on('click', '.historyDayView tr', function () {
                                                var eventno = $(this).find('.fa-trash').attr('id');
                                                open_event(eventno);
                                            });
                                        });
                                        function open_event(eventno) {
                                            if (eventno != '' && eventno != 0 && typeof eventno != "undefined") {
                                                $('#addcalendar').modal({backdrop: 'static', keyboard: false});
                                                getCalendarDataByEventno(eventno);
                                            }
                                        }
                                        function get_cal_evt() {
                                            $('#calendar').fullCalendar('refetchEvents');
                                        }
                                        function reset_calcal_evt() {
                                            $('#cal_atty_hand').val('Select Attorney Handling');
                                            $('#cal_attyass').val('Select Attorney Assigned');
                                            $("#cal_typ").select2("val", "0");
                                            /*$('#calendar').fullCalendar('refetchEvents');*/
                                        }
                                        function setHistoryViewDatatable(isNewInstance) {
                                            if (isNewInstance && historyViewDataTable != "") {
                                                historyViewDataTable.destroy();
                                                historyViewDataTable = "";
                                            }

                                            if (historyViewDataTable == "") {
                                                historyViewDataTable = $('.historyDayView').DataTable({
                                                    dom: 'lftr<"col-sm-5 no-left-padding marg-top10"i><"col-sm-7 no-padding marg-top10"p>',
                                                    scrollX: true,
                                                    stateSave: true,
                                                    /*fixedColumns: true,*/
													stateSaveParams: function (settings, data) {
														data.search.search = "";
														data.start = 0;
														delete data.order;
														data.order = [0, "desc"];
													},
                                                    scrollY: "300px",
                                                    scrollCollapse: true,
                                                    "sScrollX": "100%",
                                                    "sScrollXInner": "150%",
                                                    "columns": [
                                                        {width: "10.5%"},
                                                        {width: "8.5%"},
                                                        {width: "8.5%"},
                                                        {width: "19.75%"},
                                                        {width: "8.5%"},
                                                        {width: "8.5%"},
                                                        {width: "8.5%"},
                                                        {width: "18.75%"},
                                                        {width: "8.5%"}
                                                    ],
                                                    buttons: [
                                                        {extend: 'excel', text: '<i class="fa fa-file-excel-o"></i>  Excel', title: '<?php echo APPLICATION_NAME; ?> : Search Cases'},
                                                        {
                                                            extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i>  PDF', title: '<?php echo APPLICATION_NAME; ?> : Search Cases', header: true, customize: function (doc) {
                                                                doc.styles.table = {width: '100%'};
                                                                doc.styles.tableHeader.alignment = 'left';
                                                                doc['header'] = function (page, pages) {
                                                                    return {
                                                                        columns: ['<?php echo APPLICATION_NAME; ?> ', {
                                                                                alignment: 'right',
                                                                                text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                                                            }
                                                                        ],
                                                                        margin: [10, 0]
                                                                    };
                                                                };
                                                                doc.content[1].table.widths = '*';
                                                            }
                                                        },
                                                        {
                                                            extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i>  Print',
                                                            customize: function (win) {
                                                                $(win.document.body).addClass('white-bg');
                                                                $(win.document.body).css('font-size', '10px');
                                                                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                                                            }
                                                        }
                                                    ],
                                                    pageLength: 5,
                                                    lengthMenu: [[5, 10, 20, 50, 100, 500, 1000, -1], [5, 10, 20, 50, 100, 500, 1000, "All"]],
                                                    processing: true,
                                                    serverSide: true,
                                                    order: [[0, "desc"]],
                                                    ajax: {
                                                        url: "<?php echo base_url(); ?>calendar/getEventsDatatable",
                                                        dataType: 'json',
                                                        type: "POST",
                                                        data: function (d) {
                                                            d.attyh = $('#cal_atty_hand').val() == "Select Attorney Handling" ? '' : $('#cal_atty_hand').val();
                                                            d.attya = $('#cal_attyass').val() == "Select Attorney Assigned" ? '' : $('#cal_attyass').val();
                                                            d.caltyp = $('#cal_typ').val();
                                                            d.caseno = $('#case_no').val();
                                                        }
                                                    },
                                                    "columnDefs": [
                                                        {
                                                            "targets": 8,
                                                            "bSortable": false,
                                                            "bSearchable": false
                                                        }
                                                    ]
                                                });
                                            } else {
                                                historyViewDataTable.ajax.reload();
                                            }
                                        }

                                        $('#ExportCalEvt').on('click', function() {
                                            $('#ExportCalEvt').css('background-color', '#4d7ab1');
                                            $('#ExportCalEvt').css('border-color', '#4d7ab1');
                                            $('#ExportCalEvt').css('color', '#fff');
                                            $.ajax({
                                                url: "<?php echo base_url(); ?>calendar/getEventsForExport",
                                                dataType: 'json',
                                                type: "POST",
                                                beforeSend: function (xhr) {
                                                    $.blockUI();
                                                },
                                                complete: function (jqXHR, textStatus) {
                                                    $.unblockUI();
                                                },
                                                data: { 
                                                    caltyp: $('#cal_typ').val(),
                                                    caseno: $('#case_no').val()
                                                },
                                                success: function (res) {
                                                    $.ajax({
                                                        url: "<?php echo base_url(); ?>calendar/callExportScript",
                                                        type: "POST",
                                                        data: { res: res},
                                                        success: function (data) {
                                                            if(data) {
                                                                $('#downloadICS')[0].click();
                                                            }
                                                        },
                                                        error:function(data,exception){
                                                            console.log(exception);
                                                        }
                                                    });
                                                }
                                            });
                                        });
</script>
