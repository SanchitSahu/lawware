<style>
    .chat-active {
        background: green;
        color: white;
        font-weight: 700;
    }
    .chat-user:hover {
        background: green;
        color: white;
        font-weight: 700;
    }
</style>
<div class="wrapper wrapper-content" >
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Chat</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <small class="pull-right text-muted msg-time"></small>
                    <h5>Broadcast Chat panel</h5>
                </div>
                <div class="ibox-content broadcast">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="chat-discussion" id="chat-content">
                                    <div class="chat-message left">
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-3">
                                <div class="chat-users">
                                    <div class="users-list">
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="chat-message-form">
                                    <form id="chatMessage" name="chatMessage" method="post">
                                        <div class="form-group col-xs-11" >
                                            <textarea class="form-control message-input " contenteditable="true" name="message" placeholder="Enter message text"></textarea>
                                        </div>
                                        <div class="form-group col-xs-1">
                                             <button type="button" class="pull-right btn btn-primary marg-bot0" id="send"><i class="fa fa-send"></i>&nbsp; Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var lastData = 1;
    var arrayData = [];
    var dataHTML = '';
    var userListObject = {};
    var colorCode = {};
    var init = 0;
    var highlightwords = [];
    var isPaused = false;

    $(document).ready(function () {

        update_chats(1);
        /* getChatUser(); */

        setInterval(function () {
            if(!isPaused){
                update_chats(2);
                /* getChatUser(2);
                getOnlineUser(); */
            }
        }, 5000);
        $('.message-input').on('keypress',function(e){

            if(e.which == 64){ init = 1;}
            if(e.which == 13)
            {
                if($('textarea.message-input').val().trim() != ''){
                	$( "#send" ).trigger( "click" );

                }

            }
            if(init == 1 && e.which == 32){
                var myString = $('.message-input').val().split("@").pop();
                myString.toUpperCase();

                if(userListObject[myString.toUpperCase()]){
                    /*var str = $('.message-input').val().replace('@'+myString,userListObject[myString.toUpperCase()]);*/
                    $('.message-input').val(str);

                    highlightwords.push(userListObject[myString.toUpperCase()]);
                    $('.message-input').highlightTextarea('destroy');
                    $('.message-input').highlightTextarea({
                       words: highlightwords
                    });
                }
                init = 0;
            }
        });
        $('button#send').attr('disabled',true);
        $('textarea.message-input').keyup(function(e) {
            if($('textarea.message-input').val().trim() != ''){
                $('button#send').attr('disabled',false);
            }else{
                $('button#send').attr('disabled',true);
            }
        });
        $('.message-input').on('keyup',function(e){
            var str = $('.message-input').val();

            if(e.which == 46 || e.which == 8){
                if(str.match(/@\w+$/) != null){
                    init = 1;
                }
            }
        });
        $('#send').click(function (e) {
        	$("#send").attr('disabled','disabled');
            isPaused = true;
            var $field = $('textarea[name=message]');

            if($('.highlightTextarea-highlighter').length > 0){
               var data = $('.highlightTextarea-highlighter').html();
               $('.highlightTextarea-highlighter').remove();
            }else{
               var data = $field.val();
            }

            $field.addClass('disabled').attr('disabled', 'disabled');
            $('#send').attr('disabled');
            sendChat(data);
        });
        $('#chat-content').on('scroll', function (e) {
            if (this.scrollTop == 0) {
                $.ajax({
                    url: '<?php echo base_url() . "chat/get_messages" ?>',
                    method: "GET",
                    data: {'length': lastData},
                    dataType: 'json',
                    complete: function (jqXHR, textStatus) {
                        $('#chat-content').unblock();
                    },
                    success: function (result) {
                        if (result.length > 0)
                            lastData++;

                        dataHTML = append_chat_data(result);
                        var scHeight = $("#chat-content")[0].scrollHeight;

                        $("#chat-content").prepend(dataHTML);
                        if($("#chat-content")[0].scrollHeight - scHeight > 0){
                            $('#chat-content').scrollTop($("#chat-content")[0].scrollHeight - scHeight);
                        }
                    }
                });
            }
        });
    });
    /* function getChatUser(call){
        $.ajax({
            url: '<?php echo base_url() ?>chat/getUsers',
            method: "GET",
            data:{'timestamp':request_timestamp},
            dataType: 'json',
            success: function (result) {
                var htmlContent = createUserHtml(result);
                if(call == 2){
                    $('.users-list').prepend(htmlContent);
                }else{
                    $('.users-list').html(htmlContent);
                }
            }
        });
    }
    function createUserHtml(recordData){

        var userHtml = '';
        var dpColor = '';
        recordData.forEach(function (data) {

            if(!userListObject[data.initials]) {
                userListObject[data.initials] = data.fullname;
            }

            if(data.initials !== '<?php echo $loginUser; ?>'){

                if($('#user-'+data.initials).length){
                    $('#user-'+data.initials).remove();
                }

                dpColor = setColor(data.initials);
                userHtml += '<div class="chat-user" id="user-'+data.initials+'">';
                status = (data.isloggedon != null) ? '<span class="pull-right label label-primary online-status">Online</span>' : '';
                userHtml += '<div class="chat-avatar chat-avatar-small" style="background-color:'+dpColor[data.initials]+'">'+data.initials+'</div>';

                userHtml += status  ;
                userHtml += '	<div class="chat-user-name">';
                userHtml += '           <a href="javascript:;"><span>'+data.initials+'</span></a>';
                userHtml += '     </div>';
                userHtml += '</div>';
                '<div class="message-avatar">'+data.whofrom+'</div>';
            }
        });
        return userHtml;
    }
    function getUnique(
        array, newArray) {
        $.each(array, function (i, e) {
            var count = $.grep(newArray, function (el, index) {
                return el.id == e.id;
            });

            if (count.length < 1) {
                newArray.push(e);
            }
        });

        return newArray;
    }
    function getOnlineUser(){
        $.ajax({
            url: '<?php echo base_url() ?>chat/getOnlineUser',
            method: "GET",
            dataType: 'json',
            success: function (result) {
                updateStatus(result);
            }
        });
    }
    function updateStatus(result){
        $.each(userListObject, function( key, value ) {
            if(result.indexOf(key) == -1){
                $('#user-'+key+' span.online-status').remove();
            }else{
                if($('#user-'+key).find('.online-status').length < 1){
                    $('#user-'+key).find('.chat-user-name').before('<span class="pull-right label label-primary online-status">Online</span>');
                }
            }
        });
    } */
    function getColor()
    {
        return '#' + ('000000' + parseInt(Math.random() * (256 * 256 * 256 - 1)).toString(16)).slice(-6);
    }
    function setColor(data) {
        if(!colorCode[data]){
            colorCode[data] = getColor();
        }
        return colorCode;
    }
    var sendChat = function (message) {
        $.ajax({
            url: '<?php echo base_url() . "chat/send_message" ?>',
            method: "POST",
            data: {'message': message, 'initials': '<?php echo $loginUser ?>'},
            dataType: 'json',
            beforeSend: function (xhr) {
                $('#chat-content').block();
            },
            complete: function (jqXHR, textStatus) {
                $('#send').removeClass('disabled');
                $('textarea[name=message]').val('').removeClass('disabled').removeAttr('disabled');
            },
            success: function (result) {
                update_chats('1','bottom');
                highlightwords = new Array();
                $('#send').attr('disabled',true);
            },
            error: function (jqXHR, exception) {
                var msg = '';

                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
            }
        });
    };
    var append_chat_data = function (chat_data) {
        var html = '';
        var uColor = '';

        chat_data.sort(function (a, b) {
            return new Date(a.datetime).getTime() - new Date(b.datetime).getTime();
        });

        chat_data.forEach(function (data) {
            var is_me = false;

            if (data.whofrom == '<?php echo $loginUser; ?>') {
                is_me = true;
            }

            uColor = setColor(data.whofrom);

            if (is_me) {
                html += '<div class="chat-message left">';
            }else{
                html += '<div class="chat-message right">';
            }
            html += '<div class="message-avatar chat-avatar-big" style="background-color:'+uColor[data.whofrom]+'">'+data.whofrom+'</div>';
            html += '	<div class="message">';
            html += '           <a class="message-author" href="#">' + data.whofrom + '</a>';
            var nwdate = new Date((data.timestamp)*1000);
            html += '<span class="message-date">' + moment(nwdate).format("MM/DD/YYYY, h:mm:ss a")  + '</span>';
           html += '           <span class="message-content">' + data.message + '</span>';
            html += '	</div>';
            html += '</div>';

        });
        return html;
    };

    function IsScrollbarAtBottom() {
        var documentHeight = $('#chat-content')[0].scrollHeight;
        var scrollDifference = $('#chat-content')[0].scrollTop + $('#chat-content')[0].offsetHeight;
        return (documentHeight === scrollDifference);
    }
    var update_chats = function (call) {

        if (typeof (lastData) == 'undefined') {
            lastData = 0;
        }
        if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
                request_timestamp = 0;
	}

        var isTop = IsScrollbarAtBottom();

        $.getJSON('<?php echo base_url(); ?>chat/get_messages?timestamp='+request_timestamp, function (data) {
            dataHTML = '';
            if(data.length > 0){
                isPaused = false;
                dataHTML = append_chat_data(data);

                var date = new Date((data[data.length - 1].timestamp)*1000);
                $('small.msg-time').text(moment(date).format("dddd, MMMM Do YYYY, h:mm:ss a"));
                request_timestamp = data[data.length - 1].timestamp;
                $("#chat-content").append(dataHTML);

                if(isTop == true || call == 1){
                    $('#chat-content').animate({scrollTop: $('#chat-content')[0].scrollHeight}, 1000);
                }
            }
            $('#chat-content').unblock();
        });
    }
</script>