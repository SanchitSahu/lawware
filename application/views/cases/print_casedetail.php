<?php

$dob = '';
$intakeDate = '';
$followup = '';
$dateReferred = '';
$ps = '';
$address = '';
$venueName = 'NA';

if($case_details[0]->city != ''){
    $address .= $case_details[0]->city.", ";
}
if($case_details[0]->state != ''){
    $address .= $case_details[0]->state.", ";
}
if($case_details[0]->zip != ''){
    $address .= $case_details[0]->zip;
}

if($case_details[0]->venueName != ''){
    $venueName = $case_details[0]->venueName;
}

if($case_details[0]->birth_date != '' && $case_details[0]->birth_date != '0000-00-00 00:00:00' && $case_details[0]->birth_date != '1899-12-30 00:00:00'){
    $dob = date('m/d/Y',strtotime($case_details[0]->birth_date));
}
if($case_details[0]->dateenter != '' && $case_details[0]->dateenter != '0000-00-00 00:00:00' && $case_details[0]->dateenter != '1899-12-30 00:00:00'){
    $intakeDate = date('m/d/Y',strtotime($case_details[0]->dateenter));
}
if($case_details[0]->followup != '' && $case_details[0]->followup != '0000-00-00 00:00:00' && $case_details[0]->followup != '1899-12-30 00:00:00'){
    $followup = date('m/d/Y',strtotime($case_details[0]->followup));
}
if($case_details[0]->d_r != '' && $case_details[0]->d_r != '0000-00-00 00:00:00' && $case_details[0]->d_r != '1899-12-30 00:00:00'){
    $dateReferred = date('m/d/Y',strtotime($case_details[0]->d_r));
}
if($case_details[0]->psdate != '' && $case_details[0]->psdate != '0000-00-00 00:00:00' && $case_details[0]->psdate != '1899-12-30 00:00:00'){
    $ps = date('m/d/Y',strtotime($case_details[0]->psdate));
}

?>
<html>
<head>
<style>
.container {
  width:100%;
}
.row {
  width: 100%;
  margin-top: 10px;
}
table{
    width: 100%;
}
td.border_line {
    border-bottom: 1px solid #000;
}
td {
    line-height: 25px;
    white-space: normal;
    vertical-align: baseline;
    padding-left: 5px;
}
td#first {
    width: 120px;
}
td#third{
    width: 300px;
    
}
html, body { height: auto; }
</style>
</head>
<body>
<div class="container">
  <div class="row">
      <table>
          <tr>
              <td id="first">Case Number</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->caseno; ?></td>
              <td>Page Number</td>
              <td>:</td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Case Name</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->caption1; ?></td>
              <td>Today</td>
              <td>:</td>
              <td><?php echo date('m/d/Y'); ?></td>
          </tr>
          <tr>
              <td colspan="6" class="border_line"></td>
          </tr>
          <tr>
              <td colspan="6"><h3>General Case Information</h3></td>
          </tr>
          <tr>
              <td id="first">Case Number</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->caseno; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Type of Case</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->casetype; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Case Status</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->casestat; ?></td>
              <td>Atty Resp</td>
              <td>:</td>
              <td><?php echo $case_details[0]->atty_resp; ?></td>
          </tr>
          <tr>
              <td id="first">Your File Number</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->yourfileno; ?></td>
              <td>Atty Hand</td>
              <td>:</td>
              <td><?php echo $case_details[0]->atty_hand; ?></td>
          </tr>
          <tr>
              <td id="first">File Location</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->location; ?></td>
              <td>Para Hand</td>
              <td>:</td>
              <td><?php echo $case_details[0]->para_hand; ?></td>
          </tr>
          <tr>
              <td id="first">Venue</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->venueName; ?></td>
              <td>Sec Hand</td>
              <td>:</td>
              <td><?php echo $case_details[0]->sec_hand; ?></td>
          </tr>
          <tr>
              <td colspan="6" class="border_line"></td>
          </tr>
          <tr>
              <td colspan="6"><h3>Primary Client Information</h3></td>
          </tr>
          <tr>
              <td id="first">Name</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->first." ".$case_details[0]->last; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Firm</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->firm; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Address</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->address1." ".$case_details[0]->address2; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">City,State,Zip</td>
              <td>:</td>
              <td id="third"><?php echo $address; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Phone(Home)</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->home; ?></td>
              <td>Phone(Car)</td>
              <td>:</td>
              <td><?php echo $case_details[0]->car; ?></td>
          </tr>
          <tr>
              <td id="first">Phone(Business)</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->business; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Phone(Fax)</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->fax; ?></td>
              <td>Email</td>
              <td>:</td>
              <td><?php echo $case_details[0]->email; ?></td>
          </tr>
          <tr>
              <td id="first">Date of Birth</td>
              <td>:</td>
              <td id="third"><?php echo $dob; ?></td>
              <td>License No.</td>
              <td>:</td>
              <td><?php echo $case_details[0]->licenseno; ?></td>
          </tr>
          <tr>
              <td id="first">Interpreter /</br>Language</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->interpret; ?></br><?php echo $case_details[0]->language; ?></td>
              <td>Social Sec No.</td>
              <td>:</td>
              <td><?php echo $case_details[0]->social_sec; ?></td>
          </tr>
          <tr>
              <td colspan="6" class="border_line"></td>
          </tr>
          <tr>
              <td colspan="6"><h3>Case Dates</h3></td>
          </tr>
          <tr>
              <td id="first">Intake Dates</td>
              <td>:</td>
              <td id="third"><?php echo $intakeDate; ?></td>
              <td>P & S</td>
              <td>:</td>
              <td><?php echo $case_details[0]->ps; ?></td>
          </tr>
          <tr>
              <td id="first">Follow up Date</td>
              <td>:</td>
              <td id="third"><?php echo $followup; ?></td>
              <td>P & S Date</td>
              <td>:</td>
              <td><?php echo $ps; ?></td>
          </tr>
          <tr>
              <td id="first">Date Referred</td>
              <td>:</td>
              <td id="third"><?php echo $dateReferred; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td id="first">Referred By</td>
              <td>:</td>
              <td id="third"><?php echo $case_details[0]->rb; ?></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <td colspan="6" class="border_line"></td>
          </tr>
          <tr>
              <td colspan="6"><h3>Case Caption</h3></td>
          </tr>
          <tr>
              <td colspan="6"><?php echo $case_details[0]->caption1; ?></td>
          </tr>
          <tr>
              <td colspan="6" class="border_line"></td>
          </tr>
          <tr>
              <td colspan="6"><h3>Quick Note</h3></td>
          </tr>
          <tr>
              <td colspan="6"><?php echo $case_details[0]->quickNotes; ?></td>
          </tr>
          <tr>
              <td colspan="6" class="border_line"></td>
          </tr>
          <tr>
              <td colspan="6"><h3>Messages</h3></td>
          </tr>
          <tr>
              <td colspan="6"><?php echo $case_details[0]->myMessage1; ?> <?php echo $case_details[0]->myMessage2; ?> <?php echo $case_details[0]->myMessage3; ?> <?php echo $case_details[0]->myMessage4; ?></td><br>
          </tr>
      </table>
  </div>
</div>
</body>
</html>
