<!-- Protect Case Modal START-->
<div id="protactcasepassword" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="window.open('<?php echo base_url() ?>cases', '_self');
                            return false;">&times;</button>
                <h4 class="modal-title">Password</h4>
            </div>
            <div class="modal-body">
                <form name="protectCaseForm" id="protectCaseForm">
                    <p>This client card is protected.Please enter the password.</p>
                    <div class="form-group">
                        <input type="password" id="pro_propassword" name="pro_propassword" placeholder="Password" class="form-control" />
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="protectpassbtn">Proceed</a>
                        <a class="btn btn-md btn-danger" id="protectpasscnbtn">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Protect Case Modal END-->

<?php
if($display_details->password != '' or $display_details->password != null) {
    ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#protactcasepassword').modal({backdrop: 'static', keyboard: false});
            $('#protectpassbtn').click(function () {
                var pass = $('#pro_propassword').val();
                var protectedCaseId = <?php echo $this->uri->segment(3); ?>;
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url() ?>cases/getProtectPass',
                    data: {id: protectedCaseId, pass: pass},
                    dataType: 'html',
                    success: function (data) {
                        if (data == 1)
                        {
                            $('#protactcasepassword').modal('hide');
                            $('#pro_propassword').val('');
                            $('#protactcasepassword').modal('hide');
                        }
                        if (data == 0)
                        {
                            swal("Invalid Password.", data.message, "error");
                            $('#protactcasepassword').modal('hide');
                            $('#pro_propassword').val('');
                            window.open("<?php echo base_url() ?>cases", '_self');
                            return false;
                        }
                    }
                });
            });

            function hashHandler() {
                $('#protactcasepassword').modal('show');
                $('#protectpasscnbtn').click(function () {
                    $('#protactcasepassword').modal('hide');
                    window.open("<?php echo base_url() ?>cases", '_self');
                    return false;
                });
            }
            window.onhashchang = hashHandler();
        });
    </script>

    <?php
}
?>

<script src="<?php echo base_url('assets'); ?>/js/plugins/ckeditor/ckeditor.js"></script>
<style>
a[disabled] { pointer-events: none;cursor: default;}#applicantImage { background: #4d7ab1;font-size: 35px;color: #fff;width: 100px;height: 100px;text-align: center;line-height: 100px;display: inline-block;}.staffimg{ background: #4d7ab1;font-size: 24px;color: #fff;width: 35px;height: 35px;text-align: center;display: inline-block;}.icon-wrench{ display:none !important;}#myProgress{ width: 100%;background-color: #ddd;}#myBar{ width: 0%;height: 30px;background-color: #4CAF50;text-align: center;line-height: 30px;color: white;}#myProgress1{ width: 100%;background-color: #ddd;}#myBar1{ width: 0%;height: 30px;background-color: #4CAF50;text-align: center;line-height: 30px;color: white;}#myProgress3 { width: 100%;background-color: #ddd;}#myBar3 { width: 0%;height: 30px;background-color: #4CAF50;text-align: center;line-height: 30px;color: white;}.sidebar, .toggle { transition:all .5s ease-in-out;-webkit-transition:all .5s ease-in-out;}.sidebar { background:#fefefe;width:0px;height:387px;position:absolute;top:0;right:0;z-index:9999;padding: 0;box-shadow: 0 0 5px rgba(0,0,0,0.2);}.toggle { position:absolute;left: -24px;top:0px;width:25px;height:25px;background:red;box-shadow: -2px 0 5px rgba(0,0,0,0.2);}.sidebar-collapsed { width:400px;}.sidebar-collapsed .toggle { left: -25px;}.sidebar .sidebar-row{width: 100%;overflow: hidden;height: 100%;padding-bottom: 10px;}.sidebar .sidebar-wrapper{ width: 400px !important;padding: 15px;max-height: 100%;overflow-y: auto;}.case-details{ font-size: 14px;}.staff-info td{height: 41px !important;} .caseDates .ibox-content{min-height: 256px;} table.caseDateTbl td{ border: 1px solid #e0e0e0;height: 30px;padding-left: 5px;padding: 5px;}.bnoti{ background-color: #4d7ab1;color: #fff;padding: 5px;line-height: 1;display: block;text-align: center;}.notreadyet{font-weight: 700!important;}.addfile,.removefile,.ca-addfile {top:0px;cursor: pointer;position: absolute;right: 25px;}.removefile {margin-left: 26px;color: #ee5d5c;right: 0px;}.addfile,.ca-addfile {color: #4d7ab1;}.tab-pane .ibox-content {padding: 15px;}.padding-left-none{ padding-left:0px !important;}.padding-right-none{ padding-right:0px !important;}#injurysummary .ibox{ background-color: #f5f5f5;}#injurysummary hr{ margin-top: 0px !important;}.padding-ten{ padding:10px;}.colorgrey{ color:#3a3a3a;}.colorblack{ color:#000;font-weight: 600;}#injurysummary .ibox-content{ border:none;}div#dropzonePreview{left: 0px;}#tools .icheckbox_square-green{ display: block !important;float:right !important;}#addcontact .tabs-container .panel-body{padding:0px !important;}#addRolodexForm label{font-weight: normal;}#editcasedetail .modal-body{padding: 0px 15px 0px;}#editcasedetail .tabs-container .panel-body{padding: 0px;}#edit_case_details label{font-weight: bold;}#caseSpecific label{font-weight: normal;}#addcaseact label{font-weight: normal;}#add_new_case_activity .tabs-container .panel-body{padding: 5px;}#addcalendar label{font-weight: normal;}#calendarForm .tab-pane .ibox-content{padding:10px 10px 10px 10px;}#addcalendar .modal-body{padding:0px 15px 0px;}#calendarForm .tabs-container .tab-pane .panel-body{padding:0px;}#clonecalendar label{font-weight: normal;}#addparty .modal-body{padding:0px 10px 0px;}.collapse-link{cursor: pointer;}#addcontact .modal-dialog{margin-top: 5px;}#addcontact .modal-body{padding:0px 10px 5px;}#filterbottom{margin-bottom: 50px;}#rolodexPartiesResult_filter{ padding-right:1px !important;}#case_activity_list tbody tr td:nth-child(2){white-space: normal;}select#parties_case_specific{margin-bottom: 5px;}#emailDetailDiv .modal-body{padding: 15px !important;}#emailDetailDiv .mail-box{margin-bottom: 0px !important;}#case_activity_list_wrapper .dataTables_scrollBody{height:auto !important;max-height:350px; }.chosen-container {width: 100% !important;}.pac-container{z-index: 9999;}#addTaskForm label{font-weight:normal;}.container-checkbox{padding-bottom: 20px;}.ui-datepicker-trigger{border:none;background:none;position: absolute;right: 6px;top: 5px;}div#case_activity_list_processing{color:#ffffff;background-color:#4d7ab1;}div#party-table-data_processing{color:#ffffff;background-color:#4d7ab1;}.displayattatchment a { float: left !important;}.rdateerror{color:red;}.uk-dropdown{width:100%;}
#case_activity_list.table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td{word-break: break-word;}
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<input type="hidden" value="<?php echo $this->uri->segment(3); ?>" id="caseNo">
<input type="hidden" value="<?php echo $display_details->case_status; ?>" id="caseStatus">
<input type="hidden" value="<?php echo isset($display_details->social_sec) ? $display_details->social_sec : '' ?>" id="social_sec">

<?php
if (!empty($display_details->firm)) {
    $applicant_name = $display_details->firm;
    if ($display_details->first != '') {
        $handling = 'Handling attorney : ' . $display_details->first . ' ' . $display_details->last;
    }
} else {
    $applicant_name = $display_details->salutation;
    if ($display_details->first != '') {
        $applicant_name .= ' ' . $display_details->first;
    }
    if ($display_details->middle != '') {
        $applicant_name .= ' ' . $display_details->middle;
    }
    if ($display_details->last != '') {
        $applicant_name .= ' ' . $display_details->last;
    }
}
if(strlen($applicant_name) > 30) {
    $applicant_name = substr($applicant_name, 0, 29).'...';
}
if($RefName != '' && strlen($RefName) > 30) {
    $RefName = substr($RefName, 0, 29).'...';
}

if (isset($display_details->popular) && !empty($display_details->popular)) {
    if (strlen($display_details->popular) < 10) {
        $popular_name = '<span class = "f-size12 marg-bot5 popular"><b>goes by ' . $display_details->popular . '</b></span><br/>';
    } else {
        $popular_name = '<span class = "f-size12 marg-bot5 popular"><b>goes by ' . substr(0, 10, $display_details->popular) . '..</b></span><br/>';
    }
} else {
    $popular_name = '';
}

$applicant_address = '';
$applicant_address2 = '';
if ($display_details->address1 != '' || $display_details->address2 != '' || $display_details->city != '' || $display_details->state != '') {
    $applicant_address = ($display_details->address1 != '') ? ($display_details->address1 . ', ') : '';
    $applicant_address = ($display_details->address2 != '') ? (($applicant_address == '') ? $display_details->address2 . ', ' : $applicant_address . $display_details->address2 . ', ') : $applicant_address;
    $applicant_address = trim($applicant_address, ', ');
}

$applicant_address2 = ($display_details->city != '') ? (($applicant_address2 == '') ? $display_details->city . ', ' : $applicant_address2 . $display_details->city . ', ') : $applicant_address2;
$applicant_address2 = ($display_details->state != '') ? (($applicant_address2 == '') ? $display_details->state . ' ' : $applicant_address2 . $display_details->state . ' ') : $applicant_address2;
$applicant_address2 = ($display_details->zip != '') ? (($applicant_address2 == '') ? $display_details->zip . ', ' : $applicant_address2 . $display_details->zip . ', ') : $applicant_address2;
$applicant_address2 = trim($applicant_address2, ', ');

$opponent_name = '';
if (count($users) > 0) {
    if (isset($users['client'])) {
        $opponent_name = $users['client']->salutation;
        if ($users['client']->first != '') {
            $opponent_name .= ' ' . $users['client']->first;
        }
        if ($users['client']->middle != '') {
            $opponent_name .= ' ' . $users['client']->middle;
        }
        if ($users['client']->last != '') {
            $opponent_name .= ' ' . $users['client']->last;
        }
    }
}
$birthDate = date('m/d/Y', strtotime(isset($Prospect_data->birthdate) ? $Prospect_data->birthdate : 0));
$birthDate = explode("/", $birthDate);
$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
$filesudf = isset($Prospect_data->fieldsudf) ? $Prospect_data->fieldsudf : '';
$filesudf = explode(',', $filesudf);
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
}
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="breadcrumbs col-sm-3">
                    <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                    <a href="<?php echo base_url('cases'); ?>">Cases</a> /
                    <span><?php echo $case_no; ?></span>
                </div>
                <div class="col-sm-8">
                    <?php
                    if (isset($birthdaynotification) && !empty($birthdaynotification)) {
                        $bnoti = json_decode($birthdaynotification, true);
                        echo "<span class='bnoti'><b>" . $bnoti['title'] . "</b> " . $bnoti['type1'] . "</span>";
                    }
                    ?>
                </div>
                <div id="redalertAct" style="display:none;" class="sidebar col-lg-1 pull-right">
                    <div class="toggle"></div>
                    <div class="sidebar-row"><div id="contentData" class="sidebar-wrapper"></div></div>
                </div>

            </div>
            <input type="hidden" name="hdnCaseId" id="hdnCaseId" value="<?php echo $case_id; ?>" />

            <div class="row">

                <div class="col-sm-8" id="maincaption">
                    <?php if ($display_details->case_status == '1') {
                        ?>
                        <div class="sticky fadeIn animated marg-bot15 caption caption-opened" title="<?php echo $display_details->caption1; ?>">
                            <span id="caption_details">
                                <?php
                                if (strlen($display_details->caption1) > 55) {
                                    echo substr($display_details->caption1, 0, 55) . '...';
                                } else {
                                    echo substr($display_details->caption1, 0, 55);
                                }
                                ?>
                            </span>
                            <?php
                            if ($CaptionAccess->captionAccess == 1) {
                                ?>
                                <span style="position: absolute;top: 10px;right: 20px;"><a data-tab="caption_tab" class="pull-right ajaxdropdowncase" data-target="#editcasedetail"><i class="fa fa-pencil-square-o"></i></a></span>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } else {
                        ?>
                        <div class="sticky fadeIn animated marg-bot15 caption caption-closed" title="<?php echo $display_details->caption1 . "- DELETED"; ?>">
                            <span id="caption_details">
                                <?php
                                if (strlen($display_details->caption1) > 50) {
                                    echo substr($display_details->caption1, 0, 50) . '...' . "- DELETED";
                                } else {
                                    echo substr($display_details->caption1, 0, 50) . "- DELETED";
                                }
                                ?>
                            </span>
                            <span style="position: absolute;top: 10px;right: 20px;"><a data-tab="caption_tab" class="pull-right ajaxdropdowncase" data-target="#editcasedetail"><i class="icon-edit fa-5x"></i></a></span>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="col-sm-8" id="captiontext" style="display:none;">
                    <input type="text" id="caption_text" name="caption_text" value="<?php echo $display_details->caption1; ?>" style="width: 100%;padding-left: 30px;text-align: center;font-size: 21px;">
                </div>
                <div class = "col-sm-4">
                    <div class="utilities marg-bot15">
                        <ul>
                            <li><a onclick="return send_email_show();" title="Email"><i class="icon fa fa-envelope"></i></a></li>
                            <li><a href ="javaScript:void(0)" id="group_chat" title="Chat"><i class="icon fa fa-wechat"></i></a></li>
                            <li><a id="printCasedetails" data-toggle = "modal" title="Print"><i class="icon fa fa-print"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class = "tabs-container">
                <ul class = "nav nav-tabs case-details">
                    <li class = "active"><a data-toggle = "tab" href = "#casedetails">Case Details</a></li>
                    <li class = ""><a data-toggle = "tab" href = "#parties">Parties</a></li>
                    <li class = ""><a target="_blank" href = "<?php echo base_url('injury/' . $case_id) ?>">Injuries (<span id="count_injury"><?php echo $injury_cnt; ?></span>)</a></li>
                    <li class = ""><a data-toggle = "tab" href = "#media">Photos & Videos </a></li>
                    <li class = ""><a data-toggle = "tab" href = "#caseact">Case Activity</a></li>
                    <li class = ""><a target="_blank" href = "<?php echo base_url() ?>cases/documents/<?php echo $case_id; ?>">Documents</a></li>
                    <li class = ""><a target="_blank" href = "<?php echo base_url('calendar/' . $case_id) ?>">Calendar</a></li>
                    <li class = ""><a data-toggle = "tab" href = "#relcases">Related Cases <span id="count_related"></span></a></li>
                    <!--<li class = ""><a target="_blank" href = "<?php echo base_url('injury/' . $case_id) ?>">Injury Summary</a></li>-->
                    <li class = ""><a data-toggle = "tab" href = "#tools" class="showtool" id="showtools">Tools</a></li>
                    <li class = ""><a target="_blank" href = "<?php echo base_url('cases/tasks/' . $case_id) ?>">Tasks</a></li>
                    <li class = ""><a data-toggle = "tab" href = "#caseEmail">Related Email(<span class="related_cnt"><?php
                            if (isset($case_email)) {
                                echo ($case_email);
                            } else {
                                echo '0';
                            }
                            ?></span>)</a></li>
                </ul>
                <div class="tab-content">

                    <div id="casedetails" class="tab-pane active">
                        <div class = "panel-body">
                            <div class="float-e-margins">
                                <div class = "row">
                                    <div class = "col-sm-8 col-md-8 col-lg-6">
                                        <div class="ibox">
                                            <div class = "ibox-title">
                                                <!-- <h5>Applicant Details</h5> -->
                                                <h5>
                                                    <span id="card_Type"><?php echo ucwords(strtolower($display_details->cardtype)); ?></span> Information
                                                </h5><!-- edit-card -->
                                                <a class="edit-card pull-right ajaxdropdowncase btn btn-sm btn-primary" href="#addcontact" data-cardcode="<?php echo $display_details->cardcode; ?>">Edit</a>
                                            </div>
                                            <div class = "ibox-content marg-bot15 detailContainer">
                                                <div class = "row">
                                                    <div class = "col-sm-5">
                                                        <div class = "client-img marg-bot15" >
                                                            <?php
                                                            if (isset($display_details->picture)) {
                                                                $url = FCPATH . 'assets/rolodexprofileimages/' . $display_details->picture;
                                                                if (file_exists($url) && $display_details->picture != '') {
                                                                    ?>
                                                                    <img class = "img-circle" height="67px;" src = "<?php echo base_url('assets/rolodexprofileimages') . '/' . $display_details->picture; ?>" />

                                                                <?php } else { ?>
                                                                    <input type="hidden" id="applicant_ini" value="<?php echo $display_details->first; ?>">
                                                                    <div id="applicantImage" height="67" width="67" class="img-circle"></div>

                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <input type="hidden" id="applicant_ini" value="<?php echo $display_details->first; ?>">
                                                                <div id="applicantImage" height="67" width="67" class="img-circle"></div>

                                                                <?php
                                                            }
                                                            ?>
                                                        </div>

                                                    </div>
                                                    <div class = "col-sm-7">
                                                        <div class = "client-personal">
                                                            <span class = "f-size17 marg-bot5 applicant_name"><b><?php echo $applicant_name; ?></b></span><br/>
                                                            <?php echo $popular_name; ?>

<!--                                                            <span class="marg-bot5 handling_attr f-size15"><?php echo $handling; ?></span><br/>-->
                                                            <span class="marg-bot5 applicant_address f-size15"><?php echo $applicant_address; ?></span><br/>
                                                            <span class="marg-bot5 applicant_address2 f-size15"><?php echo $applicant_address2; ?></span><br/>
                                                            <span class="marg-bot5 f-size15 viewonmap <?php echo $applicant_address == "" ? 'hidden' : ''; ?>"><a target="_blank" id="address_map" href="https://maps.google.com/?q=<?php echo $applicant_address . $applicant_address2; ?>">View on map</a></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <span class="marg-bot5 f-size15"><i class="icon fa fa-phone pull-left marg-top2"></i> &nbsp; (H)<span class="home"> <?php echo ($display_details->home != '') ? $display_details->home : 'NA'; ?></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <?php if($display_details->car != ''){ ?>
                                                        <span class="marg-bot5 f-size15"><i class="icon fa fa-phone car pull-left marg-top2"></i> &nbsp; <span class="business" >(C) <?php echo ($display_details->car != '') ? $display_details->car : 'NA'; ?></span></span>
                                                        <?php }
                                                        elseif($display_details->phone1 != ''){ ?>
                                                        <span class="marg-bot5 f-size15"><i class="icon fa fa-phone car pull-left marg-top2"></i> &nbsp; <span class="business" >(B) <?php echo ($display_details->phone1 != '') ? $display_details->phone1 : 'NA'; ?></span></span>
                                                        <?php }elseif($display_details->business != ''){ ?>
                                                        <span class="marg-bot5 f-size15"><i class="icon fa fa-phone car pull-left marg-top2"></i> &nbsp; <span class="business" >(B) <?php echo ($display_details->business != '') ? $display_details->business : 'NA'; ?></span></span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <span class="marg-bot5 f-size15 emailalignment"><i class="icon fa fa-envelope pull-left marg-top2"></i>&nbsp;<span class="email"> <?php echo ($display_details->email != '') ? $display_details->email : 'NA'; ?></span></span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <span class="marg-bot5 f-size15"><i class="icon fa fa-calendar pull-left  marg-top2"></i>&nbsp;
                                                            <?php if ($display_details->birth_date != '' && $display_details->birth_date != '1970-01-01 00:00:00') { ?>
                                                                <span class="birth_date f-size15"> Born on <?php echo date('m/d/Y', strtotime($display_details->birth_date)); ?>, <?php echo 'Age: '. ($this->common_functions->get_timeago(strtotime(date("m/d/Y", strtotime($display_details->birth_date))))); ?></span>
                                                                <?php
                                                            } else {
                                                                echo '<span class="birth_date">NA</span>';
                                                            }
                                                            ?>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-5 f-size15">
                                                        <span class="marg-bot5 interpret"><b>Interp </b>
                                                            <span id="interp_text">
                                                            <?php echo ($display_details->interpret != '') ? $display_details->interpret : 'NA'; ?>
                                                            <?php echo ($display_details->language != '') ? $display_details->language : 'NA'; ?>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <span class="marg-bot5 interpret f-size15"><i class="icon fa fa-key pull-left marg-top2"></i>&nbsp; <b>SS</b> <span class="ssNo"><?php echo ($display_details->social_sec != '') ? $display_details->social_sec : 'NA'; ?></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-3">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Case Information</h5>
                                                <a data-tab="case_info" class="pull-right ajaxdropdowncase btn btn-sm btn-primary" data-target="#editcasedetail">Edit</a>
                                            </div>
                                            <div class="ibox-content marg-bot15 detailContainer">
                                                <table border="1" width="100%" class="caseDateTbl">
                                                    <tr>
                                                        <td><strong>Type:</strong></td>
                                                        <td class="casetype"><?php echo $display_details->casetype; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Status:</strong></td>
                                                        <td class="casestat"><?php echo ($display_details->casestat != '') ? $display_details->casestat : 'NA'; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>File No:</strong></td>
                                                        <td class="yourfileno"><?php echo ($display_details->yourfileno != '') ? $display_details->yourfileno : 'NA'; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Venue:</strong></td>
                                                        <td>
                                                            <span class="venue" id="locationDisplay"><?php echo ($display_details->Displayvenue != '') ? $display_details->Displayvenue : ''; ?></span>
                                                            <?php if (isset($case_details['court']->venue) && $case_details['court']->venue != '') { ?>
                                                                <br>
                                                                <span><a target="_blank" href="https://maps.google.com/?q=<?php echo $case_details['court']->venue ?>">View On Map</a></span>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>CaseNo:</strong></td>
                                                        <td><?php echo $case_no; ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-12 col-lg-3" id="calenderEventDiv">
                                        <?php
                                        if (isset($next_calendar_event)) {
                                            $date = date_create($next_calendar_event);
                                            $nw_dt = date_format($date, "Y-m-d");
                                            $new_tm = $next_calendar_event_tym;
                                            ?>
                                            <a href="<?php echo base_url() . 'calendar/' . $case_id ?>" target="_blank">


                                                <div class="claendar-event fadeIn animated marg-bot15">
                                                    <span>Next Calendar Event : <?php echo $next_calendar_event_event; ?></span><br/>
                                                    <span id="Nxtevtdt"><?php echo isset($next_calendar_event) ? $next_calendar_event . "  " . $new_tm : 'N/A'; ?></span><br/>
                                                    <span>Event Venue : <?php echo isset($next_calendar_event_venue) ? $next_calendar_event_venue : 'N/A'; ?></span><br/>
                                                    <span>Event Attorney : <?php echo isset($next_calendar_event_from) ? $next_calendar_event_from : 'N/A'; ?></span>
                                                </div>
                                            <?php } else { ?>
                                                <div class="claendar-event fadeIn animated marg-bot15" style="padding-top:0px !important;">
                                                    <span id="Nxtevtdt"><h3 class="noevent">No Event Available</h3></span>
                                                </div>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <!--                                </div> -->
                                    <div class="clearfix"></div>
                                    <!--     <div class="row">-->
                                    <div class="col-sm-6 col-lg-4 caseDates">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Staff Information</h5>
                                                <a data-tab="staff_info" class="pull-right ajaxdropdowncase btn btn-sm btn-primary" data-target="#editcasedetail">Edit</a>
                                            </div>
                                            <div class="ibox-content staff-info">
                                                <table border="1" width="100%" class="caseDateTbl">
                                                    <tr>
                                                        <td><strong>Attorney Resp:</strong></td>
                                                        <td>
                                                            <?php
                                                            if ($display_details->atty_resp != '') {
                                                                $imgResp = $this->common_functions->getProfileImage($display_details->atty_resp);
                                                                $imgRespurl = FCPATH . 'assets/profileimages/' . $imgResp;
                                                                if (file_exists($imgRespurl) && !empty($imgResp)) {
                                                                    ?>
                                                                    <img alt="image" class="img-circle" src="<?php echo base_url('assets/profileimages') . '/' . $imgResp; ?>">
                                                                <?php } else { ?>
                                                                    <input type="hidden" id="atty_resp_ini" value="<?php echo $display_details->atty_resp; ?>">
                                                                    <div class="staffimg img-circle" id="imgRespurl" height="20" width="20" class="img-circle"><?php echo substr($display_details->atty_resp, 0, 1); ?></div>
                                                                <?php } ?>
                                                                <p class="atty_resp"><?php echo $display_details->atty_resp; ?></p>
                                                            <?php } else { ?>
                                                                <input type="hidden" id="atty_resp_ini" value="">
                                                                <div class="staffimg img-circle" id="imgRespurl" height="20" width="20" class="img-circle">&nbsp;</div>
                                                                <p class="atty_resp">NA</p>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Parra Hand : </strong></td>
                                                        <td>
                                                            <?php
                                                            if ($display_details->para_hand != '') {
                                                                $imgParaHand = $this->common_functions->getProfileImage($display_details->para_hand);
                                                                $imgParaurl = FCPATH . 'assets/profileimages/' . $imgParaHand;
                                                                if (file_exists($imgParaurl) && !empty($imgParaHand)) {
                                                                    ?>
                                                                    <img alt="image" class="img-circle" src="<?php echo base_url('assets/profileimages') . '/' . $imgParaHand; ?>">
                                                                <?php } else { ?>
                                                                    <input type="hidden" id="para_hand_ini" value="<?php echo $display_details->para_hand; ?>">
                                                                    <div class="staffimg img-circle" id="imgParaurl" height="20" width="20" class="img-circle"><?php echo substr($display_details->para_hand, 0, 1); ?></div>
                                                                <?php } ?>
                                                                <p class="para_hand"><?php echo $display_details->para_hand; ?></p>
                                                            <?php } else { ?>
                                                                <input type="hidden" id="para_hand_ini" value="">
                                                                <div class="staffimg img-circle" id="imgParaurl" height="20" width="20" class="img-circle">&nbsp;</div>
                                                                <p class="para_hand">NA</p>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Attorney Hand : </strong></td>
                                                        <td>
                                                            <?php
                                                            if ($display_details->atty_hand != '') {
                                                                $imgAttyHand = $this->common_functions->getProfileImage($display_details->atty_hand);
                                                                $imgAttyHandurl = FCPATH . 'assets/profileimages/' . $imgAttyHand;
                                                                if (file_exists($imgAttyHandurl) && !empty($imgAttyHand)) {
                                                                    ?>
                                                                    <img alt="image" class="img-circle" src="<?php echo base_url('assets/profileimages') . '/' . $imgAttyHand; ?>">
                                                                <?php } else { ?>
                                                                    <input type="hidden" id="atty_hand_ini" value="<?php echo $display_details->atty_hand; ?>">
                                                                    <div class="staffimg img-circle" id="imgAttyHandurl" height="20" width="20" class="img-circle"><?php echo substr($display_details->atty_hand, 0, 1); ?></div>
                                                                <?php } ?>
                                                                <p class="atty_hand"><?php echo $display_details->atty_hand; ?></p>
                                                            <?php } else { ?>
                                                                <input type="hidden" id="atty_hand_ini" value="">
                                                                <div class="staffimg img-circle" id="imgAttyHandurl" height="20" width="20" class="img-circle">&nbsp;</div>
                                                                <p class="atty_hand">NA</p>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Sec Hand : </strong></td>
                                                        <td>
                                                            <?php
                                                            if ($display_details->sec_hand != '') {
                                                                $imgSecHand = $this->common_functions->getProfileImage($display_details->sec_hand);
                                                                $imgSecHandurl = FCPATH . 'assets/profileimages/' . $imgSecHand;
                                                                if (file_exists($imgSecHandurl) && !empty($imgSecHand)) {
                                                                    ?>
                                                                    <img alt="image" class="img-circle" src="<?php echo base_url('assets/profileimages') . '/' . $imgSecHand; ?>">
                                                                <?php } else { ?>
                                                                    <input type="hidden" id="sec_hand_ini" value="<?php echo $display_details->sec_hand; ?>">
                                                                    <div class="staffimg img-circle" id="imgSecHandurl" height="20" width="20" class="img-circle"><?php echo substr($display_details->sec_hand, 0, 1); ?></div>
                                                                <?php } ?>
                                                                <p class="sec_hand"><?php echo $display_details->sec_hand; ?></p>
                                                            <?php } else { ?>
                                                                <input type="hidden" id="sec_hand_ini" value="">
                                                                <div class="staffimg img-circle" id="imgSecHandurl" height="20" width="20" class="img-circle">&nbsp;</div>
                                                                <p class="sec_hand">NA</p>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class = "col-sm-6 col-lg-4 caseDates">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Case Dates/Important Dates</h5>
                                                <a data-tab="case_dates" class="pull-right ajaxdropdowncase btn btn-sm btn-primary" data-target="#editcasedetail">Edit</a>
                                            </div>
                                            <div class="ibox-content marg-bot15">
                                                <table border="1" width="100%" class="caseDateTbl" id="case_dates_tbl">
                                                    <tr>
                                                        <td><strong>Entered : </strong></td>
                                                        <td>
                                                            <?php if ($display_details->dateenter != '' && $display_details->dateenter != 0) { ?>
                                                                <span class="dateenter"><?php
                                                                    $entime = $display_details->dateenter;
                                                                    $endt = new DateTime($entime);
                                                                    $ennwdt = $endt->format('m/d/Y');

                                                                    if ($ennwdt == '01/01/1970' || $ennwdt == '12/30/1899') {
                                                                        echo 'NA';
                                                                    } else {
                                                                        echo $ennwdt;
                                                                    }
                                                                    ?></span>
                                                            <?php } else { ?>
                                                                <span class="dateenter">NA</span>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>Open : </strong></td>
                                                        <td>
                                                            <?php if ($display_details->dateopen != '' && $display_details->dateopen != 0) { ?>
                                                                <span class="dateopen"><?php
                                                                    $opntime = $display_details->dateopen;
                                                                    $opndt = new DateTime($opntime);
                                                                    $opnnwdt = $opndt->format('m/d/Y');

                                                                    if ($opnnwdt == '01/01/1970' || $opnnwdt == '12/30/1899') {
                                                                        echo 'NA';
                                                                    } else {
                                                                        echo $opnnwdt;
                                                                    }
                                                                    ?></span>
                                                            <?php } else { ?>
                                                                <span class="dateopen">NA</span>
                                                            <?php } ?>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>F/U : </strong></td>
                                                        <td>
                                                            <?php if ($display_details->followup != '' && $display_details->followup != 0) { ?>
                                                                <span class="followup"><?php
                                                                    $futime = $display_details->followup;
                                                                    $fudt = new DateTime($futime);
                                                                    $funwdt = $fudt->format('m/d/Y');

                                                                    if ($funwdt == '01/01/1970' || $funwdt == '12/30/1899') {
                                                                        echo 'NA';
                                                                    } else {
                                                                        echo $funwdt;
                                                                    }
                                                                    ?></span>
                                                            <?php } else { ?>
                                                                <span class="followup">NA</span>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td><strong>Refer By : </strong></td>
                                                        <td>
                                                            <?php
                                                            $name = "";
                                                            if (!empty($case_cards[0]->rb)) {
                                                                if (isset($case_cards) && $case_cards != '') {
                                                                    foreach ($case_cards as $key => $details) {
                                                                        if (!empty($details->firm)) {
                                                                            $name = $details->firm;
                                                                        } else {
                                                                            $name = $details->last . ', ' . $details->first;
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                $RefName = 'NA';
                                                            }
                                                            if (!empty($case_cards[0]->rb)) {
                                                                ?>
                                                                <span class="rb"><a class="edit-card" data-cardcode="<?php echo $case_cards[0]->rb ?>"><?php echo ($RefName != '') ? $RefName : 'NA'; ?></a></span>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <span class="rb"><?php echo ($name != '') ? $name : 'NA'; ?></span>
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    if ($refdt == 1) {
                                                        ?>
                                                        <tr>
                                                            <td><strong>R/D : </strong></td>
                                                            <td>
                                                                <?php if ($display_details->d_r != '' && $display_details->d_r != 0) { ?>
                                                                    <span class="d_r"><?php
                                                                        $d_rtime = $display_details->d_r;
                                                                        $d_rdt = new DateTime($d_rtime);
                                                                        $d_rdtnwdt = $d_rdt->format('m/d/Y');

                                                                        if ($d_rdtnwdt == '01/01/1970' || $d_rdtnwdt == '12/30/1899') {
                                                                            echo 'NA';
                                                                        } else {
                                                                            echo $d_rdtnwdt;
                                                                        }
                                                                        ?></span>
                                                                <?php } else { ?>
                                                                    <span class="d_r">NA</span>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    if ($pnsdt == 1) {
                                                        ?>
                                                        <tr>
                                                            <td><strong>P/S Date : </strong></td>
                                                            <td>
                                                                <?php if ($display_details->psdate != '' && $display_details->psdate != 0) { ?>
                                                                    <span class="ps"><?php
                                                                        $pstime = $display_details->psdate;
                                                                        $psdt = new DateTime($pstime);
                                                                        $psdtnwdt = $psdt->format('m/d/Y');

                                                                        if ($psdtnwdt == '01/01/1970' || $psdtnwdt == '12/30/1899') {
                                                                            echo 'NA';
                                                                        } else {
                                                                            echo $psdtnwdt;
                                                                        }
                                                                        ?></span>
                                                                <?php } else { ?>
                                                                    <span class="ps">NA</span>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><strong>Closed : </strong></td>
                                                        <td>
                                                            <?php if ($display_details->dateclosed != '' && $display_details->dateclosed != 0) { ?>
                                                                <span class="dateclosed"><?php
                                                                    $dateclosed = $display_details->dateclosed;
                                                                    $dtcl = new DateTime($dateclosed);
                                                                    $dtclnw = $dtcl->format('m/d/Y');

                                                                    if ($dtclnw == '01/01/1970' || $dtclnw == '12/30/1899') {
                                                                        echo 'NA';
                                                                    } else {
                                                                        echo $dtclnw;
                                                                    }
                                                                    ?></span>
                                                            <?php } else { ?>
                                                                <span class="dateclosed">NA</span>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-4 caseDates">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Quick Notes</h5>
                                                <a data-tab="quick_note" class="pull-right ajaxdropdowncase btn btn-sm btn-primary" data-target="#editcasedetail">Edit</a>
                                            </div>
                                            <div class="ibox-content marg-bot15">
                                                <textarea readonly style="resize: none;height:210px;width: 100%;border:none;background-color: #f5f5f5;" class="notes"><?php echo isset($category_array) ? $category_array : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-lg-12">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Task List</h5>
                                                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#addtask">Add Task</button>
                                            </div>
											 <div class="ibox-content casecontainer searchoxevent">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover case_tasks">
                            <thead>
                                <tr>
                                    <th align="right">Task Date</th>
                                    <th align="center">From</th>
                                    <th align="center">Tasks Detail</th>
                                    <th align="center">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
											
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="ibox">
                                            <?php
                                            if (isset($message1) && !empty($message1)) {
                                                $style1 = '';
                                            } else {
                                                $style1 = 'display:none';
                                            }
                                            ?>

                                            <div class="ibox-title" id="div_casemessage1" style="<?php echo $style1; ?>">
                                                <a data-tab="messages"  data-target="#editcasedetail">
                                                    <textarea class="form-control text-center" name="casemessage1" id="casemessage1" style="height: 30px;width: 100%;background:<?php echo isset($main1->msg1) ? $main1->msg1 : '' ?>;color:<?php echo isset($main1->msg1c) ? $main1->msg1c : '' ?>"><?php echo $message1; ?></textarea></a>
                                            </div>

                                            <?php
                                            if (isset($message2) && !empty($message2)) {
                                                $style2 = '';
                                            } else {
                                                $style2 = 'display:none';
                                            }
                                            ?>

                                            <div class="ibox-title" id="div_casemessage2" style="<?php echo $style2; ?>">
                                                <a data-tab="messages"  data-target="#editcasedetail">
                                                    <textarea class="form-control text-center" name="casemessage2" id="casemessage2" style="height: 30px;width: 100%;background:<?php echo isset($main1->msg2) ? $main1->msg2 : '' ?>;color:<?php echo isset($main1->msg2c) ? $main1->msg2c : '' ?>"><?php echo $message2; ?></textarea></a>
                                            </div>
                                            <?php
                                            if (isset($message3) && !empty($message3)) {
                                                $style3 = '';
                                            } else {
                                                $style3 = 'display:none';
                                            }
                                            ?>

                                            <div class="ibox-title" id="div_casemessage3" style="<?php echo $style3; ?>">
                                                <a data-tab="messages"  data-target="#editcasedetail" >
                                                    <textarea class="form-control text-center" name="casemessage3" id="casemessage3" style="height: 30px;width: 100%;background:<?php echo isset($main1->msg3) ? $main1->msg3 : '' ?>;color:<?php echo isset($main1->msg3c) ? $main1->msg3c : '' ?>"><?php echo $message3; ?></textarea>
                                                </a>
                                            </div>
                                            
                                            <?php
                                            if (isset($message4) && !empty($message4)) {
                                                $style4 = '';
                                            } else {
                                                $style4 = 'display:none';
                                            }
                                            ?>

                                            <div class="ibox-title" id="div_casemessage4" style="<?php echo $style4; ?>">
                                                <a data-tab="messages"  data-target="#editcasedetail" >
                                                    <textarea class="form-control text-center" name="casemessage4" id="casemessage4" style="height: 30px;width: 100%;background:<?php echo isset($main1->msg4) ? $main1->msg4 : '' ?>;color:<?php echo isset($main1->msg4c) ? $main1->msg4c : '' ?>"><?php echo $message4; ?></textarea>
                                                </a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="parties" class="tab-pane">
                        <div class="panel-body">
                            <div class="float-e-margins">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5 style="float: left;margin-top: -2px;" >Parties involved in case
                                                    <small style="display: block;margin-top: 0px;">(Use drag &AMP; drop on number column for reordering)</small>
                                                </h5>
                                                
                                                <a class="pull-right addparty btn btn-sm btn-primary" href="" data-toggle="modal" >Add Party</a>
                                            </div>
                                            <div class="ibox-content marg-bot15 party-in">
                                                <form>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="table-responsive party-table">
                                                                    <table id="party-table-data"  class="table table-bordered table-hover table-striped" style="width:100% !important;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No</th>
                                                                                <th>Full Name</th>
                                                                                <th>Role</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody></tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Party Details</h5>
                                                <a href="#" class="pull-right edit-party btn btn-primary btn-sm"  id="edit-party" data-case="" data-casecard="">Edit</a>
                                            </div>
                                            <div class="ibox-content marg-bot15 party">
                                                <form>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-lg-12 party_showhide main_address_drp">
                                                                <div class="table-responsive">
                                                                    <table class="table no-bottom-margin">
                                                                        <tbody>
                                                                            <tr>
<!--                                                                                <td class="text-left" width="110"><strong><a class="" href="#partiesNameChangeWarning" data-toggle="modal" data-dismiss="modal">Name :</a></strong></td>-->
                                                                                <td class="text-left" width="110"><strong><a class="edit-party" >Name :</a></strong></td>
                                                                                <td id="party_name" data-cardcodeWarning=""></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong><a class="typeLink" href="" data-toggle="modal" data-dismiss="modal">Type :</a></strong></td>
                                                                                <td id="party_type" data-cardcodeWarning=""></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Firm :</strong></td>
                                                                                <td id="firm_name"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong><a class="addressVal" target="_blank" href="javascript:;">Address :</a></strong></td>
                                                                                <td id="party_address1"></td>
                                                                                <td class="clone-address" style="cursor: pointer;"><a href="javascript:void(0)" title="Copy to Clipboard"><i class="fa fa-clone" aria-hidden="true"></i></a></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Address 2 :</strong></td>
                                                                                <td id="party_address2"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>City :</strong></td>
                                                                                <td id="party_city"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Phone Home :</strong></td>
                                                                                <td id="party_phn_home"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Phone Office :</strong></td>
                                                                                <td id="party_phn_office"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Fax :</strong></td>
                                                                                <td id="party_fax"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Cell :</strong></td>
                                                                                <td id="party_cell"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Email :</strong></td>
                                                                                <td id="party_email"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>Office No :</strong></td>
                                                                                <td id="party_offc_no"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong><a class="typeLink" href="" data-toggle="modal" data-dismiss="modal">Notes :</a></strong></td>
                                                                                <td id="party_notes"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-left" width="110"><strong>EAMS :</strong></td>
                                                                                <td id="party_eams"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 party_showhide party_notes_drp" style="display: none;">
                                                                <div class="table-responsive">
                                                                    <table class="table no-bottom-margin">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="text-right" width="145"><strong>Personal Comments:</strong></td>
                                                                                <td id="comment_per"></td>
                                                                            </tr>
                                                                            <tr class="busComment">
                                                                                <td class="text-right" width="145"><strong>Business Comments :</strong></td>
                                                                                <td id="comment_bus"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="media" class="tab-pane">
                        <div class="panel-body">
                            <div class="float-e-margins">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Photos</h5>
                                            </div>
                                            <div class="ibox-content marg-bot15">
                                                <div class="image-row lightBoxGallery">
                                                    <?php
                                                    $count = 0;
                                                    $files = glob(DOCUMENTPATH . 'casePhotoVideo/' . $case_no . '/photo/*');
                                                    if (count($files) > 0) {
                                                        foreach ($files as $file) {
                                                            $fileArr = explode("/", $file);
                                                            ?>
                                                            <div class="imgGallery">
                                                                <a class="example-image-link scroller-el" data-lightbox="example-1" href="<?php echo base_url() . 'assets/casePhotoVideo/' . $case_no . '/photo/' . $fileArr[count($fileArr) - 1]; ?>" title="Image from <?php echo APPLICATION_NAME; ?>" data-gallery="">
                                                                    <img class="example-image"  src="<?php echo base_url() . 'assets/casePhotoVideo/' . $case_no . '/photo/' . $fileArr[count($fileArr) - 1]; ?>" width="100px" height="100px">
                                                                </a>
                                                                <i class="fa fa-close" id="del_photo" data-id="<?php echo DOCUMENTPATH . 'casePhotoVideo/' . $case_no . '/photo/' . $fileArr[count($fileArr) - 1]; ?>"> </i>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <?php
                                                if (count($files) > 0) {
                                                    ?>
                                                    <div class="pull-right">
                                                        <a href="#" class="prev btn btn-primary btn-sm"><span class="icon fa fa-angle-left"></span></a>
                                                        <a href="#" class="next btn btn-primary btn-sm"><span class="icon fa fa-angle-right"></span></a>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class=" fileContainer upload-button marg-top15">
                                                    <form id="uploadphotofileForm" method="post" enctype="">
                                                        <input type="file" class=""   id="uploadBtn" accept="image/*" name="uploadBtn[]" multiple >
                                                        <button type="button" id="uploadFile" class="btn btn-sm btn-primary">Upload More Photos</button>
                                                        <div><span>Upload gif,png,jpg or jpeg only(Max upload size 20MB)</span></div>
                                                        <div id="myProgress1" style="display:none">
                                                            <div id="myBar1"></div>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Videos</h5>
                                            </div>
                                            <div class="ibox-content marg-bot15">
                                                <div class="video clearfix">
                                                    <?php
                                                    $count = 0;
                                                    $files = glob(DOCUMENTPATH . 'casePhotoVideo/' . $case_no . '/video/*');

                                                    if (count($files) > 0) {
                                                        $i = 0;
                                                        foreach ($files as $file) {
                                                            $fileArr = explode("/", $file);
                                                            ?>  <div class="col-md-6 videoset" style="margin-bottom: 10px;"><a class="case_video"  data-video="<?php echo base_url() . 'assets/casePhotoVideo/' . $case_no . '/video/' . $fileArr[8]; ?>" data-toggle="modal" data-target="#videoModal">
                                                                    <video controls><source src="<?php echo base_url() . 'assets/casePhotoVideo/' . $case_no . '/video/' . $fileArr[8]; ?>" />
                                                                    </video>
                                                                </a><i class="fa fa-close" id="del_video" data-id="<?php echo DOCUMENTPATH . 'casePhotoVideo/' . $case_no . '/video/' . $fileArr[8]; ?>"> </i></div>
                                                            <?php
                                                            if ($i % 2) {
                                                                ?>
                                                                <div class="clearfix"></div>
                                                                <?php
                                                            }
                                                            $i++;
                                                        }
                                                    }
                                                    ?>

                                                </div>
                                                <?php
                                                if (count($files) > 0) {
                                                    ?>
                                                    <div class="text-right">

                                                        <a href="#" class="vprev btn btn-primary btn-sm"><span class="icon fa fa-angle-left"></span></a>
                                                        <a href="#" class="vnext btn btn-primary btn-sm"><span class="icon fa fa-angle-right"></span></a>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                                <div class="fileContainer upload-button marg-top15">
                                                    <form id="uploadFileFrom">
                                                        <input type="file" class=""   data-id="video" id="uploadBtn1" name="uploadBtn1[]" accept="video/*" multiple/>
                                                        <button type="button" id="uploadFile" class="btn btn-sm btn-primary">Upload Videos</button>
                                                        <div><span>Upload mp4 only(Max upload size  20MB)</span></div>
                                                        <div id="myProgress" style="display:none">
                                                            <div id="myBar"></div>
                                                        </div>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="caseact" class="tab-pane">
                        <div class="panel-body">
                            <div class="ibox float-e-margins">
                                <div class="">
                                    <div class="ibox-title inline-block-100">
                                        <h5 class="marg-top5">Case Activity</h5>
                                        <a class="btn btn-sm btn-primary pull-right no-bottom-margin add_case_activity marg-left5" href="javascript:;" >Add new</a>
                                        <!-- <a class="btn pull-right btn-primary no-bottom-margin btn-sm " id="addtaskModal" >Add Task</a> -->
                                    </div>
                                    <div class="ibox-content marg-bot15">
                                        <div class="filters filters-cat">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <select id="Category" name="Category" class="form-control select2Class width125">
                                                    <?php foreach ($caseactcategory as $k => $v) { ?>
                                                        <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="i-checks">
                                                    <label>
                                                        <input type="checkbox" name="all_category" id="all_category" checked />
                                                        <i></i> Show All
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table id="case_activity_list" class="table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Event</th>
                                                        <th>Initials</th>
                                                        <th>Original</th>
                                                        <th>Document</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbodyid"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="calendar" class="tab-pane ">
                        <div class="panel-body">
                            <div class="ibox float-e-margins">
                                <div class="">
                                    <div class="ibox-title inline-block-100">
                                        <h5 class="marg-top5">Calendar</h5>
                                        <a class="btn btn-sm btn-primary pull-right no-bottom-margin add_calendar_event" data-attr="<?php echo $case_no ?>" id="CalAddEvt" href="javascript:void(0);">Add Event</a>
                                    </div>
                                    <div class="ibox-content marg-bot15">
                                        <div class="table-responsive">
                                            <table id="caseDetailCalendarTable" class="table table-striped table-bordered table-hover dataTables-example" >
                                                <thead>
                                                    <tr>
                                                        <th style="width: 8%;">DateTime</th>
                                                        <th style="width: 5%;">AttH</th>
                                                        <th style="width: 5%;">AttA</th>
                                                        <th style="width: 17%;">Name vs Defendant</th>
                                                        <th style="width: 17%;">Event</th>
                                                        <th style="width: 11%;">Venue</th>
                                                        <th style="width: 11%;">Judge</th>
                                                        <th style="width: 10%;">Notes</th>
                                                        <th style="width: 16%;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="relcases" class="tab-pane">
                        <div class="panel-body">
                            <div class="ibox float-e-margins">
                                <div class="">
                                    <div class="ibox-title inline-block-100">
                                        <h5 class="marg-top5">Related Cases</h5>
                                        <?php
                                        if (isset($case_cards[0]->social_sec) && !empty($case_cards[0]->social_sec)) {
                                            $displayStyle = '';
                                        } else {
                                            $displayStyle = 'disabled="disabled"';
                                        }
                                        ?>
                                        <a class="btn pull-right btn-primary btn-sm no-bottom-margin marg-left5 attach_case" <?php echo $displayStyle; ?>  href="javascript:void(0);"> Attach Case</a>

                                        <a  class="btn pull-right btn-primary btn-sm no-bottom-margin" href="#addRelatedCase" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Related Case</a>

                                    </div>
                                    <div class="ibox-content marg-bot15">
                                        <div class="table-responsive">
                                            <table id="related_case_table" class="table table-striped table-bordered table-hover" style="width: 100%;" >
                                                <thead>
                                                    <tr>
                                                        <th>Case Number</th>
                                                        <th>Type of Case</th>
                                                        <th>Case Status</th>
                                                        <th>AttyH</th>
                                                        <th>Case Caption</th>
                                                        <th>Action </th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tools" class="tab-pane showtool">
                        <div class="panel-body">
                            <div class="float-e-margins">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5>Operations</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">
                                                        <a href="#" data-toggle="modal" data-target="#clonecase" class="btn btn-block btn-md btn-primary">Clone Case</a>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">
                                                        <a href="#" class="btn btn-block btn-md btn-primary" disabled="disabled">Pack Case Activity</a>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">
                                                        <a href="#" data-toggle="modal" data-target="#protactcase" class="btn btn-block btn-md btn-info">Protect Case</a>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">
                                                        <a  href="javaScript:void(0)" id="group_chat" class="btn btn-block btn-md btn-primary">Start Chat</a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">

                                                        <button class="btn btn-block btn-md btn-info case_delete">Delete Case</button>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">
                                                        <a data-toggle="modal" id="printCase" value="printCase" class="btn btn-block btn-md btn-info">Print Case</a>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">
                                                        <button class="btn btn-block btn-md btn-info recall_case">Recall Case</button>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-3 col-lg-3">
                                                        <a target="_blank" href = "<?php echo base_url() ?>cases/documents/<?php echo $case_id; ?>" class="btn btn-block btn-md btn-info">Forms</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5 class="marg-top5">New - Email</h5>
                                                <div style="float: right; margin-top: -3px;">
                                                    <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send" id="mailsenddisable1" style="margin: 0px;"><i class="fa fa-reply"></i> Send</button>
                                                    <a href="javascript:;" class="btn btn-white btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                                                </div>
                                            </div>
                                            <div class="ibox-content marg-bot15">
                                                <form class="form-horizontal" id="send_mail_form"  name="send_mail_form" method="post" enctype="multipart/form-data">
                                                    <div class="form-group"><label class="col-xs-3 col-lg-2 control-label">Internal User <span class="asterisk">*</span></label>
                                                        <div class="col-xs-9 col-lg-10">
                                                            <select name="whoto" id="whoto" style="width:100%" multiple  placeholder="To">
                                                                <option value="">-- Select User to Send Email --</option>
                                                                <?php foreach ($staff_list as $staff) { ?>
                                                                    <option value="<?php echo $staff->initials; ?>"><?php echo $staff->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"><label class="col-xs-3 col-lg-2 control-label">External Parties <span class="asterisk">*</span></label>
                                                        <div class="col-xs-9 col-lg-10">
                                                            <input type="text" name="emails" id="emails" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-xs-3 col-lg-2 control-label">Subject</label>
                                                        <div class="col-xs-9 col-lg-10">
                                                            <input type="text" name="mail_subject" id="mail_subject" class="form-control">
                                                            <input type="hidden" name="filenameVal" id="filenameVal">
                                                        </div>
                                                    </div>

                                                    <div class="form-group inline-block marg-right40 urgent-form-group">
                                                        <label class="col-xs-2 control-label">Urgent:</label>
                                                        <input type="checkbox" name="mail_urgent" id="mail_urgent" value="Y" class="i-checks">
                                                    </div>

                                                    <div class="form-inline pull-right">
                                                        <label class="marg-right10">Case No:</label>
                                                        <input type="text" name="case_no" id="case_no" class="form-control" placeholder="" readonly value="<?php echo $case_no; ?>" />
                                                    </div>
                                                    <div class=" form-group mail-text h-200" style="padding-right:15px; padding-left:15px;">
                                                        <label>Mail Content <span class="asterisk">*</span></label>
                                                        <textarea id="mailbody"  name="mailbody"></textarea>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="form-group text-right inline-block-100">
                                                        <div class="col-sm-12">
														<div id="selectedloader"></div>
														<div id="selectedFiles"></div>
                                                            <div class="col-xs-9 att-add-file1 marg-top5"> <input type="file" name="afiles[]" id="afiles" multiple > <!--<i class="fa fa-plus-circle marg-top5 addfile" aria-hidden="true"></i>--></div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="col-sm-12 m-t-sm">
                                                            <button class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Send" id="mailsenddisable"><i class="fa fa-reply"></i> Send</button>
                                                            <a href="javascript:;" class="btn btn-white btn-sm discard_message" data-toggle="tooltip" data-placement="bottom" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                                                        </div>
                                                        <div class="col-xs-12 supported-files text-center"><b>Note: </b> File size upload upto 20 MB <b>Supported Files :</b> PNG, JPG, JPEG, BMP,TXT, ZIP,RAR,DOC,DOCX,PDF,XLS,XLSX,PPT,PTTX.MP4 </div>
                                                        <div class="clearfix"></div>
                                                        <input type="hidden" name="mailType" id="mailType">
                                                        <input type="hidden" name="totalFileSize" id="totalFileSize" value="0">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="caseEmail" class="tab-pane">
                        <div class="panel-body">
                            <div class="ibox float-e-margins">
                                <div class="">
                                    <div class="ibox-title inline-block-100">
                                        <?php
                                        if (isset($case_email) && !empty($case_email)) {
                                            ?>
                                            <div class="mail-box">
                                                <table class="table table-hover table-mail table-hover dataTables-example" id="related_email">
                                                    <thead>
                                                        <tr>
                                                            <td>From </td>
                                                            <td>Subject</td>
                                                            <td class="">Date</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php
                                        }
                                        else
                                        {
                                        ?>
                                        <h4 style="text-align: center; font-size: 15px;">No Related Email is Available</h4>
                                        <?php 
                                        } 
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="clonecase" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Clone A Case</h4>
            </div>
            <div class="modal-body">
                <form name="cloneCaseForm" id="cloneCaseForm">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Case No <span class="asterisk">*</span></label>
                                <input class="form-control" type="number" id="caseFrom" name="caseFrom" min="0" placeholder="Copy Information from Case" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>From Case Caption</label>
                                <input class="form-control" type="text" id="caseFromCaption" readonly placeholder="From Case Caption" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" name="caseTo" readonly placeholder="Copy Information into Case" value="<?php echo $this->uri->segment(3); ?>" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input class="form-control" type="text" readonly placeholder="To Case Caption" value="<?php echo $display_details->caption1; ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Details of Copy <span class="asterisk">*</span></label>
                        <span class="display-block"><label> <input type="checkbox" name="CopyData[]" id="cParties" value="cParties" class="i-checks"> Copy Parties </label></span>
                        <span class="display-block"><label> <input type="checkbox" name="CopyData[]" id="cInjuries" value="cInjuries" class="i-checks"> Copy Injuries </label></span>
                        <span class="display-block"><label> <input type="checkbox" name="CopyData[]" id="cCaseActivity" value="cCaseActivity" class="i-checks"> Copy Case Activity </label></span>

                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="cloneCaseBtn" href="#" >Proceed</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="protactcase" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Protect Case</h4>
            </div>
            <div class="modal-body">
                <form name="protectCaseForm" id="protectCaseForm">
                    <?php
                    if (empty($display_details->password)) {
                        ?>
                        <p>Create a password to open this case.</p>
                        <p style="color:red">Leave the fields blank to remove password.</p>
                        <?php
                    } else {
                        ?>
                        <p>Enter password to open this case. Leave blank to clear password</p>
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <input type="hidden" name="pro_case_no" id="pro_case_no" value="<?php echo $this->uri->segment(3); ?>" >
                        <input type="password" id="propassword" name="propassword" placeholder="New Password" class="form-control" />
                    </div>
                    <div class="form-group">
                        <input type="password" id="procpassword" name="procpassword" placeholder="Confirm New Password" class="form-control" />
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="protectCasebtn">Proceed</a>
                        <a class="btn btn-md btn-primary btn-danger" id="protectCasecnlbtn">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="deletecase" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form>
                    <p class="text-center">Are you sure to delete this case? All information related to this case will be removed.</p>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" href="#">Proceed</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <video width="100%" height="350" src="" controls autoplay=""></video>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="emailDetailDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Message</h4>
            </div>
            <div class="modal-body">
                <div class="mail-box-header">
                    <div class="mail-tools tooltip-demo">
                        <h3><span class="font-normal">Subject: </span><span id="emailSubject"></span></h3>
                        <h5>
                            <span class="pull-right font-normal" id="emailDatetime"></span>
                            <span class="font-normal" id="sentto"></span>
                            <span class="font-normal">From: </span><span id="emailfromName"></span>
                            <span class="font-normal" id="inboxto"></span>
                        </h5>
                    </div>
                </div>
                <div class="mail-box">
                    <div class="mail-body">
                        <pre id="emailBody"></pre>
                        <div id="file_with_attachment"></div>
                    </div>
                    <input type="hidden" name="filenameVal" id="filenameVal">
                    <input type="hidden" name="reply_iore" id="reply_iore">
                    <div class="clearfix"></div>
                </div>
            </div>
             <div class="modal-footer" id="reply_bttn" style="display:none">
                  <div class="col-sm-12 text-center">
                    <a href="javascript:;" class="btn btn-sm btn-primary reply_compose_email"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Reply">Reply</a>
                    <a href="javascript:;" class="btn btn-sm btn-primary reply_all_compose_email" data-toggle="tooltip" data-placement="bottom" title="Reply All" email_con_id="" >Reply All</a>
                  </div>
             </div>
        </div>
    </div>
</div>


<div id="taskCompletePopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <span>Are you sure to Set Task as Completed?</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <?php $caseNo = $this->uri->segment(3); ?>
                        <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo; ?>">
                        <input id="taskNo" name="taskNo" type="hidden" value="">
                        <button id="complete" type="button" class="btn btn-primary">Complete</button>
                        <button id="completewithnew" type="button" class="btn btn-primary">Complete and Add New</button>
                        <button type="button" id="cncl" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="TodayTaskCompleted" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Task Completed</h4>
            </div>
            <div class="modal-body">
                <form id="edit_case_details" method="post">
                    <div class="panel-body">
                        <div class="float-e-margins">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox-content">
                                        <div class="form-group">
                                            <textarea class="form-control" name="case_event" id="case_event" style="height: 100px;width: 100%;"></textarea>
                                            <input type='hidden' id='caseTaskId' name='caseTaskId'>
                                            <input type='hidden' id='caseno' class='caseNo' name='caseno'>
                                            <input id="taskNo" name="taskNo" type="hidden" value="">
                                            <input type='hidden' id='addnew' class='addnew' value="0">
                                        </div>
                                        <div class="form-group">
                                            <label>Enter any information to add to the  task to be posted to case activity</label>
                                            <textarea class="form-control" name="case_extra_event" id="case_extra_event" style="height: 100px;width: 100%;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class='col-sm-6'>
                    <select id="case_category" name="case_category" class="form-control select2Class">
                        <?php foreach ($caseactcategory as $k => $v) { ?>
                            <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                        <?php } ?>
                        <?php
                        /* $jsonCategory = json_decode(category);
                          foreach ($jsonCategory as $key => $option) {
                          echo '<option value=' . $key . '>' . $option . '</option>';
                          } */
                        ?>

                    </select>
                </div>
                <div class='col-sm-6'>
                    <div class="form-btn text-right">
                        <button type="submit" id="saveCompletedTask1" class="btn btn-primary btn-md">Complete</button>
                        <button type="submit" id="saveCompletedTask1" class="btn btn-primary btn-md">Complete and Add New</button>
                        <button type="button" class="btn btn-primary btn-md btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="social_security" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Related case for social security</h4>
            </div>
            <form method="post" name="parties_case_specific" id="parties_case_specific">
                <div class="modal-body">
                    <div class="row">
                        <div class="table-responsive">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <table id="related_case_social" class="table table-bordered table-hover table-striped dataTables-example related_case_social" style="width: 100%">
                                        <thead>
                                        <th><input name="select_all" value="1" id="someCheckbox-all" type="checkbox" />Select All
                                        </th>
                                        <th>Name</th>
                                        <th>Case No</th>

                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-md btn-primary" id="addsocialsecurity" >Process</button>
                        <button type="button" data-dismiss="modal" class="btn btn-md btn-danger">Cancel</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="addtask" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body">
                <form id="addTaskForm" name="addTaskForm" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <input type="hidden" name="caseno" id="caseNo" class="caseNo" value="<?php echo $caseNo; ?>"/>
                                        <label class="col-sm-4 no-padding">From</label>
                                        <div class="col-sm-8 no-padding"><select class="form-control select2Class" name="whofrom" id="whofrom">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected="selected"' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-2 no-padding">To</label>
                                        <div class="col-sm-10 no-padding"><select class="form-control select2Class staffTO staffvacation" name="whoto2" onchange="return remove_error('whoto2')" id="whoto2">
                                               <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected="selected"' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Finish By<span class="asterisk">*</span></label>
                                        <div class="col-sm-9 no-padding"><input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" onchange="return remove_error('taskdatepickern')" id="taskdatepickern" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Color</label>
                                        <div class="col-sm-8 no-padding"><select class="form-control select2Class" name="color" id="color">
                                                <?php
                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                foreach ($jsoncolor_codes as $k => $color) {
                                                    ?>
                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Task Type</label>
                                        <div class="col-sm-8 no-padding"><select class="form-control select2Class" name="typetask">
                                                <option value="" selected="selected">Select Task Type</option>
                                                <option value=""></option>
                                                <option value="follow up">Follow Up</option>
                                                <option value="statute">Statute</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-padding">Priority</label>
                                        <div class="col-sm-9 no-padding"><select class="form-control select2Class" name="priority" id="priority">
                                                <option value="3">Low</option>
                                                <option value="2">Medium</option>
                                                <option value="1">High</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6">
                                     <div class="form-group">
                                         <label class="col-sm-3 no-left-padding">Phase</label>
                                         <div class="col-sm-9 no-padding"><select class="form-control select2Class" name="phase">
                                                 <option value="">Select Phase</option>
                                                 <option value=""></option>
                                                 <option value="begining">Beginning</option>
                                                 <option value="middle">Middle</option>
                                                 <option value="final">Final</option>
                                             </select>
                                         </div>
                                     </div>
                                 </div>-->
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task <span class="asterisk">*</span></label>
                                        <textarea class="form-control" name="event" style="height: 150px;resize: none;"></textarea>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <!--label class="">Reminder&nbsp;&nbsp;<input type="checkbox" class="i-checks" name="reminder" ></label-->
                                        <label class="container-checkbox pull-left">Reminder
                                            <input type="checkbox" class="treminder" name="reminder" onchange="showhidedt();">
                                            <span class="checkmark"></span> <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="You will get notified on your screen prior to 10 minutes of your scheduled task. To enable calender reminder notification in your browser to receive all notifications, allow notification in site settings." id="taskreminder"></i> &nbsp;&nbsp;<span class="rdateerror"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group">
                                        <label class="col-sm-3">Date</label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" name="rdate" class="rdate form-control" data-mask="99/99/9999" placeholder class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group">
                                        <label class="col-sm-3">Time</label>
                                        <div class="col-sm-9 no-padding uk-autocomplete">
                                            <input type="text" name="rtime" class="rtime form-control" class="form-control" data-uk-timepicker="{format:'12h'}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <!--<button type="button" class="btn btn-md btn-primary pull-left">Save to Group</button>-->
                                        <button type="button" class="btn btn-md btn-primary" id="addTask" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                        <label class="container-checkbox pull-right marg-top5">&nbsp;Email
                                            <input type="checkbox" class="" name="notify" >
                                            <span class="checkmark"  style="padding-right: 5px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') . '/css/plugins/jquery-slider/jquery.radiant_scroller.css'; ?>" media="all">
<script src="<?php echo base_url('assets') . '/js/plugins/jquery-slider/jquery.radiant_scroller.js'; ?>"></script>
<?php
$url_segment = $this->uri->segment(2);
if ($url_segment == 'case_details_new' || $url_segment == 'case_details_New') {
?>
<input type="hidden" name="module" id="module" value="email_new">
<?php }else if($url_segment == 'case_details'){ ?>
<input type="hidden" name="module" id="module" value="email">
<?php } ?>
<script type="text/javascript">

                                                $("#addtask.modal").on("hidden.bs.modal", function () {
                                                    $("#addtask h4.modal-title").text("Add a Task");

                                                    $("#addTaskForm")[0].reset();

                                                    $("select[name=typetask]").select2('val', 'Task Type');
                                                    $("select[name=typetask]").val('').trigger("change");

                                                    $("select[name=phase]").select2('val', 'Select Phase');
                                                    $("select[name=phase]").val('').trigger("change");

                                                    $("select[name=phase]").val('').trigger("change");

                                                    $(".select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");

                                                    $("input[name=taskid]").val(0);
                                                    $('#addtask a[href="#name"]').tab("show");
                                                });

                                                $('#addtask.modal').on('show.bs.modal', function () {
                                                        var taskid = $("input[name=taskid]").val();

                                                        if(taskid == 0) {
                                                                $(".select2Class#whofrom").val("<?php echo $this->session->userdata('user_data')['initials']; ?>").trigger("change");
                                                                $(".select2Class#whoto2").val("<?php echo $this->session->userdata('user_data')['initials']; ?>").trigger("change");
                                                                $(".select2Class#priority").val("2").trigger("change");
                                                                $(".select2Class#color").val("1").trigger("change");
                                                        }
                                                        $('.rmdt').css('display','none');


                                                });	


                                                var searchBy = {
                                                    'caseno': $('#caseNo').val()
                                                };
                                                var searchBycat = {
                                                    'caseno': $('#caseNo').val()
                                                };
                                                var caseactTable = '';
                                                var caseDetailCalendarTable = '';
                                                $('.edit-party').hide();
                                                var rcs = '';
                                                var table = '';
                                                var injurytable = "";
                                                var partytable = "";
                                                var releteddataTable = "";
                                                var relatedEmailDatatable = "";
                                                var formdatalist = "";
                                                var myDropzone = "";
                                                var Prospect_data = JSON.parse('<?php echo json_encode($Prospect_data); ?>');
                                                var start = 0;
                                                var length = <?php echo DEFAULT_LISTING_LENGTH; ?>;
                                                var total = <?php echo $count; ?>;
                                                var current_list = 'inbox';
                                                var clickFlag = false;
                                                function setTaskValidation() {
                                                    var a = $("#addTaskForm").validate({
                                                        ignore: [],
                                                        rules: {
                                                            whofrom: {
                                                                required: true
                                                            },
                                                            whoto2: {
                                                                required: true
                                                            },
                                                            datereq: {
                                                                required: true,
                                                                greaterThan: ".rdate"
                                                            },
                                                            event: {
                                                                required: true,
                                                                noBlankSpace: true
                                                            }
                                                        }
                                                    });
                                                    return a;
                                                }

                                                function showhidedt() {
                                                    if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
                                                        $('input[type="checkbox"][name="reminder"]').val("on");
                                                        $(".rmdt").show();
                                                    } else {
                                                        $(".rmdt").hide();
                                                        $(".rdate").val("");
                                                        $(".rtime").val("");
														$('.rdateerror').text('');
                                                    }
                                                }
												function editTask(a) {
													    $.blockUI();
                                                        $.ajax({
                                                            url: "<?php echo base_url(); ?>tasks/getTask",
                                                            data: {
                                                                taskId: a
                                                            },
                                                            method: "POST",
                                                            success: function (b) {
                                                                var c = $.parseJSON(b);
                                                                $.unblockUI();
                                                                if (c.status == 0) {
                                                                    swal("Oops...", c.message, "error");
                                                                } else {
                                                                    $("#addtask").modal({
                                                                        backdrop: "static",
                                                                        keyboard: false
                                                                    });
                                                                    $("#addtask").modal("show");
                                                                    $("#addtask .modal-title").html("Edit Task");
                                                                    $("input[name=taskid]").val(a);
                                                                    $("select[name=whofrom]").val(c.whofrom);
                                                                    $("select[name=whoto2]").val(c.whoto);
                                                                    $("input[name=datereq]").val(c.datereq);
                                                                    $("textarea[name=event]").val(c.event);
                                                                    $("select[name=color]").val(c.color);
                                                                    $("select[name=priority]").val(c.priority).trigger("change");
                                                                    $("select[name=typetask]").val(c.typetask);
                                                                    $("select[name=phase]").val(c.phase);
																	
																	if (c.notify == 1) {
                                                                        $("input[name=notify]").iCheck("check");
                                                                    } else {
                                                                        $("input[name=notify]").iCheck("uncheck");
                                                                    }
                                                                    if (c.reminder == 1) {
                                                                        $('input[type="checkbox"][name="reminder"]').prop("checked", true).change();
                                                                        $("input[name=rdate]").val(c.popupdate);
                                                                        $("input[name=rtime]").val(c.popuptime);
                                                                        $(".rmdt").show();
                                                                    } else {
                                                                        $('input[type="checkbox"][name="reminder"]').prop("checked", false).change();
                                                                        $(".rmdt").hide();
                                                                    }
                                                                    
                                                                    $("#addTaskForm select").trigger("change");
                                                                }
                                                            }
                                                        });
                                                    }
												
												var casetasksTable ='';
                                                $(document).ready(function () {
													var casetasksTable ='';
													var caseno = $("#caseNo").val();
                                                    setTaskValidation();
													
													        casetasksTable = $('.case_tasks').DataTable({
																processing: true,
																serverSide: true,
																stateSave: true,
																stateSaveParams: function (settings, data) {
																	data.search.search = "";
																	data.start = 0;
																	delete data.order;
																	data.order = [0, "ASC"];
																},
																scrollX: true,
																scrollY: "330px",
																scrollCollapse: true,
																bFilter: true,
																order: [
																	[0, "ASC"]
																],
																destroy: true,
																pageLength: 5,
																searchable: true,
																lengthMenu: [5, 10, 20, 50, 100],
																ajax: {
																	url: "<?php echo base_url(); ?>cases/getcaseTask",
																	method: "POST",
																	dataType: 'json',
																	async: true,
																	data: {
																		caseno: $("#caseNo").val()
																	},
																},
													            fnRowCallback: function (nRow, aData) {
																	if (aData[aData.length - 2] != "") {
																		var b = aData[aData.length - 1].split("-");
																		if (b[0] != "") {
																			$(nRow).css({
																				"background-color": b[0],
																				color: b[1]
																			});
																		} else {
																			$(nRow).css({
																				"background-color": "#ffffff",
																				color: "#000000"
																			});
																		}
																	}
																},
													
																columnDefs: [{
																		"width": "7%",
																		"targets": 0
																	},
																	{
																		"width": "3%",
																		"targets": 1
																	},
																	{
																		"width": "80%",
																		"targets": 2
																	},
																	{
																		"width": "10%",
																		"textalign": "center",
																		"targets": 3,
																		"bSortable": false,
																		"bSearchable": false

																	}

																]
															});

													$(document.body).on("click", ".deletetask", function () {
														var a = $(this).attr("data-taskId");
														
														swal({
                                                            title: "Alert",
                                                            text: "Are you sure to Remove this Task? ",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, delete it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            $.blockUI();
                                                            $.ajax({
                                                                url: "<?php echo base_url(); ?>tasks/deleteRec",
                                                                data: {
                                                                    taskId: a
                                                                },
                                                                method: "POST",
                                                                success: function (b) {
                                                                    $.unblockUI();

                                                                    var c = $.parseJSON(b);
                                                                    if (c.status == 1) {
																		swal("Success !", c.message, "success");
																		casetasksTable.draw();
                                                                    } else {
                                                                        swal("Oops...", c.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        });
                                                
													});		
													
                                                    $('#addTask').on('click', function () {
													if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
														if($('.rdate').val()=='' || $('.rtime').val()=='')
														{
															$('.rdateerror').text('Please select Reminder date and time');
															return false;
														}
													}
                                                        if ($('#addTaskForm').valid()) {
                                                            $.blockUI();
                                                            var url = '';
                                                            if ($('input[name=taskid]').val() != '' && $('input[name=taskid]').val() != 0) {
                                                                url = '<?php echo base_url(); ?>tasks/editTask';
                                                            } else {
                                                                url = '<?php echo base_url(); ?>tasks/addTask';
                                                            }

                                                            $.ajax({
                                                                url: url,
                                                                method: "POST",
                                                                data: $('#addTaskForm').serialize(),
                                                                success: function (result) {
                                                                    var data = $.parseJSON(result);
                                                                    $.unblockUI();
                                                                    if (data.status == '1') {
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: data.message,
                                                                            type: "success"
                                                                        },
                                                                                function ()
                                                                                {
                                                                                    $('#addTaskForm')[0].reset();

                                                                                    $("select[name=typetask]").select2('val', 'Task Type');
                                                                                    $("select[name=typetask]").val('').trigger("change");

                                                                                    $("select[name=phase]").select2('val', 'Select Phase');
                                                                                    $("select[name=phase]").val('').trigger("change");

                                                                                    $(".select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");

                                                                                    $('#addtask').modal('hide');
                                                                                    $('#addtask .modal-title').html('Add a Task');
																					$('.rtime').val('');
																					$('.rmdt').css('display','none');
                                                                                }

                                                                        );
                                                                        casetasksTable.draw();
                                                                        $('.whoto').removeClass("active");
                                                                        $('.whofrom').addClass("active");
                                                                    } else {
                                                                        swal("Oops...", data.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
													
                                                    $(".rdate").datetimepicker({
                                                        format: "mm/dd/yyyy",
                                                        minView: 2,
                                                        autoclose: true,
                                                        startDate: new Date()
                                                    });

                                                    jQuery.validator.addMethod("greaterThan", function (c, b, d) {
                                                        if ($(d).val() != "") {
                                                            if (!/Invalid|NaN/.test(new Date(c))) {
                                                                return new Date(c) >= new Date($(d).val());
                                                            }
                                                            return isNaN(c) && isNaN($(d).val()) || (Number(c) >= Number($(d).val()));
                                                        } else {
                                                            return true;
                                                        }
                                                    }, "Must be greater than Reminder Date.");


                                                    $("#taskdatepickern").datepicker({
                                                        dateformat: "mm/dd/yyyy",
                                                        showOn: "button",
                                                        minDate: 0,
                                                        buttonImage: false,
                                                        buttonText: '<i class="fa fa-calendar" aria-hidden="true"></i>',
                                                        buttonImageOnly: false
                                                    });


                                                    $('#taskdatepickern').on('change', function () {
                                                        var today = new Date();
                                                        var ad = $(this).val();
                                                        if (isValidDate(ad)) {
                                                            $('#taskdatepickern').val(moment(ad).format('MM/DD/YYYY'));
                                                        } else {
                                                            var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                                                            $('#taskdatepickern').val(moment(new_date).format('MM/DD/YYYY'));
                                                            $('.weekendsalert').trigger("change");
                                                        }

                                                    });

                                                    var birthdaynotification = '<?php echo isset($birthdaynotification) ? $birthdaynotification : 0; ?>';
                                                    var caseno = $("#caseNo").val();
                                                    setImagePagination();
                                                    setVideoPagination();
                                                    setCaseCloneValidation();
                                                    
                                                    $.ajax({
                                                        url: '<?php echo base_url('cases/getRedAlertActivity'); ?>',
                                                        dataType: 'json',
                                                        type: 'POST',
                                                        async: false,
                                                        data: {
                                                            caseno: caseno
                                                        },
                                                        beforeSend: function () {},
                                                        complete: function () {
                                                            $.unblockUI();
                                                        },
                                                        success: function (data) {
                                                            $("#redalertAct").css("display", "block");
                                                            $("#contentData").html(data);
                                                            console.log(data);
                                                        }
                                                    });
                                                    $('.case-details a[data-toggle="tab"]').on('click', function (e) {
                                                        if ($(this).attr('href') == '#caseact') {
                                                            if (caseactTable != "") {
                                                                caseactTable.draw();
                                                            } else {
                                                                var category = $('#Category').val();
                                                                var checkpost = $('#all_category').val();
                                                                if (checkpost == 'on')
                                                                {
                                                                    var checkpost = 1;
                                                                } else
                                                                {
                                                                    var checkpost = '';
                                                                }

                                                                caseactTable = $('#case_activity_list').DataTable({
                                                                    "retrieve": true,
                                                                    stateSave: true,
                                                                    processing:true,
                                                                    stateSaveParams: function (settings, data) {
                                                                            data.search.search = "";
                                                                            data.start = 0;
                                                                            delete data.order;
                                                                            data.order = [0, "desc"];
                                                                    },
                                                                    "serverSide": true,
                                                                    "autoWidth": true,
                                                                    "scrollX": true,
                                                                    scrollY: "470px",
                                                                    "order": [
                                                                        [0, "desc"]
                                                                    ],
                                                                    "pageLength": 100,
                                                                    "columnDefs": [{
                                                                            "width": "10%",
                                                                            "targets": 0
                                                                        },
                                                                        {
                                                                            "width": "5%",
                                                                            "targets": 1
                                                                        },
                                                                        {
                                                                            "width": "50%",
                                                                            "targets": 2
                                                                        },
                                                                        {
                                                                            "width": "4%",
                                                                            "targets": 3
                                                                        },
                                                                        {
                                                                            "width": "4%",
                                                                            "targets": 4,
                                                                        },
                                                                        {
                                                                            "width": "27%",
                                                                            "targets": 5
                                                                        }

                                                                    ],
                                                                    lengthMenu: [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
                                                                    "aoColumnDefs": [{
                                                                            "bSortable": false,
                                                                            "aTargets": [5]
                                                                        },
                                                                        {
                                                                            "bSearchable": false,
                                                                            "aTargets": [5]
                                                                        },
                                                                        {
                                                                            "bSortable": false,
                                                                            "aTargets": [4]
                                                                        },
                                                                        {
                                                                            "bSearchable": false,
                                                                            "aTargets": [4]
                                                                        },
                                                                        {
                                                                            className: "event_details",
                                                                            "aTargets": [1]
                                                                        }
                                                                    ],
                                                                    "ajax": {
                                                                        url: "<?php echo base_url(); ?>cases/getAllActivity",
                                                                        type: "POST",
                                                                        data: {
                                                                            'caseId': $("#caseNo").val(),
                                                                            'chackvalue': parseInt(checkpost),
                                                                            'category': parseInt(category)

                                                                        }
                                                                    },
                                                                    "fnCreatedRow": function (nRow, aData, iDataIndex) {

                                                                        $(nRow).attr('id', 'caseAct' + aData[6]);
                                                                        if (aData[7] != 0) {
                                                                            $(nRow).css({
                                                                                'background-color': aData[7],
                                                                                'color': aData[8]
                                                                            });
                                                                        }
                                                                    },
                                                                    "drawCallback": function (settings) {
                                                                        $('div.dataTables_scrollBody').scrollTop(0);
                                                                    }
                                                                });
                                                            }
                                                        } else if ($(this).attr('href') == '#caseEmail') {
                                                            var caseno = $("#caseNo").val();
                                                            var chkmodule = $('#module').val();
                                                            if(chkmodule == 'email_new'){
                                                                    var newUrl = '<?php echo base_url('email_new/case_email_listing'); ?>';
                                                                }else{
                                                                    var newUrl = '<?php echo base_url('email/case_email_listing'); ?>'
                                                                }
                                                            $.ajax({
                                                                url: newUrl,
                                                                dataType: 'json',
                                                                type: 'POST',
                                                                data: {
                                                                    caseno: caseno,
                                                                },
                                                                beforeSend: function () {
                                                                    $.blockUI();
                                                                },
                                                                complete: function () {
                                                                    $.unblockUI();
                                                                },
                                                                success: function (data) {
                                                                    var listing = "";
                                                                    $.each(data.result, function (index, item) {
                                                                        listing += generate_tr_html(item);
                                                                    });
                                                                    $('.mail-box tbody').html('');
                                                                    $('.mail-box tbody').html(listing);
                                                                    if (relatedEmailDatatable != "") {
                                                                        relatedEmailDatatable.draw();
                                                                    } else {
                                                                        relatedEmailDatatable = $('#related_email').DataTable({
                                                                            "order": [
                                                                                [2, "desc"]
                                                                            ],
                                                                            stateSave: true,
                                                                            serverSide: false,
    																		stateSaveParams: function (settings, data) {
    																			data.search.search = "";
    																			data.start = 0;
    																			delete data.order;
    																			data.order = [2, "desc"];
    																		},
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        } else if ($(this).attr('href') == '#parties') {

                                                            if (partytable != "") {
                                                                partytable.draw();
                                                            } else {
                                                                partytable = $('#party-table-data').DataTable({
                                                                    rowReorder: {
                                                                        update: false
                                                                    },
                                                                    processing: true,
                                                                    stateSave: true,
                                                                    stateSaveParams: function (settings, data) {
                                                                            data.search.search = "";
                                                                            data.start = 0;
                                                                            delete data.order;
                                                                    },
                                                                    retrieve: true,
                                                                    serverSide: true,
                                                                    bFilter: true,
                                                                    scrollX: true,
                                                                    bSortable: true,
                                                                    searching : true,
                                                                    aaSorting: [],
                                                                    aoColumnDefs: [{
                                                                            bSortable: false,
                                                                            aTargets: [0,3]
                                                                        },
                                                                        {
                                                                            bSearchable: false,
                                                                            aTargets: [0,3]
                                                                        }
                                                                    ],
                                                                    ajax: {
                                                                        url: "<?php echo base_url(); ?>cases/getCaseParties",
                                                                        type: "POST",
                                                                        data: {'caseId': $("#caseNo").val()},
                                                                        complete: function () {
                                                                            if($("#party-table-data tbody tr").length == 1 && $("#party-table-data tbody tr").eq(0).find('td').length == 1) {
                                                                            setPartyViewDataEmpty();
                                                                            }else{ 
                                                                                $("#party-table-data tbody tr").eq(0).find('a.btn.view_party').trigger("click");
                                                                            }
                                                                        }
                                                                    },
                                                                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                                                                        $(nRow).attr('id', 'id_' + aData[4]);
                                                                        $(nRow).addClass('view_party_full_click').css("cursor", "pointer");
                                                                    }
                                                                });
                                                                partytable.on('row-reorder', function (e, diff, edit) {
                                                                    if(diff.length > 0){
                                                                        var ids1 = new Array();
                                                                        var ids3 = new Array();
                                                                        for (var i = 0, ien = diff.length; i < ien; i++) {
                                                                            var rowData = partytable.row(diff[i].node).data();
                                                                            var ids = new Array();
                                                                            var ids2 = new Array();
                                                                            if (diff[i].newData == 1) {
                                                                                var newid = diff[i].newData;
                                                                                var casecard_no = $(rowData[3]).attr('data-casecard_no');
                                                                                var card_no = $(rowData[3]).attr('data-card');
                                                                                setTimeout(function () {
                                                                                    swal({
                                                                                        title: "Warning!",
                                                                                        text: "You are changing the name at the top of the list.\n The client should always be at the top of the list.\n Are you sure you want continue anyways?",
                                                                                        type: "warning",
                                                                                        showCancelButton: true,
                                                                                        confirmButtonColor: "#DD6B55",
                                                                                        confirmButtonText: "Yes",
                                                                                        closeOnConfirm: false
                                                                                    }, function (isConfirm) {
                                                                                        if (isConfirm) {
                                                                                            ids2.push(newid);
                                                                                            ids2.push(casecard_no);
                                                                                            ids2.push(card_no);
                                                                                            ids3.push(ids2);
                                                                                            $.ajax({
                                                                                                url: '<?php echo base_url(); ?>cases/updateorderno',
                                                                                                type: "POST",
                                                                                                data: {
                                                                                                    position: ids3,
                                                                                                    caseno: $('#hdnCaseId').val()
                                                                                                },
                                                                                                success: function (data) {
                                                                                                    if (data == 1) {
                                                                                                        swal("Success !", "Parties are successfully reordered", "success");
                                                                                                        $('.case-details a[href="#parties"]').trigger('click');
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                            swal.close();
                                                                                        }
                                                                                    });
                                                                                }, 800);
                                                                            } else {
                                                                                ids.push(diff[i].newData);
                                                                                ids.push($(rowData[3]).attr('data-casecard_no'));
                                                                                ids.push($(rowData[3]).attr('data-card'));
                                                                                ids1.push(ids);
                                                                            }
                                                                        }
                                                                        if (ids1.length > 0) {
                                                                            $.ajax({
                                                                                url: '<?php echo base_url(); ?>cases/updateorderno',
                                                                                type: "POST",
                                                                                data: {
                                                                                    position: ids1,
                                                                                    caseno: $('#hdnCaseId').val()
                                                                                },
                                                                                success: function (data) {
                                                                                    $('.case-details a[href="#parties"]').trigger('click');
                                                                                    if (data > 0) {
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                    } 
                                                                });
                                                            }
                                                        } else if ($(this).attr('href') == '#relcases') {
                                                            if (releteddataTable != "") {
                                                                releteddataTable.draw();
                                                            } else {
                                                                releteddataTable = $('#related_case_table').DataTable({
                                                                    serverSide: true,
                                                                    processing: true,
                                                                    scrollX: true,
                                                                    stateSave: true,
																	stateSaveParams: function (settings, data) {
																		data.search.search = "";
																		data.start = 0;
																		delete data.order;
																		data.order = [0, "desc"];
																	},
                                                                    scrollY: '470px',
                                                                    columns: [
                                                                        {width: "11.5%"},
                                                                        {width: "11.2%"},
                                                                        {width: "11.5%"},
                                                                        {width: "9.5%"},
                                                                        {width: "42.8%"},
                                                                        {width: "9.5%"}
                                                                    ],
                                                                    order: [
                                                                        [0, "desc"]
                                                                    ],
                                                                    ajax: {
                                                                        url: "<?php echo base_url(); ?>cases/getReletedDataAjax",
                                                                        data: {
                                                                            'caseNo': $("#caseNo").val()
                                                                        },
                                                                        type: "POST"
                                                                    },
                                                                    fnDrawCallback: function (oSettings) {
                                                                        $("#count_related").html('(' + oSettings._iRecordsTotal + ')');
                                                                    },
                                                                    columnDefs: [{
                                                                            render: function (data, type, row) {
                                                                                return '<button class="btn btn-danger btn-circle-action btn-circle" type="button" onClick="deleteRelatedcase(' + data + ')"><i class="fa fa-trash"></i></button>';
                                                                            },
                                                                            targets: 5,
                                                                            bSortable: false,
                                                                            bSearchable: false
                                                                        },
                                                                        {
                                                                            render: function (data, type, row) {
                                                                                return '<a href="javascript:void(0)" id="' + row[0] + '" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-popover-content="#data">' + data + '</a>';
                                                                            },
                                                                            targets: 0
                                                                        }
                                                                    ]
                                                                });
                                                            }
                                                        }
                                                    });
                                                    rcs = $('#related_case_social').DataTable();
                                                    CKEDITOR.replace('mailbody');
                                                    $('#change_part_details').change(function () {
                                                        $('.party_showhide').hide();
                                                        $('.' + $(this).val()).show();
                                                    });
                                                    $('#change_part_details').change(function () {
                                                        $('.party_showhide').hide();
                                                        $('.' + $(this).val()).show();
                                                    });
                                                    $('#uploadBtn').change(function () {
                                                        var files = $("#uploadBtn")[0].files;
                                                        var form_data = new FormData();
                                                        var ins = $('input#uploadBtn')[0].files.length;
                                                        var imageFlag = false;
                                                        var fsize = 0;
                                                        for (var x = 0; x < ins; x++) {
                                                            var ext = $('#uploadBtn')[0].files[x].name.split('.').pop().toLowerCase();
                                                            fsize = fsize + ($('#uploadBtn')[0].files[x].size);
                                                            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                                                            } else {
                                                                imageFlag = true;
                                                                form_data.append("files_" + x, $('#uploadBtn')[0].files[x]);
                                                            }
                                                        }

                                                        form_data.append('caseno', $('#hdnCaseId').val());
                                                        if (imageFlag == true && fsize <= 20000000) {
                                                            document.getElementById('myProgress1').style.display = 'block';
                                                            var elem = document.getElementById("myBar1");
                                                            var width = 10;
                                                            var id = setInterval(frame, 100);

                                                            function frame() {
                                                                if (width >= 100) {
                                                                    $.ajax({
                                                                        url: '<?php echo base_url(); ?>cases/UploadPhoto',
                                                                        type: "POST",
                                                                        data: form_data,
                                                                        contentType: false,
                                                                        cache: false,
                                                                        processData: false,
                                                                        success: function (data) {
                                                                            var res = $.parseJSON(data);
                                                                            if (res.status == 1) {
                                                                                var image_uploded = '';
                                                                                for (var x = 0; x < res.file.length; x++) {
                                                                                    var link = '<?php echo base_url() ?>' + 'assets/casePhotoVideo/' + $('#caseNo').val() + '/photo/' + res.file[x];
                                                                                    var dp_link = '<?php echo DOCUMENTPATH ?>' + 'casePhotoVideo/' + $('#case_no').val() + '/photo/' + res.file[x];
                                                                                    image_uploded += '<div class="imgGallery"><a class="example-image-link scroll-el" data-lightbox="example-1" href="' + link + '" title="Image from <?php echo APPLICATION_NAME; ?>" data-gallery=""><img class="example-image" src="' + link + '"width="100px" height="100px"></a><i class="fa fa-close" id="del_photo" data-id="' + dp_link + '"> </i></div>';
                                                                                }
                                                                                swal("Success !", res.message, "success");
                                                                                $("#myBar1").text("");
                                                                                $("#myBar1").removeAttr("style");
                                                                                document.getElementById('myProgress1').style.display = 'none';
                                                                                $('.image-row.lightBoxGallery').append(image_uploded);
                                                                                setImagePagination();
                                                                            }
                                                                        }
                                                                    });
                                                                    window.clearInterval(id);
                                                                } else {
                                                                    width++;
                                                                    elem.style.width = width + '%';
                                                                    elem.innerHTML = width * 1 + '%';
                                                                }
                                                            }
                                                        } else {
                                                            if (fsize > 20000000) {
                                                                swal("Oops...", "Max upload size is 20MB");
                                                            } else {
                                                                swal("Oops...", "Please enter the valid extension('png','jpg','jpeg')");
                                                            }
                                                        }
                                                    });

                                                    $('#uploadBtn1').change(function () {
                                                        var files = $("#uploadBtn1")[0].files;
                                                        var form_data = new FormData();
                                                        var ins = $('input#uploadBtn1')[0].files.length;
                                                        var image_uploded = '';
                                                        var valid_file = true;
                                                        var cnt = 0;
                                                        var fsize = 0;
                                                        for (var x = 0; x < ins; x++) {
                                                            var valid_extension = /(\.mp4)$/i;
                                                            var fld_value = $('#uploadBtn1')[0].files[x].name;
                                                            fsize = fsize + ($('#uploadBtn1')[0].files[x].size);
                                                            form_data.append('caseno', $('#hdnCaseId').val());
                                                            if (!valid_extension.test(fld_value)) {
                                                                swal({
                                                                    title: "Alert",
                                                                    text: "Oops! Please provide valid video format.\n" + fld_value + ' is not valid video format',
                                                                    type: "warning"
                                                                });
                                                            } else {
                                                                form_data.append("files_" + x, $('#uploadBtn1')[0].files[x]);
                                                                cnt++;
                                                            }
                                                        }

                                                        if ((fsize) <= 20000000) {
                                                            if (cnt > 0) {
                                                                document.getElementById('myProgress').style.display = 'block';
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/Uploadvideo',
                                                                    type: "POST",
                                                                    data: form_data,
                                                                    contentType: false,
                                                                    cache: false,
                                                                    processData: false,
                                                                    beforeSend: function (xhr) {},
                                                                    complete: function (jqXHR, textStatus) {},
                                                                    success: function (data) {
                                                                        var elem = document.getElementById("myBar");
                                                                        var width = 10;
                                                                        var id = setInterval(frame, 10);

                                                                        function frame() {
                                                                            if (width >= 100) {
                                                                                var res = $.parseJSON(data);
                                                                                if (res.status == 1) {
                                                                                    var video_uploded = '';
                                                                                    for (var x = 0; x < res.file.length; x++) {
                                                                                        var link = '<?php echo base_url() ?>' + 'assets/casePhotoVideo/' + $('#caseNo').val() + '/video/' + res.file[x];
                                                                                        var delink = '<?php echo DOCUMENTPATH; ?>casePhotoVideo/' + $('#caseNo').val() + '/video/' + res.file[x];
                                                                                        image_uploded += '<div class="col-md-6 videoset" style="margin-bottom: 10px;"><a class="case_video" data-video="' + link + '" title="Image from Unsplash" data-toggle="modal" data-target="#videoModal" > <video controls><source src="' + link + '" /></video></a><i class="fa fa-close" id="del_video" data-id="' + delink + '"> </i> </div>';
                                                                                    }
                                                                                    swal({
                                                                                        title: "Success!",
                                                                                        text: res.message,
                                                                                        type: "success"
                                                                                    });
                                                                                    $("#myBar").text("");
                                                                                    $("#myBar").removeAttr("style");
                                                                                    document.getElementById('myProgress').style.display = 'none';
                                                                                    $('.video').append(image_uploded);
                                                                                    setVideoPagination();
                                                                                }
                                                                                window.clearInterval(id);
                                                                            } else {
                                                                                width++;
                                                                                elem.style.width = width + '%';
                                                                                elem.innerHTML = width * 1 + '%';
                                                                            }
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            ;
                                                        } else {
                                                            if (fsize > 20000000) {
                                                                swal("Oops...", "Max upload size is 20MB");
                                                            }
                                                        }
                                                    });
                                                    $('#uploadinjDoc').change(function () {
                                                        var files = $("#uploadinjDoc")[0].files;
                                                        var form_data = new FormData();
                                                        var ins = $('input#uploadinjDoc')[0].files.length;
                                                        var image_uploded = '';
                                                        var valid_file = true;
                                                        var cnt = 0;
                                                        var Cno = $('#hdnCaseId').val();
                                                        for (var x = 0; x < ins; x++) {
                                                            var valid_extension = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.gif|\.png|\.txt|\.xlsx|\.xls|\.rtf|\.html|\.htm|\.wpd|\.wps|\.csv|\.ppt|\.pptx|\.zip|\.tar|\.xml|\.XLR)$/i;
                                                            ;
                                                            var fld_value = $('#uploadinjDoc')[0].files[x].name;
                                                            form_data.append('caseno', $('#hdnCaseId').val());
                                                            if (!valid_extension.test(fld_value)) {
                                                                swal({
                                                                    title: "Alert",
                                                                    text: "Oops! Please provide valid file format.\n" + fld_value + ' is not valid file format',
                                                                    type: "warning"
                                                                });
                                                            } else {
                                                                form_data.append("files_" + x, $('#uploadinjDoc')[0].files[x]);
                                                                cnt++;
                                                            }
                                                        }
                                                        if (cnt > 0) {
                                                            document.getElementById('myProgress3').style.display = 'block';
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/UploadinjDocs',
                                                                type: "POST",
                                                                data: form_data,
                                                                contentType: false,
                                                                cache: false,
                                                                processData: false,
                                                                beforeSend: function (xhr) {},
                                                                complete: function (jqXHR, textStatus) {},
                                                                success: function (data) {
                                                                    var elem = document.getElementById("myBar3");
                                                                    var width = 10;
                                                                    var id = setInterval(frame, 10);

                                                                    function frame() {
                                                                        if (width >= 100) {
                                                                            var res = $.parseJSON(data);
                                                                            if (res.status == 1) {
                                                                                var injdoc_uploded = '';
                                                                                var newDiv = '';
                                                                                swal({
                                                                                    title: "Success!",
                                                                                    text: res.message,
                                                                                    type: "success"
                                                                                });
                                                                                newDiv += '<div> <a class="case_video" href="<?php echo base_url() ?>assets/clients/' + Cno + '/' + res.file + '">' + res.file + '</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-close" id="del_injDoc" data-id="<?php echo DOCUMENTPATH ?>clients/' + Cno + '/' + res.file + '"> </i> </div>';
                                                                                $('.injDocs').append(newDiv);
                                                                                $("#myBar3").text("");
                                                                                $("#myBar3").removeAttr("style");
                                                                                document.getElementById('myProgress3').style.display = 'none';
                                                                            }
                                                                            window.clearInterval(id);
                                                                        } else {
                                                                            width++;
                                                                            elem.style.width = width + '%';
                                                                            elem.innerHTML = width * 1 + '%';
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        ;
                                                    });
                                                    $(document.body).on("click", ".edit-card", function () {
                                                        var cardCode = $(this).data('cardcode');
                                                        $('#caseCardCode').val($(this).data('cardcode'));
                                                        if(cardCode){
                                                        $.ajax({
                                                            url: "<?php echo base_url() . "rolodex/checkCase" ?>",
                                                            method: "POST",
                                                            data: {'cardcode': cardCode},
                                                            success: function (result) {
                                                                if (result > 1) {
                                                                    $("#partiesNameChangeWarning").modal("show");
                                                                } else
                                                                {
                                                                    ChangeRolodex();
                                                                }
                                                            }
                                                        });
                                                        }else{
                                                            swal({
                                                                title: "Alert",
                                                                text: "There are no Clients attached to this case.",
                                                                type: "warning"
                                                            });
                                                        }
                                                    });
                                                    
                                                    $('a[data-target="#editcasedetail"]').click(function () {
                                                        editCaseDetails($('#hdnCaseId').val(), $(this).data('tab'));
                                                    });
                                                    $('#data_5 .input-daterange').datetimepicker({
                                                        keyboardNavigation: false,
                                                        forceParse: false,
                                                        autoclose: true
                                                    });
                                                    $(document.body).on('click', '.view_party', function () {
                                                        var cardcode = $(this).data('card');
                                                        var caseno = $(this).data('case');
                                                        var partyId = $(this).attr('id');
                                                        var trid = $(this).closest('tr').attr('id');
                                                        $('.edit-party').data('casecard', cardcode);
                                                        $('table#party-table-data').find('.table-selected').not('tr#' + trid).removeClass('table-selected');
                                                        $('#' + trid).addClass('table-selected');
                                                        setPartyViewData(cardcode, caseno);
                                                    });
                                                    $(document.body).on('click', '.view_party_full_click', function (e) {
                                                        var trid = $(this).attr('id');
                                                        $('#' + trid).addClass('table-selected');
                                                        if($(e.target).hasClass("view_party") || $(e.target).parent().hasClass("view_party")) return false;
                                                        var cardcode = $(this).find('a:eq(0)').data('card');
                                                        var caseno = $(this).find('a:eq(0)').data('case');
                                                        var partyId = $(this).find('a:eq(0)').attr('id');
                                                        
                                                        $('.edit-party').data('casecard', cardcode);
                                                        $('table#party-table-data').find('.table-selected').not('tr#' + trid).removeClass('table-selected');
                                                        $('#' + trid).addClass('table-selected');
                                                        setPartyViewData(cardcode, caseno);
                                                    });
                                                    $(document.body).on('click', '.remove_party', function () {
                                                        var cardcode = $(this).data('card');
                                                        var caseno = $(this).data('case');
                                                        var contentId = $(this).parents('tr').attr('id');
                                                        swal({
                                                            title: "Are you sure to delete this Party? ",
                                                            text: "Deleted parties will not be recovered!",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, delete it!",
                                                            cancelButtonText: "No, cancel plz!",
                                                            closeOnConfirm: false,
                                                            closeOnCancel: true
                                                        }, function (isConfirm) {
                                                            if (isConfirm) {
                                                                $.blockUI();
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/removeParty',
                                                                    method: 'POST',
                                                                    dataType: 'json',
                                                                    data: {
                                                                        cardcode: cardcode,
                                                                        caseno: caseno
                                                                    },
                                                                    success: function (res) {
                                                                        $.unblockUI();
                                                                        if (contentId === 'tr-0') {
                                                                            location.reload();
                                                                        }
                                                                        
                                                                        if (res.status == 1) {
                                                                            partytable.ajax.reload(null, false);
                                                                            swal({
                                                                                title: "Deleted!",
                                                                                text: res.message,
                                                                                type: "success"
                                                                            });
                                                                            $('.caption').html(res.caption);
                                                                        } else {
                                                                            swal("Opps..", res.message, "error");
                                                                        }
                                                                        if (res.status == false) {
                                                                            swal({
                                                                                title: "Alert",
                                                                                text: "Oops! Some error has been occured while fetching details. Please try again later.",
                                                                                type: "warning"
                                                                            });
                                                                        } 
                                                                    }
                                                                });
                                                            }
                                                        });
                                                        return false;
                                                    });
                                                    $(".ajaxdropdowncase").click(function () {
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/getCaseConfigAjax',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            success: function (data) {
                                                                var staffInitial = '<?php echo $display_details->atty_hand ?>';
                                                                if ($("#editCaseStat").hasClass("select2Class")) {
                                                                    $("#editCaseStat option").remove();
                                                                    $.each(data.casestat, function (index, item) {
                                                                        $('#editCaseStat').append("<option value='" + item.casestat + "'>" + item.casestat + "</option>");
                                                                    });
                                                                } else if ($("#editCaseStat").hasClass("cstmdrp")) {
                                                                    $('#editCaseStat').next('ul.es-list').find('li').remove();
                                                                    $.each(data.casestat, function (index, item) {
                                                                        $('#editCaseStat').next('ul.es-list').append("<li value='" + item.casestat + "' class='es-visible'>" + item.casestat + "</li>");
                                                                    });
                                                                }
                                                                if ($("#editCaseType").hasClass("select2Class")) {
                                                                    $("#editCaseType option").remove();
                                                                    $.each(data.casetype, function (index, item) {
                                                                        $('#editCaseType').append("<option value='" + item.casetype + "'>" + item.casetype + "</option>");
                                                                    });
                                                                } else if ($("#editCaseType").hasClass("cstmdrp")) {
                                                                    $('#editCaseType').next('ul.es-list').find('li').remove();
                                                                    $.each(data.casetype, function (index, item) {
                                                                        $('#editCaseType').next('ul.es-list').append("<li value='" + item.casetype + "' class='es-visible'>" + item.casetype + "</li>");
                                                                    });
                                                                }
                                                                if ($("#editCaseAttyResp").hasClass("select2Class")) {
                                                                    $("#editCaseAttyResp option").remove();
                                                                    $.each(data.atty_resp, function (index, item) {
                                                                        $('#editCaseAttyResp').append("<option value='" + item.atty_resp + "'>" + item.atty_resp + "</option>");
                                                                    });
                                                                } else if ($("#editCaseAttyResp").hasClass("cstmdrp")) {
                                                                    $('#editCaseAttyResp').next('ul.es-list').find('li').remove();
                                                                    $.each(data.atty_resp, function (index, item) {
                                                                        $('#editCaseAttyResp').next('ul.es-list').append("<li value='" + item.atty_resp + "' class='es-visible'>" + item.atty_resp + "</li>");
                                                                    });
                                                                }
                                                                if ($("#editCaseAttyHand").hasClass("select2Class")) {
                                                                    $("#editCaseAttyHand option").remove();
                                                                    $.each(data.atty_hand, function (index, item) {
                                                                        $('#editCaseAttyHand').append("<option value='" + item.atty_hand + "'>" + item.atty_hand + "</option>");
                                                                    });
                                                                } else if ($("#editCaseAttyHand").hasClass("cstmdrp")) {
                                                                    $('#editCaseAttyHand').next('ul.es-list').find('li').remove();
                                                                    $.each(data.atty_hand, function (index, item) {
                                                                        $('#editCaseAttyHand').next('ul.es-list').append("<li value='" + item.atty_hand + "' class='es-visible'>" + item.atty_hand + "</li>");
                                                                    });
                                                                }
                                                                if ($("#editCaseAttyPara").hasClass("select2Class")) {
                                                                    $("#editCaseAttyPara option").remove();
                                                                    $.each(data.para_hand, function (index, item) {
                                                                        $('#editCaseAttyPara').append("<option value='" + item.para_hand + "'>" + item.para_hand + "</option>");
                                                                    });
                                                                } else if ($("#editCaseAttyPara").hasClass("cstmdrp")) {
                                                                    $('#editCaseAttyPara').next('ul.es-list').find('li').remove();
                                                                    $.each(data.para_hand, function (index, item) {
                                                                        $('#editCaseAttyPara').next('ul.es-list').append("<li value='" + item.para_hand + "' class='es-visible'>" + item.para_hand + "</li>");
                                                                    });
                                                                }
                                                                if ($("#editCaseAttySec").hasClass("select2Class")) {
                                                                    $("#editCaseAttySec option").remove();
                                                                    $.each(data.sec_hand, function (index, item) {
                                                                        $('#editCaseAttySec').append("<option value='" + item.sec_hand + "'>" + item.sec_hand + "</option>");
                                                                    });
                                                                } else if ($("#editCaseAttySec").hasClass("cstmdrp")) {
                                                                    $('#editCaseAttySec').next('ul.es-list').find('li').remove();
                                                                    $.each(data.sec_hand, function (index, item) {
                                                                        $('#editCaseAttySec').next('ul.es-list').append("<li value='" + item.sec_hand + "' class='es-visible'>" + item.sec_hand + "</li>");
                                                                    });
                                                                }
                                                                $.each(data.staffini, function (index, item) {
                                                                    $('#case_act_atty').append("<option value='" + item.staffini + "'>" + item.staffini + "</option>");
                                                                });
                                                                $.each(data.allstaff, function (index, item) {
                                                                    if (item.initials == '<?php echo $loginUser ?>') {
                                                                        $('select#whofrom').append("<option value='" + item.initials + "' selected='selected'>" + item.initials + "</option>");
                                                                    } else {
                                                                        $('select#whofrom').append("<option value='" + item.initials + "'>" + item.initials + "</option>");
                                                                    }
                                                                    $('select#whoto').append("<option value='" + item.initials + "'>" + item.initials + "</option>");
                                                                    $('#activity_staff').append("<option value='" + item.initials + "'>" + item.initials + "</option>");
                                                                    $('#default_staff').append("<option value='" + item.initials + "'>" + item.initials + "</option>");
                                                                });
                                                            }
                                                        });
                                                    });
                                                    $(document.body).on('click', '.view_attach', function () {
                                                        var caseNo = $("#caseNo").val();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/viewAttechment',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                actno: $(this).data('actid'),
                                                                caseno: caseNo,
                                                                filename: $(this).data('filename')
                                                            },
                                                            success: function (data) {}
                                                        });
                                                    });
                                                    $('#addinjury.modal').on('hidden.bs.modal', function () {
                                                        $('#addinjury h4.modal-title').text('Add Injury');
                                                        $('.select2Class').trigger('change');
                                                        $('input[type=checkbox]').val('').iCheck('uncheck');
                                                        $('#addInjuryForm')[0].reset();
                                                    });
                                                    $(document.body).on('click', 'ul.case-details li a', function () {
                                                        if ($(this).attr('href') == '#casedetails') {
                                                            casetasksTable.draw();
                                                        }
                                                    });
                                                    $(document.body).on('click', 'a.task-completed', function (e) {
                                                        var taskId = $(this).attr('data-taskId');
                                                        $('#taskNo').val(taskId);
                                                        var c = $('#' + taskId).attr("data-taskid");
                                                        var d = $('#' + taskId).attr("data-event");
                                                        var b = $('#' + taskId).attr("data-caseno");
                                                        $("#case_event").val(d);
                                                        $("#caseTaskId").val(c);
                                                        $("#caseno").val(b);
                                                        $("#TodayTaskCompleted").modal("show");

                                                        return false;
                                                    });


                                                    $(document.body).on('click', '#complete', function () {
                                                        $("#addnew").val('0');
                                                        var taskId = $('#taskNo').val();
                                                        var c = $('#' + taskId).attr("data-taskid");
                                                        var d = $('#' + taskId).attr("data-event");
                                                        var b = $('#' + taskId).attr("data-caseno");
                                                        $("#case_event").val(d);
                                                        $("#caseTaskId").val(c);
                                                        $("#caseno").val(b);
                                                        $("#TodayTaskCompleted").modal("show");
                                                    });

                                                    $('#TodayTaskCompleted.modal').on('show.bs.modal', function () {
                                                        $('#case_extra_event').val('');
														$(".select2Class#case_category").val("1").trigger("change");
														
                                                    });
                                                    $(document.body).on('click', '#completewithnew', function () {
                                                        var taskId = $('#taskNo').val();
                                                        var c = $('#' + taskId).attr("data-taskid");
                                                        var d = $('#' + taskId).attr("data-event");
                                                        var b = $('#' + taskId).attr("data-caseno");
                                                        $("#case_event").val(d);
                                                        $("#caseTaskId").val(c);
                                                        $("#caseno").val(b);
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>tasks/checkforlasttask',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                taskId: taskId,
                                                                caseno: b
                                                            },
                                                            success: function (data) {
                                                                return false;
                                                            }
                                                        });
                                                        $("#addnew").val('1');
                                                        $("#TodayTaskCompleted").modal("show");
                                                    });
                                                    $(document.body).on('click', '.injury_view', function () {
                                                        var caseNo = $("#caseNo").val();
                                                        var injuryId = $(this).attr('id');
                                                        $('div#injury_div').block();
                                                        $('table#injuryTable').find('.table-selected').not('tr#injury-' + injuryId).removeClass('table-selected');
                                                        $('#injury-' + injuryId).addClass('table-selected');
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                injury_id: injuryId,
                                                                caseno: caseNo
                                                            },
                                                            success: function (data) {
                                                                $('div#injury_div').unblock();
                                                                if (data != false) {
                                                                    setInjuryViewData(data);
                                                                }
                                                            }
                                                        });
                                                        return false;
                                                    });
                                                    $(document.body).on('click', '.injury_edit', function () {
                                                        var caseNo = $("#caseNo").val();
                                                        var injuryId = $(this).attr('id');
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                injury_id: injuryId,
                                                                caseno: caseNo
                                                            },
                                                            success: function (data) {
                                                                if (data != false) {
                                                                    $('#addinjury .modal-title').html('Edit Injury');
                                                                    $('input[name=field_injury_id]').val(data.injury_id);
                                                                    $('input[name=field_first]').val(data.first);
                                                                    $('input[name=field_last]').val(data.last);
                                                                    $('input[name=field_address]').val(data.address);
                                                                    $('input[name=field_city]').val(data.city);
                                                                    $('input[name=field_state]').val(data.state);
                                                                    $('input[name=field_zip_code]').val(data.zip_code);
                                                                    $('input[name=field_social_sec]').val(data.social_sec);
                                                                    $('input[name=field_aoe_coe_status]').val(data.aoe_coe_status);
                                                                    $('input[name=field_dor_date]').val(data.dor_date);
                                                                    $('input[name=field_s_w]').val(data.s_w);
                                                                    $('input[name=field_liab]').val(data.liab);
                                                                    $('input[name=field_other_sol]').val(data.other_sol);
                                                                    $('input[name=field_follow_up]').val(data.follow_up);
                                                                    $('input[name=field_app_status]').val(data.app_status);
                                                                    $('input[name=field_office]').val(data.office);
                                                                    $('select[name=field_status2]').val(data.status2);
                                                                    $('select[name=field_amended]').val(data.amended);
                                                                    $('input[name=field_case_no]').val(data.case_no);
                                                                    $('input[name=field_eamsno]').val(data.eamsno);
                                                                    $('input[name=field_e_name]').val(data.e_name);
                                                                    $('input[name=field_e_address]').val(data.e_address);
                                                                    $('input[name=field_e_city]').val(data.e_city);
                                                                    $('input[name=field_e_state]').val(data.e_state);
                                                                    $('input[name=field_e_zip]').val(data.e_zip);
                                                                    $('input[name=field_e_phone]').val(data.e_phone);
                                                                    $('input[name=field_e_fax]').val(data.e_fax);
                                                                    $('input[name=field_e2_name]').val(data.e2_name);
                                                                    $('input[name=field_e2_address]').val(data.e2_address);
                                                                    $('input[name=field_e2_city]').val(data.e2_city);
                                                                    $('input[name=field_e2_state]').val(data.e2_state);
                                                                    $('input[name=field_e2_zip]').val(data.e2_zip);
                                                                    $('input[name=field_e2_phone]').val(data.e2_phone);
                                                                    $('input[name=field_e2_fax]').val(data.e2_fax);
                                                                    $('select[name=field_i_adjsal]').val(data.i_adjsal);
                                                                    $('input[name=field_i_adjfst]').val(data.i_adjfst);
                                                                    $('input[name=field_i_adjuster]').val(data.i_adjuster);
                                                                    $('input[name=field_i_claimno]').val(data.i_claimno);
                                                                    $('input[name=field_i_name]').val(data.i_name);
                                                                    $('input[name=field_i_address]').val(data.i_address);
                                                                    $('input[name=field_i_city]').val(data.i_city);
                                                                    $('input[name=field_i_state]').val(data.i_state);
                                                                    $('input[name=field_i_zip]').val(data.i_zip);
                                                                    $('input[name=field_i_phone]').val(data.i_phone);
                                                                    $('input[name=field_i_fax]').val(data.i_fax);
                                                                    $('select[name=field_i2_adjsal]').val(data.i2_adjsal);
                                                                    $('input[name=field_i2_adjfst]').val(data.i2_adjfst);
                                                                    $('input[name=field_i2_adjuste]').val(data.i2_adjuste);
                                                                    $('input[name=field_i2_claimno]').val(data.i2_claimno);
                                                                    $('input[name=field_i2_name]').val(data.i2_name);
                                                                    $('input[name=field_i2_address]').val(data.i2_address);
                                                                    $('input[name=field_i2_city]').val(data.i2_city);
                                                                    $('input[name=field_i2_state]').val(data.i2_state);
                                                                    $('input[name=field_i2_zip]').val(data.i2_zip);
                                                                    $('input[name=field_i2_phone]').val(data.i2_phone);
                                                                    $('input[name=field_i2_fax]').val(data.i2_fax);
                                                                    $('input[name=field_d1_first]').val(data.d1_first);
                                                                    $('input[name=field_d1_last]').val(data.d1_last);
                                                                    $('input[name=field_d1_firm]').val(data.d1_firm);
                                                                    $('input[name=field_d1_address]').val(data.d1_address);
                                                                    $('input[name=field_d1_city]').val(data.d1_city);
                                                                    $('input[name=field_d1_state]').val(data.d1_state);
                                                                    $('input[name=field_d1_zip]').val(data.d1_zip);
                                                                    $('input[name=field_d1_phone]').val(data.d1_phone);
                                                                    $('input[name=field_d1_fax]').val(data.d1_fax);
                                                                    $('input[name=field_d2_first]').val(data.d2_first);
                                                                    $('input[name=field_d2_last]').val(data.d2_last);
                                                                    $('input[name=field_d2_firm]').val(data.d2_firm);
                                                                    $('input[name=field_d2_address]').val(data.d2_address);
                                                                    $('input[name=field_d2_city]').val(data.d2_city);
                                                                    $('input[name=field_d2_state]').val(data.d2_state);
                                                                    $('input[name=field_d2_zip]').val(data.d2_zip);
                                                                    $('input[name=field_d2_phone]').val(data.d2_phone);
                                                                    $('input[name=field_d2_fax]').val(data.d2_fax);
                                                                    if (data.ct1 == 1) {
                                                                        $('input[name=field_ct1]').iCheck('check');
                                                                    }
                                                                    $('input[name=field_doi]').val(data.doi);
                                                                    $('input[name=field_doi2]').val(data.doi2);
                                                                    $('input[name=field_adj1a]').val(data.adj1a);
                                                                    $('input[name=field_adj1b]').val(data.adj1b);
                                                                    $('input[name=field_adj1c]').val(data.adj1c);
                                                                    $('input[name=field_adj1d]').val(data.adj1d);
                                                                    $('input[name=field_adj1d2]').val(data.adj1d2);
                                                                    $('input[name=field_adj1d3]').val(data.adj1d3);
                                                                    $('input[name=field_adj1d4]').val(data.adj1d4);
                                                                    $('textarea[name=field_adj1e]').val(data.adj1e);
                                                                    $('select[name=field_pob1]').val(data.pob1);
                                                                    $('select[name=field_pob2]').val(data.pob2);
                                                                    $('select[name=field_pob3]').val(data.pob3);
                                                                    $('select[name=field_pob4]').val(data.pob4);
                                                                    $('select[name=field_pob5]').val(data.pob5);
                                                                    $('textarea[name=field_adj2a]').val(data.adj2a);
                                                                    $('input[name=field_adj3a]').val(data.adj3a);
                                                                    $('input[name=field_adj4a]').val(data.adj4a);
                                                                    $('input[name=field_adj5a]').val(data.adj5a);
                                                                    $('input[name=field_adj5b]').val(data.adj5b);
                                                                    $('input[name=field_adj5c]').val(data.adj5c);
                                                                    $('input[name=field_adj5d]').val(data.adj5d);
                                                                    $('input[name=field_adj6a]').val(data.adj6a);
                                                                    $('input[name=field_adj7a]').val(data.adj7a);
                                                                    $('input[name=field_adj7b]').val(data.adj7b);
                                                                    $('input[name=field_adj7c]').val(data.adj7c);
                                                                    $('input[name=field_adj7d]').val(data.adj7d);
                                                                    $('input[name=field_adj7e]').val(data.adj7e);
                                                                    $('input[name=field_doctors]').val(data.doctors);
                                                                    $('input[name=field_adj8a]').val(data.adj8a);
                                                                    $('input[name=field_adj9a]').val(data.adj9a);
                                                                    $('input[name=field_adj9b]').val(data.adj9b);
                                                                    $('input[name=field_adj9c]').val(data.adj9c);
                                                                    $('input[name=field_adj9d]').val(data.adj9d);
                                                                    $('input[name=field_adj9e]').val(data.adj9e);
                                                                    $('input[name=field_adj9f]').val(data.adj9f);
                                                                    $('input[name=field_adj9g]').val(data.adj9g);
                                                                    $('input[name=field_adj10city]').val(data.adj10city);
                                                                    $('input[name=field_adj10a]').val(data.adj10a);
                                                                    $('input[name=field_firmatty1]').val(data.firmatty1);
                                                                    $('select[name=field_fld1]').val(data.fld1);
                                                                    $('input[name=field_fld2]').val(data.fld2);
                                                                    $('input[name=field_fld3]').val(data.fld3);
                                                                    $('input[name=field_fld4]').val(data.fld4);
                                                                    $('input[name=field_fld5]').val(data.fld5);
                                                                    $('input[name=field_fld11]').val(data.fld11);
                                                                    $('input[name=field_fld12]').val(data.fld12);
                                                                    $('input[name=field_fld13]').val(data.fld13);
                                                                    $('input[name=field_fld14]').val(data.fld14);
                                                                    $('input[name=field_fld20]').val(data.fld20);
                                                                    $('#addinjury').modal({
                                                                        backdrop: 'static',
                                                                        keyboard: false
                                                                    });
                                                                    $("#addinjury").modal('show');
                                                                    $("#addInjuryForm select").trigger('change');
                                                                    $.unblockUI();
                                                                }
                                                            }
                                                        });
                                                        return false;
                                                    });
                                                    $(document.body).on('click', '#addInjury', function (e) {

                                                        if ($('#addInjuryForm').valid()) {
                                                            if (clickFlag == true) {
                                                                return false;
                                                            }
                                                            clickFlag = true;
                                                            var injuryId = $('input[name=field_injury_id]').val();
                                                            var caseNo = $("#caseNo").val();

                                                            if (injuryId == 0)
                                                            {
                                                                var injurydefaultcount = $('#count_injury').text();
                                                                var injurycount = parseInt(injurydefaultcount) + 1;
                                                            } else
                                                            {
                                                                var injurydefaultcount = $('#count_injury').text();
                                                                var injurycount = parseInt(injurydefaultcount);

                                                            }
                                                            e.preventDefault();
                                                            $('#addInjury').addClass("disabled");
                                                            e.preventDefault();
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/addInjuries',
                                                                method: 'POST',
                                                                dataType: 'json',
                                                                data: $('form#addInjuryForm').serialize() + '&field_caseno=' + caseNo,
                                                                success: function (data) {
                                                                    if (data.status == 1) {
                                                                        $("#copyinjury").removeClass("disabled");
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: data.message,
                                                                            type: "success"
                                                                        },
                                                                                function () {
                                                                                    $("#addinjury").modal('hide');
                                                                                    $.ajax({
                                                                                        url: '<?php echo base_url(); ?>cases/getAjaxinjuryDetails',
                                                                                        method: 'POST',
                                                                                        dataType: 'json',
                                                                                        data: {
                                                                                            injury_id: data.injury_id,
                                                                                            caseno: caseNo
                                                                                        },
                                                                                        success: function (data) {
                                                                                            $('div#injury_div').unblock();
                                                                                            if (data != false) {
                                                                                                injurytable.draw();
                                                                                                setInjuryViewData(data);
                                                                                                $("#count_injury").text(injurycount);
                                                                                            }

                                                                                        }
                                                                                    });
                                                                                }
                                                                        );

                                                                    } else {
                                                                        swal("Oops...", data.message, "error");
                                                                    }
                                                                    $('#addInjury').removeClass("disabled");
                                                                    clickFlag = false;
                                                                }
                                                            });
                                                        }
                                                    });
                                                    $(document.body).on('hidden.bs.modal', '#addinjury.modal', function () {
                                                        $('#addinjury a[href="#general"]').tab('show');
                                                    });
                                                    $(document.body).on('click', '.injury_delete', function () {
                                                        var injuryId = this.id;

                                                        var injurydefaultcount = $('#count_injury').text();
                                                        var injurycount = parseInt(injurydefaultcount) - 1;

                                                        swal({
                                                            title: "Are you sure to Remove this Injury?",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, Remove it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            $('div#injury_div').block();
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/RemoveInjury',
                                                                data: {
                                                                    injury_id: injuryId
                                                                },
                                                                method: "POST",
                                                                dataType: 'json',
                                                                success: function (result) {
                                                                    $('div#injury_div').unblock();
                                                                    if (result.status == 1) {
                                                                        setInjuryViewData('');
                                                                        injurytable.rows('tr#injury-' + injuryId).remove().draw();
                                                                        $("#count_injury").text(injurycount);
                                                                        if (injurycount == 0) {
                                                                            $("#copyinjury").addClass("disabled");
                                                                        }
                                                                        if ($('#injuryTable tbody tr').length < 1) {

                                                                            $('#injuryTable tbody').html('<tr id="noRecord"><td colspan="5"><h4>No Injury Found</h4></td></tr>');
                                                                        }
                                                                        swal("Success !", result.message, "success");
                                                                    } else {
                                                                        swal("Oops...", result.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        });
                                                        return false;
                                                    });
                                                    $(document.body).on('blur', 'input[name="caseFrom"]', function () {
                                                        if (this.value !== '' && this.value > 0) {
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/getCaseCaption',
                                                                dataType: 'json',
                                                                method: 'post',
                                                                data: {
                                                                    'caseNo': this.value
                                                                },
                                                                success: function (data) {
                                                                    if (data.status == '1') {
                                                                        $('input#caseFromCaption').val(data.caption);
                                                                    } else {
                                                                        $('input#caseFromCaption').val('');
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            $('input#caseFromCaption').val('');
                                                        }
                                                    });
                                                    $(document.body).on('click', '#cloneCaseBtn', function () {
                                                        swal({
                                                            title: 'Are you sure you want to copy information from '+$("input#caseFromCaption").val()+'?',
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, copy it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            if ($('#cloneCaseForm').valid()) {
                                                                $.blockUI();
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/cloneCase',
                                                                    dataType: 'json',
                                                                    method: 'post',
                                                                    data: $('#cloneCaseForm').serialize(),
                                                                    success: function (data) {
                                                                        if (data.status == '1') {
                                                                            swal("Success !", data.message, "success");
                                                                            $("#cParties").iCheck("uncheck");
                                                                            $("#cInjuries").iCheck("uncheck");
                                                                            $("#cCaseActivity").iCheck("uncheck");
                                                                            $('#clonecase').modal('hide');
                                                                            $('#cloneCaseForm')[0].reset();
                                                                        } else {
                                                                            swal("Oops...", data.message, "error");
                                                                        }
                                                                        $.unblockUI();
                                                                    }
                                                                });
                                                            }
                                                        });
                                                        return false;
                                                    });
                                                    $('#clonecase.modal').on('show.bs.modal', function () {
                                                        $("#cParties").iCheck("uncheck");
                                                        $("#cInjuries").iCheck("uncheck");
                                                        $("#cCaseActivity").iCheck("uncheck");
                                                        $('#cloneCaseForm')[0].reset();
                                                    });

                                                    $(document.body).on('click', '#protectCasebtn', function () {
                                                        setCaseProtectValidation();
                                                        if ($('#protectCaseForm').valid()) {
                                                            $.blockUI();
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/protectCase',
                                                                dataType: 'json',
                                                                method: 'post',
                                                                data: $('#protectCaseForm').serialize(),
                                                                success: function (data) {
                                                                    if (data.status == '1') {
                                                                        swal("Success !", data.message, "success");
                                                                        $('#protactcase').modal('hide');
                                                                    } else {
                                                                        swal("Oops...", data.message, "error");
                                                                    }
                                                                    $.unblockUI();
                                                                }
                                                            });
                                                        }
                                                        return false;
                                                    });
                                                    $(document.body).on('click', '.add_case_activity', function () {
                                                        $("#uploadFile3").html('Upload');
                                                        $("#caseact_uploaddocDefault").val('');
                                                        $(".caseActUploadDocument").hide();
                                                        /*myDropzone.removeAllFiles(true);*/
                                                        var caseNo = $("#caseNo").val();
                                                        $('#add_new_case_activity')[0].reset();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/getBillingDefault',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                caseno: caseNo
                                                            },
                                                            success: function (data) {
                                                                $("#default_staff").val(data.initials).trigger("change");
                                                                $("#default_entry_type").val(data.memo1).trigger("change");
                                                                /*if (data.freezedt != '1970-01-01 05:30:00' && data.freezedt != '1899-12-30 00:00:00' && data.freezedt != '0000-00-00 00:00:00') {
                                                                 $('.freezeUntil').html(data.freezedt);
                                                                 $('.freezeUntil').parent().removeClass('hidden');
                                                                 }*/
                                                                $("#activity_entry_type").val(data.memo1).trigger("change");
                                                                var activityText = $("#activity_entry_type option:selected").text();
                                                                $("#activity_short_desc").html(activityText);
                                                                $("#activity_event_desc").html(activityText);
                                                                $('#activity_teamno').val(data.biteamno);
                                                                $('#activity_staff').val(data.initials).trigger("change");
                                                                $('.dateoflastpayment').html(data.paidto);
                                                                $('#activity_latefee').html(data.latefee);
                                                                $('#activity_bi_rate').val(data.bihourrate);
                                                            }
                                                        });
                                                        $("#activity_event").val('');
                                                        $("#case_act_atty").trigger('change');
                                                        $("#activity_title").trigger('change');
                                                        $("#case_category").trigger('change');
                                                        $("#color_codes").trigger('change');
                                                        $("#security").trigger('change');
                                                        $("#activity_typeact").trigger('change');
                                                        $("#activity_doc_type").trigger('change');
                                                        $("#activity_style").trigger('change');
                                                        $("#case_act_orientation").trigger('change');
                                                        $("#activity_staff").trigger('change');
                                                        $('#addcaseact .modal-title').html('Add New Case Activity');
                                                        $('#addcaseact').modal('show');
                                                    });
                                                    $(document.body).on('click', '.activity_delete', function () {
                                                        var actId = $(this).data('actid');
                                                        swal({
                                                            title: "Are you sure to Remove this Activity?",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, delete it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            var caseNo = $("#caseNo").val();
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>cases/deleteActivity',
                                                                method: 'POST',
                                                                data: {
                                                                    actno: actId,
                                                                    caseno: caseNo
                                                                },
                                                                success: function (data) {
                                                                    if (data == 1) {
                                                                        swal("Deleted!", "Activity Deleted Successfully !!", "success");
                                                                        $('.activity_delete[data-actid=' + actId + ']').parents('tr').remove();
                                                                    } else {
                                                                        swal("Oops...", "Oops! Something went wrong.", "error");
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    });
                                                    $(document.body).on('click', '.view_all_attach', function () {
                                                        var caseNo = $("#caseNo").val();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/viewAllAttechment',
                                                            method: 'POST',
                                                            data: {
                                                                actno: $(this).data('actid'),
                                                                oledoc: $(this).data('oledoc'),
                                                                caseno: caseNo
                                                            },
                                                            success: function (data) {
                                                                $(".viewAllAtachment").html(data);
                                                                $("#viewMultipleDocument").modal('show');
                                                            }
                                                        });
                                                    });
                                                    $('select[name="whoto"]').select2({
                                                        placeholder: 'Select User to Send Email',
                                                        multiple: true,
                                                        selectOnClose: false
                                                    }).on('change', function () {
                                                        $('select[name="whoto"]').valid();
                                                        if($('ul.select2-selection__rendered li:first').attr('title') == '-- Select User to Send Email --') {
                                                            $('.select2-selection__choice__remove:first').trigger('click');
                                                        }
                                                    });
                                                    $('#mailsenddisable1').on('click', function() {
                                                        $('#mailsenddisable').trigger('click');
                                                    });
                                                    $('#send_mail_form').submit(function () {
                                                        $('#mailsenddisable').attr("disabled", "disabled");
                                                        $('#mailsenddisable1').attr("disabled", "disabled");

                                                        setTimeout(function () {
                                                            $('#mailsenddisable').removeAttr("disabled", "disabled");
                                                            $('#mailsenddisable1').removeAttr("disabled", "disabled");
                                                        }, 3000);

                                                        var whoto = [];
                                                        if($('select[name="whoto"]').val()){
                                                            whoto = $('select[name="whoto"]').val();
                                                        }
                                                        var subject = $('#mail_subject').val();
                                                        var urgent = $('input[name="mail_urgent"]:checked').val();
                                                        var ext_emails = $('input[name="emails"]').val();
                                                        var mailbody = CKEDITOR.instances.mailbody.getData();
                                                        var filenameVal = $("#filenameVal").val();
                                                        var mailType = $("#mailType").val();
                                                        var case_no = $("#case_no").val();
                                                        var case_category = $("#case_category").val();
                                                        var vaild_data = true;
                                                        if (whoto == '' && ext_emails == '') {
                                                            $('select[name="whoto"]').parent().append('<span id="wvalid" style="color:red">Please Select a User to send mail to</span>');
                                                            $('input[name="emails"]').parent().append('<span style="color:red">Please add external party to send email </span>');
                                                            setTimeout(function () {
                                                                $('select[name="whoto"]').parent().find('#wvalid').remove();
                                                                $('input[name="emails"]').parent().find('span').remove();
                                                            }, 5000);
                                                            vaild_data = false;
                                                        }
                                                        if (ext_emails != '') {
                                                            vaild_data = validate_ext_email($("input[name=emails]").val());
                                                        }
                                                        if (mailbody == '') {
                                                            vaild_data = false;
                                                            $('.mail-text label').after('<span id="content-mail-text" style="color:red">Please Enter Mail Content</span>');
                                                            setTimeout(function () {
                                                                $(document).find('#content-mail-text').remove();
                                                            }, 5000);
                                                            return false;
                                                        }
                                                        var form_data = new FormData();
                                                        form_data.append("whoto", whoto);
                                                        form_data.append("subject", subject);
                                                        form_data.append("urgent", urgent);
                                                        form_data.append("ext_emails", ext_emails);
                                                        form_data.append("mailbody", mailbody);
                                                        form_data.append("mailType", mailType);
                                                        form_data.append("filenameVal", filenameVal);
                                                        form_data.append("caseNo", case_no);
                                                        form_data.append("caseCategory", case_category);
														
														var filelength = $("input[name='afiles1[]']");
                                                        var fileGlobal = [];
                                                        var tmp_fileGlobal = $("input[name='files_global_var[]']").map(function(){return $(this).val();}).get();
                                                        $.each(tmp_fileGlobal, function(k, v) {
                                                            var array = $.map(JSON.parse(v), function(value, index) {
                                                                return [ index + ':' + value];
                                                            });
                                                            fileGlobal[k] = array;
                                                        });
                                                        var files_arry =[];
                                                        form_data.append("fileGlobalVariable",JSON.stringify(fileGlobal));
						
														$.each(filelength, function (e, t) {
															files_arry.push(t.defaultValue);
															if(filelength.length == files_arry.length)
															{
																form_data.append("files" ,files_arry);
															}
														});
                                                        if (vaild_data == true) {
                                                            var chkmodule = $('#module').val();
                                                            if(chkmodule == 'email_new'){
                                                                    var newUrl = '<?php echo base_url('email_new/send_email'); ?>';
                                                                }else{
                                                                    var newUrl = '<?php echo base_url('email/send_email'); ?>'
                                                                }
                                                            $.ajax({
                                                                url: newUrl,
                                                                method: 'POST',
                                                                dataType: 'json',
                                                                cache: false,
                                                                contentType: false,
                                                                processData: false,
                                                                data: form_data,
                                                                beforeSend: function (xhr) {
                                                                    $.blockUI();
                                                                },
                                                                complete: function (jqXHR, textStatus) {
                                                                    $.unblockUI();
                                                                },
                                                                success: function (data) {
                                                                    if (data.result == "true") {
                                                                        var rltcnt = $('.related_cnt').html();
                                                                        rltcnt++;
                                                                        $('.related_cnt').html(rltcnt);
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: "Mail is successfully sent.",
                                                                            type: "success"
                                                                        });
																		$('#emails').val('');
																		$('#mail_subject').val('');
                                                                        $("#whoto").val($("#whoto option:eq(0)").val()).trigger("change");
																		$('#mail_urgent').iCheck('uncheck');
																		$('.fileremove').remove();
                                                                        $('#totalFileSize').val('0');
																		CKEDITOR.instances.mailbody.setData("");
                                                                        $('input[name="afiles1[]"]').remove();
                                                                        $('input[name="files_global_var[]"]').remove();
																		
                                                                    } else if (data.result == "true" && data.ext_party == '0') {
                                                                        swal({
                                                                            title: "Alert",
                                                                            text: "Oops! Something went wrong. Email wasn't send to External Party.",
                                                                            type: "warning"
                                                                        });
                                                                    } else if (data.result == "false" && data.attachment_issue != '') {
                                                                        swal({
                                                                            title: "Alert",
                                                                            text: "Oops! " + data.attachment_issue,
                                                                            type: "warning"
                                                                        });
                                                                        return false;
                                                                    } else {
                                                                        swal({
                                                                            title: "Alert",
                                                                            text: "Oops! Some error has occured. Please try again.",
                                                                            type: "warning"
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        return false;
                                                    });
                                                    if (document.getElementById('applicant_ini')) {
                                                        var firstName = document.getElementById('applicant_ini').value;
                                                        var intials = firstName.charAt(0);
                                                        var profileImage = $('#applicantImage').text(intials);
                                                    }

                                                    if ($("#caseStatus").val() == 0) {
                                                        $('.case_delete').attr('disabled', "disabled");
                                                    } else {
                                                        $('.recall_case').attr('disabled', "disabled");
                                                    }
                                                    $(".video ").on('click', '.case_video', function () {
                                                        var theModal = $(this).data("target"),
                                                                videoSRC = $(this).attr("data-video"),
                                                                videoSRCauto = videoSRC;
                                                        $(theModal + ' video').attr('src', videoSRCauto);
                                                        $(theModal + ' button.close').click(function () {
                                                            $(theModal + ' video').attr('src', videoSRC);
                                                        });
                                                    });
                                                    $(".video").on('click', '#del_video', function () {
                                                        var link = $(this).attr('data-id');
                                                        var $this = $(this);
                                                        swal({
                                                            title: 'Video',
                                                            text: "Are you sure to delete?",
                                                            type: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonText: 'Yes, delete it!'
                                                        }, function () {
                                                            if (link != '') {
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/delete_video_photo',
                                                                    method: 'POST',
                                                                    data: {
                                                                        link: link
                                                                    },
                                                                    beforeSend: function (xhr) {
                                                                        $.blockUI();
                                                                    },
                                                                    complete: function (jqXHR, textStatus) {
                                                                        $.unblockUI();
                                                                    },
                                                                    success: function (result) {
                                                                        var data = $.parseJSON(result);
                                                                        if (data.status == 1) {
                                                                            swal({
                                                                                title: "Video",
                                                                                text: "File is successfully removed!",
                                                                                type: "success"
                                                                            });
                                                                            $this.prev("a").remove();
                                                                            $this.remove();
                                                                            $('#social_security').modal('hide');
                                                                        } else {
                                                                            swal("Oops...", "Oops! Something went wrong.", "error");
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                                    $(".image-row").on('click', '#del_photo', function () {
                                                        var link = $(this).attr('data-id');
                                                        var $this = $(this);
                                                        swal({
                                                            title: 'Image',
                                                            text: "Are you sure to delete?",
                                                            type: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonText: 'Yes, delete it!'
                                                        }, function () {
                                                            if (link != '') {
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/delete_video_photo',
                                                                    method: 'POST',
                                                                    data: {
                                                                        link: link
                                                                    },
                                                                    beforeSend: function (xhr) {
                                                                        $.blockUI();
                                                                    },
                                                                    complete: function (jqXHR, textStatus) {
                                                                        $.unblockUI();
                                                                    },
                                                                    success: function (result) {
                                                                        var data = $.parseJSON(result);
                                                                        if (data.status == 1) {
                                                                            swal({
                                                                                title: "Photo",
                                                                                text: "Removed Successfully",
                                                                                type: "success"
                                                                            });
                                                                            $this.prev("a").remove();
                                                                            $this.remove();
                                                                            $('#social_security').modal('hide');
                                                                        } else {
                                                                            swal("Oops...", "Oops! Something went wrong.", "error");
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                                    $(".injDocs").on('click', '#del_injDoc', function () {
                                                        var link = $(this).attr('data-id');
                                                        var $this = $(this);
                                                        swal({
                                                            title: 'Injury Document',
                                                            text: "Are you sure to delete?",
                                                            type: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonText: 'Yes, delete it!'
                                                        }, function () {
                                                            if (link != '') {
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/delete_injury_doc',
                                                                    method: 'POST',
                                                                    data: {
                                                                        link: link
                                                                    },
                                                                    beforeSend: function (xhr) {
                                                                        $.blockUI();
                                                                    },
                                                                    complete: function (jqXHR, textStatus) {
                                                                        $.unblockUI();
                                                                    },
                                                                    success: function (result) {
                                                                        var data = $.parseJSON(result);
                                                                        if (data.status == 1) {
                                                                            swal({
                                                                                title: "Injury Document",
                                                                                text: "Removed Successfully",
                                                                                type: "success"
                                                                            });
                                                                            $this.prev("a").remove();
                                                                            $this.remove();
                                                                        } else {
                                                                            swal("Oops...", "Oops! Something went wrong.", "error");
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                                    $(document.body).on('click', '#printCasedetails', function () {
                                                        $.ajax({
                                                            url: '<?php echo base_url() . 'cases/printCaseDetails'; ?>',
                                                            method: "POST",
                                                            data: {
                                                                'caseNo': $("#caseNo").val()
                                                            },
                                                            success: function (result) {
                                                                $(result).printThis({
                                                                    debug: false,
                                                                    header: "<h2 style='margin-top:20px !important;'><center>General Case Information</center></h2>"
                                                                });
                                                            }
                                                        });
                                                    });
                                                    $(document.body).on('click', '#printCase', function () {
                                                        $.ajax({
                                                            url: '<?php echo base_url() . 'cases/printCaseDetails'; ?>',
                                                            method: "POST",
                                                            data: {
                                                                'caseNo': $("#caseNo").val()
                                                            },
                                                            success: function (result) {
                                                                $(result).printThis({
                                                                    debug: false,
                                                                    header: "<h2 style='margin-top:20px !important;'><center>General Case Information</center></h2>"
                                                                });
                                                            }
                                                        });
                                                    });
                                                    $(document.body).on('click', '.case_delete', function () {
                                                        var case_status = 0;
                                                        swal({
                                                            title: "Are you sure to delete this case?",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, Remove it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            case_recall_delete(case_status);
                                                        });
                                                    });
                                                    $(document.body).on('click', '.recall_case', function () {
                                                        var case_status = 1;
                                                        swal({
                                                            title: "Are you sure to recall this case?",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, Recall it!",
                                                            closeOnConfirm: false
                                                        }, function () {
                                                            case_recall_delete(case_status);
                                                        });
                                                    });
                                                    $(document).on('dblclick', '#related_case_table tbody tr td:first-child a', function () {
                                                        window.open("<?php echo base_url() ?>cases/case_details/" + this.id, '_blank');
                                                        return false;
                                                    });
                                                    $(document).on('submit', "#addRelatedcaseForm", function (e) {
                                                        e.preventDefault();
                                                        e.stopPropagation();
                                                        $('#addRelatedcase').trigger("click");
                                                    });
                                                    $(document.body).on('click', '#addRelatedcase', function () {
                                                        $.blockUI();
                                                        url = '<?php echo base_url(); ?>cases/addrelatedcase';
                                                        if ($('#hdnCaseId').val() != $('#relatedcasenumber').val()) {
                                                            $.ajax({
                                                                url: url,
                                                                method: "POST",
                                                                data: $('#addRelatedcaseForm').serialize() + '&caseno=' + $('#caseNo').val(),
                                                                success: function (result) {
                                                                    var data = $.parseJSON(result);
                                                                    $.unblockUI();
                                                                    if (data.status == '1') {
                                                                        /*swal("Success !", data.message, "success");*/
                                                                        swal({
                                                                            title: "Success !",
                                                                            text: data.message,
                                                                            type: "success"
                                                                        }, function() {
                                                                            releteddataTable.draw();
                                                                        });
                                                                        $('#addRelatedcaseForm')[0].reset();
                                                                        $('#addRelatedCase').modal('hide');
                                                                        /*releteddataTable.ajax.reload(null, false);*/
                                                                        
                                                                    } else {
                                                                        swal("Oops...", data.message, "error");
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            $.unblockUI();
                                                            swal("Oops...", 'The same case should be not attached again.', "error");
                                                        }
                                                    });
                                                    $(document.body).on('click', '.attach_case', function () {
                                                        var social_sec = $("#social_sec").val();
                                                        var caseNO = $("#caseNo").val();
                                                        if (social_sec != '') {
                                                            swal({
                                                                title: "Warning!",
                                                                text: "Are you sure to Relate all Cases with the Same Social Security number? You will need to go into each case and detach the related cases to undo this action",
                                                                type: "warning",
                                                                showCancelButton: true,
                                                                confirmButtonColor: "#DD6B55",
                                                                confirmButtonText: "Yes",
                                                                closeOnConfirm: false
                                                            }, function () {
                                                                $.blockUI();
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/SameSocialSecurityCase',
                                                                    data: {
                                                                        social_sec: social_sec,
                                                                        caseNO: caseNO
                                                                    },
                                                                    method: "POST",
                                                                    success: function (result) {
                                                                        $.unblockUI();
                                                                        rcs.clear();
                                                                        var res = $.parseJSON(result);
                                                                        swal.close();
                                                                        $('#social_security').modal('show');
                                                                        $.each(res.data, function (k, v) {
                                                                            rcs.row.add([v.checkbox, v.caption, v.caseno]).draw(false);
                                                                        });
                                                                        $('#someCheckbox-all').on('click', function () {
                                                                            var rows = rcs.rows({
                                                                                'search': 'applied'
                                                                            }).nodes();
                                                                            $('input[type="checkbox"]', rows).prop('checked', this.checked);
                                                                        });
                                                                    }
                                                                });
                                                            });
                                                        } else {
                                                            swal("Oops... ", "You have not entered a Social Security Number for this client", "error");
                                                        }
                                                    });
                                                    $(document.body).on('click', '#addsocialsecurity', function () {
                                                        var related = [];
                                                        $.each($("input[name='relatedcase']:checked"), function () {
                                                            related.push($(this).val());
                                                        });
                                                        var caseNo = $("#caseNo").val();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/AddSocialSecurity',
                                                            method: 'POST',
                                                            data: {
                                                                related: related,
                                                                caseno: caseNo
                                                            },
                                                            success: function (result) {
                                                                var data = $.parseJSON(result);
                                                                if (data.status == 1)
                                                                {
                                                                    /*swal("Success !", data.message, "success");*/
                                                                    swal({
                                                                        title: "Success !",
                                                                        text: data.message,
                                                                        type: "success"
                                                                    }, function() {
                                                                        releteddataTable.draw();
                                                                    });
                                                                    $('#social_security').modal('hide');
                                                                    /*releteddataTable.ajax.reload(null, false);*/
                                                                } else {
                                                                    swal("Oops...", data.message, "error");
                                                                }
                                                            }
                                                        });
                                                    });
                                                    $(document.body).on('click', '#group_chat', function () {
                                                        if (!$('#toggle-controlbox').hasClass('hidden')) {
                                                            $('#toggle-controlbox').trigger("click");
                                                        }
                                                        if (!$('.chatbox.chatroom').attr('id')) {
                                                            $('a[data-id=chatrooms]').trigger("click");
                                                            var chat = "<?php echo htmlspecialchars($case_no . "-" . trim($display_details->caption1)); ?>";
                                                            $('input[name=chatroom]').val(chat);
                                                            $('input[name=join]').trigger('click');
                                                            var initials = '<?php echo $this->session->userdata('user_data')['initials']; ?>';
                                                            var setNicknameInterval = setInterval(function () {
                                                                if ($('input[name=nick]').length > 0) {
                                                                    clearInterval(setNicknameInterval);
                                                                    $('input[name=nick]').val(initials);
                                                                    $('input[name=join]').trigger("click");
                                                                    $('a[data-id=users]').trigger("click");
                                                                }
                                                            }, 100);
                                                        }
                                                    });
                                                    $(document.body).on('click', '.edit_caseAct td:not(:nth-child(n+6))', function () {
                                                        var row = $(this).closest('tr').attr('id');
                                                        var Actno = row.replace('caseAct', '');
                                                        caseActEdit(Actno);
                                                    });
                                                    $(document.body).on("click", ".back_email", function () {
                                                        show_hide_main_window('email_listing');
                                                    });
                                                    $(document.body).on("click", "a.read-email", function () {
                                                        var filename = $(this).data('filename');
                                                        var email_con_id = $(this).attr('email_con_id');
                                                        var $this = $(this);
                                                        var event_info = '';
                                                        var eventInfo = '';
                                                        if($this.parent().parent().find( ".event_details" ).text().indexOf("Email to") != -1) {
                                                            event_info = $this.parent().parent().find( ".event_details" ).text().split(": ");
                                                            eventInfo = event_info[1];
                                                        } else if($this.parent().parent().find( ".event_details" ).text().indexOf("Email received from") != -1) {
                                                            event_info = $this.parent().parent().find( ".event_details" ).text().split("To : ");
                                                            eventInfo = event_info[1];
                                                        }
                                                        
                                                        var chkmodule = $('#module').val();
                                                        if(chkmodule == 'email_new'){
                                                            var newUrl = '<?php echo base_url('email_new/view'); ?>';
                                                        }else{
                                                            var newUrl = '<?php echo base_url('email/view'); ?>'
                                                        }
                                                        $.ajax({
                                                                url: newUrl,
                                                                dataType: 'json',
                                                                type: 'POST',
                                                                data: {
                                                                    current_list: '',
                                                                    parcelid: filename,
                                                                },
                                                                beforeSend: function () {
                                                                    start = 0;
                                                                    $.blockUI();
                                                                },
                                                                complete: function () {
                                                                    $.unblockUI();
                                                                },
                                                                success: function (data) {
																	
                                                                    show_hide_main_window('email_detail');
                                                                    $("#filenameVal").val(filename);
                                                                    $("#reply_iore").val(iore);
                                                                    if (data.unread_count > 0) {
                                                                        $('.total_mail_count').html(data.unread_count).show();
                                                                        $('.email_count').html(data.unread_count).show();
                                                                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                                                                    } else {
                                                                        $('.total_mail_count').hide();
                                                                    }
                                                                    if (current_list == 'sent') {
                                                                        $('.total_mail_count').hide();
                                                                    }
                                                                    show_hide_navigation_buttons();
                                                                    $('#emailSubject').html(data.emails[0].subject);
                                                                    $('#emailDatetime').html(moment(data.emails[0].datesent).format('hh:mm A DD MMM YYYY'));
                                                                    $('#emailfromName').html(data.emails[0].whofrom);
                                                                    if (data.emails[0].whofrom == data.emails[0].email_con_people) {
                                                                        data.emails[0].email_con_people = '<?php echo $this->session->userdata('user_data')['initials']; ?>';
                                                                    }
                                                                    $("#sentto").html("");
                                                                    $('#inboxto').html("<br><br>To: " + data.emails[0].whoto);
                                                                    if(data.emails[0].whotoexternal){
                                                                        var str = data.emails[0].whoto+','+data.emails[0].whotoexternal;
                                                                        var trim = str.replace(/(^\s*,)|(,\s*$)/g, '');
                                                                        $('#inboxto').html('<br><br>To: '+trim);
                                                                    }
                                                                    $('.mail-body #emailBody').html(data.emails[0].body);
                                                                    $('.mail-body #file_with_attachment').html(data.file_with_attachment);
                                                                    $('.move_to_trash_one').attr('data-id', filename);
                                                                    $('.print_email').attr('data-id', filename);
                                                                    $('#emailDetailDiv').modal('show');
                                                                    if (data.reply_all == 1) {
                                                                        $('.emailDetailDiv .reply_all_compose_email').attr('email_con_id', data.emails[0].email_con_id);
                                                                    } else {
                                                                        $('.emailDetailDiv .reply_all_compose_email').hide();
                                                                    }
                                                                }
                                                            });
                                                        
                                                    });
                                                    $(document.body).on("click", "a.read-email1", function () {
                                                        $('#reply_bttn').css('display','none');
                                                        var filename = $(this).data('filename');
                                                        var email_con_id = $(this).attr('email_con_id');
                                                        var $this = $(this);
                                                        var event_info = '';
                                                        var eventInfo = '';
                                                        if($this.parent().parent().find( ".event_details" ).text().indexOf("Email to") != -1) {
                                                            event_info = $this.parent().parent().find( ".event_details" ).text().split(": ");
                                                            eventInfo = event_info[1];
                                                        } else if($this.parent().parent().find( ".event_details" ).text().indexOf("Email received from") != -1) {
                                                            event_info = $this.parent().parent().find( ".event_details" ).text().split("To : ");
                                                            eventInfo = event_info[1];
                                                        }
                                                        var chkmodule = $('#module').val();
                                                            if(chkmodule == 'email_new'){
                                                                var newUrl = '<?php echo base_url('email_new/view'); ?>';
                                                            }else{
                                                                var newUrl = '<?php echo base_url('email/view'); ?>'
                                                            }
                                                            $.ajax({
                                                                url: newUrl,
                                                                dataType: 'json',
                                                                type: 'POST',
                                                                data: {
                                                                    current_list: '',
                                                                    parcelid: filename,
                                                                },
                                                                beforeSend: function () {
                                                                    start = 0;
                                                                    $.blockUI();
                                                                },
                                                                complete: function () {
                                                                    $.unblockUI();
                                                                },
                                                                success: function (data) {
                                                                    
                                                                    show_hide_main_window('email_detail');
                                                                    $("#filenameVal").val(filename);
                                                                    if (data.unread_count > 0) {
                                                                        $('.total_mail_count').html(data.unread_count).show();
                                                                        $('.email_count').html(data.unread_count).show();
                                                                        $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                                                                    } else {
                                                                        $('.total_mail_count').hide();
                                                                    }
                                                                    if (current_list == 'sent') {
                                                                        $('.total_mail_count').hide();
                                                                    }
                                                                    show_hide_navigation_buttons();
                                                                    $('#emailSubject').html(data.emails[0].subject);
                                                                    $('#emailDatetime').html(moment(data.emails[0].datesent).format('hh:mm A DD MMM YYYY'));
                                                                    $('#emailfromName').html(data.emails[0].whofrom);
                                                                    if (data.emails[0].whofrom == data.emails[0].email_con_people) {
                                                                        data.emails[0].email_con_people = '<?php echo $this->session->userdata('user_data')['initials']; ?>';
                                                                    }
                                                                    
                                                                    $("#sentto").html("");
                                                                    $('#inboxto').html("<br><br>To: " + data.emails[0].whoto);
                                                                    if(data.emails[0].whotoexternal){
                                                                        var str = data.emails[0].whoto+','+data.emails[0].whotoexternal;
                                                                        var trim = str.replace(/(^\s*,)|(,\s*$)/g, '');
                                                                        $('#inboxto').html('<br><br>To: '+trim);
                                                                    }
                                                                    $('.mail-body #emailBody').html(data.emails[0].body);
                                                                    $('.mail-body #file_with_attachment').html(data.file_with_attachment);
                                                                    $('.move_to_trash_one').attr('data-id', filename);
                                                                    $('.print_email').attr('data-id', filename);
                                                                    $('#emailDetailDiv').modal('show');
                                                                    if (data.reply_all == 1) {
                                                                        $('.emailDetailDiv .reply_all_compose_email').attr('email_con_id', data.emails[0].email_con_id);
                                                                    } else {
                                                                        $('.emailDetailDiv .reply_all_compose_email').hide();
                                                                    }
                                                                }
                                                            });
                                                    });
                                                    $(document.body).on("click", "a.read-email-close", function () {
                                                        $(this).parents('tr').next('tr.rclose').hide();
                                                        $(this).removeClass('read-email-close');
                                                        $(this).addClass('read-email');
                                                    });
                                                    $(document.body).on("click", "a.activity_email_close", function () {
                                                        $(this).parents('tr').next('tr.rclose').hide();
                                                        $(this).removeClass('activity_email_close');
                                                        $(this).addClass('activity_email');
                                                    });
                                                    $('#Category').on('change', function () {
                                                        var categoryser = $(this).val();
                                                        var chackvalue = $('#all_category').is(':checked') ? 1 : 0;
                                                        search_category(categoryser, chackvalue);
                                                    });
                                                    $(document.body).on('ifToggled', 'input#all_category', function () {
                                                        var categoryser = $('#Category').val();
                                                        var chackvalue = $(this).is(':checked') ? 1 : 0;
                                                        search_category(categoryser, chackvalue);
                                                    });
                                                    $('#protectCasecnlbtn').click(function () {
                                                        $('#protactcase').modal('hide');
                                                        $('#propassword').val('');
                                                        $('#procpassword').val('');
                                                    });
                                                    $('.toggle').on('click', function () {
                                                        $('.sidebar').toggleClass("sidebar-collapsed");
                                                    });
                                                    $(document).on('click', '.addfile', function () {
                                                        $('.att-add-file1:last').after('<div class="clearfix"></div><label class="col-xs-3 control-label"></label><div class="col-xs-9 att-add-file1 marg-top5 clearfix"> <input type="file" name="afiles[]" id="afiles" multiple><i class="fa fa-plus-circle marg-top5 addfile" aria-hidden="true"></i> <i class="fa fa-times marg-top5 removefile" aria-hidden="true"></i></div>');
                                                    });
                                                    $(document).on('click', '.removefile', function () {
                                                        $(this).closest('.att-add-file1').prev('label').remove();
                                                        $(this).closest('.att-add-file1').remove();
                                                    });
                                                    $('.discard_message').click(function (e) {
                                                        swal({
                                                            title: "Are you sure?",
                                                            text: "The changes you made will be lost",
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Yes, discard it!",
                                                            closeOnConfirm: true
                                                        }, function () {
                                                            $('#emails').val('');
                                                            $('#mail_subject').val('');
                                                            $("#whoto").val($("#whoto option:eq(0)").val()).trigger("change");
                                                            $('#mail_urgent').iCheck('uncheck');
                                                            $('.fileremove').remove();
                                                            $('#totalFileSize').val('');
                                                            CKEDITOR.instances.mailbody.setData("");
                                                            $('#CaseEmailModal').find('.att-add-file').not(':first').remove();
                                                            $('#CaseEmailModal').modal("hide");
                                                        });
                                                    });
                                                    $(document.body).on("click", "#saveCompletedTask1", function () {
                                                        if ($(this).text() == 'Complete and Add New') {
                                                            $("#addnew").val(1);
                                                        } else {
                                                            $("#addnew").val(0);
                                                        }
                                                        var e = $("#caseTaskId").val();
                                                        var a = $("#case_event").val();
                                                        var d = $("#case_extra_event").val();
                                                        var b = $("#case_category").val();
                                                        var c = $("#caseno").val();
                                                        var n = $("#addnew").val();
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: "<?php echo base_url(); ?>tasks/completedTaskWithCase",
                                                            data: {
                                                                taskId: e,
                                                                case_event: a,
                                                                case_extra_event: d,
                                                                case_category: b,
                                                                caseno: c
                                                            },
                                                            method: "POST",
                                                            success: function (f) {
                                                                $.unblockUI();
                                                                var g = $.parseJSON(f);
														
                                                                if (g.status == 1) {
																	casetasksTable.draw();	
                                                                    swal({
                                                                        title: "Success!",
                                                                        text: g.message,
                                                                        type: "success"
                                                                    }, function () {
                                                                        if (n == 1) {
                                                                            $('#addtask').modal("show");
                                                                        }
                                                                    });
                                                                    $("#addnew").val('0');
																	 $('tr#li-' + e).remove(); 
                                                                } else if (g.status == 2) {
																	casetasksTable.draw();
                                                                    if (n == 0) {
                                                                        swal({
                                                                            title: "Warning!",
                                                                            text: '<?php echo AddNewTaskCase; ?>',
                                                                            type: "warning",
                                                                            showCancelButton: true,
                                                                            confirmButtonColor: "#DD6B55",
                                                                            confirmButtonText: "Add new",
                                                                            closeOnConfirm: true
                                                                        }, function () {
                                                                            $('#addtask').modal("show");
                                                                        });
                                                                    } else if (n == 1) {
																		casetasksTable.draw();
                                                                        swal({
                                                                            title: "Success!",
                                                                            text: g.message,
                                                                            type: "success"
                                                                        }, function () {
                                                                            if (n == 1) {
                                                                                $('#addtask').modal("show");
                                                                            }
                                                                        });
                                                                    }
																	$('tr#li-' + e).remove();
                                                                }
                                                                $("#TodayTaskCompleted").modal("hide");
                                                            }
                                                        });
                                                    });
                                                    $(document.body).on('click', '.addSearchedParties', function () {
                                                        var caseNo = $("#caseNo").val();
                                                        var cardcode = $("#searched_party_cardnumber").html();
                                                        var type = $("#searched_party_type").html();
                                                        var fullname = $("#searched_party_fullname").val();
                                                        var firm = $("#searched_party_firmname").val();
                                                        var nameofParty = '';
                                                        if (firm != "") {
                                                            nameofParty = firm;
                                                        } else {
                                                            nameofParty = fullname;
                                                        }
                                                        var radioVal = $("input[name='rolodex_search_parties']:checked").val();
                                                        if (radioVal == "rolodex-cardno") {
                                                            nameofParty = fullname;
                                                        }
                                                        $("#addparty").modal('hide');
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/addSearchedPartyToCase',
                                                            method: 'POST',
                                                            dataType: 'json',
                                                            data: {
                                                                cardcode: cardcode,
                                                                caseno: caseNo,
                                                                type: type
                                                            },
                                                            beforeSend: function (xhr) {
                                                                $.blockUI();
                                                            },
                                                            complete: function (jqXHR, textStatus) {
                                                                $.unblockUI();
                                                            },
                                                            success: function (data) {
                                                                swal("Success !", "Party is successfully added", "success");
                                                                $.ajax({
                                                                    url: '<?php echo base_url(); ?>cases/getCaseCaption',
                                                                    dataType: 'json',
                                                                    method: 'post',
                                                                    data: {
                                                                        'caseNo': caseNo
                                                                    },
                                                                    success: function (data) {
                                                                        if (data.status == '1') {
                                                                            $('.caption').html(data.caption);
                                                                        }
                                                                    }
                                                                });
                                                                partytable.ajax.reload(null, false);
                                                            }
                                                        });
                                                    });
                                                });

                                                $(document.body).on('click', 'a.activity_email', function () {
                                                    $('#reply_bttn').css('display','block');
                                                    var filename = $(this).data('actid');
                                                    var caseNo = $("#caseNo").val();
                                                    var $this = $(this);
                                                    var event_info = '';
                                                    var eventInfo = '';
                                                    if($this.parent().parent().find( ".event_details" ).text().indexOf("Email to") != -1) {
                                                        event_info = $this.parent().parent().find( ".event_details" ).text().split(": ");
                                                        eventInfo = event_info[1];
                                                    } else if($this.parent().parent().find( ".event_details" ).text().indexOf("Email received from") != -1) {
                                                            event_info = $this.parent().parent().find( ".event_details" ).text().split("To : ");
                                                            eventInfo = event_info[1];
                                                    }
                                                    var chkmodule = $('#module').val();
                                                    var newUrl = '<?php echo base_url('cases/viewactivityemailNew'); ?>';
                                                    
                                                    $.ajax({
                                                        url: newUrl,
                                                        method: 'POST',
                                                        data: {
                                                            filename: filename,
                                                            caseno: caseNo
                                                        },
                                                        beforeSend: function () {
                                                            $.blockUI();
                                                        },
                                                        success: function (data) {
                                                            if($(".read-email").length != 0) {
                                                                $(".read-email").remove();
                                                            }
                                                            $this.parent().parent().after(data);
                                                            var filename = $('a.read-email').data('filename');
                                                            var email_con_id = $('a.read-email').attr('email_con_id');
                                                            var $this_email = $('a.read-email');
                                                            var chkmodule = $('#module').val();
                                                            if(chkmodule == 'email_new'){
                                                                var newUrl = '<?php echo base_url('email_new/view'); ?>';
                                                            }else{
                                                                var newUrl = '<?php echo base_url('email/view'); ?>'
                                                            }
                                                            $.ajax({
                                                                    url: newUrl,
                                                                    dataType: 'json',
                                                                    type: 'POST',
                                                                    data: {
                                                                        current_list: '',
                                                                        parcelid: filename,
                                                                    },
                                                                    beforeSend: function () {
                                                                        start = 0;
                                                                    },
                                                                    complete: function () {
                                                                        $.unblockUI();
                                                                    },
                                                                    success: function (data) {
                                                                        
                                                                        show_hide_main_window('email_detail');
                                                                        $("#filenameVal").val(filename);
                                                                        if (data.unread_count > 0) {
                                                                            $('.total_mail_count').html(data.unread_count).show();
                                                                            $('.email_count').html(data.unread_count).show();
                                                                            $('.mail-box-header .total_mail_count').html('(' + data.unread_count + ')').show();
                                                                        } else {
                                                                            $('.total_mail_count').hide();
                                                                        }
                                                                        if (current_list == 'sent') {
                                                                            $('.total_mail_count').hide();
                                                                        }
                                                                        show_hide_navigation_buttons();
                                                                        $('#emailSubject').html(data.emails[0].subject);
                                                                        $('#emailDatetime').html(moment(data.emails[0].datesent).format('hh:mm A DD MMM YYYY'));
                                                                        $('#emailfromName').html(data.emails[0].whofrom);
                                                                        if (data.emails[0].whofrom == data.emails[0].email_con_people) {
                                                                            data.emails[0].email_con_people = '<?php echo $this->session->userdata('user_data')['initials']; ?>';
                                                                        }
                                                                        
                                                                       
                                                                           
                                                                        $("#sentto").html("");
                                                                        $('#inboxto').html("<br><br>To: " + data.emails[0].whoto);
                                                                        if(data.emails[0].whotoexternal){
                                                                            var str = data.emails[0].whoto+','+data.emails[0].whotoexternal;
                                                                            var trim = str.replace(/(^\s*,)|(,\s*$)/g, '');
                                                                            $('#inboxto').html('<br><br>To: '+trim);
                                                                        }
                                                                        if (data.emails[0].caseno != '0' && data.emails[0].caseno != '') {
                                                                            var url = '<?php echo base_url() ?>';
                                                                            $.ajax({
                                                                                url: url + "cases/get_party_info",
                                                                                dataType: "json",
                                                                                type: "POST",
                                                                                async: false,
                                                                                data: {
                                                                                    caseno: data.emails[0].caseno
                                                                                },
                                                                                success: function (result) {
                                                                                    if (typeof (result['firm']) != 'undefined' && result['firm'] != '') {
                                                                                        caseInfo = result['firm'];
                                                                                    } else if (typeof (result['first']) != 'undefined' && typeof (result['last']) != 'undefined') {
                                                                                        caseInfo = result['first'] + ' ' + result['last'];
                                                                                    } else {
                                                                                        caseInfo = '';
                                                                                    }
                                                                                },
                                                                                complete: function () {
                                                                                    $.unblockUI();
                                                                                }
                                                                            });
                                                                            var i = url + "cases/case_details/" + data.emails[0].caseno;
                                                                            $("#gotocase").html('Go To Case : <a href="' + i + '" target="_blank" >' + data.emails[0].caseno + " " + caseInfo + "</a>");
                                                                        }
                                                                        
                                                                        $('.mail-body #emailBody').html(data.emails[0].body);
                                                                        $('.mail-body #file_with_attachment').html(data.file_with_attachment);
                                                                        $('.move_to_trash_one').attr('data-id', filename);
                                                                        $('.print_email').attr('data-id', filename);
                                                                        $('#emailDetailDiv').modal('show');
                                                                        if (data.reply_all == 1) {
                                                                            $('.emailDetailDiv .reply_all_compose_email').attr('email_con_id', data.emails[0].email_con_id);
                                                                        } else {
                                                                            $('.emailDetailDiv .reply_all_compose_email').hide();
                                                                        }
                                                                    }
                                                                });
                                                  
                                                        }
                                                    });
                                                });

                                                function case_recall_delete(case_status) {
                                                    var caseNo = $("#caseNo").val();
                                                    var caseStatus = $("#caseStatus").val();
                                                    $.ajax({
                                                        url: '<?php echo base_url(); ?>cases/RemoveCase',
                                                        data: {
                                                            caseNo: caseNo,
                                                            caseStatus: case_status
                                                        },
                                                        method: "POST",
                                                        dataType: 'json',
                                                        success: function (result) {
                                                            if (result.status == 1) {
                                                                swal("Success !", result.message, "success");
                                                                window.location.href = '<?php echo base_url() . 'cases/case_details/'; ?>' + caseNo;
                                                            } else {
                                                                swal("Oops...", result.message, "error");
                                                            }
                                                        }
                                                    });
                                                    return false;
                                                }
                                                function search_category(categoryser, chackvalue) {
                                                    var caseno = $("#caseNo").val();
                                                    caseactTable.destroy();
                                                    caseactTable = $('#case_activity_list').DataTable({
                                                        retrieve: true,
                                                        "serverSide": true,
                                                        stateSave: true,
                                                        processing:true,
                                                        stateSaveParams: function (settings, data) {
                                                                data.search.search = "";
                                                                data.start = 0;
                                                                delete data.order;
                                                                data.order = [0, "desc"];
                                                        },
                                                        "autoWidth": true,
                                                        "scrollX": true,
                                                        "order": [
                                                            [0, "desc"]
                                                        ],
                                                        "pageLength": 100,
                                                        "columnDefs": [{
                                                                "width": "10%",
                                                                "targets": 0
                                                            },
                                                            {
                                                                "width": "5%",
                                                                "targets": 1
                                                            },
                                                            {
                                                                "width": "50%",
                                                                "targets": 2
                                                            },
                                                            {
                                                                "width": "4%",
                                                                "targets": 3
                                                            },
                                                            {
                                                                "width": "4%",
                                                                "targets": 4
                                                            },
                                                            {
                                                                "width": "27%",
                                                                "targets": 5
                                                            }
                                                        ],
                                                        lengthMenu: [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
                                                        "aoColumnDefs": [{
                                                                "bSortable": false,
                                                                "aTargets": [5]
                                                            },
                                                            {
                                                                "bSearchable": false,
                                                                "aTargets": [5]
                                                            },
                                                            {
                                                                "bSortable": false,
                                                                "aTargets": [4]
                                                            },
                                                            {
                                                                "bSearchable": false,
                                                                "aTargets": [4]
                                                            },
                                                            {
                                                                className: "event_details",
                                                                "aTargets": [1]
                                                            }
                                                        ],
                                                        "ajax": {
                                                            url: "<?php echo base_url(); ?>cases/getAllActivity",
                                                            type: "POST",
                                                            data: {
                                                                'category': parseInt(categoryser),
                                                                'chackvalue': parseInt(chackvalue),
                                                                'caseId': parseInt(caseno)
                                                            }
                                                        },
                                                        "fnCreatedRow": function (nRow, aData, iDataIndex) {
                                                            $(nRow).attr('id', 'caseAct' + aData[6]);
                                                            if (aData[7] != 0) {
                                                                $(nRow).css({
                                                                    'background-color': aData[7],
                                                                    'color': aData[8]
                                                                });
                                                            }
                                                        },
                                                        "drawCallback": function (settings) {
                                                            $('div.dataTables_scrollBody').scrollTop(0);
                                                        }
                                                    });
                                                }

                                                function show_hide_navigation_buttons() {
                                                    $('.start_mail').html(start + 1);
                                                    if (total > 20) {
                                                        $('.end_mail').html(start + length);
                                                    } else {
                                                        $('.end_mail').html(start + total);
                                                    }
                                                    $('.total_mail').html(total);
                                                    if (start == 0) {
                                                        $('.prev_mails').attr('disabled', true);
                                                        if (total > start + length) {
                                                            $('.next_mails').attr('disabled', false);
                                                        } else {
                                                            $('.next_mails').attr('disabled', true);
                                                        }
                                                    } else {
                                                        $('.prev_mails').attr('disabled', false);
                                                        if (total > start + length) {
                                                            $('.next_mails').attr('disabled', false);
                                                        } else {
                                                            $('.next_mails').attr('disabled', true);
                                                            $('.end_mail').html(total);
                                                        }
                                                    }
                                                }
                                                function show_hide_main_window(screen) {
                                                    if (screen == 'email_detail') {
                                                        $('.emailDetailDiv').show();
                                                    } else if (screen == 'email_listing') {
                                                        $('.changeContentDiv').hide();
                                                        $('.emailDetailDiv').hide();
                                                        $('.emailListing').show();
                                                    } else if (screen == 'email_compose') {
                                                        $('.changeContentDiv').hide();
                                                        $('.email_conversation').hide();
                                                        $('.composeEmailDiv').show();
                                                    } else if (screen == 'email_conversation') {
                                                        $('.changeContentDiv').hide();
                                                        $('.email_conversation').show();
                                                    }
                                                }
												
                                                function validate_ext_email(v_ext_emails) {
                                                    var emails = '';
                                                    var count_e = 0;
                                                    if (v_ext_emails.indexOf(',') !== -1) {
                                                        emails = v_ext_emails.split(",");
                                                        count_e = emails.length;
                                                    }
                                                    var pattern = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                                                    if (count_e > 0) {
                                                        for (i = 0; i < emails.length; i++) {
                                                            if (!pattern.test(emails[i].trim())) {
                                                                swal({
                                                                    title: "Alert",
                                                                    text: "Oops! Please enter valid email ID.\n" + emails[i].trim(),
                                                                    type: "warning"
                                                                });
                                                                return false;
                                                            }
                                                        }
                                                    } else {
                                                        if (!pattern.test(v_ext_emails.trim())) {
                                                            swal({
                                                                title: "Alert",
                                                                text: "Oops! Please enter valid email ID.\n" + v_ext_emails.trim(),
                                                                type: "warning"
                                                            });
                                                            return false;
                                                        }
                                                    }
                                                    return true;
                                                }
                                                function validateAttachFileExtension(fld_value) {
                                                    var valid_extension = /(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.gif|\.png|\.txt|\.xlsx|\.xls|\.rtf|\.html|\.htm|\.wpd|\.wps|\.csv|\.ppt|\.pptx|\.zip|\.tar|\.xml|\.XLR)$/i;
                                                    if (!valid_extension.test(fld_value)) {
                                                        swal({
                                                            title: "Alert",
                                                            text: "Oops! Please provide valid file format.\n" + fld_value + ' is not valid file format',
                                                            type: "warning"
                                                        });
                                                        return false;
                                                    }
                                                    return true;
                                                }
                                                function send_email_show() {
                                                    $('li.active').removeClass('active');
                                                    $('.showtool').addClass('active');
                                                    $('.showtool').click();
                                                }
                                                function deleteRelatedcase(id) {
                                                    swal({
                                                        title: "Warning!",
                                                        text: "Are you sure to Remove this Related Case?",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Yes, delete it!",
                                                        closeOnConfirm: false
                                                    }, function () {
                                                        $.blockUI();
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>cases/deleteRelatedcase',
                                                            data: {
                                                                caseId: id
                                                            },
                                                            method: "POST",
                                                            success: function (result) {
                                                                $.unblockUI();
                                                                var data = $.parseJSON(result);
                                                                if (data.status == 1) {
                                                                    /*swal("Success !", data.message, "success");*/
                                                                    swal({
                                                                        title: "Success !",
                                                                        text: data.message,
                                                                        type: "success"
                                                                    }, function() {
                                                                        releteddataTable.draw();
                                                                    });
                                                                    /*releteddataTable.ajax.reload(null, false);*/
                                                                } else {
                                                                    swal("Oops...", data.message, "error");
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                                function validateVideoFileExtension(fld_value) {
                                                    var valid_extension = /(\.mp4)$/i;
                                                    if (!valid_extension.test(fld_value)) {
                                                        swal({
                                                            title: "Alert",
                                                            text: "Oops! Please provide valid video format.\n" + fld_value + ' is not valid video format',
                                                            type: "warning"
                                                        });
                                                        return false;
                                                    }
                                                    return true;
                                                }
                                                function setCaseCloneValidation() {
                                                    var validator = $('#cloneCaseForm').validate({
                                                        ignore: [],
                                                        rules: {
                                                            'CopyData[]': {
                                                                required: true
                                                            },
                                                            caseFrom: {
                                                                remote: {
                                                                    url: '<?php echo base_url(); ?>Prospects/CheckCaseno',
                                                                    type: "post",
                                                                    data: {
                                                                        pro_caseno: function () {
                                                                            return $('#cloneCaseForm :input[name="caseFrom"]').val();
                                                                        }
                                                                    }
                                                                },
                                                                digits: true,
                                                                required: true
                                                            }
                                                        },
                                                        messages: {
                                                            'CopyData[]': "Please select minimum 1 checkbox.",
                                                            caseFrom: {
                                                                remote: "This caseno is not exist"
                                                            }
                                                        },
                                                        errorPlacement: function (error, element) {
                                                            error.insertAfter($(element).parents('div.form-group'));
                                                        }
                                                    });
                                                    return validator;
                                                }
                                                function setCaseProtectValidation() {
                                                    var validator = $('#protectCaseForm').validate({
                                                        rules: {
                                                            procpassword: {
                                                                equalTo: "#propassword"
                                                            }
                                                        },
                                                        messages: {
                                                            procpassword: " Enter Confirm Password Same as Password"
                                                        },
                                                        errorPlacement: function (error, element) {
                                                            error.insertAfter($(element).parents('div.form-group'));
                                                        }
                                                    });
                                                    return validator;
                                                }
                                                function updateTask() {
                                                    $.ajax({
                                                        url: '<?php echo base_url(); ?>cases/getTaskDetails',
                                                        method: 'POST',
                                                        dataType: 'json',
                                                        async: true,
                                                        data: {
                                                            caseno: $("#caseNo").val()
                                                        },
                                                        success: function (data) {
                                                            var task_content = '';
                                                            var today = new Date();
                                                            var dd = today.getDate();
                                                            var mm = today.getMonth() + 1;
                                                            var yyyy = today.getFullYear();
                                                            today = mm + '/' + dd + '/' + yyyy;
                                                            if (data.length > 0) {
                                                                task_content = '<table class="table table-striped table-bordered table-hover"><thead>\n\
																					<th width="10%">Task Date</th>\n\<th width="15%">From</th>\n\
																					<th width="60%">Task Details</th>\n\
																					<th width="27%">Actions</th></thead><tbody>';
                                                                $.each(data, function (key, value) {
                                                                    task_content += '<tr id="li-' + value.mainkey + '" data-id="' + value.mainkey + '" data-event="' + value.event + '" data-caseno="' + value.caseno + '" >';
                                                                    var task_dt = value.datereq;
																	
																	
                                                                    if (value.isCompleted == 1) {
                                                                    } else {
                                                                        task_content += '<td><span class="m-l-xs inline-block">' + value.datereq + '</span></td>';
                                                                        task_content += '<td><span class="m-l-xs inline-block">' + value.whofrom + '</span></td>';
                                                                        task_content += '<td><span class="m-l-xs inline-block">' + value.event + '</span></td>';
																		if (value.isCompleted == 1) {
																			task_content += '<td><span class="m-l-xs inline-block"><button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onclick="editTask(' + value.mainkey + ')"><i class="fa fa-edit"></i></button></span> <button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onclick="deleteTask(' + value.mainkey + ')"><i class="fa fa-trash"></i></button></td>';
																		}
																		else
																		{
																			task_content += '<td><span class="m-l-xs inline-block"><button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onclick="editTask(' + value.mainkey + ')"><i class="fa fa-edit"></i></button></span> <button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onclick="deleteTask(' + value.mainkey + ')"><i class="fa fa-trash"></i></button> <a class="btn btn-info btn-circle-action btn-circle task-completed" title="Complete" type="button" id="' + value.mainkey + '" data-taskId="' + value.mainkey + '" data-event="' + value.event + '" data-caseno="' + value.caseno + '"><i class="fa fa-check"></i></a></td>';
																		}
                                                                    }
                                                                    task_content += '</tr>';
                                                                });
                                                                task_content += '</tbody></table>';
                                                            } else {
                                                                task_content += '<li>No Task Available</li>';
                                                            }
                                                            $('#task-to-list ul.todo-list').html(task_content);
                                                        }
                                                    });
                                                }
                                                function generate_tr_html(item) {
                                                    var readClass = "";
                                                    var who_or_from = "";
                                                    var html = "";
                                                    var ecount = (item.ethread > 1) ? '&nbsp;(' + item.ethread + ')' : '';
                                                    html += '<tr class="' + readClass + '">';
                                                    html += '<td style="width:12%" class="mail-ontact">';
                                                    if (item.urgent == 'Y') {
                                                        html += '<i class="fa fa-circle text-danger"></i>&nbsp;';
                                                    } else if (item.readyet != 'Y') {
                                                        html += '<i class="fa fa-circle text-navy"></i>&nbsp;';
                                                    } else {
                                                        html += '<i class="fa fa-circle text-warning"></i>&nbsp;';
                                                    }
                                                    html += '<a class="read-email1" href="javascript:" data-filename="' + item.parcelid + '" email_con_id="' + item.parcelid + '">' + ((item.whofrom_name == null) ? item.whofrom.split('@')[0] : item.whofrom) + ecount + '</a>';
                                                    html += '</td>';
                                                    html += '<td style="width:58%" class="mail-subject">';
                                                    html += '<a class="read-email1" href="javascript:" data-filename="' + item.parcelid + '" email_con_id="' + item.parcelid + '">' + item.subject + '<span class="pull-right">';
                                                    if (item.attachment_file != 0) {
                                                        html += '<i class="fa fa-paperclip"></i>';
                                                    }
                                                    html += '</span></a>';
                                                    html += '</td>';
                                                    html += '<td style="width:20%" class="mail-date">' + moment(item.datesent).format('MMM DD, YYYY hh:mm A') + '</td>';
                                                    html += '</tr>';
                                                    return html;
                                                }
                                                function setImagePagination() {
                                                    var start = 0;
                                                    var nb = 16;
                                                    var end = start + nb;
                                                    var length = $('.image-row.lightBoxGallery img').length;
                                                    var list = $('.image-row.lightBoxGallery img');
                                                    list.hide().filter(':lt(' + (end) + ')').show();
                                                    if (start == 0) {
                                                        $('a.prev').attr('disabled', true);
                                                    }
                                                    $('.prev, .next').click(function (e) {
                                                        e.preventDefault();
                                                        if ($(this).hasClass('prev')) {
                                                            start -= nb;
                                                        } else {
                                                            start += nb;
                                                        }
                                                        if (start < 0 || start >= length) {
                                                            start = 0;
                                                        }
                                                        end = start + nb;
                                                        if (start == 0) {
                                                            list.hide().filter(':lt(' + (end) + ')').show();
                                                        } else {
                                                            list.hide().filter(':lt(' + (end) + '):gt(' + (start - 1) + ')').show();
                                                        }
                                                        if (start == 0) {
                                                            $('a.prev').attr('disabled', true);
                                                        } else {
                                                            $('a.prev').attr('disabled', false);
                                                        }
                                                        if (end == length) {
                                                            $('a.next').attr('disabled', true);
                                                        } else {
                                                            $('a.next').attr('disabled', false);
                                                        }
                                                    });
                                                }
                                                function setVideoPagination() {
                                                    var start = 0;
                                                    var nb = 9;
                                                    var end = start + nb;
                                                    var length = $('.video .case_video video').length;
                                                    var list = $('.video .case_video video');
                                                    list.hide().filter(':lt(' + (end) + ')').show();
                                                    if (start == 0) {
                                                        $('a.vprev').attr('disabled', true);
                                                    }
                                                    $('.vprev, .vnext').click(function (e) {
                                                        e.preventDefault();
                                                        if ($(this).hasClass('vprev')) {
                                                            start -= nb;
                                                        } else {
                                                            start += nb;
                                                        }
                                                        if (start < 0 || start >= length) {
                                                            start = 0;
                                                        }
                                                        end = start + nb;
                                                        if (start == 0) {
                                                            list.hide().filter(':lt(' + (end) + ')').show();
                                                        } else {
                                                            list.hide().filter(':lt(' + (end) + '):gt(' + (start - 1) + ')').show();
                                                        }
                                                        if (start == 0) {
                                                            $('a.vprev').attr('disabled', true);
                                                        } else {
                                                            $('a.vprev').attr('disabled', false);
                                                        }
                                                        if (end == length) {
                                                            $('a.vnext').attr('disabled', true);
                                                        } else {
                                                            $('a.vnext').attr('disabled', false);
                                                        }
                                                    });
                                                }
                                                $(document.body).on('hidden.bs.modal', '#videoModal.modal', function () {
                                                    $('video').trigger('pause');
                                                });

                                                $(document.body).on('hidden.bs.modal', '#CaseEmailModal.modal', function () {
                                                    send_mail_form1_validate.resetForm();
                                                    $('.fileremove').remove();
                                                    $('#totalFileSize').val('0');
                                                    $('input[name="afiles1[]"]').remove();
                                                    $('input[name="files_global_var[]"]').remove();
                                                    $('#CaseEmailModal').find('.att-add-file').not(':first').remove();
                                                });

                                                $(document.body).on('click', 'div#addcontact a[data-toggle="tab"]', function (e) {
                                                    if (!$('#addRolodexForm').valid()) {
                                                        e.preventDefault();
                                                        return false;
                                                    }
                                                });
                                                function remove_error(id) {
                                                    var myElem = document.getElementById(id + '-error');
                                                    if (myElem === null) {

                                                    } else {
                                                        document.getElementById(id + '-error').innerHTML = '';
                                                    }
                                                }
</script>
	<script>
	$(document).on('click',".compose-mail",function (e) {
			$('.displayattatchment').hide();
			$("#send_mail_form")[0].reset();
	});
	$(function() {
    $('#afiles').bind("change", function() {
        var fileSize = parseInt($('#totalFileSize').val());
        var formData = new FormData();
		var fileName = document.getElementById('afiles').files;
        var fi = document.getElementById('afiles');
		var fsize = '';
        for (var i = 0, len = document.getElementById('afiles').files.length; i < len; i++) {
		var fsize = fi.files.item(i).size / 1024;
        fileSize = fileSize + fsize;
		if(fsize < 20001)
		{
            formData.append("file" + i, document.getElementById('afiles').files[i]);
		}else
		{
			var filenameerror = fi.files.item(i).name;
			swal({
                    title: "Alert",
                    text: "Your " + filenameerror + " file size " + Math.round(fsize/1024) + " MB. Please upload file size up to 20 MB",
                    type: "warning"
                });
            var input = $("#afiles");
            var fileName = input.val();
            if(fileName) {
                input.val('');
            }
		}
        }
        if(fileSize >= 20001) {
            swal({
                    title: "Alert",
                    text: "Total size of all files including the current file which you have selected is " + Math.round(fileSize/1024) + " MB. Please upload files which are in total of size up to 20 MB",
                    type: "warning"
                });
            $("#afiles").val('');
            fileSize = fileSize - fsize;
            return false;
        }
        $('#totalFileSize').val(fileSize);
		$.ajax({
            url: "<?php echo base_url();?>email/emailattaupload",
            type: 'post',
            data: formData,
            dataType: 'html',
            async: true,
            processData: false,
            contentType: false,
			beforeSend: function () { $('#selectedloader').addClass('loader');},
            complete: function () {
            $('#selectedloader').removeClass('loader');
            },
            success : function(data) {
                var input = $("#afiles");
				 var fileName = input.val();
    
				if(fileName) { 
					input.val('');
				}
				$('#selectedFiles').append(data);
				 
            },
            error : function(request) {
                console.log(request.responseText);
            }
        });
    });
});
$(document).on('click',".remove",function (e) {
	var a = $(this).parent().css('display','none');
	var imagename = $(this).data('image');
	
	$.ajax({
            url: "<?php echo base_url();?>email/emailattaremove",
            type: 'post',
            data: {'imagename': imagename},
            success : function(data) {
                removedFileSize = data.split(':')[1];
                totalFileSize = $('#totalFileSize').val() - removedFileSize;
                console.log(totalFileSize);
                $('#totalFileSize').val(totalFileSize);
				console.log(data);
            },
            error : function(request) {
                console.log(request);
            }
        });			
	});
        
        $('.rdate').on('show.bs.modal', function (e) {
    if (e.stopPropagation) e.stopPropagation();
});

$(document.body).on("click", ".reply_compose_email", function () {
   var url = '<?php echo base_url() ?>';
    var l = $("#filenameVal").val();
    $(".compose-mail").trigger("click"), $.ajax({
        url: url + "email_new/reply_email",
        dataType: "json",
        type: "POST",
        data: {
            filename: l,
           
        },
        beforeSend: function () {
            start = 0, $.blockUI()
        },
        complete: function () {
            $.unblockUI()
        },
        success: function (e) {
                $('#emailDetailDiv').modal('hide');
                $('#CaseEmailModal').modal('show');
                $("#mailType").val("reply"),e.emails[0].whofrom ? $("#whoto1").val(e.emails[0].whofrom).trigger("change") : $("#emails1").val(e.emails[0].whotoexternal), $("#mail_subject1").val(e.emails[0].subject), $("#mail_urgent1").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent1").iCheck("check"), $("#case_no1").val(e.emails[0].caseno != '0' ? e.emails[0].caseno : "");
            
            if (CKEDITOR.instances.mailbody1)
                    CKEDITOR.instances.mailbody1.destroy();
            CKEDITOR.replace('mailbody1');
            CKEDITOR.instances.mailbody1.setData(e.emails[0].body);
        }
    })
});


$(document.body).on("click", ".reply_all_compose_email", function () {
    var url = '<?php echo base_url() ?>';
    $("#whoto").val([]).trigger("change");
    var i = $("#filenameVal").val();
        e = $("#task-navigation li.active").attr("id");
    $(".compose-mail").trigger("click"), $.ajax({
        url: url + "email_new/reply_all_email",
        dataType: "json",
        type: "POST",
        data: {
            current_list: e,
            filename: i,
        },
        beforeSend: function () {
            start = 0, $.blockUI()
        },
        complete: function () {
            $.unblockUI()
        },
        success: function (e) {
            $('#emailDetailDiv').modal('hide');
            $('#CaseEmailModal').modal('show');
            $("#mailType").val("reply"), e.internal_party.length > 0 && $("#whoto1").val(e.internal_party).trigger("change"), "" != e.external_party && $("#emails1").val(e.external_party), $("#mail_subject1").val(e.emails[0].subject), $("#mail_urgent1").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent1").iCheck("check"), $("#case_no1").val((e.emails[0].caseno) != '0' ? e.emails[0].caseno : '');
            if (CKEDITOR.instances.mailbody1)
                    CKEDITOR.instances.mailbody1.destroy();
            CKEDITOR.replace('mailbody1');
            CKEDITOR.instances.mailbody1.setData(e.emails[0].body);
        }
    });
});

var copy = function(elementId) {
    var flag = '';
    var tempInput = document.getElementById('temp-copy-elem');
    var isiOSDevice = navigator.userAgent.match(/ipad|iphone/i);

    if (isiOSDevice) {
        var editable = tempInput.contentEditable;
        var readOnly = tempInput.readOnly;
        tempInput.contentEditable = true;
        tempInput.readOnly = false;
        var range = document.createRange();
        range.selectNodeContents(document.getElementById(elementId));
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        tempInput.setSelectionRange(0, 999999);
        tempInput.contentEditable = editable;
        tempInput.readOnly = readOnly;
        flag = document.execCommand('copy');
    } else {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($('#'+elementId).text()).select();
        flag = document.execCommand("copy");
        $temp.remove();
    }
    document.getElementById('temp-copy-elem').remove();
    if(flag == true) {
        Command: toastr["success"]("Copied to Clipboard..");
    } else {
        Command: toastr["error"]("Failed to get copied..");
    }
}

$(document).on('click', '.clone-address', function() {
    var html = '<input type="text" id="temp-copy-elem" value="' + $('#party_address1').html() + '" style="opacity: 0; width: 0px; height: 0px;">';
    $(this).prev().append(html);
    copy('party_address1');
});

</script>