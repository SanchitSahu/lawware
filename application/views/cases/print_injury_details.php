
<html>
   <head>
   <style>
   @media print {
   a[href]:after {
   main: " (" attr(href) ")";
   }
	body{
		font-family: "Times New Roman", Times, serif;
		
	}
	input ,.font-b{
		font-family: Arial, Helvetica, sans-serif;
		font-weight:bold;
      font-size:12px;
      line-height:12px;     
	}
	span,.font-a{
		font-family: Arial, Helvetica, sans-serif;
		font-size:6px;
	}
	
   }
</style>
   </head>
   <body style="font-family: 'Source Sans Pro', sans-serif;max-width: 1000px;margin:0px 10px auto;font-size: 18px;  page-break-inside : avoid;">
      <div class="main">
		<div class="head" style="text-align: center;  ">
            <h3 style="font-weight: normal;font-size: 12px;margin-bottom: 3px;line-height: 12px;margin-top: 0px;" class="injury_datail">STATE
                OF CALIFORNIA</h3>
            <h3 style="font-weight: normal;font-size: 12px;margin-bottom: 3px;margin-top: 0px;line-height: 12px;">DEPARTMENT OF
					 INDUSTRIAL RELATIONS</h3>
					
            <h1 style="font-size: 25px;margin-bottom:15px;margin-top: 10px;position: relative;line-height: 12px;">WORKERS'
					 COMPENSATION APPEALS BOARD  <span style="position:absolute; right:0px;bottom:0px;text-transform: uppercase; font-size:12px;">see reverse side<br/> for instructions</span></h1>
					
					
        </div>
         <div class="body" style="top:0px; ">
		
        
            <div style="display: inline-block;width: 100%;position: relative;  ">
               <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;float: left;font-weight: bold;font-size: 15px; position: relative;">APPLICATION FOR ADJUDICATION OF CLAIM<br><span style="font-size:12px;line-height:13px;">(PRINT OR TYPE NAMES AND ADDRESSES)</span>&nbsp;&nbsp;&nbsp;&nbsp;
               <?php $amendedApp = json_decode(amendedApplication); 
                     foreach($amendedApp as $key => $value){
                         if($key == $injury->amended){$amended = $value;}
                     }?>
               <span style="margin-bottom: 5px;margin-top: 0px;float: right;font-weight: bold;font-size: 22px; font-family: 'Times New Roman', Times, serif !important;position: absolute; bottom:-20px; right:-120px;"><?php echo $amended;?></span></p>
               <?php } ?>
               <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?><span style="font-size: 15px;float: right;position: absolute;right: 0;top: 25px;"></span><?php } ?>
               <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;float: right;font-weight: bold;font-size: 15px;">CASE No. <?php }?>
                  <input type="text" name="caseno" value="<?php echo (isset($injury->case_no))?$injury->case_no:'';?>" style="width: 300px;border: none;border-bottom: 1px solid;text-align:left " /> 
               </p>
            </div>
            <div style="float: left;width: 50%;" >
               <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;display: inline-block !important;font-size: 16px;">M</p>
               <?php } ?>
               <p style="margin-bottom: 0px;margin-top: 0px;display: inline-block  !important;position: relative;">
                  <input type="text" value="<?php echo (isset($injury->first))?$injury->first:'';?><?php echo (isset($injury->last))?$injury->last:'';?>" style="width: 420px;border: none;border-bottom: 1px solid; " />
               </p>
               <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
               <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block">Social Security No.:</p>
               <?php } ?>
               <p style="margin-bottom: 0px;margin-top: 0px;display: inline-block;position: relative;">
                  <input type="text" value="<?php echo (isset($injury->social_sec))?$injury->social_sec:'';?>" style="width: 310px;border: none;border-bottom: 1px solid;text-align:left " />
               </p>
               <p style="margin-bottom: 0px;margin-top: 0px;"> <input type="text"  value=""  style="width: 435px;border: none;border-bottom: 1px solid; " /></p>
               <p  class="font-a" style="margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;" > <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>(APPLICANT, IF OTHER THAN INJURED EMPLOYEE)<br>VS.<?php } ?></p>
               <p style="margin-bottom: 0px;margin-top: 0px;">
                  <input type="text" value="<?php echo (isset($injury->e_name))?$injury->e_name:'';?>"  style="width: 435px;border: none;text-align:left " />
                  <input type="text" value="<?php echo (isset($injury->e2_name))?$injury->e2_name:'';?>"  style="width: 435px;border: none;border-bottom: 1px solid;text-align:left " />
               </p>
               <p   class="font-a" style="margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;"> <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>(EMPLOYER - STATE IF SELF-INSURED)<?php } ?></p>
               <p style="margin-bottom: 0px;margin-top: 0px;"> <input type="text" value="<?php echo (isset($injury->i_name))?$injury->i_name:'';?>" style="width: 435px;border: none;text-align:left " />
               <input type="text" value="<?php echo (isset($injury->i2_name))?$injury->i2_name:'';?>" style="width: 435px;border: none;border-bottom: 1px solid;text-align:left " />
               </p>
               <p  class="font-a" style="margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;"><?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>(EMPLOYER'S INSURANCE CARRIER OR, IF SELF-INSURED, ADJUSTING AGENCY)<?php  } ?></p>
            </div>
            <div style="float: right;width: 50%; position:absolute; top:142px; right:20px;" >
               <p style="margin-bottom: 0px;margin-top: 0px;">
                  <input type="text" value="<?php echo (isset($injury->address))?$injury->address:'';?>" style="margin-left: 50px;width: 450px;border: none;" />
                  <input type="text" value="<?php echo (isset($injury->city))?$injury->city:'';?><?php echo (isset($injury->state))?' '.$injury->state:'';?><?php echo (isset($injury->zip_code))?' '.$injury->zip_code:'';?> " style="margin-left: 50px;width: 450px;border: none;border-bottom: 1px solid; " />
               </p>
               <p class="font-a" style="margin-top: 0px;font-size: 8px;text-align: center;">
                  <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?> (INJURED EMPLOYEE'S ADDRESS AND ZIP CODE)<?php } ?>
               </p>
               <p style="margin-top: -9px; position:relative;">
                  <input type="text" value="" style="margin-left: 50px;width: 450px;border: none;border-bottom: 1px solid; position:absolute;margin-top: -9px;" />
               </p>
               <p  style="margin-bottom: 0px;margin-top:10px; padding-top:10px;">
                  <input type="text" value="" style="margin-left: 50px;width: 450px;border: none;border-bottom: 1px solid;padding-top:10px;" />
               </p>
               <p class="font-a" style="margin-top: 0px;font-size: 8px;text-align: center;">  <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?> (APPLICANT'S ADDRESS AND ZIP CODE)<?php } ?></p>
               <p style="margin-bottom: 0px;margin-top: 0px;">
                  <?php
                    if(empty($injury->e2_address)&&empty($injury->e2_city)&&empty($injury->e2_state)&&empty($injury->e2_zip)){
                  ?>
                  <input type="text" value="<?php echo (isset($injury->e_address))?$injury->e_address:'';?>" style="margin-left: 50px;width: 450px;border: none;" />
                  <input type="text" value="<?php echo (isset($injury->e_city))?$injury->e_city:'';?><?php echo (isset($injury->e_state))?' '.$injury->e_state:''?><?php echo (isset($injury->e_zip))?' '.$injury->e_zip:''?>" style="margin-left: 50px;width: 450px;border: none;border-bottom: 1px solid; " />
                    <?php }else{?>
                        <input type="text" value="<?php echo (isset($injury->e_address))?$injury->e_address:'';?> <?php echo (isset($injury->e_city))?' '.$injury->e_city:'';?><?php echo (isset($injury->e_state))?' '.$injury->e_state:''?><?php echo (isset($injury->e_zip))?' '.$injury->e_zip:''?>" style="margin-left: 50px;width: 450px;border: none;" /> 
                       <input type="text" value="<?php echo (isset($injury->e2_address))?$injury->e2_address:'';?> <?php echo (isset($injury->e2_city))?' '.$injury->e2_city:'';?><?php echo (isset($injury->e2_state))?' '.$injury->e2_state:''?><?php echo (isset($injury->e2_zip))?' '.$injury->e2_zip:''?>" style="margin-left: 50px;width: 450px;border: none;border-bottom: 1px solid;" /> 
                   <?php }
                    ?>
               </p>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;">  <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?> (EMPLOYER'S ADDRESS AND ZIP CODE)<?php } ?></p>
               <p style="margin-bottom: 0px;margin-top: 0px;">
                  <input type="text" value="<?php echo (isset($injury->i_address))?$injury->i_address:'';?>" style="margin-left: 50px;width: 450px;border: none;" />
                  <input type="text" value="<?php echo (isset($injury->i_city))?$injury->i_city:'';?><?php echo  (isset($injury->i_state))?' '.$injury->i_state:''?><?php echo  (isset($injury->i_zip))?' '.$injury->i_zip:''?>" style="margin-left: 50px;width: 450px;border: none;border-bottom: 1px solid; " />
               </p>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;">  <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?> (INSURANCE CARRIER OR ADJUSTING AGENCY'S ADDRESS)<?php } ?></p>
            </div>
        			<div style="display: inline-block;width: 100%;padding: 0 15px;box-sizing: border-box; margin-top:30px;" >
		
              
               <ol style="padding: 0px;list-style-position: outside;">
                  <li>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;"><?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>The injured employee, born<?php } ?></p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text"  value="<?php echo (isset($injury->adj1b) && !empty($injury->adj1b))?date('m/d/Y',strtotime($injury->adj1b)):'';?>"style="width: 200px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(DATE OF BIRTH)<?php } ?></span>
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">while employed as a</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj1a))?$injury->adj1a:'';?>" style="width: 315px;border: none;border-bottom: 1px solid; " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(OCCUPATION AT TIME OF INJURY)<?php } ?></span>
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">on</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj1c))?$injury->adj1c:'';?>" style="width: 300px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(DATE OF INJURY)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">at</p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj1d))?'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$injury->adj1d:'';?><?php  echo (isset($injury->adj1d2))?'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$injury->adj1d2:'';?><?php echo (isset($injury->adj1d3))?'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$injury->adj1d3:'';?><?php echo (isset($injury->adj1d4))?'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$injury->adj1d4:'';?>" style="width: 460px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(ADDRESS)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(CITY)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(STATE)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ZIP CODE)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 10px;display: inline-block;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	By the employer sustained injury arising out of and in the course of employment to<?php } ?></p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj1e))?$injury->adj1e:'';?>"  style="width: 100%;border: none;border-bottom: 1px solid;text-align:center " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;"><?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>(STATE WHAT PARTS OF BODY WERE INJURED)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>		
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">The injury ocurred as follows:</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text"  value="<?php echo (isset($injury->adj2a))?$injury->adj2a:'';?>" style="width: 647px;border: none;border-bottom: 1px solid;text-align:center" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(EXPLAIN WHAT EMPLOYEE WAS DOING AT TIME OF INJURY AND HOW INJURY WAS RECEIVED)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	 
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">Actual earnings at time of injury were:</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj3a))?$injury->adj3a:'';?>" style="width: 590px;border: none;border-bottom: 1px solid; " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(GIVE WEEKLY OR MONTHLY SALARY OF HOURLY RATE AND NUMBER OF HOURS WORKED PER WEEK)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: block;position: relative;">
                        <input type="text" value="" style="width: 100%;border: none;border-bottom: 1px solid;text-align:center" />
                        <span style="position: absolute;margin-bottom: 7px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(SEPARATELY STATE VALUE PER WEEK OR MONTH OF TIPS, MEALS, LODGING OR OTHER ADVANTAGES REGULARLY RECEIVED)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>		
                     <p style="margin-bottom: 7px;margin-top: 5px;display: inline-block;">The injury caused disability as follows:</p>
                     <?php } ?>
                     <p style="margin-bottom: 7px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj4a))?$injury->adj4a:'';?>" style="width: 583px;border: none;border-bottom: 1px solid;text-align:center " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(SPECIFY LAST DAY OFF WORK DUE TO THIS INJURY AND BEGINNING AND ENDING DATES OF ALL PERIODS OFF DUE TO THIS INJURY)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>		
                     <p style="margin-bottom: 7px;margin-top: 0px;display: inline-block;">Compensation was paid</p>
                     <?php } ?>
                     <p style="margin-bottom: 7px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj5a))?($injury->adj5a=='Y')?$injury->adj5a:'':'';?>" style="margin: 0 10px;width: 50px;border: none;border-bottom: 1px solid; " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(YES) <?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj5a))?($injury->adj5a=='N')?$injury->adj5a:'':'';?>" style="margin: 0 10px;width: 50px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(NO)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;margin-left: 20px;">$</p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text"  value="<?php echo (isset($injury->adj5b))?$injury->adj5b:'';?>" style="width: 110px;border: none;border-bottom: 1px solid; " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(TOTAL PAID) <?php }?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;margin-left: 10px;">$</p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj5c))?$injury->adj5c:'';?>" style="width: 110px;border: none;border-bottom: 1px solid; " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(WEEKLY RATE)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj5d))?$injury->adj5d:'';?>"  style="width: 250px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(DATE OF LAST PAYMENT)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 0px;margin-top: 0px;display: block;">Unemployment insurance or unemployment compensation disability benefits have been received since the date of injury</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj6a))?($injury->adj6a=='Y')?$injury->adj6a:'':'';?>" style="margin: 0 10px;width: 50px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(YES)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj6a))?($injury->adj6a=='N')?$injury->adj6a:'':'';?>" style="margin: 0 10px;width: 50px;border: none;border-bottom: 1px solid; " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;"> <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>(NO)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">Medical treatment was received</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj7a))?($injury->adj7a=='Y')?$injury->adj7a:'':'';?>" style="margin: 0 10px;width: 50px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(YES)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj7a))?($injury->adj7a=='N')?$injury->adj7a:'':'';?>" style="margin: 0 10px;width: 50px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(NO)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text"  value="<?php echo (isset($injury->adj7b) && !empty($injury->adj7b))?date('m/d/Y',strtotime($injury->adj7b)):'';?>" style="margin: 0 10px;width: 240px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(DATE OF LAST TREATMENT)<?php } ?></span>
                        <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 10px;display: inline-block;">All treatment was furnished by</p>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">the Employer or Insurance Company</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj7c))?($injury->adj7c=='Y')?$injury->adj7c:'':'';?>" style="margin: 0 10px;width: 30px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>(YES)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj7c))?($injury->adj7c=='N')?$injury->adj7c:'':'';?>" style="margin: 0 10px;width: 30px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(NO)<?php } ?></span>
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">Other treatment was provided or paid for by</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="" style="width: 180px;border: none;border-bottom: 1px solid;" />
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj7d))?$injury->adj7d:'';?>" style="width: 590px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(NAME OF PERSON OR AGENCY PROVIDING OR PAYING FOR MEDICAL CARE)<?php } ?></span>
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">Did Medi-Cal pay for any health care</p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">related to this claim</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj7e))?($injury->adj7e=='Y')?$injury->adj7e:'':'';?>" style="margin: 0 4px;width: 50px;border: none;border-bottom: 1px solid; " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(YES)<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj7e))?($injury->adj7e=='N')?$injury->adj7e:'':'';?>"  style="margin: 0 3px;width: 50px;border: none;border-bottom: 1px solid;text-align:center " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(NO)<?php } ?></span>
                        <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">doctors not provided or paid for by employer or insurance company  who treated or</p>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;"> examined for this injury are</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->doctors))?$injury->doctors:'';?>" style="width: 660px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(STATE NAMES AND ADDRESSES OF SUCH DOCTORS AND NAMES OF HOSPITALS TO WHICH SUCH DOCTORS ADMITTED INJURIES)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 7px;margin-top:5px;display: block;">Other cases have been filed for industrial injuries by this employee as follows:</p>
                     <?php } ?>
                     <p style="margin-bottom: 7px;margin-top: 0px;display: block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj8a))?$injury->adj8a:'';?>"  style="width: 100%;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(SPECIFY CASE NUMBER AND CITY WHERE FILED)<?php } ?></span>
                     </p>
                  </li>
                  <li>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">This application is filed because of a disagreement regarding liability for:</p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;margin-left: 85px;">Temporary disability indemnity</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj9a))?$injury->adj9a:'';?>"  style="width: 50px;border: none;border-bottom: 1px solid;" />
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">Permanent disability indemnity</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj9b))?$injury->adj9b:'';?>" style="width: 50px;border: none;border-bottom: 1px solid;" />
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;margin-left: 30px;">Reimbursement for medical expense</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj9c))?$injury->adj9c:'';?>" style="width: 50px;border: none;border-bottom: 1px solid;" />
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;margin-left: 30px;">Medical Treatment</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj9d))?$injury->adj9d:'';?>" style="width: 50px;border: none;border-bottom: 1px solid;text-align:center " />
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">Compensation at proper rate</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value=""style="width: 50px;border: none;border-bottom: 1px solid;text-align:center" />
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;margin-left: 30px;">Rehabilitation</p>
                     <?php  } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="" style="width: 50px;border: none;border-bottom: 1px solid;text-align:center " />
                     </p>
                     <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;margin-left: 30px;">Other (Specify)</p>
                     <?php } ?>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="<?php echo (isset($injury->adj9g))?$injury->adj9g:'';?>" style="width: 277px;border: none;border-bottom: 1px solid;text-align:center " />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	AND APPLICANT REQUESTS A HEARING AND AWARD OF<?php } ?></span>
                     </p>
                     <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                        <input type="text" value="" style="width: 680px;border: none;border-bottom: 1px solid;" />
                        <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	THE SAME, AND FOR ALL OTHER APPROPRIATE BENEFITS PROVIDED BY LAW.<?php } ?></span>
                     </p>
                  </li>
               </ol>
            </div>
            <div style="padding-top: 5px; display: inline-block;width: 100%;">
               <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
               <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">Dated at</p>
               <?php } ?>
               <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                  <input type="text" value="<?php echo (isset($injury->adj10city))?$injury->adj10city:'';?>" style="width: 300px;border: none;border-bottom: 1px solid;" />
                  <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(CITY)<?php } ?></span>
               </p>
               <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>
               <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">California,</p>
               <?php } ?>
               <p style="margin-bottom: 5px;margin-top: 0px;display: inline-block;position: relative;">
                  <input type="text" value="<?php echo (isset($injury->adj10a))?date('m-d-Y',strtotime($injury->adj10a)):'';?>" style="width: 300px;border: none;border-bottom: 1px solid;text-align:center " />
                  <span style="position: absolute;margin-bottom: 0px;margin-top: 0px;font-size: 8px;text-align: center;left: 0;right: 0;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(DATE)<?php } ?></span>
               </p>
            </div>
            <div style="padding-top: 5px; float: left;width: 50%;">
               <p style="margin-bottom: 0px;margin-top: 5px;"> <input type="text" value=""  style="width: 500px;border: none;border-bottom: 1px solid; " /></p>
               <p class="font-a" style="margin-bottom: 10px;margin-top: 0px;font-size: 8px;text-align: center;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(APPLICANT'S ATTORNEY)<?php } ?></p>
               <!-- <p style="margin-bottom: 0px;margin-top: 5px;"> <input type="text"  value="<?php echo (isset($injury->d1_address))?$injury->d1_address:'';?> <?php echo (isset($injury->d1_phone))?$injury->d1_phone:'';?> " style="width: 500px;border: none;border-bottom: 1px solid;text-align:center" /></p> -->
               <p style="margin-bottom: 0px;margin-top: 5px; width: 500px;border: none;border-bottom: 1px solid;" class="font-b"><?php echo (isset($attorney))?$attorney:'';?>
                                      <br/> 14555 Sylvan St.
                                       <br/>Straussner Sherman
                                       <br/> Van Nuys, CA  91411-2325,  (818) 788-1700
               </p>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;"><?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>(ADDRESS AND TELEPHONE NUMBER OF ATTORNEY)<?php } ?></p>
            </div>
            <div style="padding-top: 5px; float: right;width: 50%;">
               <p style="margin-bottom: 0px;margin-top: 5px;"> <input type="text" value="<?php echo (isset($injury->first))?$injury->first:'';?><?php echo (isset($injury->last))?$injury->last:'';?>" style="margin-left: 50px;width: 450px;border: none;border-bottom: 1px solid;" /></p>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;font-size: 8px;text-align: center;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	(APPLICANT'S SIGNATURE)<?php } ?></p>
            </div>
            <div style="display: inline-block;width: 100%;margin-top: 5px;">
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;display: inline-block;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	DIA WCAB FORM 1 (REV. 7/81) <?php } ?><br> <span style="margin-bottom: 0px;margin-top: 0px;font-size: 8px;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?> Law<?php } ?></span></p>
               <p class="font-a" style="margin-bottom: 5px;margin-top: 0px;display: inline-block;float: right;">   <?php if(isset($form_data_type) && $form_data_type!='data-only') { ?>	84 32595 <?php } ?> </p>
            </div>
         </div>
      </div>
   </body>
</html>