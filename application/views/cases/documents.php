<style>#applicantImage { background: #4d7ab1;font-size: 35px;color: #fff;width: 100px;height: 100px;text-align: center;line-height: 100px;display: inline-block;}</style>
<input type="hidden" value="<?php echo $this->uri->segment(3); ?>" id="caseNo">
<input type="hidden" value="<?php echo $display_details->case_status; ?>" id="caseStatus">
<input type="hidden" value="<?php echo isset($display_details->social_sec) ? $display_details->social_sec : '' ?>" id="social_sec">
<?php
$applicant_name = $display_details->salutation;
if ($display_details->first != '') {
    $applicant_name .= ' ' . $display_details->first;
}
if ($display_details->middle != '') {
    $applicant_name .= ' ' . $display_details->middle;
}
if ($display_details->last != '') {
    $applicant_name .= ' ' . $display_details->last;
}

if (isset($display_details->popular) && !empty($display_details->popular)) {
    if (strlen($display_details->popular) < 10) {
        $popular_name = '<span class = "f-size12 marg-bot5 popular"><b>goes by ' . $display_details->popular . '</b></span><br/>';
    } else {
        $popular_name = '<span class = "f-size12 marg-bot5 popular"><b>goes by ' . substr(0, 10, $display_details->popular) . '..</b></span><br/>';
    }
} else {
    $popular_name = '';
}

$applicant_address = '';
$applicant_address2 = '';
if ($display_details->address1 != '' || $display_details->address2 != '' || $display_details->city != '' || $display_details->state != '') {
    $applicant_address = ($display_details->address1 != '') ? ($display_details->address1 . ', ') : '';
    $applicant_address = ($display_details->address2 != '') ? (($applicant_address == '') ? $display_details->address2 . ', ' : $applicant_address . $display_details->address2 . ', ') : $applicant_address;
    $applicant_address = trim($applicant_address, ', ');
}

$applicant_address2 = ($display_details->city != '') ? (($applicant_address2 == '') ? $display_details->city . ', ' : $applicant_address2 . $display_details->city . ', ') : $applicant_address2;
$applicant_address2 = ($display_details->state != '') ? (($applicant_address2 == '') ? $display_details->state . ' ' : $applicant_address2 . $display_details->state . ' ') : $applicant_address2;
$applicant_address2 = ($display_details->zip != '') ? (($applicant_address2 == '') ? $display_details->zip . ', ' : $applicant_address2 . $display_details->zip . ', ') : $applicant_address2;
$applicant_address2 = trim($applicant_address2, ', ');

$opponent_name = '';
if (count($users) > 0) {
    if (isset($users['client'])) {
        $opponent_name = $users['client']->salutation;
        if ($users['client']->first != '') {
            $opponent_name .= ' ' . $users['client']->first;
        }
        if ($users['client']->middle != '') {
            $opponent_name .= ' ' . $users['client']->middle;
        }
        if ($users['client']->last != '') {
            $opponent_name .= ' ' . $users['client']->last;
        }
    }
}
$birthDate = date('m/d/Y', strtotime(isset($Prospect_data->birthdate) ? $Prospect_data->birthdate : 0));
$birthDate = explode("/", $birthDate);
$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
$filesudf = isset($Prospect_data->fieldsudf) ? $Prospect_data->fieldsudf : '';
$filesudf = explode(',', $filesudf);
?>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="breadcrumbs col-lg-4">
                    <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                    <a href="<?php echo base_url('cases'); ?>">Cases</a> /
                    <a href="<?php echo base_url('cases/case_details') . '/' . $case_no; ?>"><?php echo $case_no; ?></a> /
                    <span>Documents</span>
                </div>
                <div class="col-lg-8">
                    <?php
                    if (isset($birthdaynotification) && !empty($birthdaynotification)) {
                        $bnoti = json_decode($birthdaynotification, true);
                        echo "<span class='bnoti'><b>" . $bnoti['title'] . "</b> " . $bnoti['type1'] . "</span>";
                    }
                    ?>
                </div>

            </div>
            <input type="hidden" name="hdnCaseId" id="hdnCaseId" value="<?php echo $case_id; ?>" />
            <div class="row">
                <div class="col-sm-12" id="maincaption">
                    <?php if ($display_details->case_status == '1') {
                        ?>
                        <div class="sticky fadeIn animated marg-bot15 caption caption-opened" title="<?php echo $display_details->caption1; ?>">
                            <span id="caption_details">
                                <?php
                                if (strlen($display_details->caption1) > 55)
                                    echo substr($display_details->caption1, 0, 55) . '...';
                                else
                                    echo substr($display_details->caption1, 0, 55);
                                ?>
                            </span>
                            <?php
                            if ($CaptionAccess->captionAccess == 1) {
                                ?>
                                <span style="position: absolute;top: 10px;right: 20px;"><a data-tab="caption_tab" class="pull-right ajaxdropdowncase" data-target="#editcasedetail"><i class="fa fa-pencil-square-o"></i></a></span>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } else {
                        ?>
                        <div class="sticky fadeIn animated marg-bot15 caption caption-closed" title="<?php echo $display_details->caption1 . "- DELETED"; ?>">
                            <span id="caption_details">
                                <?php
                                if (strlen($display_details->caption1) > 50)
                                    echo substr($display_details->caption1, 0, 50) . '...' . "- DELETED";
                                else
                                    echo substr($display_details->caption1, 0, 50) . "- DELETED";
                                ?>
                            </span>
                            <span style="position: absolute;top:10px;right: 25px;"><a data-tab="caption_tab" class="pull-right ajaxdropdowncase" data-target="#editcasedetail"><i class="icon-edit fa-5x"></i></a></span>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="col-sm-12" id="captiontext" style="display:none;">
                    <input type="text" id="caption_text" name="caption_text" value="<?php echo $display_details->caption1; ?>" style="width: 100%;padding-left: 30px;text-align: center;font-size: 21px;">
                </div>
            </div>
            <div class = "row">
                <div id="casedetails" class="tab-pane active">
                    <div class = "panel-body documentcasebox">
                        <div class = "row">
                            <div class = "col-lg-12">
                                <div class="ibox">
                                    <div class = "ibox-title">
                                        <h5>Applicant Details</h5>
                                        <!--a class="edit-card pull-right ajaxdropdowncase" href="#addcontact" data-cardcode="<?php echo $display_details->cardcode; ?>">Edit</a-->
                                    </div>
                                    <div class = "ibox-content marg-bot15">
                                        <div class = "row">
                                            <div class = "col-sm-2">
                                                <div class = "client-img marg-bot15" >
                                                    <?php
                                                    if (isset($display_details->picture)) {
                                                        $url = FCPATH . 'assets/rolodexprofileimages/' . $display_details->picture;
                                                        if (file_exists($url) && $display_details->picture != '') {
                                                            ?>
                                                            <img class = "img-circle" height="30" width="30" src = "<?php echo base_url('assets/rolodexprofileimages') . '/' . $display_details->picture; ?>" style="width:60px;height:60px;"/>

                                                        <?php } else { ?>
                                                            <input type="hidden" id="applicant_ini" value="<?php echo $display_details->first; ?>">
                                                            <div id="applicantImage" height="50" width="50" class="img-circle"></div>

                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <input type="hidden" id="applicant_ini" value="<?php echo $display_details->first; ?>">
                                                        <div id="applicantImage" height="67" width="67" class="img-circle"></div>

                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class = "col-sm-4">
                                                <div class = "client-personal">
                                                    <span class = "f-size17 marg-bot5 applicant_name"><b><?php echo $applicant_name; ?></b></span><br/>
                                                    <?php echo $popular_name; ?>
                                                    <?php if ($applicant_address != '') { ?>
                                                        <span class="marg-bot5 applicant_address f-size15"><?php echo $applicant_address; ?></span><br/>
                                                        <span class="marg-bot5 applicant_address2 f-size15"><?php echo $applicant_address2; ?></span><br/>
                                                        <span class="marg-bot5 f-size15"><a target="_blank" id="address_map" href="https://maps.google.com/?q=<?php echo $applicant_address . $applicant_address2; ?>">View on map</a></span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <span class="marg-bot5 f-size15"><i class="icon fa fa-phone pull-left marg-top2"></i> &nbsp; (H)<span class="home"> <?php echo ($display_details->home != '') ? $display_details->home : 'NA'; ?></span>
                                                </span>
                                                <br />
                                                <span class="marg-bot5 f-size15 emailalignment"><i class="icon fa fa-envelope pull-left marg-top2"></i>&nbsp;<span class="documentemail"> <?php echo ($display_details->email != '') ? $display_details->email : 'NA'; ?></span></span>
                                                <br />
                                                <span class="marg-bot5 interpret"><b>Interp </b>
                                                    <?php echo ($display_details->interpret != '') ? $display_details->interpret : 'NA'; ?>
                                                    <?php echo ($display_details->language != '') ? $display_details->language : 'NA'; ?>
                                                </span>
                                            </div>
                                            <div class="col-sm-3">
                                                 <?php if($display_details->car != ''){ ?>
                                                    <span class="marg-bot5 f-size15"><i class="icon fa fa-phone car pull-left marg-top2"></i> &nbsp; (C)<span class="business" > <?php echo ($display_details->car != '') ? $display_details->car : 'NA'; ?></span></span>
                                                    <?php }
                                                    elseif($display_details->phone1 != ''){ ?>
                                                    <span class="marg-bot5 f-size15"><i class="icon fa fa-phone car pull-left marg-top2"></i> &nbsp; (B)<span class="business" > <?php echo ($display_details->phone1 != '') ? $display_details->phone1 : 'NA'; ?></span></span>
                                                    <?php }elseif($display_details->business != ''){ ?>
                                                    <span class="marg-bot5 f-size15"><i class="icon fa fa-phone car pull-left marg-top2"></i> &nbsp; (B)<span class="business" > <?php echo ($display_details->business != '') ? $display_details->business : 'NA'; ?></span></span>
                                                    <?php } ?>
<!--                                                <span class="marg-bot5 f-size15"><i class="icon fa fa-phone car pull-left marg-top2"></i> &nbsp; (B)<span class="business" > <?php echo ($display_details->business != '') ? $display_details->business : 'NA'; ?></span></span>-->
                                                <br />
                                                <span class="marg-bot5 f-size15"><i class="icon fa fa-calendar pull-left  marg-top2"></i>&nbsp;
                                                    <?php if ($display_details->birth_date != '' && $display_details->birth_date != '1970-01-01 00:00:00') { ?>
                                                        <span class="birth_date f-size15"> Born on <?php echo date('m/d/Y', strtotime($display_details->birth_date)); ?>, <?php echo 'Age: '.($this->common_functions->get_timeago(strtotime(date("m/d/Y", strtotime($display_details->birth_date))))); ?></span>
                                                        <?php
                                                    } else {
                                                        echo '<span class="birth_date">NA</span>';
                                                    }
                                                    ?>
                                                </span>
                                                <br />
                                                <span class="marg-bot5 interpret f-size15"><i class="icon fa fa-key pull-left marg-top2"></i>&nbsp; <b>SS</b> <?php echo ($display_details->social_sec != '') ? $display_details->social_sec : 'NA'; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="document" class="tab-pane">
                    <div class="panel-body documentcasebox">
                        <div class="ibox float-e-margins">
                            <div class="">
                                <div class="ibox-title">
                                    <h5 class="marg-top5">Forms</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#saved-documents1">Saved Documents</a></li>
                                            <li class=""><a data-toggle="tab" href="#a1court-forms1">Court Forms</a></li>
                                            <li class=""><a id="printletters" data-toggle="tab" href="#print-letters1">Print Letters</a></li>
                                            <!-- <li class=""><a data-toggle="tab" href="#edit-letters1">Edit Letters</a></li> -->
                                            <li class=""><a data-toggle="tab" href="#pension-forms">Pension Letters</a></li>

                                        </ul>
                                        <div class="tab-content">
                                            <div id="a1court-forms1" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Court Forms</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-lg-5">
                                                                        <select class="form-control" id="A1forms" name="A1forms" multiple style="width: 100%;">
                                                                            <?php foreach ($form_name as $key => $val) { ?>
                                                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                        <div class="form-control" id="A1forms2" name="A1forms2" >
                                                                            <?php echo count($print_letters) ?> Forms
                                                                        </div>
                                                                        <div class="form-control" id="A1forms1" name="A1forms1"  style="width: 100%;height: 100px;">
                                                                        </div>
                                                                        <div class="form-control showsearch" style="width: 100%;height: 100px; font-size:15px; margin-top:10px;">Showing forms containing <input type="text" name="search_form" id="search_form" class="form-control"> Example (Petition, order, stip, etc)
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-7">
                                                                        <div id='main' style="height:400px;">
                                                                            <ul class='list0001'>

                                                                                <?php
                                                                                foreach ($print_letters as $key => $val) {
                                                                                    if ($val['category'] != 'workers comp' && $val['category'] != 'pension') {
                                                                                        ?>

                                                                                        <li><?php if ($val['modalType'] != '') { ?>
                                                                                                <a href="#" class="viewDocForm" data-toggle="modal" data-number="<?php echo $val['number']; ?>" data-prefix="<?php echo $val['prefix']; ?>" id="<?php echo $val['filename']; ?>" data-target="#<?php echo $val['modalType']; ?>"><?php echo ucwords(strtolower($val['title'])); ?></a>
                                                                                            <?php } else { ?>
                                                                                                <a class="createDoc" data-number="<?php echo $val['number']; ?>" id="<?php echo $val['filename']; ?>" data-prefix="<?php echo $val['prefix']; ?>" data-cardcode="<?php echo $display_details->cardcode; ?>"><?php echo ucwords(strtolower($val['title'])); ?></a>
                                                                                            <?php } ?>
                                                                                        </li>

                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="print-letters1" class="tab-pane ">
                                                <div class="panel-body">
                                                    <div class="float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Print Letters</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="table-responsive">
                                                                <table id="form-letter2" class="table table-striped table-bordered nowrap dataTables-example" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="10%">Number</th>
                                                                            <th width="10%">Group</th>
                                                                            <th width="75%" >Title</th>
                                                                            <!-- <th width="15%">Action</th> -->
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        foreach ($print_letters as $key => $val) {
                                                                            if ($val['category'] == 'workers comp') {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?php echo $val['number']; ?></td>
                                                                                    <td><?php echo $val['group']; ?></td>
                                                                                    <!-- <td><?php echo $val['title']; ?></td> -->
                                                                                    <td><?php if ($val['modalType'] != '') { ?>
                                                                                            <a href="#" class="viewDocForm" data-toggle="modal" id="<?php echo $val['filename']; ?>" data-prefix="<?php echo $val['prefix']; ?>" data-number="<?php echo $val['number']; ?>" data-target="#<?php echo $val['modalType']; ?>"><?php echo $val['title']; ?></a>
                                                                                        <?php } else { ?>
                                                                                            <a class="createDoc" id="<?php echo $val['filename']; ?>" data-number="<?php echo $val['number']; ?>" data-prefix="<?php echo $val['prefix']; ?>" data-cardcode="<?php echo $display_details->cardcode; ?>"><?php echo $val['title']; ?></a>
                                                                                        <?php } ?>

                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div id="edit-letters1" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Edit Letters</h5>
                                                        </div>
                                                        <div class="ibox-content form-view">
                                                            <div class="table-responsive">
                                                                <table id="form-letter4" class="table table-striped table-bordered nowrap dataTables-example" cellspacing="0" width="100% !important;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="10%">Number</th>
                                                                            <th width="10%">Group</th>
                                                                            <th width="75%" >Title</th> -->
                                                                            <!-- <th width="15%">Action</th> -->
                                                                        <!-- </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        foreach ($print_letters as $key => $val) {
                                                                            if ($val['category'] == 'workers comp') {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?php echo $val['number']; ?></td>
                                                                                    <td><?php echo $val['group']; ?></td> -->
                                                                                    <!-- <td><?php echo $val['title']; ?></td> -->
                                                                                    <!-- <td><?php if ($val['modalType'] != '') { ?>
                                                                                            <a href="#" class="viewDocForm" data-toggle="modal" id="<?php echo $val['filename']; ?>" data-prefix="<?php echo $val['prefix']; ?>" data-number="<?php echo $val['number']; ?>" data-target="#<?php echo $val['modalType']; ?>"><?php echo $val['title']; ?></a>
                                                                                        <?php } else { ?>
                                                                                            <a class="createDoc" id="<?php echo $val['filename']; ?>" data-number="<?php echo $val['number']; ?>" data-prefix="<?php echo $val['prefix']; ?>" data-cardcode="<?php echo $display_details->cardcode; ?>"><?php echo $val['title']; ?></a>
                                                                                        <?php } ?>

                                                                                    </td> </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div id="pension-forms" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="float-e-margins marg-bot10">
                                                        <div class="ibox-title">
                                                            <h5>Pension Letters</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="table-responsive">
                                                                <table id="form-letter7" class="table table-striped table-bordered nowrap dataTables-example" cellspacing="0" width="100% !important;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="10%">Number</th>
                                                                            <th width="10%">Group</th>
                                                                            <th width="75%" >Title</th>
                                                                            <!-- <th width="15%">Action</th> -->
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        foreach ($print_letters as $key => $val) {
                                                                             if ($val['category'] == 'pension') {
                                                                                ?>
                                                                                <tr>
                                                                                    <td><?php echo $val['number']; ?></td>
                                                                                    <td><?php echo $val['group']; ?></td>
                                                                                    <!-- <td><?php echo $val['title']; ?></td> -->
                                                                                    <td><?php if ($val['modalType'] != '') { ?>
                                                                                            <a href="#" class="viewDocForm" data-toggle="modal" id="<?php echo $val['filename']; ?>" data-prefix="<?php echo $val['prefix']; ?>" data-number="<?php echo $val['number']; ?>" data-target="#<?php echo $val['modalType']; ?>"><?php echo $val['title']; ?></a>
                                                                                        <?php } else { ?>
                                                                                            <a class="createDoc" id="<?php echo $val['filename']; ?>" data-number="<?php echo $val['number']; ?>" data-prefix="<?php echo $val['prefix']; ?>" data-cardcode="<?php echo $display_details->cardcode; ?>"><?php echo $val['title']; ?></a>
                                                                                        <?php } ?>

                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="saved-documents1" class="tab-pane active">
                                                <div class="panel-body" style="overflow: auto;">
                                                    <div class="float-e-margins marg-bot15">
                                                        <div class="ibox-title">
                                                            <h5>Saved Documents</h5>
                                                        </div>

                                                        <div class="ibox-content"><!-- form-letter5 -->
                                                            <div class="table-responsive">
                                                                <table id="form-letter5" class="table table-striped table-bordered dataTables-example" cellspacing="0" width="100% !important;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="8%">Initials</th>
                                                                            <th width="9%">Doc No</th>
                                                                            <th width="10%">Date</th>
                                                                            <th width="60%">Event</th>
                                                                            <th width="13%">Action</th>
                                                                            <!-- <th width="6%">View</th> -->
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        foreach ($case_activity as $activity) {
                                                                            $category = json_decode(category, true);
                                                                            if ($activity->category == $category) {
                                                                                ?>

                                                                                <tr class='edit_caseAct'id="caseAct<?php echo $activity->actno ?>" style="<?php echo isset($jsoncolor_codes[$activity->color]) ? "background-color:{$jsoncolor_codes[$activity->color][2]};color: {$jsoncolor_codes[$activity->color][1]}" : "" ?>">
                                                                                    <td><?php echo $activity->initials0; ?></td>
                                                                                    <td> <?php echo $activity->actno ?> </td>
                                                                                    <td><?php echo date('m/d/Y', strtotime($activity->date)) ?></td>
                                                                                    <td class="caseact_event" style="word-wrap: break-word;"><?php echo $activity->event; ?></td>
                                                                                    <td>
                                                                                        <a class="btn btn-xs btn-info btn-circle btn-circle-action marginClass activity_edit" data-actid='<?php echo $activity->actno; ?>' href = "javascript:;" title="Edit"><i class = "icon fa fa-edit"></i> </a>
                                                                                        <a class="btn btn-xs btn-danger btn-circle btn-circle-action marginClass activity_delete" data-actid='<?php echo $activity->actno; ?>' href = "javascript:;" title="Delete"><i class = "icon fa fa-trash"></i> </a>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if (!empty($activity->filename)) { ?>
                                                                                            <a class="btn btn-xs btn-info btn-circle btn-circle-action marginClass" data-actid='<?php echo $activity->filename; ?>' target="_blank" href = "<?php echo base_url(); ?>assets/clients/<?php echo $activity->caseno; ?>/<?php echo $activity->filename; ?>" title="View"><i class = "icon fa fa-paste"></i> View <?php echo $activity->filename; ?></a>
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var formsTable = "";
    var printLetters = "";
    var pensionFormsTable = "";
    var formdatalist = <?php echo json_encode($print_letters) ?>;
    $(document).ready(function () {
        if (document.getElementById('applicant_ini')) {
            var firstName = document.getElementById('applicant_ini').value;
            var intials = firstName.charAt(0);
            var profileImage = $('#applicantImage').text(intials);
        }
        var caseno = $('#caseNo').val();

        $(document).on('shown.bs.tab', 'a[data-toggle="tab"][href="#saved-documents1"]', function (e) {
            formsTable.columns.adjust();
        });

        formsTable = $('#form-letter5').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveParams: function (settings, data) {
                data.search.search = "";
                data.start = 0;
                delete data.order;
                data.order = [1, "desc"];
            },
            scrollX: true,
            "scrollY":"400px",
            "scrollCollapse": true,
            bFilter: true,
            scrollCollapse: true,
            order: [[1, "DESC"]],
            aoColumnDefs: [
                {
                    "aTargets": [3],
                    "className":"white-space"
                    
                },
                {
                    "bSortable": false,
                    "aTargets": [4],
                    "className":"d-flex-a"
                },
                {
                    "bSearchable": false,
                    "aTargets": [4]
                }
            ], "ajax": {
                url: "<?php echo base_url(); ?>cases/getFormActivity",
                type: "POST",
                data: {
                    'caseId': caseno
                }
            },
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                console.log(aData);
                if (aData[5] != 0) {
                    $(nRow).css({
                        'background-color': aData[5],
                        'color': aData[6]
                    });
                }
            },
            "drawCallback": function (settings) {
                $('div.dataTables_scrollBody').scrollTop(0);
            }
        });
    });

    $(document).on('shown.bs.tab', 'a[data-toggle="tab"][href="#print-letters1"]', function (e) {
        printLetters = $('#form-letter2').DataTable({
            processing: true,
            order: [[0, "asc"]],
            scrollX: true,
            scrollY:"400px",
            scrollCollapse: false,
            bFilter: true,
            "drawCallback": function (settingsPL) {
                $(this).parent().scrollTop(0);
            }
        });
        printLetters.columns.adjust();
    });

    $(document).on('shown.bs.tab', 'a[data-toggle="tab"][href="#pension-forms"]', function (e) {
        pensionFormsTable = $('#form-letter7').DataTable({
            "processing": true,
            "order": [[0, "asc"]],
            scrollX: true,
            "scrollY":"400px",
            "scrollCollapse": true,
            "bFilter": true,
            "drawCallback": function (settings) {
                $('div.dataTables_scrollBody').scrollTop(0);
            }
        });
        pensionFormsTable.columns.adjust();
    });
    $(document.body).on('click', '#printletters', function () {
        $("tr").removeClass("label-success");
        $("#form-letter2 tbody tr:first").addClass("label-success");
    });
    $(document.body).on('click', '#form-letter2 tr', function() {
        $(this).parent().find(".label-success").removeClass("label-success");
        $(this).addClass("label-success");
    });
    $(document.body).on('click', '#form-letter7 tr', function() {
        $(this).parent().find(".label-success").removeClass("label-success");
        $(this).addClass("label-success");
    });
    $("#A1forms").val($("#A1forms option:first").val());
    $("ul.list0001 li:first").addClass("label-success");
    var t = $('ul.list0001').find('li.label-success');
    var fid = t.find('a').attr('id');
    var prefix = t.find('a').data('prefix');
    var fname = t.find('a').text();
    var fid = fid.split('.')[0];
    var fname = t.find('a').text();
    $('#A1forms1').text('Form #:' + fid + ", " + prefix + " " + fname);
    var last_valid_selection = null;

    $(document.body).on('change', '#A1forms', function (event) {
        if ($("#A1forms").val() == 'All Forms') {
            $('.showsearch').show();
        } else {
            $('.showsearch').hide();
        }
        if ($(this).val().length > 1) {
            $(this).val(last_valid_selection);
        } else {
            last_valid_selection = $(this).val();
            
            str = JSON.stringify(formdatalist);
            var full_list = "";
            var i = 0;
            $.each($.parseJSON(str), function (key, value) {
                if (last_valid_selection == value['group']) {
                    full_list = full_list + "<li>";
                    if (value['modalType'] != '') {
                        full_list = full_list + '<a href="#" class="viewDocForm" data-toggle="modal" data-prefix="' + value['prefix'] + '" data-number="' + value['number'] + '" id="' + value['filename'] + '" data-target="#' + value['modalType'] + '">' + value['title'] + '</a>';
                    } else {
                        full_list = full_list + '<a class="createDoc" data-number="' + value['number'] + '" data-prefix="' + value['prefix'] + '" id="' + value['filename'] + '" data-cardcode="' + <?php echo $display_details->cardcode; ?> + '">' + value['title'] + '</a>';
                    }
                    full_list = full_list + "<li>";
                    i++;
                }
                if (last_valid_selection == 'All Forms') {
                    if (value['category'] != 'workers comp' && value['category'] != 'pension') {
                        full_list = full_list + "<li>";
                        if (value['modalType'] != '') {
                            full_list = full_list + '<a href="#" class="viewDocForm" data-toggle="modal" data-prefix="' + value['prefix'] + '" data-number="' + value['number'] + '" id="' + value['filename'] + '" data-target="#' + value['modalType'] + '">' + value['title'] + '</a>';
                        } else {
                            full_list = full_list + '<a class="createDoc" data-prefix="' + value['prefix'] + '" data-number="' + value['number'] + '" id="' + value['filename'] + '" data-cardcode="' + <?php echo $display_details->cardcode; ?> + '">' + value['title'] + '</a>';
                        }
                        full_list = full_list + "<li>";
                        i++;
                    }
                }
            });
            $("#main").html('<ul class="list0001">' + full_list + '<ul>');
            $("ul.list0001 li:first").addClass("label-success");
            $('ul.list0001 li.label-success');
            var t = $('ul.list0001').find('li.label-success');
            var fid = t.find('a').attr('id');
            var prefix = t.find('a').data('prefix');
            var fid = fid.split('.')[0];
            var fname = t.find('a').text();
            $('#A1forms1').text('Form #:' + fid + ", " + prefix + " " + fname);
            $('#A1forms2').text(i + " Forms");
        }
    });

    $('#search_form').on('keypress', function(e) {
        if (e.which == 13) {
            getFilteredList('#search_form', e);
        }
    });

    $(document.body).on('blur', '#search_form', function (event) {
        getFilteredList('#search_form', event);
    });

    function getFilteredList(field_id, event) {
        last_valid_selection = $(field_id).val();
        str = JSON.stringify(formdatalist);
        var full_list = "";
        var i = 0;
        $.each($.parseJSON(str), function (key, value) {
            var title = value['title'].toUpperCase();
            var selection = last_valid_selection.toUpperCase();
            
            if(value['category'] != 'workers comp' && value['category'] != 'pension'){
                if (title.indexOf(selection) >= 0) {
                full_list = full_list + "<li>";
                if (value['modalType'] != '') {
                    full_list = full_list + '<a href="#" class="viewDocForm" data-toggle="modal" data-number="' + value['number'] + '" data-prefix="' + value['prefix'] + '" id="' + value['filename'] + '" data-target="#' + value['modalType'] + '">' + value['title'] + '</a>';
                } else {
                    full_list = full_list + '<a class="createDoc" data-number="' + value['number'] + '" id="' + value['filename'] + '" data-prefix="' + value['prefix'] + '" data-cardcode="' + <?php echo $display_details->cardcode; ?> + '">' + value['title'] + '</a>';
                }
                full_list = full_list + "<li>";
                i++;
                }
            }
        });
        if (full_list != '') {
            $("#main").html('<ul class="list0001">' + full_list + '<ul>');
            $("ul.list0001 li:first").addClass("label-success");
            $('ul.list0001 li.label-success');
            var t = $('ul.list0001').find('li.label-success');
            var fid = t.find('a').attr('id');
            var prefix = t.find('a').data('prefix');
            var fid = fid.split('.')[0];
            var fname = t.find('a').text();
            $('#A1forms1').text('Form #:' + fid + ", " + prefix + " " + fname);
            $('#A1forms2').text(i + " Forms");
        } else {
            swal("Warning..", "No forms found.", "warning");
            $('#search_form').val('');
            getFilteredList('#search_form', event);
        }
    }
</script>
