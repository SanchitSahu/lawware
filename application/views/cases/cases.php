<style type="text/css">
    /*.dataTables_filter {
        display:none;
    }*/
    #addNewCase label{font-weight: normal;}
    #addRolodexForm label {font-weight: normal;}
    #addcontact .modal-body{padding:0px 10px 5px;}
    #addcontact .tabs-container .panel-body{padding:0px;}
    #addcontact .modal-dialog{margin-top: 3px;}
</style>
 <link href="assets/css/plugins/iCheck/icheck.css" rel="stylesheet">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>Search Case</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Search Cases</h5>
                    <a class="btn pull-right btn-primary btn-sm" href="#addcase" data-toggle="modal" title="Add a Case">Add a Case</a>
                </div>
                <div class="ibox-content">
                    <?php
                    $tabindex = 1;
                    ?>
                    <form role="form" id="filterForm" name="filterForm" method="post" >
                        <div class="form-group">
                            <label>Search Options</label>
                            <div class="row">
                                <div class="col-lg-3">
                                    <select type="select" id="searchBy" name="searchBy" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                        <option value="">Search by</option>
                                        <?php foreach ($filters['searchBy'] as $key => $val) { ?>
                                            <option value="<?php echo $key; ?>" <?php
                                            if ($key == 'cd.last') {
                                                echo 'selected';
                                            }
                                            ?> ><?php echo $val; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <input type="text" data-toggle="tooltip" tabindex="<?php echo $tabindex++; ?>" title="please Select Search By" data-placement="bottom" id="searchText" name="searchText" placeholder="Search By ie. XYZ" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkbox m-r-xs" style="display:inline-block">
<!--                                        <input type="checkbox" name="deleted_cases" tabindex="<?php echo $tabindex++; ?>" id="checkbox1">
                                        <label for="checkbox1">
                                            Include Deleted Cases
                                        </label>-->
                                        <label class="container-checkbox">&nbsp;Include Deleted Cases
                                            <input type="checkbox" name="deleted_cases" tabindex="<?php echo $tabindex++; ?>" id="checkbox1">
                                            <span class="checkmark" ></span>
                                        </label>
                                    </div>
                                    <div class="checkbox m-r-xs" style="display:inline-block">
                                        <label class="container-checkbox">&nbsp;Unique
                                            <input type="checkbox" id="unique_cases" name="unique_cases" tabindex="<?php echo $tabindex++; ?>" id="checkbox1">
                                            <span class="checkmark" title="Do not display duplicates"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Search Filters</label>
                            <div class="row">
                                <div class="col-lg-3">
                                    <select type="select" name="casetype" id="casetype" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                        <option value="">Search by Case Type</option>
                                        <?php foreach ($filters['casetype'] as $key => $val) { ?>
                                            <option value="casetype~<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Case Status</option>
                                        <?php foreach ($filters['casestat'] as $key => $val) { ?>
                                            <option value="casestat~<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Card Type</option>
                                        <?php foreach ($filters['type'] as $key => $val) { ?>
                                            <option value="type~<?php echo $val['type']; ?>"><?php echo $val['type']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select type="select" id="atty_resp" name="atty_resp" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                        <option value="">Search by Atty Resp</option>
                                        <?php foreach ($filters['atty_resp'] as $key => $val) { ?>
                                            <option value="atty_resp~<?php echo $val['atty_resp']; ?>"><?php echo $val['atty_resp']; ?></option>
                                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Atty Hand</option>
                                        <?php foreach ($filters['atty_hand'] as $key => $val) { ?>
                                            <option value="atty_hand~<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Para Hand</option>
                                        <?php foreach ($filters['para_hand'] as $key => $val) { ?>
                                            <option value="para_hand~<?php echo $val['para_hand']; ?>"><?php echo $val['para_hand']; ?></option>
                                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Sec Hand</option>
                                        <?php foreach ($filters['sec_hand'] as $key => $val) { ?>
                                            <option value="sec_hand~<?php echo $val['sec_hand']; ?>"><?php echo $val['sec_hand']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <button class="btn btn-sm btn-primary m-r-xs" title="Search" id="btnSubmit" tabindex="<?php echo $tabindex++; ?>" type="search">Search</button>
                                    <button class="btn btn-sm btn-primary btn-danger" title="Clear" id="resetFilter" tabindex="<?php echo $tabindex++; ?>" type="clear">Clear</button>
                                </div>
                                <div class="col-lg-3">
                                    <a class="pull-right partyemail btn btn-sm btn-primary" href="" data-toggle="modal" >Party-Email</a>
                                </div>

                                <!--div class="col-lg-3">
                                    <select type="select" name="casestat" tabindex="<?php echo $tabindex++; ?>"  placeholder="Search by" class="form-control select2Class">
                                        <option value="">Search by Case Status</option>
                                <?php foreach ($filters['casestat'] as $key => $val) { ?>
                                                                                                    <option value="<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select type="select" name="type" tabindex="<?php echo $tabindex++; ?>" placeholder="Search by" class="form-control select2Class">
                                        <option value="">Search by Card Type</option>
                                <?php foreach ($filters['type'] as $key => $val) { ?>
                                                                                                    <option value="<?php echo $val['type']; ?>"><?php echo $val['type']; ?></option>
                                <?php } ?>
                                    </select>
                                </div-->
                            </div>
                        </div>
                        <!--div class="form-group">
                            <label>Staff Filters</label>
                            <div class="row">
                                <div class="col-lg-3">
                                    <select type="select" name="atty_resp" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                        <option value="" disabled="disabled">Search by Atty Resp</option>
                        <?php foreach ($filters['atty_resp'] as $key => $val) { ?>
                                                                                                    <option value="atty_resp-<?php echo $val['atty_resp']; ?>"><?php echo $val['atty_resp']; ?></option>
                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Atty Hand</option>
                        <?php foreach ($filters['atty_hand'] as $key => $val) { ?>
                                                                                                    <option value="atty_hand-<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Para Hand</option>
                        <?php foreach ($filters['para_hand'] as $key => $val) { ?>
                                                                                                    <option value="para_hand-<?php echo $val['para_hand']; ?>"><?php echo $val['para_hand']; ?></option>
                        <?php } ?>
                                        <option value="" disabled="disabled">Search by Sec Hand</option>
                        <?php foreach ($filters['sec_hand'] as $key => $val) { ?>
                                                                                                    <option value="sec_hand-<?php echo $val['sec_hand']; ?>"><?php echo $val['sec_hand']; ?></option>
                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select type="select" name="atty_hand" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                        <option value="">Search by Atty Hand</option>
                        <?php foreach ($filters['atty_hand'] as $key => $val) { ?>
                                                                                                    <option value="<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select type="select" name="para_hand" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                        <option value="">Search by Para Hand</option>
                        <?php foreach ($filters['para_hand'] as $key => $val) { ?>
                                                                                                    <option value="<?php echo $val['para_hand']; ?>"><?php echo $val['para_hand']; ?></option>
                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select type="select" name="sec_hand" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                        <option value="">Search by Sec Hand</option>
                        <?php foreach ($filters['sec_hand'] as $key => $val) { ?>
                                                                                                    <option value="<?php echo $val['sec_hand']; ?>"><?php echo $val['sec_hand']; ?></option>
                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div-->
                        <!--                        <div class="form-group marg-top15 inline-block">-->
                        <!--                            <button class="btn btn-sm btn-primary m-t-n-xs" title="Search" id="btnSubmit" tabindex="<?php echo $tabindex++; ?>" type="search">Search</button>
                                                    <button class="btn btn-sm btn-primary m-t-n-xs btn-danger" title="Clear" id="resetFilter" tabindex="<?php echo $tabindex++; ?>" type="clear">Clear</button>-->
                        <!--                            <button class="btn btn-sm btn-primary m-t-n-xs" type="save"><strong>Save Option</strong></button>-->
                        <!--                        </div>-->
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example case-dataTable">
                            <thead>
                                <tr>
                                    <th align="right">Case Number</th>
                                    <th>Name</th>
                                    <th align="center">AttyR</th>
                                    <th align="center">AttyH</th>
                                    <th >Case Type</th>
                                    <!--<th >Status</th>-->
                                    <th align="center">Case Status</th>
                                    <th>SS No</th>
                                    <th>Firm Name</th>
                                    <th>Followup Date</th>
                                    <th>Phone</th>
                                    <th>Your File No</th>
                                    <th>Date of Birth</th>
                                    <th>Date Enter</th>
                                    <th>Date Open</th>
                                    <th>Date Closed</th>
                                    <th>Card Type</th>
                                    <th>Cardcode</th>
                                    <th>Caption</th>
                                    <th>Mail1</th>

                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="popoverData" class="hide">
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Name:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Card Type:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Phone:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Type of Case:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Case Status:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Staff:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Next Appointment:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Quick Notes</strong></span>
        </div>
        <div class="col-sm-8">
            <textarea style="width:300px; height:200px;" ></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <a class="btn pull-right marg-top15 btn-sm btn-primary" href="#">More Details</a>
        </div>
    </div>
</div>

<!-- Protect Case Modal START-->
<div id="protactcasepassword" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Password</h4>
            </div>
            <div class="modal-body">
                <form name="protectCaseForm" id="protectCaseForm" method="post">
                    <p>This client card is protected.Please enter the password.</p>
                    <div class="form-group">
                        <input type="password" id="pro_propassword" name="pro_propassword" placeholder="Password" class="form-control" />
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="protectpassbtn">Proceed</a>
                        <a class="btn btn-md btn-danger" id="protectpasscnbtn">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Protect Case Modal END-->

<script type="text/javascript">
    var protectedCaseId = 0;
    $(document).ready(function () {
        $('a.navbar-minimalize.minimalize-styl-2.btn.btn-primary').click(function () {
            setTimeout(function () {
                casedataTable.columns.adjust();
            }, 100)
        });

        var message = '<?php echo $this->session->flashdata('case_message'); ?>';

        if (message != '') {

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": true,
                "preventDuplicates": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "400",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            Command: toastr["success"](message);
        }

        var popOverSettings = {
            container: 'body',
            html: true,
            selector: '[rel="popover"]',
            content: function () {
                return setSpinner();
            }
        }

        $('body').popover(popOverSettings).hover(function (e) {
            e.preventDefault();
        }, function (e) {

        });

        $('body').on('click', function (e) {
            if ($(e.target).data('toggle') !== 'popover'
                    && $(e.target).parents('.popover.in').length === 0) {
                $('[data-toggle="popover"]').popover('hide');
            }
        });

        $(document).on('click', '.case-dataTable tbody tr td:first-child a', function () {
            var proId = this.id;
            protectedCaseId = this.id;
            if ($(this).data('pro') == 1) {
                $('.popover-content').css('display', 'none');
                $('#protactcasepassword').modal('show');
                $('#protectpasscnbtn').click(function () {
                    $('#protactcasepassword').modal('hide');
                });
            } else {
                $('.popover .arrow').css('display', 'block');
                if (this.id.length > 0) {
                    //console.log(this.id);
                    $('[rel=popover]').not(this).popover('hide');
                    $.ajax({
                        type: "POST",
                        url: 'cases/getCaseDetails',
                        data: {id: this.id},
                        dataType: 'html',
                        context: this,
                        success: function (data) {
                            $('.popover-content').html(data);
                            $('.popover .arrow').css('display', 'none');
                        }
                    });
                }
            }
        });

        $('#pro_propassword').on('keypress', function (e) {
            if (e.which == 13)
            {
                $("#protectpassbtn").trigger("click");
            }
        });

        $('#protectpassbtn').click(function () {
            var pass = $('#pro_propassword').val();
            /* $( "#protectpassbtn").unbind( "click" ); */
            $.ajax({
                type: "POST",
                url: 'cases/getProtectPass',
                data: {id: protectedCaseId, pass: pass},
                dataType: 'html',
                //context: this,
                success: function (data) {
                    if (data == 1)
                    {
                        $('#protactcasepassword').modal('hide');
                        $('#pro_propassword').val('');
                        window.open("<?php echo base_url() ?>cases/case_details/" + protectedCaseId, '_blank');
                        return false;
                    }
                    if (data == 0)
                    {
                        swal("Invalid Password.", data.message, "error");
                        $('#protactcasepassword').modal('hide');
                        $('#pro_propassword').val('');
                    }
                }
            });
        });

        $(document).on('dblclick', '.case-dataTable tbody tr td:first-child a', function () {
            if ($(this).data('pro') == 1)
            {
                $('.popover-content').css('display', 'none');
                $('#protactcasepassword').modal('show');
                $('#protectpasscnbtn').click(function () {
                    $('#protactcasepassword').modal('hide');
                });
            } else
            {
                window.open("<?php echo base_url() ?>cases/case_details/" + this.id, '_blank');
                return false;
            }

        });
        $(document).ready(function () {

            $.casedataTablesearch = function () {
                casedataTable = $('.case-dataTable').DataTable({
                    dom: 'l<"html5buttons"B>tr<"col-sm-5 no-left-padding"i><"col-sm-7 no-padding"p>',
                    buttons: [
                        {
                            extend: 'excel',
                            text: '<i class="fa fa-file-excel-o"></i> Excel',
                            title: '<?php echo APPLICATION_NAME; ?> : Search Cases'
                        },
                        {
                            extend: 'pdf',
                            text: '<i class="fa fa-file-pdf-o"></i> PDF',
                            title: '<?php echo APPLICATION_NAME; ?> : Search Cases',
                            header: true,
                            customize: function (doc) {
                                doc.styles.table = {
                                    width: '100%',
                                };
                                doc['pageSize'] = 'A4';
                                doc['pageOrientation'] = 'landscape';
                                doc.styles.tableHeader.alignment = 'left';
                                doc['header'] = (function (page, pages) {
                                    return {
                                        columns: [
                                            '<?php echo APPLICATION_NAME; ?> ',
                                            {
                                                // This is the right column
                                                alignment: 'right',
                                                text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                            }
                                        ],
                                        margin: [0, 0]
                                    };
                                });
                                doc.content[1].table.widths = '*';
                            }
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print" aria-hidden="true"></i> Print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            }
                        }
                    ],
                    lengthMenu: [[5, 10, 25, 50, 100, 500, 1000, -1], [5, 10, 25, 50, 100, 500, 1000, "All"]],
                    processing: true,
                    stateSave: true,
                    autoWidth: true,
					stateSaveParams: function (settings, data) {
						data.search.search = "";
						data.start = 0;
						delete data.order;
						data.order = [0, "desc"];
					},
                    serverSide: true,
                    bDeferRender: true,
                    scrollX: true,
                    scrollY: '400px',
                    scrollCollapse: true,
                    /*"bFilter": false,*/
                    order: [[0, "desc"]],
                    ajax: {
                        url: "<?php echo base_url(); ?>cases/getCaseDataAjax",
                        type: "POST"
                    },
                    fnRowCallback: function (nRow, data, ) {
                        if (data[20] == "Deleted")
                        {
                            $('td', nRow).css('background-color', '#ff9999');
                        }
                    },
                    initComplete: function (settings, json) {
                        if ($("#unique_cases").val() === "SearchUniqueTrue") {
                            swal({
                                title: "Done Shorting by Name (unique)",
                                type: "success",
                                showCancelButton: false,
                                customClass: "sweet-alert-success",
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok"
                            });
                        } else if ($("#unique_cases").val() === "SearchUniqueFalse") {
                            swal({
                                title: "Done Shorting by Name",
                                type: "success",
                                showCancelButton: false,
                                customClass: "sweet-alert-success",
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok"
                            });
                        }
                        $("#unique_cases").val('');
                    },
                    columnDefs: [
                        {
                            render: function (data, type, row) {
                                if (row[19] == 1)
                                {
                                    var protected = 1;
                                } else
                                {
                                    var protected = 0;
                                }
                                return '<a href="javascript:void(0)" id="' + row[0] + '" rel="popover" data-pro="' + protected + '" data-container="body" data-toggle="popover" data-placement="right" data-popover-content="#data">' + data + '</a>';
                            },
                            targets: 0,
                            className: "text-center",
                            width: "5%"
                        },
                        {
                            targets: 1,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 2,
                            className: "text-center",
                            width: "5%"
                        },
                        {
                            targets: 3,
                            className: "text-center",
                            width: "5%"
                        },
                        {
                            targets: 4,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 5,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 6,
                            className: "text-center",
                            width: "5%"
                        },
                        {
                            targets: 7,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 8,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 9,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 10,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 11,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 12,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 13,
                            className: "text-left",
                            width: "5%"
                        },
                        {
                            targets: 14,
                            className: "text-left",
                            width: "6%"
                        },
                        {
                            targets: 15,
                            width: "6%"
                        },
                        {
                            targets: 16,
                            className: "text-left",
                            width: "6%"
                        },
                        {
                            targets: 17,
                            width: "6%"
                        },
                        {
                            targets: 18,
                            width: "4%"
                        }
                    ]
                });
            }
            $.casedataTablesearch();
            casedataTable.columns.adjust().draw();
        });

        $(document).on('click', '#close', function () {
            $("[rel=popover]").popover("destroy");
        });

        $('#searchBy').change(function () {
            var searchText = $('#searchBy').val().split('.');
            var message = '';
            var placeholder = '';

            if (searchText[1] == 'contact')
            {
                $("#searchText").addClass("contact");
            } else
            {
                $("#searchText").val('');
                $("#searchText").unmask();
                $("#searchText").removeAttr("maxlength");
                $("#searchText").removeClass("contact");
            }
            $('#searchText').datetimepicker('remove');
            switch (searchText[1]) {
                case 'first':
                    message = 'Enter all the Part of the First Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'last':
                    message = 'Enter all the Part of the Last Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'firm':
                    message = 'Enter all the Part of the Firm Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'caseno':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'caseno_less':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'caseno_more':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'yourfileno':
                    message = 'Enter all or part of your own internal office file Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'social_sec':
                    message = 'Enter all or part of the Social Security Number';
                    placeholder = 'Search By ie. 234-56-5898';
                    break;
                case 'followup':
                    message = 'Display case as per Follow up Date';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true
                    });
                    break;
                case 'contact':
                    message = "Enter all or part of the phone number to appear anywhere within the card or firm's for telephone number";
                    placeholder = '(111)111-1111';
                    break;
                case 'birth_date':
                    message = 'Enter the Date of Birth';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'birth_month':
                    message = 'Enter the number for a month ie: 1=Jan, 2=Feb, 12=Dec, 0=All';
                    placeholder = 'Numbers 0-12 Range';
                    break;
                case 'dateenter':
                    message = 'Display case as per Entered Dates';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'dateopen':
                    message = 'Display case as per Opened Date';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'dateclosed':
                    message = 'Display case as per Closed Date';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'casestat':
                    message = 'Enter Case Status';
                    placeholder = 'Search By ie. PNC';
                    break;
            }

            $('#searchText').attr("title", message);
            $('#searchText').attr("placeholder", placeholder);
        });

        $('#btnSubmit').click(function () {
            $.casedataTablesearch();
            $('#searchText').trigger('blur');
            casedataTable.search($("#filterForm").serialize()).draw();
            casedataTable.columns.adjust().draw();
            return false;
        });

        $('#resetFilter').click(function () {
            casedataTable.search('').draw();
            $('#filterForm')[0].reset();
            $('.select2Class').trigger('change');

            return false;
        });
        $('#searchText').on('blur', function () {
            dtValue = $(this).val();
            if (dtValue)
            {
                if ($('#searchBy').val() == 'cd.birth_date' || $('#searchBy').val() == 'c.dateenter' || $('#searchBy').val() == 'c.dateopen' || $('#searchBy').val() == 'c.dateclosed' || $('#searchBy').val() == 'c.followup')
                {
                    var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
                    if (!dtRegex.test(dtValue))
                    {
                        $(this).parent().append('<span style="color:red;">Please enter a valid date</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() == 'cd.birth_month')
                {
                    var dtRegex = new RegExp(/^[0-9]{0,2}$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $(this).parent().append('<span style="color:red;">Please enter a valid month</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() == 'cd.social_sec')
                {
                    var dtRegex = new RegExp(/^[0-9]{3}\-?[0-9]{2}\-?[0-9]{4}$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $('#searchText').parent().find('span').remove();
                        $(this).parent().append('<span style="color:red;">Please enter a valid Social Security Number</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() != 'cd.contact' && $('#searchBy').val() != 'c.yourfileno' && $('#searchBy').val() != 'cd2.firm' && $('#searchBy').val() != 'c.casestat') {
                    var dtRegex = new RegExp(/^\s*[a-zA-Z0-9,\s]+\s*$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $('#searchText').parent().find('span').remove();
                        $(this).parent().append('<span style="color:red;">Please enter a valid input</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }

                }
            }
        });

    });

    $('#searchText').on('keypress', function (e) {
        if (e.which == 13)
        {
            $("#btnSubmit").trigger("click");
        }
    });

    $('#casetype').change(function () {
        $("#btnSubmit").trigger("click");
    });

    $('#atty_resp').change(function () {
        $("#btnSubmit").trigger("click");
    });
    $('#unique_cases').change(function () {
        if ($(this).prop("checked") == true) {
            $("#unique_cases").val('SearchUniqueTrue');
        } else if ($(this).prop("checked") == false) {
            $("#unique_cases").val('SearchUniqueFalse');
        }
        casedataTable.destroy();
        $("#btnSubmit").trigger("click");
    });
    $(document.body).on('click', 'div#addcontact a[data-toggle="tab"]', function (e) {
        if (!$('#addRolodexForm').valid()) {
            e.preventDefault();
            return false;
        }
    });
    $(document.body).on('blur', 'input[name=card2_state]', function () {
        var add1 = $('#street_number').val();
        var add2 = $('#route').val();
        var city = $('#locality').val();
        var state = $(this).val();

        if (state != '') {
            rolodexSearch.clear();
            $("#firmcompanylist").attr("style", "z-index:9999 !important");
            $('#firmcompanylist').modal('show');
            rolodexSearch = $('#rolodexsearchResult_data').DataTable({
                processing: true,
                rolodexsearchAddress: true,
                serverSide: true,
                stateSave: true,
				stateSaveParams: function (settings, data) {
					data.search.search = "";
					data.start = 0;
					delete data.order;
					data.order = [0, "desc"];
				},
                bFilter: false,
                scrollX: true,
                sScrollY: 400,
                bScrollCollapse: true,
                destroy: true,
                autoWidth: true,
                "lengthMenu": [5, 10, 25, 50, 100],
                order: [[0, "ASC"]],
                ajax: {
                    url: '<?php echo base_url(); ?>cases/searchForRolodexAddress',
                    method: 'POST',
                    data: {add1: add1, add2: add2, city: city, state: state},
                },
                fnRowCallback: function (nRow, data) {
                    $(nRow).attr("cardcode", '' + data[8]);
                    $(nRow).addClass('viewsearchPartiesDetail1');
                },
                fnDrawCallback: function (oSettings) {
                },
                initComplete: function (settings, json) {

                },
                select: {
                    style: 'single'
                }
            });
            /*$.ajax({
             url: '<?php //echo base_url();           ?>cases/searchForRolodex2',
             method: 'POST',
             data: {add1:add1,add2:add2,city:city,state:state},
             beforeSend: function (xhr) {
             $.blockUI();
             },
             complete: function (jqXHR, textStatus) {
             $.unblockUI();
             },
             success: function (data) {
             rolodexSearch.clear();
             $("#firmcompanylist").attr("style", "z-index:9999 !important");
             $('#firmcompanylist').modal('show');
             var obj = $.parseJSON(data);
             $.each(obj, function (index, item) {
             var html = '';
             html += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + item.cardcode + "><td>" + item.firm + "</td>";
             html += "<td>" + item.city + "</td>";
             html += "<td>" + item.address + "</td>";
             html += "<td>" + item.address2 + "</td>";
             html += "<td>" + item.phone + "</td>";
             html += "<td>" + item.name + "</td>";
             html += "<td>" + item.state + "</td>";
             html += "<td>" + item.zip + "</td>";
             html += "</tr>";
             rolodexSearch.row.add($(html));
             });
             rolodexSearch.draw();
             }
             });*/
        }
    });
</script>
