<?php

$dob = '';
$intakeDate = '';
$followup = '';
$dateReferred = '';
$ps = '';
$address = '';
$venueName = 'NA';

if($case_details[0]->city != ''){
    $address .= $case_details[0]->city.", ";
}
if($case_details[0]->state != ''){
    $address .= $case_details[0]->state.", ";
}
if($case_details[0]->zip != ''){
    $address .= $case_details[0]->zip;
}

if($case_details[0]->venueName != ''){
    $venueName = $case_details[0]->venueName;
}

echo $venueName;exit;

if($case_details[0]->birth_date != '' && $case_details[0]->birth_date != '0000-00-00 00:00:00' && $case_details[0]->birth_date != '1899-12-30 00:00:00'){
    $dob = date('m/d/Y',strtotime($case_details[0]->birth_date));
}
if($case_details[0]->dateenter != '' && $case_details[0]->dateenter != '0000-00-00 00:00:00' && $case_details[0]->dateenter != '1899-12-30 00:00:00'){
    $intakeDate = date('m/d/Y',strtotime($case_details[0]->birth_date));
}
if($case_details[0]->followup != '' && $case_details[0]->followup != '0000-00-00 00:00:00' && $case_details[0]->followup != '1899-12-30 00:00:00'){
    $followup = date('m/d/Y',strtotime($case_details[0]->followup));
}
if($case_details[0]->d_r != '' && $case_details[0]->d_r != '0000-00-00 00:00:00' && $case_details[0]->d_r != '1899-12-30 00:00:00'){
    $dateReferred = date('m/d/Y',strtotime($case_details[0]->d_r));
}
if($case_details[0]->psdate != '' && $case_details[0]->psdate != '0000-00-00 00:00:00' && $case_details[0]->psdate != '1899-12-30 00:00:00'){
    $ps = date('m/d/Y',strtotime($case_details[0]->psdate));
}

?>
<html>
<head>
<style>
.container {
  width:100%;
}
.row {
  width: 100%;
  margin-top: 10px;
  border-bottom: 1px solid #000;
}
.row-last{
  width: 100%;
  margin-top: 10px;
}
.inline {
  width:50%;
  float:left;
}
p {
  font-size: 12px;
  position: relative;
  padding-left: 0px;
}
p>span:first-child {
    position: absolute;
    left:120px;
}
p>span:last-child {
    position: absolute;
    left: 130px;
}
html, body { height: auto; }
</style>
</head>
<body>
<div class="container">
  <div class="row">
      <div class="inline">
          <p>Case Number<span>:</span><span><?php echo $case_details[0]->caseno; ?></span></p>
          <p>Case Name<span>:</span><span><?php echo $case_details[0]->caption1; ?></span></p>
      </div>
      <div class="inline">
         <p>Page Number<span>:</span><span></span></p>
         <p>Today<span>:</span><span><?php echo date('m/d/Y'); ?></span></p>
      </div>
  </div>
  <div class="row">
      <div>
          <h3>General Case Information</h3>
      </div>
      <div class="inline">
          <p>Case Number<span>:</span><span><?php echo $case_details[0]->caseno; ?></span></p>
          <p>Type of Case<span>:</span><span><?php echo $case_details[0]->casetype; ?></span></p>
          <p>Case Status<span>:</span><span><?php echo $case_details[0]->casestat; ?></span></p>
          <p>Your File Number<span>:</span><span><?php echo $case_details[0]->yourfileno; ?></span></p>
          <p>File Location<span>:</span><span><?php echo $case_details[0]->location; ?></span></p>
          <p>Venue<span>:</span><span><?php echo $venueName; ?></span></p>
      </div>
      <div class="inline">
          <p></br></p>
          <p></br></p>
          <p>Atty Resp<span>:</span><span><?php echo $case_details[0]->atty_resp; ?></span></p>
          <p>Atty Hand<span>:</span><span><?php echo $case_details[0]->atty_hand; ?></span></p>
          <p>Para Hand<span>:</span> <span><?php echo $case_details[0]->para_hand; ?></span></p>
          <p>Sec Hand<span>:</span><span><?php echo $case_details[0]->sec_hand; ?></span></p>
      </div>
  </div>
  <div class="row">
      <div>
          <h3>Primary Client Information</h3>
      </div>
      <div class="inline">
          <p>Name<span>:</span><span><?php echo $case_details[0]->first." ".$case_details[0]->last; ?></span></p>
          <p>Firm<span>:</span><span><?php echo $case_details[0]->sec_hand; ?></span></p>
          <p>Address<span>:</span><span><?php echo $case_details[0]->address1." ".$case_details[0]->address2; ?></span></p>
          <p>City,state,Zip<span>:</span><span><?php echo $address; ?></span></p>
          <p>Phone(Home)<span>:</span><span><?php echo $case_details[0]->home; ?></span></p>
          <p>Phone(Business)<span>:</span><span><?php echo $case_details[0]->business; ?></span></p>
          <p>Phone(Fax)<span>:</span><span><?php echo $case_details[0]->fax; ?></span></p>
          <p>Date of Birth<span>:</span><span><?php echo $dob; ?></span></p>
          <p>Interpreter/Language<span>:</span><span><?php echo $case_details[0]->interpret; ?></span></p>
      </div>
      <div class="inline">
          <p></br></p>
          <p></br></p>
          <p></br></p>
          <p></br></p>
          <p>Phone(Car)<span>:</span><span><?php echo $case_details[0]->car; ?></span></p>
          <p>Phone(Beeper)<span>:</span><span><?php echo $case_details[0]->beeper; ?></span></p>
          <p>Email<span>:</span><span><?php echo $case_details[0]->email; ?></span></p>
          <p>License No<span>:</span><span><?php echo $case_details[0]->licenseno; ?></span></p>
          <p>Social Sec No<span>:</span><span><?php echo $case_details[0]->social_sec; ?></span></p>
      </div>
  </div>
  <div class="row">
      <div>
          <h3>Case Dates</h3>
      </div>
      <div class="inline">
          <p>Intake Date<span>:</span><span><?php echo $intakeDate; ?></span></p>
          <p>Follow up Date<span>:</span><span><?php echo $followup; ?></span></p>
          <p>Date Referred<span>:</span><span><?php echo $dateReferred; ?></span></p>
          <p>Referred By<span>:</span><span><?php echo $case_details[0]->rb; ?></span></p>
      </div>
      <div class="inline">
          <p>P & S<span>:</span><span><?php echo $case_details[0]->ps; ?></span></p>
          <p>P & S Date<span>:</span><span><?php echo $ps; ?></span></p>
      </div>
  </div>
  <div class="row">
      <div>
          <h3>Case Caption</h3>
      </div>
      <div>
          <p><?php echo $case_details[0]->caption1; ?></p>
      </div>
  </div>
  <div class="row">
      <div>
          <h3>Quick Note</h3>
      </div>
      <div >
          <p></br></p>
      </div>
  </div>
  <div class="row-last">
      <div>
          <h3>Message</h3>
      </div>
      <div>
          <p></br></p>
      </div>
  </div>
</div>
</body>
</html>
