<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url('dashboard'); ?>">Home</a> /
                <span>EAMS Lookup</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>EAMS Lookup</h5>
                </div>
                <div class="ibox-content marg-bot15">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-eamsLookup" >
                            <thead>
                                <tr>
                                    <th>Eamsref</th>
                                    <th>Name</th>
                                    <th>Address 1</th>
                                    <th>Address 2</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Zip</th>
                                    <th>Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var eamsData = '';
    $(document).ready(function () {
        $('a.navbar-minimalize.minimalize-styl-2.btn.btn-primary').click(function () {
            setTimeout(function () {
                eamsData.columns.adjust();
            }, 100);
        });

        eamsData = $('.dataTables-eamsLookup').DataTable({
            "processing": true,
            "serverSide": true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
			},
            "bFilter": true,
            "sScrollX": "100%",
            "sScrollXInner": "150%",
            "ajax": {
                url: "<?php echo base_url(); ?>eams_lookup/getEamsAjaxData",
                type: "POST"
            }
        });
    });

</script>