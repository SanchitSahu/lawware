<?php
if (isset($system_data)) {
    $main1 = json_decode($system_data[0]->main1);
    $injury9 = json_decode($system_data[0]->injury9);
    $holidayset = json_decode($system_data[0]->datealert);

    $holodaylist = isset($this->data['system_data'][0]->holidays) ? (explode("##", trim($this->data['system_data'][0]->holidays))) : '';
    $holodaylist = array_map('trim', $holodaylist);
    foreach ($holodaylist as $key => $value) {
        if (is_null($value) || $value == '') {
            unset($holodaylist[$key]);
        }
    }
}
?>
<style>
#taskdatepicker-error
{
	color:red;
}
    .chat-active {
        background: green;
        color: white;
        font-weight: 700;
    }
    .chat-user:hover {
        background: green;
        color: white;
        font-weight: 700;
    }
    .today_tasks tbody tr td:nth-child(3){word-break: break-all !important;width: 100px;}
    .today_tasks thead tr th:nth-child(5){text-align: center;}
    .searchoxevent #DataTables_Table_1_wrapper.dataTables_filter input{ width:100px !important;}
    .searchoxevent #DataTables_Table_1_wrapper.dataTables_filter{width:160px !important;}
    #addNewCase label{font-weight: normal;}
    #addRolodexForm label {font-weight: normal;}
    #addcontact .modal-body{padding:0px 10px 5px;}
    #addcontact .tabs-container .panel-body{padding:0px;}
    .today_tasks{width:auto;}.ui-datepicker-trigger{border:none;background:none;position: absolute;right: 6px;top: 5px;}
    .rdateerror{color:red;}
    .uk-dropdown {
        width: 100%;
    }
    .addcase_n{
    background-color : #4d7ab1 !important;
    border-color : #4d7ab1 !important;
    color : #fff !important;
    }
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>

<?php $tabindex = isset($tabindex) ? $tabindex : 0; ?>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6 col-xs-12">
            <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <h5>Search Case</h5>
                    <!-- <button class="btn btn-sm btn-primary pull-right"  title="Recently Viewed Cases" id="btnRecent" type="search">Recent</button>
                    <button class="btn btn-sm btn-primary pull-right" style="margin-right:5px;" title="All Cases" id="btnAll" type="search">All</button> -->
                    <a class="btn pull-right btn-primary btn-sm" style="margin-right:5px;" href="#addcase" id="addcase_new" title="Add a Case" data-toggle="modal">Add a Case</a>
                </div>
                <div class="ibox-content">
                    <form role="form" id="filterForm" name="filterForm" method="post" >
                        <div class=""><label>Search Options</label></div>
                        <div class="row">
                            <input type="hidden" id="valueRecent" name="valueRecent" value="all">
                            <div class="col-lg-4">
                                <select type="select" id="searchBy" name="searchBy" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                    <option value="">Search by</option>
                                    <?php foreach ($filters['searchBy'] as $key => $val) { ?>
                                        <option value="<?php echo $key; ?>" <?php
                                        if ($key == 'cd.last') {
                                            echo 'selected';
                                        }
                                        ?> ><?php echo $val; ?></option>
                                            <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" tabindex="<?php echo $tabindex++; ?>" title="please Select Search By"  id="searchText" name="searchText" placeholder="Search By ie. XYZ" class="form-control" autocomplete="off">
                            </div>
                            <div class="col-lg-4">
                                <div class="checkbox">
                                    <label class="container-checkbox">&nbsp;Include Deleted Cases
                                        <input type="checkbox" name="deleted_cases" tabindex="<?php echo $tabindex++; ?>" id="checkbox1">
                                        <span class="checkmark"  style="padding-right: 5px;"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class=""><label>Search Filters</label></div>
                        <div class="row">

                            <div class="col-lg-4">
                                <select type="select" name="casetype" id="casetype" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                    <option value="">Search by Case Type</option>
                                    <?php foreach ($filters['casetype'] as $key => $val) { ?>
                                        <option value="casetype~<?php echo $val['casetype']; ?>"><?php echo $val['casetype']; ?></option>
                                    <?php } ?>
                                    <option value="" disabled="disabled">Search by Case Status</option>
                                    <?php foreach ($filters['casestat'] as $key => $val) { ?>
                                        <option value="casestat~<?php echo $val['casestat']; ?>"><?php echo $val['casestat']; ?></option>
                                    <?php } ?>
                                    <option value="" disabled="disabled">Search by Card Type</option>
                                    <?php foreach ($filters['type'] as $key => $val) { ?>
                                        <option value="type~<?php echo $val['type']; ?>"><?php echo $val['type']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <select type="select" name="atty_resp" id="atty_resp" placeholder="Search by" tabindex="<?php echo $tabindex++; ?>" class="form-control select2Class">
                                    <option value="">Search by Atty Resp</option>
                                    <?php foreach ($filters['atty_resp'] as $key => $val) { ?>
                                        <option value="atty_resp~<?php echo $val['atty_resp']; ?>"><?php echo $val['atty_resp']; ?></option>
                                    <?php } ?>
                                    <option value="" disabled="disabled">Search by Atty Hand</option>
                                    <?php foreach ($filters['atty_hand'] as $key => $val) { ?>
                                        <option value="atty_hand~<?php echo $val['atty_hand']; ?>"><?php echo $val['atty_hand']; ?></option>
                                    <?php } ?>
                                    <option value="" disabled="disabled">Search by Para Hand</option>
                                    <?php foreach ($filters['para_hand'] as $key => $val) { ?>
                                        <option value="para_hand~<?php echo $val['para_hand']; ?>"><?php echo $val['para_hand']; ?></option>
                                    <?php } ?>
                                    <option value="" disabled="disabled">Search by Sec Hand</option>
                                    <?php foreach ($filters['sec_hand'] as $key => $val) { ?>
                                        <option value="sec_hand~<?php echo $val['sec_hand']; ?>"><?php echo $val['sec_hand']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-4 marg-top5 inline-block text-right">
                                <button class="btn btn-sm btn-primary" title="Search" id="btnSubmit" tabindex="10" type="search">Search</button>
                                <button class="btn btn-sm btn-primary  m-l-xs btn-danger" title="Clear" id="resetFilter" tabindex="15" type="clear">Clear</button>
                                <input type="button" class="btn btn-sm  m-l-sm btn-primary pull-right"  title="Recently Viewed Cases" id="btnRecent" type="search" value="Recent">
                                <input type="button" class="btn btn-sm btn-primary m-l-sm pull-right" style="" title="All Cases" id="btnAll" type="search" value="All">
                        </div>
                        </div>
                    </form>
                    <div class="marg-top10"></div>
                    <div class="table-responsive caseeventContiner">
                        <table class="table table-striped table-bordered table-hover dataTables-example case-dataTable" >
                            <thead>
                                <tr>
                                    <th class="text-center">Case Number</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">AttyR</th>
                                    <th class="text-center">AttyH</th>
                                    <th class="text-center">Case Type</th>
                                    <th class="text-center">Case Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($topCases as $data) { ?>
                                    <tr>
                                        <td class="text-center"><a href="javascript:void(0)" id="<?php echo $data[0]; ?>" rel="popover" data-pro="<?php echo $data[6]; ?>" data-container="body" data-toggle="popover" data-placement="right" data-popover-content="#data"><?php echo $data[0]; ?></a></td>
                                        <td class="text-left"><?php echo $data[1]; ?></td>
                                        <td class="text-center"><?php echo $data[2]; ?></td>
                                        <td class="text-center"><?php echo $data[3]; ?></td>
                                        <td class="text-center"><?php echo $data[4]; ?></td>
                                        <td class="text-center"><?php echo $data[5]; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <small class="pull-right text-muted msg-time"></small>
                    <h5>Broadcast Chat panel</h5>
                </div>
                <div class="ibox-content eventContiner">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chat-discussion" id="chat-content">
                                    <div class="chat-message left">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="chat-message-form">
                                    <form id="chatMessage" name="chatMessage" method="post">
                                        <div class="form-group col-sm-10" >
                                            <textarea class="form-control message-input " contenteditable="true" name="message" placeholder="Enter message text"></textarea>
                                        </div>
                                        <div class="form-group col-sm-2">
                                            <button type="button" class="pull-right btn btn-primary marg-bot0" id="send"><i class="fa fa-send"></i>&nbsp; Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-lg-6 col-xs-12">
            <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <h5>Today's Tasks</h5>
                    <a class="btn btn-sm pull-right btn-primary" href="<?php echo base_url() . "tasks"; ?>" title="View all Tasks" target="_blank">View All</a>
                </div>
                <div class="ibox-content casecontainer searchoxevent">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover today_tasks">
                            <thead>
                                <tr>
                                    <th align="right">Priority</th>
                                    <th align="center">From</th>
                                    <th align="center">Tasks Detail</th>
                                    <th align="center">Finish By</th>
                                    <th align="center">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <h5>Today's Events</h5>
                    <a class="btn btn-sm pull-right btn-primary" href="<?php echo base_url() . "calendar"; ?>" title="View all Events" target="_blank">View All</a>
                </div>
                <div class="ibox-content casecontainer searchoxevent">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover today_event">
                            <thead>
                                <tr>

                                    <th align="right">Time</th>
                                    <th align="center">AttyH</th>
                                    <th align="center">AttyA</th>
                                    <th>Event</th>
                                    <th align="center">Name</th>
                                    <th align="center">Venue</th>
                                    <th>Judge</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>



        </div>
    </div>
</div>

<div id="taskCompletePopup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <span>Are you sure to Set Task as Completed?</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <div class="">
                        <?php $caseNo = $this->uri->segment(3); ?>
                        <input id="caseNo" name="caseNo" type="hidden" value="<?php echo $caseNo; ?>">
                        <input id="taskNo" name="taskNo" type="hidden" value="">
                        <button id="complete" type="button" class="btn btn-primary">Complete</button>
                        <button id="completewithnew" type="button" class="btn btn-primary">Complete and Add New</button>
                        <button type="button" id="cncl" class="btn btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="TodayTaskCompleted" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mark Task Completed</h4>
            </div>
            <div class="modal-body">
                <form id="edit_case_details" method="post">
                    <div class="panel-body">
                        <div class="float-e-margins">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox-content">
                                        <div class="form-group">
                                            <textarea class="form-control" name="case_event" id="case_event" style="height: 100px;width: 100%;"></textarea>
                                            <input type='hidden' id='caseTaskId' name='caseTaskId'>
                                            <input type='hidden' id='caseno' class='caseNo' name='caseno'>
                                            <input type='hidden' id='addnew' class='addnew' value="0">
                                        </div>
                                        <div class="form-group">
                                            <label>Enter any information to add to the  task to be posted to case activity</label>
                                            <textarea class="form-control" name="case_extra_event" id="case_extra_event" style="height: 100px;width: 100%;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class='col-md-6'>
                    <select id="case_category" name="case_category" class="form-control select2Class">
                        <?php foreach ($caseactcategory as $k => $v) { ?>
                            <option value="<?php echo $v->category; ?>"><?php echo $v->actname; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class='col-md-6'>
                    <div class="form-btn text-right text_center">
                        <button type="submit" id="saveCompletedTask1" class="btn btn-primary btn-md">Complete</button>
                        <button type="submit" id="saveCompletedTask1" class="btn btn-primary btn-md">Complete and Add New</button>
                        <button type="button" class="btn btn-primary btn-md btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="addtask" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body">
                <form id="addTaskForm" name="addTaskForm" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <input type="hidden" name="caseno" id="TaskcaseNo" class="case_No" value="<?php echo $caseNo; ?>"/>
                                        <label class="col-sm-3 no-padding">From</label>
                                        <div class="col-sm-9 no-padding"><select class="form-control select2Class" name="whofrom" id="whofrom">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected="selected"' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-2 no-padding">To</label>
                                        <div class="col-sm-10 no-padding"><select class="form-control select2Class staffTO staffvacation" name="whoto2" onchange="return remove_error('whoto2')" id="whoto2">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php if($val->initials == $this->session->userdata('user_data')['initials']) { echo 'selected'; } ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Finish By<span class="asterisk">*</span></label>
                                        <div class="col-sm-9 no-padding"><input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" onchange="return remove_error('taskdatepickern')" id="taskdatepickern" />
                                            <label id="taskdatepicker-error"></label>   
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Color</label>
                                        <div class="col-sm-9 no-padding"><select class="form-control select2Class" name="color" id="color">
                                                <?php
                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                foreach ($jsoncolor_codes as $k => $color) {
                                                    ?>
                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Task Type</label>
                                        <div class="col-sm-8 no-padding"><select class="form-control select2Class" name="typetask">
                                                <option value="" selected="selected">Select Task Type</option>
                                                <option value=""></option>
                                                <option value="follow up">Follow Up</option>
                                                <option value="statute">Statute</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-padding">Priority</label>
                                        <div class="col-sm-9 no-padding"><select class="form-control select2Class" name="priority" id="priority">
                                                <option value="3">Low</option>
                                                <option value="2">Medium</option>
                                                <option value="1">High</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task <span class="asterisk">*</span></label>
                                        <textarea class="form-control" id="event" name="event" style="height: 150px;resize: none;"></textarea>
                                        <label id="event-error" class="error"></label>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="container-checkbox pull-left">Reminder
                                            <input type="checkbox" class="treminder" name="reminder" onchange="showhidedt();">
                                            <span class="checkmark"></span> <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="You will get notified on your screen prior to 10 minutes of your scheduled task. To enable calender reminder notification in your browser to receive all notifications, allow notification in site settings." id="taskreminder"></i> &nbsp;&nbsp;<span class="rdateerror"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group">
                                        <label class="col-sm-3">Date</label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" name="rdate" class="rdate form-control" data-mask="99/99/9999" placeholder class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 rmdt" style="display:none">
                                    <div class="row form-group uk-autocomplete">
                                        <label class="col-sm-3">Time</label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" name="rtime" class="rtime form-control" data-uk-timepicker="{format:'12h'}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary" id="addTask" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
                                        <label class="container-checkbox pull-right marg-top5">&nbsp;Email
                                            <input type="checkbox" class="" name="notify" >
                                            <span class="checkmark"  style="padding-right: 5px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="addtask1" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Task</h4>
            </div>
            <div class="modal-body" style="display:block;">
                <form id="addTaskForm1" name="addTaskForm1" >
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <input type="hidden" name="taskid" value="">
                                        <input type="hidden" name="caseno" id="caseNo" class="case_No" value="<?php echo $caseno; ?>"/>
                                        <label class="col-sm-4 no-padding">From</label>
                                        <div class="col-sm-8 no-padding">
                                            <select class="form-control select2Class" name="whofrom" id="whofrom">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>" <?php echo ($val->initials == $this->session->userdata('user_data')['initials']) ? 'selected' : ''; ?>><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-sm-2 no-padding">To</label>
                                        <div class="col-sm-10 no-padding">
                                            <select class="form-control select2Class" name="whoto3" id="whoto3">
                                                <?php
                                                if (isset($staffList)) {
                                                    foreach ($staffList as $val) {
                                                        ?>
                                                        <option value="<?php echo $val->initials; ?>"><?php echo $val->initials; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 no-left-padding">Finish By <span class="asterisk">*</span></label>
                                        <div class="col-sm-9 no-padding">
                                            <input type="text" class="form-control weekendsalert alerttonholiday" name="datereq" id="taskdatepicker2" />
                                            <label class="error" id="taskdatepickeredit-error"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-left-padding">Color</label>
                                        <div class="col-sm-8">
                                            <select class="form-control select2Class" name="color" id="color">
                                                <?php
                                                $jsoncolor_codes = json_decode(color_codes, true);
                                                foreach ($jsoncolor_codes as $k => $color) {
                                                    ?>
                                                    <option value="<?php echo $k; ?>"><?php echo $color[0]; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 no-padding">
                                    <div class="form-group">
                                        <label class="col-sm-4 no-padding">Task Type</label>
                                        <div class="col-sm-8 no-left-padding">
                                            <select class="form-control select2Class" name="typetask">
                                                <option value="">Select Task Type</option>
                                                <option value=""></option>
                                                <option value="follow up">Follow up</option>
                                                <option value="statute">Statute</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-4 no-padding">
                                    <div class="form-group">
                                        <label class="col-sm-5">Priority</label>
                                        <div class="col-sm-7 no-left-padding">
                                            <select class="form-control select2Class" name="priority" id="priority">
                                                <option value="3">Low</option>
                                                <option value="2">Medium</option>
                                                <option value="1">High</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>


                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Task <span class="asterisk">*</span></label>
                                        <textarea class="form-control" name="event" id="eventedit" style="height: 150px;resize: none;"></textarea>
                                    <label id="eventedit-error" class="error"></label>
                                    </div>
                                </div>

                                <div class="clearfix">&nbsp;</div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="container-checkbox pull-left">Reminder
                                                <input type="checkbox" class="treminder" name="reminder1" onchange="showhidedt1();">
                                                <span class="checkmark"></span><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="You will get notified on your screen prior to 10 minutes of your scheduled task. To enable calender reminder notification in your browser to receive all notifications, allow notification in site settings." id="taskreminderedit"></i> &nbsp;&nbsp;<span class="rdateerror"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="row form-group">
                                            <label class="col-sm-3">Date</label>
                                            <div class="col-sm-9 no-padding">
                                                <input type="text" name="rdate" class="rdate1 form-control" data-mask="99/99/9999" placeholder class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 rmdt1" style="display:none">
                                        <div class="row form-group">
                                            <label class="col-sm-3">Time</label>
                                            <div class="col-sm-9 no-padding uk-autocomplete">
                                                <input type="text" name="rtime" id="rtimeup" class="rtime form-control" data-uk-timepicker="{format:'12h'}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-md btn-primary" id="addTask1" >Save</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-md btn-primary btn-danger">Cancel</button>
										<span class="marg-left5" id="pullcasebtn"></span>
                                        <label class="container-checkbox pull-right marg-top5">&nbsp;Email
                                            <input type="checkbox" class="" name="notify" >
                                            <span class="checkmark"  style="padding-right: 5px;"></span>
                                        </label>
                                    </div>
                                </div>
                            </div></div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>


<!-- End Add/Edit Tasks Modal -->

<div id="popoverData" class="hide">
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Name:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Card Type:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Phone:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Type of Case:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Case Status:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Staff:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Next Appointment:</strong></span>
        </div>
        <div class="col-sm-8">
            <span></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <span><strong>Quick Notes</strong></span>
        </div>
        <div class="col-sm-8">
            <textarea style="width:300px; height:200px;" ></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <a class="btn pull-right marg-top15 btn-sm btn-primary" href="#">More Details</a>
        </div>
    </div>
</div>
<!-- Protect Case Modal START-->
<div id="protactcasepassword" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Password</h4>
            </div>
            <div class="modal-body">
                <form name="protectCaseForm" id="protectCaseForm">
                    <p>This client card is protected.Please enter the password.</p>
                    <div class="form-group">
                        <input type="password" id="pro_propassword" name="pro_propassword" placeholder="Password" class="form-control" />
                    </div>
                    <div class="form-group text-center marg-top15">
                        <a class="btn btn-md btn-primary" id="protectpassbtn">Proceed</a>
                        <a class="btn btn-md btn-danger" id="protectpasscnbtn">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Protect Case Modal END-->

<script type="text/javascript">

    var lastData = 1;
    var arrayData = [];
    var dataHTML = '';
    var userListObject = {};
    var colorCode = {};
    var init = 0;
    var highlightwords = [];
    var isPaused = false;
    var protectedCaseId = 0;
    var popOverSettings;


    $(document).ready(function () {
        $('#btnRecent').addClass("active"); 
        $('#btnAll').removeClass("active"); 
        update_chats(1);
        getChatUser();
        var row = '';
        $('body').tooltip({selector: '[data-toggle="tooltip"]'});

        $('.rdate').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });

        $('.rdate1').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            startDate: new Date()
        });

        $('#searchBy').change(function () {
            var searchText = $('#searchBy').val().split('.');
            var message = '';
            var placeholder = '';

            if (searchText[1] == 'contact')
            {
                $("#searchText").addClass("contact");
            } else
            {
                $("#searchText").val('');
                $("#searchText").unmask();
                $("#searchText").removeAttr("maxlength");
                $("#searchText").removeClass("contact");
            }
            $('#searchText').datetimepicker('remove');
            switch (searchText[1]) {
                case 'first':
                    message = 'Enter all the Part of the First Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'last':
                    message = 'Enter all the Part of the Last Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'firm':
                    message = 'Enter all the Part of the Firm Name';
                    placeholder = 'Search By ie. XYZ';
                    break;
                case 'caseno':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'caseno_less':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'caseno_more':
                    message = 'Enter One Case Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'yourfileno':
                    message = 'Enter all or part of your own internal office file Number';
                    placeholder = 'Search By ie. 123';
                    break;
                case 'social_sec':
                    message = 'Enter all or part of the Social Security Number';
                    placeholder = 'Search By ie. 234-56-5898';
                    break;
                case 'followup':
                    message = 'Display case as per Follow up Dates';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true
                    });
                    break;
                case 'contact':
                    message = "Enter all or part of the phone number to appear anywhere within the card or firm's for telephone number";
                    placeholder = '(111)111-1111';
                    break;
                case 'birth_date':
                    message = 'Enter the Date of Birth';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'birth_month':
                    message = 'Enter the number for a month ie: 1=Jan, 2=Feb, 12=Dec, 0=All';
                    placeholder = 'Numbers 0-12 Range';
                    break;
                case 'dateenter':
                    message = 'Display case as per Entered Date';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'dateopen':
                    message = 'Display case as per Opened Date';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'dateclosed':
                    message = 'Display case as per Closed Date';
                    placeholder = 'MM/DD/YYYY';
                    $('#searchText').datetimepicker({
                        format: 'mm/dd/yyyy',
                        minView: 2,
                        autoclose: true,
                        endDate: new Date(),
                    });
                    break;
                case 'casestat':
                    message = 'Enter Case Status';
                    placeholder = 'Search By ie. PNC';
                    break;
            }

            $('#searchText').attr("title", message);
            $('#searchText').attr("placeholder", placeholder);
            
           
        });
        $('#addtask.modal').on('show.bs.modal', function (e) {
                $(".select2Class#whoto").val("AEB").trigger("change");
                $("#addtask .select2Class#priority").val("2").trigger("change");
                $("#addtask .select2Class#color").val("1").trigger("change");
                $('.rmdt').css('display','none');
                
        });												
        $('#addtask.modal').on('hidden.bs.modal', function () {
                $('#addTaskForm')[0].reset();
                $('#addTaskForm1')[0].reset();
		$('.rtime').val('');
                $('#taskdatepicker-error').html('');
                $('#event-error').html('');
                
        });
        $('#addtask1.modal').on('hidden.bs.modal', function () {
                $('#addTaskForm')[0].reset();
                $('#addTaskForm1')[0].reset();
                $('#taskdatepickeredit-error').html('');
                $('#eventedit-error').html('');
                $('.rmdt').css('display','none');
        });
        $('#resetFilter').click(function () {
            $('#filterForm')[0].reset();
            $('.select2Class').trigger('change');

            return false;
        });

        popOverSettings = {container: 'body', html: true, selector: '[rel="popover"]', content: function () {
                return setSpinner();
            }};

        $('body').popover(popOverSettings).hover(function (e) {
            e.preventDefault();
        }, function (e) {});

        $('body').on('click', function (e) {
            if ($(e.target).data('toggle') !== 'popover'
                    && $(e.target).parents('.popover.in').length === 0) {
                $('[data-toggle="popover"]').popover('hide');
            }
        });

        casedataTable = $('.case-dataTable').DataTable({
            dom: 'l<"html5buttons"B>tr<"col-sm-5 no-left-padding"i><"col-sm-7 no-padding"p>',
            scrollX: true,
			stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            scrollY: "300px",
            scrollCollapse: true,
            buttons: [
                {extend: 'excel', text: '<i class="fa fa-file-excel-o"></i>  Excel', title: '<?php echo APPLICATION_NAME;?> : Search Cases'},
                {
                    extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i>  PDF', title: '<?php echo APPLICATION_NAME;?> : Search Cases', header: true, customize: function (doc) {
                        doc.styles.table = {
                            width: '100%'
                        };
                        doc.styles.tableHeader.alignment = 'left';
                        doc['header'] = function (page, pages) {
                            return {
                                columns: [
                                    '<?php echo APPLICATION_NAME;?> ',
                                    {
                                        alignment: 'right',
                                        text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                    }
                                ],
                                margin: [10, 0]
                            };
                        };
                        doc.content[1].table.widths = '*';
                    }
                },
                {extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i>  Print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            "pageLength": 5,
            "lengthMenu": [[5, 10, 20, 50, 100, 500, 1000, -1], [5, 10, 20, 50, 100, 500, 1000, "All"]],
            "processing": true,
            "serverSide": true,
            "bDeferRender": true,
            "order": [[0, "desc"]],
            "ajax": {
                url: "<?php echo base_url(); ?>cases/getDashboardCaseDataAjax",
                type: "POST"
            },
            "fnRowCallback": function (nRow, data, ) {
                if (data[8] == "Deleted")
                {
                    $('td', nRow).css('background-color', '#ff9999');
                }
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        if (row[7] == 1)
                        {
                            var protected = 1;
                        } else
                        {
                            var protected = 0;
                        }
                        return '<a href="javascript:void(0)" id="' + row[0] + '" rel="popover" data-pro="' + protected + '" data-container="body" data-toggle="popover" data-placement="right" data-popover-content="#data">' + data + '</a>';
                    },
                    "targets": 0
                },
                {
                    "targets": 0,
                    "className": "text-center",
                },
                {
                    "targets": 1,
                    "className": "text-left",
                },
                {
                    "targets": 2,
                    "className": "text-center",
                },
                {
                    "targets": 3,
                    "className": "text-center",
                },
                {
                    "targets": 4,
                    "className": "text-left",
                },
                {
                    "targets": 5,
                    "className": "text-left",
                },
            ]
        });
        casedataTable.responsive.recalc();
        $(document).on('click', '.case-dataTable tbody tr td:first-child a', function () {
            var proId = this.id;
            protectedCaseId = this.id;
            if ($(this).data('pro') == 1) {
                $('.popover-content').css('display', 'none');
                $('#protactcasepassword').modal('show');
                $('#protectpasscnbtn').click(function () {
                    $('#protactcasepassword').modal('hide');
                });
            } else {
                $('.popover .arrow').css('display', 'block');
                if (this.id.length > 0) {
                    $('[rel=popover]').not(this).popover('hide');
                    $.ajax({
                        type: "POST",
                        url: 'cases/getCaseDetails',
                        data: {id: this.id},
                        dataType: 'html',
                        context: this,
                        success: function (data) {
                            $('.popover-content').html(data);
                            $('.popover .arrow').css('display', 'none');
                        }
                    });
                }
            }
        });

        $('#pro_propassword').on('keypress', function (e) {
            if (e.which == 13)
            {
                $("#protectpassbtn").trigger("click");
            }
        });

        $(document).on('click', '#protectpassbtn', function () {
            var pass = $('#pro_propassword').val();
            $.ajax({
                type: "POST",
                url: 'cases/getProtectPass',
                data: {id: protectedCaseId, pass: pass},
                dataType: 'html',
                success: function (data) {
                    if (data == 1)
                    {
                        $('#protactcasepassword').modal('hide');
                        $('#pro_propassword').val('');
                        window.open("<?php echo base_url() ?>cases/case_details/" + protectedCaseId, '_blank');
                        return false;
                    }
                    if (data == 0)
                    {
                        swal("Invalid Password.", data.message, "error");
                        $('#protactcasepassword').modal('hide');
                        $('#pro_propassword').val('');
                    }
                }
            });
        });

        $(document).on('dblclick', '.case-dataTable tbody tr td:first-child a', function () {
            if ($(this).data('pro') == 1)
            {
                $('.popover-content').css('display', 'none');
                $('#protactcasepassword').modal('show');
                $('#protectpasscnbtn').click(function () {
                    $('#protactcasepassword').modal('hide');
                });
            } else
            {
                window.open("<?php echo base_url() ?>cases/case_details/" + this.id, '_blank');
                return false;
            }

        });
        $(document).on('click', '#close', function () {
            $("[rel=popover]").popover("destroy");
        });
        today_event = $('.today_event').DataTable({
            "processing": true,
            "serverSide": true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "asc"];
			},
            "scrollX": true,
            "bFilter": true,
            "order": [[0, "asc"]],
            "pageLength": 5,
            "lengthMenu": [5, 10, 20, 50, 100],
            "ajax": {
                url: "<?php echo base_url(); ?>dashboard/getTodayEventDataAjax",
                type: "POST"
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "className": "text-right"
                },
                {
                    "targets": 1,
                    "className": "text-center"
                },
                {
                    "targets": 2,
                    "className": "text-center"
                },
                {
                    "targets": 3,
                    "className": "text-left"
                },
                {
                    "targets": 4,
                    "className": "text-center"
                },
                {
                    "targets": 5,
                    "className": "text-center"
                },
                {
                    "targets": 6,
                    "className": "text-left"
                }
            ]
        });

        $('.edittask').on('click', function () {
            row = $(this).closest('tr').attr('id');
        });



        $(document.body).on('keypress', ".message-input", function (e) {
            if (e.which == 64) {
                init = 1;
            }
            if (e.which == 13)
            {
                if ($('textarea.message-input').val().trim() != '') {
                    $("#send").trigger("click");
                }
            }
            if (init == 1 && e.which == 32) {
                var myString = $('.message-input').val().split("@").pop();
                myString.toUpperCase();

                if (userListObject[myString.toUpperCase()]) {
                    var str = $('.message-input').val().replace('@' + myString, userListObject[myString.toUpperCase()]);
                    $('.message-input').val(str);

                    highlightwords.push(userListObject[myString.toUpperCase()]);
                    $('.message-input').highlightTextarea('destroy');
                    $('.message-input').highlightTextarea({
                        words: highlightwords
                    });
                    $('.message-input').focus();
                }
                init = 0;
            }
        });
        $('button#send').attr('disabled', true);
        $('textarea.message-input').keyup(function (e) {
            if ($('textarea.message-input').val().trim() != '') {
                $('button#send').attr('disabled', false);
            } else {
                $('button#send').attr('disabled', true);
            }
        });
        $('#searchText').on('keypress', function (e) {
            if (e.which == 13)
            {
                $("#btnSubmit").trigger("click");
                checksearched();
            }
        });
        $('.message-input').on('keyup', function (e) {
            var str = $('.message-input').val();
            if (e.which == 46 || e.which == 8) {
                if (str.match(/@\w+$/) != null) {
                    init = 1;
                }
            }
        });

        $('#searchText').on('blur', function () {
            dtValue = $(this).val();
            if (dtValue)
            {
                if ($('#searchBy').val() == 'cd.birth_date' || $('#searchBy').val() == 'c.dateenter' || $('#searchBy').val() == 'c.dateopen' || $('#searchBy').val() == 'c.dateclosed' || $('#searchBy').val() == 'c.followup')
                {
                    var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
                    if (!dtRegex.test(dtValue))
                    {
                        $(this).parent().append('<span style="color:red;">Please enter a valid date</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() == 'cd.birth_month')
                {
                    var dtRegex = new RegExp(/^[0-9]{0,2}$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $(this).parent().append('<span style="color:red;">Please enter a valid month</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() == 'cd.social_sec')
                {
                    var dtRegex = new RegExp(/^[0-9]{3}\-?[0-9]{2}\-?[0-9]{4}$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $('#searchText').parent().find('span').remove();
                        $(this).parent().append('<span style="color:red;">Please enter a valid Social Security Number</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }
                } else if ($('#searchBy').val() != 'cd.contact' && $('#searchBy').val() != 'c.yourfileno' && $('#searchBy').val() != 'cd2.firm' && $('#searchBy').val() != 'c.casestat') {
                    var dtRegex = new RegExp(/^\s*[a-zA-Z0-9,\s]+\s*$/);
                    if (!dtRegex.test(dtValue))
                    {
                        $('#searchText').parent().find('span').remove();
                        $(this).parent().append('<span style="color:red;">Please enter a valid input</span>');
                        setTimeout(function () {
                            $('#searchText').parent().find('span').remove();
                        }, 5000);
                        return false;
                    }

                }
            }
        });

        $('#btnSubmit').click(function () {
            isPaused = false;
            $('#searchText').trigger('blur');
            casedataTable.search($("#filterForm").serialize()).draw();
            return false;
            checksearched();
        });

        $('#send').click(function (e) {
            isPaused = true;
            var $field = $('textarea[name=message]');

            if ($('.highlightTextarea-highlighter').length > 0) {
                var data = $('.highlightTextarea-highlighter').html();
                $('.highlightTextarea-highlighter').remove();
            } else {
                var data = $field.val();
            }

            $field.addClass('disabled').attr('disabled', 'disabled');
            $('#send').addClass('disabled').attr('disabled', 'disabled');
            sendChat(data);
        });

        $('#chat-content').on('scroll', function (e) {
            if (this.scrollTop == 0) {
                $.ajax({
                    url: '<?php echo base_url() . "chat/get_messages" ?>',
                    method: "GET",
                    data: {'length': lastData},
                    dataType: 'json',
                    complete: function (jqXHR, textStatus) {
                        $('#chat-content').unblock();
                    },
                    success: function (result) {
                        if (result.length > 0)
                            lastData++;

                        dataHTML = append_chat_data(result);
                        var scHeight = $("#chat-content")[0].scrollHeight;

                        $("#chat-content").prepend(dataHTML);
                        if ($("#chat-content")[0].scrollHeight - scHeight > 0) {
                            $('#chat-content').scrollTop($("#chat-content")[0].scrollHeight - scHeight);
                        }
                    }
                });
            }
        });

        setInterval(function () {
            if (!isPaused) {
                update_chats(2);
            }
        }, 5000);
		
        $(document.body).on('change', '.weekendsalert', function () {
            var weekalert = "<?php echo isset($holidayset->weekends) ? $holidayset->weekends : 0 ?>";
            if (weekalert != 0 && weekalert == 'on') {
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var a = new Date($(this).val());
                if (weekday[a.getDay()] == 'Saturday' || weekday[a.getDay()] == 'Sunday')
                {
                    swal({
                        title: "",
                        text: "You are trying to add a task on non business day. Are you sure?",
                        customClass: 'swal-wide',
                        confirmButtonColor: "#1ab394",
                        confirmButtonText: "OK"
                    });
                }
            }
        });
        $(document.body).on('change click', '.alerttonholiday', function () {
            var aletholi = $(this).val();
            var aftholiday = "<?php echo isset($holidayset->aftholiday) ? $holidayset->aftholiday : 0 ?>";
            if (aftholiday != 0 && aftholiday == 'on' && $(this).val() != '') {
                var holidaylist = '<?php echo isset($holodaylist) ? json_encode($holodaylist) : 0; ?>';
                if (JSON.parse(holidaylist).length > 0) {
                    $.each(JSON.parse(holidaylist), function (key, value) {
                        var temp = new Array();
                        temp = value.split(",");
                        var c = new Date(temp[0]);
                        var b = new Date(aletholi);
                        if (c.getTime() == b.getTime()) {
                            swal({
                                title: "",
                                text: "You are trying to add a task on non business day. Are you sure?",
                                customClass: 'swal-wide',
                                confirmButtonColor: "#1ab394",
                                confirmButtonText: "OK"
                            });
                        }
                    });
                }
            }
        });
    });


    function getChatUser(call) {
        $.ajax({
            url: '<?php echo base_url() ?>chat/getUsers',
            method: "GET",
            data: {'timestamp': request_timestamp},
            dataType: 'json',
            success: function (result) {
                result.forEach(function (data) {
                    if (!userListObject[data.initials]) {
                        userListObject[data.initials] = data.fullname;
                    }
                });
            }
        });
    }
    function createUserHtml(recordData) {

        var userHtml = '';
        var dpColor = '';
        recordData.forEach(function (data) {

            if (!userListObject[data.initials]) {
                userListObject[data.initials] = data.fullname;
            }

            if (data.initials !== '<?php echo $loginUser; ?>') {

                if ($('#user-' + data.initials).length) {
                    $('#user-' + data.initials).remove();
                }

                dpColor = setColor(data.initials);
                userHtml += '<div class="chat-user" id="user-' + data.initials + '">';


                status = (data.isloggedon != null) ? '<span class="pull-right" style="color:green"><i class="fa fa-circle"></i></span>' : '';
                if (data.profilePic == '') {
                    userHtml += '<div class="chat-avatar chat-avatar-small" style="background-color:' + dpColor[data.initials] + '">' + data.initials + '</div>';
                } else {
                    userHtml += '<div class="chat-avatar chat-avatar-small" style="background-color:' + dpColor[data.initials] + '">' + data.initials + '</div>';
                }
                userHtml += '	<div class="chat-user-name">';
                userHtml += '           <a href="javascript:;"><span>' + data.initials + ' ' + status + '</span></a>';
                userHtml += '     </div>';
                userHtml += '</div>';
                '<div class="message-avatar">' + data.whofrom + '</div>';
            }
        });
        return userHtml;
    }

    function getUnique(array, newArray) {
        $.each(array, function (i, e) {
            var count = $.grep(newArray, function (el, index) {
                return el.id == e.id;
            });

            if (count.length < 1) {
                newArray.push(e);
            }
        });

        return newArray;
    }

    function getOnlineUser() {
        $.ajax({
            url: '<?php echo base_url() ?>chat/getOnlineUser',
            method: "GET",
            dataType: 'json',
            success: function (result) {
                updateStatus(result);
            }
        });
    }

    function updateStatus(result) {
        $.each(userListObject, function (key, value) {
            if (result.indexOf(key) == -1) {
                $('#user-' + key + ' span.online-status').remove();
            } else {
                if ($('#user-' + key).find('.online-status').length < 1) {
                    $('#user-' + key).find('.chat-user-name').before('<span class="pull-right label label-primary online-status">Online</span>');
                }
            }
        });
    }


    function getColor() {
        return '#' + ('000000' + parseInt(Math.random() * (256 * 256 * 256 - 1)).toString(16)).slice(-6);
    }

    function setColor(data) {
        if (!colorCode[data]) {
            colorCode[data] = getColor();
        }
        return colorCode;
    }

    var sendChat = function (message) {
        $.ajax({
            url: '<?php echo base_url() . "chat/send_message" ?>',
            method: "POST",
            data: {'message': message, 'initials': '<?php echo $loginUser ?>'},
            dataType: 'json',
            beforeSend: function (xhr) {
                $('#chat-content').block()
            },
            complete: function (jqXHR, textStatus) {
                $('#send').removeClass('disabled');
                $('textarea[name=message]').val('').removeClass('disabled').removeAttr('disabled');
            },
            success: function (result) {
                update_chats('1', 'bottom');
                highlightwords = new Array();
            },
            error: function (jqXHR, exception) {
                var msg = '';

                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
            }
        });
    };

    var append_chat_data = function (chat_data) {
        var html = '';
        var uColor = '';

        chat_data.sort(function (a, b) {
            return new Date(a.datetime).getTime() - new Date(b.datetime).getTime()
        });

        chat_data.forEach(function (data) {
            var is_me = false;

            if (data.whofrom == '<?php echo $loginUser; ?>') {
                is_me = true;
            }

            uColor = setColor(data.whofrom);

            if (is_me) {
                html += '<div class="chat-message left">';
            } else {
                html += '<div class="chat-message right">';
            }
            if (data.profilepic == '') {
                html += '<div class="message-avatar chat-avatar-big" style="background-color:' + uColor[data.whofrom] + '">' + data.whofrom + '</div>';
            } else {
                html += '<div class="message-avatar chat-avatar-big" style="background-color:' + uColor[data.whofrom] + '">' + data.whofrom + '</div>';
            }

            html += '	<div class="message">';
            html += '           <a class="message-author" href="#">' + data.whofrom + '</a>';
            var nwdate = new Date((data.timestamp) * 1000);
            html += '<span class="message-date">' + moment(nwdate).format("MM/DD/YYYY, h:mm:ss a") + '</span>';
            html += '           <span class="message-content">' + data.message + '</span>';
            html += '	</div>';
            html += '</div>';

        });
        return html;
    };

    function IsScrollbarAtBottom() {
    }
    ;
    var update_chats = function (call) {

        if (typeof (lastData) == 'undefined') {
            lastData = 0;
        }
        if (typeof (request_timestamp) == 'undefined' || request_timestamp == 0) {
            request_timestamp = 0;
        }

        var isTop = IsScrollbarAtBottom();

        $.getJSON('<?php echo base_url(); ?>chat/get_messages?timestamp=' + request_timestamp, function (data) {
            dataHTML = '';
            if (data.length > 0) {
                isPaused = false;
                dataHTML = append_chat_data(data);

                var date = new Date((data[data.length - 1].timestamp) * 1000);
                $('small.msg-time').text(moment(date).format("dddd, MMMM Do YYYY, h:mm:ss a"));
                request_timestamp = data[data.length - 1].timestamp;
                $("#chat-content").append(dataHTML);

                if (isTop == true || call == 1) {
                    $('#chat-content').animate({scrollTop: $('#chat-content')[0].scrollHeight}, 1000);
                }
            }
            $('#chat-content').unblock();
        });
    };


    $('#casetype').change(function () {
        $("#btnSubmit").trigger("click");
        checksearched();
    });

    $('#atty_resp').change(function () {
        $("#btnSubmit").trigger("click");
        checksearched();
    });

    function setTaskValidation() {
        var a = $("#addTaskForm").validate({
            ignore: [],
            rules: {
                whofrom: {
                    required: true
                },
                whoto2: {
                    required: true
                },
                datereq: {
                    required: true
                },
                event: {
                    required: true,
                    noBlankSpace: true
                }
            }
        });
        return a;
    }

    /*Today's task script [start] */
    $(document).ready(function () {

        setTaskValidation();
        todaytasksTable = $('.today_tasks').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [3, "ASC"];
			},
            scrollX: true,
            scrollY: "330px",
            scrollCollapse: true,
            bFilter: true,
            order: [
                [3, "ASC"]
            ],
            destroy: true,
            pageLength: 5,
            searchable: true,
            lengthMenu: [5, 10, 20, 50, 100],
            dom: 'lf<"html5buttons"B>tr<"col-sm-5 no-left-padding"i><"col-sm-7 no-padding"p>',
            buttons: [
                {extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Excel', title: '<?php echo APPLICATION_NAME;?> : Search Tasks',
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                },

                {extend: 'pdf',
                    text: '<i class="fa fa-file-pdf-o"></i> PDF',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    },

                },
                {extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print',
                    orientation: 'portrait',
                    exportOptions: {
                        columns: ':not(:last-child)'
                    },
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            ajax: {
                url: "<?php echo base_url(); ?>cases/getTodayTask",
                type: "POST"
            },
            fnRowCallback: function (nRow, aData) {
                if (aData[aData.length - 2] != "") {
                    var b = aData[aData.length - 1].split("-");
                    if (b[0] != "") {
                        $(nRow).css({
                            "background-color": b[0],
                            color: b[1]
                        });
                    } else {
                        $(nRow).css({
                            "background-color": "#ffffff",
                            color: "#000000"
                        });
                    }
                }
            },

            columnDefs: [{
                    "width": "5%",
                    "targets": 0
                },
                {
                    "width": "5%",
                    "targets": 1
                },
                {
                    "width": "35%",
                    "targets": 2
                },
                {
                    "width": "25%",
                    "targets": 3
                },
                {
                    "width": "30%",
                    "textalign": "center",
                    "targets": 4,
                    "bSortable": false,
                    "bSearchable": false

                }

            ]
        });

        todaytasksTable.draw();


        $('#addTask').on('click', function () {
    			
    	    var finishdatereq = $('#taskdatepickern').val();
    		
            var reminderdate = new Date($('.rdate').val());
    	    var finishdate = new Date($('#taskdatepickern').val());
    		
    		var reminderdatemilliseconds = reminderdate.getTime(); 
    		var finishdatemilliseconds = finishdate.getTime(); 
    		var event = $("#event").val();

            if(finishdatereq =='' && event != '')
            {
                $('#taskdatepicker-error').html('This field is required');
                $('#event-error').html('');
                return false;
            } 
            else if(finishdatereq =='' && event == '')
            {
                $('#taskdatepicker-error').html('This field is required');
                $('#event-error').css('display','block');
                $('#event-error').html('This field is required');
                return false;
            } 
            else if($.trim(event) == '' && finishdatereq !='')
            {
                $('#event-error').css('display','block');
                $('#event-error').html('This field is required');
                $('#taskdatepicker-error').html('');
                return false;
            }
            else{
                $('#event-error').html('');
                $('#taskdatepicker-error').html('');
            }
    		if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
                    if($('.rdate').val()=='' || $('.rtime').val()=='')
    				{
    					$('.rdateerror').text('Please select Reminder date and time');
    					return false;
    				}
    			} 
    		
            if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) 
    		{
    			if (reminderdatemilliseconds > finishdatemilliseconds)
    			{
    				$('#taskdatepicker-error').html('Must be greater than Reminder Date.');
    				return false;
    			} 
    			else
    			{
    				console.log('Yes');
    			}
    		}
    		
            if ($('#addTaskForm').valid()) {
                $.blockUI();

                var url = '';
                if ($('input[name=taskid]').val() != '' && $('input[name=taskid]').val() != 0) {
                   url = '<?php echo base_url(); ?>tasks/editTask';
                } else {
                   url = '<?php echo base_url(); ?>tasks/addTask';
                }
               var caseno = $("#TaskcaseNo").val();
               $.ajax({
                    url: url,
                    method: "POST",
                    data: $('#addTaskForm').serialize() + '&caseno=' + caseno,
                    success: function (result) {
                        var data = $.parseJSON(result);
                        $.unblockUI();
                        if (data.status == '1') {
                            swal("Success !", data.message, "success");
                            $('#addTaskForm')[0].reset();
                            $('#addtask').modal('hide');
                            todaytasksTable.draw();
                        } else {
                            swal("Oops...", data.message, "error");
                        }
                    }
                });
            }
        });

        $(document.body).on('click', 'button.task-completed', function () {
            var taskId = $(this).data('taskid');
            var c = $('#' + taskId).attr("data-taskid");
            var d = $('#' + taskId).attr("data-event");
            var b = $('#' + taskId).attr("data-caseno");

            $("#case_event").val(d);
            $("#caseTaskId").val(taskId);
            $("#caseno").val(b);
            $('#taskNo').val(taskId);
            $("#TodayTaskCompleted").modal("show");
            $('#saveCompletedTask1').removeAttr("disabled");
            return false;
        });

        $("#addtask.modal").on("hidden.bs.modal", function () {
            $(this).find("h4.modal-title").text("Add a Task");
            $(this).find("#addTaskForm")[0].reset();
            $(this).find("select[name=color]").select2('val', '1');
            $(this).find("select[name=typetask]").select2('val', 'Task Type');
            $(this).find("select[name=typetask]").val('').trigger("change");
            $(this).find("select[name=phase]").select2('val', 'Select Phase');
            $(this).find("select[name=phase]").val('').trigger("change");
            $(this).find("select[name=phase]").val('').trigger("change");
            $(this).find("select2Class#whoto2").val($(".select2Class#whoto2 option:eq(0)").val()).trigger("change");
            var b = setTaskValidation();
            $(this).find("input[name=taskid]").val(0);
        });

        $('#TodayTaskCompleted').on('show.bs.modal', function () {
            $('#case_extra_event').val('');
			$(".select2Class#case_category").val("1").trigger("change");
        });

        $(document.body).on('click', 'button.task-completed-nocase', function () {
            var taskId = $(this).data('taskid');
            var c = $('#' + taskId).attr("data-taskid");
            var d = $('#' + taskId).attr("data-event");
            var b = $('#' + taskId).attr("data-caseno");
            $("#case_event").val(d);
            $("#caseTaskId").val(taskId);
            $("#caseno").val(b);
            $('#taskNo').val(taskId);
            $("#taskCompletePopup").modal("show");
            return false;
        });

        $(document.body).on('click', '#complete', function () {
            var taskId = $('#taskNo').val();
            var c = $('#' + taskId).attr("data-taskid");
            var d = $('#' + taskId).attr("data-event");
            var b = $('#' + taskId).attr("data-caseno");
            $("#taskCompletePopup").modal("hide");
            $("#case_event").val(d);
            $("#caseTaskId").val(taskId);
            $("#caseno").val(b);
            $("#addnew").val(0);
            newCompletedTask();
        });

        $(document.body).on('click', '#completewithnew', function () {
            $("#taskCompletePopup").modal("hide");
            var taskId = $('#taskNo').val();
            var c = $('#' + taskId).attr("data-taskid");
            var d = $('#' + taskId).attr("data-event");
            var b = $('#' + taskId).attr("data-caseno");
            $("#case_event").val(d);
            $("#caseTaskId").val(c);
            $("#caseno").val(b);
            $("#addnew").val(1);
            newCompletedTask();
        });


        function newCompletedTask() {
            var e = $("#caseTaskId").val();
            var a = $("#case_event").val();
            var d = $("#case_extra_event").val();
            var b = $("#case_category").val();
            var c = $("#caseno").val();
            var n = $("#addnew").val();
            $(".case_No").val(c);

            $.blockUI();
            $.ajax({
                url: "<?php echo base_url(); ?>tasks/completedTaskWithCase",
                data: {
                    taskId: e,
                    case_event: a,
                    case_extra_event: d,
                    case_category: b,
                    caseno: c
                },
                method: "POST",
                success: function (f) {
                    $.unblockUI();
                    todaytasksTable.draw();
                    var g = $.parseJSON(f);
                    if (g.status == 1) {
                        swal({
                            title: "Success!",
                            text: g.message,
                            type: "success"
                        }, function () {
                            if (n == 1) {
                                $('#addtask').modal("show");
                            }
                        });
                        $("#addnew").val('0');
                        $('#saveCompletedTask').removeAttr("disabled");
                    } else if (g.status == 2) {
                        if (n == 0) {
                            swal({
                                title: "Warning!",
                                text: '<?php echo AddNewTaskCase; ?>',
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Add new",
                                closeOnConfirm: true
                            }, function () {
                                $('#addtask').modal("show");
                            });
                        } else if (n == 1) {
                            swal({
                                title: "Success!",
                                text: g.message,
                                type: "success"
                            }, function () {
                                if (n == 1) {
                                    $('#addtask').modal("show");
                                }
                            });
                        }
                    }
                    $("#TaskCompleted").modal("hide");
                }
            });
        }

        $(document.body).on("click", "#saveCompletedTask1", function () {
            if ($(this).text() == 'Complete and Add New')
                $("#addnew").val(1);
            else
                $("#addnew").val(0);
            $('#saveCompletedTask1').attr('disabled', 'disabled');
            var e = $("#caseTaskId").val();
            var a = $("#case_event").val();
            var d = $("#case_extra_event").val();
            var b = $("#case_category").val();
            var c = $("#caseno").val();
            var n = $("#addnew").val();
            $(".case_No").val(c);

            $.blockUI();
            $.ajax({
                url: "<?php echo base_url(); ?>tasks/completedTaskWithCase",
                data: {
                    taskId: e,
                    case_event: a,
                    case_extra_event: d,
                    case_category: b,
                    caseno: c
                },
                method: "POST",
                success: function (f) {
                    $.unblockUI();
                    todaytasksTable.draw();
                    var g = $.parseJSON(f);
                    if (g.status == 1) {
                        swal({
                            title: "Success!",
                            text: g.message,
                            type: "success"
                        }, function () {
                            if (n == 1) {
                                $('#addtask').modal("show");
                            }
                        });
                        $("#addnew").val('0');
                        $('#saveCompletedTask1').removeAttr("disabled");
                    } else if (g.status == 2) {
                        if (n == 0) {
                            swal({
                                title: "Warning!",
                                text: '<?php echo AddNewTaskCase; ?>',
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Add new",
                                closeOnConfirm: true
                            }, function () {
                                $('#addtask').modal("show");
                                $("#TaskcaseNo").val(c);
                            });
                        } else if (n == 1) {
                            swal({
                                title: "Success!",
                                text: g.message,
                                type: "success"
                            }, function () {
                                if (n == 1) {
                                    $('#addtask').modal("show");
                                    $("#TaskcaseNo").val(c);
                                }
                            });
                        }
                    }
                    $("#TodayTaskCompleted").modal("hide");
                }
            });
        });

        $("#taskdatepickern").datepicker({
            dateformat: "mm/dd/yyyy",
            showOn: "button",
            minDate: 0,
            buttonImage: false,
            buttonText: '<i class="fa fa-calendar" aria-hidden="true"></i>',
            buttonImageOnly: false
        });
        $("#taskdatepicker2").datepicker({
            dateformat: "mm/dd/yyyy",
            showOn: "button",
            minDate: 0,
            buttonImage: false,
            buttonText: '<i class="fa fa-calendar" aria-hidden="true"></i>',
            buttonImageOnly: false
        });
        $('#taskdatepickern').on('change', function () {
            var today = new Date();
            var ad = $(this).val();
            if (isValidDate(ad)) {
                $('#taskdatepickern').val(moment(ad).format('MM/DD/YYYY'));
            } else {
                var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                $('#taskdatepickern').val(moment(new_date).format('MM/DD/YYYY'));
                $('.weekendsalert').trigger("change");
            }
        });
        $('#taskdatepicker2').on('change', function () {
            var today = new Date();
            var ad = $(this).val();
            if (isValidDate(ad)) {
                $('#taskdatepicker2').val(moment(ad).format('MM/DD/YYYY'));
            } else {
                var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                $('#taskdatepicker2').val(moment(new_date).format('MM/DD/YYYY'));
                $('.weekendsalert').trigger("change");
            }
        });
    });

    function deleteTask(a) {
        swal({
            title: "Alert",
            text: "Are you sure to Remove this Task? ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.blockUI();
            $.ajax({
                url: "<?php echo base_url(); ?>tasks/deleteRec",
                data: {
                    taskId: a
                },
                method: "POST",
                success: function (b) {
                    $.unblockUI();
                    var c = $.parseJSON(b);
                    if (c.status == 1) {
                        todaytasksTable.draw();
                        swal("Success !", c.message, "success");
                    } else {
                        swal("Oops...", c.message, "error");
                    }
                }
            });
        });
    }

    function editTask(a) {
        $.blockUI();
        $.ajax({
            url: "<?php echo base_url(); ?>tasks/getTask",
            data: {
                taskId: a
            },
            method: "POST",
            success: function (b) {
                var c = $.parseJSON(b);
                $.unblockUI();
                if (c.status == 0) {
                    swal("Oops...", c.message, "error");
                } else {
                    $("#addtask1 .modal-title").html("Edit Task");
                    $("input[name=taskid]").val(a);
                    $("input[name=caseno]").val(c.caseno);
                    $("select[name=whofrom]").val(c.whofrom);
                    $("select[name=whoto3]").val(c.whoto);
                    if(c.datereq != "0000-00-00 00:00:00") {
                        $("input[name=datereq]").val(c.datereq);
                    } else {
                        $("input[name=datereq]").val("");
                    }
                    $("textarea[name=event]").val(c.event);
                    $("select[name=color]").val(c.color);
                    $("select[name=priority]").val(c.priority);
                    $("select[name=typetask]").val(c.typetask);
                    $("select[name=phase]").val(c.phase);
					if(c.caseno > 0 && c.caseno != '')
					{ $('#pullcasebtn').html('<a title="Pull Case" target="_blank"  href="<?php echo base_url(); ?>cases/case_details/' + c.caseno + '"><button type="button" class="btn btn-md btn-primary">Pull Case</button></a>'); }
					else{ $('#pullcasebtn').html('');}
					
                    if (c.notify == 1) {
                        $("input[name=notify]").iCheck("check");
                    } else {
                        $("input[name=notify]").iCheck("uncheck");
                    }
                    if (c.reminder == 1) {
                        $('input[type="checkbox"][name="reminder1"]').prop("checked", true).change();
                        $("input[name=rdate]").val(c.popupdate);
                        $("input[name=rtime]").val(c.popuptime);
                        $(".rmdt").show();
                    } else {
                        $('input[type="checkbox"][name="reminder1"]').prop("checked", false).change();
                        $(".rmdt").hide();
                    }
                    $("#addtask1").modal({
                        backdrop: "static",
                        keyboard: false
                    });
                    $("#addtask1").modal("show");
                    $("#addTaskForm1 select").trigger("change");
                }
            }
        });
    }

    function showhidedt1() {
        if ($('input[type="checkbox"][name="reminder1"]').is(":checked") == true) {
            $('input[type="checkbox"][name="reminder1"]').val("on");
            $(".rmdt1").show();
        } else {
            $(".rmdt1").hide();
            $(".rdate1").val("");
            $(".rtime").val("");
			$('.rdateerror').text('');
        }
    }

    function showhidedt() {
        if ($('input[type="checkbox"][name="reminder"]').is(":checked") == true) {
            $('input[type="checkbox"][name="reminder"]').val("on");
            $(".rmdt").show();
        } else {
            $(".rmdt").hide();
            $(".rdate").val("");
            $(".rtime").val("");
            $('.rdateerror').text('');
        }
    }
    $(document.body).on("click", "#addTask1", function () {
        
        if($('#taskdatepicker2').val() == '') {
            var today = new Date();
            var ad = $(this).val();
            if (isValidDate(ad)) {
                $('#taskdatepicker2').val(moment(ad).format('MM/DD/YYYY'));
            } else {
                var new_date = moment(today, "mm/dd/yyyy").add(ad, 'days');
                $('#taskdatepicker2').val(moment(new_date).format('MM/DD/YYYY'));
                $('.weekendsalert').trigger("change");
            }
        }
        
        var eventedit = $("#eventedit").val();
        var rdateedit = $("#taskdatepicker2").val();
	    
		var reminderdateedit = new Date($('.rdate1').val());
	    var finishdateedit = new Date($('#taskdatepicker2').val());
		
		var reminderdateeditmilliseconds = reminderdateedit.getTime(); 
		var finishdateeditmilliseconds = finishdateedit.getTime(); 
		var event = $("#event").val();
        if(rdateedit =='')
        {
            $('#taskdatepickeredit-error').html('Please select Finish Date.');
            return false;
        }
        if(eventedit == '')
        {
            $('#eventedit-error').html('This field is required');
            return false;
        }
        if($('input[type="checkbox"][name="reminder1"]').is(":checked") == true) {
			if($('.rdate1').val()=='' || $('#rtimeup').val()=='')
			{
				$('.rdateerror').text('Please select Reminder date and time');
				return false;
			}
		}
        if ($('input[type="checkbox"][name="reminder1"]').is(":checked") == true) 
		{
			if (reminderdateeditmilliseconds > finishdateeditmilliseconds)
			{
				$('#taskdatepickeredit-error').html('Must be greater than Reminder Date.');
				return false;
			} 
			else
			{
				console.log('Yes');
			}
		}

		
        if ($("#addTaskForm1").valid()) {
            $.blockUI();
            var a = "";
            if ($("input[name=taskid]").val() != "" && $("input[name=taskid]").val() != 0) {
                a = "<?php echo base_url(); ?>tasks/editTask";
            } else {
                a = "<?php echo base_url(); ?>tasks/addTask";
            }
            $.ajax({
                url: a,
                method: "POST",
                data: $("#addTaskForm1").serialize() + "&prosno=" + $(".task_list").attr("data-proskey_id"),
                success: function (b) {
                    var c = $.parseJSON(b);
                    $.unblockUI();
                    if (c.status == "1") {
                        swal({
                            title: "Success!",
                            text: c.message,
                            type: "success"
                        }, function () {
                            $("#addTaskForm1")[0].reset();
                            $("#addtask1").modal("hide");
                            $("#addtask1 .modal-title").html("Add a Task");
                            todaytasksTable.draw();
                        });
                        $('.whoto').removeClass("active");
                        $('.whofrom').addClass("active");
                    } else {
                        swal("Oops...", c.message, "error");
                    }
                }
            });
        }
        function isValidDate(dateStr) {
            var datePat = /^(\d{2,2})(\/)(\d{2,2})\2(\d{4}|\d{4})$/;

            var matchArray = dateStr.match(datePat);
            if (matchArray == null) {
                return false;
            }

            month = matchArray[1];
            day = matchArray[3];
            year = matchArray[4];
            if (month < 1 || month > 12) {
                return false;
            }
            if (day < 1 || day > 31) {
                return false;
            }
            if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
                return false;
            }
            if (month == 2) {
                var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                if (day > 29 || (day == 29 && !isleap)) {
                    return false;
                }
            }
            return true;
        }
    });
    $(document).ready(function () {
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
            window.history.pushState(null, "", window.location.href);
        };
    });
    function remove_error(id) {
        var myElem = document.getElementById(id + '-error');
        if (myElem === null) {

        } else {
            document.getElementById(id + '-error').innerHTML = '';
        }
    }
	
    /*Today's task script [end] */
$('.rdate').on('show.bs.modal', function (e) {
    if (e.stopPropagation) e.stopPropagation();
});

$('#btnRecent').on('click', function() {
    $('#btnRecent').addClass("active"); 
    $('#btnAll').removeClass("active"); 
    $('#valueRecent').val('recent');
    $("#btnSubmit").trigger("click");
});

$('#btnAll').on('click', function() {
    $('#btnAll').addClass("active"); 
    $('#btnRecent').removeClass("active");
    $('#valueRecent').val('all');
    $("#btnSubmit").trigger("click");
});

function checksearched()
{
var searchin =  $('#valueRecent').val();
if(searchin == "all")
    {
        $('#btnAll').addClass("active"); 
        $('#btnRecent').removeClass("active");
    }else{
        $('#btnRecent').addClass("active");
        $('#btnAll').removeClass("active"); 
    }
}

$(document).ready(function() {
    $('#addcase.modal').on('hidden.bs.modal', function () {
        $('#addcase_new').addClass("addcase_n");
    });
});

$('.navbar-minimalize').on('click', function() {
    setTimeout(function () {
        casedataTable.columns.adjust();
        today_event.columns.adjust();
        todaytasksTable.columns.adjust();
    }, 100);
});
</script>

<style>
    div.dataTables_wrapper div.dataTables_info {
        white-space: pre-wrap !important;
    }
</style>
