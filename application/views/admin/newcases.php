<?php
$main1 = json_decode($firm['main1']);
//$ccudf1=json_decode($firm['ccudf1']);
// $yourfilen1=json_decode($firm['yourfilen1']);
// echo   $firm['ccfiletk'] ;
// print_r($main1);
//     exit;
$casetype = explode(',', $caseTypelist[0]['poptype']);
$casestatus = explode(',', $caseStatuslist[0]['popstatus']);
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a>Configure System Defaults</a> /
                <span>New Case</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New Case Defaults</h5>
                </div>
                <div class="ibox-content">
                    <form method="post" name="NewcaseForm" id="NewcaseForm" enctype="multipart/form-data" action="#">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Case Type</label>

                                    <select name="system_casetype" id="system_casetype" class="form-control select2Class" >
                                        <option value="">Select Case Type</option>
                                        <?php
                                        if (!empty($casetype)) {
                                            foreach ($casetype as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1; ?>" <?php echo ($v1 == $firm['casetype']) ? "selected" : ""; ?>><?php echo $v1; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Case Status</label>
                                    <select name="system_casestat" id="system_casestat" class="form-control select2Class" >
                                        <option value="">Select Case Type</option>
                                        <?php
                                        if (!empty($casestatus)) {
                                            foreach ($casestatus as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1; ?>" <?php echo ($v1 == $firm['casestat']) ? "selected" : ""; ?>><?php echo $v1; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label>New Case activity category default</label>
                                    <input type="number" min="1" class="form-control" name='system_main_necaactcatde' id='system_main_necaactcatde' value="<?php echo isset($main1->necaactcatde) ? $main1->necaactcatde : '' ?>" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Attorney Handling</label>
                                    <select name="system_attyhand" id="system_attyhand" class="form-control select2Class" >
                                        <option value="">Select Attorney Handling</option>
                                        <?php
                                        if (!empty($Stafflist)) {
                                            foreach ($Stafflist as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1['initials']; ?>" <?php echo ($v1['initials'] == $firm['atty_hand']) ? "selected" : ""; ?>><?php echo $v1['initials']; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>
                                    <!--<input type="text" class="form-control" maxlength="3" name='system_attyhand' id='system_attyhand' value="<?php echo isset($firm['atty_hand']) ? $firm['atty_hand'] : '' ?>"/>-->
                                </div>
                                <div class="form-group">
                                    <label>Paralegal Handling</label>
                                    <select name="system_parahand" id="system_parahand" class="form-control select2Class" >
                                        <option value="">Select Paralegal Handling</option>
                                        <?php
                                        if (!empty($Stafflist)) {
                                            foreach ($Stafflist as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1['initials']; ?>" <?php echo ($v1['initials'] == $firm['para_hand']) ? "selected" : ""; ?>><?php echo $v1['initials']; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>
                                    <!--<input type="text" class="form-control" maxlength="3" name='system_parahand' id='system_parahand' value="<?php echo isset($firm['para_hand']) ? $firm['para_hand'] : '' ?>" />-->
                                </div>
                                <div class="form-group">
                                    <label>Secretary Handling</label>
                                    <select name="system_sechand" id="system_sechand" class="form-control select2Class">
                                        <option value="">Select Secretary Handling</option>
                                        <?php
                                        if (!empty($Stafflist)) {
                                            foreach ($Stafflist as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1['initials']; ?>" <?php echo ($v1['initials'] == $firm['sec_hand']) ? "selected" : ""; ?>><?php echo $v1['initials']; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>
                                    <!--<input type="text" class="form-control" maxlength="3" name='system_sechand' id='system_sechand' value="<?php echo isset($firm['sec_hand']) ? $firm['sec_hand'] : '' ?>" />-->
                                </div>
                                <div class="form-group">
                                    <label>Attorney Responsible</label>
                                    <select name="system_attyresp" id="system_attyresp" class="form-control select2Class" >
                                        <option value="">Select Attorney Responsible</option>
                                        <?php
                                        if (!empty($Stafflist)) {
                                            foreach ($Stafflist as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1['initials']; ?>" <?php echo ($v1['initials'] == $firm['atty_resp']) ? "selected" : ""; ?>><?php echo $v1['initials']; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>
                                    <!--<input type="text" class="form-control"  maxlength="3" name='system_attyresp' id='system_attyresp' value="<?php // echo isset($firm['atty_resp'])?$firm['atty_resp']:''        ?>"/>-->
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group text-center m-t-md m-b-xs">
                                    <button type="button" id="savenewcase" class="btn btn-md btn-primary ">Save</button>
                                    <button type="submit" class="btn btn-primary btn-md btn-danger">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#savenewcase').on('click', function () {

        savetoadmin('NewcaseForm');
    });
    $(document).ready(function () {
        $(".select2Class").select2({});
    });

</script>