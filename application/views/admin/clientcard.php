<?php  $note= json_decode($firm['xnotes']); 
$main1=json_decode($firm['main1']);
?>
<div class="wrapper wrapper-content">
<div class="row">
    <div class="col-lg-12">
        <div class="breadcrumbs">
            <a>Configure System Defaults</a> /
            <span>Client Card Note Titles</span>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Client Card Note Titles</h5>
            </div>
            <div class="ibox-content">
                <form method="post" name="ClientcardForm" id="ClientcardForm" enctype="multipart/form-data" action="#">
              
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Note 1</label>
                                <input type="text" class="form-control" name="system_xnote_note1" id="system_xnote_note1" value="<?php echo isset($note->note1)?$note->note1:'' ?>" />
                            </div>
                            <div class="form-group">
                                <label>Note 2</label>
                                <input type="text" class="form-control" name="system_xnote_note2" id="system_xnote_note2" value="<?php echo isset($note->note2)?$note->note2:'' ?>" />
                            </div>
                            <div class="form-group">
                                <label>Note 3</label>
                                <input type="text" class="form-control"  name="system_xnote_note3" id="system_xnote_note3" value="<?php echo isset($note->note3)?$note->note3:'' ?>" />
                            </div>
                            <div class="form-group">
                                <label>Note 4</label>
                                <input type="text" class="form-control" name="system_xnote_note4" id="system_xnote_note4" value="<?php echo isset($note->note4)?$note->note4:'' ?>" />
                            </div>
                            <div class="form-group">
                                <label>Note 5</label>
                                <input type="text" class="form-control" name="system_xnote_note5" id="system_xnote_note5" value="<?php echo isset($note->note5)?$note->note5:'' ?>" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Note 6</label>
                                <input type="text" class="form-control" name="system_xnote_note6" id="system_xnote_note6" value="<?php echo isset($note->note6)?$note->note6:'' ?>"  />
                            </div>
                            <div class="form-group">
                                <label>Note 7</label>
                                <input type="text" class="form-control" name="system_xnote_note7" id="system_xnote_note7" value="<?php echo isset($note->note7)?$note->note7:'' ?>" />
                            </div>
                            <div class="form-group">
                                <label>Note 8</label>
                                <input type="text" class="form-control" name="system_xnote_note8" id="system_xnote_note8" value="<?php echo isset($note->note8)?$note->note8:'' ?>" />
                            </div>
                            <div class="form-group">
                                <label>Note 9</label>
                                <input type="text" class="form-control" name="system_xnote_note9" id="system_xnote_note9" value="<?php echo isset($note->note9)?$note->note9:'' ?>"  />
                            </div>
                            <div class="form-group">
                                <label>Note 10</label>
                                <input type="text" class="form-control" name="system_xnote_note10" id="system_xnote_note10" value="<?php echo isset($note->note10)?$note->note10:'' ?>"  />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Default notes to use for extended notes and prospects (style,type,number) </label>
                                <textarea class="form-control" style="width: 100%;height: 150px;" name="system_xnotedef" id="system_xnotedef"><?php echo isset($firm['xnotedef'])?$firm['xnotedef']:'' ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Open With</label>
                                <select class="form-control" name="system_main_openwith" id="system_main_openwith">
                                    <option value="A1law"> A1 Law</option>
                                    <option value="wordpad">Wordpad</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group text-center m-t-md m-b-xs">
                                <button type="button" id='saveclientcard'class="btn btn-primary btn-md">Save</button>
                                <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <script>
        $('#saveclientcard').on('click', function () {
         savetoadmin('ClientcardForm');
     
    });      
        </script>
        