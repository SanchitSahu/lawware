<div class="middle-box loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name text-center">Lw</h1>
        </div>
        <h3 class="text-center">Reset Your Password</h3>
        <form class="m-t" name="resetpassForm" id="resetpassForm" role="form" method="post" action="<?php echo base_url().'admin/admin/resetyourpass' ?>" >
            <center><div id="SuccessMessage" style="padding-bottom:10px;"></div></center>
            <div class="form-group">
                <input name="password" id="password" onBlur="clear_error('password','passError')" type="password" class="form-control" placeholder="New Password" >
                <input type="hidden" name="fpasslink" id="fpasslink" value="<?php echo $this->uri->segment(4); ?>">
                <span id="passError" style="color:#FF0000 !important;"></span>
            </div>
            <div class="form-group">
                <input name="cpassword" id="cpassword" type="password" onBlur="clear_error('cpassword','cpassError')" class="form-control" placeholder="Confirm Password" >
                <span id="cpassError" style="color:#FF0000 !important;"></span>
            </div>

            <button type="button" onclick="javascript: return checkValidation()" class="btn btn-primary block full-width m-b">Reset</button>
        </form>
    </div>
</div>
<script>

    function checkValidation(){

       var password = document.getElementById("password").value;
       var cpassword = document.getElementById("cpassword").value;
       var fstr = document.getElementById("fpasslink").value;
        if(password.length == 0)
        {
            document.getElementById("passError").innerHTML = 'Please enter your new password.';
        }
        else
        {
           document.getElementById("passError").innerHTML =  '';
        }

        if(cpassword.length == 0)
        {
             document.getElementById("cpassError").innerHTML = 'Please enter confirm password.';
        }
        else
        {
            document.getElementById("cpassError").innerHTML = '';
        }

        if(password.length != 0 && cpassword.length != 0)
        {
            document.getElementById("passError").innerHTML = "";
            if(password != cpassword)
            {
                document.getElementById("cpassError").innerHTML = 'Password and confirm password should be same.';
            }
            else
            {
                $.ajax(
                {
                    type:"post",
                    url: "<?php echo base_url(); ?>admin/admin/changepass",
                    data:{ pass:password,fstr:fstr},
                    success:function(response)
                    {
                        if (response == 1)
                        {
                              window.location.href = "<?php echo base_url(); ?>admin";
                        }
                        else
                        {
                            document.getElementById("SuccessMessage").innerHTML = "<span style='color:#FF0000'><h4>This link is expired.</h4></span>";

                        }
                    }
                });
            }
        }
    }

    function clear_error(id,eid)
    {
        var fieldval = document.getElementById(id).value;
        if(fieldval.length > 0)
        {
            document.getElementById(eid).innerHTML = '';
        }
    }
</script>

