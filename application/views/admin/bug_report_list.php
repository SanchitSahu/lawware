<style>    a.btn.btn-default.buttons-excel.buttons-html5{margin-right: 5px;
    border-radius: 5px !important;float: right;}
    
    a.btn.btn-default.buttons-pdf.buttons-html5{margin-right: 5px;
    border-radius: 5px !important;float: right;}
    a.btn.btn-default.buttons-print{border-radius: 5px !important;float: right ;}
    .dt-buttons.btn-group{float: right;width: 100%;margin-bottom: 20px;}</style>
<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default firm-list" style="padding: 20px 30px; margin-top: 20px; ">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2 style="margin-top:0px;    margin-bottom: 20px; ">Bug Report List</h2>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo base_url()?>admin/bug_report" class="btn btn-primary btn-sm pull-right marg-right5">Raise Bug</a>
                        </div>
                    </div>
                </div>
        
   <div class="row">
    <div class="col-sm-12 col-md-12">
        <?php echo $this->session->flashdata('success_msg'); ?>
<?php echo $this->session->flashdata('error_msg'); ?>
    <div class="table-responsive">
    <table class="table table-bordered table-hover dataTables-example" id="bug_report_list">
        <thead>
        <tr>
            <th>No</th>
            <th>Firm name</th>
            <th>Summary</th>
            <th>Date</th>
            <th>Status</th>  
            <th style="min-width: 65px; max-width: 65px;">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php 
        if(isset($buglist) && !empty($buglist)) {
        foreach ($buglist as $article){ ?>
        <tr>
            <td><?php echo ++$i ;?></td>
            <td><?php echo $article->firmname;?></td>
            <td><?php echo $article->summary;?></td>
            <td><?php echo $article->datecreated;?></td>
            <td><?php if($article->status=='0') { echo 'Pending'; } elseif ($article->status==1) { echo "Viewed"; } elseif ($article->status==2){ echo 'Resolved';} else{ echo 'On Hold';} ;?></td>
            <td>
                <button class="btn btn-info" id="bug_view" data-id="<?php echo $article->id;?>">View</button>
            </td>
         </tr>
        <?php } }
        else {
            echo "<tr><td colspan='6' class='text-center'>No Data Found.</td></tr>";
        } ?>
        </tbody>
    </table>
    </div>
</div></div>
</div></div></div>
          <!-- view bug details -->
  <div id="bug_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="bug_firm_name"></h4>
      </div>
      <div class="modal-body">
        <div id="bug_modal_body"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
          
<script type="text/javascript">
    var bugreportlist = '';
    
    $(document).ready(function () {
        grouptable = $('#bug_report_list').DataTable({
                dom : "B<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'<'#colvis'>p>>",
                buttons: [
                    {extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i>  Print',
                        exportOptions: {
                        columns: ':not(:last-child)'
                        },
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    },
                    {
                        extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i>  PDF', title: '<?php echo APPLICATION_NAME; ?> : Bug Report List', header: true, 
                exportOptions: {
                          columns: ':visible',
                          columns: ':not(:last-child)'
                        },

                customize: function (doc) {

                            doc.styles.table = {
                                width: '100%'
                            };
                            doc.styles.tableHeader.alignment = 'left';
                            doc['header'] = function (page, pages) {
                                return {
                                    columns: [
                                        '<?php echo APPLICATION_NAME; ?> ',
                                        {
                                            alignment: 'right',
                                            text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                        }
                                    ],
                                    margin: [10, 0]
                                };
                            };
                            doc.content[1].table.widths = '*';
                        }
                    },
                    {extend: 'excel', text: '<i class="fa fa-file-excel-o"></i>  Excel', title: '<?php echo APPLICATION_NAME; ?> : Bug Report List',
                       exportOptions: {
                            columns: ':not(:last-child)'
                        }
                    }

            ],
            pageLength: 5,
            searchable: true,
            lengthMenu: [5, 10, 20, 50, 100], scrollY: "330px",
            scrollCollapse: true
        });
            
    });
</script>