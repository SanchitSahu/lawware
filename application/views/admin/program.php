
<div class="wrapper wrapper-content">
<div class="row">
    <div class="col-lg-12">
        <div class="breadcrumbs">
            <a>Configure System Defaults</a> /
            <span>Program Menu</span>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Programs - Define Program Menu</h5>
            </div>
            <div class="ibox-content">
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <textarea class="form-control" style="width: 100%;height: 300px;"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group text-center m-t-md m-b-xs">
                                <button type="submit" class="btn btn-primary btn-md">Save</button>
                                <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
