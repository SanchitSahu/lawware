<?php //echo '<pre>'; print_r($profile[0]->initials); exit; ?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="<?php echo base_url().'dashboard'; ?>">Dashboard</a> /
                <span>Change Password</span>
            </div>

            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-2">Change Password</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-2" class="tab-pane active">
                        <div class="panel-body">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                           <div class="row" style="margin-top:20px;">
                                                <div class="col-md-2">
                                                    <label>Old Password</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <input type="password" name="oldpass" id="oldpass" class="form-control" value="" />
                                                    <span id="opassError" style="color:#FF0000 !important;"></span>
                                                </div>
                                           </div>
                                           <div class="row" style="margin-top:20px;">
                                                <div class="col-md-2">
                                                    <label>New Password</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <input type="password" name="pass" id="pass" class="form-control" value="" />
                                                    <span id="passError" style="color:#FF0000 !important;"></span>
                                                </div>
                                           </div>
                                           <div class="row" style="margin-top:20px;">
                                                <div class="col-md-2">
                                                    <label>Confirm Password</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="password" name="cpass" id="cpass" class="form-control" value="" />
                                                    <span id="cpassError" style="color:#FF0000 !important;"></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group text-center marg-top15 col-md-6">
                                    <button type="button" id="saveprofile" class="btn btn-md btn-primary" onclick="changePsaaword()">Update</button>
                                    <button type="button" id="cancel" class="btn btn-md btn-primary" data-dismiss="modal" onclick="location.reload()">Cancel</button>
                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--End tabs-container-->

        </div>

</div>

<script>


function changePsaaword()
{
    var opassword = document.getElementById("oldpass").value;
    var password = document.getElementById("pass").value;
    var cpassword = document.getElementById("cpass").value;

     if(opassword.length == 0)
     {
         document.getElementById("opassError").innerHTML = 'Please enter your old password.';
     }
     else
     {
        document.getElementById("opassError").innerHTML =  '';
     }

     if(password.length == 0)
     {
         document.getElementById("passError").innerHTML = 'Please enter your new password.';
     }
     else
     {
        document.getElementById("passError").innerHTML =  '';
     }

     if(cpassword.length == 0)
     {
          document.getElementById("cpassError").innerHTML = 'Please enter confirm password.';
     }
     else
     {
         document.getElementById("cpassError").innerHTML = '';
     }

        if(password.length != 0 && cpassword.length != 0)
        {
            document.getElementById("opassError").innerHTML = "";
            document.getElementById("passError").innerHTML = "";
            if(password != cpassword)
            {
                document.getElementById("cpassError").innerHTML = 'Password and confirm password should be same.';
            }
            else
            {
                $.ajax(
                {
                    type:"post",
                    url: "<?php echo base_url(); ?>admin/resetpassword",
                    data:{ oldpass:opassword , pass:password},
                    success:function(response)
                    {
                        if (response == 1 || response == '1')
                        {
                            location.reload();
                        }
                        else
                        {
                          document.getElementById("cpassError").innerHTML = "Old password does not matched.";
                        }
                    }

                });
            }
        }

}
</script>