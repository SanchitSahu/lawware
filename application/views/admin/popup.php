<?php
$main1 = json_decode($firm['main1']);
$casedt = '';
if (isset($firm['casedates'])) {
    $casedt = json_decode($firm['casedates']);
}
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a>Configure System Defaults</a> /
                <span>General</span>
            </div>
            <form method="post" name="PopupForm" id="PopupForm" enctype="multipart/form-data" action="#">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">Case</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">Staff</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Rolodex</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-7">Tasks</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4">Case Dates/Important Dates</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Type of Case</label>
                                                    <input type="text" class="form-control" name="system_poptype" id="system_poptype" value="<?php echo $firm['poptype']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Case Status</label>
                                                    <input type="text" class="form-control" name="system_popstatus" id="system_popstatus" value="<?php echo $firm['popstatus']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Side</label>
                                                    <input type="text"  class="form-control" name="system_main_side" id="system_main_side" value="<?php echo isset($main1->side) ? $main1->side : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary btn-md" >Law Library Groups</button>
                                                    <button type="button" class="btn btn-primary btn-md" >Use Defaults</button>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Sub-Type</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Staff initials</label>
                                                    <?php $firm['popstaff'] = ($firm['popstaff'] != "") ? explode(',', $firm['popstaff']) : []; ?>
                                                    <select class="admin-select2  form-control" name="system_popstaff" id="system_popstaff" placeholder="" multiple>
                                                        <?php
                                                        if (!empty($Stafflist)) {
                                                            foreach ($Stafflist as $v1) :
                                                                ?>
                                                                <option value="<?php echo $v1['initials']; ?>" <?php echo (in_array($v1['initials'], $firm['popstaff'])) ? "selected" : ""; ?>>
                                                                    <?php echo $v1['initials']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                        }
                                                        ?>
                                                    </select>
                                                    <!--<input type="text" class="form-control" name="system_popstaff" id="system_popstaff" value="<?php echo $firm['popstaff']; ?>">-->
                                                </div>
                                                <div class="form-group">
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_casetypedd" id="sysyem_casetypedd" <?php echo (isset($firm['casetypedd']) && $firm['casetypedd'] == '1') ? 'checked' : ''; ?>> Type Case / Dropdown 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_casestatdd" id="system_casestatdd" <?php echo (isset($firm['casestatdd']) && $firm['casestatdd'] == '1') ? 'checked' : ''; ?>> Case Status / Dropdown
                                                        </label>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Attorney Responsible</label>
                                                    <?php $firm['popattyr'] = ($firm['popattyr'] != "") ? explode(',', $firm['popattyr']) : []; ?>
                                                    <select class="admin-select2  form-control" name="system_popattyr" id="system_popattyr" placeholder="" multiple>
                                                        <?php
                                                        if (!empty($Stafflist)) {
                                                            foreach ($Stafflist as $v1) :
                                                                ?>
                                                                <option value="<?php echo $v1['initials']; ?>" <?php echo (in_array($v1['initials'], $firm['popattyr'])) ? "selected" : ""; ?>>
                                                                    <?php echo $v1['initials']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Attorney Handling</label>
                                                    <?php $firm['popattyh'] = (isset($firm['popattyh']) && $firm['popattyh'] != "") ? explode(',', $firm['popattyh']) : []; ?>
                                                    <select class="admin-select2  form-control" name="system_popattyh" id="system_popattyh" multiple>
                                                        <?php
                                                        if (!empty($Stafflist)) {
                                                            foreach ($Stafflist as $v1) :
                                                                ?>
                                                                <option value="<?php echo $v1['initials']; ?>"  <?php echo (in_array($v1['initials'], $firm['popattyh'])) ? "selected" : ""; ?>>
                                                                    <?php echo $v1['initials']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Paralegal Handling</label>
                                                    <?php $firm['poppara'] = (isset($firm['poppara']) && $firm['poppara'] != "") ? explode(',', $firm['poppara']) : []; ?>
                                                    <select class="admin-select2  form-control" name="system_poppara" id="system_poppara" multiple>
                                                        <?php
                                                        if (!empty($Stafflist)) {
                                                            foreach ($Stafflist as $v1) :
                                                                ?>
                                                                <option value="<?php echo $v1['initials']; ?>" <?php echo (in_array($v1['initials'], $firm['poppara'])) ? "selected" : ""; ?>>
                                                                    <?php echo $v1['initials']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <?php $firm['popsec'] = (isset($firm['popsec']) && $firm['popsec'] != "") ? explode(',', $firm['popsec']) : []; ?>
                                                    <label>Secretary Handling</label>
                                                    <select class="admin-select2  form-control" name="system_popsec" id="system_popsec" multiple>
                                                        <?php
                                                        if (!empty($Stafflist)) {
                                                            foreach ($Stafflist as $v1) :
                                                                ?>
                                                                <option value="<?php echo $v1['initials']; ?>"  <?php echo (in_array($v1['initials'], $firm['popsec'])) ? "selected" : ""; ?>>
                                                                    <?php echo $v1['initials']; ?>
                                                                </option>
                                                                <?php
                                                            endforeach;
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_cdstaffdd" id="system_cdstaffdd" <?php echo (isset($firm['cdstaffdd']) && $firm['cdstaffdd'] == '1') ? 'checked' : ''; ?>> Dropdown for staff handling 
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Salutation</label>
                                                    <input type="text" class="form-control" name="system_popsal" id="system_popsal" value="<?php echo $firm['popsal']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input type="text" class="form-control" name="system_poptitle" id="system_poptitle" value="<?php echo $firm['poptitle']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks Rolodaxcheck " name="system_cdsaldd" id="system_cdsaldd" <?php echo (isset($firm['cdsaldd']) && $firm['cdsaldd'] == '1') ? 'checked' : ''; ?> > Salutation Dropdown 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label> 
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_cdsufdd" id="system_cdsufdd" <?php echo (isset($firm['cdsufdd']) && $firm['cdsufdd'] == '1') ? 'checked' : ''; ?>> Suffix Dropdown
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label> 
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_cardtypedd" id="system_cardtypedd" <?php echo (isset($firm['cardtypedd']) && $firm['cardtypedd'] == '1') ? 'checked' : ''; ?>> Card/Type Dropdown 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label> 
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_main_pstdown" id="system_main_pstdown" <?php echo (isset($main1->pstdown) && $main1->pstdown == '1') ? 'checked' : ''; ?>> Party Sheet Type Dropdown 
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Suffix</label>
                                                    <input type="text" class="form-control" name="system_rolosuffix" id="system_rolosuffix" value="<?php echo $firm['rolosuffix']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Card Type</label>
                                                    <input type="text" class="form-control" name="system_popclient" id="system_popclient" value="<?php echo $firm['popclient']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Doctor specialty for Rolodex</label>
                                                    <input type="text" class="form-control" name="system_specialty" id="system_specialty" value="<?php echo $firm['specialty']; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary btn-md">Phone Dropdown</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Case Dates</h5>
                                    </div>
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="hidden" name="system_casedates_def" id="system_casedates_def" value="dts">
                                                <div class="form-group">
                                                    <span>
                                                        <label> 
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_casedates_refdt" id="system_casedates_refdt" <?php echo (isset($casedt->refdt) && $casedt->refdt == 'on') ? 'checked' : ''; ?>> Referred Date
                                                        </label>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <span>
                                                        <label> 
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_casedates_ref" id="system_casedates_ref" <?php echo (isset($casedt->ref) && $casedt->ref == 'on') ? 'checked' : ''; ?>> Referred by 
                                                        </label>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <span>
                                                        <label> 
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_casedates_pns" id="system_casedates_pns" <?php echo (isset($casedt->pns) && $casedt->pns == 'on') ? 'checked' : ''; ?>> P & S (Is applicant P & S?)
                                                        </label>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks Rolodaxcheck" name="system_casedates_pnsdt" id="system_casedates_pnsdt" <?php echo (isset($casedt->pnsdt) && $casedt->pnsdt == 'on') ? 'checked' : ''; ?>> P & S Date 
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Case activity fee dropdown</label>
                                                    <input type="text" class="form-control" name="system_main_cafeedrop" id="system_main_cafeedrop" value="<?php echo isset($main1->cafeedrop) ? $main1->cafeedrop : ''; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Other Cases</label>
                                                    <input type="text" class="form-control" name="system_main_otherCases" id="system_main_otherCases" value="<?php echo isset($main1->otherCases) ? $main1->otherCases : ''; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks" name="system_main_othercasdrop" id="system_main_othercasdrop" <?php echo isset($main1->othercasdrop) ? $main1->othercasdrop == 'on' : ''; ?>> Other cases dropdown 
                                                        </label>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary btn-md">Teams</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-6" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks"> Enforce uniqueness 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks"> Enable your file number label 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks"> Disable your file number Textbox 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks"> Disable date entered 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks"> Disable date open 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks"> Enforce security 
                                                        </label>
                                                    </span>
                                                    <span>
                                                        <label>
                                                            <input type="checkbox" class="i-checks"> Eliminate spaces 
                                                        </label>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Format</label>
                                                    <textarea class="form-control" style="width: 100%;height: 100px;"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Default status</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Prompt</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Primary Status</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks"> Run program NewCase </label></span>
                                                    <button type="submit" class="btn btn-primary btn-md">Compile</button>
                                                </div>
                                                <div class="form-group">

                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks"> Show your file no </label></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Numbers</label>
                                                    <textarea class="form-control" style="width: 100%;height: 100px;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="tab-7" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Keep Task Complete Days</label>
                                                    <input type="number" class="form-control" name="keeptaskco" id="keeptaskco" value="<?php echo $firm['keeptaskco']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group text-center m-t-md m-b-xs">
                        <button type="button" id="savepopup" class="btn btn-md btn-primary savepopup">Save</button>
                        <button type="submit" class="btn btn-danger btn-md">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.date').datepicker({
            forceParse: false,
            autoclose: true
        });

        $('.color').colorpicker({
            autoclose: true
        });
        $('#savepopup').on('click', function () {
            var serialized = $('.Rolodaxcheck').map(function () {
                return {id: this.name, value: this.checked ? 1 : 0};
            });
            var system_casetypedd = serialized[0]['id'];
            var system_casetypeddval = serialized[0]['value'];

            var system_casestatdd = serialized[1]['id'];
            var system_casestatddval = serialized[1]['value'];

            var system_cdstaffdd = serialized[2]['id'];
            var system_cdstaffddval = serialized[2]['value'];

            var system_cdsaldd = serialized[3]['id'];
            var system_cdsalddval = serialized[3]['value'];

            var system_cdsufdd = serialized[4]['id'];
            var system_cdsufddval = serialized[4]['value'];

            var system_cardtypedd = serialized[5]['id'];
            var system_cardtypeddval = serialized[5]['value'];

            var system_main_pstdown = serialized[6]['id'];
            var system_main_pstdownval = serialized[6]['value'];

            var system_popattyrval = $("#system_popattyr").select2().val().toString();
            var system_popattyhval = $("#system_popattyh").select2().val().toString();
            var system_popparaval = $("#system_poppara").select2().val().toString();
            var system_popsecval = $("#system_popsec").select2().val().toString();
            var system_popstaffval = $("#system_popstaff").select2().val().toString();

            var formdata = $("#PopupForm").not(".Rolodaxcheck").serialize() + '&' + system_cdsaldd + '=' + system_cdsalddval + '&' + system_cdsufdd + '=' + system_cdsufddval + '&' + system_cardtypedd + '=' + system_cardtypeddval + '&' + system_main_pstdown + '=' + system_main_pstdownval + '&' + system_casetypedd + '=' + system_casetypeddval + '&' + system_casestatdd + '=' + system_casestatddval + '&' + system_cdstaffdd + '=' + system_cdstaffddval + '&' + 'system_popattyr' + '=' + system_popattyrval + '&' + 'system_popattyh' + '=' + system_popattyhval + '&' + 'system_poppara' + '=' + system_popparaval + '&' + 'system_popsec' + '=' + system_popsecval + '&' + 'system_popstaff' + '=' + system_popstaffval;

            $.ajax({
                url: HTTP_PATH + 'admin/updatefirm',
                method: 'POST',
                data: formdata,
                beforeSend: function (xhr) {
                    $.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    $.unblockUI();
                },
                success: function (result) {
                    var obj = $.parseJSON(result);
                    if (obj.status == '1') {
                        // setTimeout(function() { //alert("g");
                        swal("Success !", obj.message, "success", function () {
                            location.reload();
                        });
                        //  }, 500);
                    }
                }
            });
        });

    });
</script>