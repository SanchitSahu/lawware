                        <?php $main1=json_decode($firm['main1']);
                        $ccudf1=json_decode($firm['ccudf1']);
                        $yourfilen1=json_decode($firm['yourfilen1']);
                // echo   $firm['ccfiletk'] ;
                  // print_r($main1);
                 //       exit;

                        ?>
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                    <span>General</span>
                </div>

                <form method="post" name="GeneralForm" id="GeneralForm" enctype="multipart/form-data" action="#">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> General</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">Client Card</a></li>
<!--                        <li class=""><a data-toggle="tab" href="#tab-3">Email</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4">File Tk</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-5">Case Activity</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-6">Your File No</a></li>-->
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>General</h5>
                                    </div>
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Days to hold tmp files</label>
                                                    <input type="number" min="0" name="system_tempfiles" id="system_tempfiles" value="<?php echo $firm['tempfiles']; ?>" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>2K two digit year</label>
                                                    <input type="number" name="system_y2kroll"  id="system_y2kroll" value="<?php echo $firm['y2kroll']; ?>" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Retry locked records</label>
                                                    <input type="number" name="system_reprocess" id="system_reprocess" value="<?php echo $firm['reprocess']; ?>" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Inturrupt frequency</label>
                                                    <input type="number" name="system_interrupt1" id="system_interrupt1" value="<?php echo $firm['interrupt1'] ?>" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Picutre paste location</label>
                                                    <input type="text" name="system_pictpath" id="system_pictpath" value='<?php echo $firm['pictpath'] ?>' class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Startup batch filename</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Filename</label>
                                                    <input type="text" name="system_pictname" id="system_pictname"  value="<?php echo $firm['pictname'] ?>"class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Advanced WP for XNotes</label>
                                                    <select class="form-control" name="system_xnoteswp" id="system_xnoteswp">
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>User log</label>
                                                    <input type="number" name="system_main_userlog" value="<?php echo isset($main1->userlog)?$main1->userlog:''?>" id="system_main_userlog"  class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Reindex reminder</label>
                                                    <input type="number" class="form-control" name="system_main_rindexre" value="<?php echo isset($main1->rindexre)?$main1->rindexre:''?>" id="system_main_rindexre" >
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary btn-md">Document Tracker</button>
                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks"> Prior rolodex card after editing </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks"> Enable ActiveX dual support </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks"> Oriented to defense </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks"> User log auto update </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks"> Auto adjust windows </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks"> Never warn on qutting </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks"> Sync time with server </label></span>
                                                </div>
                                            </div>
<!--                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#tab-7">Page 1</a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-8">Page 2</a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-9">Messages</a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-10">Page 4</a></li>
<!--                                            <li class=""><a data-toggle="tab" href="#tab-11">Accounts</a></li>-->
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-7" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-content marg-bot15">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Order</label>
                                                                        <select class="form-control" name="system_order1" id="system_order1ss" >
                                                                            <option value="1">Yes</option>
                                                                            <option value="2">No</option>
                                                                        </select>
                                                                    </div>
<!--                                                                    <div class="form-group">
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_SUDF_check" id="system_SUDF_check"> Show UDF fields </label></span>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>UDF 1 Name / Field</label>
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <input type="text" class="form-control" name="system_SUDF_UDF1" id="system_SUDF_UDF1" value="<?php echo isset($ccudf1->UDF1)?$ccudf1->UDF1:'' ?>">
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <input type="number" class="form-control" name="system_SUDF_UDF1n" id="system_SUDF_UDF1n" value="<?php echo isset($ccudf1->UDF1n)?$ccudf1->UDF1n:'' ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>UDF 2 Name / Field</label>
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <input type="text" class="form-control" name="system_SUDF_UDF2" id="system_SUDF_UDF2"  value="<?php echo isset($ccudf1->UDF2)?$ccudf1->UDF2:'' ?>">
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <input type="number" class="form-control" name="system_SUDF_UDF2n" id="system_SUDF_UDF2n"  value="<?php echo isset($ccudf1->UDF2n)?$ccudf1->UDF2n:'' ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>UDF 3 Name / Field</label>
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <input type="text" class="form-control" name="system_SUDF_UDF3" id="system_SUDF_UDF3"  value="<?php echo isset($ccudf1->UDF3)?$ccudf1->UDF3:'' ?>">
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <input type="number" class="form-control" name="system_SUDF_UDF3n" id="system_SUDF_UDF3n"  value="<?php echo isset($ccudf1->UDF3n)?$ccudf1->UDF3n:'' ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>UDF 4 Name / Field</label>
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <input type="text" class="form-control" name="system_SUDF_UDF4" id="system_SUDF_UDF4"  value="<?php echo isset($ccudf1->UDF4)?$ccudf1->UDF4:'' ?>">
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <input type="number" class="form-control" name="system_SUDF_UDF4n" id="system_SUDF_UDF4n"  value="<?php echo isset($ccudf1->UDF4n)?$ccudf1->UDF4n:'' ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Top sheet Rolodex udf # </label>
                                                                        <input type="number" class="form-control">
                                                                    </div>-->
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Envelope bin</label>
                                                                        <input type="text" name="system_envbin" id="system_envbin" value="<?php echo $firm['envbin'] ?>"class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Envelope Format</label>
                                                                        <input type="number" name='system_envformat' id="system_envformat" value="<?php echo $firm['envformat']?>"class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Envelope Font / Size</label>
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <input type="text" class="form-control">
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <input type="number" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <span><label> <input type="checkbox" class="i-checks"> Prefix has priority </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks"> Disable Timer </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks"> Disable red alerts </label></span>
                                                                    </div>
                                                                </div>
<!--                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center m-t-md m-b-xs">
                                                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab-8" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-content marg-bot15">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_pwdp" id="system_main_pwdp" <?php if(isset($main1->pwdp) && $main1->pwdp=='on'){ echo 'checked'; } ?>> Propmt When deleting parties </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_nudc" id="system_main_nudc" <?php if(isset($main1->nudc) && $main1->nudc=='on'){ echo 'checked'; } ?>> Never update data closed </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_rosm" id="system_main_rosm"  <?php if(isset($main1->rosm) && $main1->rosm=='on'){ echo 'checked'; } ?>> Retain old search methods </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_ddti" id="system_main_ddti"  <?php if(isset($main1->ddti) && $main1->ddti=='on'){ echo 'checked'; } ?> > Display Detailed task info </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_scpp" id="system_main_scpp"  <?php if(isset($main1->scpp) && $main1->scpp=='on'){ echo 'checked'; } ?>> Show cell phone when possible </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_esiw" id="system_main_esiw"  <?php if(isset($main1->esiw) && $main1->esiw=='on'){ echo 'checked'; } ?>> End Special instruction with </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_snif" id="system_main_snif"  <?php if(isset($main1->snif) && $main1->snif=='on'){ echo 'checked'; } ?>> Show name instead of firms </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks ChackBox" name="system_main_scdo" id="system_main_scdo"  <?php if(isset($main1->scdo) && $main1->scdo=='on'){ echo 'checked'; } ?>> Suppress copying date entered to open </label></span>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Client card status line</label>
                                                                        <select class="form-control">
                                                                            <option></option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Font</label>
                                                                        <input type="number" class="form-control" name="system_main_font" id="system_main_font" value="<?php echo isset($main1->font)?$main1->font:'' ?>" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Caption style</label>
                                                                        <input type="number" class="form-control" name="system_main_Captionstyle" id="system_main_Captionstyle" value="<?php echo isset($main1->Captionstyle)?$main1->Captionstyle:'' ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Pick every</label>
                                                                        <input type="number" class="form-control" name="system_main_Pickevery" id="system_main_Pickevery" value="<?php echo isset($main1->Pickevery)?$main1->Pickevery:'' ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary btn-md">Verification before closing</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Party Details </button>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Macro</label>
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                </div>
<!--                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center m-t-md m-b-xs">
                                                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab-9" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-content marg-bot15">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Message 1</label>
                                                                        <input type="text" class="form-control msg11" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Message 2</label>
                                                                        <input type="text" class="form-control msg22" name="" id="">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Message 3</label>
                                                                        <input type="text" class="form-control msg33" name="" id="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label>Color 1</label>
                                                                        <input type="text" class="form-control color" name="system_main_msg1" id="system_main_msg1" value="<?php echo isset($main1->msg1)?$main1->msg1:'' ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label>Height 1</label>
                                                                        <input type="number" class="form-control" min="18" name="system_main_msg1h" id="system_main_msg1h" value="<?php echo isset($main1->msg1h)?$main1->msg1h:18;?>">
                                                                    </div>
                                                                </div>
                                                                 <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label class="font-color-edit">Font color 1</label><br>
                                                                         <label class="i-checks"> <input type="radio"  name="system_main_msg1c" id="system_main_msg1c" value="Black" <?php echo (isset($main1->msg1c)&& $main1->msg1c=='Black')?"checked" : "" ;  ?>> Black</label>
                                                                        <label class="i-checks"> <input type="radio"  name="system_main_msg1c" id="system_main_msg1c" value="White" <?php echo (isset($main1->msg1c)&&$main1->msg1c=='White')?"checked" : "" ;  ?>> White </label>
                                                                    </div>
                                                                </div>
                                                                </div>

                                                                <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label>Color 2</label>
                                                                        <input type="text" class="form-control color" name="system_main_msg2" id="system_main_msg2" value="<?php echo isset($main1->msg2)?$main1->msg2:'' ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label>Height 2</label>
                                                                        <input type="number" class="form-control" min="18" name="system_main_msg2h" id="system_main_msg2h" value="<?php echo isset($main1->msg2h)?$main1->msg2h:18;?>">
                                                                    </div>
                                                                </div>
                                                                 <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label class="font-color-edit">Font color 2</label><br>
                                                                        <label class="i-checks"> <input type="radio" value="Black" id="system_main_msg2c" name="system_main_msg2c" <?php  echo (isset($main1->msg2c)&& $main1->msg2c=='Black' )?"checked" : "" ; ?>> Black </label>
                                                                        <label class="i-checks"> <input type="radio" value="White" id="system_main_msg2c" name="system_main_msg2c" <?php  echo (isset($main1->msg2c)&& $main1->msg2c=='White' )?"checked" : "" ; ?>> White </label>

                                                                    </div>
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label>Color 3</label>
                                                                        <input type="text" class="form-control color" name="system_main_msg3" id="system_main_msg3" value="<?php echo isset($main1->msg3)?$main1->msg3:'' ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label>Height 3</label>
                                                                        <input type="number" class="form-control" min="18" name="system_main_msg3h" id="system_main_msg3h" value="<?php echo isset($main1->msg3h)?$main1->msg3h:18;?>">
                                                                    </div>
                                                                </div>
                                                                 <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label class="font-color-edit">Font color 3</label><br>
                                                                        <label class="i-checks"> <input type="radio" value="Black" id="system_main_msg3c" name="system_main_msg3c" <?php  echo (isset($main1->msg3c)&& $main1->msg3c=='Black' )?"checked" : "" ; ?>> Black </label>
                                                                        <label class="i-checks"> <input type="radio" value="White" id="system_main_msg3c" name="system_main_msg3c" <?php  echo (isset($main1->msg3c)&& $main1->msg3c=='White' )?"checked" : "" ; ?>> White </label>
                                                                    </div>
                                                                </div>
                                                                </div>
<!--                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center m-t-md m-b-xs">
                                                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div id="tab-10" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-content marg-bot15">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Next Follow up on client card (optional expression)</label>
                                                                        <input type="text" class="form-control" name="system_main_cardoptional" id="system_main_cardoptional" value="<?php echo isset($main1->cardoptional)?$main1->cardoptional:''?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Next Follow up on client card (label expression)</label>
                                                                        <input type="text" class="form-control" name="system_main_cardlabel" id="system_main_cardlabel"  value="<?php echo isset($main1->cardlabel)?$main1->cardlabel:'' ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Follow up Calendar</label>
                                                                        <input type="number" class="form-control" name="system_cdfolupcal" id="system_cdfolupcal"  value="<?php echo isset($firm['cdfolupcal'])?$firm['cdfolupcal']:'' ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Page 1 Case Dates</label>
                                                                        <input type="text" class="form-control date" name="system_main_pagecaseDate" id="system_main_pagecaseDate"  value="<?php echo isset($main1->pagecaseDate)?$main1->pagecaseDate:'' ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Quick Form</label>
                                                                        <input type="text" class="form-control" name="system_main_QuickForm" id="system_main_QuickForm"  value="<?php echo isset($main1->QuickForm)?$main1->QuickForm:'' ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Client card width</label>
                                                                        <input type="number" class="form-control" name="system_main_clientcw" id="system_main_clientcw"  value="<?php echo isset($main1->clientcw)?$main1->clientcw:'' ?>">
                                                                    </div>
                                                                </div>
<!--                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center m-t-md m-b-xs">
                                                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
<!--                                            <div id="tab-11" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-content marg-bot15">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Label</label>
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <span><label> <input type="checkbox" class="i-checks"> Top </label></span>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Layout</label>
                                                                        <select class="form-control">
                                                                            <option></option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Status</label>
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Edit format</label>
                                                                        <select class="form-control">
                                                                            <option></option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Type</label>
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary btn-md">Advanced</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center">
                                                                        <button type="submit" class="btn btn-primary btn-md">Accounts Dropdown</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Payments Dropdowns</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Account report</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>EAMS Disabled</label>
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Atty Disabled types</label>
                                                                        <input type="text" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center m-t-md m-b-xs">
                                                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#tab-12">Law</a></li>
                                            <li class=""><a data-toggle="tab" href="#tab-13">Microsoft Outlook</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-12" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-content marg-bot15">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Days to Hold Email</label>
                                                                        <input type="number" class="form-control" name="system_calendelet" id="system_calendelet"  value="<?php echo isset($firm['calendelet'])?$firm['calendelet']:'' ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Email to Activity</label>
                                                                        <input type="number" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <button type="button" class="btn btn-primary btn-md">Groups</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_aeir" id="system_main_aeir" <?php if(isset($main1->aeir) && $main1->aeir=='on'){ echo 'checked'; } ?>> Attach email in replies </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_eaemails" id="system_main_eaemails" <?php if(isset($main1->eaemails) && $main1->eaemails=='on'){ echo 'checked'; } ?>> Encode all Emails </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_srnmf" id="system_main_srnmf" <?php if(isset($main1->srnmf) && $main1->srnmf=='on'){ echo 'checked'; } ?>> Show recent new mails first </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_usti" id="system_main_usti" <?php if(isset($main1->usti) && $main1->usti=='on'){ echo 'checked'; } ?>> Use Sytem tray icons </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_asdemails" id="system_main_asdemails" <?php if(isset($main1->asdemails) && $main1->asdemails=='on'){ echo 'checked'; } ?>> Allow sender to delete email </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_shortpost" id="system_main_shortpost" <?php if(isset($main1->shortpost) && $main1->shortpost=='on'){ echo 'checked'; } ?>> Short post </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_keepan" id="system_main_keepan" <?php if(isset($main1->keepan) && $main1->keepan=='on'){ echo 'checked'; } ?>> Keep as New </label></span>
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_una1email" id="system_main_una1email" <?php if(isset($main1->una1email) && $main1->una1email=='on'){ echo 'checked'; } ?>> Use new Law email </label></span>
                                                                    </div>
                                                                </div>
<!--                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center m-t-md m-b-xs">
                                                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab-13" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="ibox float-e-margins">
                                                        <div class="ibox-content marg-bot15">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Enable / Disable</label>
                                                                        <select class="form-control" id="system_main_enadis" name="system_main_enadis">
                                                                            <option>Disable Outlook</option>
                                                                            <option>Enable Outlook Advanced</option>
                                                                            <option>Enable Outlook Standard</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Category to post to (ie: W-1, P-5)</label>
                                                                        <input type="text" class="form-control" id="system_main_ctpt" name="system_main_ctpt" value="<?php echo isset($main1->ctpt)?$main1->ctpt:'' ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Starting delemiter</label>
                                                                        <input type="text" class="form-control" id="system_main_startdel" name="system_main_startdel" value="<?php echo isset($main1->startdel)?$main1->startdel:'' ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <span><label> <input type="checkbox" class="i-checks" name="system_main_dta1lesy" id="system_main_dta1lesy"  <?php if(isset($main1->dta1lesy) && $main1->dta1lesy=='on'){ echo 'checked'; } ?>> Disable the Law email system </label></span>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Subject macro</label>
                                                                        <input type="text" class="form-control" name="system_main_submac" id="system_main_submac" value="<?php echo isset($main1->submac)?$main1->submac:''; ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Ending delemiter</label>
                                                                        <input type="text" class="form-control" name="system_main_enddel" id="system_main_enddel" value="<?php echo isset($main1->enddel)?$main1->enddel:''; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label>Start / End bounds</label>
                                                                    <textarea class="form-control" style="width: 100%; height: 100px;" name="system_main_stenboun" id="system_main_stenboun"><?php echo isset($main1->stenboun)?$main1->stenboun:'' ?></textarea>
                                                                </div>
<!--                                                                <div class="col-sm-12">
                                                                    <div class="form-group text-center m-t-md m-b-xs">
                                                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_ccfiletk" id="system_ccfiletk" <?php if(isset($firm['ccfiletk']) && $firm['ccfiletk']=='on' ){ echo 'checked'; } ?>> Client card file tracking </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_ccfiletk2" id="system_ccfiletk2" <?php if(isset($firm['ccfiletk2']) && $firm['ccfiletk2']=='on'){ echo 'checked'; } ?>> Use name in file tracking </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_cdfiletkdd" id="system_cdfiletkdd" <?php if(isset($firm['cdfiletkdd']) && $firm['cdfiletkdd']=='on'){ echo 'checked'; } ?>> Dropdown for file tracking </label></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Extra dropdown for the file tracker</label>
                                                    <input type="text" class="form-control" name="system_popfiletk" id="system_popfiletk" value="<?php echo isset($firm['popfiletk'])?$firm['popfiletk']:''; ?>">
                                                </div>
                                            </div>
<!--                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Fee/cost initials</label>
                                                    <select class="form-control" id="system_main_feecostinit" name="system_main_feecostinit" value="<?php echo isset($main1->feecostinit)?$main1->feecostinit:'' ?>">
                                                        <option value="AttyH" <?php if(isset($main1->feecostinit) && $main1->feecostinit=='AttyH'){ echo "selected";}?>>AttyH</option>
                                                        <option value="User" <?php if(isset($main1->feecostinit) && $main1->feecostinit=='User'){ echo "selected";}?>>User</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Grid style</label>
                                                    <select class="form-control" name="system_main_gridstyle" id="system_main_gridstyle">
                                                        <option value="Normal" <?php if(isset($main1->gridstyle) && $main1->gridstyle=='Normal'){ echo "selected";}?>>Normal</option>
                                                         <option value="Advanced" <?php if(isset($main1->gridstyle) && $main1->gridstyle=='Advanced'){ echo "selected";}?>>Advanced</option>
                                                         <option value="rtfgrid" <?php if(isset($main1->gridstyle) && $main1->gridstyle=='rtfgrid'){ echo "selected";}?>>RTF Grid</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Watermark / Size</label>
                                                    <div class="row">
                                                        <div class="col-xs-6" >
                                                            <input type="text" class="form-control" id="system_main_watermark" name="system_main_watermark" value="<?php echo isset($main1->watermark)?$main1->watermark:'' ?>">
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <input type="number" class="form-control" id="system_main_wsize" name="system_main_wsize" value="<?php echo isset($main1->wsize)?$main1->wsize:'' ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_skipcd" id="system_main_skipcd" <?php if(isset($main1->skipcd) && $main1->skipcd=='on'){ echo 'checked'; } ?>> Skip cost details </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_asacaadd" id="system_main_asacaadd" <?php if(isset($main1->asacaadd) && $main1->asacaadd='on'){ echo 'checked'; } ?>> Allow show all case activity add </label></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Paid button - color</label>
                                                    <input type="number" class="form-control" name="system_main_paidbutton" id="system_main_paidbutton" value="<?php echo isset($main1->paidbutton)?$main1->paidbutton:'';  ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Alternate filename</label>
                                                    <input type="text" class="form-control" id="system_main_altfile" name="system_main_altfile" value="<?php echo isset($main1->altfile)?$main1->altfile:'' ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>View initilas style</label>
                                                    <select class="form-control" id="system_main_vieintst" name="system_main_vieintst" >
                                                        <option value="Last" <?php if(isset($main1->vieintst) && $main1->vieintst=='Last'){ echo "selected";}?>>Last</option>
                                                        <option value="Original" <?php if(isset($main1->vieintst) && $main1->vieintst=='Original'){ echo "selected";}?>>Original</option>
                                                        <option value="Original-Last" <?php if(isset($main1->vieintst) && $main1->vieintst=='Original-Last'){ echo "selected";}?>>Original-Last</option>
                                                        <option value="Last-Original" <?php if(isset($main1->vieintst) && $main1->vieintst=='Last-Original'){ echo "selected";}?>>Last-Original</option>
                                                         <option value="Original-Last?" <?php if(isset($main1->vieintst) && $main1->vieintst=='Original-Last?'){ echo "selected";}?>>Original-Last?</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Display category</label>
                                                    <input type="number" class="form-control"  id="system_main_dispcate" name="system_main_dispcate" value="<?php echo isset($main1->dispcate)?$main1->dispcate:'' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>File accessed category</label>
                                                    <input type="number" class="form-control" id="system_main_fileacccat" name="system_main_fileacccat" value="<?php echo isset($main1->fileacccat)?$main1->fileacccat:'' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_keepodate" id="system_main_keepodate" <?php if(isset($main1->keepodate) && $main1->keepodate='on'){ echo 'checked'; } ?>> keep old date </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_keepoddatefpaid" id="system_main_keepoddatefpaid" <?php if(isset($main1->keepoddatefpaid) && $main1->keepoddatefpaid='on'){ echo 'checked'; } ?>> keep old date for paid button </label></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Mark event</label>
                                                    <input type="text" class="form-control" id="system_main_markevent" name="system_main_markevent" value="<?php echo isset($main1->markevent)?$main1->markevent:'' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Outlook button</label>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <select class="form-control" id="system_main_outlookbutton" name="system_main_outlookbutton" >
                                                                <option value="Subject Only" <?php if(isset($main1->outlookbutton) && $main1->outlookbutton=='Subject Only'){ echo "selected";}?>>Subject Only</option>
                                                                 <option value="Subject+Body" <?php if(isset($main1->outlookbutton) && $main1->outlookbutton=='Subject+Body'){ echo "selected";}?>>Subject+Body</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <select class="form-control" id="system_main_outlookbutton1" name="system_main_outlookbutton1">
                                                                <option value="Use current date" <?php if(isset($main1->outlookbutton1) && $main1->outlookbutton1=='Use current date'){ echo "selected";}?>>Use current date</option>
                                                                <option value="Use date received" <?php if(isset($main1->outlookbutton1) && $main1->outlookbutton1=='Use date received'){ echo "selected";}?>>Use date received</option>
                                                                <option value="Use date sent" <?php if(isset($main1->outlookbutton1) && $main1->outlookbutton1=='Use date sent'){ echo "selected";}?>>Use date sent</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control"  id="system_main_outlookbutton2" name="system_main_outlookbutton2" value="<?php echo isset($main1->outlookbutton2)?$main1->outlookbutton2:'' ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
<!--                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-6" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks " name="system_YOURFILEN1_enuni" id="system_YOURFILEN1_enuni" <?php if(isset($yourfilen1->enuni) && $yourfilen1->enuni=='on'){ echo 'checked'; } ?>> Enforce uniqueness </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks " name="system_YOURFILEN1_enyofnl" id="system_YOURFILEN1_enyofnl" <?php if(isset($yourfilen1->enyofnl) && $yourfilen1->enyofnl=='on'){ echo 'checked'; } ?>> Enable your file number label </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks " name="system_YOURFILEN1_diyfint" id="system_YOURFILEN1_diyfint" <?php if(isset($yourfilen1->diyfint) && $yourfilen1->diyfint=='on'){ echo 'checked'; } ?>> Disable your file number Textbox </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks " name="system_main_didaent" id="system_main_didaent" <?php if(isset($main1->didaent) && $main1->didaent=='on'){ echo 'checked'; } ?>> Disable date entered </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks " name="system_YOURFILEN1_didaop" id="system_YOURFILEN1_didaop" <?php if(isset($yourfilen1->didaop) && $yourfilen1->didaop=='on'){ echo 'checked'; } ?>> Disable date open </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks " name="system_YOURFILEN1_ensecur" id="system_YOURFILEN1_ensecur" <?php if(isset($yourfilen1->ensecur) && $yourfilen1->ensecur=='on'){ echo 'checked'; } ?>> Enforce security </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks " name="system_main_elispa" id="system_main_elispa" <?php if(isset($main1->elispa) && $main1->elispa=='on'){ echo 'checked'; } ?>> Eliminate spaces </label></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Format</label>
                                                    <textarea class="form-control" name="system_YOURFILEN1_format" id="system_YOURFILEN1_format" style="width: 100%;height: 100px;"><?php  echo isset($yourfilen1->format)?$yourfilen1->format:''?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Default status</label>
                                                    <input type="text" class="form-control"  name="system_YOURFILEN1_defausta" id="system_YOURFILEN1_defausta" value="<?php echo isset($yourfilen1->defausta)?$yourfilen1->defausta:'' ?>"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Prompt</label>
                                                    <input type="text" class="form-control"  name="system_main_promp" id="system_main_promp" value="<?php echo isset($main1->promp)?$main1->promp:'' ?>" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Primary Status</label>
                                                    <input type="text" class="form-control" name="system_main_pristatus" id="system_main_pristatus" value="<?php echo isset($main1->pristatus)?$main1->pristatus:'' ?>" />
                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_runprnewc" id="system_main_runprnewc" <?php if(isset($main1->runprnewc) && $main1->runprnewc=='on'){ echo 'checked'; } ?>> Run program NewCase </label></span>
                                                    <button type="submit" class="btn btn-primary btn-md">Compile</button>
                                                </div>
                                                <div class="form-group">

                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_shyoufileno" id="system_main_shyoufileno:'' ?>" <?php if(isset($main1->shyoufileno) && $main1->shyoufileno=='on'){ echo 'checked'; } ?>> Show your file no </label></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Numbers</label>
                                                    <textarea class="form-control" style="width: 100%;height: 100px;" name="system_yourfilen2" id="system_yourfilen2"  ><?php echo isset($firm['yourfilen2'])?$firm['yourfilen2']:'' ?></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group text-center m-t-md m-b-xs">
                        <button type="button" id="savegenral" class="btn btn-md btn-primary savegenral">Save</button>
                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                    </div>
                </div>
                </form>
            </div>
        </div>



    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                 autoclose: true
            });
            $(".msg11").css({"background-color": "<?php echo isset($main1->msg1)?$main1->msg1:''?>"});
            $(".msg22").css({"background-color": "<?php echo isset($main1->msg2)?$main1->msg2:''?>"});
            $(".msg33").css({"background-color": "<?php echo isset($main1->msg3)?$main1->msg3:''?>"});
//            $('.ChackBox').on('ifChanged', function(event){
//                $(this).val(this.checked? '1': '0');
//            });
        });
        $('#savegenral').on('click', function () {
             savetoadmin('GeneralForm');
//             var serialized = $('input:checkbox').map(function() {
//   return { name: this.name, value: this.checked ? this.value : "false" };
// });
 //alert($("#GeneralForm input:checkbox").serialize());
     //alert($("#GeneralForm").serialize()); return false;//<?php echo base_url(); ?>admin/updatefirm
       // if($("#GeneralForm").valid()){
//                    $.ajax({
//                        url: '<?php echo base_url(); ?>admin/updatefirm',
//                        method: 'POST',
//                        data: $("#GeneralForm").serialize(),
//                    beforeSend: function (xhr) {
//                          $.blockUI();
//                    },
//                    complete: function (jqXHR, textStatus) {
//                            $.unblockUI();
//                    },
//                    success: function (result) {
//                          var obj = $.parseJSON(result);
//                           if (obj.status == '1') {
//                                swal("Success !", obj.message, "success");
//                                location.reload();
//                            }
//                    }
//                    });
       // }


    });
    $('#system_main_msg1').on('change blur',function(){

        $(".msg11").css({"background-color": $(this).val()});
    });
    $('#system_main_msg2').on('change blur',function(){

        $(".msg22").css({"background-color": $(this).val()});
    });
    $('#system_main_msg3').on('change blur',function(){

        $(".msg33").css({"background-color": $(this).val()});
    });

    </script>





</body>
</html>
