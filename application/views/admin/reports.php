<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                    <span>Prospects</span>
                </div>
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">MarkUp UDF</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">Intake Report</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Case Count</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4">Mark Up</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label>Compile RptPrgU1</label>
                                                    <button type="button" class="btn btn-primary btn-sm pull-right">Compile</button>
                                                </div>
                                                <div class="form-group">
                                                    <label>Enter The Report name and Report # To use [fieldname, columnwidth, page#, field#]</label>
                                                    <textarea class="form-control" style="width: 100%;height: 200px;"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label>Referral Type</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Doctor Type</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label>Types</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Type 1</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Type 2</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Type 3</label>
                                                    <input type="text" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <p>User Defined Page and Field number for markup report #7</p>
                                                <div class="form-group">
                                                    <label>UDF Page No</label>
                                                    <input type="number" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label>UDF Field No</label>
                                                    <input type="number" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <button type="submit" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
      
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            
            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });
            
            $('.color').colorpicker({
                 autoclose: true
            });
        });
    </script>
    
    
    
    
    
</body>
</html>
