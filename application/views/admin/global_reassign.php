<!--CHANGES MADE BY SANCHIT TO DISPLAY ALL THE STAFF MEMBERS IN ALL THE USER'S LISTING ON THIS PAGE-->
<!--CAN BE REVERTED BY LOOKING AT COMMIT ON 9th November 2018-->

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <span>Global Reassign</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Global Reassign</h5>
                </div>
                <!--form-->
                <div class="ibox-content marg-bot15 padd-bot5">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Reassign Entries for</label>
                                <select class="form-control" name="reassign_eng" id="reassign_eng" onchange="return active_div(this.value);">
                                    <option value="case">Case</option>
                                    <option value="task">Task</option>
                                    <option value="calendar">Calendar</option>
                                    <option value="CaseActivityCategories">Case Activity Categories</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Caution:</label>
                                <p style="color: #CD1818; font-size:18px;">Misuse of this section may be irreparable</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="case_div">
                    <form id="caseForm" name="caseForm" method="post">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Parameters</h5>
                            </div>
                            <div class="ibox-content padd-bot5">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Case Type</label>
                                            <select class="form-control select2Class" name="case_case_type" id="case_case_type">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (!empty($systemDropdown['casetype'])) {
                                                    foreach ($systemDropdown['casetype'] as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['casetype']; ?>"><?php echo $value['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Case Status</label>
                                            <select class="form-control select2Class" name="case_case_status" id="case_case_status">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (!empty($systemDropdown['casestat'])) {
                                                    foreach ($systemDropdown['casestat'] as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['casestat']; ?>"><?php echo $value['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Resp</label>
                                            <select class="form-control select2Class" name="case_atty_resp" id="case_atty_resp">
                                                <option value="">Select Atty Resp</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Hand</label>
                                            <select class="form-control select2Class" name="case_atty_hand" id="case_atty_hand">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Para Hand</label>
                                            <select class="form-control select2Class" name="case_para_hand" id="case_para_hand">
                                                <option value="">Select Para Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                            <!--<input type="text" name="case_para_hand" id="case_para_hand" class="form-control oldStaff" />-->
                                        </div> 
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Sec Hand</label>
                                            <select class="form-control select2Class" name="case_sec_hand" id="case_sec_hand">
                                                <option value="">Select Sec Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Primary Party</label>
                                    <input type="text" name="case_primary" id="case_primary" class="form-control" title="Enter starting and ending for the client last name i.e. A-C"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Changes to be made</h5>
                            </div>
                            <div class="ibox-content padd-bot5">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Resp</label>
                                            <select name="case_new_atty_resp" id="case_new_atty_resp" class="form-control select2Class caseStaff">
                                                <option value="">Select Atty Resp</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Hand</label>
                                            <select name="case_new_atty_hand" id="case_new_atty_hand" class="form-control select2Class caseStaff">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Para Hand</label>
                                            <select name="case_new_para_hand" id="case_new_para_hand" class="form-control select2Class caseStaff">
                                                <option value="">Select Para Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sec Hand</label>
                                            <select name="case_new_sec_hand" id="case_new_sec_hand" class="form-control select2Class caseStaff">
                                                <option value="">Select Sec Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row" id="task_div" style="display:none">
                    <form method="post" id="taskForm" name="taskForm">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Parameters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Case Type</label>
                                            <select class="form-control select2Class" name="task_case_type" id="task_case_type">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (!empty($systemDropdown['casetype'])) {
                                                    foreach ($systemDropdown['casetype'] as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['casetype']; ?>"><?php echo $value['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Case Status</label>
                                            <select class="form-control select2Class" name="task_case_status" id="task_case_status">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (!empty($systemDropdown['casestat'])) {
                                                    foreach ($systemDropdown['casestat'] as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['casestat']; ?>"><?php echo $value['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Resp</label>
                                            <select name="task_atty_resp" id="task_atty_resp" class="form-control select2Class task_atty_resp">
                                                <option value="">Select Atty Resp</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Hand</label>
                                            <select name="task_atty_hand" id="task_atty_hand" class="form-control select2Class task_atty_resp">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Para Hand</label>
                                            <select name="task_para_hand" id="task_para_hand" class="form-control select2Class task_atty_resp">
                                                <option value="">Select Para Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sec Hand</label>
                                            <select name="task_sec_hand" id="task_sec_hand" class="form-control select2Class">
                                                <option value="">Select Sec Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Task to</label>
                                    <select name="task_task_to" id="task_task_to" class="form-control select2Class">
                                        <option value="">Select Task to</option>
                                        <?php
                                        if (!empty($Stafflist)) {
                                            foreach ($Stafflist as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1['initials']; ?>"><?php echo $v1['initials']; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Changes to be made</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="form-group">
                                    <label>Task to</label>
                                    <select name="new_task_task_to" id="new_task_task_to" class="form-control select2Class">
                                        <option value="">Select Task to</option>
                                        <?php
                                        if (!empty($Stafflist)) {
                                            foreach ($Stafflist as $k1 => $v1) :
                                                ?>
                                                <option value="<?php echo $v1['initials']; ?>"><?php echo $v1['initials']; ?></option>
                                                <?php
                                            endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row" id="cal_div"  style="display:none">
                    <form method="post" id="calForm" name="calForm">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Parameters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Starting Date</label>
                                            <input type="text" name="cal_start_dt" id="cal_start_dt" class="form-control dates datepicker"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Ending Date</label>
                                            <input type="text" name="cal_end_dt" id="cal_end_dt" class="form-control dates datepicker"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Event</label>
                                            <select name="cal_event" id="cal_event" class="form-control select2Class">
                                                <option value="">Select Event</option>
                                                <?php
                                                if (!empty($Calenderdropdown['event'])) {
                                                    foreach ($Calenderdropdown['event'] as $k1 => $v1) :
                                                        ?>
                                                        <option value="<?php echo $v1; ?>"><?php echo $v1; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Venue</label>
                                            <select name="cal_venue" id="cal_venue" class="form-control select2Class">
                                                <option value="">Select Venue</option>
                                                <?php
                                                if (!empty($Calenderdropdown['Venue'])) {
                                                    foreach ($Calenderdropdown['Venue'] as $k1 => $v1) :
                                                        ?>
                                                        <option value="<?php echo $v1; ?>"><?php echo $v1; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Attorney</label>
                                            <select name="cal_attr" id="cal_attr" class="form-control select2Class">
                                                <option value="">Select Attorney</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $k1 => $v1) :
                                                        ?>
                                                        <option value="<?php echo $v1['initials']; ?>"><?php echo $v1['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Attorney Assign</label>
                                            <select name="cal_attr_ass" id="cal_attr_ass" class="form-control select2Class">
                                                <option value="">Select Attorney Assign</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $k1 => $v1) :
                                                        ?>
                                                        <option value="<?php echo $v1['initials']; ?>"><?php echo $v1['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Changes to be made</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Attorney</label>
                                            <select name="new_cal_attr" id="new_cal_attr" class="form-control select2Class calNew">
                                                <option value="">Select Attorney</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $k1 => $v1) :
                                                        ?>
                                                        <option value="<?php echo $v1['initials']; ?>"><?php echo $v1['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Attorney Assign</label>
                                            <select name="new_cal_attr_ass" id="new_cal_attr_ass" class="form-control select2Class calNew">
                                                <option value="">Select Attorney Assign</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $k1 => $v1) :
                                                        ?>
                                                        <option value="<?php echo $v1['initials']; ?>"><?php echo $v1['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--div class="form-group">
                                    <span><label> <input type="checkbox" class="i-checks"/> Set attyH to attyH on the case </label></span>
                                </div-->
                            </div>
                        </div>
                    </form>
                </div>
                <div class="" id="CaseActivityCategories_div"  style="display:none">
                    <form method="post" id="CaseActivityCat_Form" name="CaseActivityCat_Form">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5 class="text-center">Reassign Caseactivity Categories</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php $jsonCategory = json_decode(category); ?>
                                            <label>Old Category</label>
                                            <select class="form-control select2Class" name="caseact_old_category" id="caseact_old_category">
                                                <option value="">Select Category</option>
                                                <?php
                                                if (!empty($jsonCategory)) {
                                                    foreach ($jsonCategory as $catk => $catv) :
                                                        ?>
                                                        <option value="<?php echo $catk; ?>"><?php echo $catv; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php //$jsonCategory = json_decode(category); ?>
                                            <label>New Category</label>
                                            <select class="form-control select2Class" name="caseact_new_category" id="caseact_new_category">
                                                <option value="">Select Category</option>
                                                <?php
                                                if (!empty($jsonCategory)) {
                                                    foreach ($jsonCategory as $catk => $catv) :
                                                        ?>
                                                        <option value="<?php echo $catk; ?>"><?php echo $catv; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="ibox-title">
                                <h5>Parameters</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Case Type</label>
                                            <select class="form-control select2Class" name="caseact_casetype" id="caseact_casetype">
                                                <option value="">Select Case Type</option>
                                                <?php
                                                if (!empty($systemDropdown['casetype'])) {
                                                    foreach ($systemDropdown['casetype'] as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['casetype']; ?>"><?php echo $value['casetype']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Case Status</label>
                                            <select class="form-control select2Class" name="caseact_casestatus" id="caseact_casestatus">
                                                <option value="">Select Case Status</option>
                                                <?php
                                                if (!empty($systemDropdown['casestat'])) {
                                                    foreach ($systemDropdown['casestat'] as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['casestat']; ?>"><?php echo $value['casestat']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Resp</label>
                                            <select name="caseact_atty_resp" id="caseact_atty_resp" class="form-control select2Class">
                                                <option value="">Select Atty Resp</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Atty Hand</label>
                                            <select name="caseact_atty_hand" id="caseact_atty_hand" class="form-control select2Class">
                                                <option value="">Select Atty Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Para Hand</label>
                                            <select name="caseact_para_hand" id="caseact_para_hand" class="form-control select2Class oldStaff">
                                                <option value="">Select Para Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sec Hand</label>
                                            <select name="caseact_sec_hand" id="caseact_sec_hand" class="form-control select2Class oldStaff">
                                                <option value="">Select Sec Hand</option>
                                                <?php
                                                if (!empty($Stafflist)) {
                                                    foreach ($Stafflist as $key => $value) :
                                                        ?>
                                                        <option value="<?php echo $value['initials']; ?>"><?php echo $value['initials']; ?></option>
                                                        <?php
                                                    endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group text-center m-t-md m-b-xs">
                            <button type="button" class="btn btn-primary btn-md SaveglobalReassign" onclick="return global_assign();">Proceed</button>
                            <button type="button" class="btn btn-danger btn-md" id="ClearGlobalReassign">Clear</button>
                        </div>
                    </div>
                </div>
                <!--/form-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".select2Class").select2({});
    });
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            format: 'mm/dd/yyyy',
            minView: 2,
            autoclose: true,
            useCurrent: true
                    //startDate: '+0d',
        });
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $(".datepicker").val($.datepicker.formatDate("mm/dd/yy", new Date()));

        $('.date').datepicker({
            forceParse: false,
            autoclose: true
        });
        $('.color').colorpicker({
            autoclose: true
        });

        jQuery.validator.addMethod("validateparty", function (value, element) {
            if (value != '') {
                var arrayOfStrings = value.split('-');
                if (arrayOfStrings.length = 2) {
                    setTimeout(function () {
                        $(element).closest('.control-group-inner').find('label.case_primary-error').remove();
                    }, 500);
                }
            } else {
                $(element).closest('.control-group-inner').find('label.case_primary-error').remove();
            }
            return (value);
        }, "This field must contain a dash or must be left blank. Enter a range for the last name of the primary party: Leave it blank for all. Example A-C H-J A-Z");

        $('#ClearGlobalReassign').click(function () {
            $('#caseForm')[0].reset();
            $('#taskForm')[0].reset();
            $('#calForm')[0].reset();
            $('#CaseActivityCat_Form')[0].reset();
            $('.select2Class').trigger('change');
            $(".error").remove();
        });
    });

    function active_div(val) {
        if (val == 'case') {
            $('#case_div').attr("style", "display:block");
            $('#task_div').attr("style", "display:none");
            $('#cal_div').attr("style", "display:none");
            $('#SFSMAILMODULE_div').attr("style", "display:none");
            $('#Color_div').attr("style", "display:none");
            $('#CaseActivityCategories_div').attr("style", "display:none");
            $('#UserDefinedFields_div').attr("style", "display:none");
            $('#Venues_div').attr("style", "display:none");

            $('#caseForm')[0].reset();
            $('.select2Class').trigger('change');
        } else if (val == 'task') {
            $('#case_div').attr("style", "display:none");
            $('#task_div').attr("style", "display:block");
            $('#cal_div').attr("style", "display:none");
            $('#SFSMAILMODULE_div').attr("style", "display:none");
            $('#Color_div').attr("style", "display:none");
            $('#CaseActivityCategories_div').attr("style", "display:none");
            $('#UserDefinedFields_div').attr("style", "display:none");
            $('#Venues_div').attr("style", "display:none");

            $('#taskForm')[0].reset();
            $('.select2Class').trigger('change');
        } else if (val == 'calendar') {
            $('#case_div').attr("style", "display:none");
            $('#task_div').attr("style", "display:none");
            $('#cal_div').attr("style", "display:block");
            $('#SFSMAILMODULE_div').attr("style", "display:none");
            $('#Color_div').attr("style", "display:none");
            $('#CaseActivityCategories_div').attr("style", "display:none");
            $('#UserDefinedFields_div').attr("style", "display:none");
            $('#Venues_div').attr("style", "display:none");

            $('#calForm')[0].reset();
            $('.select2Class').trigger('change');
        } else if (val == 'CaseActivityCategories') {
            $('#CaseActivityCategories_div').attr("style", "display:block");
            $('#case_div').attr("style", "display:none");
            $('#task_div').attr("style", "display:none");
            $('#cal_div').attr("style", "display:none");
            $('#SFSMAILMODULE_div').attr("style", "display:none");
            $('#Color_div').attr("style", "display:none");
            $('#UserDefinedFields_div').attr("style", "display:none");
            $('#Venues_div').attr("style", "display:none");

            $('#CaseActivityCat_Form')[0].reset();
            $('.select2Class').trigger('change');
        }
    }

    function global_assign() {
        var reass = $('#reassign_eng').val();

        if (reass == 'case') {
            var partyval = $('#case_primary').val();
            var vldprty = true;
           /* if (partyval.length != 0) {
                var arrayOfStrings = partyval.split('-');
                if (arrayOfStrings.length == 2) {
                    vldprty = true;
                } else {
                    vldprty = false;
                }
            }*/
            if (partyval.length != 0) {
                var arrayOfStrings = partyval.split('-');
                
                if (partyval.length == 3) {
                    vldprty = true;
                } else {
                    vldprty = false;
                }
            }
            if (vldprty == false) {
                swal("Warning !", "Party must contain a dash or must be left blank. \nEnter a range for the last name of the primary party.\nLeave it blank for all. Example A-C H-J A-Z.", "warning", function () {});
            } else {
                if ($("#case_new_atty_resp").val().length <= "0" && $("#case_new_atty_hand").val().length <= "0" && $("#case_new_para_hand").val().length <= "0" && $("#case_new_sec_hand").val().length <= "0") {
                    swal("Warning !", "You haven't specified anything to change", "warning", function () {});
                } else {
                    var allcasedata = $("form#caseForm").serialize();
                    $.ajax({
                        url: HTTP_PATH + 'admin/globalReassign/caseReassign',
                        method: 'POST',
                        data: allcasedata,
                        beforeSend: function (xhr) {
                            $.blockUI();
                        },
                        complete: function (jqXHR, textStatus) {
                            $.unblockUI();
                        },
                        success: function (data) {
                            swal({
                                title: "Warning!",
                                text: 'Total Cases that will be updated: ' + data + '\nAre you sure you want to continue?',
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes",
                                closeOnConfirm: false
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    $.ajax({
                                        url: HTTP_PATH + 'admin/globalReassign/updatecases',
                                        type: "POST",
                                        data: allcasedata,
                                        beforeSend: function (xhr) {
                                            $.blockUI();
                                        },
                                        complete: function (jqXHR, textStatus) {
                                            $.unblockUI();
                                        },
                                        success: function (data) {
                                            swal({
                                                title: "Success!",
                                                text: 'Total cases changed :' + data,
                                                type: "success",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: false
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        } else if (reass == 'task') {
            var validTaskfrm = validateTaskForm();
            if (validTaskfrm == true) {
                var alltaskdata = $("form#taskForm").serialize();
                $.ajax({
                    url: HTTP_PATH + 'admin/globalReassign/taskReassign',
                    method: 'POST',
                    data: alltaskdata,
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        swal({
                            title: "Warning!",
                            text: 'Total tasks that will be updated: ' + data + '\nAre you sure you want to continue?',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    url: HTTP_PATH + 'admin/globalReassign/updatetasks',
                                    type: "POST",
                                    data: alltaskdata,
                                    beforeSend: function (xhr) {
                                        $.blockUI();
                                    },
                                    complete: function (jqXHR, textStatus) {
                                        $.unblockUI();
                                    },
                                    success: function (data) {
                                        swal({
                                            title: "Success!",
                                            text: 'Total task changed :' + data,
                                            type: "success",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: false
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        } else if (reass == 'calendar') {
            var attr = $('#new_cal_attr').val();
            var attrass = $('#new_cal_attr_ass').val();
            if (attr.length != 0 && attrass.length != 0) {
                swal("Warning !", "You can change only attorney/staff initials or the attorney assigned initials one at one time. You must perform each change separately.", "warning", function () {});
            } else if (attr.length === 0 && attrass.length === 0) {
                swal("Warning !", "You haven't specified any thing to change", "warning", function () {});
            } else {
                var allcaldata = $("form#calForm").serialize();
                $.ajax({
                    url: HTTP_PATH + 'admin/globalReassign/eventReassign',
                    method: 'POST',
                    data: allcaldata,
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        swal({
                            title: "Warning!",
                            text: 'Total Calendar Event that will be updated: ' + data + '\nAre you sure you want to continue?',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    url: HTTP_PATH + 'admin/globalReassign/updateevents',
                                    type: "POST",
                                    data: allcaldata,
                                    beforeSend: function (xhr) {
                                        $.blockUI();
                                    },
                                    complete: function (jqXHR, textStatus) {
                                        $.unblockUI();
                                    },
                                    success: function (data) {
                                        swal({
                                            title: "Success!",
                                            text: 'Total Calendar Event Changed :' + data,
                                            type: "success",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: false
                                        });
                                    }
                                })
                            }
                        });
                    }
                });
            }
        } else if (reass == 'CaseActivityCategories') {
            var validCaseactCategfrm = validCaseactCategForm();
            if (validCaseactCategfrm == true) {
                var allcaseactdata = $("form#CaseActivityCat_Form").serialize();
                $.ajax({
                    url: HTTP_PATH + 'admin/globalReassign/caseactReassign',
                    method: 'POST',
                    data: allcaseactdata,
                    beforeSend: function (xhr) {
                        $.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    },
                    success: function (data) {
                        swal({
                            title: "Warning!",
                            text: 'Total Cases that will be updated: ' + data + '\nAre you sure you want to continue?',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    url: HTTP_PATH + 'admin/globalReassign/updatecaseactassign',
                                    type: "POST",
                                    data: allcaseactdata,
                                    beforeSend: function (xhr) {
                                        $.blockUI();
                                    },
                                    complete: function (jqXHR, textStatus) {
                                        $.unblockUI();
                                    },
                                    success: function (data) {
                                        swal({
                                            title: "Success!",
                                            text: 'Total Cases Changed :' + data,
                                            type: "success",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: false
                                        });
                                    }
                                });
                                swal.close();
                            }
                        });
                    }
                });
            }
        }
    }

    function validateCaseForm() {
        var form = $("#caseForm");
        form.validate({
            debug: true,
            rules: {
                /*case_new_atty_resp: {
                 require_from_group: [1, ".caseStaff"]
                 },
                 case_new_atty_hand: {
                 require_from_group: [1, ".caseStaff"]
                 },
                 case_new_para_hand: {
                 require_from_group: [1, ".caseStaff"]
                 },*/
                case_new_sec_hand: {
                    require_from_group: [1, ".caseStaff"]
                },
                /*case_atty_resp: {
                 require_from_group: [1, ".oldStaff"]
                 },
                 case_atty_hand: {
                 require_from_group: [1, ".oldStaff"]
                 },
                 case_para_hand: {
                 require_from_group: [1, ".oldStaff"]
                 },*/
                /*case_sec_hand: {
                 require_from_group: [1, ".oldStaff"]
                 },*/
                case_primary: {
                    required: false
                            //validateparty: true
                }
            },
            messages: {
                case_new_atty_resp: "Atleast one Changes to be made Required",
                //case_new_atty_hand: "Atleast one Changes to be made Required",
                //case_new_para_hand: "Atleast one Changes to be made Required",
                //case_new_sec_hand: "Atleast one Changes to be made Required",
                // case_atty_resp: "Atleast one parameter for change Required",
                // case_atty_hand: "Atleast one parameter for change Required",
                //case_para_hand: "Atleast one parameter for change Required",
                case_sec_hand: "Atleast one parameter for change Required"
            }
        });
        return form.valid();
    }

    function validateTaskForm() {
        var form = $("#taskForm");
        form.validate({
            debug: true,
            rules: {
                task_task_to: {
                    required: true
                },
                new_task_task_to: {
                    required: true
                }
            }
        });

        return form.valid();
    }

    function validCaseactCategForm() {
        var form = $("#CaseActivityCat_Form");

        form.validate({
            debug: true,
            rules: {
                caseact_new_category: {
                    required: true
                }
            }
        });
        return form.valid();
    }
</script>