<?php
$injuryd = json_decode($firm['injury9']);
$main1 = json_decode($firm['main1']);
?>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a>Configure System Defaults</a> /
                <span>Injuries</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Injury Defaults</h5>
                </div>
                <div class="ibox-content">
                    <form method="post" name="injuriesForm" id="injuriesForm" enctype="multipart/form-data" action="#">

                        <div class="row">
                            <div class="col-sm-6">
                                <p>9. This application is field because of a disagreement regarding liability for:</p>
                                <div class="row row-top-15">
                                    <div class="col-sm-6">
                                        <div class="form-inline">
                                            <label>TD Indemnity</label>
                                            <input type="text" class="form-control textwidth" id="system_injuryd_tdind" name="system_injuryd_tdind" value="<?php echo isset($injuryd->tdind) ? $injuryd->tdind : '' ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-inline">
                                            <label>PD Indemnity</label>
                                            <input type="text" class="form-control textwidth" id="system_injuryd_pdind" name="system_injuryd_pdind" value="<?php echo isset($injuryd->pdind) ? $injuryd->pdind : '' ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-top-15">
                                    <div class="col-sm-12">
                                        <div class="form-inline">
                                            <label>Reimbursement for medical expense</label>
                                            <input type="text" class="form-control textwidth"  id="system_injuryd_Reformeexpense" name="system_injuryd_Reformeexpense" value="<?php echo isset($injuryd->Reformeexpense) ? $injuryd->Reformeexpense : '' ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-top-15">
                                    <div class="col-sm-12">
                                        <div class="form-inline">
                                            <label>Compensation at proper rate</label>
                                            <input type="text" class="form-control textwidth" id="system_injuryd_comatprorate" name="system_injuryd_comatprorate" value="<?php echo isset($injuryd->comatprorate) ? $injuryd->comatprorate : '' ?>"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-top-15">
                                    <div class="col-sm-6">
                                        <div class="form-inline">
                                            <label>Medical Treatment</label>
                                            <input type="text" class="form-control textwidth"  id="system_injuryd_meditreatment" name="system_injuryd_meditreatment" value="<?php echo isset($injuryd->meditreatment) ? $injuryd->meditreatment : '' ?>"  />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-inline">
                                            <label>Rehabilitation</label>
                                            <input type="text" class="form-control textwidth"  id="system_injuryd_rehabilitation" name="system_injuryd_rehabilitation" value="<?php echo isset($injuryd->rehabilitation) ? $injuryd->rehabilitation : '' ?>"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Other (specify) </label>
                                    <input type="text" class="form-control" id="system_injuryd_otherspecify" name="system_injuryd_otherspecify" value="<?php echo isset($injuryd->otherspecify) ? $injuryd->otherspecify : '' ?>"  />
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="button" id='saveinjuries' class="btn btn-primary btn-md">Save</button>
                                        <button type="submit" class="btn btn-danger btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                autoclose: true
            });
        });
        $('#saveinjuries').on('click', function () {
            savetoadmin('injuriesForm');

        });
    </script>

