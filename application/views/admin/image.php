<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a>Configure System Defaults</a> /
                <span>Image Details</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Image Details</h5>
                </div>
                <form name="imagesttngForm" id="imagesttngForm" method="post">
                <div class="ibox-content marg-bot15">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Image Name</label>
                                <textarea class="form-control" name="system_pictnames" id="system_pictnames"  style="width: 100%;height: 150px;"><?php echo isset($firm['pictnames'])?$firm['pictnames']:''; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Form Width</label>
                                <input type="text" name="system_pictwidth" id="system_pictwidth" value="<?php echo isset($firm['pictwidth'])?$firm['pictwidth']:''; ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Form Height</label>
                                <input type="text" name="system_pictheight" id="system_pictheight" class="form-control" value="<?php echo isset($firm['pictheight'])?$firm['pictheight']:''; ?>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Image Stratch</label>
                                <input type="text" name="system_pictstretc" id="system_pictstretc" class="form-control" value="<?php echo isset($firm['pictstretc'])?$firm['pictstretc']:''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Resize Form</label>
                                <input type="text" name="system_pictresize" id="system_pictresize" class="form-control" value="<?php echo isset($firm['pictresize'])?$firm['pictresize']:''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Maximize Form</label>
                                <input type="text" name="system_pictmax" id="system_pictmax" class="form-control" value="<?php echo isset($firm['pictmax'])?$firm['pictmax']:''; ?>">
                            </div>
                            <div class="form-group">
                                <label>Zoom Width / Height</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="number" name="system_pictzoomx" id="system_pictzoomx" class="form-control" value="<?php echo isset($firm['pictzoomx'])?$firm['pictzoomx']:''; ?>">
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="number" name="system_pictzoomy" id="system_pictzoomy" class="form-control" value="<?php echo isset($firm['pictzoomy'])?$firm['pictzoomy']:''; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group text-center m-t-md m-b-xs">
                                <button type="button" id="imagesttngSave" class="btn btn-primary btn-md">Save</button>
                                <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>


        </div>
    </div>
</div>


    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                 autoclose: true
            });
        });

         $('#imagesttngSave').on('click', function () {
                savetoadmin('imagesttngForm');
         });
    </script>





</body>
</html>
