<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
    vertical-align: middle;
    }
</style>

        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure Group Defaults</a> /
                    <span>General Information</span>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>General Information</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" name="editGroup" id="editGroup">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Group Name</label>
                                        <input type="hidden" name="groupid" id="groupid" value="<?php echo $this->uri->segment(3); ?>">
                                        <input type="text" name="staff3_groupname" id="staff3_groupname"value="<?php echo $group[0]->groupname ?>" class="form-control" />
                                    </div>
                                </div>

                                 <div class="col-sm-6">
                                    <div class="form-group">
                                    <h3>New Case Defaults</h3>
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <input type="text" name="staff3_casetype" id="staff3_casetype" class="form-control" value="<?php echo $group[0]->casetype ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <input type="text" name="staff3_casestat" id="staff3_casestat" class="form-control" value="<?php echo $group[0]->casestat ?>" />
                                    </div>
                                </div>
                                <!--div class="col-sm-6">
                                    <h3>Genral Information</h3>
                                    <div class="form-group">
                                        <div><label><input type="checkbox" class="i-checks" /> Show Status Bar </label></div>
                                        <div><label><input type="checkbox" class="i-checks" /> Warn Closing Windows </label></div>
                                        <div><label><input type="checkbox" class="i-checks" /> Show Tool Tips </label></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Left Margin</label>
                                        <input type="number" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Main Toolbar</label>
                                        <select class="form-control">
                                            <option>Standard</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Interrupt frequency</label>
                                        <input type="number" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <h3>New Case Defaults</h3>
                                    <div class="form-group">
                                        <label>Type of Case</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <h3>Default Batch Filenames</h3>
                                    <div class="form-group">
                                        <label>WP</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Other</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                    </div>
                                </div>-->
                                 </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="button" class="btn btn-primary btn-md" onclick="submitForm();">Save</button>
                                        <button type="button" onclick="window.location='<?php echo base_url(); ?>admin/group_profile'" class="btn btn-primary btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                 autoclose: true
            });
        });

        function submitForm(){
            var validator = $("#editGroup").validate({
             rules: {
              staff3_groupname: "required",

                },
                //errorElement: "span" ,
                messages: {
                 staff3_groupname: " Enter Group Name",
                 email: " Enter Email",
                }
            });
            if(validator.form()){ // validation perform
                var staff3_groupname = $("#staff3_groupname").val();
                var staff3_casetype = $("#staff3_casetype").val();
                var staff3_casestat = $("#staff3_casestat").val();
                var groupid = $("#groupid").val();
                $.ajax({
                       url: '<?php echo base_url(); ?>admin/editGroup',
                       method: 'POST',
                       dataType: 'json',
                       data: {
                           staff3_groupname: staff3_groupname,
                           staff3_casetype: staff3_casetype,
                           staff3_casestat: staff3_casestat,
                           groupid:groupid
                       },
                        beforeSend: function (xhr) {
                               //$.blockUI();
                           },
                           complete: function (jqXHR, textStatus) {
                               //$.unblockUI();
                           },
                       success: function (result) {
                           console.log(result);
                           var obj = result;
                           console.log(obj);
                           if (obj.status == '0') {
                                swal({title: "Warning!", text: obj.message , type: "warning"},
                                       function(){
                                           location.reload();
                                       }
                                    );

                           }
                           else
                           {
                               swal({title: "Success!", text: obj.message , type: "success"},
                                       function(){
                                           location.reload();
                                       }
                                    );
                           }

                       }
                   });
               }
           }
    </script>
