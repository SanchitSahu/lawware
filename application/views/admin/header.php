<!DOCTYPE html>
<html lang="en" class="no-scroll">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><?php echo APPLICATION_NAME; ?> | Dashboard</title>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Plugins -->
        <link href="<?php echo base_url('assets'); ?>/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/custom.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/chosen/chosen.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/switchery/switchery.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/cropper/cropper.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/switchery/switchery.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/select2/select2.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/animate.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/inspinia.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">

    </head>
<?php
//Temporary code for active LI
$self_url = $_SERVER['PHP_SELF'];
$self_url = explode("/",$self_url);
//echo $page;
?>
<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                                    <span>
                                        <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                                    </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $this->session->userdata('admin_user_data')['sa_user_name'] ?></strong>
                             </span> <span class="text-muted text-xs block"><?php echo $this->session->userdata('admin_user_data')['sa_user_name'] ?> <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <!--li><a href="profile.html">Profile</a></li-->
                                <li><a href="passchange">Change Password</a></li>
                                <!--li><a href="contacts.html">Contacts</a></li>
                                <li><a href="email.php">Mailbox</a></li-->
                                <li class="divider"></li>
                                <li><a href="logout">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            Lw
                        </div>
                    </li>
                    <?php
                        $page = $self_url[2];
                    ?>
                    <li <?php if($page == "firm.php" || $page == "general.php" || $page == "drives.php" || $page == "newcases.php" || $page == "security.php" || $page == "function.php" || $page == "image.php" || $page == "popup.php" || $page == "caseactivity.php" || $page == "colors.php" || $page == "clientcard.php" || $page == "calendar.php" || $page == "screen.php" || $page == "program.php" || $page == "forms.php" || $page == "injuries.php" || $page == "holidays.php" || $page == "tasksmacro.php" || $page == "rolodexudf.php" || $page == "clientcardudf.php" || $page == "injuryudf.php" || $page == "injuryudfview.php" || $page == "prospects.php" || $page == "reports.php" || $page == "rolodexsecurity.php" || $page == "a1com.php" || $page == "addons.php" || $page == "scan.php" ){echo 'class="active"';}?> >
                        <a href="system.php"><i class="fa fa-cog"></i> <span class="nav-label">System Defaults</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                                    <li <?php if($page == "firm" ){echo 'class="active"';}?>><a href="firm">Firm Address</a></li>
                                    <li <?php if($page == "general.php" ){echo 'class="active"';}?>><a href="general.php">General</a></li>
                                    <li <?php if($page == "drives.php" ){echo 'class="active"';}?>><a href="drives.php">Drives</a></li>
                                    <li <?php if($page == "newcases.php" ){echo 'class="active"';}?>><a href="newcases.php">New Cases</a></li>
                                    <li <?php if($page == "security.php" ){echo 'class="active"';}?>><a href="security.php">Security</a></li>
                                    <li <?php if($page == "function.php" ){echo 'class="active"';}?>><a href="function.php">Function</a></li>
                                    <li <?php if($page == "image.php" ){echo 'class="active"';}?>><a href="image.php">Images</a></li>
                                    <li <?php if($page == "popup.php" ){echo 'class="active"';}?>><a href="popup.php">Popups</a></li>
                                    <li <?php if($page == "caseactivity.php" ){echo 'class="active"';}?>><a href="caseactivity.php">Case Activity</a></li>
                                    <li <?php if($page == "colors.php" ){echo 'class="active"';}?>><a href="colors.php">System Colors</a></li>
                                    <li <?php if($page == "calendar.php" ){echo 'class="active"';}?>><a href="calendar.php">Calendar</a></li>
                                    <li <?php if($page == "screen.php" ){echo 'class="active"';}?>><a href="screen.php">User Defined Screens</a></li>
                                    <li <?php if($page == "clientcard.php" ){echo 'class="active"';}?>><a href="clientcard.php">Client Card Notes</a></li>
                                    <li <?php if($page == "program.php" ){echo 'class="active"';}?>><a href="program.php">Program Menu</a></li>
                                    <li <?php if($page == "forms.php" ){echo 'class="active"';}?>><a href="forms.php">Forms</a></li>
                                    <li <?php if($page == "injuries.php" ){echo 'class="active"';}?>><a href="injuries.php">Injuries</a></li>
                                    <li <?php if($page == "holidays.php" ){echo 'class="active"';}?>><a href="holidays.php">Holidays</a></li>
                                    <li <?php if($page == "tasksmacro.php" ){echo 'class="active"';}?>><a href="tasksmacro.php">Tasks / Task Macros </a></li>
                                    <li <?php if($page == "rolodexudf.php" ){echo 'class="active"';}?>><a href="rolodexudf.php">Rolodex UDF</a></li>
                                    <li <?php if($page == "clientcardudf.php" ){echo 'class="active"';}?>><a href="clientcardudf.php">Client Card UDF</a></li>
                                    <li <?php if($page == "injuryudf.php" ){echo 'class="active"';}?>><a href="injuryudf.php">Injury UDF</a></li>
                                    <li <?php if($page == "injuryudfview.php" ){echo 'class="active"';}?>><a href="injuryudfview.php">Injury UDF View</a></li>
                                    <li <?php if($page == "prospects.php" ){echo 'class="active"';}?>><a href="prospects.php">Prospects</a></li>
                                    <li <?php if($page == "reports.php" ){echo 'class="active"';}?>><a href="reports.php">Reports</a></li>
                                    <li <?php if($page == "rolodexsecurity.php" ){echo 'class="active"';}?>><a href="rolodexsecurity.php">Rolodex Security</a></li>
                                    <li <?php if($page == "a1com.php" ){echo 'class="active"';}?>><a href="a1com.php">Comm Settings</a></li>
                                    <li <?php if($page == "addons.php" ){echo 'class="active"';}?>><a href="addons.php">Add-Ons</a></li>
                                    <li <?php if($page == "scan.php" ){echo 'class="active"';}?>><a href="scan.php">Scan Settings</a></li>
                        </ul>
                    </li>
                    <li <?php if($page == "staff.php"){echo 'class="active"';}?> >
                        <a href="staff.php"><i class="fa fa-users"></i> <span class="nav-label">Staff Defaults</span></a>
                    </li>
                    <li <?php if($page == "group.php" || $page == "group_profile.php" || $page == "group_general.php" || $page == "group_function.php" || $page == "group_security.php" || $page == "group_calendar.php" || $page == "group_case.php" || $page == "group_form.php" || $page == "group_rolodex.php" || $page == "group_task.php" || $page == "group_messages.php" || $page == "group_client.php" ){echo 'class="active"';}?> >
                        <a href="group.php"><i class="fa fa-group"></i> <span class="nav-label">Group Defaults</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                                    <li <?php if($page == "group_profile.php" ){echo 'class="active"';}?>><a href="group_profile.php">Member Profile</a></li>
                                    <li <?php if($page == "group_general.php" ){echo 'class="active"';}?>><a href="group_general.php">General Information</a></li>
                                    <li <?php if($page == "group_function.php" ){echo 'class="active"';}?>><a href="group_function.php">Fuction Keys</a></li>
                                    <li <?php if($page == "group_security.php" ){echo 'class="active"';}?>><a href="group_security.php">Security</a></li>
                                    <li <?php if($page == "group_calendar.php" ){echo 'class="active"';}?>><a href="group_calendar.php">Calendar Security</a></li>
                                    <li <?php if($page == "group_case.php" ){echo 'class="active"';}?>><a href="group_case.php">Case Activity Security</a></li>
                                    <li <?php if($page == "group_form.php" ){echo 'class="active"';}?>><a href="group_form.php">Form Letter Security</a></li>
                                    <li <?php if($page == "group_rolodex.php" ){echo 'class="active"';}?>><a href="group_rolodex.php">Rolodex Security</a></li>
                                    <li <?php if($page == "group_task.php" ){echo 'class="active"';}?>><a href="group_task.php">Task Security</a></li>
                                    <li <?php if($page == "group_messages.php" ){echo 'class="active"';}?>><a href="group_messages.php">Messages</a></li>
                                    <li <?php if($page == "group_client.php" ){echo 'class="active"';}?>><a href="group_client.php">Client Card</a></li>
                        </ul>
                    </li>
                    <li <?php if($page == "udf-table.php"){echo 'class="active"';}?> >
                        <a href="udf-table.php"><i class="fa fa-table"></i> <span class="nav-label">UDF Table Transfer Utility</span>  </a>
                    </li>
                    <li <?php if($page == "udf-report.php"){echo 'class="active"';}?> >
                        <a href="udf-report.php"><i class="fa fa-edit"></i> <span class="nav-label">User Defined Reports</span></a>
                    </li>
                    <li <?php if($page == "browse.php"){echo 'class="active"';}?> >
                        <a href="browse.php"><i class="fa fa-windows"></i> <span class="nav-label">Browse</span></a>
                    </li>
                    <li <?php if($page == "usersoff.php"){echo 'class="active"';}?> >
                        <a href="usersoff.php"><i class="fa fa-ban"></i> <span class="nav-label">Force Users Off</span></a>
                    </li>
                    <li <?php if($page == "global-reassign.php"){echo 'class="active"';}?> >
                        <a href="global-reassign.php"><i class="fa fa-globe"></i> <span class="nav-label">Global Reassign</span></a>
                    </li>
                    <li <?php if($page == "#.php"){echo 'class="active"';}?> >
                        <a href="#"><i class="fa fa-registered"></i> <span class="nav-label">Register</span></a>
                    </li>
                    <li <?php if($page == "fix-case.php"){echo 'class="active"';}?> >
                        <a href="fix-case.php"><i class="fa fa-lock"></i> <span class="nav-label">Fix Locked Cases</span></a>
                    </li>
                    <li <?php if($page == "timeclock.php"){echo 'class="active"';}?> >
                        <a href="timeclock.php"><i class="fa fa-clock-o"></i> <span class="nav-label">TimeClock</span></a>
                    </li>
                    <li <?php if($page == "backup-log.php"){echo 'class="active"';}?> >
                        <a href="backup-log.php"><i class="fa fa-window-maximize"></i> <span class="nav-label">View Backup Log</span></a>
                    </li>
                    <li <?php if($page == "rolodexwizard.php"){echo 'class="active"';}?> >
                        <a href="rolodexwizard.php"><i class="fa fa-table"></i> <span class="nav-label">Rolodex Wizard</span></a>
                    </li>
                    <li <?php if($page == "reattach.php"){echo 'class="active"';}?> >
                        <a href="reattach.php"><i class="fa fa-paperclip"></i> <span class="nav-label">Reattach Orphan Files</span></a>
                    </li>
                    <li <?php if($page == "prospects.php"){echo 'class="active"';}?> >
                        <a href="prospects.php"><i class="fa fa-file-text"></i> <span class="nav-label">BCS Billing Management</span></a>
                    </li>
                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">

        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to <?php echo APPLICATION_NAME; ?> Admin Panel</span>
                </li>
                <!--<li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div class="media-body">
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a4.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/profile.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="email.php">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>-->


                <li>
                    <a href="logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
                <!--<li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>-->
            </ul>

        </nav>
        </div>