<div class="wrapper wrapper-content">
<div class="row">
    <div class="col-lg-12">
        <div class="breadcrumbs">
            <a>Configure System Defaults</a> /
            <span>Tasks</span>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Tasks</h5>
            </div>
            <form>
                <div class="ibox-content marg-bot15">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="marg-right10"><label> <input type="checkbox" class="i-checks" /> Warn when entering task before today </label></div>
                                <div class="marg-right10"><label> <input type="checkbox" class="i-checks" /> Force task to not null when adding </label></div>
                                <div class="marg-right10"><label> <input type="checkbox" class="i-checks" /> Expand all advanced tasks </label></div>
                            </div>
                            <div class="form-group">
                                <label>Days to keep completed tasks</label>
                                <input type="number" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Action when closing client cards</label>
                                <select class="form-control">
                                    <option>None</option>
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm">Groups</button>
                                <button type="button" class="btn btn-primary btn-sm">Use Advanced Features</button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Task to Activity</label>
                                <input type="number" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Phone calls to activity</label>
                                <input type="number" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Action when editing tasks</label>
                                <select class="form-control">
                                    <option>None</option>
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Action when adding tasks</label>
                                <select class="form-control">
                                    <option>Name</option>
                                    <option></option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="ibox-title">
                    <h5>Task Macros</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="marg-right10"><label> <input type="checkbox" class="i-checks" /> Alert when P & S status changes </label></div>
                                <div class="marg-right10"><label> <input type="checkbox" class="i-checks" /> Automate task by P & S date </label></div>
                                <div class="marg-right10"><label> <input type="checkbox" class="i-checks" /> Do not display </label></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Number of days</label>
                                <input type="number" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Whot to</label>
                                <select class="form-control">
                                    <option>Secretary Handling</option>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label>Task Event</label>
                            <textarea class="form-control" style="width: 100%;height: 150px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group text-center m-t-md m-b-xs">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

