<?php
$datealert = json_decode($firm['datealert']);
$birthdaydate = json_decode($firm['birthday']);
$main1 = json_decode($firm['main1']);
?> 

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a>Configure System Defaults</a> /
                <span>Holidays</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Date Configuration</h5>
                </div>
                <div class="ibox-content">
                    <form method="post" name="holidaysForm" id="holidaysForm" enctype="multipart/form-data" action="#">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <span class="marg-right10"><label> <input type="checkbox" class="i-checks" name='system_datealertd_aftholiday' id='system_datealertd_aftholiday' <?php
                                            if (isset($datealert->aftholiday) && $datealert->aftholiday == 'on') {
                                                echo 'checked';
                                            }
                                            ?>/> After on Holidays </label></span>
                                    <span class="marg-right10"><label> <input type="checkbox" class="i-checks" name='system_datealertd_vacation' id='system_datealertd_vacation' <?php
                                            if (isset($datealert->vacation) && $datealert->vacation == 'on') {
                                                echo 'checked';
                                            }
                                            ?>/> Vacations </label></span>
                                    <span class="marg-right10"><label> <input type="checkbox" class="i-checks" name='system_datealertd_weekends' id='system_datealertd_weekends' <?php
                                            if (isset($datealert->weekends) && $datealert->weekends == 'on') {
                                                echo 'checked';
                                            }
                                            ?>/> Weekends </label></span>
                                </div>
                                <div class="form-group">
                                    <label>Holidays</label>
                                    <textarea class="form-control" style="width: 100%;height: 200px;" name='system_holidays' id='system_holidays'><?php echo isset($firm['holidays']) ? str_replace("##", '', $firm['holidays']) : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Client birthday auto alert: Days Before / After</label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="number" class="form-control"  name='system_birthdays_before' id='system_birthdays_before' value='<?php echo isset($birthdaydate->before) ? $birthdaydate->before : '' ?>'/>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="number" class="form-control" name='system_birthdays_after' id='system_birthdays_after' value='<?php echo isset($birthdaydate->after) ? $birthdaydate->after : '' ?>' />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span><label> <input type="checkbox" class="i-checks"  name='system_main_bialaddcalevent' id='system_main_bialaddcalevent' <?php
                                            if (isset($main1->bialaddcalevent) && $main1->bialaddcalevent == 'on') {
                                                echo 'checked';
                                            }
                                            ?>/> Birthday alert adding calendar events </label></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group text-center m-t-md m-b-xs">
                                    <button type="button" id='saveholidays'class="btn btn-primary btn-md">Save</button>
                                    <button type="submit" class="btn btn-danger btn-md">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                autoclose: true
            });
        });
        $('#saveholidays').on('click', function () {
            savetoadmin('holidaysForm');

        });
    </script>