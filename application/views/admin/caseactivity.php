

<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
    vertical-align: middle;
    }
</style>
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                    <span>Case Activity</span>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Case Activity</h5>
                        <a href="#order" class="btn btn-primary btn-sm pull-right" data-toggle="modal">Order</a>
                        <a href="#delete" class="btn btn-primary btn-sm pull-right marg-right5" data-toggle="modal">Delete</a>
                        <a href="#edit" class="btn btn-primary btn-sm pull-right marg-right5" data-toggle="modal">Edit</a>
                        <a href="#add" class="btn btn-primary btn-sm pull-right marg-right5" data-toggle="modal">Add</a>
                    </div>
                    <div class="ibox-content">
                        <form>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Type</th>
                                                    <th>Category</th>
                                                    <th>Case Activity Name</th>
                                                    <th>Access Level</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                                    </td>
                                                    <td>*</td>
                                                    <td>1</td>
                                                    <td>General</td>
                                                    <td>1</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                                    </td>
                                                    <td>*</td>
                                                    <td>2</td>
                                                    <td>Monthly Status</td>
                                                    <td>1</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                                    </td>
                                                    <td>*</td>
                                                    <td>3</td>
                                                    <td>Correspondence and Emails</td>
                                                    <td>1</td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                                    </td>
                                                    <td>*</td>
                                                    <td>4</td>
                                                    <td>Settlement</td>
                                                    <td>1</td>
                                                </tr>
                                                  <tr>
                                                    <td>
                                                        <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                                    </td>
                                                    <td>*</td>
                                                    <td>5</td>
                                                    <td>Vocational Rehab Expert</td>
                                                    <td>1</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
       
        
        </div>
   
    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            
            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });
            
            $('.color').colorpicker({
                 autoclose: true
            });
        });
    </script>
        
    <div id="add" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add New Activity</h4>
                </div>
                <div class="modal-body">
                  <form role="form">
                      <div class="tabs-container">
                          <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#tab-1">General</a></li>
                              <li class=""><a data-toggle="tab" href="#tab-2">User</a></li>
                              <li class=""><a data-toggle="tab" href="#tab-3">Extended UDF</a></li>
                          </ul>
                          <div class="tab-content">
                              <div id="tab-1" class="tab-pane active">
                                  <div class="panel-body">
                                      <div class="ibox float-e-margins">
                                          <div class="ibox-content">
                                              <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Title</label>
                                                             <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Type of Case</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Access level</label>
                                                            <select class="form-control">
                                                                <option>Full</option>
                                                                <option>None</option>
                                                                <option>None</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>User defined page ID</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Category</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Event Width</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Event Height</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Extended Categories</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group text-center marg-top15">
                                                            <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                            <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                        </div>
                                                    </div>
                                              </div>
                                          </div>
                                          
                                      </div>
                                  </div>
                              </div>
                              <div id="tab-2" class="tab-pane">
                                  <div class="panel-body">
                                      <div class="ibox float-e-margins">
                                          <div class="ibox-content">
                                              <div class="row">
                                                  <div class="col-sm-7">
                                                        <div class="form-group">
                                                            <span><label><input type="checkbox" class="i-checks" /> Enable the user defined page </label></span>
                                                            <span><label><input type="checkbox" class="i-checks" /> Edit the user defined page first </label></span>
                                                            <span><label><input type="checkbox" class="i-checks" /> Use formatting defaults </label></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Access level to add</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Access level to edit</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Page caption / title</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                  </div>
                                                  <div class="col-sm-5">
                                                        <div class="form-group">
                                                            <label>Top Offset</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Left Offset</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Columns</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Width</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-md btn-primary" id="savename">User</button>
                                                        </div>
                                                  </div>
                                                  <div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15">
                                                        <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                        <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div id="tab-3" class="tab-pane ">
                                    <div class="panel-body">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <p>Enter the user defined fields for this category</p>
                                                            <p>Separate each field by pressing enter</p>
                                                        </div>
                                                        <div class="form-group">
                                                            <span><label> <input type="checkbox" class="i-checks" /> Activat Ectended UDFs </label></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea class="form-control" style="width: 100%;height: 200px;"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                      <div class="form-group text-center marg-top15">
                                                          <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                          <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                      </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
                </div> <!--End modal-body-->
            </div>
            <!-- End Modal content-->
            
        </div>
    </div> <!--End Add Modal-->
    
    <div id="edit" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Case Activity</h4>
                </div>
                <div class="modal-body">
                  <form role="form">
                      <div class="tabs-container">
                          <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#tab-1">General</a></li>
                              <li class=""><a data-toggle="tab" href="#tab-2">User</a></li>
                              <li class=""><a data-toggle="tab" href="#tab-3">Extended UDF</a></li>
                          </ul>
                          <div class="tab-content">
                              <div id="tab-1" class="tab-pane active">
                                  <div class="panel-body">
                                      <div class="ibox float-e-margins">
                                          <div class="ibox-content">
                                              <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Title</label>
                                                             <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Type of Case</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Access level</label>
                                                            <select class="form-control">
                                                                <option>Full</option>
                                                                <option>None</option>
                                                                <option>None</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>User defined page ID</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Category</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Event Width</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Event Height</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Extended Categories</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group text-center marg-top15">
                                                            <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                            <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                        </div>
                                                    </div>
                                              </div>
                                          </div>
                                          
                                      </div>
                                  </div>
                              </div>
                              <div id="tab-2" class="tab-pane">
                                  <div class="panel-body">
                                      <div class="ibox float-e-margins">
                                          <div class="ibox-content">
                                              <div class="row">
                                                  <div class="col-sm-7">
                                                        <div class="form-group">
                                                            <span><label><input type="checkbox" class="i-checks" /> Enable the user defined page </label></span>
                                                            <span><label><input type="checkbox" class="i-checks" /> Edit the user defined page first </label></span>
                                                            <span><label><input type="checkbox" class="i-checks" /> Use formatting defaults </label></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Access level to add</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Access level to edit</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Page caption / title</label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                  </div>
                                                  <div class="col-sm-5">
                                                        <div class="form-group">
                                                            <label>Top Offset</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Left Offset</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Columns</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Width</label>
                                                            <input type="number" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-md btn-primary" id="savename">User</button>
                                                        </div>
                                                  </div>
                                                  <div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15">
                                                        <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                        <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div id="tab-3" class="tab-pane ">
                                    <div class="panel-body">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <p>Enter the user defined fields for this category</p>
                                                            <p>Separate each field by pressing enter</p>
                                                        </div>
                                                        <div class="form-group">
                                                            <span><label> <input type="checkbox" class="i-checks" /> Activat Ectended UDFs </label></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea class="form-control" style="width: 100%;height: 200px;"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                      <div class="form-group text-center marg-top15">
                                                          <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                          <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                      </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
                </div> <!--End modal-body-->
            </div>
            <!-- End Modal content-->
            
        </div>
    </div> <!--End Edit Modal-->
    
    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form role="form" class="text-center">
                        <p>Are you sure to delete this activity</p>
                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary btn-md">Delete</button>
                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                        </div>
                    </form>
                </div> <!--End modal-body-->
            </div>
            <!-- End Modal content-->
            
        </div>
    </div> <!--End Delete Modal-->
    
    <div id="order" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Order Categories</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="8%"></th>
                                        <th>Type</th>
                                        <th width="80%">Title</th>
                                        <th>Category</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                        </td>
                                        <td>*</td>
                                        <td>General</td>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                        </td>
                                        <td>*</td>
                                        <td>Monthly Status</td>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                        </td>
                                        <td>*</td>
                                        <td>Correspondence and Emails</td>
                                        <td>1</td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                        </td>
                                        <td>*</td>
                                        <td>Settlement</td>
                                        <td>1</td>
                                    </tr>
                                      <tr>
                                        <td>
                                            <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                        </td>
                                        <td>*</td>
                                        <td>Vocational Rehab Expert</td>
                                        <td>1</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                            <button type="submit" class="btn btn-primary btn-md">Browse</button>
                        </div>
                    </form>
                </div> <!--End modal-body-->
            </div>
            <!-- End Modal content-->
            
        </div>
    </div> <!--End Delete Modal-->
    
</body>
</html>