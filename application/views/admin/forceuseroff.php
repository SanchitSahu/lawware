<style>.error{color: red;}</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <span>Force Users Off</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="row">
                    <form method="post">
                        <div class="col-sm-7 col-xs-12">
                            <div class="ibox-title">
                                <h5>Force Users Off - <?php echo APPLICATION_NAME;?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="forcelogmessage">This Window allows you to log Users out of <?php echo strtoupper(APPLICATION_NAME);?>. This may be useful before you do system maintenance. Enter a * to force everyone off the system.</div>
                                            <label>User Initials</label>
                                            <input type="text" name="userinitials" id="userinitials" class="form-control" required="">
                                            <span id="erroruserinitials" class="error"></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Minutes</label>
                                            <input type="number" name="minutes" id="minutes" class="form-control" required=""> 
                                            <span id="errorminutes" class="error"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group m-t-md m-b-xs pull-right">
                            <button type="button" class="btn btn-primary btn-md" id="forceproceed">Proceed</button>
                        </div>
                                        
                                    </div>

                                </div>
                              
                               
                            </div>
                        </div>
                       
                    </form>
                </div>
             
                <!--/form-->
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets'); ?>/js/jquery.mask.min.js"></script>
<script>
$( document ).ready(function() {    
    $('#forceproceed').on('click', function () {
        
        if ($('#userinitials').val() == '') {
            $('#erroruserinitials').text('Initials Required');
            return false;
        }
        else if ($('#minutes').val() == '') {
            $('#errorminutes').text('Minutes Required');
            return false;
        }
        else
        {
            var userinitials = $('#userinitials').val();
            var minutes = $('#minutes').val();
            swal({
                title: "Are you sure you want to force some of the users off?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            },function () {
                $.blockUI();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/forceuseroff',
                method: 'POST',
                data: {
                    userinitials: userinitials,
                    minutes: minutes
                },
                
                success: function (data) {
                    $.unblockUI();
                     swal("Done", "All done logging users off", "success");
                },
                error: function (data) {

                }
            });
             });
           }
    });
});    
</script>