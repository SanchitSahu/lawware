<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
        vertical-align: middle;
    }
    .ibox-gm {
        margin-bottom: 25px;
        margin-top: 0;
        padding: 0;
    }
    div.hide { display:none; }
    div.show { display:block; }
    .g-name{
        display: inline-block;
        font-size: 14px;
        font-weight: bold;
        margin: 0 0 7px;
        padding: 3px 0 0 0;
        text-overflow: ellipsis;
        float: left; 
    }
    .mail-contact {
        width: 0%;
    }
    #groupnameerror,#initialerror    {
        color: red;
        font-weight: normal;
    }
    #initials,#select1{
        width: 250px;
        height: 200px;
        margin-left: 3px;
        margin-right: 3px;
    }
    a.btn.btn-default.buttons-excel.buttons-html5{margin-right: 5px;
    border-radius: 5px !important;float: right;}
    
    a.btn.btn-default.buttons-pdf.buttons-html5{margin-right: 5px;
    border-radius: 5px !important;float: right;}
    a.btn.btn-default.buttons-print{border-radius: 5px !important;float: right ;}
    .dt-buttons.btn-group{float: right;width: 100%;}

</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a href="dashboard.php">Home</a> /
                <span>Group Management</span>
            </div>
            <div class="row">
                <div class="col-lg-6 ibox-gm float-e-margins">
                    <div class="ibox-title">
                        <h5>Group Management</h5>
                        <a class="btn pull-right btn-primary btn-sm" href="#addgroup" data-toggle="modal">Add New Group</a>
                    </div>
                    <div class="ibox-content marg-bot15">
                        <form>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table id="grouptable" class="grouptable table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                       <!-- <th><input type="checkbox" id="checkall"/></th>-->
                                                        <th>Id</th>
                                                        <th>Group Name</th>
                                                        <th>Group Type</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 1;
                                                    foreach ($groupname as $getgroupname) {
                                                        ?>
                                                        <tr class="">
                                                                <!--<td class="check-mail">
                                                                        <input type="checkbox" class="checkbox">
                                                                </td>-->
                                                            <td class="mail-contact"><?php echo $i; ?></a></td>
                                                            <td class="mail-subject"><?php echo $getgroupname->group_name; ?></td>
                                                            <td class="mail-subject"><?php echo $getgroupname->module_name; ?></td>
                                                            <td>
                                                                <a data-groupname="<?php echo $getgroupname->group_id; ?>" data-groupid="<?php echo $getgroupname->group_name; ?>" data-grouptype="<?php echo $getgroupname->module_name; ?>" class="groupname btn btn-info btn-circle-action btn-circle"><i class="icon fa fa-eye"></i></a>
                                                                <a href="javascript:void(0)" class="marg-left5 btn btn-danger btn-circle-action btn-circle group_delete" data-groupid="<?php echo $getgroupname->group_id; ?>"><i class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>	

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="group-name" class="col-lg-6 ibox-gm float-e-margins">
                    <div>
                        <div class="ibox-title">
                            <div id="gname" class="g-name"></div>
                            <button class="btn btn-danger pull-right delete_all">Delete Selected</button>&nbsp;
                            <a class="btn pull-right btn-primary btn-sm" href="#addnew" data-toggle="modal" style="margin-right:10px;">Edit Members</a>
                        </div>
                        <div class="ibox-content marg-bot15">
                            <div class="form-group">
                                <div class="error" style="color:red;" id="groupmemberchk"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table id="groupmembertable" class="groupmember table table-striped table-bordered table-hover dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th width="5%"><input type="checkbox" id="selectall"/></th>
                                                        <th width="5%">Id</th>
                                                         <th width="80%">Group Name</th>
                                                        <th width="80%">Initials</th>
                                                        <th width="10%">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="groupdata"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addgroup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add a Group</h4>
            </div>
            <div class="modal-body">

                <form method="post" name="addgroupform" id="addgroupform">
                    <div class="form-group">
                        <label>Group Type</label><br />
                        <input type="radio" name="modulename" id="tasks" value="tasks" checked><label for="tasks">Tasks</label>
                        <input type="radio" name="modulename" id="email" value="email"><label for="email">Email</label>
                    </div>
                    <div class="form-group">
                        <label>Group Name</label>
                        <input type="text" class="form-control" name="groupname" id="groupname" placeholder="Enter Group Name"/>
                        <label id="groupnameerror"></label>
                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="button" class="btn btn-md btn-primary groupadd" name="groupadd" id="groupadd">Save</button>
                        <a class="btn btn-md btn-danger" href="" data-toggle="modal" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="addnew" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add new</h4>
            </div>
            <div class="modal-body">
                <form method="post">
                    <input type="hidden" name="groupnameid" id="groupnameid" value="">

                    <div class="form-group">
                        <?php /* <select id="initials" name="initials" class="select2_demo_2 form-control" multiple="multiple">
                          <option value="">--Select Members--</option>
                          <?php
                          foreach($initialname as $initialnamelist)
                          {
                          ?>
                          <option value="<?php echo $initialnamelist->initials;?>"><?php echo $initialnamelist->initials;?></option>
                          <?php }?>
                          </select> */ ?>

                        <div class="form-group locationdepo">
                            <label>Group Members</label>
                            <select multiple="multiple" id="select1" class="whatever">
                                <?php
                                foreach ($initialname as $initialnamelist) {
                                    ?>
                                    <option value="<?php echo $initialnamelist->initials; ?>"><?php echo $initialnamelist->initials; ?></option>
                                <?php } ?>
                            </select>

                            <label>Group Members to Merge</label>
                            <select multiple="multiple" id="initials" name="initials" class="whatever"></select>
                        </div>

                        <label id="initialerror"></label>
                    </div>

                    <div class="form-group text-center marg-top15">
                        <button type="button" name="savegroupmember" id="savegroupmember" class="btn btn-md btn-primary">Save</button>
                        <a class="btn btn-md btn-danger" href=""  data-toggle="modal" data-dismiss="modal">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var allStaffList = <?php echo $initialname; ?>;
    var groupmembertable = '';
    var grouptable = '';

    document.getElementById("addgroupform").onsubmit = function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.groupadd').trigger("click");
    };

    $(document).ready(function () {
        grouptable = $('#grouptable').DataTable({
                dom : "B<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'<'#colvis'>p>>",
                buttons: [
                    {extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i>  Print',
                        exportOptions: {
                        columns: ':not(:last-child)'
                        },
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    },
                    
                    {
                        extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i>  PDF', title: '<?php echo APPLICATION_NAME; ?> : Search Group', header: true, 
                exportOptions: {
                          columns: ':visible',
                          columns: ':not(:last-child)'
                        },

                customize: function (doc) {

                            doc.styles.table = {
                                width: '100%'
                            };
                            doc.styles.tableHeader.alignment = 'left';
                            doc['header'] = function (page, pages) {
                                return {
                                    columns: [
                                        '<?php echo APPLICATION_NAME; ?> ',
                                        {
                                            alignment: 'right',
                                            text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                        }
                                    ],
                                    margin: [10, 0]
                                };
                            };
                            doc.content[1].table.widths = '*';
                        }
                    },
                            {extend: 'excel', text: '<i class="fa fa-file-excel-o"></i>  Excel', title: '<?php echo APPLICATION_NAME; ?> : Group',
                       exportOptions: {
                            columns: ':not(:last-child)'
                        }
                    },

            ],
            pageLength: 5,
            searchable: true,
            lengthMenu: [5, 10, 20, 50, 100], scrollY: "330px",
            scrollCollapse: true
        });
        groupmembertable = $('#groupmembertable').DataTable({
            pageLength: 5,
            searchable: true,
            lengthMenu: [5, 10, 20, 50, 100], 
            scrollX: true,
            scrollY: "330px",
            scrollCollapse: true,
            
            dom : "B<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'<'#colvis'>p>>",
            buttons: [
                {extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i>  Print',
                    exportOptions: {
                    columns: ':not(:last-child)'
                    },
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                },
                {
                    extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i>  PDF', title: '<?php echo APPLICATION_NAME; ?> : Group Management Members', header: true, 
             exportOptions: {
                      columns: ':visible',
                      columns: ':not(:last-child)'
                    },
            customize: function (doc) {
                        doc.styles.table = {
                            width: '100%'
                        };
                        doc.styles.tableHeader.alignment = 'left';
                        doc['header'] = function (page, pages) {
                            return {
                                columns: [
                                    '<?php echo APPLICATION_NAME; ?> ',
                                    {
                                        alignment: 'right',
                                        text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                    }
                                ],
                                margin: [10, 0]
                            };
                        };
                        doc.content[1].table.widths = '*';
                    }
                },
               {extend: 'excel', text: '<i class="fa fa-file-excel-o"></i>  Excel', title: '<?php echo APPLICATION_NAME; ?> : Group Management Members',exportOptions: {
                    columns: ':not(:last-child)'
                    }
                }         
                
        ]
        });
        $(".select2_demo_2").select2({
            placeholder: "Select Group Members",
            allowClear: true
        });

        $('.groupname').click(function () {
            var groupnameval = $(this).data("groupname");
            var groupnamefill = $(this).data("groupid") + ' (Type : ' + $(this).data("grouptype") + ')';

            document.getElementById('gname').innerHTML = groupnamefill;
            
            $('#groupnameid').val(groupnameval);
            $.ajax({
                url: '<?php echo base_url(); ?>admin/groupmembermanagement',
                method: 'POST',
                data: {
                    groupname: groupnameval
                },
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    data = JSON.parse(data);
                    $.unblockUI();
                    $('#groupdata').removeClass('hide');
                    $('.groupname').parents('tr').removeClass('table-selected');
                    $('.groupname[data-groupname=' + groupnameval + ']').parents('tr').addClass('table-selected');
                    groupmembertable.clear();
                    if (data.status) {
                        groupmembertable.rows.add($(data.data));
                        groupmembertable.draw();
                    } else {
                        groupmembertable.draw();
                    }
                },
                error: function (data) {

                }
            });

        });
        $('a.groupname:eq(0)').trigger("click");

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.date').datepicker({
            forceParse: false,
            autoclose: true
        });

        $('.color').colorpicker({
            autoclose: true
        });

        /* Save Group data and fetch data*/

        $('.groupadd').click(function () {

            var groupname = $('#groupname').val();
            var modulename = $('input[name=modulename]:checked').val();
            if (groupname == '')
            {
                $('#groupnameerror').html('Please Enter Group Name');
                return false;
            }

            $.ajax({
                url: '<?php echo base_url(); ?>admin/storegroupdata',
                method: 'POST',
                data: {
                    groupname: groupname,
                    modulename: modulename
                },
                beforeSend: function () {
                    $.blockUI();
                },
                success: function (data) {
                    $.unblockUI();
                    if (data == "This Group Already Exist") {
                        $("#groupnameerror").html(data);
                    } else {
                        swal("Save!", "Group Added Successfully !!", "success");
                        $('#addgroup').modal('hide');
                        window.location.href = '<?php echo base_url(); ?>admin/groupmanagement';
                    }
                },
                error: function (data) {
                    console.log(data);

                }
            });
        });

        $('#addgroup.modal').on('hidden.bs.modal', function () {
            $('#addgroupform')[0].reset();
            $("#groupnameerror").html('');
        });

        /* End Save Group data and fetch data*/

        /*save group Member*/

        $('#savegroupmember').click(function () {

            var groupnameid = $('#groupnameid').val();
            var initialschk = $('#initials option').length;
            // var initials = $('#initials').val();

            var initials = [];
            $.each($("#initials option"), function () {
                initials.push($(this).val());
            });
            var initials = initials.join(",");

            if (initialschk <= 0)
            {
                $('#initialerror').html('Please select Members');
                return false;
            } else
            {
                $('#initialerror').html('');
            }

            $.ajax({
                url: '<?php echo base_url(); ?>admin/storegroupmemberdata',
                method: 'POST',
                data: {
                    groupnameid: groupnameid,
                    initials: initials
                },
                beforeSend: function () {
                    $.blockUI();
                },
                success: function (data) {
                    $('.groupname[data-groupname=' + groupnameid + ']').trigger('click');
                    $('#addnew').modal('hide');
                    swal("Save!", "Group Members Added Successfully !!", "success");

                },
                error: function (data) {

                }
            });
        });

        /* End Save group member */

        /* Group Delete Single Record */

        $(document.body).on('click', '.group_delete', function () {
            var groupid = $(this).data('groupid');
            swal({
                title: "Are you sure to Remove this Group?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/deletegroup',
                    method: 'POST',
                    data: {
                        groupid: groupid
                    },
                    success: function (data) {
                        $.unblockUI();
                        swal("Deleted!", "Group Deleted Successfully !!", "success");
                        $('.group_delete[data-groupid=' + groupid + ']').parents('tr').remove();
                        // $('a.groupname:eq(0)').trigger("click");
                        window.location.href = '<?php echo base_url(); ?>admin/groupmanagement';

                    }
                });
            });
        });

        /* End of Delete Group Single Record*/

        /* Delete Group Member Single Record*/

        $(document.body).on('click', '.group_member_delete', function () {
            var groupmemberid = $(this).data('groupmemberid');
            var groupid = $(this).data('groupid');


            swal({
                title: "Are you sure to Remove this Group Members?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.blockUI();
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/deletegroupmember',
                    method: 'POST',
                    data: {
                        groupmemberid: groupmemberid
                    },
                    success: function (data) {
                        $.unblockUI();
                        $('.group_member_delete[data-groupmemberid=' + groupmemberid + ']').parents('tr').remove();
                        $('.groupname[data-groupname=' + groupid + ']').trigger('click');
                        swal("Deleted!", "Group Members Deleted Successfully !!", "success");
                        // groupmembertable.draw();
                    }
                });
            });
        });

        /* End of Group Member Delete Single Reccord*/

        /*Group Member Multiple Delete*/

        $('#selectall').on('click', function (e) {
            if ($(this).is(':checked', true))
            {
                $(".checkbox1").prop('checked', true);
                $('#groupmemberchk').html("");

            } else {
                $(".checkbox1").prop('checked', false);
            }
        });

        $('.delete_all').on('click', function (e) {
            var allVals = [];
            $(".checkbox1:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });


            if (allVals.length <= 0)
            {
                $('#groupmemberchk').html("Please select Minimum one record.");
            } else {
                $('#groupmemberchk').html("");

                var join_selected_values = allVals.join(",");
                swal({
                    title: "Are you sure to Remove this Group Members?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $.blockUI();

                    $.ajax({
                        url: 'admin/deletemultiplegroupmember',
                        type: 'POST',
                        data: 'ids=' + join_selected_values,
                        success: function (data) {
                            $.unblockUI();
                            swal("Deleted!", "Group Members Deleted Successfully !!", "success");
                            $(".checkbox1:checked").each(function () {
                                $(this).parents("tr").remove();
                            });
                            window.location.href = '<?php echo base_url(); ?>admin/groupmanagement';

                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });
                });

                $.each(allVals, function (index, value) {
                    $('table tr').filter("[data-row-id='" + value + "']").remove();
                });

            }
        });

        /* End of Multiple Group Member Delete*/
        $("#addnew.modal").on("show.bs.modal", function () {
            var groupnameid = $('#groupnameid').val();
            var a = $('#initials');
            a.siblings("select").append(a.find("option:selected"));
            $("#select1, #initials", a.parent()).each(function (d, c) {
                var b = $(d).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $('#initials').html(b);
            });
            $.blockUI();

            $.ajax({
                url: '<?php echo base_url(); ?>admin/checkduplicatevalue',
                method: 'POST',
                data: {
                    groupid: groupnameid
                },
                success: function (data) {
                    $.unblockUI();
                    initalname = '';
                    tempStaffHtml = '';
                    data = $.parseJSON(data);

                    if (data.exitingmember.length > 0)
                    {
                        $.each(data.exitingmember, function (i, item) {
                            initalname += '<option value="' + item.initials + '">' + item.initials + '</option>';
                        });
                        initalname += '';
                        $('#initials').html(initalname);
                    }
                    if (data.newmember.length > 0)
                    {
                        $.each(data.newmember, function (i, item) {
                            tempStaffHtml += '<option value="' + item.initials + '">' + item.initials + '</option>';
                        });
                        tempStaffHtml += '';
                        $('#select1').html(tempStaffHtml);
                    }


                }

            });

        });

        $("#select1, #initials").change(function () {
            var a = $(this);
            a.siblings("select").append(a.find("option:selected"));
            $("#select1, #initials", a.parent()).each(function (d, c) {
                var b = $(c).find("option");
                b = b.sort(function (f, e) {
                    return f.value - e.value;
                });
                $(this).html(b);
            });
        });

    });
</script>
