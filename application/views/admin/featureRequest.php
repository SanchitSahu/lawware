<?php //echo '<pre>'; print_r($checklist); exit; ?>
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Admin</a> /
                    <span>Feature Request</span>
                </div>
                <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Feature Request</h5>
            </div>
            <div class="ibox-content">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="feature1" name="feature_module" id="feature_module"style="position: absolute; opacity: 0;" indeterminate="true">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> <?php echo strtoupper(APPLICATION_NAME);?></label></div>

                    </div>
                    <div class="col-md-4">
                        <div class="i-checks"><label class=""> <div class="iradio_square-green " style="position: relative;">
                            <input type="radio" checked="" value="feature2" name="feature_module" id="feature_module" style="position: absolute; opacity: 0;">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> SNS</label></div>

                    </div>
                    <div class="col-md-4">
                        <div class="i-checks"><label class=""> 
                            <div class="iradio_square-green" style="position: relative;">
                            <input type="radio" id="feature_module" checked="" value="feature3" name="feature_module" style="position: absolute; opacity: 0;">
                            <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                        </div> <i></i> CRM</label></div>

                    </div>
                </div>
                <div class="clearfix"> <span id="error_desc" class="col-md-12" style="color:#FF0000; text-align: center;text-transform:capitalize;"></span></div>
                <form id="requestForm" name="requestForm" method="post">

                                <div class="row form-group" id="feature-lawware" style="display: none; padding: 20px;">
                                    <div class="col-sm-3"><label><?php echo APPLICATION_NAME; ?></label></div>
                                    <div class="col-sm-3">
                                        <select class="form-control" name="submodule" onchange="Clear_text()" id="submodule">
                                            <option value="">Select Sub Module</option>
                                            <option value="Cases">Cases</option>
                                            <option value="Calendar">Calendar</option>
                                            <option value="Emails">Emails</option>
                                            <option value="Rolodex">Rolodex</option>
                                            <option value="Tasks">Tasks</option>
                                            <option value="Reminders">Reminders</option>
                                            <option value="Reports">Reports</option>
                                            <option value="PNC">PNC</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-7">
                                        <textarea class="form-control" name="lawware_description" onChange="clear_error1()" id="lawware_description"></textarea>
                                    <span id="error_law_desc" style="color:#FF0000"></span>
                                    </div>

                                </div> 
                                <div class="row form-group" id="feature-sns" style="display: none;padding: 20px;">
                                        <div class="col-sm-3">
                                        <label>SNS</label>
                                         </div>
                                        <div class="col-sm-6">
                                         <textarea class="form-control" name="sns_description" id="sns_description" onChange="clear_error()"></textarea>
                                        </div>

                                        <div class="col-sm-3">&nbsp;
                                        </div>
                                </div>
                                <div class="row form-group" id="feature-crm" style="display: none;padding: 20px;" >
                                     <div class="col-sm-3">
                                        <label>CRM</label>
                                      </div>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" name="crm_description" id="crm_description" onChange="clear_error()"></textarea>
                                    </div>

                                     <div class="col-sm-3">&nbsp;
                                     </div>
                                </div>
                            <div class="form-group text-center m-t-md m-b-xs" id="save_data" style="display: none;">
                                
                                <button type="button" id="save_req" class="btn btn-primary btn-md">Save</button>
                                <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                            </div>
                </form>
            </div>
        </div>
         </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[name=feature_module]').removeAttr('checked');
        $('.i-checks').iCheck({
            //checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.date').datepicker({
            forceParse: false,
            autoclose: true
        });

        $('.color').colorpicker({
             autoclose: true
        });
    

    $(document.body).on('ifChanged','#feature_module',function(){
        var $thisval = $(this).val();
        if($thisval =="feature1")
        {
            $('#feature-lawware').show();
            $('#save_data').show();
            $('#feature-sns').hide();
            $('#feature-crm').hide();
            $("#crm_description").val('');
            $("#sns_description").val('');
        }
        else if($thisval =="feature2")
        {
            $('#feature-sns').show();
            $('#save_data').show();
            $('#feature-lawware').hide();
            $("#submodule").val('');
            $("#lawware_description").val('');
            $('#feature-crm').hide();
            $("#crm_description").val('');

        }
        else if($thisval =="feature3")
        {
            $('#feature-crm').show();
            $('#save_data').show();
            $('#feature-lawware').hide();
            $("#submodule").val('');
            $("#lawware_description").val('');
            $('#feature-sns').hide();
            $("#sns_description").val('');
        }
    });

    });
    $('#save_req').on('click', function () {
        var feature_mod_val = $('#feature_module:checked').val();
        var subcat = $("#submodule").val();
        var law_desc = $("#lawware_description").val();
        var sns_desc = $("#sns_description").val();
        var crm_desc = $("#crm_description").val();
        var flag = 1;
        //alert(sns_desc.length);
        if(subcat.length != 0 && law_desc.length == 0 && feature_mod_val =='feature1')
        {
            $("#error_desc").text('');
            $("#error_desc").text('Subcategory feature requirement description required');
            flag = 0;
        }
        else if(law_desc.length != 0 && subcat.length == 0 && feature_mod_val =='feature1')
        {
            $("#error_desc").text('');
            $("#error_desc").text('Subcategory required');
            flag = 0;
        }
        else if(law_desc.length == 0 && sns_desc.length == 0 && crm_desc.length == 0)
        {
            $("#error_desc").text('');
            $("#error_desc").text('Atleast one Feature requirement description required');
            flag = 0;
        }

        if(flag == 1)
        {
            alert(flag); return false;
            $.ajax({
                url: '<?php echo base_url(); ?>admin/saveRequest',
                method: 'POST',
                data: $("#requestForm").serialize(),
                beforeSend: function (xhr) {
                      //$.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                        //$.unblockUI();
                },
                success: function (result) {
                      var obj = $.parseJSON(result);
                       if (obj.status == '1') {
                              swal("Success !", obj.message, "success",function(){                                                          location.reload();
                              });

                        }
                }
            });
        }

   });

   function clear_error1()
   {
       $("#error_law_desc").text('');
       $("#error_sns_desc").text('');
   }

   function clear_error()
   {
       $("#error_sns_desc").text('');
   }

   function Clear_text()
   {
       $("#lawware_description").val('');
   }
</script>

