<?php
$pncdd = json_decode($firm['pncdd']);
$main = json_decode($firm['main1']);
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a>Configure System Defaults</a> /
                <span>Prospects</span>
            </div>
            <form method="post" name="PopupForm" id="PopupForm" enctype="multipart/form-data" action="#">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">Dropdowns</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">Mailing Lists</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <span style="color: #D51616; font-size:14px;">For Dropdown please enter comma seprated values.</span><br/>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Type of Case</label>
                                                            <input type="text" id="system_pncdd_toc" name="system_pncdd_toc" class="form-control" value="<?php echo isset($pncdd->toc) ? $pncdd->toc : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <label>Dropdowns</label>
                                                            <span><label> <input type="checkbox"  class="i-checks" id="system_pncdd_drp1" name="system_pncdd_drp1" <?php
                                                                    if (isset($pncdd->drp1) && $pncdd->drp1 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <label>Force non blank entry</label>
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk1" name="system_pncdd_chk1" <?php
                                                                    if (isset($pncdd->chk1) && $pncdd->chk1 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?>  /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input type="text" id="system_pncdd_status" name="system_pncdd_status" class="form-control" value="<?php echo isset($pncdd->status) ? $pncdd->status : '' ?>">
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_drp2" name="system_pncdd_drp2" <?php
                                                                    if (isset($pncdd->drp2) && $pncdd->drp2 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk2" name="system_pncdd_chk2" <?php
                                                                    if (isset($pncdd->chk2) && $pncdd->chk2 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Referral Code</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input type="text" id="system_pncdd_refc" name="system_pncdd_refc" class="form-control" value="<?php echo isset($pncdd->refc) ? $pncdd->refc : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_drp3" name="system_pncdd_drp3" <?php
                                                                    if (isset($pncdd->drp3) && $pncdd->drp3 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?>/> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk3" name="system_pncdd_chk3" <?php
                                                                    if (isset($pncdd->chk3) && $pncdd->chk3 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Referral Source</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input type="text" id="system_pncdd_refsrc" name="system_pncdd_refsrc" class="form-control" value="<?php echo isset($pncdd->refsrc) ? $pncdd->refsrc : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_drp4" name="system_pncdd_drp4" <?php
                                                                    if (isset($pncdd->drp4) && $pncdd->drp4 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk4" name="system_pncdd_chk4" <?php
                                                                    if (isset($pncdd->chk4) && $pncdd->chk4 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?>/> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Referred By</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input type="text" id="system_pncdd_refby" name="system_pncdd_refby" class="form-control" value="<?php echo isset($pncdd->refby) ? $pncdd->refby : '' ?>">
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_drp5" name="system_pncdd_drp5" <?php
                                                                    if (isset($pncdd->drp5) && $pncdd->drp5 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?>/> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk5" name="system_pncdd_chk5" <?php
                                                                    if (isset($pncdd->chk5) && $pncdd->chk5 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Association</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input type="text" id="system_pncdd_assoc" name="system_pncdd_assoc" class="form-control" value="<?php echo isset($pncdd->assoc) ? $pncdd->assoc : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_drp6" name="system_pncdd_drp6" <?php
                                                                    if (isset($pncdd->drp6) && $pncdd->drp6 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk6" name="system_pncdd_chk6" <?php
                                                                    if (isset($pncdd->chk6) && $pncdd->chk6 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Referred to</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input type="text" id="system_pncdd_refto" name="system_pncdd_refto" class="form-control" value="<?php echo isset($pncdd->refto) ? $pncdd->refto : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_drp7" name="system_pncdd_drp7" <?php
                                                                    if (isset($pncdd->drp7) && $pncdd->drp7 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk7" name="system_pncdd_chk7" <?php
                                                                    if (isset($pncdd->chk7) && $pncdd->chk7 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?>/> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Our Ref Status </label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input type="text" id="system_pncdd_ors" name="system_pncdd_ors" class="form-control" value="<?php echo isset($pncdd->ors) ? $pncdd->ors : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_drp8" name="system_pncdd_drp8" <?php
                                                                    if (isset($pncdd->drp8) && $pncdd->drp8 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?>/> </label></span>
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_chk8" name="system_pncdd_chk8" <?php
                                                                    if (isset($pncdd->chk8) && $pncdd->chk8 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Pending</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <!--<input type="text" class="form-control">-->
                                                        </div>
                                                        <div class="col-xs-2 text-center">
                                                            <!--span><label> <input type="checkbox" class="i-checks" /> </label></span-->
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_pending" name="system_pncdd_pending" <?php
                                                                    if (isset($pncdd->pending) && $pncdd->pending == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                    <!--button type="button" name="savedropdown" id="savedropdown" class="btn btn-primary btn-md">Save</button>
                                                    <button type="submit" class="btn btn-primary btn-md">Cancel</button-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <label>Title</label>
                                                            <input type="text" class="form-control" id="system_pncdd_t1" name="system_pncdd_t1" value="<?php echo isset($pncdd->t1) ? $pncdd->t1 : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <label>Add defaults</label>
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_d1" name="system_pncdd_d1" <?php
                                                                    if (isset($pncdd->d1) && $pncdd->d1 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" id="system_pncdd_t2" name="system_pncdd_t2" value="<?php echo isset($pncdd->t2) ? $pncdd->t2 : '' ?>">
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_d2" name="system_pncdd_d2" <?php
                                                                    if (isset($pncdd->d2) && $pncdd->d2 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" id="system_pncdd_t3" name="system_pncdd_t3" value="<?php echo isset($pncdd->t3) ? $pncdd->t3 : '' ?>">
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_d3" name="system_pncdd_d3" <?php
                                                                    if (isset($pncdd->d3) && $pncdd->d3 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" id="system_pncdd_t4" name="system_pncdd_t4" value="<?php echo isset($pncdd->t4) ? $pncdd->t4 : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_d4" name="system_pncdd_d4" <?php
                                                                    if (isset($pncdd->d4) && $pncdd->d4 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" id="system_pncdd_t5" name="system_pncdd_t5" value="<?php echo isset($pncdd->t5) ? $pncdd->t5 : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_d5" name="system_pncdd_d5" <?php
                                                                    if (isset($pncdd->d5) && $pncdd->d5 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <input type="text" class="form-control" id="system_pncdd_t6" name="system_pncdd_t6" value="<?php echo isset($pncdd->t6) ? $pncdd->t6 : '' ?>" >
                                                        </div>
                                                        <div class="col-xs-4  text-center">
                                                            <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_d6" name="system_pncdd_d6" <?php
                                                                    if (isset($pncdd->d6) && $pncdd->d6 == 'on') {
                                                                        echo 'checked';
                                                                    }
                                                                    ?> /> </label></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" id="system_pncdd_aeml" name="system_pncdd_aeml" <?php
                                                            if (isset($pncdd->aeml) && $pncdd->aeml == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?> /> Alert when editing if on mailing list 'NONE' </label></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group text-center m-t-md m-b-xs">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group text-center m-t-md m-b-xs">
                                <button type="button" name="savedropdown" id="savedropdown" class="btn btn-primary btn-md">Save</button>
                                <button type="submit" class="btn btn-danger btn-md">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                autoclose: true
            });

            $('#savedropdown').on('click', function () {
                savetoadmin('PopupForm');
            });
        });
    </script>


