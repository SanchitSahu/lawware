<?php $main1=json_decode($firm['main1']);?>

<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                    <span>Security</span>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Security</h5>
                    </div>
                    <form name="securityForm" id="securityForm" method="post">
                        <div class="ibox-content marg-bot10">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Master Password</label>
                                        <input type="password" name="system_masterpass" id="system_masterpass" class="form-control" value="<?php if(isset($firm['masterpass'])){ echo $firm['masterpass']; } ?>" />
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-md">Staff User Blocking</button>
                                    </div>
                                    <div class="form-group">
                                        <label>Backup critical files every(days)</label>
                                        <input type="number" id="system_main_backup" value="<?php echo isset($main1->backup)?$main1->backup:'' ?>" name="system_main_backup" class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>Login Style</label>
                                        <select class="form-control" name="system_loginstyle" id="system_loginstyle" >
                                            <option value="0" <?php if(isset($main1->loginstyle) && $main1->loginstyle=='0'){ echo "selected";}?>>Dropdown/Last initials</option>
                                            <option value="1" <?php if(isset($main1->loginstyle) && $main1->loginstyle=='1'){ echo "selected";}?>>Last initials</option>
                                            <option value="2" <?php if(isset($main1->loginstyle) && $main1->loginstyle=='2'){ echo "selected";}?>>Nothing</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Ristrictions</label>
                                        <select class="form-control">
                                            <option>None</option>
                                            <option>Calendar</option>
                                            <option>Email</option>
                                            <option>Both</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Block IM</label>
                                        <input type="text" name="system_main_blockIM" id="system_main_blockIM" class="form-control" value="<?php echo isset($main1->blockIM)?$main1->blockIM:'' ?>"  />
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-md">Websites</button>
                                        <button type="button" class="btn btn-primary btn-md">Autolog</button>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Password attempts</label>
                                        <input type="number" name="system_passtimes" id="system_passtimes" class="form-control" value="<?php  echo isset($firm['passtimes'])?$firm['passtimes']:''; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label>Timeout / Logoff</label>
                                        <input type="text" name="system_main_logoff" id="system_main_logoff" class="form-control" value="<?php echo isset($main1->logoff)?$main1->logoff:'' ?>" />
                                    </div>
                                    <div class="form-group">
                                        <span><label> <input type="checkbox" class="i-checks"> Perofrm full backup </label></span>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-md">Workstation ID</button>
                                        <button type="button" class="btn btn-primary btn-md">Backup Now</button>
                                    </div>
                                    <div class="form-group">
                                        <span><label> <input type="checkbox" name="system_main_awnml" id="system_main_awnml" class="i-checks" <?php if(isset($main1->awnml) && $main1->awnml=='on'){ echo 'checked'; } ?>> Always use window's name for the login </label></span>
                                        <span><label> <input type="checkbox" name="system_main_vcms" id="system_main_vcms" class="i-checks" <?php if(isset($main1->vcms) && $main1->vcms=='on'){ echo 'checked'; } ?>> Show version conflict message on startup </label></span>
                                        <span><label> <input type="checkbox"  class="i-checks" > Force posting of Email to case activity </label></span>
                                        <span><label> <input type="checkbox" name="system_main_rpcmp" id="system_main_rpcmp" class="i-checks" <?php if(isset($main1->rpcmp) && $main1->rpcmp=='on'){ echo 'checked'; } ?>> Ristrict password changing in My Pref </label></span>
                                        <span><label> <input type="checkbox" class="i-checks"> Laptop Computer </label></span>
                                        <span><label> <input type="checkbox" name="system_main_instance" id="system_main_instance" class="i-checks" <?php if(isset($main1->instance) && $main1->instance=='on'){ echo 'checked'; } ?>> One Instance </label></span>
                                        <span><label> <input type="checkbox" name="system_main_piu" id="system_main_piu" class="i-checks" <?php if(isset($main1->piu) && $main1->piu=='on'){ echo 'checked'; } ?>> Password for Internet Updates </label></span>
                                        <span><label> <input type="checkbox" name="system_main_prompt" id="system_main_prompt" class="i-checks" <?php if(isset($main1->prompt) && $main1->prompt=='on'){ echo 'checked'; } ?>> Prompt to continue for internet updates </label></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="button" id="savesecurity" name="savesecurity" class="btn btn-primary btn-md">Save</button>
                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        </div>
  <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                 autoclose: true
            });
        });

             $('#savesecurity').on('click', function () {
                savetoadmin('securityForm');
            });
    </script>

</body>
</html>