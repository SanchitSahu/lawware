<?php //echo '<pre>'; print_r($checklist); exit; ?>
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Admin</a> /
                    <span>Support Checklist</span>
                </div>
                <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Support Checklist</h5>
            </div>
            <div class="ibox-content">
                <form id="chklistForm" method="post">
                    <div class="row">
                        <div class="col-sm-12">


                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label>Firm Name</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text"  readonly name="frmnm" class="form-control" value="<?php echo $firm['firmname']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3"><label>Installation Checklist</label></div>
                                    <div class="col-sm-9">
                                    <span><label> <input type="checkbox" <?php if(isset($checklist['install_xampp']) && $checklist['install_xampp'] == 1){ echo 'checked';} ?> name="install_xampp" class="i-checks ChackBox"> Install Xampp </label></span><br/>
                                    <span><label> <input type="checkbox" <?php if(isset($checklist['code']) &&$checklist['code'] == 1){ echo 'checked';} ?> name="code" class="i-checks ChackBox"> Code </label></span><br/>
                                    <span><label> <input type="checkbox" <?php if(isset($checklist['database_migration']) &&$checklist['database_migration'] == 1){ echo 'checked';} ?> name="data_migration" class="i-checks ChackBox"> Data Migration </label></span><br/>
                                    <span><label> <input type="checkbox" <?php if(isset($checklist['chat_server']) &&$checklist['chat_server'] == 1){ echo 'checked';} ?> name="chat_server" class="i-checks ChackBox"> Chat-Server (Open-fire) </label></span><br/>
                                    <span><label> <input type="checkbox" <?php if(isset($checklist['created_users']) &&$checklist['created_users'] == 1){ echo 'checked';} ?> name="chat_user" class="i-checks ChackBox"> Created Users in Chat Server </label></span>
                                    </div>
                                </div>
                                 <div class="form-group">
                                        <div class="col-sm-3">
                                        <label>Public Domain</label>
                                         </div>
                                        <div class="col-sm-9">
                                        <input type="text" name="public_domain"  value="<?php if(isset($checklist['public_domain'])) { echo $checklist['public_domain'];} ?>" id="public_domain" class="form-control" />
                                        </div>
                                </div>
                                 <div class="form-group">
                                     <div class="col-sm-3">
                                        <label>IP Address</label>
                                      </div>
                                    <div class="col-sm-9">
                                        <input type="text" name="ip_addr" value="<?php if(isset($checklist['ip_address'])) { echo $checklist['ip_address'];} ?>" id="ip_addr" class="form-control" />
                                    </div>
                                </div>
                            <div class="form-group text-center m-t-md m-b-xs">
                                <button type="button" id="save_chklist" class="btn btn-primary btn-md">Save</button>
                                <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
         </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.date').datepicker({
            forceParse: false,
            autoclose: true
        });

        $('.color').colorpicker({
             autoclose: true
        });
    });

    $('#save_chklist').on('click', function () {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/updatechklist',
            method: 'POST',
            data: $("#chklistForm").serialize(),
            beforeSend: function (xhr) {
                  //$.blockUI();
            },
            complete: function (jqXHR, textStatus) {
                    //$.unblockUI();
            },
            success: function (result) {
                  var obj = $.parseJSON(result);
                   if (obj.status == '1') {
                          swal("Success !", obj.message, "success",function(){                                                          location.reload();
                          });

                    }
            }
        });
   });
</script>

