<!DOCTYPE html>
<html lang="en" class="no-scroll">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><?php echo APPLICATION_NAME; ?> | <?php echo $pageTitle; ?></title>
<style>
</style>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets'); ?>/img/favicon.png">
        <script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>

        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Plugins -->
        <!--<link href="<?php echo base_url('assets'); ?>/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">-->

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
        <!-- sweetalert-->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/custom.css" rel="stylesheet">-->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/chosen/chosen.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/switchery/switchery.css" rel="stylesheet">-->

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/cropper/cropper.min.css" rel="stylesheet">-->

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/switchery/switchery.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">-->

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/select2/select2.min.css" rel="stylesheet">-->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/select2/select2.css" rel="stylesheet">

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote.css" rel="stylesheet">-->

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">-->

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">-->

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/animate.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">

        <!--<link href="<?php echo base_url('assets'); ?>/css/inspinia.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/custom1.css" rel="stylesheet">
    </head>
    <body>
        <?php
        //Temporary code for active LI
        $self_url = $_SERVER['PHP_SELF'];
        $self_url = explode("/", $self_url);
        //print_r( $self_url );
        ?>
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span>
                                <img alt="image" height="42" width="42"  class="img-circle" src="<?php echo base_url('assets'); ?>/img/lawware-logo.jpg" />
                            </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $this->session->userdata('admin_user_data')['sa_user_name'] ?></strong>
                                    </span> <span class="text-muted text-xs block"><?php echo $this->session->userdata('admin_user_data')['sa_user_name'] ?> <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="#">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="logout" class="btn btn-primary">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                        <img alt="image" height="42" width="42"  class="img-circle" src="<?php echo base_url('assets'); ?>/img/lawware-logo.jpg" />
                        </div>
                    </li>
                    <?php $page = $self_url[4]; ?>
                    <li id="collapse-active" <?php echo in_array($page, array("firm", "general", "drives", "newcases", "security", "function", "image", "popup", "caseactivity", "colors", "clientcard", "calendar", "screen", "program", "forms", "injuries", "holidays", "tasksmacro", "rolodexudf", "clientcardudf", "injuryudf", "injuryudfview", "prospects", "reports", "rolodexsecurity", "a1com", "addons", "scan")) ? 'class="active"' : '' ?> >
                        <a href="<?php echo base_url() ?>admin/system"><i class="fa fa-cog"></i>
                            <span class="nav-label">System Defaults</span><span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level out collapse">
                            <li <?php echo ($page == "firm") ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url() ?>admin/firm">Firm Address</a>
                            </li>
                            <li <?php echo ($page == "newcases") ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url() ?>admin/newcases">New Cases</a>
                            </li>
                            <li <?php echo ($page == "popup") ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url() ?>admin/popup">Popups</a>
                            </li>
                           
                            <li <?php echo ($page == "calendar") ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url() ?>admin/calendar">Calendar</a>
                            </li>
                            <li <?php echo ($page == "injuries") ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url() ?>admin/injuries">Injuries</a>
                            </li>
                            <li <?php echo ($page == "holidays") ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url() ?>admin/holidays">Holidays</a>
                            </li>
                            <li <?php echo ($page == "prospects") ? 'class="active"' : ''; ?>>
                                <a href="<?php echo base_url() ?>admin/prospects">Prospects</a>
                            </li>
                            
                        </ul>
                    </li>
                    <li <?php echo ($page == "staff") ? 'class="active"' : ''; ?>>
                        <a href="<?php echo base_url() ?>admin/staff"><i class="fa fa-users"></i> <span class="nav-label">Staff Defaults</span></a>
                    </li>
                     <li <?php echo ($page == "timeclock") ? 'class="active"' : ''; ?>>
                         <a href="<?php echo base_url() ?>admin/timeclock"><i class="fa fa-clock-o"></i> <span class="nav-label">Time Clock</span></a>
                            </li>
                    <li <?php echo ($page == "groupmanagement") ? 'class="active"' : ''; ?>>
                        <a href="<?php echo base_url() ?>admin/groupmanagement"><i class="fa fa-users"></i> <span class="nav-label">Group Management</span></a>
                    </li>
                    <li <?php echo ($page == "global_reassign") ? 'class="active"' : ''; ?>>
                        <a href="<?php echo base_url() ?>admin/global_reassign"><i class="fa fa-globe"></i> <span class="nav-label">Global Reassign</span></a>
                    </li>
                    <!--<li <?php echo ($page == "backup-log") ? 'class="active"' : ''; ?>>
                        <a href="backup-log"><i class="fa fa-window-maximize"></i> <span class="nav-label">View Backup Log</span></a>
                    </li>-->
                    <li <?php echo ($page == "checklist") ? 'class="active"' : ''; ?>>
                        <a href="checklist"><i class="fa fa-list-alt"></i> <span class="nav-label">Support Checklist</span></a>
                    </li>
                    <li <?php echo ($page == "featureRequest") ? 'class="active"' : ''; ?>>
                        <a href="featureRequest"><i class="fa fa-list-alt"></i> <span class="nav-label">Feature Request</span></a>
                    </li>
                    <li <?php echo ($page == "bug_report_list") ? 'class="active"' : ''; ?>>
                        <a href="<?php echo base_url() ?>admin/bug_report_list"><i class="fa fa-list-alt"></i> <span class="nav-label">Bugs Report List</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('/admin/forcelogout');?>"><i class="fa fa-sign-out"></i> <span class="nav-label">Force Users Off</span></a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Welcome to <?php echo APPLICATION_NAME; ?> Admin Panel</span>
                        </li>
                        <li>
                            <a href="logout"  class="btn btn-primary">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <script type="text/javascript">
                var HTTP_PATH = '<?php echo base_url(); ?>';
            </script>