<!-- Mainly scripts -->
<script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>
<!-- block UI -->
<script src="<?php echo base_url('assets'); ?>/js/blockUI.js"></script>
<!-- sweetalert -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/flot/jquery.flot.resize.js"></script>-->

<!-- Peity -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/demo/peity-demo.js"></script>-->

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url('assets'); ?>/js/inspinia.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-ui/jquery-ui.min.js"></script>-->

<!-- GITTER -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/gritter/jquery.gritter.min.js"></script>-->

<!-- ChartJS-->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/chartJs/Chart.min.js"></script>-->

<!-- Toastr -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/toastr/toastr.min.js"></script>

<!-- Jvectormap -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->

<!-- iCheck -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>

<!-- EayPIE -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/easypiechart/jquery.easypiechart.js"></script>-->

<!-- Sparkline -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/sparkline/jquery.sparkline.min.js"></script>-->

<!-- Sparkline demo data  -->
<!--<script src="<?php echo base_url('assets'); ?>/js/demo/sparkline-demo.js"></script>-->

<!-- jqGrid -->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/jqGrid/i18n/grid.locale-en.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jqGrid/jquery.jqGrid.min.js"></script>-->

<script src="<?php echo base_url('assets'); ?>/js/plugins/jeditable/jquery.jeditable.js"></script>

<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/dropzone/dropzone.js"></script>-->

<script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/datatables.min.js"></script>

<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/switchery/switchery.js"></script>-->

<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>-->

<script src="<?php echo base_url('assets'); ?>/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Data picker -->


<script src="<?php echo base_url('assets'); ?>/js/plugins/daterangepicker/daterangepicker.js"></script>

<script src="<?php echo base_url('assets'); ?>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/dataTables/dataTables.select.min.js"></script>
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/cropper/cropper.min.js"></script>-->

<script src="<?php echo base_url('assets'); ?>/js/plugins/fullcalendar/moment.min.js"></script>

<script src="<?php echo base_url('assets'); ?>/js/plugins/summernote/summernote.min.js"></script>

<script src="<?php echo base_url('assets'); ?>/js/plugins/clockpicker/clockpicker.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/codemirror/codemirror.js"></script>-->
<!--<script src="<?php echo base_url('assets'); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>-->
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/additional-methods.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/admin_common_js.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/select2/select2.full.js" rel="stylesheet"></script>

<div class="footer">
    <div>
        <strong><?php echo APPLICATION_NAME; ?></strong> &copy; 2016-<?php echo Date('Y'); ?>
    </div>
</div>
</div>
<script>
    $(document).ready(function () {
        $(".admin-select2").select2({
            placeholder: 'Select'
        });
        <?php if ($this->session->flashdata('message')) { ?>
                    Command: toastr["success"]("<?php echo $this->session->flashdata('message'); ?>")
        <?php } ?>
    });
</script>
<script>
    $(document).ready(function($){
        $("#NewEmailDashboard").DataTable();
        $("#TableRecentTask").DataTable({
            "columnDefs": [{
                "targets": 'nosort',
                "orderable": false
        }]});
        $("#DashboardRecentEvents").DataTable();
    });
</script>
</body>
</html>
