<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
    vertical-align: middle;
    }
</style>



        <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure Group Defaults</a> /
                    <span>Member Profile</span>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit a Staff Member Profile</h5>
                        <a href="#delete" class="btn btn-primary btn-sm pull-right marg-right5" data-toggle="modal">Delete</a>
                        <a href="#move" class="btn btn-primary btn-sm pull-right marg-right5" data-toggle="modal">Move</a>
                        <a href="group_general" class="btn btn-primary btn-sm pull-right marg-right5">Add</a>
                    </div>
                    <div class="ibox-content">
                        <form>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <!--th width="4%"></th-->
                                                    <th>Group Name</th>
                                                    <th width="6%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($groups as $key=>$value)
                                                    {
                                                ?>
                                                <tr>
                                                    <!--td>
                                                        <span><label> <input type="checkbox" class="i-checks" /></label></span>
                                                    </td-->
                                                    <td><?php echo $value->groupname; ?></td>
                                                    <td class="text-center"><a href="<?php echo base_url() ?>admin/edit_general/<?php echo $value->groupkey;  ?>" data-toggle="modal">Edit</a></td>
                                                </tr>
                                                <?php
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                 autoclose: true
            });
        });
    </script>


    <div id="delete" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form role="form" class="text-center">
                        <p>Are you sure to delete this activity</p>
                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary btn-md">Delete</button>
                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                        </div>
                    </form>
                </div> <!--End modal-body-->
            </div>
            <!-- End Modal content-->

        </div>
    </div> <!--End Delete Modal-->
