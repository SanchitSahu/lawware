<?php // print_r($firm);
$main1=json_decode($firm['main1']);
//print_r($main1);
//echo $main1[0]->firm[1];
?>
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Address Information</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" name="FirmForm" id="FirmForm" enctype="multipart/form-data" action="#">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Firm Name</label>
                                        <input type="text" name="system_firmname" id="system_firmname" value="<?php echo $firm['firmname']; ?>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Firm Address</label>
                                        <input type="text" name="system_firmaddr1" id="system_firmaddr1" value="<?php echo $firm['firmaddr1']; ?>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Firm State</label>
                                        <input type="text" name="system_firmstate" id="system_firmstate" value="<?php echo $firm['firmstate']; ?>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>County</label>
                                        <input type="text" name='system_main_country'  id='system_main_country' value="<?php echo isset($main1->country)?$main1->country:'' ?>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Firm Zip</label>
                                        <input type="text" name='system_firmzip' id='system_firmzip' value="<?php echo $firm['firmzip']; ?>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Firm Area Code</label>
                                        <input type="text" name='system_firmareacd' id='system_firmareacd' maxlength="3" value="<?php echo $firm['firmareacd']; ?>" class="form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label>Firm Telephone</label>
                                        <input type="text" name='system_firmphone' id='system_firmphone' value="<?php echo $firm['firmphone']; ?>" class="form-control contact valid" maxlength="14"  />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Firm Fax</label>
                                        <input type="text" name='system_firmfax' id='system_firmfax' value="<?php echo $firm['firmfax']; ?>" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Firm Tax ID</label>
                                        <input type="text" class="form-control" id="system_main_firmTextid" name="system_main_firmTextid" value="<?php echo isset($main1->firmTextid)?$main1->firmTextid:''; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>EAMS Ref No</label>
                                        <input type="text" class="form-control" id="system_main_EAMSRefNo" name="system_main_EAMSRefNo" value="<?php echo isset($main1->EAMSRefNo)?$main1->EAMSRefNo:'' ?>" />
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-md">EAMS Exrta Defaults</button>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="button" id="savefirm" class="btn btn-md btn-primary savefirm">Save</button>

                                        <button type="submit" class="btn btn-danger btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<script>
    $(document).ready(function () {
    function validaeForm(frmName, rulesObj, messageObj, groupObj)
 {
    // validate form
    $('#' + frmName).validate({
        ignore: '',
        onkeyup: false,
        errorClass: "error text-danger",
        validClass: "text-success",
        rules: rulesObj,
        messages: messageObj,
        groups: groupObj,
        invalidHandler: function (form, validator) {
        },
        showErrors: function (errorMap, errorList) {

            // create array of error list string

            var strElement = $.param(errorMap).split("&");



            // check error string is blank or not
            if ($.trim(strElement) != "")
            {
                // get id of first element
                ///following one line is added by keyur
                strElement[0] = unescape(strElement[0]);

                var arrName = strElement[0].split("=");

                // get element id

                var eleId = $('[name="' + arrName[0] + '"]').attr("id");

                // find tab div id of form element
                if (eleId == undefined)
                {///this condition is added by keyur
                    var parentId = $('[name="' + arrName[0] + '"]').parents('div[id]').parents('div[id]').attr("id");
                } else
                    var parentId = $("#" + eleId).parents('div[id]').attr("id");


                // active tab
                $('a[href|="#' + parentId + '"]').tab("show");

            }

            // show default error message
            this.defaultShowErrors();
        }
    });
}
        var rules = {
            system_firmstate: {required: true},
            system_firmareacd:{digits:true,min:3}
            //firmphone: {phoneUSA: true},
            /*location: {required: true},
            zipcode: {required: true},
            account: {required: true},
            phone: {required: true},
            contact_perosn: {required: true},*/
           // pwd: {required: true, whitespaceValue: true,minlength:5 /*passwordValue: true*/},
          //  pwd1:{required: true, whitespaceValue: true,minlength:5 /*passwordValue: true*/},
        }
        var messages = {
            system_firmstate: {
                required: "Please enter Firm State"
            },
            system_firmareacd:
            {     digits:"Please enter only digits",
                   max:"Please enter maximum 3 digits"
            }
//            firmphone: {
//                phoneUSA: "Please enter address"
//            },
           /* location: {
                required: "Please enter location"
            },
            zipcode:{
                required: "Please enter zipcode"
            },

            account: {
                required: "Please select nature of business"
            },
            phone:{
                required: "Please enter phone"
            },
            contact_perosn:{
              required: "Please enter contact person name"
            },



          /*  pwd: {
                required: "Please enter password",
                minlength: "Please enter minimum 5 character password",
                whitespaceValue: "White Space is not allowed for password",
                //passwordValue: "Password must have at least one uppercase letter, one digits and one special character"
            }*/
        }


         validaeForm('FirmForm', rules, messages);
    });
    $('#savefirm').on('click', function () {
         savetoadmin('FirmForm');
     //alert($("#FirmForm").serialize());//<?php echo base_url(); ?>admin/updatefirm
//        if($("#FirmForm").valid()){
//                    $.ajax({
//                        url: '<?php echo base_url(); ?>admin/updatefirm',
//                        method: 'POST',
//                        data: $("#FirmForm").serialize(),
//                    beforeSend: function (xhr) {
//                          $.blockUI();
//                    },
//                    complete: function (jqXHR, textStatus) {
//                            $.unblockUI();
//                    },
//                    success: function (result) {
//                          var obj = $.parseJSON(result);
//                           if (obj.status == '1') {
//                                swal("Success !", obj.message, "success");
//                                location.reload();
//                            }
//                    }
//                    });
//        }
//

    });
</script>

