<style>
        a.btn.btn-default.buttons-excel.buttons-html5{margin-right: 5px;
    border-radius: 5px !important;float: right;}
    
    a.btn.btn-default.buttons-pdf.buttons-html5{margin-right: 5px;
    border-radius: 5px !important;float: right;}
    a.btn.btn-default.buttons-print{border-radius: 5px !important;float: right ;}
    .dt-buttons.btn-group{float: right;width: 100%;margin-bottom: 20px;}

</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <span>Timestamp Information</span>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Timestamp Information</h5>
                  </div>
                <div class="ibox-content">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover loginfo">
                                        <thead>
                                            <tr>
                                                <!--<th width="4%"></th>-->
<!--                                                <th width="10%">Location</th>-->
                                                <th width="50%">Initials</th>
                                                <th width="10%">Sign On</th>
                                                <th width="10%">Sign Off</th>

                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
    var loginfoTableData ='';

        $.loginfoTableData = function () {
            loginfoTableData = $('.loginfo').DataTable({
                    dom : "B<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'<'#colvis'>p>>",
                    buttons: [
                        {extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i>  Print',
                            
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                            }
                        },
                        {
                            extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> PDF', title: '<?php echo APPLICATION_NAME; ?> : Time stamp Information', header: true, 
                    

                    customize: function (doc) {
                                
                                doc.styles.table = {
                                    width: '100%'
                                };
                                
                                doc.styles.tableHeader.alignment = 'left';
                                
                                doc['header'] = function (page, pages) {
                                    return {
                                        columns: [
                                            '<?php echo APPLICATION_NAME; ?> ',
                                            {
                                                alignment: 'left',
                                                text: ['page ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                            }
                                        ],
                                        margin: [10, 0]
                                    };
                                };
                                doc.content[1].table.widths = '*';
                            }
                        },
                        {extend: 'excel', text: '<i class="fa fa-file-excel-o"></i>  Excel', title: '<?php echo APPLICATION_NAME; ?> : Time stamp Information',
                           
                        }

                ],
                processing : true,
                serverSide : true,
                scrollX : true,
                bFilter : true,
                destroy: true,
                pageLength : 10,
                order: [[1, "desc"]],
                lengthMenu : [10, 20, 50, 100],
                ajax: {
                    url: "<?php echo base_url(); ?>admin/timeclockdata",
                    type: "POST"
                },
                columnDefs: [{
                        "width": "20%",
                        "targets": 0
                    },
                    {
                        "width": "20%",
                        "targets": 1
                    },
                    {
                        "width": "30%",
                        "targets": 2
                    },
                    /*{
                        "width": "30%",
                        "targets": 3
                    }*/
                ]
            });
        };
        $.loginfoTableData();

        $('#staffTable').dataTable();
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.date').datepicker({
            forceParse: false,
            autoclose: true
        });

        $('.color').colorpicker({
            autoclose: true
        });

        $('.addstaff').click(function () {
            $('#addStaff')[0].reset();
        });

        $('.close-editstaff').click(function () {
            $('#editStaff')[0].reset();
        });
    });


    $('#staffTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('table-selected')) {
            $(this).removeClass('table-selected');
        } else {
            $('#staffTable tr').removeClass('table-selected');
            $(this).addClass('table-selected');
        }
    });

    function setstaffIni(stini) {
        $("#staffini").val(stini);
    }

    function getstaffdetails(ini) {
        //var ini = $("#staffini").val();

        if (ini != '') {
            $("#mlabel").text(ini);
            $("#edit").modal("show");
            $("#tab-6").trigger("click");
            $("#tab-9").trigger("click");
            $.ajax({
                url: '<?php echo base_url(); ?>admin/getAjaxStaffDetails',
                method: 'POST',
                dataType: 'json',
                data: {
                    initials: ini,
                },
                beforeSend: function (xhr) {
                    //$.blockUI();
                },
                complete: function (jqXHR, textStatus) {
                    //$.unblockUI();
                },
                success: function (data) {
                    if (data == false) {
                        swal({
                            title: "Alert",
                            text: "Oops! Some error has been occured while fetching details. Please try again later.",
                            type: "warning"
                        });
                    } else {
                        if (data.main1)
                        {
                            var x = data.main1;
                            if (x.charAt(0) == '{') {
                                var main1 = $.parseJSON(data.main1);
                                $('#estaff-main-mname').val(main1.mname);
                                $('#estaff-main-barno').val(main1.barno);
                                $('#estaff-main-privateph').val(main1.privateph);
                                $('#estaff-main-privatefx').val(main1.privatefx);
                                $('#estaff-main-alias').val(main1.alias);
                                $('#estaff-main-email').val(main1.email);
                            }
                        }

                        //console.log(main1);
                        $('#staffinis').val(data.initials);
                        $('#estaff-initials').val(data.initials);
                        $('#estaff-username').val(data.username);
                        $('#estaff-password').val(data.password);
                        $('#estaff-fname').val(data.fname);
                        $('#estaff-lname').val(data.lname);
                        //$('select[name=estaff-function1]').val(data.function1);
                        $('select[name=estaff-sex]').val(data.sex);
                        $('#estaff-title').val(data.title);
                        $('#estaff-specialty').val(data.specialty);
                        $('#estaff-atty_resp').val(data.atty_resp);
                        $('#estaff-atty_hand').val(data.atty_hand);
                        $('#estaff-para_hand').val(data.para_hand);
                        $('#estaff-sec_hand').val(data.sec_hand);
                        $('#estaff-comport').val(data.comport);
                        $('#estaff-phoneareac').val(data.phoneareac);
                        $('#estaff-phoneoutsi').val(data.phoneoutsi);
                        $('#estaff-phoneprefx').val(data.phoneprefx);
                        $('#estaff-hourlyrate').val(data.hourlyrate);
                        if (data.vacation)
                            var text = data.vacation.replace(/\##/g, "");
                        else
                            var text = '';
                        // var text = text.replace("##");
                        // console.log(data.vacation,text);
                        $('#estaff-vacation').val(text);
                        //$('#estaff-vacation').val(data.vacation);
                        //alert(data.initials);
                    }
                }
            });
        } else {
            swal({
                title: "Alert",
                text: "Please select staff member to edit.",
                type: "warning"
            });
        }
    }


    function deleteStaff(ini) {
        // var ini = $("#staffini").val();
        //alert(ini);
        if (ini != '') {
            $("#memberini").text(ini);
            $("#delete").modal("show");
        } else {
            swal({
                title: "Alert",
                text: "Please select staff member to delete.",
                type: "warning"
            });
        }
    }

    function changeStatus(ini, status) {
        if (status == 1) {
            var txt = "Are you sure you want to Active?";
        } else {
            var txt = 'Are you sure you want to Inactive?';
        }
        swal({
            title: txt,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/changeStatus',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        initials: ini,
                        status: status
                    },
                    beforeSend: function (xhr) {
                        //$.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        //$.unblockUI();
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            swal({
                                title: "Alert",
                                text: data.message,
                                type: "success"
                            }, function () {
                                location.reload();
                            });
                        } else {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has been occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        }
                    }
                });
            }
        });
    }

    function changeCaptionAccess(ini, status) {
        if (status == 1) {
            var txt = 'Are you sure you want to allow access?';
        } else {
            var txt = "Are you sure you want to remove access?";
        }
        swal({
            title: txt,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/changeCaptionAccess',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        initials: ini,
                        status: status
                    },
                    beforeSend: function (xhr) {
                        //$.blockUI();
                    },
                    complete: function (jqXHR, textStatus) {
                        //$.unblockUI();
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            swal({
                                title: "Alert",
                                text: data.message,
                                type: "success"
                            }, function () {
                                location.reload();
                            });
                        } else {
                            swal({
                                title: "Alert",
                                text: "Oops! Some error has been occured while fetching details. Please try again later.",
                                type: "warning"
                            });
                        }
                    }
                });
            }
        });
    }
</script>

<div id="add" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Configure Staff Defaults</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="addStaff" name="addStaff">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1">User Information</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-5">Details</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">General Information</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3">Vacation Dates</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Initials</label>
                                                        <input type="text" name="staff-initials" id="staff-initials" class="form-control" />
                                                        <span id="errorIni" style="color: #CD1818"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <input type="text" name="staff-username" id="staff-username" class="form-control" />
                                                        <span id="erroruname" style="color: #CD1818"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input type="password" name="staff-password" id="staff-password" class="form-control" />
                                                        <span id="errorpass" style="color: #CD1818"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>First Name</label>
                                                        <input type="text" name="staff-fname" id="staff-fname" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Middle</label>
                                                        <input type="text" name="staff-main-mname" id="staff-main-mname" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input type="text" id="staff-lname" name="staff-lname" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <!--div class="form-group">
                                                        <label>Group</label>
                                                        <select name="staff-function1" id="staff-function1" class="form-control">
                                                            <option value="">Select Group</option>
                                                    <?php
                                                    if (isset($group) && !empty($group)) {
                                                        $i = 0;
                                                        foreach ($group as $key => $value) {
                                                            ?>
                                                                                                                                                                                                                        <option value="<?php echo $i; ?>"><?php echo $value->groupname; ?></option>
                                                            <?php
                                                            $i++;
                                                        }
                                                    }
                                                    ?>
                                                        </select>
                                                    </div-->
                                                    <div class="form-group">
                                                        <label>Gender</label>
                                                        <select name="staff-sex" id="staff-sex" class="form-control">
                                                            <option value="0">Select Gender</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <select name="staff-title" id="staff-title" class="form-control">
                                                            <option value="">Select Title</option>
                                                            <?php
                                                            $jsonstaff_type = json_decode(staff_title);
                                                            foreach ($jsonstaff_type as $key => $option) {
                                                                echo '<option value=' . $key . '>' . $option . '</option>';
                                                            }
                                                            ?>


                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Bar No</label>
                                                        <input type="text" name="staff-main-barno" id="staff-main-barno" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-5" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Private Phone</label>
                                                        <input type="text" name="staff-main-privateph" placeholder="(xxx) xxx-xxxx" minlength="14" id="staff-main-privateph" class="form-control staff-main-privateph" />
                                                        <span id="errorprivatephone" style="color: #CD1818"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Private Fax</label>
                                                        <input type="text" name="staff-main-privatefx" placeholder="(xxx) xxx-xxxx" minlength="14" id="staff-main-privatefx" class="form-control staff-main-privatefx" />
                                                        <span id="errorprivatefax" style="color: #CD1818"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alias Initials</label>
                                                        <input type="text" name="staff-main-alias" id="staff-main-alias" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="email" name="staff-main-email" id="staff-main-email" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Specialty</label>
                                                        <textarea name="staff-specialty" id="staff-specialty" class="form-control" style="width: 100%;height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                                <!--div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15">
                                                        <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                        <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                    </div>
                                                </div-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>New Case Defaults</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Attorney Responsible</label>
                                                        <input type="text" name="staff-atty_resp" id="staff-atty_resp" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Attorney Handling</label>
                                                        <input type="text" name="staff-atty_hand" id="staff-atty_hand" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Paralegal Handling</label>
                                                        <input type="text" name="staff-para_hand" id="staff-para_hand" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Secretary Handling</label>
                                                        <input type="text" name="staff-sec_hand" id="staff-sec_hand" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Modem Settings</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Com Port</label>
                                                        <input type="number" name="staff-comport" id="staff-comport" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Area Code</label>
                                                        <input type="text" name="staff-phoneareac" id="staff-phoneareac" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Outside line</label>
                                                        <input type="text" name="staff-phoneoutsi" id="staff-phoneoutsi" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Long Dist Prefix</label>
                                                        <input type="text" name="staff-phoneprefx" id="staff-phoneprefx" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hourly Rate</label>
                                                        <input type="number" name="staff-hourlyrate" id="staff-hourlyrate" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--div class="col-sm-12">
                                            <div class="form-group text-center marg-top15">
                                                <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                            </div>
                                        </div-->
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Vacation Dates</label>
                                                        <textarea class="form-control" name="staff-vacation" id="staff-vacation" style="width: 100%;height: 200px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group text-center marg-top15">
                                    <button type="button" name="savestaff" id="savestaff" class="btn btn-md btn-primary">Save</button>
                                    <button type="button" class="btn btn-md btn-primary btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div> <!--End modal-body-->
        </div>
    </div>
</div> <!--End Add Modal-->

<div id="edit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-editstaff" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Configure Staff Defaults For <span id="mlabel"></span></h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" id="editStaff" name="editStaff">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-6">User Information</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-10">Details</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-7">General Information</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-8">Vacation Dates</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-6" class="tab-pane active">
                                <div id="tab-9" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Initials</label>
                                                            <input type="hidden" name="staffinis" id="staffinis" class="form-control" />
                                                            <input type="text" name="estaff-initials" id="estaff-initials" class="form-control" />
                                                            <span id="ederrorIni" style="color: #CD1818"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Username</label>
                                                            <input type="text" name="estaff-username" id="estaff-username" class="form-control" />
                                                            <span id="ederroruname" style="color: #CD1818"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input type="password" name="estaff-password" id="estaff-password" class="form-control" />
                                                            <span id="ederrorpass" style="color: #CD1818"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>First Name</label>
                                                            <input type="text" name="estaff-fname" id="estaff-fname" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Middle</label>
                                                            <input type="text" name="estaff-main-mname" id="estaff-main-mname" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" name="estaff-lname" id="estaff-lname" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <!--div class="form-group">
                                                            <label>Group</label>
                                                            <select class="form-control" name="estaff-function1" id="estaff-function1">
                                                                <option value="">Select Group</option>
                                                        <?php
                                                        /*
                                                          if (isset($group) && !empty($group)) {
                                                          $i = 0;
                                                          foreach ($group as $key => $value) {
                                                          ?>
                                                          <option value="<?php echo $i ?>"><?php echo $value->groupname ?></option>
                                                          <?php
                                                          $i++;
                                                          }
                                                          } */
                                                        ?>
                                                            </select>
                                                        </div-->
                                                        <div class="form-group">
                                                            <label>Gender</label>
                                                            <select name="estaff-sex" id="estaff-sex" class="form-control">
                                                                <option value="0"></option>
                                                                <option value="1">Male</option>
                                                                <option value="2">Female</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Title</label>
                                                            <select name="estaff-title" id="estaff-title" class="form-control">
                                                                <option value="">Select Title</option>
                                                                <?php
                                                                $jsonstaff_type = json_decode(staff_title);
                                                                foreach ($jsonstaff_type as $key => $option) {
                                                                    echo '<option value=' . $key . '>' . $option . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Bar No</label>
                                                            <input type="text" name="estaff-main-barno" id="estaff-main-barno" class="form-control" />
                                                        </div>
                                                    </div>
                                                    <!--div class="col-sm-12">
                                                        <div class="form-group text-center marg-top15">
                                                            <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                            <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                        </div>
                                                    </div-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!--tab-4 End-->
                            </div>
                            <div id="tab-10" class="tab-pane">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Private Phone</label>
                                                        <input type="text" name="estaff-main-privateph" minlength="14" placeholder="(xxx) xxx-xxxx" id="estaff-main-privateph" class="form-control staff-main-privateph" />
                                                        <span id="errorprivatephoneedit" style="color: #CD1818"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Private Fax</label>
                                                        <input type="text" name="estaff-main-privatefx" placeholder="(xxx) xxx-xxxx" id="estaff-main-privatefx" class="form-control staff-main-privatefx" />
                                                        <span id="errorprivatefaxedit" style="color: #CD1818"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alias Initials</label>
                                                        <input type="text" name="estaff-main-alias" id="estaff-main-alias" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="email" name="estaff-main-email" id="estaff-main-email" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Specialty</label>
                                                        <textarea name="estaff-specialty" id="estaff-specialty" class="form-control" style="width: 100%;height: 100px;"></textarea>
                                                    </div>
                                                </div>
                                                <!--div class="col-sm-12">
                                                    <div class="form-group text-center marg-top15">
                                                        <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                        <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                                    </div>
                                                </div-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-7" class="tab-pane">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>New Case Defaults</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Attorney Responsible</label>
                                                        <input type="text" name="estaff-atty_resp" id="estaff-atty_resp" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Attorney Handling</label>
                                                        <input type="text" name="estaff-atty_hand" id="estaff-atty_hand" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Paralegal Handling</label>
                                                        <input type="text" name="estaff-para_hand" id="estaff-para_hand" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Secretary Handling</label>
                                                        <input type="text" name="estaff-sec_hand" id="estaff-sec_hand" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Modem Settings</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="form-group">
                                                        <label>Com Port</label>
                                                        <input type="number" name="estaff-comport" id="estaff-comport" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Area Code</label>
                                                        <input type="text" name="estaff-phoneareac" id="estaff-phoneareac" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Outside line</label>
                                                        <input type="text" name="estaff-phoneoutsi" id="estaff-phoneoutsi" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Long Dist Prefix</label>
                                                        <input type="text" name="estaff-phoneprefx" id="estaff-phoneprefx" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Hourly Rate</label>
                                                        <input type="number" name="estaff-hourlyrate" id="estaff-hourlyrate" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--div class="col-sm-12">
                                            <div class="form-group text-center marg-top15">
                                                <button type="button" class="btn btn-md btn-primary" id="savename">Save</button>
                                                <button type="button" class="btn btn-md btn-primary">Cancel</button>
                                            </div>
                                        </div-->
                                    </div>
                                </div>
                            </div>
                            <div id="tab-8" class="tab-pane ">
                                <div class="panel-body">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Vacation Dates</label>
                                                        <textarea class="form-control" name="estaff-vacation" id="estaff-vacation" style="width: 100%;height: 200px;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group text-center marg-top15">
                                    <button type="button" name="saveeditstaff" id="saveeditstaff" class="btn btn-md btn-primary">Save</button>
                                    <button type="button" id="editcancel" class="btn btn-md btn-primary btn-danger close-editstaff">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div> <!--End modal-body-->
        </div>
        <!-- End Modal content-->
    </div>
</div> <!--End Edit Modal-->

<div id="delete" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form role="form" method="post" id="deleteForm" name="deleteForm" class="text-center">
                    <p>Are you sure you want to delete <span id="memberini"></span></p>
                    <div class="form-group ">
                        <button type="button" id="delMember" name="delMember" class="btn btn-primary btn-md">Delete</button>
                        <button type="button" id="delCancel" name="delCancel" class="btn btn-primary btn-md btn-danger">Cancel</button>
                    </div>
                </form>
            </div> <!--End modal-body-->
        </div>
        <!-- End Modal content-->
    </div>
</div> <!--End Delete Modal-->

<script src="<?php echo base_url('assets'); ?>/js/jquery.mask.min.js"></script>
<script>
    $(document).ready(function($){
        $('.staff-main-privateph').mask("(000) 000-0000");
        $('.staff-main-privatefx').mask("(000) 000-0000");
    });

    $('#savestaff').on('click', function () {
        var valid = true, message = '';

        if ($('#staff-initials').val() == '') {
            $('#errorIni').text('Initials Required');
            valid = false;
        }

        if ($('#staff-username').val() == '') {
            $('#erroruname').text('Username Required');
            valid = false;
        }

        if ($('#staff-password').val() == '') {
            $('#errorpass').text('Password Required');
            valid = false;
        }
        if ($('#staff-main-privateph').val() != '') {
            if($('#staff-main-privateph').val().length < 14) {
                $('#errorprivatephone').text('Enter a valid Private Phone');
                valid = false;
            } else {
                $('#errorprivatephone').text('');
            }
        }
        if ($('#staff-main-privatefx').val() != '') {
            if($('#staff-main-privatefx').val().length < 14) {
                $('#errorprivatefax').text('Enter a valid Private Fax');
                valid = false;
            } else {
                $('#errorprivatefax').text('');
            }
        }

        if (valid) {
            addstaff('addStaff');
        }
    });

    $('#saveeditstaff').on('click', function () {
        var valid = true, message = '';

        if ($('#estaff-initials').val() == '') {
            $('#ederrorIni').text('Initials Required');
            valid = false;
        }

        if ($('#estaff-username').val() == '') {
            $('#ederroruname').text('Username Required');
            valid = false;
        }

        if ($('#estaff-password').val() == '') {
            $('#ederrorpass').text('Password Required');
            valid = false;
        }
        if ($('#estaff-main-privateph').val() != '') {
            if($('#estaff-main-privateph').val().length < 14) {
                $('#errorprivatephoneedit').text('Enter a valid Private Phone');
                valid = false;
            } else {
                $('#errorprivatephoneedit').text('');
            }
        }
        if ($('#estaff-main-privatefx').val() != '') {
            if($('#estaff-main-privatefx').val().length < 14) {
                $('#errorprivatefaxedit').text('Enter a valid Private Fax');
                valid = false;
            } else {
                $('#errorprivatefaxedit').text('');
            }
        }

        if (valid) {
            editstaff('editStaff');
        }
    });

    $("#editcancel").on('click', function () {
        $('#edit').modal('hide');
    });

    $("#delCancel").on('click', function () {
        $('#delete').modal('hide');
    });

    $('#edit').on('hidden.bs.modal', function (e) {
        $(this).find("input,textarea,select").val('').end().find("input[type=checkbox], input[type=radio]").prop("checked", "").end();
    })

    $("#delMember").on('click', function () {
        var ini = $("#staffini").val();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/deleteAjaxStaff',
            method: 'POST',
            dataType: 'json',
            data: {
                initials: ini,
            },
            success: function (result) {
                console.log(result);
                //var obj = $.parseJSON(result);
                var obj = result;
                console.log(obj);
                if (obj.status == '0') {
                    swal({title: "Warning!", text: obj.message, type: "warning"},
                            function () {
                                $("#delete").modal("hide");
                            }
                    );
                } else {
                    swal({
                        title: "Success!",
                        text: obj.message,
                        type: "success"
                    }, function () {
                        $("#delete").modal("hide");
                        location.reload();
                    });
                }
            }
        });
    });

</script>