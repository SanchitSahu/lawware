<?php $main1=json_decode($firm['main1']);?>
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                    <span>System Colors</span>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Colors</h5>
                    </div>
                    <div class="ibox-content">
                        <form name="colorsForm" id="colorsForm" method="post">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Number of colors</label>
                                        <input type="number" name="system_ncolors" class="form-control" value="<?php if(isset($firm['ncolors'])){ echo $firm['ncolors']; } ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Categories</label>
                                        <textarea class="form-control" name="system_ccolors" style="width: 100%;height: 150px;"><?php if(isset($firm['ccolors'])){ echo $firm['ccolors']; } ?></textarea>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Override default client card background color</label>
                                        <textarea class="form-control" name="system_main_odccbc" style="width: 100%;height: 135px;"><?php echo isset($main1->odccbc)?$main1->odccbc:'' ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <span><label> <input type="checkbox" name="system_main_ccofcc" class="i-checks" <?php if(isset($main1->ccofcc) && $main1->ccofcc=='on'){ echo 'checked'; } ?> /> Change color only at the far right of the client card </label></span>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-sm">Browse</button>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="button" id="savecolor" name="savecolor" class="btn btn-primary btn-md">Save</button>
                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>





    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                 autoclose: true
            });
        });

        $('#savecolor').on('click', function () {
                savetoadmin('colorsForm');
         });
    </script>

</body>
</html>