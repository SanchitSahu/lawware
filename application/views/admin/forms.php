
<div class="wrapper wrapper-content">
<div class="row">
    <div class="col-lg-12">
        <div class="breadcrumbs">
            <a>Configure System Defaults</a> /
            <span>Forms</span>
        </div>
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1">Forms</a></li>
                <li class=""><a data-toggle="tab" href="#tab-2">Word</a></li>
                <li class=""><a data-toggle="tab" href="#tab-3">Corel</a></li>
                <li class=""><a data-toggle="tab" href="#tab-4">Miscellaneous</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content marg-bot15">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label>Margin - top, bottom, left, right</label>
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-3">
                                                    <input type="number" class="form-control">
                                                </div>
                                                <div class="col-xs-6 col-sm-3">
                                                    <input type="number" class="form-control">
                                                </div>
                                                <div class="col-xs-6 col-sm-3">
                                                    <input type="number" class="form-control">
                                                </div>
                                                <div class="col-xs-6 col-sm-3">
                                                    <input type="number" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Page - width, height</label>
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6">
                                                    <input type="number" class="form-control">
                                                </div>
                                                <div class="col-xs-6 col-sm-6">
                                                    <input type="number" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Font Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Font Size</label>
                                            <input type="number" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Range for print filter</label>
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6">
                                                    <input type="number" class="form-control">
                                                </div>
                                                <div class="col-xs-6 col-sm-6">
                                                    <input type="number" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <span><label> <input type="checkbox" class="i-checks" /> Notify after printing </label></span>
                                            <span><label> <input type="checkbox" class="i-checks" /> Never overwrite form templates 10001-10004, 10851  </label></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group text-center m-t-md m-b-xs">
                                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h1>Reserved for future use</h1>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group text-center m-t-md m-b-xs">
                                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-3" class="tab-pane">
                    <div class="panel-body">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label>WP fast mode (style 3)</label>
                                            <select class="form-control">
                                                <option>Slow way</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <span><label> <input type="checkbox" class="i-checks" /> WP dubbeging on </label></span>
                                            <span><label> <input type="checkbox" class="i-checks" /> Run the corel1 macro instead of automation </label></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Always load Corel before running the Corel1 macro. Path, filename </label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Corel class ID</label>
                                            <input type="text" class="form-control">
                                        </div>

                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group text-center m-t-md m-b-xs">
                                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-4" class="tab-pane">
                    <div class="panel-body">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content marg-bot15">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label>Method to open documents</label>
                                            <select class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Spell check</label>
                                            <select class="form-control">
                                                <option>Law small dictionary</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Case activity update</label>
                                            <select class="form-control">
                                                <option>Use new date</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Compile FormPrg</label>
                                            <button type="button" class="btn btn-primary brn-sm marg-left10">Compile</button>
                                        </div>
                                        <div class="form-group">
                                            <span><label> <input type="checkbox" class="i-checks"> Open form letters maximized </label></span>
                                            <span><label> <input type="checkbox" class="i-checks"> 640*480 compatibility mode </label></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Exclude categories</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group text-center m-t-md m-b-xs">
                                            <button type="submit" class="btn btn-primary btn-md">Save</button>
                                            <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

