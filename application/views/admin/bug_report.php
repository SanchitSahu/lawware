<div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Report Bug</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url();?>admin/send_bug_report" enctype='multipart/form-data'>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Summary</label>
                                            <div class="col-sm-7">
                                                <input type="text" name="summary" id="summary" placeholder="E.g : Having issue in sending email" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Descriptions</label>
                                            <div class="col-sm-7">
                                                <textarea id="bug_desciptions" name="bug_desciptions" class="form-control" rows="10"><ul>
                                                <li>Lorem Ipsum Dolor Sit Amet</li>
                                                <li>Lorem Ipsum Dolor Sit Amet</li>
                                                <li>Lorem Ipsum Dolor Sit Amet</li>
                                                <li>Lorem Ipsum Dolor Sit Amet</li>
                                            </ul></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Reporter</label>
                                            <div class="col-sm-7">
                                                <input type="text" id="repoter" name="repoter" class="form-control" value="<?php echo $firm_name->firmname.' Administrator';?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Module</label>
                                            <div class="col-sm-7">
                                                <!--  <input id="module" name="module" type="text" class="form-control" value=""> -->
                                                 <select id="module" name="module" class="form-control">
                                                     <option value="Dashboard">Dashboard</option>
                                                     <option value="Case">Case</option>
                                                     <option value="Calender">Calender</option>
                                                     <option value="Email">Email</option>
                                                     <option value="Rolodex">Rolodex</option>
                                                     <option value="Tasks">Tasks</option>
                                                     <option value="Reminder">Reminder</option>
                                                     <option value="Reports">Reports</option>
                                                     <option value="PNC">Potential New Client</option>
                                                     <option value="Chat">Chat</option>
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Sub-Module</label>
                                            <div class="col-sm-7">
                                                <!--  <input id="sub_module" name="sub_module" type="text" class="form-control" value=""> -->
                                                 <select id="sub_module" name="sub_module" class="form-control">
                                                    <optgroup label="Dashboard">
                                                        <option value="Quick Links">Quick Links</option>
                                                        <option value="Email">Email section</option>
                                                        <option value="Tasks assigned">Tasks assigned</option>
                                                        <option value="Today's Events">Today's Events</option>
                                                    </optgroup>

                                                    <optgroup label="Cases">
                                                        <option value="Search Case">Search Case</option>
                                                        <option value="Parties">Parties</option>
                                                        <option value="Injuries">Injuries</option>
                                                        <option value="Photos & Videos">Photos &amp; Videos</option>
                                                        <option value="Document">Document</option>
                                                        <option value="Calendar">Calendar</option>
                                                        <option value="Related Cases">Related Cases</option>
                                                        <option value="Tools">Tools</option>
                                                        <option value="Tasks">Tasks</option>
                                                        <option value="Related Email">Related Email</option>
                                                    </optgroup>
                                                    <optgroup label="Calender">
                                                        <option value="Add/Edit">Add/Edit</option>
                                                        <option value="Layout">Layout</option>
                                                    <optgroup label="Email">
                                                        <option value="Internal/External Emails">Internal/External Emails</option>
                                                        <option value="Folder section">Folder section</option>
                                                        <option value="Category Section">Category Section</option>
                                                        <option value="Search">Search</option>
                                                    </optgroup>
                                                    <optgroup label="Rolodex">
                                                        <option value="Search">Search</option>
                                                        <option value="Add/Edit">Add/Edit</option>
                                                        <option value="Tab Name">Tab Name</option>
                                                        <option value="Telephone/Email">Tab Telephone/Email</option>
                                                        <option value="Tab Details">Tab Details</option>
                                                        <option value="Tab Comments">Tab Comments</option>
                                                        <option value="Tab Addresses">Tab Addresses</option>
                                                        <option value="Tab Profile">Tab Profile</option>
                                                    </optgroup>
                                                    <optgroup label="Tasks">
                                                            <option value="Add/Edit">Add/Edit</option>
                                                            <option value="Reminder">Reminder</option>
                                                            <option value="Print">Print</option>
                                                            <option value="Task For Me">Task For Me</option>
                                                            <option value="Task From Me">Task From Me</option>
                                                    </optgroup>
                                                    <optgroup label="Reminder">
                                                        <option value="Reminder Login ">Reminder Login</option>
                                                        <option value="Add/Edit">Add/Edit</option>
                                                        <option value="Status Of Reminder">Status Of Reminder</option>
                                                        <option value="Contacts">Contacts</option>
                                                        <option value="Logs">Logs</option>
                                                        <option value="Settings Appointment page">Settings Appointment page</option>
                                                        <option value="Settings Email/Call/SMS Setting">Settings Email/Call/SMS Setting</option>
                                                        <option value="Settings Other">Settings Other</option>
                                                        <option value="Reports">Reports</option>
                                                    </optgroup>
                                                    <optgroup label="Reports">
                                                           <option value="Case Count">Case Count</option>
                                                           <option value="Case Report">Case Report</option>
                                                           <option value="Referral Report">Referral Report</option>
                                                           <option value="Case Activity Report">Case Activity Report</option>
                                                           <option value="Case Injury Report">Case Injury Report</option>
                                                           <option value="Task Report">Task Report</option>
                                                           <option value="Markup Report">Markup Report</option>
                                                           <option value="Markup Report with User Defined Field">Markup Report with User Defined Field</option>
                                                    </optgroup>
                                                    <optgroup label="Potential New Client">
                                                        <option value="Add/Edit">Add/Edit</option>
                                                        <option value="Add / Accept This Case">Add / Accept This Case</option>
                                                        <option value="Add a Task">Add a Task</option>
                                                        <option value="Clone this PNC">Clone this PNC</option>
                                                        <option value="Print">Print</option>
                                                        <option value="Print Envelope">Print Envelope</option>
                                                        <option value="Task For PNC">Task For PNC</option>
                                                    </optgroup>
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Image</label>
                                            <div class="col-sm-7">
                                                    <input type="file" name="file_name" id="file_name" class="form-control" >                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" align="center">
                                        <div class="col-sm-7 col-sm-offset-2">
                                            <input name="submit" class="btn btn-primary btn-right" value="Submit" type="submit">
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  