<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                    <span>Function Keys</span>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Shift Function Keys Abbreviations</h5>
                    </div>
                    <div class="ibox-content">
                        <form>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <div class="i-checks" data-id="text1" onclick="return display_text('text1');"><label> <input type="radio" value="option1" name="a"  checked="checked"> Shift - F1 </label></div>
                                        <div class="i-checks" data-id="text2"><label> <input type="radio" value="option2" name="a" > Shift - F2 </label></div>
                                        <div class="i-checks" data-id="text3"><label> <input type="radio" value="option3" name="a" > Shift - F3 </label></div>
                                        <div class="i-checks" data-id="text4"><label> <input type="radio" value="option4" name="a" > Shift - F4 </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="option5" name="a" onClick="open_textarea('text5')"> Shift - F5 </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="option6" name="a" onClick="open_textarea('text6')"> Shift - F6 </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="option7" name="a" onClick="open_textarea('text7')"> Shift - F7 </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="option8" name="a" onClick="open_textarea('text8')"> Shift - F8 </label></div>
                                        <div class="i-checks"><label> <input type="radio" value="option9" name="a" onClick="open_textarea('text9')"> Shift - F9 </label></div>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group radio-val" id='option1'>
                                        <textarea class="form-control" id="text1" style="width: 100%;height: 250px;"></textarea>
                                    </div>
                                    <div class="form-group radio-val" id="option2">
                                        <textarea class="form-control" id="text2" style="width: 100%;height: 250px; "></textarea>
                                     </div>
                                    <div class="form-group radio-val" id="option3">
                                        <textarea class="form-control" id="text3" style="width: 100%;height: 250px; "></textarea>
                                     </div>
                                    <div class="form-group radio-val" id="option4">
                                        <textarea class="form-control" id="text4" style="width: 100%;height: 250px; "></textarea>
                                     </div>
                                    <div class="form-group radio-val" id="option5">
                                        <textarea class="form-control" id="text5" style="width: 100%;height: 250px; "></textarea>
                                     </div>
                                    <div class="form-group radio-val" id="option6">
                                        <textarea class="form-control" id="text6" style="width: 100%;height: 250px; "></textarea>
                                     </div>
                                    <div class="form-group radio-val" id="option7">
                                        <textarea class="form-control" id="text7" style="width: 100%;height: 250px; "></textarea>
                                     </div>
                                    <div class="form-group radio-val" id="option8">
                                        <textarea class="form-control" id="text8" style="width: 100%;height: 250px; "></textarea>
                                     </div>
                                    <div class="form-group radio-val" id="option9">
                                        <textarea class="form-control" id="text9" style="width: 100%;height: 250px; "></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                        <button type="submit" class="btn btn-primary btn-md">Save</button>
                                        <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        </div>


        <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.i-checks').on('Click', function () {
                alert(this)
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                 autoclose: true
            });
        });
        
    </script>

</body>
</html>