<div class="wrapper wrapper-content">
    <div class="dashboard">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="row dashboard-panels">
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-primary panel-01">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa-user fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge">No. of Users</div>
                                                    <div><h3>Active - <?php echo $user_active_cnt; ?></h3></div>
                                                    <div><h3>Non-Active - <?php echo $user_non_active_cnt; ?></h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="http://www.lawwaresuperadmin.dev/firm">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-success panel-02">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa-folder-open fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge">No. of Cases</div>
                                                    <div><h3>Active - <?php echo $case_active_cnt; ?></h3></div>
                                                    <div><h3>Deleted - <?php echo $case_del_active_cnt; ?></h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-danger panel-03">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa-envelope fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge"><?php echo $email_cnt;?></div>
                                                    <div><h3>Number of emails </h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="http://www.lawwaresuperadmin.dev/firm">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-warning panel-04">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa-list fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge"><?php echo $caseact_cnt;?></div>
                                                    <div><h3>Case Activities </h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="http://www.lawwaresuperadmin.dev/firm">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                             <div class="row dashboard-panels">
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-primary panel-05">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa-edit fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge"><?php echo $pnc_cnt;?></div>
                                                    <div><h3>No. of PNC</h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="http://www.lawwaresuperadmin.dev/firm">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-success panel-06">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa-check-square-o fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge"><?php echo $completed_task_cnt;?></div>
                                                    <div><h3>Completed Tasks</h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-danger panel-07">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa fa-list-alt fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge"><?php echo $total_task_cnt;?></div>
                                                    <div><h3>Total Tasks </h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="http://www.lawwaresuperadmin.dev/firm">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="panel panel-warning panel-08">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3 icon-top">
                                                    <i class="fa fa-users fa-4x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div class="huge"><?php echo $card_cnt; ?></div>
                                                    <div><h3>No of Rolodex parties </h3></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="http://www.lawwaresuperadmin.dev/firm">
                                            <div class="panel-footer hidden">
                                                <span class="pull-left">View Details</span>
                                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
           
                <div class="col-lg-12">
                        <div class="ibox float-e-margins ">
                            <div class="ibox-title ibox-bg-forcolor">
                                <h5>Configurations</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                   <div class="col-lg-3 col-sm-3">
                                    <a href="<?php echo base_url();?>admin/firm">
                                        <div class="ibox-content box">
                                            <i class="fa fa-table"></i>
                                            <span>Firm Settings</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-sm-3">
                                    <a href="<?php echo base_url();?>admin/general">
                                        <div class="ibox-content box">
                                            <i class="fa fa-pie-chart"></i>
                                            <span>General Settings</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-sm-3">
                                    <a href="<?php echo base_url();?>admin/newcases">
                                        <div class="ibox-content box">
                                            <i class="fa fa fa-list-alt" aria-hidden="true"></i>
                                            <span>New Case</span>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-lg-3 col-sm-3">
                                    <a href="<?php echo base_url();?>admin/injuries">
                                        <div class="ibox-content box">
                                            <i class="fa fa-list-alt"></i>
                                            <span>Injuries Settings</span>
                                        </div>
                                    </a>
                                </div>


                            </div>
                            <div class="row">
                               <div class="col-lg-3 col-sm-3">
                                <a href="<?php echo base_url();?>admin/prospects">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>PNC Settings</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="<?php echo base_url();?>admin/featureRequest">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>Feature(s) Request</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <a href="<?php echo base_url();?>admin/checklist">
                                    <div class="ibox-content box">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>Support Checklist</span>
                                    </div>
                                </a>
                            </div>
                            </div>
                </div>
            </div>
        </div>
                <!--   <div class="col-lg-6">
            <div class="ibox float-e-margins dContainer">
                <div class="ibox-title ibox-bg-forcolor">
                    <h5>New Emails</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="NewEmailDashboard">
                            <thead>
                                <tr>
                                    <th style="width: 20%">Date</th>
                                    <th style="width: 30%">From</th>
                                    <th style="width: 30%">Subject</th>
                                    <th style="width: 10%">Urgent</th>
                                    <th style="width: 10%">Read</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($received_emails as $key => $emails) {
                                    if ($key > 4)
                                        continue;
                                    ?>
                                    <tr>
                                        <td><?php echo date('m/d/Y', strtotime($emails->datesent)); ?></td>
                                        <td><?php echo (!empty($emails->whofrom_name)) ? $emails->whofrom_name : ucfirst(strstr($emails->whofrom,'@',true)); ?></td>
                                        <td><?php echo $emails->subject; ?></td>
                                        <td><?php echo ($emails->urgent == 'Y') ? 'Yes' : 'No'; ?></td>
                                        <td><?php echo ($emails->readyet == 'Y') ? 'Yes' : 'No'; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>

                </div>
        </div>
                </div> -->
        </div>
    
<div class="row">
    
  <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title ibox-bg-forcolor">
                <h5>Recent Tasks </h5>
                <!-- <a class="btn pull-right btn-primary btn-sm" href="#" title="View all Tasks" target="_blank">View All</a> -->
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="TableRecentTask">
                        <thead>
                            <tr>
                                <th style="width:20%">Finish by</th>
                                <th style="width:6%">Priority</th>
                                <th>Event</th>
                                <th style="width:20%">Type</th>
                                <th width="85" class="nosort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($latest_tasks as $key=>$task) { ?>
                            <tr id='<?php echo $task->caseno; ?>'>
                                <td><?php echo date('m/d/Y', strtotime($task->finishby)); ?></td>
                                <td><?php echo $task->priority; ?></td>
                                <td class="textwrap-line"><?php echo $task->event; ?></td>
                                <td><?php echo $task->typetask; ?></td>
                                <td> <button class="btn btn-info btn-circle-action btn-circle edittask" type="button" onClick="editTask(<?php echo $task->mainkey;?>)"><i class="fa fa-edit" title="Edit"></i></button>&nbsp;&nbsp;
                                  <?php if($task->whofrom==$username){?>  <button class="btn btn-danger btn-circle-action btn-circle" type="button" onClick="deleteTask(<?php echo $task->mainkey;?>)"><i class="fa fa-trash" title="Delete"></i></button> <?php } ?>
                                  <?php if($task->completed!='0000-00-00'){ ?><a href="#" class="btn btn-primary btn-circle-action btn-circle complete_task" title="Complete"  data-id='<?php echo $task->mainkey;?>' data-event='<?php echo $task->event;?>' data-caseno='<?php echo $task->caseno;?>'><i class="fa fa-check"></i></a><?php } ?>
                              </td>
                          </tr>
                          <?php } ?>
                      </tbody>
                  </table>
              </div>

          </div>
      </div>
  </div>
  
  <div class="col-lg-6">
    <div class="ibox float-e-margins">
        <div class="ibox-title ibox-bg-forcolor">
            <h5>Recent Events</h5>
            <!-- <a class="btn btn-sm pull-right btn-primary" href="<?php echo base_url()."calendar"; ?>" title="View all Events" target="_blank">View All</a> -->
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="DashboardRecentEvents">
                    <thead>
                        <tr>

                            <th>Time</th>
                            <th>AttyH</th>
                            <th>AttyA</th>
                            <th>Name</th>
                            <th>Event</th>
                            <th>Venue</th>
                            <th>Judge</th>
                        </tr>
                    </thead>
                    <tbody>
                  <?php foreach($calendar_event as $event){ ?>
                    <tr>   
            
                        <td><?php echo $event->caltime ;?></td>
                        <td><?php echo $event->atty_hand ;?></td>
                        <td><?php echo $event->attyass ;?></td>
                        <td><?php echo $event->initials ;?></td>
                        <td><?php echo $event->event ;?></td>
                        <td><?php echo $event->venue ;?></td>
                        <td><?php echo $event->judge ;?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
</div>
    </div>
</div>


