<?php $main1=json_decode($firm['main1']);
                        //$ccudf1=json_decode($firm['ccudf1']);
                       // $yourfilen1=json_decode($firm['yourfilen1']);
                // echo   $firm['ccfiletk'] ;
                  // print_r($main1);
                 //       exit;
                        
                        ?> 
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <a>Configure System Defaults</a> /
                    <span>New Case</span>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Drives and Sub-Directories</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" name="DrivesForm" id="DrivesForm" enctype="multipart/form-data" action="#">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Client folder alernate UNC paths</label>
                                        <select class="form-control">
                                            <option>Disabled</option>  
                                             <option>Activate</option>
                                              <option>Activate and Override</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Example paths</label>
                                        <textarea class="form-control" style="width: 100%;height: 100px;"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-md">Network/offline status</button>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Prospect path override</label>
                                        <input type="text" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Local Drive/dir</label>
                                        <input type="text" class="form-control"  name="system_drvlocl" id="system_drvlocl" value="<?php echo isset($firm['drvlocl'])?$firm['drvlocl']:''?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Comp data</label>
                                        <input type="file" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group text-center m-t-md m-b-xs">
                                           <button type="button" id="savedrives" class="btn btn-md btn-primary ">Save</button>
                                           <button type="submit" class="btn btn-primary btn-md">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
      <script type="text/javascript">
      $('#savedrives').on('click', function () {
                 savetoadmin('DrivesForm');
     }); 
     
      </script>  