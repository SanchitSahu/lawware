<?php
$main1 = json_decode($firm['main1']);
$gen2 = json_decode($firm['caldeflt']);
?>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumbs">
                <a>Configure System Defaults</a> /
                <span>Calendar</span>
            </div>
            <form method="post" name="CalenderForm" id="CalenderForm" enctype="multipart/form-data" action="#">

                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">General 1</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">General 2</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Notes</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4">Popups</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Calendar Titles</label>
                                                    <textarea class="form-control" style="width: 100%;height: 220px;" name="system_calendars" id="system_calendars"><?php echo isset($firm['calendars']) ? $firm['calendars'] : ''; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Popup Times</label>
                                                    <input type="text" class="form-control" name="system_timedd" id="system_timedd" value="<?php echo isset($firm['timedd']) ? $firm['timedd'] : '' ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Default Calendar Time</label>
                                                    <input type="text" class="form-control" name="system_caltime" id="system_caltime" value="<?php echo isset($firm['caltime']) ? $firm['caltime'] : '' ?>">

                                                </div>
                                                <div class="form-group">
                                                    <label>Default Calendar View</label>
                                                    <select class="form-control" name='system_calview' id='system_calview'>
                                                        <option value='basicDay' <?php
                                                        if ($firm['calview'] == 'basicDay') {
                                                            echo "selected";
                                                        }
                                                        ?>>Day</option>
                                                        <option value='basicWeek' <?php
                                                        if ($firm['calview'] == 'basicWeek') {
                                                            echo "selected";
                                                        }
                                                        ?>>Week</option>
                                                        <option value='month' <?php
                                                        if ($firm['calview'] == 'month') {
                                                            echo "selected";
                                                        }
                                                        ?>>Month</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Attorney Assigned</label>
                                                    <select class="form-control" name='system_calattyass' id='system_calattyass'>
                                                        <option value='1' <?php
                                                        if ($firm['calattyass'] == '1') {
                                                            echo "selected";
                                                        }
                                                        ?>>Always Enable</option>
                                                        <option value='2' <?php
                                                        if ($firm['calattyass'] == '2') {
                                                            echo "selected";
                                                        }
                                                        ?>>Enable When adding</option>
                                                        <option value='3' <?php
                                                        if ($firm['calattyass'] == '3') {
                                                            echo "selected";
                                                        }
                                                        ?>>Always disable</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <!-- <div class="col-sm-6">
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_utocfs"   id="system_main_utocfs" <?php
                                                            if (isset($main1->utocfs) && $main1->utocfs == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Use type of case for status </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_uaaafcase" id="system_main_uaaafcase" <?php
                                                            if (isset($main1->uaaafcase) && $main1->uaaafcase == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Use AttyR and AttyH from cases </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_GEN2_udifa"  id="system_GEN2_udifa" <?php
                                                            if (isset($gen2->udifa) && $gen2->udifa == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Use default initials for Attorney </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_GEN2_udifaa" id="system_GEN2_udifaa" <?php
                                                            if (isset($gen2->udifaa) && $gen2->udifaa == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Use default initials for AttyA </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_GEN2_confAtt" id="system_GEN2_confAtt" <?php
                                                            if (isset($gen2->confAtt) && $gen2->confAtt == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Conflicts for Attorney </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_GEN2_confAttA" id="system_GEN2_confAttA"   <?php
                                                            if (isset($gen2->confAttA) && $gen2->confAttA == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Conflicts for AttyA </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_ocwsave" id="system_main_ocwsave" <?php
                                                            if (isset($main1->ocwsave) && $main1->ocwsave == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Only check when saving </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_fptcaseact" id="system_main_fptcaseact" <?php
                                                            if (isset($main1->fptcaseact) && $main1->fptcaseact == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Force posting to case activity </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_ieflastch" id="system_main_ieflastch" <?php
                                                            if (isset($main1->ieflastch) && $main1->ieflastch == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Ignore edits for last changed </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_ust2ediscr" id="system_main_ust2ediscr" <?php
                                                            if (isset($main1->ust2ediscr) && $main1->ust2ediscr == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Use style 2 Edit screen </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_ucinre" id="system_main_ucinre" <?php
                                                            if (isset($main1->ucinre) && $main1->ucinre == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Use captions in reports </label></span>
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_creaufile" id="system_main_creaufile" <?php
                                                            if (isset($main1->creaufile) && $main1->creaufile == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>/> Create Audit file </label></span>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Defendant party to use</label>
                                                    <input type="text" class="form-control" name="system_main_dpartytuse"  id="system_main_dpartytuse" value="<?php echo isset($main1->dpartytuse) ? $main1->dpartytuse : '' ?>"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Time range for conflicts</label>
                                                    <input type="number" class="form-control" min='0' name='system_GEN2_tranforcon' id='system_GEN2_tranforcon' value='<?php echo isset($gen2->tranforcon) ? $gen2->tranforcon : '' ?>'/>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label>Year to use when adding</label>
                                                    <input type="number" class="form-control" min='0' name='system_GEN2_yeartud' id='system_GEN2_yeartud' value='<?php echo isset($gen2->yeartud) ? $gen2->yeartud : '' ?>' />
                                                </div> -->
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Case activity categories to post to</label>
                                                    <input type="text" class="form-control" name='system_main_caseactcatpt' id='system_main_caseactcatpt' value='<?php echo isset($main1->caseactcatpt) ? $main1->caseactcatpt : '' ?>'  />
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label>Default status</label>
                                                    <input type="text" class="form-control" name='system_main_defsta' id='system_main_defsta' value='<?php echo isset($main1->defsta) ? $main1->defsta : '' ?>'   />
                                                </div> -->
                                                <div class="form-group">
                                                    <label>Report caption</label>
                                                    <input type="text" class="form-control" name='system_main_repcap' id='system_main_repcap' value='<?php echo isset($main1->repcap) ? $main1->repcap : '' ?>'  />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Default notes to use for this calendar</label>
                                                    <textarea class="form-control" style="width: 100%;height: 200px;" name='system_calnotes' id='system_calnotes'><?php echo isset($firm['calnotes']) ? $firm['calnotes'] : '' ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content marg-bot15">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Judge</label>
                                                    <input type="text" class="form-control" name="system_main_judge" id="system_main_judge" value="<?php echo isset($main1->judge) ? $main1->judge : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Event</label>
                                                    <input type="text" class="form-control" name="system_main_event" id="system_main_event" value="<?php echo isset($main1->event) ? $main1->event : '' ?>">
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label>AttyR - Cal</label>
                                                    <input type="text" class="form-control" name="system_main_attrcal" id="system_main_attrcal" value="<?php echo isset($main1->attrcal) ? $main1->attrcal : '' ?>">
                                                </div> -->
                                                <div class="form-group">
                                                    <span><label> <input type="checkbox" class="i-checks" name="system_main_useattpoincalv" id="system_main_useattpoincalv" <?php
                                                            if (isset($main1->useattpoincalv) && $main1->useattpoincalv == 'on') {
                                                                echo 'checked';
                                                            }
                                                            ?>> Use AttyH popups in calendar views </label></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Venue</label>
                                                    <input type="text" class="form-control" name="system_main_venue" id="system_main_venue" value="<?php echo isset($main1->venue) ? $main1->venue : ''; ?>">
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label>Status</label>
                                                    <input type="text" class="form-control" name="system_main_status" id="system_main_status" value="<?php echo isset($main1->status) ? $main1->status : ''; ?>">
                                                </div> -->
                                                <!-- <div class="form-group">
                                                    <label>AttyH - Cal</label>
                                                    <input type="text" class="form-control" name="system_main_atthcal" id="system_main_atthcal" value="<?php echo isset($main1->atthcal) ? $main1->atthcal : ''; ?>">
                                                </div> -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group text-center m-t-md m-b-xs">
                                <button type="button"  id="savecalender" class="btn btn-primary btn-md">Save</button>
                                <button type="submit" class="btn btn-danger btn-md">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>




    <script type="text/javascript">
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.date').datepicker({
                forceParse: false,
                autoclose: true
            });

            $('.color').colorpicker({
                autoclose: true
            });
        });
        $('#savecalender').on('click', function () {
            savetoadmin('CalenderForm');

        });
    </script>


