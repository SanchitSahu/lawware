<?php

class Report_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //*************************************************//
    // * @name   : getCasecountReport
    // * @todo   : Get data of case count report by filter
    // * @Date   : 22-May-2018
    //************************************************//

    public function getCasecountReport($data, $start = 0, $limit = 10, $order = '', $orCondition = '') {
        echo "<pre>";
        print_r($data);
        exit;
        if (count($data) > 0) {
            $this->db->where($data);
        }

        $this->db->order_by('cl.date');
        $this->db->group_by('cc.caseno');

        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }

        $this->db->select('*');
        $this->db->join('casecard c', 'c.caseno = cc.caseno', 'left');
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('(SELECT `date`,`event`,caseno,attyass FROM cal1 ORDER BY `date` DESC) cl', 'cl.caseno = cc.caseno', 'left');
        $q = $this->db->get('case c');
        $result = $q->result_array();

        echo "<pre>";
        print_r($result);
        exit;

        return $result;
    }

    //*************************************************//
    // * @name   : getFilterCaseReport
    // * @todo   : Get data of case report by filter
    // * @Date   : 22-May-2018
    //************************************************//

    public function getFilterCaseReport($start = 0, $limit = 10, $where = '', $orCond = '', $order = '', $CaseUnique = '') {

        $select = array("c.caseno", "cd.last", "c.atty_resp", "c.atty_hand", "c.casetype", "c.casestat", "cd.home", "c.yourfileno", "c.followup", "c.dateenter", "c.para_hand", "c.sec_hand", "cd.social_sec", "cd.type", "cc.cardcode", "cd2.firm", "case_status", "c.caption1", "cd2.address1", "cd.business", "cd.middle", "cd.first", "cd2.city", "cd2.address2", "cd2.state", "cd2.zip");

        if ($order != '' && !empty($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }

        if ($orCond != '') {
            $this->db->where($orCond);
        }
        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }
        if($CaseUnique == "1") {
            $this->db->group_by("cd.first");
            $this->db->group_by("cd.last");
        } else {
            $this->db->group_by('c.caseno');
        }

        $this->db->select("c.caseno,cd.first,cd.middle,cd.last,c.atty_resp,c.atty_hand,c.casetype,c.casestat,cd.home, cd.business,c.yourfileno,c.followup,c.dateenter,c.para_hand,c.sec_hand,cd.social_sec,cc.type,cc.cardcode,cd2.firm,c.caption1,cd2.address1,c.case_status, cd2.city,cd2.address2,cd2.state,cd2.zip");
        $this->db->from('case c');
        $this->db->join('casecard cc', 'c.caseno = cc.caseno', 'left');
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $q = $this->db->get();
        $result = $q->result_array();
        //echo $this->db->last_query(); exit;
        return $result;
    }

    //*************************************************//
    // * @name   : getFilterCaseReportCount
    // * @todo   : Get count of Case report
    // * @Date   : 22-May-2018
    //************************************************//

    public function getFilterCaseReportCount($filterArray = "") {
        if (!empty($filterArray)) {
            $this->db->where($filterArray);
        }
        
        if($CaseUnique == "1") {
            $this->db->group_by("cd.first");
            $this->db->group_by("cd.last");
        } else {
            $this->db->group_by('c.caseno');
        }
        
        $this->db->select("c.caseno");
        $this->db->from('case c');
        $this->db->join('casecard cc', 'c.caseno = cc.caseno', 'left');
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        

        $q = $this->db->get();
        $result = $q->result_array();
        $Casecount = count($result);
        return $Casecount;
    }

    //*************************************************//
    // * @name   : getinjuryDates
    // * @todo   : Get Dates of injury of selected case
    // * @Date   : 14-Sep-2018
    //************************************************//

    public function getinjuryDates($caseno = NULL) {

        $this->db->select("i.adj1c, i.eamsno, i.case_no");
        $this->db->from('injury i');
        $this->db->where('i.caseno', $caseno);
        $q = $this->db->get();
        $result = $q->result_array();
        return $result;
    }

    //*************************************************//
    // * @name   : getSearchReferralReport
    // * @todo   : Get data for Referral Report
    // * @Date   : 25-May-2018
    //************************************************//

    public function getSearchReferralReport($data, $start = 0, $limit = 10, $order = '', $orCondition = '') {

        $select = array('c.caseno', 'c.casetype', 'c.casestat', 'c.atty_hand', 'c.d_r', 'c2.firm', 'c.caption1', 'cd.last', 'cd.first', 'c2.firmcode', 'c.rb');

        if ($order != '' && !empty($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        if (count($data) > 0) {
            $this->db->where($data);
        }

        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }

        $this->db->from('case c');
        $this->db->select('c.caseno,c.casetype,c.casestat,c.atty_hand,c.d_r,c2.firm,c.caption1, cd.last, cd.first,c2.firmcode,,c.rb');
        $this->db->join('casecard cc', 'cc.cardcode = c.rb', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('card2 c2', 'c2.firmcode= cd.firmcode', 'left');
        $this->db->group_by('c.caseno');
        $q = $this->db->get();
        return $q->result();
    }

    //*************************************************//
    // * @name   : getSearchReferralReportCount
    // * @todo   : Get count for search referral report
    // * @Date   : 25-May-2018
    //************************************************//

    public function getSearchReferralReportCount($data = '') {

        if ($data != '' && is_array($data)) {
            $this->db->where($data);
        }
        $this->db->select('c.caseno');
        $this->db->from('case c');
//        $this->db->where('c.rb !=', 0);
        $q = $this->db->get();
        return $q->num_rows();
    }

    //*************************************************//
    // * @name   : getCaseactivity
    // * @todo   : Get data for Referral Report
    // * @Date   : 10-Aug-2018
    //************************************************//

    public function getCaseactivity($filterarray, $start = 0, $limit = 10, $order = '', $orCondition = '') {

        $where = [];
        if ($filterarray['casetype'] != '') {
            $where['c.casetype'] = $filterarray['casetype'];
        }
        if ($filterarray['casestat'] != '') {
            $where['c.casestat'] = $filterarray['casestat'];
        }
        if ($filterarray['atty_hand'] != '') {
            $where['c.atty_hand'] = $filterarray['atty_hand'];
        }
        if ($filterarray['category'] != '') {
            $where['ca.category'] = $filterarray['category'];
        }
        if ($filterarray['event_contains'] != '') {
            $event_contains = $filterarray['event_contains'];
        }

        if ($filterarray['event'] != '') {
            if ($event_contains == 'on') {
                $where['ca.event LIKE '] = "%" . $filterarray['event'] . "%";
            } else {
                $where['ca.event'] = $filterarray['event'];
            }
        }
        if ($filterarray['documents'] != '') {
            $filterarray['documents'] = str_replace("f", "", $filterarray['documents']);
            $filterarray['documents'] = strtok($filterarray['documents'], '_');
            $where['ca.actno LIKE'] = "%" . $filterarray['documents'] . "%";
        }

        if ($filterarray['end_ca_date'] != '') {
            $end_ca_date = $filterarray['end_ca_date'];
        }
        if ($filterarray['start_ca_date'] != '') {
            $start_ca_date = $filterarray['start_ca_date'];
        }
        if ($start_ca_date != "" && $end_ca_date != "") {
            $where['ca.date >='] = date('Y-m-d 00:00:00', strtotime($start_ca_date));
            $where['ca.date <='] = date('Y-m-d 23:59:59', strtotime($end_ca_date));
        } elseif ($start_ca_date != "" && $end_ca_date == "") {
            $where['ca.date >='] = date('Y-m-d 00:00:00', strtotime($start_ca_date));
        } elseif ($start_ca_date == "" && $end_ca_date != "") {
            $where['ca.date <='] = date('Y-m-d 23:59:59', strtotime($end_ca_date));
        }
        if ($filterarray['max_category'] != '') {
            $where['ca.category <='] = $filterarray['max_category'];
        }

        if (isset($filterarray['noActivity']) && $filterarray['noActivity'] == 'on') {
            $this->db->group_by('c.caseno');
            $this->db->where('(ca.id IS NULL OR ((ca.category BETWEEN 1000001 AND 1000021) AND (SELECT COUNT(*) FROM caseact WHERE NOT(category BETWEEN 1000001 AND 1000021) AND caseno = c.caseno) <= 0))');
        } else {
            $this->db->where('NOT(ca.category BETWEEN 1000001 AND 1000020)');
            if (isset($filterarray['most_recent']) && $filterarray['most_recent'] == 'on') {
                $this->db->where('ca.category !=', '1000021');
                $this->db->group_by('ca.caseno');
            } else {
//                $this->db->group_by('ca.id');
            }
        }

        $select = array('ca.caseno', 'c.casetype', 'c.casestat', 'c.atty_hand', 'ca.date', 'ca.initials', 'cd.first', 'ca.event', 'ca.actno', 'cd.last');

        if ($order != '' && !empty($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        if (!empty($where)) {
            $this->db->where($where);
        }
        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }

        $this->db->select('ca.caseno,c.casetype,c.casestat,c.atty_hand,ca.date,ca.initials,cd.first,ca.event,ca.actno,cd.last');
        $this->db->from('case c');
        $this->db->join('caseact ca', 'c.caseno = ca.caseno', 'left');
        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->where('cc.orderno', '1');
        $q = $this->db->get();
        $result = $q->result();
        return $result;
    }

    //*************************************************//
    // * @name   : getCaseactivityCount
    // * @todo   : Get count for search case activity report
    // * @Date   : 10-Aug-2018
    //************************************************//

    public function getCaseactivityCount($filterarray = []) {

        $where = [];
        if ($filterarray['casetype'] != '') {
            $where['c.casetype'] = $filterarray['casetype'];
        }
        if ($filterarray['casestat'] != '') {
            $where['c.casestat'] = $filterarray['casestat'];
        }
        if ($filterarray['atty_hand'] != '') {
            $where['c.atty_hand'] = $filterarray['atty_hand'];
        }
        if ($filterarray['category'] != '') {
            $where['ca.category'] = $filterarray['category'];
        }
        if ($filterarray['event_contains'] != '') {
            $event_contains = $filterarray['event_contains'];
        }

        if ($filterarray['event'] != '') {
            if ($event_contains == 'on') {
                $where['ca.event LIKE '] = "%" . $filterarray['event'] . "%";
            } else {
                $where['ca.event'] = $filterarray['event'];
            }
        }
        if ($filterarray['documents'] != '') {
            $filterarray['documents'] = str_replace("f", "", $filterarray['documents']);
            $filterarray['documents'] = strtok($filterarray['documents'], '_');
            $where['ca.actno LIKE'] = "%" . $filterarray['documents'] . "%";
        }

        if ($filterarray['end_ca_date'] != '') {
            $end_ca_date = $filterarray['end_ca_date'];
        }
        if ($filterarray['start_ca_date'] != '') {
            $start_ca_date = $filterarray['start_ca_date'];
        }
        if ($start_ca_date != "" && $end_ca_date != "") {
            $where['ca.date >='] = date('Y-m-d 00:00:00', strtotime($start_ca_date));
            $where['ca.date <='] = date('Y-m-d 23:59:59', strtotime($end_ca_date));
        } elseif ($start_ca_date != "" && $end_ca_date == "") {
            $where['ca.date >='] = date('Y-m-d 00:00:00', strtotime($start_ca_date));
        } elseif ($start_ca_date == "" && $end_ca_date != "") {
            $where['ca.date <='] = date('Y-m-d 23:59:59', strtotime($end_ca_date));
        }
        if ($filterarray['max_category'] != '') {
            $where['ca.category <='] = $filterarray['max_category'];
        }

        if (isset($filterarray['noActivity']) && $filterarray['noActivity'] == 'on') {
            $this->db->group_by('c.caseno');
            $this->db->where('(ca.id IS NULL OR ((ca.category BETWEEN 1000001 AND 1000021) AND (SELECT COUNT(*) FROM caseact WHERE NOT(category BETWEEN 1000001 AND 1000021) AND caseno = c.caseno) <= 0))');
        } else {
            $this->db->where('NOT(ca.category BETWEEN 1000001 AND 1000020)');
            if (isset($filterarray['most_recent']) && $filterarray['most_recent'] == 'on') {
                $this->db->group_by('ca.caseno');
                $this->db->where('ca.category !=', '1000021');
            } else {
//                $this->db->group_by('ca.id');
            }
        }

        if (!empty($where)) {
            $this->db->where($where);
        }

        $this->db->select('c.caseno');
        $this->db->from('case c');
        $this->db->join('caseact ca', 'c.caseno = ca.caseno', 'left');
        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->where('cc.orderno', '1');
        $q = $this->db->get();
        return $q->num_rows();
    }

    //*************************************************//
    // * @name   : getSearchInjuryReport
    // * @todo   : Get data for Injury Report
    // * @Date   : 09-Aug-2018
    //************************************************//

    public function getSearchInjuryReport($filterArray, $start = 0, $limit = 10, $order = '', $orCondition = '') {
        $select = array('c.caseno', 'c.casetype', 'c.casestat', 'c.atty_hand', 'cd.first', 'i.e_name', 'i.i_name', 'i.d1_first', 'i.adj1c', 'i.adj1e', 'i.adj2a', 'i.eamsno', 'i.i_claimno', 'i.doi2', 'c.yourfileno', 'i.status2', 'c2.venue', 'i.adj1a', 'i.injury_id', 'i.app_status', 'i.e2_name', 'i.i2_name', 'i.pob1', 'i.pob2', 'i.pob3', 'i.pob4', 'i.pob5', 'i.i2_claimno', 'cd.last', 'i.i_adjfst', 'i.i2_adjfst', 'i.d1_last', 'i.d2_first', 'i.d2_last', 'i.case_no');

        $where = [];
        if ($filterArray['casetype'] != "") {
            $where['c.casetype'] = $filterArray['casetype'];
        }
        if ($filterArray['case_status'] != "") {
            $where['c.casestat'] = $filterArray['case_status'];
        }
        if ($filterArray['atty_hand'] != "") {
            $where['c.atty_hand'] = $filterArray['atty_hand'];
        }
        if ($filterArray['app_status'] != "") {
            $where['i.app_status'] = $filterArray['app_status'];
        }

        if ($filterArray['pob'] != "") {
            $where['i.adj1e'] = $filterArray['pob'];
        }

        if ($filterArray['body_parts'] != "") {
            $this->db->where('(i.pob1 = "' . $filterArray['body_parts'] . '"  OR i.pob2 = "' . $filterArray['body_parts'] . '" OR i.pob3 = "' . $filterArray['body_parts'] . '" OR i.pob4 = "' . $filterArray['body_parts'] . '" OR i.pob5 = "' . $filterArray['body_parts'] . '")');
        }

        if (isset($filterArray['i_wcab']) && $filterArray['i_wcab'] != "") {
            $this->db->where('(i.case_no = "' . $filterArray['i_wcab'] . '" OR i.eamsno = "' . $filterArray['i_wcab'] . '")');
        }

        if (isset($filterArray['i_claimno']) && $filterArray['i_claimno'] != '') {
            $this->db->where('(i.i_claimno = "' . $filterArray['i_claimno'] . '" OR i.i2_claimno = "' . $filterArray['i_claimno'] . '")');
        }
        if (isset($filterArray['doi']) && $filterArray['doi'] != "") {
            $this->db->where('i.doi', (date('Y-m-d H:i:s', strtotime($filterArray["doi"]))));
        }

        if (isset($filterArray["doi_my"]) && $filterArray["doi_my"] != "") {
            $doi_my = DateTime::createFromFormat('m/Y', $filterArray["doi_my"]);
            $doi_my = $doi_my->format('Y-m');

            $doimonth = date('m', strtotime($doi_my));
            $doiyear = date('Y', strtotime($doi_my));

            $this->db->where('month(i.doi)', $doimonth);
            $this->db->where('year(i.doi)', $doiyear);
        }
        if (isset($filterArray['no_injury']) && $filterArray['no_injury'] == 'on') {
            $this->db->where('i.injury_id IS NULL');
            $this->db->group_by('c.caseno');
        } else {
            $this->db->group_by('i.injury_id');
        }

        if ($order != '' && !empty($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        if (!empty($where)) {
            $this->db->where($where);
        }

        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }

        $this->db->select('i.injury_id,c.caseno,c.casetype,c.casestat,i.app_status,c.atty_hand,i.e_name,i.e2_name,i.i_adjfst,i.i2_adjfst,i.d1_first,i.d1_last,i.d2_first, i.d2_last,i.adj1c,i.adj1e,i.adj2a,i.case_no,i.i_claimno,i.i2_claimno,i.doi2,c.yourfileno,i.status2,c2.venue,i.adj1a,i.eamsno,cd.first,cd.last,i.i_name,i.i2_name,i.pob1,i.pob2,i.pob3,i.pob4,i.pob5');
        $this->db->from('injury i');
        $this->db->join('case c', 'c.caseno = i.caseno', 'left');
        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('card2 c2', 'c2.firmcode= cd.firmcode', 'left');

        $q = $this->db->get();
        return $q->result();
    }

    //*************************************************//
    // * @name   : getSearchInjuryReportCount
    // * @todo   : Get count for search Injury report
    // * @Date   : 09-Aug-2018
    //************************************************//

    public function getSearchInjuryReportCount($filterArray = '') {
        $where = [];
        if ($filterArray['casetype'] != "") {
            $where['c.casetype'] = $filterArray['casetype'];
        }
        if ($filterArray['case_status'] != "") {
            $where['c.casestat'] = $filterArray['case_status'];
        }
        if ($filterArray['atty_hand'] != "") {
            $where['c.atty_hand'] = $filterArray['atty_hand'];
        }
        if ($filterArray['app_status'] != "") {
            $where['i.app_status'] = $filterArray['app_status'];
        }

        if ($filterArray['pob'] != "") {
            $where['i.adj1e'] = $filterArray['pob'];
        }

        if ($filterArray['body_parts'] != "") {
            $this->db->where('(i.pob1 = "' . $filterArray['body_parts'] . '"  OR i.pob2 = "' . $filterArray['body_parts'] . '" OR i.pob3 = "' . $filterArray['body_parts'] . '" OR i.pob4 = "' . $filterArray['body_parts'] . '" OR i.pob5 = "' . $filterArray['body_parts'] . '")');
        }

        if (isset($filterArray['i_wcab']) && $filterArray['i_wcab'] != "") {
            $this->db->where('(i.case_no = "' . $filterArray['i_wcab'] . '" OR i.eamsno = "' . $filterArray['i_wcab'] . '")');
        }

        if (isset($filterArray['i_claimno']) && $filterArray['i_claimno'] != '') {
            $this->db->where('(i.i_claimno = "' . $filterArray['i_claimno'] . '" OR i.i2_claimno = "' . $filterArray['i_claimno'] . '")');
        }
        if (isset($filterArray['doi']) && $filterArray['doi'] != "") {
            $this->db->where('i.doi', (date('Y-m-d H:i:s', strtotime($filterArray["doi"]))));
        }

        if (isset($filterArray["doi_my"]) && $filterArray["doi_my"] != "") {
            $doi_my = DateTime::createFromFormat('m/Y', $filterArray["doi_my"]);
            $doi_my = $doi_my->format('Y-m');

            $doimonth = date('m', strtotime($doi_my));
            $doiyear = date('Y', strtotime($doi_my));

            $this->db->where('month(i.doi)', $doimonth);
            $this->db->where('year(i.doi)', $doiyear);
        }
        if (isset($filterArray['no_injury']) && $filterArray['no_injury'] == 'on') {
            $this->db->where('i.injury_id IS NULL');
            $this->db->group_by('c.caseno');
        } else {
            $this->db->group_by('i.injury_id');
        }

        if (!empty($where)) {
            $this->db->where($where);
        }

        $this->db->select('i.injury_id');
        $this->db->from('case c');
        $this->db->join('injury i', 'c.caseno = i.caseno', 'left');
        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $q = $this->db->get();
        return $q->num_rows();
    }

    //*************************************************//
    // * @name   : getFilterTaskReport
    // * @todo   : Get Search filter for Task Reportt
    // * @Date   : 26-Sep-2018
    //************************************************//

    public function getFilterTaskReport($where, $start = 0, $limit = 10, $order = '', $filterType = '',$completed = '') {
        //echo '  ftype => '. $completed; exit;
        $select = array('t.caseno', 'c.casetype', 'c.casestat', 'c.atty_hand', 't.datereq', 't.whofrom', 't.whoto', 't.event', 'cd.last', 't.typetask', 't.priority', 't.dateass','t.completed','cd.first');
        if (!empty($where)) {
            $this->db->where($where);
        }
        
        if($completed == 'completed'){
            $this->db->where('t.completed != ','"1899-12-30 00:00:00"',FALSE);
            $this->db->where('t.completed != ','"0000-00-00 00:00:00"',FALSE);
        }
        else{
            $this->db->where('(t.completed = "1899-12-30 00:00:00" OR t.completed = "0000-00-00 00:00:00")');
        }
        if (isset($order) && !empty($order)) {
            foreach($order as $k => $v) :
                $this->db->order_by($select[$v['column']], $v['dir']);
            endforeach;
        }
        
        if($select[$order[0]['column']] != 'priority')
        {
                $this->db->order_by('priority',' ASC');
        }

        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }
        if($filterType == 'noact')
        {
            $this->db->select('c.caseno,c.casetype,c.casestat,c.atty_hand,c.caption1,cd.first,cd.last,t.datereq,t.whofrom,t.whoto,t.event,t.typetask,t.priority,t.dateass,t.completed');
            $this->db->join('tasks t', 't.caseno = c.caseno', 'left');
            $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
            $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
            $this->db->where('NOT EXISTS (SELECT * FROM `case`  cd WHERE cd.caseno != t.caseno)', NULL, FALSE);
            $this->db->group_by('c.caseno');
            $q = $this->db->get('case c');
        }
        else
        {
            $this->db->select('t.caseno,c.casetype,c.casestat,c.atty_hand,t.datereq,t.whofrom,t.whoto,t.event,cd.last ,t.typetask,t.priority,t.dateass,cd.first,t.completed');
            $this->db->join('case c', 'c.caseno = t.caseno', 'left');
            $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
            $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
            $this->db->group_by('t.mainkey');
            $q = $this->db->get('tasks t');
        }
        
       // echo $this->db->last_query(); exit;
        return $q->result();
    }

    //*************************************************//
    // * @name   : getFilterTaskReportCount
    // * @todo   : Get count for search task report
    // * @Date   : 26-Sep-2018
    //************************************************//

    public function getFilterTaskReportCount($where = '',$filterType = '',$completed) {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        
        if($completed == 'completed'){
            $this->db->where('t.completed != ','"1899-12-30 00:00:00"',FALSE);
            $this->db->where('t.completed != ','"0000-00-00 00:00:00"',FALSE);
        }
        else{
            $this->db->where('(t.completed = "1899-12-30 00:00:00" OR t.completed = "0000-00-00 00:00:00")');
        }
        
        if($filterType == 'noact')
        {
            $this->db->select('t.mainkey');
            $this->db->join('tasks t', 't.caseno = c.caseno', 'left');
            $this->db->where('NOT EXISTS (SELECT * FROM `case`  cd WHERE cd.caseno != t.caseno)', NULL, FALSE);
            $q = $this->db->get('case c');
        }
        else
        {
            $this->db->select('t.mainkey');
            $this->db->join('case c', 'c.caseno = t.caseno', 'left');
            $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
            $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
            $this->db->group_by('t.mainkey');
            $q = $this->db->get('tasks t');
        }
        //echo $this->db->last_query();
        return $q->num_rows();
    }

    //*************************************************//
    // * @name   : getTaskpoolrepot
    // * @todo   : Get Data For Pool Report of Tasks
    // * @Date   : 27-Sep-2018
    //************************************************//

    public function getTaskpoolrepot($start = 0, $limit = 10, $order = '', $orCondition = '') {

        $select = array('s.initials', 'total_task', 'Last3MonthComplTasks', 'TotalCompletedTasks');
        if (isset($order) && !empty($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }

        $TodayDate = date('Y-m-d 23:59:59');
        $TheeMonthBeforeDate = date("Y-m-d 00:00:00", strtotime("-3 months"));

        $this->db->select('s.initials, COUNT(t.mainkey) AS total_task,(SELECT COUNT(DISTINCT(mainkey)) FROM tasks WHERE  whoto  =  s.initials AND completed BETWEEN "' . $TheeMonthBeforeDate . '" AND "' . $TodayDate . '") AS Last3MonthComplTasks, (SELECT COUNT(DISTINCT(mainkey)) FROM tasks WHERE  whoto  =  s.initials AND completed !="1899-12-30 00:00:00" AND completed !="0000-00-00 00:00:00") AS TotalCompletedTasks');
        $this->db->from('staff s');
        $this->db->join('tasks t', 't.whoto = s.initials', 'left');
        $this->db->group_by('s.initials');
        $q = $this->db->get();
        $result = $q->result_array();
        return $result;
    }

    //*************************************************//
    // * @name   : getTaskpoolrepotCount
    // * @todo   : Get Count of Data For Pool Report of Tasks
    // * @Date   : 27-Sep-2018
    //************************************************//

    public function getTaskpoolrepotCount() {

        $TodayDate = date('Y-m-d 23:59:59');
        $TheeMonthBeforeDate = date("Y-m-d 00:00:00", strtotime("-3 months"));

        $this->db->select('s.initials, COUNT(t.mainkey) AS total_task, (SELECT COUNT(DISTINCT(mainkey)) FROM tasks WHERE  whoto  =  s.initials AND completed !="1899-12-30 00:00:00" AND completed !="0000-00-00 00:00:00") AS TotalCompletedTasks, (SELECT COUNT(DISTINCT(mainkey)) FROM tasks WHERE  whoto  =  s.initials AND completed BETWEEN "' . $TheeMonthBeforeDate . '" AND "' . $TodayDate . '") AS Last3MonthComplTasks');
        $this->db->from('staff s');
        $this->db->join('tasks t', 't.whoto = s.initials', 'left');
        $this->db->group_by('s.initials');
        $q = $this->db->get();
        $count = $q->num_rows();
        return $count;
    }

    //*************************************************//
    // * @name   : getFilterMarkupReport
    // * @todo   : Get Data for search Markup report
    // * @Date   : 30-May-2018
    //************************************************//

    public function getFilterMarkupReport($data, $start = 0, $limit = 10, $order = '', $orCondition = '') {

        if (count($data) > 0) {
            $this->db->where($data);
        }

        $this->db->order_by('cl.date');
        $this->db->group_by('cc.caseno');

        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }

        $this->db->select('c.caseno,c.followup,cd.first,cd.last,c.dateopen,c.venue,c.casetype,c.casestat,c.yourfileno,c.atty_resp,c.atty_hand,c.caption1,c.para_hand,cl.date,cl.event,cl.attyass');
        $this->db->join('case c', 'c.caseno = cc.caseno', 'left');
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('(SELECT `date`,`event`,caseno,attyass FROM cal1 ORDER BY `date` DESC) cl', 'cl.caseno = cc.caseno', 'left');
        $q = $this->db->get('casecard cc');
        return $q->result();
    }

    //*************************************************//
    // * @name   : getFilterMarkupReportCount
    // * @todo   : Get Count for search Markup report
    // * @Date   : 30-May-2018
    //************************************************//

    public function getFilterMarkupReportCount($where = '', $orCond = '') {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }

        $this->db->select('c.caseno');
        $this->db->join('case c', 'c.caseno = cc.caseno', 'left');
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $q = $this->db->get('casecard cc');

        return $q->num_rows();
    }

    //*************************************************//
    // * @name   : getcasetypelisting
    // * @todo   : Get listing of casetype for dropdown
    // * @Date   : 10-Oct-2018
    //************************************************//

    public function hgj() {
        $this->db->select('casetype');


        $q = $this->db->get('casecard cc');
        return $q->result();
    }

}
