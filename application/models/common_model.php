<?php

class Common_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getAllRecord($table, $fields, $cond = '', $orderby = '',$limit ='',$groupby='') {
        if ($orderby != '') {
            $this->db->order_by($orderby);
        }
        if ($cond != '') {
            $this->db->where($cond);
        }
        $this->db->select($fields,false);
        $this->db->from($table);
        if($limit !=''){
          $this->db->limit($limit, 0);
        }
        if($groupby !='')
        {
          $this->db->group_by($groupby);
        }
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result();
    }

    public function getSingleRecordById($table, $fields, $cond = '', $orderby = '',$groupby ='') {
      //echo $fields; exit;
        if ($orderby != '') {
            $this->db->order_by($orderby);
        }

        $this->db->where($cond);
        $this->db->select($fields,false);
        $this->db->from($table);
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->row();
    }
    /* public function update_note()
    {
        $this->db->select('comments,cardcode');
        $this->db->from('card');
        $q = $this->db->get();
        $query=$q->result();
       // print_r($query);exit;
       // $card=array();
        $i=0;
        $card=array();
        foreach($query as $row)
        {

            $card1=array();
           //$card1['notes']='';
          // $card1['comments']=$row->comments;
           $card1[] =array(
               'notes' =>'',
           );
           $card1[] =array(
               'comments' =>$row->comments,
           );
           //$card[]=$row->cardcode;
          $card=$card1;
           $comments= json_encode($card);
             $this->db->where('cardcode',$row->cardcode);
           $this->db->update('card', array('comments' => $comments));
          print_r($comments);
           //array_push($card,$card1);
        }
         exit;

          $this->db->where('cardcode',$row->cardcode);
           $this->db->update('card', array('comments' => $comments));
          // echo $row->cardcode;
      // }




    }*/
    public function getAllRecordProspect($caseid=''){

        $this->db->select('*');
        if($caseid !=''){
           $this->db->where('caseno',$caseid);
        }
        $this->db->order_by('prospect_id','DESC');
        $q = $this->db->get('intake');
       //echo $this->db->last_query(); exit;
        return $q->row();
    }
    public function getsingleRecordProspect($caseid='',$limit=1, $start=0){

        $this->db->select('*');
        if($caseid !=''){
           $this->db->where('caseno',$caseid);
        }
        $this->db->order_by('prospect_id','DESC');
       // if($limit!='' && $start!=''){
       $this->db->limit($limit, $start);
      //}
        $q = $this->db->get('intake');
       //echo $this->db->last_query(); exit;
        return $q->row();
    }
    public function getProspectId($caseid='',$limit=1, $start=0){

        $this->db->select('prospect_id');
        if($caseid !=''){
           $this->db->where('caseno',$caseid);
        }
       // $this->db->order_by('prospect_id','DESC');
       // if($limit!='' && $start!=''){
       //$this->db->limit($limit, $start);
      //}
        $q = $this->db->get('intake');
        //echo $this->db->last_query(); exit;
        return $q->row();
    }
    public function getMainKey() {
        $this->db->select('proskey', false);
        $this->db->order_by('proskey','DESC');
        $this->db->limit(1);
        $result = $this->db->get('intake');

        $data=$result->result();
        if(!empty($data))
        {
            return $data[0]->proskey+1;
        }
        else
        {
            return 1;
        }

    }
    public function getRecord(){

        $this->db->select('prospect_id,proskey,status,caseno');
        $this->db->order_by('prospect_id','DESC');
        $q = $this->db->get('intake');
        return $q->result();
    }
    public function getcount($table, $fields,$cond =''){
      $this->db->select($fields);
      $this->db->from($table);
       if ($cond != '') {
            $this->db->where($cond);
        }
      return $this->db->count_all_results();
    }
    public function getProspectDetails($table, $fieldname) {
        $this->db->select($fieldname);
        $this->db->from($table);
        $q = $this->db->get();
        $result = $q->result();

        return json_decode($result[0]->$fieldname);
        //echo $this->db->last_query(); exit;
        //return $q->result();
    }
    public function updatetabledata($table ='',$fields ='',$cond ='')
    {
      if($cond !=''){$this->db->where($cond);}
        // $this->db->where('whoto', $current_user);
        if ($this->db->update($table, $fields)) {
            return true;
        } else {
            return false;
        }
    }
    public function insertData($table ='',$datafields ='')
    {
       if ($table !='' && $datafields !='') {
        if($this->db->insert($table, $datafields))
        {
          return $this->db->insert_id();
        }
        else{
          return false;
        }
      }
    }
    public function updateData($table ='',$datafields ='',$cond ='')
    {
       if ($table !='' && $datafields !='') {
        $this->db->where($cond);
        $this->db->update($table, $datafields);
        return true;
      }
    }

}

?>
