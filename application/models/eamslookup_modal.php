<?php

class Eamslookup_modal extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    public function getEamsListing($start = 0,$limit = 10,$where = '',$orCond = '',$order = '')    {
        $select = array('eamsref','name','address1','address2','city','state','zip','phone');
        $this->db->select('eamsref,name,address1,address2,city,state,zip,phone', false);

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCond != '') {
            $this->db->or_where($orCond);
        }
        if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }
        $this->db->limit($limit, $start);
        $result = $this->db->get('card3');
        //echo $this->db->last_query();exit;
        return $result->result();
    }
    
    public function getEamsListingCount($where = '', $orCond = '') {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCond != '') {
            $this->db->or_where($orCond);
        }
        $this->db->from('card3 cd');
        return $this->db->count_all_results();
    }
    
    public function geteamsDetails($eamsref){
        $where = array('eamsref'=>$eamsref);
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        $result = $this->db->get('card3');
        return $result->result();
    }
    
    public function geteamsnumber($caseno,$cardCode){
        $where = array('caseno'=>$caseno,'cd.cardcode'=>$cardCode);
        $this->db->select('cd.firmcode,cd2.eamsref', false);
        $this->db->join('card cd', 'cd.firmcode = cd2.firmcode', 'LEFT');
        $this->db->join('casecard cc', 'cc.cardcode = cd.cardcode', 'LEFT');
        $this->db->where($where);
        $result = $this->db->get('card2 cd2');
        //echo $this->db->last_query(); exit;
        return $result->result();
    }
    
    public function delteeamsref($eamsref,$firmcode)
    {
        $cond = array('eamsref' =>$eamsref,'firmcode' =>$firmcode);
        $this->db->where($cond);
        $this->db->update('card2', array('eamsref' => ''));
        return $this->db->affected_rows();
    }
    
    public function updateeamsref($eamsref,$firmcode)
    {
        $cond = array('firmcode' =>$firmcode);
        $this->db->where($cond);
        $this->db->update('card2', array('eamsref' => $eamsref));
        return $this->db->affected_rows();
    }
}
