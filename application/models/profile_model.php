<?php

class Profile_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getprofile($initial)
    {
        $this->db->where('initials',$initial);
        $this->db->select('initials,fname,lname,title,username,password,emailid,profilepic,onoffsignature,signature');
        $res = $this->db->get('staff');
        //$this->db->last_query(); exit;
        return $res->result();
    }

    public function change_profile($filename)
    {
        //echo '<pre>'; print_r($this->session->userdata('user_data')); exit;
        $initial = $this->session->userdata('user_data')['initials'];
        $record = array('profilepic'=>$filename);
        $this->db->where('initials',$initial);
        $this->db->update('staff',$record);
        //return $res->result();
    }

    public function check_pass($cond)
    {
        $this->db->where($cond);
        $this->db->select('initials');
        $q = $this->db->get('staff');
        $this->db->last_query();
        return $result = $q->row_array();

    }

    public function change_pass($pass)
    {
        $initial = $this->session->userdata('user_data')['initials'];
        $record = array('password'=>$pass);
        $this->db->where('initials',$initial);
        $this->db->update('staff',$record);
        //echo $this->db->last_query(); exit;
    }

    public function updatesignature($signatureupdatedata ,$signaturestatus)
    {
        $initial = $this->session->userdata('user_data')['initials'];
        $record = array('signature'=>$signatureupdatedata,'onoffsignature'=>$signaturestatus);
        $this->db->where('initials',$initial);
        $res = $this->db->update('staff',$record);
        // echo $this->db->last_query(); exit;
         return $res;
    }

    public function getsignaturedata($initial)
    {
        $this->db->where('initials',$initial);
        $this->db->select('signature,onoffsignature');
        $res = $this->db->get('staff');
        if ($res->num_rows() > 0)
         {
            return $res->result();
            } 
        else 
         {
            return false;
         }
    }

    
}

