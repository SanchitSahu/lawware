<?php

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getLoginUsername() {
        $this->db->select('username');
        $q = $this->db->get('staff');
        return $q->result();
    }

    public function checkLoginUser($cond) {
        $this->db->where($cond);
        $this->db->select('initials,username,password,fname,title,lname,loggedin_id');
        $q = $this->db->get('staff');
        return $q->row_array();
    }

    public function updateActlinkUser($Actlink) {
        $record = array('actstr' => '', 'isActive' => 1);
        $this->db->where('actstr', $Actlink);
        $this->db->update('staff', $record);
    }

    public function checkActivationUser($cond, $cond2) {
        $this->db->where($cond);
        $this->db->where($cond2);
        $this->db->select('initials,username,fname,lname,title');
        $q = $this->db->get('staff');
        return $q->row_array();
    }

    public function addLogin($user) {
        $this->db->where('initials', $user);
        $this->db->select('*');
        $q = $this->db->get('staff2');

        $record = array('isloggedon' => 1, 'initials' => $user);
        if ($q->num_rows() > 0) {
            $this->db->where('initials', $user);
            $this->db->update('staff2', $record);
        } else {
            $this->db->insert('staff2', $record);
        }

        return true;
    }

    public function setLogout($user) {
        $record = array('isloggedon' => 0);
        $this->db->where('initials', $user);
        $this->db->delete('staff2');
        return $q;
    }

    public function addLogin_newsession($user, $unique_id) {
        $this->db->where('username', $user);
        $this->db->select('*');
        $q = $this->db->get('staff');

        $record = array('loggedin_id' => $unique_id);
        if ($q->num_rows() > 0) {
            $this->db->where('username', $user);
            $this->db->update('staff', array('loggedin_id' => $unique_id));
            return true;
        } else {
            return true;
        }
    }

    public function checkExist($cond) {
        $this->db->where($cond);
        $this->db->select('*');
        $q = $this->db->get('staff');
        return $q->row_array();
    }

    public function addUser($data, $files) {
        $newUser = array(
            "fname" => $data['fname'],
            "lname" => $data['lname'],
            "initials" => $data['initial'],
            "username" => $data['uname'],
            "password" => $data['pass'],
            "title" => $data['designation'],
            "emailid" => $data['email'],
        );

        $insert = $this->db->insert('staff', $newUser);
        if ($insert > 0) {
            $token = md5(uniqid(rand(), true));
            $to = $data['email'];
            $link = "<a target='_blank' href='" . base_url() . "login/index/$token'>Link</a>";
            $body = "Hello , " . $data['fname'] . " " . $data['lname'] . ",<br/>
                        Please click on link to activate your account : $link<br/>
                        Thanks";

            $respose_mail = $this->common_functions->reg_send($to, 'Registration email', 'notification', $body);

            $actData = array('actstr' => $token);
            $this->db->where('initials', $data['initial']);
            $this->db->update('staff', $actData);

            if ($_FILES['profile']['name'] != "") {
                $this->load->library('upload');
                $nwfile = explode(".", $_FILES["profile"]['name']);
                $ext = end($nwfile);
                if (!is_dir(FCPATH . 'assets/profileimages')) {
                    mkdir(FCPATH . 'assets/profileimages', 0777);
                }
                $config['upload_path'] = './assets/profileimages/';

                $new_name = time() . "." . $ext;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = $new_name;
                $config['max_size'] = '2048000';
                $config['max_width'] = '1024';
                $config['max_height'] = '768';
                $config['overwrite'] = false;

                //Initialize
                $this->upload->initialize($config);
                if (!$this->upload->do_upload("profile")) {
                    $this->upload->display_errors();
                    exit;
                } else {
                    $record = array('profilepic' => $new_name);
                    $this->db->where('initials', $data['initial']);
                    $this->db->update('staff', $record);
                }
            }
        }
        return true;
    }

    public function check_email($email) {
        $this->db->where('emailid', $email);
        $this->db->select('initials');
        $q = $this->db->get('staff');
        $result = $q->row_array();
        $cnt = count($result);
        if (!empty($cnt)) {
            $token = md5(uniqid(rand(), true));
            $to = $email;
            $initial = $result['initials'];
            $link = "<a target='_blank' href='" . base_url() . "login/resetpass/$token'>Link</a>";
            $body = "Hello $initial,<br>
                        Please click on Following link to reset your password :<br>
                        <h2>$link</h2><br>
                        <br>
                   Thanks,<br>".APPLICATION_NAME." Team";

            $this->common_functions->reg_send($to, 'Reset Password', $result['initials'], $body);

            $actData = array('fpassstr' => $token);
            $this->db->where('emailid', $email);
            $this->db->update('staff', $actData);
        }
        return $result;
    }

    public function check_fstr($fstr) {
        $this->db->where('fpassstr', $fstr);
        $this->db->select('initials');
        $q = $this->db->get('staff');
        return $q->row_array();
    }

    public function change_pass($fstr, $pass) {
        $record = array('password' => $pass, 'fpassstr' => '');
        $this->db->where('fpassstr', $fstr);
        $this->db->update('staff', $record);
    }

    public function check_emailexist($email) {
        $this->db->where('emailid', $email);
        $this->db->select('initials');
        $q = $this->db->get('staff');
        return $q->row_array();
    }

    public function getFirmData() {
        $this->db->select('firm_id,uniqueid');
        $q = $this->db->get('support_checklist');
        return $q->row_array();
    }

}
