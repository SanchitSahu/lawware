<?php

class Script_model extends CI_Model {

    private $caseact = '';
    private $injury = '';
    private $newDb;

    function __construct() {
        parent::__construct();

        $this->caseact = 'caseact_final';
        $this->injury = 'injury_final';
    }

    public function mergeCaseactQuery($folderNumber) {
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "";
        $config['database'] = "a1law_" . $folderNumber;
        $config['dbdriver'] = "mysqli";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $this->newDb = $this->load->database($config, TRUE);

        $tables = $this->newDb->list_tables();

        // echo "<pre>";print_r($tables);echo "</pre>";exit;

        if (!$this->newDb->table_exists($this->caseact)) {
            $this->newDb->query("CREATE TABLE $this->caseact (
                                `actno` int(11) DEFAULT NULL,
                                `caseno` int(11) DEFAULT NULL,
                                `initials0` varchar(3) DEFAULT NULL,
                                `initials` varchar(3) DEFAULT NULL,
                                `date` datetime DEFAULT NULL,
                                `event` longtext,
                                `eventrtf` longtext,
                                `title` decimal(2,0) DEFAULT NULL,
                                `atty` varchar(3) DEFAULT NULL,
                                `cost` decimal(10,2) DEFAULT NULL,
                                `minutes` int(11) DEFAULT NULL,
                                `rate` int(11) DEFAULT NULL,
                                `arap` decimal(1,0) DEFAULT NULL,
                                `type` varchar(15) DEFAULT NULL,
                                `mainnotes` bit(1) DEFAULT NULL,
                                `access` decimal(1,0) DEFAULT NULL,
                                `redalert` bit(1) DEFAULT NULL,
                                `upontop` bit(1) DEFAULT NULL,
                                `category` int(11) DEFAULT NULL,
                                `color` decimal(2,0) DEFAULT NULL,
                                `tbolenotes` varchar(1) DEFAULT NULL,
                                `gfole1` longblob,
                                `ole2` int(11) DEFAULT NULL,
                                `ole3` longtext,
                                `ole3style` decimal(2,0) DEFAULT NULL,
                                `wordstyle` decimal(2,0) DEFAULT NULL,
                                `frmwidth` decimal(4,2) DEFAULT NULL,
                                `frmheight` decimal(4,2) DEFAULT NULL,
                                `frmtm` decimal(4,2) DEFAULT NULL,
                                `frmbm` decimal(4,2) DEFAULT NULL,
                                `frmlm` decimal(4,2) DEFAULT NULL,
                                `frmrm` decimal(4,2) DEFAULT NULL,
                                `orient` decimal(1,0) DEFAULT NULL,
                                `copies` decimal(2,0) DEFAULT NULL,
                                `fld1` longtext,
                                `fld2` longtext,
                                `fld3` longtext,
                                `fld4` longtext,
                                `fld5` longtext,
                                `fld6` longtext,
                                `fld7` longtext,
                                `fld8` longtext,
                                `fldmany` longtext,
                                `cardcode` int(11) DEFAULT NULL,
                                `typeact` decimal(1,0) DEFAULT NULL,
                                `bistyle` decimal(2,0) DEFAULT NULL,
                                `biteamno` int(11) DEFAULT NULL,
                                `biowner` varchar(3) DEFAULT NULL,
                                `bicycle` datetime DEFAULT NULL,
                                `bitime` int(11) DEFAULT NULL,
                                `bihourrate` int(11) DEFAULT NULL,
                                `bifee` decimal(9,2) DEFAULT NULL,
                                `bicost` decimal(9,2) DEFAULT NULL,
                                `bidesc` longtext,
                                `bicheckno` varchar(12) DEFAULT NULL,
                                `bipmt` decimal(9,2) DEFAULT NULL,
                                `bipmtdate` datetime DEFAULT NULL,
                                `bipmtduedt` datetime DEFAULT NULL,
                                `bilatefee` decimal(9,2) DEFAULT NULL,
                                `oldno` int(11) DEFAULT NULL
                              ) ENGINE=InnoDB DEFAULT CHARSET=latin1");

            $this->newDb->query("ALTER TABLE $this->caseact ADD KEY `caseno` (`caseno`),ADD KEY `initials0` (`initials0`),ADD KEY `initials` (`initials`)");
        }

        $query = $this->newDb->query("select * from $this->caseact");
        $field_array = $query->list_fields();

        $tableArray = array();
        $html = '';
        foreach ($tables as $tablename) {
            if ($tablename != "caseact_final") {
                $html .= "</br>";
                $html .=  "======================================================";
                $html .=  "</br>";
                $html .=  "Table name is : " . $tablename;
                $html .=  "</br>";
                $query = $this->newDb->query("select * from $tablename");
                $dynamicfield_array = $query->list_fields();

                $data = "";
                foreach ($field_array as $field) {
                    if (in_array($field, $dynamicfield_array)) {
                        $data .= $field . ", ";
                    }
                }
                $data = rtrim($data, ", ");

                $html .=  "</br>";
                if ($this->newDb->query("INSERT INTO $this->caseact ($data) SELECT $data FROM " . $tablename)) {
                    $html .=  'Copied ' . $tablename;
                    //array_push($tableArray,'Copied '.$tablename);
                } else {
                    $html .=  'Not Copied ' . $tablename;
                    //array_push($tableArray,'Not Copied '.$tablename);
                }
                $html .=  "</br>";
                $html .=  "======================================================";
            }
        }
        echo $html;
        // return $tableArray;
    }

    public function mergeInjuryQuery() {
        $tables = $this->newDb->list_tables();

        if (!$this->newDb->table_exists($this->injury)) {
            $this->newDb->query("CREATE TABLE $this->injury (
            `caseno` int(11) DEFAULT NULL,
            `orderno` int(11) DEFAULT NULL,
            `amended` varchar(1) DEFAULT NULL,
            `plate1name` varchar(1) DEFAULT NULL,
            `plate2name` varchar(1) DEFAULT NULL,
            `plate3name` varchar(1) DEFAULT NULL,
            `plate4name` varchar(1) DEFAULT NULL,
            `doctors` varchar(80) DEFAULT NULL,
            `app_status` varchar(6) DEFAULT NULL,
            `dor_date` datetime DEFAULT NULL,
            `salutation` varchar(10) DEFAULT NULL,
            `first` varchar(30) DEFAULT NULL,
            `last` varchar(30) DEFAULT NULL,
            `address` varchar(50) DEFAULT NULL,
            `city` varchar(30) DEFAULT NULL,
            `state` varchar(5) DEFAULT NULL,
            `zip_code` varchar(10) DEFAULT NULL,
            `social_sec` varchar(15) DEFAULT NULL,
            `e_name` varchar(60) DEFAULT NULL,
            `e_address` varchar(50) DEFAULT NULL,
            `e_city` varchar(30) DEFAULT NULL,
            `e_state` varchar(5) DEFAULT NULL,
            `e_zip` varchar(10) DEFAULT NULL,
            `e_phone` varchar(25) DEFAULT NULL,
            `e_fax` varchar(25) DEFAULT NULL,
            `e2_name` varchar(60) DEFAULT NULL,
            `e2_address` varchar(50) DEFAULT NULL,
            `e2_city` varchar(30) DEFAULT NULL,
            `e2_state` varchar(5) DEFAULT NULL,
            `e2_zip` varchar(10) DEFAULT NULL,
            `e2_phone` varchar(25) DEFAULT NULL,
            `e2_fax` varchar(25) DEFAULT NULL,
            `i_name` varchar(60) DEFAULT NULL,
            `i_address` varchar(50) DEFAULT NULL,
            `i_city` varchar(30) DEFAULT NULL,
            `i_state` varchar(5) DEFAULT NULL,
            `i_zip` varchar(10) DEFAULT NULL,
            `i_phone` varchar(25) DEFAULT NULL,
            `i_fax` varchar(25) DEFAULT NULL,
            `i_adjsal` varchar(5) DEFAULT NULL,
            `i_adjfst` varchar(15) DEFAULT NULL,
            `i_adjuster` varchar(40) DEFAULT NULL,
            `i_claimno` varchar(25) DEFAULT NULL,
            `i2_name` varchar(60) DEFAULT NULL,
            `i2_address` varchar(50) DEFAULT NULL,
            `i2_city` varchar(30) DEFAULT NULL,
            `i2_state` varchar(5) DEFAULT NULL,
            `i2_zip` varchar(10) DEFAULT NULL,
            `i2_phone` varchar(25) DEFAULT NULL,
            `i2_fax` varchar(25) DEFAULT NULL,
            `i2_adjsal` varchar(5) DEFAULT NULL,
            `i2_adjfst` varchar(15) DEFAULT NULL,
            `i2_adjuste` varchar(40) DEFAULT NULL,
            `i2_claimno` varchar(25) DEFAULT NULL,
            `case_no` varchar(20) DEFAULT NULL,
            `eamsno` varchar(20) DEFAULT NULL,
            `adj1a` datetime DEFAULT NULL,
            `adj1b` varchar(50) DEFAULT NULL,
            `ct1` bit(1) DEFAULT NULL,
            `doi` datetime DEFAULT NULL,
            `doi2` datetime DEFAULT NULL,
            `adj1c` varchar(25) DEFAULT NULL,
            `adj1d` varchar(60) DEFAULT NULL,
            `adj1d2` varchar(25) DEFAULT NULL,
            `adj1d3` varchar(2) DEFAULT NULL,
            `adj1d4` varchar(5) DEFAULT NULL,
            `adj1e` varchar(250) DEFAULT NULL,
            `pob1` varchar(5) DEFAULT NULL,
            `pob2` varchar(5) DEFAULT NULL,
            `pob3` varchar(5) DEFAULT NULL,
            `pob4` varchar(5) DEFAULT NULL,
            `pob5` varchar(5) DEFAULT NULL,
            `adj2a` longtext,
            `adj3a` varchar(60) DEFAULT NULL,
            `adj4a` varchar(60) DEFAULT NULL,
            `adj5a` varchar(1) DEFAULT NULL,
            `adj5b` varchar(12) DEFAULT NULL,
            `adj5c` varchar(12) DEFAULT NULL,
            `adj5d` varchar(13) DEFAULT NULL,
            `adj6a` varchar(1) DEFAULT NULL,
            `adj7a` varchar(1) DEFAULT NULL,
            `adj7b` varchar(50) DEFAULT NULL,
            `adj7c` varchar(1) DEFAULT NULL,
            `adj7d` varchar(60) DEFAULT NULL,
            `adj7e` varchar(1) DEFAULT NULL,
            `adj8a` varchar(80) DEFAULT NULL,
            `adj9a` varchar(1) DEFAULT NULL,
            `adj9b` varchar(1) DEFAULT NULL,
            `adj9c` varchar(1) DEFAULT NULL,
            `adj9d` varchar(1) DEFAULT NULL,
            `adj9e` varchar(1) DEFAULT NULL,
            `adj9f` varchar(1) DEFAULT NULL,
            `adj9g` varchar(60) DEFAULT NULL,
            `adj10a` datetime DEFAULT NULL,
            `adj10city` varchar(20) DEFAULT NULL,
            `firmatty1` varchar(30) DEFAULT NULL,
            `d1_firm` varchar(60) DEFAULT NULL,
            `d1_first` varchar(30) DEFAULT NULL,
            `d1_last` varchar(30) DEFAULT NULL,
            `d1_address` varchar(50) DEFAULT NULL,
            `d1_city` varchar(30) DEFAULT NULL,
            `d1_state` varchar(5) DEFAULT NULL,
            `d1_zip` varchar(10) DEFAULT NULL,
            `d1_phone` varchar(25) DEFAULT NULL,
            `d1_fax` varchar(25) DEFAULT NULL,
            `d2_firm` varchar(60) DEFAULT NULL,
            `d2_first` varchar(30) DEFAULT NULL,
            `d2_last` varchar(30) DEFAULT NULL,
            `d2_address` varchar(50) DEFAULT NULL,
            `d2_city` varchar(30) DEFAULT NULL,
            `d2_state` varchar(5) DEFAULT NULL,
            `d2_zip` varchar(10) DEFAULT NULL,
            `d2_phone` varchar(25) DEFAULT NULL,
            `d2_fax` varchar(25) DEFAULT NULL,
            `office` varchar(20) DEFAULT NULL,
            `s_w` datetime DEFAULT NULL,
            `other_sol` datetime DEFAULT NULL,
            `follow_up` datetime DEFAULT NULL,
            `liab` datetime DEFAULT NULL,
            `empno` int(11) DEFAULT NULL,
            `empno2` int(11) DEFAULT NULL,
            `insno` int(11) DEFAULT NULL,
            `insno2` int(11) DEFAULT NULL,
            `atty1` int(11) DEFAULT NULL,
            `atty2` int(11) DEFAULT NULL,
            `fld1` longtext,
            `fld2` longtext,
            `fld3` longtext,
            `fld4` longtext,
            `fld5` longtext,
            `fld6` longtext,
            `fld7` longtext,
            `fld8` longtext,
            `fld9` longtext,
            `fld10` longtext,
            `fld11` longtext,
            `fld12` longtext,
            `fld13` longtext,
            `fld14` longtext,
            `fld15` longtext,
            `fld16` longtext,
            `fld17` longtext,
            `fld18` longtext,
            `fld19` longtext,
            `fld20` longtext,
            `status2` varchar(40) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        }

        $query = $this->newDb->query("select * from $this->injury");
        $field_array = $query->list_fields();

        $tableArray = array();

        foreach ($tables as $tablename) {
            if (strpos($tablename, 'injury_') !== false) {
                echo "</br>";
                echo "======================================================";
                echo "</br>";
                echo "Table name is : " . $tablename;
                echo "</br>";

                $query = $this->newDb->query("select * from $tablename");
                $dynamicfield_array = $query->list_fields();

                $data = "";
                foreach ($field_array as $field) {
                    if (in_array($field, $dynamicfield_array)) {
                        $data .= $field . ", ";
                    }
                }
                $data = rtrim($data, ", ");

                if ($this->newDb->query("INSERT INTO $this->injury ($data) SELECT $data FROM " . $tablename)) {
                    echo 'Copied ' . $tablename;
                    //array_push($tableArray,'Copied '.$tablename);
                } else {
                    echo 'Not Copied ' . $tablename;
                    //array_push($tableArray,'Not Copied '.$tablename);
                }
                echo "</br>";
                echo "======================================================";
            }
        }

        return $tableArray;
    }

    public function removeTable($from, $to = '') {

        if ($to != '') {
            for ($i = $from + 1; $i <= $to; $i++) {
                $tablename = 'injury_' . $i;
                $res = $this->newDb->query('DROP TABLE IF EXISTS ' . $tablename);

                if ($res) {
                    echo $tablename . " Deleted";
                } else {
                    echo $tablename . " Not Deleted";
                }
                /* if($this->newDb->table_exists($tablename))
                  {
                  echo 'Table '.$tablename.' Exists';
                  }else{
                  echo 'Table '.$tablename.' Not Exists';
                  } */
                echo "</br>";
            }
        }
    }


    public function mergeCaseactSingleTable() {
        error_reporting(E_ALL);
        ini_set("display_errors","ON");
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "";
        $config['database'] = "remainging_files";
        $config['dbdriver'] = "mysqli";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $this->newDb = $this->load->database($config, TRUE);

        $tables = $this->newDb->list_tables();

        // $query = $this->newDb->query("select * from a1law_final.injury");
        // $field_array = $query->list_fields();

        $field_array = $this->newDb->list_fields("a1law_final.caseact");

        // echo "<pre>";print_r($field_array);echo "</pre>";
        // echo "<pre>";print_r($field_arraynew);echo "</pre>";exit;
        // echo "<pre>";print_r($tables);echo "</pre>";exit;

        $tableArray = array();
        $html = '';
        foreach ($tables as $key => $tablename) {
            // if($key > 3993 && $key <= 4000) {
                if (strpos($tablename, 'caseact') !== false) {
                    $html .= "</br>";
                    $html .=  "======================================================";
                    $html .=  "</br>";
                    $html .=  "Table name is : " . $tablename;
                    $html .=  "</br>";


                    // $query = $this->newDb->query("select * from $tablename");
                    // $dynamicfield_array = $query->list_fields();

                    $dynamicfield_array = $this->newDb->list_fields($tablename);

                    $data = "";
                    foreach ($field_array as $field) {
                        if (in_array($field, $dynamicfield_array)) {
                            $data .= $field . ", ";
                        }
                    }
                    $data = rtrim($data, ", ");

                    if ($this->newDb->query("INSERT INTO a1law_final.caseact ($data) SELECT $data FROM " . $tablename)) {
                        echo 'Copied ' . $tablename;
                        //array_push($tableArray,'Copied '.$tablename);
                    } else {
                        echo 'Not Copied ' . $tablename;
                        //array_push($tableArray,'Not Copied '.$tablename);
                    }


                    
                    
                    // if ($this->newDb->query("INSERT INTO a1law_final.injury SELECT * FROM " . $tablename)) {
                    //     $html .=  'Copied ' . $tablename;
                    //     //array_push($tableArray,'Copied '.$tablename);
                    // } else {
                    //     $html .=  'Not Copied ' . $tablename;
                    //     //array_push($tableArray,'Not Copied '.$tablename);
                    // }
                    $html .=  "</br>";
                    $html .=  "======================================================";
                }
            // }
            // echo $html;
            // exit;
        }
        echo $html;
        // return $tableArray;
    }

    public function mergeInjurySingleTable() {
        error_reporting(E_ALL);
        ini_set("display_errors","ON");
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "";
        $config['database'] = "injury_8000";
        $config['dbdriver'] = "mysqli";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $this->newDb = $this->load->database($config, TRUE);

        $tables = $this->newDb->list_tables();

        $query = $this->newDb->query("select * from a1law_final.injury");
        $field_array = $query->list_fields();

        $field_array = $this->newDb->list_fields("a1law_final.injury");

        // echo "<pre>";print_r($field_array);echo "</pre>";
        // echo "<pre>";print_r($field_arraynew);echo "</pre>";exit;
        // echo "<pre>";print_r($tables);echo "</pre>";exit;

        $tableArray = array();
        $html = '';
        foreach ($tables as $key => $tablename) {
            // if($key > 3993 && $key <= 4000) {
                if (strpos($tablename, 'injury') !== false) {
                    $html .= "</br>";
                    $html .=  "======================================================";
                    $html .=  "</br>";
                    $html .=  "Table name is : " . $tablename;
                    $html .=  "</br>";


                    $query = $this->newDb->query("select * from $tablename");
                    $dynamicfield_array = $query->list_fields();

                    $dynamicfield_array = $this->newDb->list_fields($tablename);

                    $data = "";
                    foreach ($field_array as $field) {
                        if (in_array($field, $dynamicfield_array)) {
                            $data .= $field . ", ";
                        }
                    }
                    $data = rtrim($data, ", ");

                    if ($this->newDb->query("INSERT INTO a1law_final.injury ($data) SELECT $data FROM " . $tablename)) {
                        $html .= 'Copied ' . $tablename;
                        //array_push($tableArray,'Copied '.$tablename);
                    } else {
                        $html .= 'Not Copied ' . $tablename;
                        //array_push($tableArray,'Not Copied '.$tablename);
                    }


                    
                    
                    // if ($this->newDb->query("INSERT INTO a1law_final.injury SELECT * FROM " . $tablename)) {
                    //     $html .=  'Copied ' . $tablename;
                    //     //array_push($tableArray,'Copied '.$tablename);
                    // } else {
                    //     $html .=  'Not Copied ' . $tablename;
                    //     //array_push($tableArray,'Not Copied '.$tablename);
                    // }

                    $html .=  "</br>";
                    $html .=  "======================================================";
                }
            // }
            // echo $html;
            // exit;
        }
        echo $html;
        // return $tableArray;
    }

    public function createEmailConvIdForSingleMail() {
        error_reporting(E_ALL);
        ini_set("display_errors","ON");
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "";
        $config['database'] = "a1law_final";
        $config['dbdriver'] = "mysqli";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $this->newDb = $this->load->database($config, TRUE);

        $tables = $this->newDb->list_tables();

        $query = $this->newDb->query("select email.filename from email where isnull(email_con_id)");
        /*$field_array = $query->list_fields();*/
        $result = $query->result();

        $email_con_id = 1;
        foreach ($result as $key => $value) {
            $new_q = $this->newDb->query("update email set email_con_id = ".$value->filename." where filename = ".$value->filename);
            $email_con_id++;
        }

        $res = $this->newDb->query('select * from email')->result();
        echo "<pre>";print_r($res);
    }

    public function createEmailConversionEntries() {
        error_reporting(E_ALL);
        ini_set("display_errors","ON");
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "";
        $config['database'] = "a1law_final";
        $config['dbdriver'] = "mysqli";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";

        $this->newDb = $this->load->database($config, TRUE);

        $tables = $this->newDb->list_tables();

        $query = $this->newDb->query("select email_con_id, subject, email.datecreate, GROUP_CONCAT(email2.whoto) AS whoto, email.whofrom from email left join email2 on email.filename = email2.filename group by email.email_con_id");
        /*$field_array = $query->list_fields();*/
        $result = $query->result();
        
        foreach ($result as $key => $value) {
            $new_q[$key] = $this->newDb->query("insert into email_conversion (email_con_id, email_con_subject, email_con_date, email_con_people,  email_con_initiate_by) values ('".$value->email_con_id."', '".htmlspecialchars($value->subject, ENT_QUOTES, 'UTF-8')."', '".$value->datecreate."', '".$value->whoto.",".$value->whofrom."', '".$value->whofrom."')");
        }

        $res = $this->newDb->query('select * from email_conversion')->result();
        echo "<pre>";print_r($res);
    }
    
    public function GrabExistingEmails() {
        error_reporting(E_ALL);
        ini_set("display_errors","ON");
        $config['hostname'] = "localhost";
        $config['username'] = "root";
        $config['password'] = "";
        $config['database'] = "a1law_email";
        $config['dbdriver'] = "mysqli";
        $config['dbprefix'] = "";
        $config['pconnect'] = FALSE;
        $config['db_debug'] = TRUE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = "";
        $config['char_set'] = "utf8";
        $config['dbcollat'] = "utf8_general_ci";
        
        $this->newDb = $this->load->database($config, TRUE);
        $tables = $this->newDb->list_tables();
        
        
        foreach($tables as $table){
            $query = $this->newDb->query("select * from a1law_email.$table");
            $result = $query->result();
            //echo '<pre>'; print_r($result); exit;
            foreach($result as $key=>$value){
                
                //echo '<br/>'.$value->datecreate;
                $new_q[$key] = $this->newDb->query("insert into a1law.email (datecreate, whofrom, subject,  caseno,urgent,mail_body) values ('".$value->datecreate."','".$value->whofrom."', '".htmlspecialchars($value->subject, ENT_QUOTES, 'UTF-8')."', '".$value->caseno."', '".$value->priority."','".htmlspecialchars($value->body, ENT_QUOTES, 'UTF-8')."')");
                
                $filename = $this->newDb->insert_id();
                
                if($value->foldername == 'Deleted'){
                    $detached = 1;
                    $old = 0;
                }
                else if($value->foldername == 'Old Mail'){
                    $old = 1;
                    $detached = 0;
                }
                
                if($value->readyet == 1){
                    $readyet = 'Y';
                }else{
                    $readyet = '';
                }
                
                $whotoall = explode(',',$value->whoto);
                foreach($whotoall as $whoto){
                    $query2 = $this->newDb->query("select * from a1law.email2 where whoto='".$whoto."' AND datesent='".$value->datesent."' AND mail_body='".htmlspecialchars($value->body, ENT_QUOTES, 'UTF-8')."'");
                    $result2 = $query2->result();
                    if(count($result2) == 0){
                        $new_qr[$key] = $this->newDb->query("insert into a1law.email2 (filename, whoto, readyet,datesent,dateread,detached,mail_body,oldmail) values ($filename,'".$whoto."','".$readyet."','".$value->datesent."', '".$value->dateread."', '".$detached."','".htmlspecialchars($value->body, ENT_QUOTES, 'UTF-8')."','".$value->$old."')");
                    }
                }
            }
        }
        //echo '<pre>'; print_r($result);exit;
    }

    public function createEmailTableForNewUser($staff_listing)
    {
        $tables = $this->db->list_tables();
        echo "<pre>";
        // print_r($tables);
        // echo "</pre>";
        // exit;
        foreach ($staff_listing as $users) {
            if (!in_array(strtolower($users->initials) . '_mymail', $tables)) {
                $query = "CREATE TABLE " . strtolower($users->initials) . "_mymail (
                    `parcelid` VARCHAR(15) DEFAULT NULL,
                    `folderid` INT(11) DEFAULT NULL,
                    `foldername` VARCHAR(25) DEFAULT NULL,
                    `priority` VARCHAR(1) DEFAULT NULL,
                    `emailtype` INT(11) DEFAULT NULL,
                    `subject` VARCHAR(80) DEFAULT NULL,
                    `body` LONGTEXT,
                    `caseno` INT(11) DEFAULT NULL,
                    `catno` INT(11) DEFAULT NULL,
                    `datecreate` DATETIME DEFAULT NULL,
                    `datesent` DATETIME DEFAULT NULL,
                    `dateread` DATETIME DEFAULT NULL,
                    `readyet` BIT(1) DEFAULT NULL,
                    `whofrom` VARCHAR(5) DEFAULT NULL,
                    `whoto` LONGTEXT,
                    `whotoexternal` LONGTEXT,
                    `cc` LONGTEXT,
                    `bcc` LONGTEXT,
                    `encrypt1` INT(11) DEFAULT NULL,
                    `password` VARCHAR(10) DEFAULT NULL
                  ) ENGINE=INNODB DEFAULT CHARSET=latin1";
                $result = $this->db->query($query);
                if ($result) {
                    echo strtolower($users->initials) . '_mymail' . "true<br><br>";
                } else {
                    echo strtolower($users->initials) . '_mymail' . "false<br><br>";
                }
                // echo $query;
                // exit;
            }
        }
    }

    public function alterEmailRelatedTables($staff_listing)
    {
        // $skip_staff = array('AAZ','ABH','AEC','ALN','ANI','ARN','AWS','AYH','BCH','CKB','DAR','DJG','EMM','EOM', 'GST','INT','IRA','ISK','JDR','JJB','JLS','JMD','JPL', 'KCH','KKD','LAB','LAH','LNC','MDH','MDT','MHA','MSF','NAR','NRS','RAC','SAM','SMC','TCC','TCF', 'TGE','TRG','VGO');
        foreach ($staff_listing as $users) {
            // if(!in_array($users->initials, $skip_staff)) {
                echo '<br/>'. $users->initials;
                 //$this->db->query('ALTER TABLE ' . strtolower($users->initials) . '_mymail   
                // ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT FIRST, 
                // ADD PRIMARY KEY (`id`);');

                /*$this->db->query('ALTER TABLE ' . strtolower($users->initials) . '_mymail
                CHANGE `readyet` `readyet` VARCHAR(1) CHARSET latin1 COLLATE latin1_swedish_ci NOT NULL;');

                $this->db->query("UPDATE " . strtolower($users->initials) . "_mymail SET readyet = 'Y' WHERE readyet = 1;");

                $this->db->query('ALTER TABLE ' . strtolower($users->initials) . '_mymail
                CHANGE `whofrom` `whofrom` VARCHAR(100) CHARSET latin1 COLLATE latin1_swedish_ci NULL;');

                $this->db->query('ALTER TABLE `caseact`
                CHANGE `email_con_id` `email_con_id` VARCHAR(100) NULL;');*/

                 //$this->db->query('ALTER TABLE ' . strtolower($users->initials) . '_mymail  
                 //ADD COLUMN `whotoexternal` LONGTEXT NULL AFTER `whoto`;');
            // }

            // exit;
            
        }
    }

}
