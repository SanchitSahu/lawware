<?php

/**
 *
 */
class Calendar_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getCalendarDataByEvent($id) {

        $this->db->where('cal1.eventno', $id);
        $this->db->select('cal1.eventno,cal1.caseno,cal1.initials0,cal1.initials,cal1.calstat,DATE(cal1.date) AS caldate,TIME(cal1.date) AS caltime,cal1.tickle,DATE(cal1.tickledate) AS tickdate,TIME(cal1.tickledate) AS ticktime,cal1.whofrom,cal1.whoto,cal1.tottime,cal1.event,cal1.first,cal1.last,cal1.defendant,cal1.venue,cal1.judge,cal1.attyass,cal1.location,cal1.notes,cal1.chgwho,DATE(cal1.chglast) AS chgdate,TIME(cal1.chglast) AS chgtime,cal1.rollover,cal1.protected,cal1.color,cal1.todo,cal2.initials as atty_hand,cal1.reminder,cal1.doctor,cal1.tevent');
        $this->db->join('cal2', 'cal1.eventno = cal2.eventno', 'left');
        $result = $this->db->get('cal1');
        //echo $this->db->last_query(); exit;
        return $result->row();
    }

    public function getFilterArray() {

        $filterArray = array();

        $this->load->library('common_functions');

//        $popattyh = $this->common_functions->getSingleRecordArray('system', 'popattyh');
//
//        $filterArray['popattyh'] = array();
//
//        $popattyh = explode(",", $popattyh['popattyh']);
//        foreach($popattyh as $key=>$val){
//            if($val != ''){
//                array_push($filterArray['popattyh'], $val);
//            }
//        }
        //$ccolor = $this->common_functions->getSingleRecordArray('system', 'ccolors');
        //$ccolor = preg_split('/[\s]+/', $ccolor['ccolors']);
        //$ccolor = explode(', $ccolor['ccolors']);
//        echo "<pre>";
//        print_r($ccolor);exit;

        $filterArray['popattyh'] = array('RLG', 'JLB', 'DSR', 'RJV', 'GJL', 'EGZ', 'DSP', 'MAZ', 'TLC');
        $filterArray['appointmentType'] = array('Normal', 'Tickler', 'Alert');
        $filterArray['ccolor'] = array('1' => 'Black', '2' => 'Yellow Highlight', '3' => 'Black Highlight', '4' => 'Red', '5' => 'Red Highlight', '6' => 'Green', '7' => 'Green Highlight', '8' => 'Blue', '9' => 'Blue Highlight', '10' => 'Pink Highlight', '15' => 'Purple Highlight');
        $filterArray['category'] = array('1' => 'Appearances', '2' => 'In Office Appearances', '3' => 'Employee Attendance', '4' => 'Intake', '5' => 'rlg calendar');

        return $filterArray;
    }

    public function getTodayEvent($start = 0, $limit = 10, $order = '', $count = 0, $singleFilter = '') {
        $this->db->where('cal1.attyass', $this->session->userdata('user_data')['initials']);
        //$this->db->where('DATE(cal1.date)','2016-02-22');
        $this->db->where('DATE(cal1.date)', date('Y-m-d'));
        //$start_date = date('Y-m-d 00:00:00');
        //$end_date = date('Y-m-d 00:00:00',strtotime("+1 days"));
        //$this->db->where('DATE(cal1.date) >= ', $start_date);
        //$this->db->where('DATE(cal1.date) <= ', $end_date);
        $this->db->select('cal1.eventno,cal1.caseno,cal1.initials0,cal1.initials,cal1.calstat,DATE(cal1.date) AS caldate,TIME(cal1.date) AS caltime,cal1.tickle,DATE(cal1.tickledate) AS tickdate,TIME(cal1.tickledate) AS ticktime,cal1.whofrom,cal1.whoto,cal1.tottime,cal1.event,cal1.first,cal1.last,cal1.defendant,cal1.venue,cal1.judge,cal1.attyass,cal1.location,cal1.notes,cal1.chgwho,DATE(cal1.chglast) AS chgdate,TIME(cal1.chglast) AS chgtime,cal1.todo,cal2.initials as atty_hand');
        $this->db->join('cal2', 'cal1.eventno = cal2.eventno', 'left');
        $select = array('caltime', 'atty_hand', 'attyass', 'event', 'initials', 'venue', 'judge');
        if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        //echo $this->db->last_query();exit;
        if ($count == 0) {
            $this->db->limit($limit, $start);
        }
        $this->db->order_by('cal1.date', ' ASC');
        $result = $this->db->get('cal1');
        //echo $this->db->last_query();exit;
        if ($count > 0) {
            return $result->num_rows();
        } else {
            return $result->result();
        }

        // return $result->result();
    }

    public function getrecenteventadmin() {
        $this->db->limit(5);
        $this->db->select('cal1.eventno,cal1.caseno,cal1.initials0,cal1.initials,cal1.calstat,DATE(cal1.date) AS caldate,TIME(cal1.date) AS caltime,cal1.tickle,DATE(cal1.tickledate) AS tickdate,TIME(cal1.tickledate) AS ticktime,cal1.whofrom,cal1.whoto,cal1.tottime,cal1.event,cal1.first,cal1.last,cal1.defendant,cal1.venue,cal1.judge,cal1.attyass,cal1.location,cal1.notes,cal1.chgwho,DATE(cal1.chglast) AS chgdate,TIME(cal1.chglast) AS chgtime,cal1.todo,cal2.initials as atty_hand');
        $this->db->join('cal2', 'cal1.eventno = cal2.eventno', 'left');
        $result = $this->db->get('cal1');
        return $result->result();
    }

    public function checkevent($attyass, $nwcaldate, $tododata) {

        //$this->db->where('DATE',date('Y-m-d H:i:s', strtotime($nwcaldate)));
        /* The below development is for static 60 minutes. */
        /*$following = date('Y-m-d H:i:s', strtotime('+60 minutes', strtotime($nwcaldate)));
        $before = date('Y-m-d H:i:s', strtotime('-60 minutes', strtotime($nwcaldate)));*/

        /* The below development is for dynamic time conflict. */
        $conflict_time_range_minutes = $this->db->query("SELECT caldeflt FROM system WHERE system_id = 1");
        $conflict_minutes = json_decode($conflict_time_range_minutes->result()[0]->caldeflt)->tranforcon;
        $following = date('Y-m-d H:i:s', strtotime('+'.$conflict_minutes.' minutes', strtotime($nwcaldate)));
        $before = date('Y-m-d H:i:s', strtotime('-'.$conflict_minutes.' minutes', strtotime($nwcaldate)));

        $this->db->where('DATE >=', $before);
        $this->db->where('DATE <=', $following);
		$this->db->where('cal1.todo', $tododata);
		$this->db->where('cal1.attyass', $attyass);
        $this->db->select('cal1.eventno');
        $this->db->join('cal2', 'cal1.eventno = cal2.eventno', 'left');
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->order_by('cal1.date', ' ASC');
        $result = $this->db->get('cal1');
        //echo $this->db->last_query();exit;
        if ($count > 0) {
            return $result->num_rows();
        } else {
            return $result->result();
        }

        // return $result->result();
    }

    public function checkeventedit($attyass, $nwcaldate, $eventno, $tododata) {

        //$this->db->where('DATE',date('Y-m-d H:i:s', strtotime($nwcaldate)));
        /* The below development is for static 60 minutes. */
        /*$following = date('Y-m-d H:i:s', strtotime('+60 minutes', strtotime($nwcaldate)));
        $before = date('Y-m-d H:i:s', strtotime('-60 minutes', strtotime($nwcaldate)));*/

        /* The below development is for dynamic time conflict. */
        $conflict_time_range_minutes = $this->db->query("SELECT caldeflt FROM system WHERE system_id = 1");
        $conflict_minutes = json_decode($conflict_time_range_minutes->result()[0]->caldeflt)->tranforcon;
        $following = date('Y-m-d H:i:s', strtotime('+'.$conflict_minutes.' minutes', strtotime($nwcaldate)));
        $before = date('Y-m-d H:i:s', strtotime('-'.$conflict_minutes.' minutes', strtotime($nwcaldate)));

        $this->db->where('DATE >=', $before);
        $this->db->where('DATE <=', $following);
        $this->db->where('cal1.eventno !=', $eventno);
	$this->db->where('cal1.todo', $tododata);
	$this->db->where('cal1.attyass', $attyass);
        $this->db->select('cal1.eventno');
        $this->db->join('cal2', 'cal1.eventno = cal2.eventno', 'left');
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->order_by('cal1.date', ' ASC');
        $result = $this->db->get('cal1');
        //echo $this->db->last_query();exit;
        if ($count > 0) {
            return $result->num_rows();
        } else {
            return $result->result();
        }

        // return $result->result();
    }

    public function getAjaxCalendarListing($caseno, $caltyp, $start = 0, $limit = 10, $where = '', $order = '', $dir = '', $singleFilter = '') {
        $cond = 'orderno ASC';

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($order != '' && $dir != '') {
            $this->db->order_by($order, $dir);
        } else {
            $this->db->order_by($cond);
        }

        if (!empty($caseno)) {
            $this->db->where('caseno', $caseno);
        }
        $this->db->where('c1.todo', $caltyp);

        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }

        $this->db->select('c1.*,c2.initials as intl');
        $this->db->from('cal1 c1');
        $this->db->join('cal2 c2', 'c1.eventno = c2.eventno', 'left');
        // $this->db->group_by('c1.eventno');
        $q = $this->db->get();
        // echo $this->db->last_query();
        // log_message('error', "CALENDAR :: Query :: getAjaxCalendarListing :: \n" . $this->db->last_query());
        // log_message('error', "CALENDAR :: Query Result1:: getAjaxCalendarListing :: \n" . serialize($q->result()));
        return $q->result();
    }

    public function getAjaxCalendarListingCount($caseno, $caltyp, $where = '', $singleFilter = '') {
        $cond = 'orderno ASC';

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->where('c1.todo', $caltyp);

        if (!empty($caseno)) {
            $this->db->where('caseno', $caseno);
        }

        $this->db->select('c1.*,c2.initials as intl');
        $this->db->from('cal1 c1');
        $this->db->join('cal2 c2', 'c1.eventno = c2.eventno', 'left');
//        $this->db->group_by('c1.eventno');
        $q = $this->db->count_all_results();
//        print_r($q);
    //    echo $this->db->last_query();
//        exit;
        return $q;
//        return $q->result();
    }

    /* Function to check defendant value from Admin*/
    public function checkDefendant() {
        $result = $this->db->query("SELECT main1 FROM system WHERE system_id = 1");
        return json_decode($result->result()[0]->main1)->dpartytuse;
    }

    /* Function to get attorney assigned on the basis of case number*/
    public function get_attorney_assigned($caseno) {
        $result = $this->db->query("SELECT attyass FROM cal1 WHERE caseno = ".$caseno);
        return $result->result()[0]->attyass;
    }

    /*Function to get calendar events for Export*/
    public function getCalendarEventForExport($caseno, $caltyp) {
        if ($caseno != '') {
            $this->db->where('caseno', $caseno);
        }
        $this->db->where('c1.todo', $caltyp);

        $this->db->order_by('date', 'DESC');

        $this->db->limit(15, 0);

        $this->db->select('c1.caseno, c1.date, c1.tottime, c1.event, c1.first, c1.last, c1.defendant, c1.venue, c1.judge, c1.attyass, c1.location, c1.notes, c2.initials as attyhand');
        $this->db->from('cal1 c1');
        $this->db->join('cal2 c2', 'c1.eventno = c2.eventno', 'left');
        $this->db->group_by('c1.eventno');
        $q = $this->db->get();
        return $q->result();
    }

}
