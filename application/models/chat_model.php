<?php

class Chat_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $colorCode = array();
    }

    public function getAllChatUser($timestamp){

        if($timestamp > 0){
            // $this->db->where('timestamp >', $timestamp);
        }

        // $this->db->order_by('MAX(c.datetime)','DESC');
        $this->db->order_by('initials','ASC');
        $this->db->group_by('s.initials');
        // $this->db->select('s.initials,MAX(c.datetime) as latestDate,s.fname,s.lname,s.profilepic,s2.isloggedon');
        $this->db->select('s.initials,s.fname,s.lname,s.profilepic');
        // $this->db->join('chats c','s.initials = c.whofrom','left');
        // $this->db->join('staff2 s2','s2.initials = s.initials','left');
        $this->db->from('staff s');
        $q = $this->db->get();
        $row = array();

        foreach($q->result() as $k=>$v){
            //$rec = $this->getUnreadMsgCount($v->initials);
            $row[$k]['initials'] = $v->initials;
            //$row[$k]['unread'] = $rec->unreadMessage;
            //$row[$k]['datetime'] = $v->latestDate;
            $row[$k]['fullname'] = $v->fname.' '.$v->lname;
            $row[$k]['profilePic'] = '';
            //$row[$k]['isloggedon'] = $v->isloggedon;
            if($v->profilepic != ''){
                $row[$k]['profilePic'] = base_url().'assets/profileimages/'.$v->profilepic;
            }
        }

        return $row;
        exit;
    }

    public function getUnreadMsgCount($user){
        $loginUser = $this->session->userdata('user_data')['initials'];
        $where = array('whofrom'=>$user,'whoto'=>$loginUser,'is_read'=>'0');

        $this->db->where($where);
        $this->db->select('COUNT(id) as unreadMessage');
        $q = $this->db->get('chats');

        return $q->row();
    }


    public function addNewChat($data){
        $this->db->insert('chats',$data);
        return $this->db->insert_id();
    }

    public function getOnlineUser(){

        $this->db->where('initials !=','');
        $this->db->select('initials');
        $q = $this->db->get('staff2');

        $row = array();
        foreach($q->result() as $v){
            array_push($row, $v->initials);
        }
        return $row;

    }

    public function get_messages($timestamp,$length){

        $offset = 0;
        $limit = 10;
        if($length != ''){
            $offset = $length*$limit;
        }

        $this->db->where('timestamp >', $timestamp);
        $this->db->order_by('timestamp', 'DESC');
        $this->db->limit($limit,$offset);
        $this->db->select('c.*,s.profilepic');
        $this->db->join('staff s','s.initials = c.whofrom','left');
        $query = $this->db->get('chats c');

        $row = array();
        foreach($query->result_array() as $k=>$v){
            $row[$k] = $v;
            $row[$k]['profilepic'] = '';
            if($v['profilepic'] != ''){
                $row[$k]['profilepic'] = base_url().'assets/img/profilepic/'.$v['profilepic'];
            }
        }

        return array_reverse($row);
    }


}