<?php
class Task_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getTasks($start = 0, $limit = 10, $where = '', $order = '',$orCondition = '',$singleFilter = ''){

       // $select = array('mainkey','dateass','datepopup','whofrom','whoto','datereq','typetask','priority','event','completed');
        //$select = array('mainkey','dateass','whofrom','whoto','datereq','typetask','priority','event','datepopup','completed');
		//$select = array('mainkey','dateass','whofrom','whoto','datereq','typetask','priority','event','reminder','completed');
		$select = array('mainkey','dateass','event','whofrom','whoto','datereq','typetask','priority','reminder','completed');

//        $this->db->select('mainkey,whofrom,whoto,datepopup,dateass,datereq,event,completed,typetask,priority,color,caseno');
        //$this->db->select('mainkey,dateass,whofrom,whoto,datereq,typetask,priority,event,completed,color,caseno,datepopup');
		$this->db->select('mainkey,dateass,event,whofrom,whoto,datereq,typetask,priority,completed,color,caseno,reminder');
        //$this->db->select('mainkey,dateass,whofrom,whoto,datereq,typetask,priority,event,completed');

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
		
        if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column'] + 1], $order[0]['dir']);
        }
        if($orCondition != ''){
            $this->db->where($orCondition);
        }
        if($singleFilter != ''){
            $this->db->where($singleFilter);
        }
      //  $this->db->where('isdeleted','0');
        if($select[$order[0]['column']] != 'datereq')
		{
			$this->db->order_by('datereq',' ASC');
		}
        if($select[$order[0]['column']] != 'priority')
		{
			$this->db->order_by('priority',' ASC');
		}

        // $this->db->where('DATE(datereq) <=',date('Y-m-d'));
        $this->db->limit($limit, $start);
        $q = $this->db->get('tasks');
        //echo $this->db->last_query();exit;
        return $q->result();
    }

    public function getTaskCount($where = '',$orCondition = '',$singleFilter = '') {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if($orCondition != ''){
            $this->db->where($orCondition);
        }
        if($singleFilter != ''){
            $this->db->where($singleFilter);
        }
      //  $this->db->where('isdeleted','0');
       //$this->db->where('DATE(datereq) <=',date('Y-m-d'));
        $this->db->from('tasks');

        return $this->db->count_all_results();
    }

    public function getstaff(){

        $this->db->order_by('initials','ASC');
        $this->db->select('initials');
        $q = $this->db->get('staff');

        return $q->result();
    }

    public function deleteTask($cond,$orCond = ''){
        if($orCond != ''){
            $this->db->where($orCond);
        }
        $this->db->where($cond);
        $q = $this->db->delete('tasks');
//        echo $this->db->last_query();exit;
        return $this->db->affected_rows();
    }

    public function checkTaskCompletetion($cond){

        $this->db->where($cond);
        $this->db->select('completed');
        $res = $this->db->get('tasks');
        return $res->row();
    }

    public function getTaskPrintData($cond){

      if(isset($cond['selectedData']) && $cond['selectedData'] != ''){
            $this->db->where("FIND_IN_SET(mainkey,'".$cond['selectedData']."') !=", 0);
      }
      //$this->db->where("isdeleted","0");

      if(isset($cond['selectedData'])) { unset($cond['selectedData']); }

      $this->db->where($cond);
      $this->db->select("DATE_FORMAT(dateass, '%m/%d/%Y') as date,whofrom,whoto,DATE_FORMAT(datereq, '%m/%d/%Y') as finishby,caseno,event,typetask,priority,phase",false);
      $result = $this->db->get('tasks');
      //echo $this->db->last_query();exit;
      return $result->result();
    }
    public function getcompletedtask($id)
    {
        $this->db->where($id);
        $this->db->select('*');
        $res = $this->db->get('tasks');
        return $res->row();
    }

    public function checkTaskReminder()
    {
        $dt = date('Y-m-d H:i:s');
        $afterfive = date('Y-m-d H:i:s',strtotime('+10 minutes ',strtotime($dt)));
        $initialuser = $this->session->userdata('user_data')['initials'];
        //echo $afterfive;
       // echo '<br />';

        //$cond = "reminder = 1 AND notifyreminder != 1 AND datepopup >= '".$dt."' AND datepopup <= '".$afterfive."'";
        $cond = "reminder = 1 AND notifyreminder != 1 AND datepopup >= '".$dt."' AND datepopup <= '".$afterfive."' AND whoto='".$initialuser."'";
        $this->db->where($cond);
        //$this->db->select('*');
        $this->db->select('*');
        $res = $this->db->get('tasks');
        //echo $this->db->last_query();exit;
        $this->db->where($cond);
        $this->db->update('tasks', array('notifyreminder' => '1'));

        return $res->result();
    }
    public function tasksdeleteddayscount()
    {  
        $this->db->select('keeptaskco');
        $res = $this->db->get('system');
        $result= $res->row();
        $dayscount=$result->keeptaskco;
        
        $recorddelete=date("Y-m-d 00:00:00", strtotime($dayscount." days"));
        $this->db->where("datereq <",$recorddelete);
        $this->db->where("completed !=","0000-00-00 00:00:00");
        $this->db->delete('tasks'); 
    }
    
}
