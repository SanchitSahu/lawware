<?php

/**
 *
 */
class Injury_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getInjuryListing($caseId)
    {

        $this->db->where('caseno', $caseId);
        return $this->db->count_all_results('injury');
        // $result = $this->db->get();
        // return $result->result();
    }
}
