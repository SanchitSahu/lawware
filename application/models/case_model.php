<?php

/**
 *
 */
class Case_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getCaseListing($start = 0, $limit = 10, $where = '', $orCond = '', $order = '', $unique = "", $recent = "") {
        $select = array(
            'c.caseno',
            'cd.last', // cd.first,
            'c.atty_resp',
            'c.atty_hand',
            'c.casetype',
            'c.casestat',
            'cd.social_sec',
            'cd2.firm',
            'c.followup',
            'cd.home',
            'c.yourfileno',
            'cd.birth_date',
            'c.dateenter',
            'c.dateopen',
            'c.dateclosed',
            'cc.type',
            'cd.cardcode',
            'c.caption1',
            'cd2.address1'
        );
        $this->db->select('c.caseno,c.caption1,cd.cardcode,c.casetype,c.casestat,c.atty_resp,c.atty_hand,cd.first,cd.social_sec,c.followup,cd.home,c.yourfileno,c.dateenter,c.dateopen,c.dateclosed,cd.birth_date,cd.last,cd2.firm,cc.type,cd2.mailing1,cd2.firm,cd2.city,cd2.address1,cd2.address2,cd2.state,cd2.zip,IF(c.case_status = "1", "Active", "Deleted") as case_status,c.protected', false);

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCond != '') {
            $this->db->where($orCond);
        }
        //echo $recent;exit;
        if($recent != ""){
             $this->db->where_in('c.caseno',explode(",", $recent));
        }

        if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }
        
        
        
        if ($limit > -1) {
            if ($limit >= 5) {
                $this->db->limit($limit, $start);
            } else {
                $this->db->limit(5);
            }
        }
        if ($unique == "1") {
            $this->db->group_by("cd.first");
            $this->db->group_by("cd.last");
            $this->db->group_by("cd.social_sec");
        } else {
            $this->db->group_by('c.caseno');
        }

        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->join('intake intake', 'c.caseno = intake.caseno', 'left');
        $result = $this->db->get('case c');
        //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function getCaseCount($where = '', $orCond = '', $unique = "",$casenos = "") {

        if ($where == '' && $orCond == '') {

        } else {
            $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
            $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
            $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
            if($casenos != ""){
                $this->db->where_in('c.caseno',explode(",", $casenos));
            }
            if ($where != '' && is_array($where)) {
                $this->db->where($where);
            }
            if ($orCond != '') {
                $this->db->where($orCond);
            }
            /* if ($_POST['search']['value']) {
              } else {
              $this->db->limit(5);
              } */
        }

        if ($unique == "1") {
            $this->db->group_by("cd.first");
            $this->db->group_by("cd.last");
        } else {
            $this->db->group_by('c.caseno');
        }
        $this->db->select('c.caseno', false);

        $result = $this->db->get('case c');
        return $result->num_rows();
    }

    public function getCaseDetailsById($caseData) {

        $where = array('c.caseno' => $caseData[0]); //, 'cd.type' => $caseData[1]);

        $this->db->where($where);

        $this->db->select('c.caseno,c.casetype,c.casestat,cd.first,ca.event,cd.last,cd.type,cd.home,cd. business,cc.notes,c.followup,c.atty_resp', false);
        $this->db->join('case c', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('caseact ca', 'cc.caseno = ca.caseno', 'left');
        $result = $this->db->get('casecard cc');

        // echo $this->db->last_query();exit;

        return $result->row();
    }

    public function getCaseDetailsByIdNew($caseData) {

        $where = array('c.caseno' => $caseData[0]); //, 'cd.type' => $caseData[1]);

        $this->db->where($where);

        $this->db->select('c.caseno,c.casetype,c.casestat,cd.first,ca.event,cd.last,cd.type,cd.home,cd. business,cc.notes,c.followup,c.atty_resp', false);
        $this->db->join('case c', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('caseact ca', 'cc.caseno = ca.caseno', 'left');
        $result = $this->db->get('casecard cc');

        // echo $this->db->last_query();exit;

        return $result->row();
    }

    public function getFilterArray($systemData) {
        $this->load->model('common_model');

        $filterRecord = array();
        //'shortcuts' => 'ShortCuts',
        $searchBy = array('cd.first' => 'First Name', 'cd.last' => 'Last Name', 'cd2.firm' => 'Firm Name', 'c.caseno' => 'Case No', 'c.caseno_less' => 'Case No (all less)', 'c.caseno_more' => 'Case No (all more)', 'c.yourfileno' => 'Your File No', 'cd.social_sec' => 'Social Security No', 'c.followup' => 'Follow Up Dates', 'cd.contact' => 'Phone No', 'cd.birth_date' => 'Birth Date', 'cd.birth_month' => 'Birth Month', 'c.dateenter' => 'Date Entered', 'c.dateopen' => 'Date Open', 'c.dateclosed' => 'Date Closed', 'c.casestat' => 'Case Status');
        asort($searchBy);
        $filterRecord['searchBy'] = $searchBy;

        // $getAttyResp = $this->common_model->getAllRecord('system', 'POPATTYR');
        $getAttyResp = explode(",", $systemData[0]->popattyr);
        $newgetAttyResp = array();
        foreach ($getAttyResp as $keyaar => $valueaar) {
            $newgetAttyResp['atty_resp'] = $valueaar;
            $filterRecord['atty_resp'][] = $newgetAttyResp;
        }

        // $getAttyHand = $this->common_model->getAllRecord('system', 'POPATTYH');
        $getAttyHand = explode(",", $systemData[0]->popattyh);
        $newgetAttyHand = array();
        foreach ($getAttyHand as $keyath => $valueath) {
            $newgetAttyHand['atty_hand'] = $valueath;
            $filterRecord['atty_hand'][] = $newgetAttyHand;
        }

        // $getParaHand = $this->common_model->getAllRecord('system', 'POPPARA');
        //echo $this->db->last_query(); exit;
        $getParaHand = explode(",", $systemData[0]->poppara);
        $newgetParaHand = array();
        foreach ($getParaHand as $keyapr => $valueapr) {
            $newgetParaHand['para_hand'] = $valueapr;
            $filterRecord['para_hand'][] = $newgetParaHand;
        }

        //$filterRecord['para_hand'] = $newgetParaHand;
        //$getSecHand = $this->common_model->getAllRecord('case', 'DISTINCT(sec_hand)', 'sec_hand != ""', 'sec_hand asc');
        // $getSecHand = $this->common_model->getAllRecord('system', 'POPSEC');
        $getSecHand = explode(",", $systemData[0]->popsec);
        $newgetSecHand = array();
        foreach ($getSecHand as $keyasc => $valuesc) {
            $newgetSecHand['sec_hand'] = $valuesc;
            $filterRecord['sec_hand'][] = $newgetSecHand;
        }

        //$filterRecord['sec_hand'] = $getSecHand;
        //$getCaseType = $this->common_model->getAllRecord('case', 'DISTINCT(casetype)', 'casetype != ""', 'casetype asc');
        // $getCaseType = $this->common_model->getAllRecord('system', 'POPTYPE');
        $getCaseType = explode(",", $systemData[0]->poptype);
        $newgetCaseType = array();
        foreach ($getCaseType as $keyc => $valuc) {
            $newgetCaseType['casetype'] = $valuc;
            $filterRecord['casetype'][] = $newgetCaseType;
        }
        //$filterRecord['casetype'] = $getCaseType;
        //$getCaseStat = $this->common_model->getAllRecord('case', 'DISTINCT(casestat)', 'casestat != ""', 'casestat asc');
        //$filterRecord['casestat'] = $getCaseStat;
        // $getCaseStat = $this->common_model->getAllRecord('system', 'POPSTATUS');
        $getCaseStat = explode(",", $systemData[0]->popstatus);
        $newgetCaseStat = array();
        foreach ($getCaseStat as $keyat => $valuat) {
            $newgetCaseStat['casestat'] = $valuat;
            $filterRecord['casestat'][] = $newgetCaseStat;
        }

        // $getStaffIni = $this->common_model->getAllRecord('system', 'POPSTAFF');
        $getStaffIni = explode(",", $systemData[0]->popstaff);
        $newgetStaffIni = array();
        foreach ($getStaffIni as $keyat => $valuat) {
            $newgetStaffIni['staffini'] = $valuat;
            $filterRecord['staffini'][] = $newgetStaffIni;
        }

        //$getCardType = $this->common_model->getAllRecord('card', 'DISTINCT(type)', 'type != ""', 'type asc');
        // $getCardType = $this->common_model->getAllRecord('system', 'popclient');
        $getCardType = explode(",", $systemData[0]->popclient);
        $newgetCardType = array();
        foreach ($getCardType as $keypc => $valupc) {
            $newgetCardType['type'] = $valupc;
            $filterRecord['type'][] = $newgetCardType;
        }

        //$filterRecord['type'] = $getCardType;

        $getAllStaff = $this->common_model->getAllRecord('staff', 'DISTINCT(initials)', '', 'initials asc');

        $filterRecord['allstaff'] = $getAllStaff;
        //echo '<pre>'; print_r($filterRecord); exit;
        return $filterRecord;
    }

    public function getCaseFullDetails($case_id) {
        $case_search = explode('-', $case_id);
        $q = "SELECT *,c.venue AS mainVenue,c2.firmcode,c2.firm,c2.city,c2.address1,cc.casecard_no,cc.type AS cardtype FROM casecard cc
            LEFT JOIN `case` c ON cc.caseno = c.caseno
            LEFT JOIN card cd ON cc.cardcode = cd.cardcode
            LEFT JOIN card2 c2 ON c2.firmcode= cd.firmcode
            WHERE c.caseno = $case_search[0] order by orderno ASC
        ";
        $query = $this->db->query($q);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            $q1 = "SELECT *,c.venue AS mainVenue
                FROM `case` c
                WHERE c.caseno = $case_search[0]
            ";
            $query1 = $this->db->query($q1);
            if($query1->num_rows() > 0) {
                return $query1->result();
            } else {
                return false;
            }
        }
    }

    public function getCaseFullDetails2($case_id) {
        $case_search = explode('-', $case_id);
        /*$query = $this->db->query("
            SELECT *,c.venue AS mainVenue,c2.firmcode,c2.firm,c2.city,c2.address1,cc.casecard_no,cc.type AS cardtype,cd.fax AS cfax FROM casecard cc
            LEFT JOIN `case` c ON cc.caseno = c.caseno
            LEFT JOIN card cd ON cc.cardcode = cd.cardcode
            LEFT JOIN card2 c2 ON c2.firmcode= cd.firmcode
            WHERE (c.caseno = $case_search[0]
            OR cd.cardcode = 853)
            AND cd.cardcode != ''
            GROUP BY cc.cardcode
            order by orderno ASC
        ");*/
        
        $query = $this->db->query("(SELECT *,c2.firm,c.venue AS mainVenue,c2.firmcode,c2.city,c2.address1,cc.casecard_no,cc.type AS cardtype,cd.fax AS cfax 
            FROM casecard cc 
            LEFT JOIN `case` c ON cc.caseno = c.caseno 
            LEFT JOIN card cd ON cc.cardcode = cd.cardcode 
            LEFT JOIN card2 c2 ON c2.firmcode= cd.firmcode 
            WHERE (c2.firmcode = 745)
            GROUP BY c2.firmcode)
            UNION 
            (SELECT *,c2.firm,c.venue AS mainVenue,c2.firmcode,c2.city,c2.address1,cc.casecard_no,cc.type AS cardtype,cd.fax AS cfax 
            FROM casecard cc 
            LEFT JOIN `case` c ON cc.caseno = c.caseno 
            LEFT JOIN card cd ON cc.cardcode = cd.cardcode 
            LEFT JOIN card2 c2 ON c2.firmcode= cd.firmcode 
            WHERE c.caseno = $case_search[0]
            AND cd.cardcode != ''
            ORDER BY orderno ASC LIMIT 0,100)");

        if ($query->num_rows() > 0) {
            //echo $this->db->last_query();exit;
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCaseDataByCaseId($case_id) {
        $cond = array('caseno' => $case_id);
        $this->db->where($cond);
        $this->db->select('*');
        $q = $this->db->get('case');

        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return false;
        }
    }

    public function getCaseActivity($case_id, $category = '', $chackvalue = '', $start = 0, $limit = 10, $order = '', $dir = '', $where = '', $singleFilter="", $callFrom = '') {

        $this->db->select('*');
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        $this->db->where(array('caseno' => $case_id));
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->order_by('upontop', ' DESC ');
        if ($order != '' && $dir != '') {
            $this->db->order_by($order, $dir);
        }
        /*else{
            $this->db->order_by('date', ' asc ');
        }*/
        $this->db->order_by('date', ' DESC ');
        $this->db->order_by('id', ' DESC ');
        $this->db->order_by('actno', ' ASC ');
        
        // Query with DINKY
        $this->db->where('NOT( category BETWEEN 1000000 AND 1000020) and NOT( category BETWEEN 10000010 AND 10000021)');
        //$this->db->where("NOT(category in(1000000,1000021) OR mainnotes = '1')");
        if ($category != '' && ($chackvalue != 1 || $chackvalue == '')) {
            // if ($category != '' || $chackvalue != 1 || $chackvalue = '') {
            if ($category == '1' && $chackvalue == 0) {
                $this->db->where("(category = '$category' or category = '1000021' OR mainnotes = '1')");
            } else {
                $this->db->where("(category = '$category' or mainnotes = '1')");
            }
        }/* elseif($category != '' && $chackvalue == 1) {
            if ($category == '1') {
                $this->db->where("(mainnotes = '1')");
            } else {
                $this->db->where("(mainnotes = '1')");
            }
        }*/

        if ($limit > -1) {
            if($callFrom != 'document') {
                $this->db->limit($limit, $start);
            }
        }
        $q = $this->db->get('caseact');
        //echo $this->db->last_query(); exit;
        return $q->result();
    }

    // public function getCaseActivity1($case_id)
    //    {
    //        $this->db->where('NOT( category BETWEEN 1000000 AND 1000021)');
    //        $this->db->order_by('upontop DESC,date DESC');
    //        $result = $this->db->get_where('caseact', array('caseno' => $case_id));
    //       // echo $this->db->last_query(); exit;
    //        return $result->result();
    //    }

    public function getCaseActivity1($start = 0, $limit = 10, $where = '', $order = '', $orCondition = '', $singleFilter = '') {

        $this->db->select('*');

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        /*  if ($order != '' && is_array($order)) {
          $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
          } */
        if ($orCondition != '') {
            if ($orCondition != 'all') {
                $this->db->where($orCondition);
            }
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->where('NOT( category BETWEEN 1000000 AND 1000021) and NOT( category BETWEEN 10000010 AND 10000021)');
        $this->db->order_by('upontop', ' DESC ');
        // $this->db->where('DATE(datereq) <=',date('Y-m-d'));
        // $this->db->limit($limit, $start);
        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }
        $q = $this->db->get('caseact');
        // echo $this->db->last_query();exit;
        return $q->result();
    }

    public function getCaseactivityCount($where = '', $orCondition = '', $singleFilter = '') {
        //print_r($arr);       echo implode(',', $arr);exit;
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCondition != '') {
            if ($orCondition != 'all') {
                $this->db->where($orCondition);
            }
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if (!empty($arr)) {
            $actno = implode(',', $arr);

            $this->db->where_not_in('actno', $actno);
        }
        $this->db->where('NOT( category BETWEEN 1000000 AND 1000021) and NOT( category BETWEEN 10000010 AND 10000021)');

        $q = $this->db->get('caseact');

        $result = $q->result();

        // return $this->db->count_all_results();
        $count = 0;
        foreach ($result as $result) {
            if ($result->access == 3) {
                if ($result->access == $this->session->userdata('user_data')['username']) {
                    $count++;
                }
            } else {
                $count++;
            }
        }
        return $count;

        //  $this->db->from('caseact');
        //return $this->db->count_all_results();
    }

    public function getEventActivity($case_id) {
        $this->db->where('category BETWEEN 1000000 AND 1000021');
        $this->db->select('event,category');
        $result = $this->db->get_where('caseact', array('caseno' => $case_id));

        return $result->result();
    }

    public function getCaseCalendar($caseno, $start = 0, $limit = 10, $where = '', $order = '', $dir = '', $singleFilter = '') {
        $cond = 'c1.date DESC';
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($order != '' && $dir != '') {
            $this->db->order_by($order, $dir);
        } else {
            $this->db->order_by($cond);
        }

        $this->db->select('c1.*, c2.initials as attyh', false);
        $this->db->join('cal2 c2', 'c1.eventno = c2.eventno', 'LEFT');
        $this->db->where(array('caseno' => $caseno, 'event !=' => ''));
        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get('cal1 c1');
        //echo $this->db->last_query();
        return $result->result();
    }

    public function getCardDetails($cardcode, $caseno) {
        //$this->db->select("c.car,CONCAT(c.salutation, ' ', c.first, ' ', c.middle, ' ', c.last) AS fullname,c.popular, c.type,ca.atty_hand, c.home, IF(c.business = '', c2.phone1, c.business) business, IF(c.fax = '', c2.fax, c.fax) fax, c.email, c2.firm, c2.address1, c2.address2, CONCAT(c2.city, ', ', c2.state, ' ', c2.zip) as city, IF(c2.eamsref = 0, '', c2.eamsref) eamsref,cc.*, IFNULL(c3.name,'') name, c.comments comment_per, c2.comments comment_bus, c.beeper", false);
        $this->db->select("c.car,CONCAT(c.salutation, ' ', c.first, ' ', c.middle, ' ', c.last) AS fullname,c.popular, c.type,ca.atty_hand, c.home, IF(c2.phone1 = '', c.business, c2.phone1) business, IF(c.fax = '', c2.fax, c.fax) fax, c.email, c2.firm, c2.address1, c2.address2, CONCAT(c2.city, ', ', c2.state, ' ', c2.zip) as city, IF(c2.eamsref = 0, '', c2.eamsref) eamsref,cc.*, IFNULL(c3.name,'') name, c.comments comment_per, c2.comments comment_bus, c.beeper", false);
        $this->db->where('c.cardcode', $cardcode);
        $this->db->join('card2 c2', 'c.firmcode=c2.firmcode', 'LEFT');
        $this->db->join('casecard cc', "cc.cardcode=c.cardcode AND cc.caseno=$caseno", 'LEFT');
        $this->db->join('case ca', 'ca.caseno=cc.caseno', 'LEFT');
        $this->db->join('card3 c3', 'c2.eamsref=c3.eamsref', 'LEFT');
        $result = $this->db->get('card c');
        // echo $this->db->last_query();
        return $result->result();
    }

    public function addEditActivity($data, $files) {
        //print_R($data);exit;
        /*$current_second = date('s');*/
        $newDate = date("Y-m-d", strtotime($data['add_case_cal']));
        $exploded_time = explode(' ', $data['add_case_time']);
        $temp_time = explode(':', $exploded_time[0]);
        if($exploded_time[1] == 'PM' || $exploded_time[1] == 'pm') {
            if($temp_time[0] != 12) {
                $added_time = $temp_time[0] + 12;
                if($added_time == 24) {
                    $newTime = '00:'.$temp_time[1].':00'/*.$current_second*/;
                } else {
                    $newTime = $added_time.':'.$temp_time[1].':00'/*.$current_second*/;
                }
            } else {
                $newTime = $exploded_time[0].':00'/*.$current_second*/;
            }
        } else {
            if($temp_time[0] == 12) {
                $newTime = '00:'.$temp_time[1].':00'/*.$current_second*/;
            } else {
                $newTime = $exploded_time[0].':00'/*.$current_second*/;
            }
        }
        $newDateTime = $newDate . ' ' . $newTime;
        /*echo $newDateTime;exit;*/
        //$newDate = date("Y-m-d H:i:s");
        $billingDate = $this->checkValidDate($data['activity_biling_cycle']);
        $pmDate = $this->checkValidDate($data['activity_postmark_date']);
        $pdDate = $this->checkValidDate($data['activity_payment_due']);

        if (isset($data['activity_mainnotes'])) {
            $mainnotes = $data['activity_mainnotes'];
        } else {
            $mainnotes = 0;
        }
        if (isset($data['activity_redalert'])) {
            $redalert = $data['activity_redalert'];
        } else {
            $redalert = 0;
        }
        if (isset($data['activity_upontop'])) {
            $upontop = $data['activity_upontop'];
        } else {
            $upontop = 0;
        }
        if (isset($data['activity_classification'])) {
            $classification = $data['activity_classification'];
        } else {
            $classification = 0;
        }
        $activity_fee1 = '';
        if ($data['activity_fee1'] != '') {
            $activity_fee1 = isset($data['activity_fee1']) ? "-" . $data['activity_fee1'] : '';
        }

        if (isset($data['activity_entry_type'])) {
            $activity_entry_type = $data['activity_entry_type'];
        } else {
            $activity_entry_type = '';
        }

        $caseact_uploaddocname = "";
        if (isset($files['caseact_uploaddoc']) && $files['caseact_uploaddoc'] != "") {

            if (!file_exists(DOCUMENTPATH . 'clients/' . $data['caseActNo'])) {
                mkdir(DOCUMENTPATH . 'clients/' . $data['caseActNo'], 0777, true);
            }
            $caseact_uploaddocPath = pathinfo($files['caseact_uploaddoc']['name']);
            $extension = (isset($caseact_uploaddocPath['extension'])) ? $caseact_uploaddocPath['extension'] : "";
            $caseact_uploaddocname = "Doc" . time() . "." . $extension;
            $uploadDirpath = DOCUMENTPATH . 'clients/' . $data['caseActNo'] . "/";
            move_uploaded_file($files['caseact_uploaddoc']['tmp_name'], $uploadDirpath . $caseact_uploaddocname);
        } else {
            $caseact_uploaddocname = $_POST['caseact_uploaddocDefault'];
        }

        $act_array = array(//'actno'      =>  $caseActno + 1,
            // 'caseno'    =>  $data['caseActNo'],
            'initials0' => $data['orignal_ini'],
            'initials' => $data['last_ini'],
            'date' => $newDateTime,
            'event' => $data['activity_event'],
            'atty' => $data['case_act_atty'],
            'title' => $data['activity_title'],
            'cost' => $data['case_act_cost_amt'],
            'minutes' => $data['case_act_time'],
            'rate' => $data['case_act_hour_rate'],
            'arap' => $classification,
            'mainnotes' => $mainnotes,
            'redalert' => $redalert,
            'upontop' => $upontop,
            'typeact' => $data['activity_typeact'],
            'type' => $data['activity_fee'] . $activity_fee1,
            'category' => $data['case_category'],
            'color' => $data['color_codes'],
            'access' => $data['security'],
            'wordstyle' => $data['activity_doc_type'],
            'ole3style' => $data['activity_style'],
            'frmtm' => $data['case_act_TM'],
            'frmbm' => $data['case_act_BM'],
            'frmlm' => $data['case_act_LM'],
            'frmrm' => $data['case_act_RM'],
            'frmwidth' => $data['case_act_PW'],
            'frmheight' => $data['case_act_PH'],
            'orient' => $data['case_act_orientation'],
            'copies' => $data['case_act_copies'],
            'bistyle' => $activity_entry_type,
            'biteamno' => $data['activity_teamno'],
            'bicycle' => $billingDate,
            'bipmtdate' => $pmDate,
            'bipmtduedt' => $pdDate,
            'bilatefee' => $data['activity_latefee'],
            'bicheckno' => $data['activity_check_no'],
            'bidesc' => $data['activity_short_desc'],
            //'biowner'   =>  $data['activity_staff'],
            'bitime' => $data['activity_bi_min'],
            'bihourrate' => $data['activity_bi_rate'],
            'bifee' => $data['activity_bi_fee'],
            'bicost' => $data['activity_event_desc'],
            'bilatefee' => $data['activity_bi_latefee'],
            'bipmt' => $data['activity_bi_creditac'],
            'ole3' => $caseact_uploaddocname,
        );

        $caseActivityNumber = 0;

        if ($data['activity_no'] != "") {
            $caseActivityNumber = $data['activity_no'];
            $this->db->trans_start();
            $this->db->where("caseno", $data['caseActNo']);
            $this->db->where("actno", $data['activity_no']);
            $this->db->update('caseact', $act_array);
            //echo "update ";echo $this->db->last_query();exit;

            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $return = false;
            } else if ($this->db->affected_rows() > 0 || !empty($files)) {
                $return = "edited";
            } else {
                $return = "updated";
            }
        } else {
            $res = $this->common_functions->getRecordById('caseact', 'actno', 'caseno =' . $data['caseActNo'] . ' order by actno desc limit 0,1');
            //echo "kk<pre>";print_r($data);//exit;
            if (!empty($res)) {
                $caseActno = $res->actno;
            } else {
                $caseActno = 0;
            }

            $caseActivityNumber = $caseActno + 1;

            $act_array['actno'] = $caseActno + 1;
            $act_array['caseno'] = $data['caseActNo'];

            if ($this->db->insert('caseact', $act_array)) {
                //echo "insert";echo $this->db->last_query();exit;
                $return = $act_array['actno'];
            } else {
                $return = false;
            }
        }

        //print_r($files);

        if (!empty($files['document'])) {
            //echo "here";
            /* if (!file_exists(base_url() . 'clients/' . $data['caseActNo'])) {
              //echo "here now";
              mkdir(base_url() . 'clients/' . $data['caseActNo'], 0777, true);
              } */
            // echo DOCUMENTPATH .'clients/'. $data['caseActNo'];
            // exit;
            if (!file_exists(DOCUMENTPATH . 'clients/' . $data['caseActNo'])) {
                //echo "here now";
                mkdir(DOCUMENTPATH . 'clients/' . $data['caseActNo'], 0777, true);
            }

            if ($data['activity_no'] != "") {
                $directory = DOCUMENTPATH . 'clients/' . $data['caseActNo'] . DIRECTORY_SEPARATOR . 'F' . $caseActivityNumber . "*";
                $filesPrev = glob($directory);

                $cnt = count($filesPrev) + 1;
            } else {
                $cnt = 1;
            }

            for ($i = 0; $i < count($files['document']['name']); $i++) {
                $temp = explode(".", $files['document']["name"][$i]);
                //print_r($temp);exit;
                $newfilename = 'F' . $caseActivityNumber . "_" . $cnt . '.' . end($temp);
                $directory = DOCUMENTPATH . 'clients/' . $data['caseActNo'] . DIRECTORY_SEPARATOR . 'F' . $caseActivityNumber . "_" . $cnt . ".*";
                array_map('unlink', glob($directory));
                move_uploaded_file($files['document']["tmp_name"][$i], DOCUMENTPATH . 'clients' . DIRECTORY_SEPARATOR . $data['caseActNo'] . DIRECTORY_SEPARATOR . $newfilename);
                $cnt++;
            }
        }

        return $return;
        //echo "id".$this->db->insert_id();exit;
    }

    public function getCalendarDataByEvent($eventNo) {
        $this->db->select('c1.*, c2.initials as attyh', false);
        $this->db->join('cal2 c2', 'c1.eventno = c2.eventno', 'LEFT');
        $this->db->where('c1.eventno', $eventNo);
        $res = $this->db->get('cal1 c1');

        return $res->row();
    }

    public function checkValidDate($dateVal) {
        if (strtotime($dateVal) != '0000-00-00 00:00:00') {
            return date('Y-m-d h:i;s', strtotime($dateVal));
        } else {
            return '0000-00-00 00:00:00';
        }
    }

    public function setBillingDefaults($data) {
        //echo "<pre>";print_r($data);exit;

        $pmtmin = $data['default_min_payment'] * 100;
        $lateFee = $data['default_late_fee'] * 100;
        $freeze = isset($data['default_freeze']) ? $data['default_freeze'] : 1;
        $paidto = date('Y-m-d H:i:s', strtotime($data['default_payment_dt']));
        $staff = isset($data['default_staff']) ? $data['default_staff'] : "";

        if (isset($data['default_freeze_until'])) {
            $duedate = date('Y-m-d H:i:s', strtotime($data['default_freeze_until']));
        } else {
            $duedate = '';
        }

        $res = $this->db->get_where('bill2', array('caseno' => $data['caseNo']));
        //print_r($res);exit;

        $entryType = isset($data['default_entry_type']) ? $data['default_entry_type'] : 8;
        $bill1_array = array(
            'status1' => $data['default_status'],
            'initials' => $staff,
            'pmtmin' => $pmtmin,
            'freeze' => (int) $freeze,
            'freezedt' => $duedate,
            'duedate' => $data['default_due_date'],
            'paidto' => $paidto,
            'latefee' => $lateFee,
            'lateday' => $data['default_late_fee_day'],
            'biteamno' => $data['default_teamno'],
            'bihourrate' => $data['default_hr_rate'],
            'memo1' => $entryType,
        );
        if (!empty($res)) {
            $this->db->where("caseno", $data['caseNo']);
            $this->db->update('bill2', $bill1_array);
            //echo $this->db->last_query();exit;
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                return 0;
            } else {
                return 1;
            }
        } else {
            $bill1_array['caseNo'] = $data['caseNo'];

            //echo $this->db->last_query();exit;
            //echo "id".$this->db->insert_id();exit;

            if ($this->db->insert('bill2', $bill1_array)) {
                //echo $this->db->last_query();exit;
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function getBillingDefault($case_id) {
        $this->db->select('*');
        $this->db->where(array('caseno' => $case_id));
        $result = $this->db->get('bill2');
        return $result->result();
    }

    public function getActivityDetails($actno, $caseno) {
        $this->db->select("*", false);
        $this->db->where('actno', $actno);
        $this->db->where('caseno', $caseno);
        $result = $this->db->get('caseact');
        //echo $this->db->last_query();exit;
        return $result->result();
    }

    // Get Today's Task Info , $user
    /*public function tasksDetailsByDate($caseno) {
        $cond = array('DATE(datereq) >= ' => date('Y-m-d'), 'caseno' => $caseno); //, 'whoto' => $user

        $this->db->select('mainkey,datereq,event,completed,whofrom,whoto,caseno');
        $this->db->where($cond);
        $this->db->order_by('datereq', 'ASC');
        $result = $this->db->get('tasks');
        //echo $this->db->last_query(); exit;
        $row = array();
		
        $completedArray = array('0000-00-00 00:00:00', '1899-12-30 00:00:00', '');
        foreach ($result->result() as $k => $v) {
            $row[$k] = $v;
            $row[$k]->isCompleted = 0;
            $row[$k]->datereq = date('m/d/Y', strtotime($v->datereq));
            $row[$k]->whofrom = $v->whofrom;
            $row[$k]->whoto = $v->whoto;
            if (!in_array($v->completed, $completedArray)) {
                $row[$k]->isCompleted = 1;
            }
            $row[$k]->caseno = $v->caseno;
        }

        return $row;
    }
*/

  public function tasksDetailsByDate($limit = 5, $start = 0, $user, $singleFilter = '', $order = '',$caseno='') {

        // $cond = array('DATE(datereq) <= ' => date('Y-m-d'), 'whoto' => $user);

        $cond = array(/*'whoto' => $user,*//*'DATE(datereq) >= ' => date('Y-m-d'),*/'caseno' => $caseno);

        $select = array('datereq', 'whofrom', 'event');
        $this->db->select('datereq,whofrom,event,mainkey,color,caseno,completed', false);
        $this->db->where($cond);
        $this->db->where('(completed = "0000-00-00 00:00:00" OR completed = "1899-12-30 00:00:00"  OR completed = "")');
         if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }
        if($select[$order[0]['column']] != 'priority')
        {
            $this->db->order_by('priority',' ASC');
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
		
        if ($limit >= 5) {
            $this->db->limit($limit, $start);
        } else {
            $this->db->limit(5);
        }



        $result = $this->db->get('tasks');
         //echo $this->db->last_query(); exit;
		
        $result = $result->result();
//        $row = array();
//        $completedArray = array('0000-00-00 00:00:00', '1899-12-30 00:00:00', '');
//        foreach ($result->result() as $k => $v) {
//            $row[$k] = $v;
//            $row[$k]->isCompleted = 0;
//            $row[$k]->datereq = date('m/d/Y', strtotime($v->datereq));
//            $row[$k]->whofrom = $v->whofrom;
//            if (!in_array($v->completed, $completedArray)) {
//                $row[$k]->isCompleted = 1;
//            }
//
//        }

        return $result;
    }

    public function get_all_case_tasks_count($currentuser,$caseno='', $singleFilter = "") {
        // $cond = array('DATE(datereq) <= ' => date('Y-m-d'), 'whoto' => $currentuser);
        $cond = array(/*'whoto' => $currentuser,*/'caseno' => $caseno);
        $this->db->where($cond);
        if ($singleFilter != "") {
            $this->db->where($singleFilter);
        }
        $this->db->where('(completed = "1899-12-30 00:00:00" OR completed = "0000-00-00 00:00:00")');
        $this->db->from('tasks');
        // $count = $this->db->count_all_results();
        return $this->db->count_all_results();
    }

  
  
    public function tasksDetailsToday($limit = 5, $start = 0, $user, $singleFilter = '', $order = '') {

        // $cond = array('DATE(datereq) <= ' => date('Y-m-d'), 'whoto' => $user);

        $cond = array('whoto' => $user);

        $select = array('priority', 'whofrom', 'event', 'datereq');
        $this->db->select('priority,whofrom,event,mainkey,datereq,color,caseno,completed', false);
        $this->db->where($cond);
        $this->db->where('(completed = "1899-12-30 00:00:00" OR completed = "0000-00-00 00:00:00")');
         if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
		if($select[$order[0]['column']] != 'datereq')
		{
			$this->db->order_by('datereq', ' ASC');
		}
		if($select[$order[0]['column']] != 'priority')
		{
			$this->db->order_by('priority', ' ASC');
		}
		
        if ($limit >= 5) {
            $this->db->limit($limit, $start);
        } else {
            $this->db->limit(5);
        }



        $result = $this->db->get('tasks');
//         echo $this->db->last_query(); exit;

        $result = $result->result();
//        $row = array();
//        $completedArray = array('0000-00-00 00:00:00', '1899-12-30 00:00:00', '');
//        foreach ($result->result() as $k => $v) {
//            $row[$k] = $v;
//            $row[$k]->isCompleted = 0;
//            $row[$k]->datereq = date('m/d/Y', strtotime($v->datereq));
//            $row[$k]->whofrom = $v->whofrom;
//            if (!in_array($v->completed, $completedArray)) {
//                $row[$k]->isCompleted = 1;
//            }
//
//        }

        return $result;
    }

    public function getInjuryListing($caseId) {

        $this->db->where('caseno', $caseId);
        $this->db->order_by('orderno', 'DESC');
        $this->db->select('*');
        $result = $this->db->get('injury');
        //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function getInjuryCountForCase($caseId) {
        $this->db->where('caseno', $caseId);
        $this->db->from('injury');
        return $this->db->count_all_results();
    }
    
    public function getcaseAttorney($caseId) {
        $this->db->select('atty_hand');
        $this->db->where('caseno', $caseId);
        $result = $this->db->get('case');
        return $result->result();
    }

    public function getInjuryData($cond) {
        $this->db->where($cond);
        $this->db->order_by('orderno', 'ASC');
        $this->db->select('*');
        $result = $this->db->get('injury');
        //echo $this->db->last_query(); exit;
        return $result->row();
    }

    public function getRolodexSearchParties($where = '', $start = 0, $limit = "", $order = '',$singleFilter) {
        //echo 'in'; exit;
        //$select = array('c2.firm', 'c2.city', 'c2.phone1', 'c2.address1', 'c2.address2', 'c2.city', 'c2.state', 'c2.zip');
        $select = array('c2.firm', 'c2.city', 'c2.address1', 'c2.address2', 'c2.phone1', 'c.first','c2.state', 'c2.zip');
        $this->db->select('c.cardcode,c.type,c2.firm,c.first,c.last,c2.city,c2.phone1,c2.address1, c2.address2, c2.city,c2.state,c2.zip', false);
        if ($where != '') {
            $this->db->where($where);
        }
        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }
        
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }

        if ($order != '' && !empty($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        $this->db->join('card2 c2', 'c.firmcode = c2.firmcode', 'left');
        $this->db->group_by('c.firmcode');
        $result = $this->db->get('card c');
      //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function getRolodexSearchPartiesCount($where = '',$singleFilter) {

        $this->db->select('c.cardcode,c.type,c2.firm,c.first,c.last,c2.city,c2.phone1,c2.address1, c2.address2, c2.city,c2.state,c2.zip', false);
        if ($where != '') {
            $this->db->where($where);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }

        $this->db->join('card2 c2', 'c.firmcode = c2.firmcode', 'left');
        $this->db->group_by('c.firmcode');
        $result = $this->db->get('card c');
        $result = $result->result();
        $Casecount = count($result);
        return $Casecount;
    }

    public function getRolodexSearchParties1($where = '', $orderby = '') {
        //$select = array('c.caseno', 'cd.first', 'c.atty_resp', 'c.atty_hand', 'c.casetype', 'c.casestat', 'cd.type');
        $this->db->select('c.cardcode,c.type,c2.firm,c.first,c.last,c2.city,c2.phone1,c2.address1, c2.address2, c2.zip', false);

        if ($where != '') {
            $this->db->where($where);
        }

        if ($orderby != '') {
            $this->db->order_by($orderby);
        }
        //$this->db->limit($limit, $start);
        $this->db->join('card2 c2', 'c.firmcode = c2.firmcode', 'left');
        //$this->db->group_by('c.firmcode');
        $result = $this->db->get('card c');

        //echo $this->db->last_query();exit;

        return $result->result();
    }

    public function getSearchedPartieCardDetails($cardcode, $caseno) {
        $this->db->select("CONCAT(c.salutation, ' ', c.first, ' ', c.middle, ' ', c.last) AS fullname,c2.firmcode,c.cardcode, c.specialty, c.social_sec, c.licenseno, c.type, c.home, IF(c.business = '', c2.phone1,
            c.business) business, c.email, c2.firm, c2.address1, c2.address2, CONCAT(c2.city, ', ', c2.state, '
            ,', c2.zip) as city ,c2.venue", false);
        $this->db->where('c.cardcode', $cardcode);
        $this->db->join('card2 c2', 'c.firmcode=c2.firmcode', 'LEFT');
        if ($caseno != '') {
            $this->db->join('casecard cc', "cc.cardcode=c.cardcode AND cc.caseno=$caseno", 'LEFT');
        }
        $result = $this->db->get('card c');
//        echo $this->db->last_query();exit;
        return $result->result();
    }

    public function addSearchedPartyToCase($cardcode, $caseno, $type) {
        $this->db->where('caseno', $caseno);
        $this->db->where('cardcode', $cardcode);
        $this->db->where('type', $type);
        $this->db->select('*');
        $result = $this->db->get('casecard');

        if (!empty($result->result())) {
            return 0;
        } else {
            $this->db->where('caseno', $caseno);
            //$this->db->where('cardcode', $cardcode);
            $this->db->order_by("orderno", "DESC");
            $this->db->select('orderno');
            $result = $this->db->get('casecard');
            $orderno = $result->result_array();
            // echo $this->db->last_query();exit;
            $orderno = $orderno[0]['orderno'] + 1;
            $newParty = array(
                "caseno" => $caseno,
                "cardcode" => $cardcode,
                "type" => $type,
                "orderno" => $orderno,
            );

            if ($this->db->insert('casecard', $newParty)) {
                //echo $this->db->last_query();exit;
                return $this->db->insert_id();
            } else {
                return 0;
            }
        }
    }

    public function removeParty($cardcode, $caseno, $newCap) {
        $cond = array('cardcode' => $cardcode, 'caseno' => $caseno);
        $resp = $this->common_functions->deleteRec('casecard', $cond);
        $res = array();
        if ($resp == 1) {
            $res = array('status' => 1, 'Party Removed Successfully', 'caption' => $newCap);
        } else {
            $res = array('status' => 0, 'Party Not Removed', 'caption' => $newCap);
        }

        echo json_encode($res);
    }

    public function addNewCase($first_name, $last_name, $casetype, $casestatus) {
        //echo 'casetype =>'.$casetype; exit;
        $newCase = array(
            "casetype" => $casetype,
            "casestat" => $casestatus,
            "dateenter" => date("Y-m-d h:i:s"),
            "caption1" => $first_name . " " . $last_name . " v. Straussner & Sherman",
        );

        if ($this->db->insert('case', $newCase)) {
            //echo $this->db->last_query();exit;
            $keyArr = array(
                "caseno" => $this->db->insert_id(),
                "related" => 0,
                "actno" => 0,
            );
            $this->db->insert('casekey', $keyArr);
            //  echo $keyArr['caseno'] ;exit;

            $ii = 1;
            for ($i = 1000001; $i <= 1000021; $i++) {
                if ($i == 1000021) {
                    /* $event = $formdata['card_type'] . " Entered Into Computer"; */
                    $event = "APPLICANT Entered Into Computer";
                } else {
                    $event = '';
                }
                $caseActArr = array(
                    "actno" => $ii++,
                    "caseno" => $this->db->insert_id(),
                    "initials0" => $this->session->userdata('user_data')['username'],
                    'initials' => '',
                    "date" => date('Y-m-d H:i:s'),
                    'event' => $event,
                    'atty' => '',
                    'title' => '0',
                    'cost' => '0.00',
                    'minutes' => '0',
                    'rate' => '0',
                    'arap' => '1',
                    'mainnotes' => '0',
                    'redalert' => '0',
                    'upontop' => '0',
                    'type' => '',
                    "category" => $i,
                    'color' => '1',
                    'access' => '1',
                    'wordstyle' => '0',
                    'ole3style' => '0',
                    'frmtm' => '0.00',
                    'frmbm' => '0.00',
                    'frmlm' => '0.00',
                    'frmrm' => '0.00',
                    'frmwidth' => '0.00',
                    'frmheight' => '0.00',
                    'orient' => '0',
                    'copies' => '0',
                    'typeact' => '0',
                    'bistyle' => '0',
                    'biteamno' => '0',
                    'bicycle' => '1899-12-30 00:00:00',
                    'bitime' => '0',
                    'bihourrate' => '0',
                    'bifee' => '0.00',
                    'bicost' => '0.00',
                    'bidesc' => '',
                    'bicheckno' => '',
                    'bipmt' => '0.00',
                    'bipmtdate' => '1899-12-30 00:00:00',
                    'bipmtduedt' => '1899-12-30 00:00:00',
                    'bilatefee' => '0.00'
                );
                $this->common_functions->insertRecord('caseact', $caseActArr);
            }
            return $keyArr['caseno'];
        } else {
            return 0;
        }
    }

    public function updatepnccase($prospect_id, $case) {
        $this->db->where('prospect_id', $prospect_id);
        $this->db->update('intake', array('caseno' => $case));
    }

    public function editCase($data) {
        // echo "<pre>";print_r($data);exit;
        if (isset($data['casedate_entered'])) {
            $data['casedate_entered'] = $this->dateFormatChange($data['casedate_entered']);
        } else {
            $data['casedate_entered'] = '';
        }

        if (isset($data['casedate_open'])) {
            $data['casedate_open'] = $this->dateFormatChange($data['casedate_open']);
        } else {
            $data['casedate_open'] = '';
        }

        if (isset($data['casedate_followup'])) {
            $data['casedate_followup'] = $this->dateFormatChange($data['casedate_followup']);
        } else {
            $data['casedate_followup'] = '';
        }

        if (isset($data['casedate_closed'])) {
            $data['casedate_closed'] = $this->dateFormatChange($data['casedate_closed']);
        } else {
            $data['casedate_closed'] = '';
        }

        if (isset($data['casedate_referred'])) {
            $data['casedate_referred'] = $this->dateFormatChange($data['casedate_referred']);
        } else {
            $data['casedate_referred'] = '';
        }

        if (isset($data['casedate_psdate'])) {
            $data['casedate_psdate'] = $this->dateFormatChange($data['casedate_psdate']);
        } else {
            $data['casedate_psdate'] = '';
        }

        if (isset($data['case_ps'])) {
            $data['case_ps'] = $data['case_ps'];
        } else {
            $data['case_ps'] = '';
        }

        if (isset($data['editCaseStat'])) {
            $casesta = $data['editCaseStat'];
        } else {
            $casesta = '';
        }
        $arr = array(
            'casetype' => $data['editCaseType'],
            'casestat' => $casesta,
            'yourfileno' => $data['fileno'],
            'location' => $data['fileloc'],
            'udfall2' => $data['closedfileno'] . "," . $data['editOtherClass'],
            'udfall' => $data['editCaseSubType'],
            'caption1' => $data['editcase_caption'],
            'atty_resp' => $data['editCaseAttyResp'],
            'atty_hand' => $data['editCaseAttyHand'],
            'para_hand' => $data['editCaseAttyPara'],
            'sec_hand' => $data['editCaseAttySec'],
            'dateenter' => $data['casedate_entered'],
            'dateopen' => $data['casedate_open'],
            'followup' => $data['casedate_followup'],
            'dateclosed' => $data['casedate_closed'],
            'd_r' => $data['casedate_referred'],
            'psdate' => $data['casedate_psdate'],
            'ps' => $data['case_ps'],
            'venue' => $data['editCaseVenue'],
            'rb' => isset($data['case_reffered_by']) ? $data['case_reffered_by'] : 0,
        );
        //echo 'quick note=>' . $data['editcase_quicknote'];
        // if ($data['editcase_quicknote'] != '') {
        $cond = array('caseno' => $data['caseNo'], 'category' => '1000001');
        $this->db->where($cond);
        $this->db->update('caseact', array('event' => $data['editcase_quicknote']));
        $d = $this->db->affected_rows();

        if ($d == 0) {
            // echo "hi"; exit;
            $cond = array('caseno' => $data['caseNo'], 'category' => '1000001');
            //$this->db->where($cond);
            $this->db->insert('caseact', array('event' => $data['editcase_quicknote'], 'caseno' => $data['caseNo'], 'category' => '1000001', 'mainnotes' => 0));
        }
        // }
        //echo $this->db->last_query();exit;
        //if ($data['caseedit_message1'] != '') {
        $cond = array('caseno' => $data['caseNo'], 'category' => '1000002');
        $this->db->where($cond);
        $this->db->update('caseact', array('event' => $data['caseedit_message1']));
        //}
        //if ($data['caseedit_message2'] != '') {
        $cond = array('caseno' => $data['caseNo'], 'category' => '1000005');
        $this->db->where($cond);
        $this->db->update('caseact', array('event' => $data['caseedit_message2']));
        //}
        //if ($data['caseedit_message3'] != '') {
        $cond = array('caseno' => $data['caseNo'], 'category' => '1000006');
        $this->db->where($cond);
        $this->db->update('caseact', array('event' => $data['caseedit_message3']));
        //}
        $cond = array('caseno' => $data['caseNo'], 'category' => '1000007');
        $this->db->where($cond);
        $this->db->update('caseact', array('event' => $data['caseedit_message4']));
        
        $this->db->where("caseno", $data['caseNo']);
        $this->db->update('case', $arr);

        return $this->db->affected_rows();
    }

    public function dateFormatChange($variable) {
        if ($variable != '') {
            $variable = date('Y-m-d', strtotime($variable)) . " " . date('H:i:s', strtotime($variable));
        } else {
            $variable = '1899-12-30 00:00:00';
        }
        return $variable;
    }

    public function cloneParties($data) {

        $cond = array('caseno' => $data->caseno, 'cardcode' => $data->cardcode);
        unset($data->casecard_no);
        $checkRecord = $this->common_functions->check_record_exists('casecard', $cond);
        $response = array();
        if ($checkRecord < 1) {
            $res = $this->db->insert('casecard', $data);

            if (!$res) {
                $response = array('status' => 0, 'message' => 'Something went wrong');
            }
        }
        return $response;
    }

    public function cloneInjuries($data) {
        unset($data->injury_id);
        $response = array();
        if (!$this->db->insert('injury', $data)) {
            $response = array('status' => 0, 'message' => 'Something went wrong');
        } else {
            $response = $this->db->insert_id();
        }
        return $response;
    }

    public function cloneCaseActivity($data) {
        unset($data->id);
        $res = $this->db->insert('caseact', $data);

        if (!$res) {
            $response = array('status' => 0, 'message' => 'Something went wrong');
        }
    }

    public function setPartiesCaseSpecific($data) {
        if (isset($data['parties_party_sheet'])) {
            $partySheet = $data['parties_party_sheet'];
        } else {
            $partySheet = 0;
        }

        $res = $this->db->get_where('casecard', array('caseno' => $data['caseNo'], 'cardcode' => $data['partiesCardCode']));
        $arr = array(
            'officeno' => $data['parties_office_number'],
            'side' => isset($data['parites_side']) ? $data['parites_side'] : '',
            'type' => $data['parties_case_specific'],
            'notes' => $data['parties_notes'],
            'flags' => $partySheet,
        );

        //echo "ok";print_r($arr);exit;

        if (!empty($res)) {
            $this->db->where("caseno", $data['caseNo']);
            $this->db->where("cardcode", $data['partiesCardCode']);
            $this->db->update('casecard', $arr);
            //echo $this->db->last_query();exit;
            //return $this->db->affected_rows();
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                return 0;
            } else {
                return 1;
            }
        } else {
            $arr['caseno'] = $data['caseNo'];
            $arr['cardcode'] = $data['partiesCardCode'];

            $this->db->insert('casecard', $arr);
            //  echo $this->db->last_query();exit;
            return $this->db->insert_id();
        }
    }

    public function getCaseSpecific($caseno, $cardcode) {
        $res = $this->db->get_where('casecard', array('caseno' => $caseno, 'cardcode' => $cardcode));
        return $res->result();
    }

    public function checkDuplicateCase($first_name, $last_name) {
        $this->db->select('c.*,cd.*,cc.type as cardtype', false);
        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        //$this->db->where("caption1", "$first_name $last_name");
        $this->db->where("first", "$first_name");
        $this->db->where("last", "$last_name");
        $result = $this->db->get('case c');
        //echo $this->db->last_query();exit;
        return $result->result();
    }

    public function getReletedCaseListing($start = 0, $limit = 10, $where = '', $order = '', $count = 0, $singleFilter = '') {

        $this->db->select('related');
        $this->db->where("caseno", "$where");
        //$this->db->where("caseno","203");
        $result = $this->db->get('casekey');

        $releted = $result->row();

        if (!empty($releted) && $releted->related > 0) {

            $select = array('cd.caseno', 'cd.casetype', 'cd.casestat', 'cd.atty_hand', 'cd.caption1');
            if ($order != '' && is_array($order)) {
                $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
            }
            $this->db->select('cd.caseno,cd.casetype,cd.casestat,cd.atty_hand,cd.caption1');
            $this->db->where("ck.related", $releted->related);
            $this->db->where("ck.caseno != ", "$where");
            if ($singleFilter != '') {
                $this->db->where($singleFilter);
            }

            if ($count == 0) {
                $this->db->limit($limit, $start);
            }
            $this->db->join('case cd', 'ck.caseno = cd.caseno', 'left');
            $result = $this->db->get('casekey ck');
           // echo $this->db->last_query();exit;
            if ($count > 0) {
                return $result->num_rows();
            } else {
                return $result->result();
            }
        }
        return 0;
    }

    //get Maximum related Key
    public function getMaxRelationKey() {
        $this->db->select('max(related) as rel');
        $result = $this->db->get('casekey');
        $rel_new = $result->row();

        return $rel_new->rel;
    }

    public function getNextCalendarEvent($caseno) {

        $this->db->limit(1);
        $this->db->order_by('date', 'asc');
        $this->db->where('date >=', date('Y-m-d H:i:s'));
        $this->db->where('caseno', $caseno);
        $this->db->select('date,venue,attyass,event');
        $query = $this->db->get('cal1');
        //echo $this->db->last_query();exit;
        return $query->row();
    }

    public function getCaseEmails($caseno) {
        $this->db->order_by('e.datecreate', 'desc');
        $this->db->where('e.caseno', $caseno);
        $this->db->select('*');
        $this->db->where('e2.detached', '0');
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $query = $this->db->get('email e');
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function getCaseEmailsCount($caseno) {
        $username = $this->session->userdata('user_data')['initials'];
        $this->db->where('caseno',$caseno);
        $this->db->where('foldername','New Mail');
        $this->db->order_by('datecreate', 'desc');
        return $this->db->count_all_results(strtolower($username).'_mymail');
        
    }

    // public function getRolodexSearchPartiescompany($where = '', $orderby = '') {
    //        //$select = array('c.caseno', 'cd.first', 'c.atty_resp', 'c.atty_hand', 'c.casetype', 'c.casestat', 'cd.type');
    //        $this->db->select('c.cardcode,c.type,c2.firm,c.first,c.last', false);
    //
    //        if ($where != '') {
    //            $this->db->where($where);
    //        }
    //
    //        if ($orderby != '') {
    //            $this->db->order_by($orderby);
    //        }
    //        //$this->db->limit($limit, $start);
    //        $this->db->join('card2 c2', 'c.firmcode = c2.firmcode', 'left');
    //        $result = $this->db->get('card c');
    //
    //        //echo $this->db->last_query();exit;
    //
    //        return $result->result();
    //
    //    }

    public function getPartiesDetais($data) {

        $this->db->where($data);
        $this->db->select('*');
        $this->db->join('case c', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $query = $this->db->get('casecard cc');
        //echo 'query =>'.$this->db->last_query(); exit;
        return $query->result();
    }

    // get particular category details
    public function getCaseActivityByEvent($case_id, $category) {
        $result = $this->db->get_where('caseact', array('caseno' => $case_id, 'category' => $category, 'event !=' => ''));
        return $result->result();
    }

    public function getEventActivityList($case_id) {
        $this->db->where('category BETWEEN 1 AND 26');
        // $this->db->select('event,category');
        $result = $this->db->get_where('caseact', array('caseno' => $case_id, 'event !=' => ''));
        return $result->result();
    }

    public function getCaseListing2($orCond = '') {

        $this->db->select('c.caseno,c.casetype,c.casestat,c.atty_resp,c.atty_hand,cd.first,cd.last,cd.type', false);
        $this->db->where($orCond);
        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $result = $this->db->get('case c');
        //echo 'query =>'.$this->db->last_query(); exit;
        return $result->result();
    }

    // public function editCasecaption($data){
    //
    //        $arr = array(
    //            'caption1' => $data['captionvalue'],
    //        );
    //         $this->db->where("caseno", $data['caseno']);
    //
    //        $this->db->update('case', $arr);
    //       return $this->db->affected_rows();
    //    }

    public function getCaseRActivity($Condition = '') {

        $this->db->select('event');
        $this->db->where($Condition);
        $result = $this->db->get('caseact');
        //echo 'query =>'.$this->db->last_query(); exit;
        return $result->result();
    }

    public function getCasePass($id, $pass) {

        $where = array('caseno' => $id, 'password' => $pass); //, 'cd.type' => $caseData[1]);

        $this->db->where($where);

        $this->db->select('count(*) as cnt', false);

        $result = $this->db->get('case');
        //echo $this->db->last_query();exit;
        return $result->row();
    }

    public function getAjaxInjuryListing($caseId, $start = 0, $limit = 10, $where = '', $order = '', $dir = '', $singleFilter = '') {

        $cond = 'adj1c desc';
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($order != '' && $dir != '') {
            $this->db->order_by($order, $dir);
        } else {
            $this->db->order_by($cond);
        }

        //$this->db->order_by('orderno', 'ASC');
        //$this->db->select('injury_id,adj1c,i_claimno,caseno');
        $this->db->select('*');
        $this->db->where('caseno', $caseId);
        if ($limit > -1) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get('injury');
        //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function getInjuryCount($where = '', $orCond = '') {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCond != '') {
            $this->db->where($orCond);
        }
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->from('card cd');

        return $this->db->count_all_results();
    }

    public function get_all_injury_count($caseId, $singleFilter) {
        $this->db->where('caseno', $caseId);
        if($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->from('injury');
        //echo $this->db->last_query(); exit;
        return $this->db->count_all_results();
    }

    public function get_all_calendar_count($caseId = '') {
        if ($caseId != '') {
            $this->db->where('caseno', $caseId);
        }
        $this->db->from('cal1 c1');
        return $this->db->count_all_results();
    }

    public function get_all_today_tasks_count($currentuser, $singleFilter = "") {
        // $cond = array('DATE(datereq) <= ' => date('Y-m-d'), 'whoto' => $currentuser);
        $cond = array('whoto' => $currentuser);
        $this->db->where($cond);
        if ($singleFilter != "") {
            $this->db->where($singleFilter);
        }
        $this->db->where('(completed = "1899-12-30 00:00:00" OR completed = "0000-00-00 00:00:00")');
        $this->db->from('tasks');
        // $count = $this->db->count_all_results();
        return $this->db->count_all_results();
    }
	 

    public function get_all_event_count($caseId, $where = '') {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        $this->db->select('c1.*, c2.initials as attyh', false);
        $this->db->join('cal2 c2', 'c1.eventno = c2.eventno', 'LEFT');
        $this->db->where(array('caseno' => $caseId, 'event !=' => ''));
        $this->db->from('cal1 c1');
        //echo $this->db->last_query(); exit;
        return $this->db->count_all_results();
    }

    public function get_all_activity_count($caseId, $category, $checkvalue, $check_filename = false,$where = '', $singleFilter="") {
        //echo 'single filter' . $singleFilter; exit;
        if ($singleFilter != '') {
            $cond = 'SELECT count(*) as numrows from caseact WHERE NOT( category BETWEEN 1000000 AND 1000020) and NOT( category BETWEEN 10000010 AND 10000021) AND caseno =' . $caseId .' AND '.$singleFilter;
        }
        else{
            $cond = 'SELECT count(*) as numrows from caseact WHERE NOT( category BETWEEN 1000000 AND 1000020) and NOT( category BETWEEN 10000010 AND 10000021) AND caseno =' . $caseId;
        }
        if($check_filename) {
            $cond .= ' AND filename != ""';
        }
        
         if ($category != '' && ($checkvalue != 1 || $checkvalue == '')) {
            if ($category == '1' && $checkvalue == 0) {
                $cond .= " AND (category in(1,1000021) OR mainnotes = '1') ";
            } else {
                $cond .= " AND (category = '$category' OR mainnotes = '1') ";
            }
        }/* elseif($category != '' && $checkvalue == 1) {
            if ($category == '1') {
                $cond .= " AND (category in(1,1000021) OR mainnotes = '1') ";
            } else {
                $cond .= " AND (category = '$category' OR mainnotes = '1') ";
            }
        }*/

        $result = $this->db->query($cond)->result();
        //echo $this->db->last_query(); exit;
        return $result[0]->numrows;
        //  $this->db->where('caseno',$caseId);
        //        $this->db->from('caseact');
        //        $this->db->count_all_results();
        //echo $this->db->last_query(); exit;
    }

    public function getAttNm($data) {
        $cond = "SELECT CONCAT(fname,' ', lname) as attyname from staff WHERE initials ='" . $data . "'";
        $result = $this->db->query($cond)->result();
        //echo $this->db->last_query(); exit;
        return $result[0]->attyname;
    }

    public function getcaseParties($start = 0, $limit = 10, $where = '', $order = '',$dir='', $orCondition = '', $singleFilter = '') {
        //echo $order; exit;
        $this->db->select('cc.type,IF(cd.firm = "", CONCAT(c.first," ",c.last), cd.firm) cname, c.cardcode,cc.orderno,cc.caseno,cc.casecard_no,cd.firm', false);

        //$this->db->select('*');
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($order != '' && $dir != '') {
            $this->db->order_by($order, $dir);
        }else {
            $this->db->order_by('orderno', ' ASC');
        }
        
        if ($orCondition != '') {
            $this->db->where($orCondition);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->where('c.cardcode != ""');
        $this->db->limit($limit, $start);
        $this->db->join('card c', 'c.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd', 'cd.firmcode = c.firmcode', 'left');
        $q = $this->db->get('casecard cc');
        //echo $this->db->last_query();exit;
        return $q->result();
    }

    public function getPartyCount($where = '', $orCondition = '', $singleFilter = '') {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCondition != '') {
            $this->db->where($orCondition);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        
        
        //$this->db->where('DATE(datereq) <=',date('Y-m-d'));
        //$this->db->from('casecard');
        $this->db->where('c.cardcode != ""');
        //$this->db->where('DATE(datereq) <=',date('Y-m-d'));
        $this->db->from('casecard cc');
        $this->db->join('card c', 'c.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd', 'cd.firmcode = c.firmcode', 'left');
        
        //echo $this->db->last_query();exit;
        //return $this->db->count_all_results();

        return $this->db->count_all_results();
    }

    public function getVenue($cardcode) {
        $cond = "SELECT c2.venue,c.last FROM card2 c2 LEFT JOIN card c ON c.firmcode = c2.firmcode WHERE c.cardcode =" . $cardcode;
        return $result = $this->db->query($cond)->result();
    }

    public function getRefName($cardcode) {
        $cond = "SELECT c.first,c.last,c2.firm,c2.venue FROM card c LEFT JOIN card2 c2 ON c2.firmcode = c.firmcode WHERE c.cardcode = " . $cardcode;
        return $result = $this->db->query($cond)->result();
    }

    /*     * **** Get party details for adding in injury form (Namrata Dubey) ****************** */

    public function getmainpartydetail($caseno) {
        $this->db->select('cd.occupation,cd2.address1,cd2.address2,cd2.city,cd2.state,cd2.zip');
        $this->db->from('casecard cc');
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd2.firmcode = cd.firmcode', 'left');
        $this->db->where('cc.caseno', $caseno);
        $this->db->where('cc.orderno', 1);
        $q = $this->db->get();
        return $q->result_array();
    }

    public function getinjurydetailsbyid($injuryid = "") {

        $this->db->select('*');
        $this->db->from('injury');
        $this->db->where('injury_id', $injuryid);
        $q = $this->db->get();
        return $q->result_array();
    }

    public function countinsuranceparty($caseno = "") {

        $this->db->select('*');
        $this->db->from('casecard');
        $this->db->where('caseno', $caseno);
        $this->db->where('type', 'INSURANCE');
        $q = $this->db->get();
        return $q->num_rows();
    }
    
    public function countemployer($caseno = "") {

        $this->db->select('*');
        $this->db->from('casecard');
        $this->db->where('caseno', $caseno);
        $this->db->where('type', 'EMPLOYER');
        $q = $this->db->get();
        return $q->num_rows();
    }
    
    public function countdefenseatt($caseno = "") {

        $this->db->select('*');
        $this->db->from('casecard');
        $this->db->where('caseno', $caseno);
        $this->db->where('type', 'ATTORNEY');
        $q = $this->db->get();
        return $q->num_rows();
    }
    
    public function countdoc($caseno = "") {
        $type = array('DR', 'DR-AME','DR-CONSULT', 'DR-IME','DR-PQME', 'DR-QME','DR-PTP');
        $this->db->select('*');
        $this->db->from('casecard');
        $this->db->where('caseno', $caseno);
        $this->db->where_in('type',$type );
        $q = $this->db->get();
        return $q->num_rows();
    }
    
    
    public function getinsuranceparty($caseno = "", $selected_ins_party = "") {
        $this->db->select("cd3.name, cd3.address1, cd3.address2, cd3.city, cd3.state, cd3.zip, cd3.phone, CONCAT(cd.first,' ', cd.last) AS insname,cd2.firm as ifirm, cd2.address1 as iaddress1, cd2.address2 as iaddress2, cd2.city as icity, cd2.state as istate, cd2.zip as izip, cd.first, cd.last", FALSE);
        $this->db->from('casecard cc');
        //$this->db->where('cc.type', 'INSURANCE'); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selected_ins_party) && $selected_ins_party != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selected_ins_party ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->join('card3 cd3', 'cd3.eamsref = cd2.eamsref', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.casecard_no');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }
    
    public function getdefinsuranceparty($caseno = "", $selected_ins_party = "") {
        $this->db->select("cd3.name, cd3.address1, cd3.address2, cd3.city, cd3.state, cd3.zip, cd3.phone, CONCAT(cd.first,' ', cd.last) AS insname,cd2.firm as ifirm, cd2.address1 as iaddress1, cd2.address2 as iaddress2, cd2.city as icity, cd2.state as istate, cd2.zip as izip, cd.first, cd.last", FALSE);
        $this->db->from('casecard cc');
        $this->db->where('cc.type', 'INSURANCE'); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selected_ins_party) && $selected_ins_party != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selected_ins_party ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->join('card3 cd3', 'cd3.eamsref = cd2.eamsref', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.casecard_no');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }
    
    public function getempparty($caseno = "", $selectedempparty = "") {
        $this->db->select("cd2.firm, cd2.address1, cd2.address2, cd2.city, cd2.state, cd2.zip, cd2.phone1");
        $this->db->from('casecard cc');
        //$this->db->where('cc.type', 'EMPLOYER'); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selectedempparty) && $selectedempparty != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selectedempparty ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.casecard_no');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }
    
    public function getdefempparty($caseno = "", $selectedempparty = "") {
        $this->db->select("cd2.firm, cd2.address1, cd2.address2, cd2.city, cd2.state, cd2.zip, cd2.phone1");
        $this->db->from('casecard cc');
        $this->db->where('cc.type', 'EMPLOYER'); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selectedempparty) && $selectedempparty != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selectedempparty ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.orderno');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }
    
    public function getdefatt($caseno = "", $selectedempparty = "") {
        $this->db->select("cd3.name, cd3.address1, cd3.address2, cd3.city, cd3.state, cd3.zip, cd3.phone, cd3.eamsref, cd.first, cd.last,cd2.firm as dfirm, cd2.address1 as daddress1, cd2.address2 as daddress2, cd2.city as dcity, cd2.state as dstate, cd2.zip as dzip");
        $this->db->from('casecard cc');
        //$this->db->where('cc.type', 'ATTORNEY'); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selectedempparty) && $selectedempparty != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selectedempparty ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->join('card3 cd3', 'cd3.eamsref = cd2.eamsref', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.casecard_no');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }
    
     public function getdedefatt($caseno = "", $selectedempparty = "") {
        $this->db->select("cd3.name, cd3.address1, cd3.address2, cd3.city, cd3.state, cd3.zip, cd3.phone, cd3.eamsref, cd.first, cd.last,cd2.firm as dfirm, cd2.address1 as daddress1, cd2.address2 as daddress2, cd2.city as dcity, cd2.state as dstate, cd2.zip as dzip,cd.suffix as suffix,cd.business as phn_new");
        $this->db->from('casecard cc');
        $this->db->where('cc.type', 'ATTORNEY'); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selectedempparty) && $selectedempparty != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selectedempparty ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->join('card3 cd3', 'cd3.eamsref = cd2.eamsref', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.casecard_no');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }

    public function getfirstinjuryid($caseNo = "") {
        $this->db->select('injury_id');
        $this->db->from('injury');
        $this->db->where('caseno', $caseNo);
        $this->db->order_by('injury_id ASC');
        $this->db->limit(1);
        $q = $this->db->get();
        return $q->result_array();
    }

    //Function to check the checkbox value which has been set by Admin
    public function check_attyr_aattyh_checkbox() {
        $query = "SELECT main1 FROM system WHERE system_id = 1";
        $result = $this->db->query($query)->result();
        return $result[0]->main1;
    }

    //Function to check the Attribute Assigned checkbox value which has been set by Admin
    public function check_attya_checkbox() {
        $query = "SELECT calattyass from system WHERE system_id = 1";
        $result = $this->db->query($query)->result();
        return $result[0]->calattyass;
    }
    
    
     public function getdr($caseno = "", $selectedphy = "") {
        $type = array('DR', 'DR-AME','DR-CONSULT', 'DR-IME','DR-PQME', 'DR-QME','DR-PTP');
        $this->db->select("cd.salutation, cd.first, cd.last, cd.business, cd.suffix, cd2.phone1, cd2.firm");
        $this->db->from('casecard cc');
        $this->db->where_in('cc.type', $type); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selectedphy) && $selectedphy != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selectedphy ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.orderno');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }
    
    public function getdefdr($caseno = "", $selectedphy = "") {
        $type = array('DR', 'DR-AME','DR-CONSULT', 'DR-IME','DR-PQME', 'DR-QME','DR-PTP');
        $this->db->select("cd.salutation, cd.first, cd.last, cd.suffix, cd2.phone1, cd2.firm");
        $this->db->from('casecard cc');
        $this->db->where_in('cc.type', $type); 
        $this->db->where('cc.caseno', $caseno); 
        if(isset($selectedphy) && $selectedphy != "") {
            $this->db->where("FIND_IN_SET(cd.cardcode, '" . $selectedphy ."')");
        }
        $this->db->join('card cd', 'cd.cardcode = cc.cardcode', 'left');
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->limit(1);
        $this->db->order_by('cc.orderno');
        $q = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $q->result_array();
    }
    
    /*Function to get Quick notes for particular case number*/
    public function getQuickNotes($caseno) {
        $query = "SELECT event FROM caseact WHERE caseno = ".$caseno." AND category = 1000001 LIMIT 1";
        $result = $this->db->query($query)->result();
        return $result[0]->event;
    }

    /*Function to get Message1 for particular case number*/
    public function getMyMessage1($caseno) {
        $query = "SELECT event FROM caseact WHERE caseno = ".$caseno." AND category = 1000002 LIMIT 1";
        $result = $this->db->query($query)->result();
        return $result[0]->event;
    }
    /*Function to get Message2 for particular case number*/
    public function getMyMessage2($caseno) {
        $query = "SELECT event FROM caseact WHERE caseno = ".$caseno." AND category = 1000005 LIMIT 1";
        $result = $this->db->query($query)->result();
        return $result[0]->event;
    }
    /*Function to get Message3 for particular case number*/
    public function getMyMessage3($caseno) {
        $query = "SELECT event FROM caseact WHERE caseno = ".$caseno." AND category = 1000006 LIMIT 1";
        $result = $this->db->query($query)->result();
        return $result[0]->event;
    }
    /*Function to get Message4 for particular case number*/
    public function getMyMessage4($caseno) {
        $query = "SELECT event FROM caseact WHERE caseno = ".$caseno." AND category = 1000007 LIMIT 1";
        $result = $this->db->query($query)->result();
        return $result[0]->event;
    }
    
    /*Function to check count of defendant in party*/
    public function countdefendant($caseno = "") {

        $this->db->select('*');
        $this->db->from('casecard');
        $this->db->where('caseno', $caseno);
        $this->db->where('type', 'DEFENDANT');
        $q = $this->db->get();
        return $q->num_rows();
    }

    /*Function to get party information*/
    public function get_party_info($caseno) {
        $this->db->select('cardcode, type');
        $this->db->from('casecard');
        $this->db->where('caseno = '.$caseno.' AND (type LIKE "CLIENT" OR type LIKE "APPLICANT")');
        $q = $this->db->get();
        $casecard_result = $q->result();
        $result = array();
        $client_count = $applicant_count = 0;
        foreach($casecard_result as $key => $val) {
            if($val->type == 'CLIENT') {
                $client_count += 1;
            } else {
                $applicant_count += 1;
            }
        }
        if($client_count > 0) {
            foreach($casecard_result as $key => $val) {
                $result = $this->db->query('SELECT c.first, c.last, c2.firm FROM casecard cc LEFT JOIN card c ON cc.cardcode = c.cardcode LEFT JOIN card2 c2 ON c.firmcode = c2.firmcode WHERE cc.type LIKE "CLIENT" AND c.cardcode = '.$val->cardcode)->result()[0];
            }
        } else {
            foreach($casecard_result as $key => $val) {
                $result = $this->db->query('SELECT c.first, c.last, c2.firm FROM casecard cc LEFT JOIN card c ON cc.cardcode = c.cardcode LEFT JOIN card2 c2 ON c.firmcode = c2.firmcode WHERE cc.type LIKE "APPLICANT" AND c.cardcode = '.$val->cardcode)->result()[0];
            }
        }
        return $result;
    }
    
     public function getcardType($cardcode,$caseno) {
        $where = array('cc.cardcode' => $cardcode,'cc.caseno'=>$caseno); //, 'cd.type' => $caseData[1]);

        $this->db->where($where);

        $this->db->select('cd.*,cc.type as cardtype', false);
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $result = $this->db->get('casecard cc');
        
        return $result->row();
    }
    
    public function getCaseDetailsforSNS($caseData) {

        $where = array('c.caseno' => $caseData[0]); //, 'cd.type' => $caseData[1]);

        $this->db->where($where);

        $this->db->select('c.caseno,c.casetype,c.casestat,cd.first,cd.email,cd.last,cd.type,cd.home,cd.business,cd.fax,cd.car,cc.notes,c.followup,c.atty_resp', false);
        $this->db->join('case c', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $result = $this->db->get('casecard cc');

        return $result->row();
    }
    
    public function getCaseActivityAll($case_id, $category = '', $chackvalue = '') {
        $this->db->select('*');
        $this->db->where(array('caseno' => $case_id));
        $this->db->where('NOT( category BETWEEN 1000000 AND 1000020) and NOT( category BETWEEN 10000010 AND 10000021)');
        $q = $this->db->get('caseact');
        //echo $this->db->last_query();exit;
        return $q->result();
    }
	
	public function getRecentCases() {
        $this->db->where('user', $this->session->userdata('user_data')['initials']);
		$result = $this->db->get('recent_cases');
        return $result->result();
    }
	
	public function addRecentCases($case){
		$rc_array['user'] = $this->session->userdata('user_data')['initials'];
		$rc_array['caseno'] = $case;
		$this->db->insert('recent_cases', $rc_array);
	}
	
	public function updateRecentCases($cases){
		$this->db->where('user', $this->session->userdata('user_data')['initials']);
        $this->db->update('recent_cases', array('caseno' => $cases));
    }
    
    public function getPartiesDetails($parties){
        $this->db->select('card2.address1, card2.address2, card2.city, card2.state, card2.zip, card2.firm, card.salutation, card.first, card.last, card.suffix');
        $this->db->from('card');
        $this->db->join('card2', 'card.firmcode = card2.firmcode');
        $this->db->where_in('card.cardcode', $parties);
        $q = $this->db->get();
        $partiesAddr = $q->result();
        
        return $partiesAddr;
        // echo '<pre>'; print_r($partiesAddr); exit;
    }

    public function get_details_for_emailparty($cardcase){
        // $this->db->select("case.atty_resp,case.atty_hand,case.para_hand,case.sec_hand ");
        // $this->db->select(" DISTINCT CONCAT(case.atty_resp)  SEPARATOR ';'");
        $this->db->select(' GROUP_CONCAT(CONCAT(case.atty_resp,", ",case.atty_hand,", ",case.para_hand,", ",case.sec_hand)) AS staff_list', false);
        $this->db->from('casecard');
        $this->db->where('casecard.cardcode', $cardcase); 
        $this->db->join('case', 'casecard.caseno = case.caseno', 'left');
        $q = $this->db->get();
        //  echo $this->db->last_query(); exit;
        $tmp_result = $q->result_array()[0]['staff_list'];
        $result = implode(',',array_unique(explode(',', $tmp_result)));
        $result = implode(',',array_filter(explode(',', $result)));
        // echo '<pre>';print_r($result);exit;
        return $result;
    }

    public function remove_case_activity_attachment($case_no, $activity_no, $filename) {
        $this->db->where('caseno = '.$case_no.' AND actno = '.$activity_no);
        $this->db->update('caseact', array('ole3' => null));
        return $this->db->affected_rows();
    }

}
