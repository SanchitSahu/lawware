<?php
class Prospects_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getProspectsdata($prospect_id) {
        $this->db->where('prospect_id', $prospect_id);
        $this->db->select('*');
        $q = $this->db->get('intake');
       // echo $this->db->last_query();exit;
        return $q->row();
    }

     public function getAllRecord(){
        $this->db->select('*');
        $q = $this->db->get('intake');
       // $this->db->order_by('proskey','DESC');
        //$this->db->limit(1);
        return $q->row();
    }
    public function checkDuplicateProspect($first_name, $last_name) {
        $this->db->select('*', false);
        $this->db->where('first', $first_name);
        $this->db->where('last', $last_name);
        $result = $this->db->get('intake');
        return $result->result();
    }

    public function getProspectListing( $where = '')
    {
       $this->db->select('prospect_id,proskey,prosno,first,last,initials0,casetype,casestat,datelastcn,city,state,zip,refcode,refsource,refby,refto,reffee,outrefstat,amount,datepaid,todonext,dateref,pendwith,atty,email,cell,home,business,address1,social_sec,birthdate,type,daterefin,salutation,suffix,mailingh,mailing1,mailing2,mailing3,mailing4,mailing5,fieldsudf', false);
        //$this->db->select('c.cardcode,c.type,c2.firm,c.first,c.last,c2.city,c2.phone1,c2.address1', false);

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
       /* if ($orCond != '') {
            $this->db->where($orCond);
        }

        if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        $this->db->limit($limit, $start);*/

      //  $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $result = $this->db->get('intake');
       //echo $this->db->last_query();exit;
        return $result->result();



    }


   }

?>
