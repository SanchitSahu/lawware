<?php

/**
 * Bhanushankr Joshi @ Solulab
 * Started : 21-8-2017
 */
class Admin_login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * resolve_admin_user_login function.
     *
     * @access public
     * @param mixed $username
     * @param mixed $password
     * @return bool true on success, false on failure
     */
    public function resolve_admin_user_login($username, $password) {
        /* $this->db->where('sa.sa_user_name', $username);
          $this->db->where('sa.sa_password', md5($password));
          $this->db->join('support_checklist sc','sa.sa_id = sc.id');
          $this->db->select('sa.sa_id,sa.sa_user_name,sa.sa_password,sa.sa_email,sa.sa_role,sa.sa_status,sc.uniqueid,sc.firm_id');

          $this->db->get('system_admin sa'); */
        $this->db->select('sa_id,sa_user_name,sa_password,sa_email,sa_role,sa_status');
        $this->db->from('system_admin');
        $this->db->where('sa_user_name', $username);
        $this->db->where('sa_password', md5($password));

        //echo $this->db->last_query(); exit;
        $hash = $this->db->get()->row_array();
        return $hash;
    }

    public function check_email($email) {
        $this->db->where('sa_email', $email);
        $this->db->select('sa_user_name');
        $q = $this->db->get('system_admin');
        $result = $q->row_array();
        $cnt = count($result);
        if (!empty($cnt)) {
            //$token = bin2hex(random_bytes(16));
            $token = md5(uniqid(rand(), true));
            $to = $email;
            //$this->common_functions->reg_send('dinky@solulab.com','Registration email','notification',$body);
            $link = "<a target='_blank' href='" . base_url() . "admin/admin/resetpass/$token'>Link</a>";
            $body = "Hello " . $result['sa_user_name'] . ",<br/>
                        Please click on link to reset your password : $link<br/>
                        Thanks";

            $this->common_functions->reg_send($to, 'Reset Password', $result['sa_user_name'], $body);

            $actData = array('fpassstr' => $token);
            $this->db->where('sa_email', $email);
            $this->db->update('system_admin', $actData);
        }
        return $result;
    }

    public function check_fstr($fstr) {
        $this->db->where('fpassstr', $fstr);
        $this->db->select('sa_email');
        $q = $this->db->get('system_admin');
        return $q->row_array();
    }

    public function change_pass($fstr, $pass) {
        $record = array('sa_password' => md5($pass), 'fpassstr' => '');
        $this->db->where('fpassstr', $fstr);
        $this->db->update('system_admin', $record);
    }

    public function check_pass($cond) {
        $this->db->where($cond);
        $this->db->select('sa_email');
        $q = $this->db->get('system_admin');
        //echo $this->db->last_query(); exit;
        return $result = $q->row_array();
    }

    public function change_password($pass) {
        //echo '<pre>'; print_r($this->session->userdata('admin_user_data')); exit;
        $initial = $this->session->userdata('admin_user_data')['sa_email'];
        $record = array('sa_password' => md5($pass));
        $this->db->where('sa_email', $initial);
        $this->db->update('system_admin', $record);

        $arr = $this->session->userdata('admin_user_data');
        $arr['sa_password'] = md5($pass);
        $this->session->set_userdata('admin_user_data', $arr);
        //echo $this->db->last_query(); exit;
    }

    public function get_timeclock($limit = 10, $start = 0, $singleFilter = '', $order, $dir) {
        //echo $order; exit;
        $this->db->select('initials,signon,signoff');
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($order != '' && $dir != '') {
            $this->db->order_by($order, $dir);
        }
        if ($limit >= 10) {
            $this->db->limit($limit, $start);
        } else {
            $this->db->limit(10);
        }
        $result = $this->db->get('loginfo');
        //echo $this->db->last_query(); exit;
        $row = array();

        foreach ($result->result() as $k => $v) {
            $row[$k] = $v;
            /* $row[$k]->location = $v->location; */
            $row[$k]->initials = $v->initials;
            $row[$k]->signon = $v->signon;
            $row[$k]->signoff = $v->signoff;
        }

        return $row;
    }

    public function get_all_loginfo_count($singleFilter = "") {
        if ($singleFilter != "") {
            $this->db->where($singleFilter);
        }
        $this->db->from('loginfo');
        //echo $this->db->last_query(); exit;
        return $this->db->count_all_results();
    }

    public function get_firm_details() {
//        $arr = array();
//   $arr1 = array();
//   $arr1['firm'][0]='Los Angeles';
//    $arr1['firm'][1]='13232434';
//    $arr[]=$arr1;
//   //print_r($arr);
//print_r(json_encode($arr));//   exit;
//         $actData = array('main1'=>json_encode($arr));
////            //$this->db->where('sa_email',$email);
//         $this->db->update('system',$actData);
        //     $arr = array();
        //$arr1 = array();
        //$arr1['country']='Los Angeles';
        // $arr1['firmTextid']='13232434';
        // $arr[]=$arr1;
        //print_r($arr);
//print_r(json_encode($arr));//   exit;
        // $actData = array('main1'=>json_encode($arr));
////            //$this->db->where('sa_email',$email);
        //  $this->db->update('system',$actData);
        $this->db->select('*');
        $q = $this->db->get('system');
        return $result = $q->row_array();
    }

    public function get_checklist_details() {
        $this->db->select('*');
        $q = $this->db->get('support_checklist');
        return $result = $q->row_array();
    }

    public function get_userdefined_details() {
        $this->db->select('*');
        $result = $this->db->get('user2');
        return $result->result();
        //return $result = $q->row_array();
    }

    public function get_staff_details() {
        $this->db->select('initials,username,isActive,captionAccess');
        $result = $this->db->get('staff');
        return $result->result();
        //return $result = $q->row_array();
    }

    public function getStaffSpecific($initials) {
        $res = $this->db->get_where('staff', array('initials' => $initials));
        //echo $this->db->last_query(); exit;
        return $res->row_array();
    }

    public function deleteStaffMember() {
        $ini = $this->input->post('initials');
        $this->db->where("(atty_resp = '$ini') or (atty_hand = '$ini') or (para_hand = '$ini') or (sec_hand = '$ini')");
        $this->db->select('caseno');
        $q = $this->db->get('case');
        $rowcount = $q->num_rows();
        if ($rowcount == 0) {
            $this->db->where('initials', $ini);
            $this->db->delete('staff');
            //echo $this->db->last_query(); exit;
        }
        return $rowcount;
    }

    public function get_group_details() {
        $this->db->select('groupname,groupkey');
        $result = $this->db->get('staff3');
        return $result->result();
        //return $result = $q->row_array();
    }

    /*public function get_cases($wherearray) {
        $where = [];
        if ($wherearray['case.casetype'] != "") {
            $where['case.casetype'] = $wherearray['case.casetype'];
        }
        if ($wherearray['case.casestat'] != "") {
            $where['case.casestat'] = $wherearray['case.casestat'];
        }
        if ($wherearray['case.atty_resp'] != "") {
            $where['case.atty_resp'] = $wherearray['case.atty_resp'];
        }
        if ($wherearray['case.atty_hand'] != "") {
            $where['case.atty_hand'] = $wherearray['case.atty_hand'];
        }
        if ($wherearray['case.para_hand'] != "") {
            $where['case.para_hand'] = $wherearray['case.para_hand'];
        }
        if ($wherearray['case.sec_hand'] != "") {
            $where['case.sec_hand'] = $wherearray['case.sec_hand'];
        }


        $andwhere = [];
        if ($wherearray["first_name"] != "" && $wherearray["last_name"] != "") {
            $andwhere = "(card.first LIKE '" . $wherearray["first_name"] . "%'or card.last LIKE '" . $wherearray["last_name"] . "%' )";
        } else if ($wherearray["first_name"] != "" && $wherearray["last_name"] == "") {
            $andwhere = "(card.first LIKE '" . $wherearray["first_name"] . " )";
        } else if ($wherearray["first_name"] == "" && $wherearray["last_name"] != "") {
            $andwhere = "(card.last LIKE '" . $wherearray["last_name"] . "%' )";
        }

        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($andwhere)) {
            $this->db->where($andwhere);
        }

        $this->db->select('DISTINCT(case.caseno)');
        $this->db->join('casecard', 'case.caseno = casecard.caseno', 'left');
        $this->db->join('card', 'card.cardcode = casecard.cardcode', 'left');
        $q = $this->db->get('case');
        echo $this->db->last_query(); exit;
        return $rowcount = $q->num_rows();
    }*/
    
    public function get_cases($wherearray) {
        $where = [];
        if ($wherearray['case.casetype'] != "") {
            $where['case.casetype'] = $wherearray['case.casetype'];
        }
        if ($wherearray['case.casestat'] != "") {
            $where['case.casestat'] = $wherearray['case.casestat'];
        }
        if ($wherearray['case.atty_resp'] != "") {
            $where['case.atty_resp'] = $wherearray['case.atty_resp'];
        }
        if ($wherearray['case.atty_hand'] != "") {
            $where['case.atty_hand'] = $wherearray['case.atty_hand'];
        }
        if ($wherearray['case.para_hand'] != "") {
            $where['case.para_hand'] = $wherearray['case.para_hand'];
        }
        if ($wherearray['case.sec_hand'] != "") {
            $where['case.sec_hand'] = $wherearray['case.sec_hand'];
        }


        $andwhere = [];
        if ($wherearray["first_name"] != "" && $wherearray["last_name"] != "") {
            $andwhere = "card.last >='" . $wherearray["first_name"] . "' AND card.last <= '" . $wherearray["last_name"]."'";
        } /*else if ($wherearray["first_name"] != "" && $wherearray["last_name"] == "") {
            $andwhere = "card.last LIKE '" . $wherearray["first_name"];
        } else if ($wherearray["first_name"] == "" && $wherearray["last_name"] != "") {
            $andwhere = "card.last LIKE '" . $wherearray["last_name"] . "%'";
        }*/

        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($andwhere)) {
            $this->db->where($andwhere);
        }

        $this->db->select('DISTINCT(case.caseno)');
        $this->db->join('casecard', 'case.caseno = casecard.caseno', 'left');
        $this->db->join('card', 'card.cardcode = casecard.cardcode', 'left');
        $q = $this->db->get('case');
 //       echo $this->db->last_query(); exit;
        return $rowcount = $q->num_rows();
    }

    public function reassign_cases($wherearray, $fields) {
        $this->db->start_cache();
        $where = [];
        if ($wherearray['case.casetype'] != "") {
            $where['case.casetype'] = $wherearray['case.casetype'];
        }
        if ($wherearray['case.casestat'] != "") {
            $where['case.casestat'] = $wherearray['case.casestat'];
        }
        if ($wherearray['case.atty_resp'] != "") {
            $where['case.atty_resp'] = $wherearray['case.atty_resp'];
        }
        if ($wherearray['case.atty_hand'] != "") {
            $where['case.atty_hand'] = $wherearray['case.atty_hand'];
        }
        if ($wherearray['case.para_hand'] != "") {
            $where['case.para_hand'] = $wherearray['case.para_hand'];
        }
        if ($wherearray['case.sec_hand'] != "") {
            $where['case.sec_hand'] = $wherearray['case.sec_hand'];
        }

        $andwhere = [];
        if ($wherearray["first_name"] != "" && $wherearray["last_name"] != "") {
            $andwhere = "(card.first LIKE '" . $wherearray["first_name"] . "%'or card.last LIKE '" . $wherearray["last_name"] . "%' )";
        } else if ($wherearray["first_name"] != "" && $wherearray["last_name"] == "") {
            $andwhere = "(card.first LIKE '" . $wherearray["first_name"] . " )";
        } else if ($wherearray["first_name"] == "" && $wherearray["last_name"] != "") {
            $andwhere = "(card.last LIKE '" . $wherearray["last_name"] . "%' )";
        }

        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!empty($andwhere)) {
            $this->db->where($andwhere);
        }
        $this->db->stop_cache();

        $this->db->select('DISTINCT(case.caseno)');
        $this->db->join('casecard', 'case.caseno = casecard.caseno', 'left');
        $this->db->join('card', 'card.cardcode = casecard.cardcode', 'left');
        $q = $this->db->get('case');
        $rowcount = $q->num_rows();

        $this->db->set($fields);
        $this->db->update('case LEFT JOIN casecard ON case.caseno = casecard.caseno LEFT JOIN card ON card.cardcode = casecard.cardcode');
        $this->db->flush_cache();
        //return $this->db->affected_rows();
        return $rowcount;
    }

    public function get_task($where) {

        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->where("tasks.completed = ",'0000-00-00 00:00:00');
        $this->db->select('*');
        $this->db->join('case', 'case.caseno = tasks.caseno');
        $q = $this->db->get('tasks');
       // echo $this->db->last_query(); exit;
        
        return $rowcount = $q->num_rows();
    }

    public function reassign_tasks($where, $fields) {

        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->where("tasks.completed = ",'0000-00-00 00:00:00');
        $this->db->set($fields);
        $this->db->update('tasks INNER JOIN `case` ON case.caseno = tasks.caseno');
        return $this->db->affected_rows();
    }

    public function get_events($where) {

        $this->db->select('DISTINCT(cal1.eventno)');
        $this->db->from('cal1');
        $this->db->join('cal2', 'cal2.eventno = cal1.eventno');
        if (!empty($where)) {
            $this->db->where($where);
        }
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function reassign_eventss($where, $fields) {

        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->set($fields);
        $this->db->update('cal2 INNER JOIN cal1 on cal1.eventno = cal2.eventno');
        return $this->db->affected_rows();
    }

    public function getcaseactReassign($where) {

        // $this->db->select('DISTINCT(caseact.id)');
        $this->db->select('DISTINCT(case.caseno)');
        $this->db->from('caseact');
        $this->db->join('case', 'caseact.caseno = case.caseno');
        if (!empty($where)) {
            $this->db->where($where);
        }
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function updatecaseactassign($where, $fields) {
        $this->db->start_cache();
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->stop_cache();

        $this->db->select('DISTINCT(case.caseno)');
        $this->db->from('caseact');
        $this->db->join('case', 'caseact.caseno = case.caseno');
        $result = $this->db->get();
        $rowcount = $result->num_rows();

        $this->db->set($fields);
        $this->db->update('caseact INNER JOIN `case` on case.caseno = caseact.caseno');
        // return $this->db->affected_rows();
        return $rowcount;
    }

    public function get_groupname() {
        $this->db->select('*');
        $this->db->order_by('group_id', 'desc');
        $result = $this->db->get('group');
        return $result->result();
    }

    public function get_groupmemberByid($id) {
        $this->db->where('group_id', $id);
        $this->db->select('*');
        $this->db->order_by('initials', 'ASC');
        $result = $this->db->get('group_member');
        //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function get_groupnamebyID($id) {
        $this->db->where('group_id', $id);
        $this->db->select('group_name,module_name,group_id');
        $result = $this->db->get('group');
        //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function deletegroupname($id) {
        $this->db->where('group_id', $id);
        $this->db->delete('group');

        $this->db->where('group_id', $id);
        $this->db->delete('group_member');
    }

    public function get_existinginitialname($groupid) {
        $groupid = $_POST['groupid'];
        $this->db->order_by("initials", "asc");
        $this->db->where('group_id', $groupid);
        $this->db->select('*');
        $result = $this->db->get('group_member');
        return $result->result();
    }

    public function existingmemberreduce($groupid) {
        $result = $this->db->query("SELECT DISTINCT(staff.initials) FROM staff  WHERE (SELECT COUNT(*) FROM group_member WHERE initials =  staff.initials AND group_id = " . $groupid . ") <= 0 order by initials ASC");
        return $result->result();
    }

    public function deletegroupmembername($id) {
        $this->db->where('member_id', $id);
        $this->db->delete('group_member');
    }

    public function deletegroupmemberbygroup($id) {
        $this->db->where('group_id', $id);
        $this->db->delete('group_member');
    }

    public function get_initialname() {
        $this->db->select('initials');
        $result = $this->db->get('staff');
        return $result->result();
    }

    public function get_duplicategroup($groupname, $modulename) {
        $this->db->where("module_name", $modulename);
        $this->db->where("group_name", $groupname);
        $this->db->select('group_name');
        $result = $this->db->get('group');
        return $result->num_rows();
    }

    public function getstafflist() {
        $this->db->select('initials');
        $this->db->from('staff');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getcasetypelist() {
        $this->db->select('poptype');
        $this->db->from('system');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getcasestatusist() {
        $this->db->select('popstatus');
        $this->db->from('system');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function checkduplicateemail($email = "", $inits = "") {
        $this->db->select('initials');
        $this->db->from('staff');
        $this->db->where('emailid', $email);
        if ($inits != "") {
            $this->db->where('initials !=', $inits);
        }
        $result = $this->db->get();
//        echo $this->db->last_query(); exit;
        return $result->result_array();
    }

    public function checkduplicateinitial($initials = "", $inits = "") {
        $this->db->select('initials');
        $this->db->from('staff');
        $this->db->where('initials', $initials);
        if ($inits != "") {
            $this->db->where('initials !=', $inits);
        }
        $result = $this->db->get();
        return $result->result_array();
    }

    public function forcelogoutusers($data) {
        $this->db->update('staff', $data);
    }
    
    public function createmailtable($initials = "") {
         $query = "CREATE TABLE " . strtolower($initials) . "_mymail (
                    `id` int(11) NOT NULL AUTO_INCREMENT, 
                    `parcelid` VARCHAR(15) DEFAULT NULL,
                    `folderid` INT(11) DEFAULT NULL,
                    `foldername` VARCHAR(25) DEFAULT NULL,
                    `priority` VARCHAR(1) DEFAULT NULL,
                    `emailtype` INT(11) DEFAULT NULL,
                    `subject` VARCHAR(80) DEFAULT NULL,
                    `body` LONGTEXT,
                    `caseno` INT(11) DEFAULT NULL,
                    `catno` INT(11) DEFAULT NULL,
                    `datecreate` DATETIME DEFAULT NULL,
                    `datesent` DATETIME DEFAULT NULL,
                    `dateread` DATETIME DEFAULT NULL,
                    `readyet` VARCHAR(1) DEFAULT NULL,
                    `whofrom` VARCHAR(100) DEFAULT NULL,
                    `whoto` LONGTEXT,
                    `whotoexternal` LONGTEXT,
                    `cc` LONGTEXT,
                    `bcc` LONGTEXT,
                    `encrypt1` INT(11) DEFAULT NULL,
                    `password` VARCHAR(10) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                  ) ENGINE=INNODB DEFAULT CHARSET=latin1";
        $result = $this->db->query($query);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

}

?>