<?php

/**
 *
 */
class Rolodex_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getRolodexListing($start = 0, $limit = 10, $where = '', $orCond = '', $order = '') {

        $select = array('cd.cardcode','cd.first', 'caseCount', 'cd2.city', 'cd2.firm', 'cd.home', 'cd2.phone1', 'cd.business', 'cd.social_sec', 'cd.birth_date', 'cd.type', 'cd.licenseno', 'cd.specialty', 'cd.cardcode');
        $this->db->select('cd.first,cd.last,cd.type,cd.cardcode,cd.home,cd.licenseno,cd.specialty,cd.birth_date,cd.social_sec,cd2.firm,cd2.phone1,cd.business,cd2.city, (SELECT COUNT(*) AS numrows FROM casecard WHERE cardcode = cd.cardcode AND caseno!=0) AS caseCount', false);

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCond != '') {
            $this->db->where($orCond);
        }

        if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }

        if($limit != -1) {
            $this->db->limit($limit, $start);
        }

        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $result = $this->db->get('card cd');
        /*echo $this->db->last_query(); exit;*/
        return $result->result();
    }

    public function getRolodexCount($where = '', $orCond = '') {
        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }
        if ($orCond != '') {
            $this->db->where($orCond);
        }
        $this->db->join('card2 cd2', 'cd.firmcode = cd2.firmcode', 'left');
        $this->db->from('card cd');

        return $this->db->count_all_results();
    }


     public function getCaseCount($where = '', $orCond = '') {
        $this->db->where($where);
        if (count($orCond) > 0) {
            $orString = '';
            foreach($orCond as $field => $value) {
                $orString .= $field . ' ' . $value . ' OR ';
            }
            $this->db->where('(' . trim($orString, ' OR ') . ')');
        }
        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        return count($this->db->get('case c')->result());
    }

    public function getRolodexRecord($cardcode) {

        $this->db->where('cardcode', $cardcode);
        $this->db->select('card.salutation,card.suffix,card.title,card.type,card.letsal,card.first,card.middle,card.last,card.popular,card2.firm,card2.address1,card2.address2,card2.city,card2.state,card2.zip,card2.venue,card2.eamsref,card.business,card.home,card.fax as card_fax,card.beeper,card.email,card.comments,card2.phone1,card.car,card2.phone2,card2.fax as card2_fax,card2.fax2,card2.tax_id,card.social_sec,card.birth_date,card.licenseno,card.specialty,card.mothermaid,card.cardcode,card.firmcode,card.interpret,card.language,card.origchg as card_origchg,card.lastchg as card_lastchg,card.origdt as card_origdt,card.lastdt as card_lastdt,card2.origchg as card2_origchg,card2.lastchg as card2_lastchg,card2.origdt as card2_origdt,card2.lastdt as card2_lastdt,card.comments as card_comments,card2.comments as card2_comments,card2.mailing1,card2.mailing2,card2.mailing3,card2.mailing4,card.occupation,card.employer,card.date_of_hire,card.picture,card3.name as eamsname');
        $this->db->join('card2', 'card.firmcode = card2.firmcode', 'left');
        $this->db->join('card3', 'card2.eamsref = card3.eamsref', 'left');
        $q = $this->db->get('card');
        return $q->row();
    }

    public function rolodex_profile($imagename,$cardcode)
    {
        $record = array('picture'=>$imagename);
        $this->db->where('cardcode',$cardcode);
        $this->db->update('card',$record);
    }

    public function getCaseListing($start = 0, $limit = 10, $where = '', $orCond = '', $order = '') {
        $select = array(
            'c.caseno',
            'cd.first',
            'c.atty_resp',
            'c.atty_hand',
            'c.casetype',
            'cc.type',
            'c.casestat',
            'c.yourfileno',
            'cd.social_sec'
        );

        $this->db->select('*,cc.type cardtype');

        if ($where != '' && is_array($where)) {
            $this->db->where($where);
        }

        if (count($orCond) > 0) {
            $orString = '';
            foreach($orCond as $field => $value) {
                $orString .= $field . ' ' . $value . ' OR ';
            }
            $this->db->where('(' . trim($orString, ' OR ') . ')');
        }

        if ($order != '' && is_array($order)) {
            $this->db->order_by($select[$order[0]['column']], $order[0]['dir']);
        }
        
        if ($limit > -1) {
            if ($limit >= 5) {
                $this->db->limit($limit, $start);
            } else {
                $this->db->limit(5);
            }
        }
        if ($unique == "1") {
            $this->db->group_by("cd.first");
            $this->db->group_by("cd.last");
        } else {
            $this->db->group_by('c.caseno');
        }

        $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
        $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
        $result = $this->db->get('case c');
        $result1 = $result->result();
        //echo $this->db->last_query(); exit;
        return $result1;
    }

    public function getCaseDetails($caseno,$cardcode)
    {

        $query = $this->db->query("
            SELECT c.caseno, i.case_no,i.eamsno,i.injury_id,i.adj1c,cc.type,c.atty_resp,c.atty_hand,c.para_hand,c.sec_hand, CONCAT(cd.home,'  ',cd.business) AS phone, CONCAT(cd.first,' ',cd.last) AS fullname,c2.firm ,c.casestat,c.casetype,c.followup,c.dateenter,c.yourfileno,cd.social_sec FROM casecard cc
            LEFT JOIN `case` c ON cc.caseno = c.caseno
            LEFT JOIN card cd ON cc.cardcode = cd.cardcode
            LEFT JOIN card2 c2 ON c2.firmcode= cd.firmcode
            LEFT JOIN injury i ON i.caseno = c.caseno
            WHERE c.caseno = $caseno AND cc.cardcode = $cardcode
        ");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function updatepnccase($prospect_id,$case)
    {
            $this->db->where('prospect_id',$prospect_id);
            $this->db->update('intake', array('caseno' => $case));
    }
    
    public function checkDuplicateRolodex($first_name, $last_name) {
        $this->db->select('CONCAT(c.first," ", c.last) as name,cd.address1,cd.city,c.social_sec,cd.firm', false);
        $this->db->where("first", "$first_name");
        $this->db->where("last", "$last_name");
        $this->db->join('card2 cd', 'cd.firmcode = c.firmcode', 'left');
        $result = $this->db->get('card c');
        return $result->result();
    }

}

?>
