<?php

class Email_conversation_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    /* create_email_conversation provide facility to create conversation and return inserted id */
    public function create_email_conversation($email_con_subject,$email_con_people,$email_con_initiate_by)
    {
        $email_conversation_data = array(
            'email_con_date' => date('Y-m-d h:i:s'),
            'email_con_subject' => $email_con_subject,
            'email_con_people' => $email_con_people,
            'email_con_initiate_by' => $email_con_initiate_by
        );
        if ($this->db->insert('email_conversion', $email_conversation_data)) {
            // echo $this->db->last_query();exit;
            $id = $this->db->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }
    /* create_email_conversation ends here */
    /*fetch all conversation starts here */
    public function grab_email_conversation($current_user,$limit = DEFAULT_LISTING_LENGTH, $offset = 0)
    {
        $this->db->distinct();
        $this->db->select("email_con_id");
        $this->db->where('email_con_initiate_by',$current_user);
        $this->db->or_where("email_con_people LIKE '%$current_user%'");
        $this->db->order_by('email_con_id', 'desc');
        $result = $this->db->get("email_conversion");//, $limit, $offset);
        //echo $this->db->last_query();
        return $result->result();
    }
    /*fetch all conversation ends here */

    /*fetch conversation upon id */
    public function grab_conversation_from_id($id)
    {
        $this->db->where('email_con_id',$id);
        $result = $this->db->get("email_conversion");//, $limit, $offset);
        return $result->result();
    }
    /*fetch conversation upon id ends here */
}

?>