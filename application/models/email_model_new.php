<?php

class Email_model_new extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function get_single_email($current_list = '', $parcelid) {

        if($current_list != ''){
            if($current_list == 'new'){
            $foldername = 'New Mail';
            }
            if($current_list == 'old'){
                $foldername = 'Old Mail';
            }

            if($current_list == 'sent'){
                $foldername = 'Sent Mail';
            }

            if($current_list == 'delete'){
                $foldername = 'Deleted';
            }
            
            if($current_list == 'Self'){
                $foldername = 'New Mail';
            }
            $username = $this->session->userdata('user_data')['initials'];
            $this->db->where('foldername',$foldername);
        }
        else{
            $usernm = explode('_',$parcelid);
            $username = $usernm[1];
        }
        $this->db->where('parcelid',$parcelid);
        $this->db->group_by('parcelid');
        $this->db->order_by('datecreate', 'desc');
        return $result = $this->db->get(strtolower($username).'_mymail')->result();
        //echo $this->db->last_query(); exit;
    }

    public function update_email_read($current_user, $filename, $iore='I') {
        $filename_id = explode(',', $filename);
        $this->db->where_in('parcelid',$filename_id);
        //if ($iore !='E' ){$this->db->where('whoto', $current_user);}
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update(strtolower($current_user).'_mymail', array('readyet' => 'Y', 'dateread'=> $dateread))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_unread($current_user, $filename, $iore='I') {
        $filename_id = explode(',', $filename);
        $this->db->where_in('parcelid',$filename_id);
        //if ($iore !='E' ){$this->db->where('whoto', $current_user);}
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update(strtolower($current_user).'_mymail', array('readyet' => '', 'dateread'=> $dateread))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_old($current_user, $filename, $iore='I') {
        $this->db->where_in('parcelid', explode(',', $filename));
        $this->db->where('foldername', 'New Mail');
        //if ($iore !='E' ){$this->db->where('whoto', $current_user);}
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update(strtolower($current_user).'_mymail', array('foldername' => 'Old Mail'))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_urgent($current_user, $filename) {
        $this->db->where_in('parcelid', explode(',', $filename));
        // $this->db->where('whoto', $current_user);
        if ($this->db->update(strtolower($current_user).'_mymail', array('priority' => 'Y'))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_trash($current_user, $filename,$foldername) {
        $this->db->where_in('parcelid', explode(',', $filename));
        $dateread = date('Y-m-d h:i:s');
        if($foldername == 'new'){
            $foldername = 'New Mail';
        }
        if($foldername == 'old'){
            $foldername = 'Old Mail';
        }
        
        if($foldername == 'sent'){
            $foldername = 'Sent Mail';
        }
        
        if($foldername == 'delete'){
            $foldername = 'Deleted';
        }
        
        if($foldername == 'Self'){
            $foldername = 'New Mail';
        }
        if($foldername == 'Deleted')
        {
            $this->db->where('foldername', $foldername);
            if ($this->db->update(strtolower($current_user).'_mymail', array('foldername' => 'Permanently Deleted','folderid'=>'-2'))) {
            //echo $this->db->last_query(); exit;
                return true;
            } else {
                return false;
            }
        }
        else{
            $this->db->where('foldername', $foldername);
            if ($this->db->update(strtolower($current_user).'_mymail', array('foldername' => 'Deleted','folderid'=>'-1'))) {
            //echo $this->db->last_query(); exit;
                return true;
            } else {
                return false;
            }
        }
        
        
    }

   public function get_new_emails($singleFilter,$order,$dir, $limit = DEFAULT_LISTING_LENGTH, $offset = 0,$defaultFilter='',$caseType ='',$emailType) {
        $username = $this->session->userdata('user_data')['initials'];
        $current_list = $_POST['current_list'];
        //$this->db->select("subject,body,whofrom", false);
        if($current_list == 'new'){
            $foldername = 'New Mail';
        }
        if($current_list == 'old'){
            $foldername = 'Old Mail';
        }
        
        if($current_list == 'sent'){
            $foldername = 'Sent Mail';
        }
        
        if($current_list == 'delete'){
            $foldername = 'Deleted';
        }
        
        $this->db->where('foldername',$foldername);
        if ($defaultFilter != '') {
            $this->db->where($defaultFilter);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($caseType == 'withcase') {
           $this->db->where('caseno IS NOT NULL',null, false);
           $this->db->where('caseno !=','0');
        } else if ($caseType == 'withoutcase') {
           //$this->db->where('caseno IS NULL',null, true);
          // $this->db->or_where('caseno','0');
            $where  = "(caseno IS NULL OR ";
            $where .= "caseno = 0)";
            $this->db->where($where);
        }
        if ($emailType == 'interemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom NOT LIKE '%@%' )");
        } else if ($emailType == 'extemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom LIKE '%@%'  )");
        }
        
        if ($order !='' && $dir !='') {
            $this->db->order_by('datecreate', 'desc');
            $this->db->order_by($order,$dir);
            
            
            /*$this->db->order_by("e2.readyet","ASC");*/

        } else {
            $this->db->order_by('datecreate', 'desc');
        }
        return $result = $this->db->get(strtolower($username).'_mymail', $limit, $offset)->result();
        //echo $this->db->last_query();exit;
        //echo '<pre>'; print_r($result); exit;
    }
    
    public function get_new_messages_count($singleFilter='',$defaultFilter='',$caseType ='',$emailType) {
        $current_list = $_POST['current_list'];
        if($current_list == 'new'){
            $foldername = 'New Mail';
        }
        if($current_list == 'old'){
            $foldername = 'Old Mail';
        }
        
        if($current_list == 'sent'){
            $foldername = 'Sent Mail';
        }
        
        if($current_list == 'delete'){
            $foldername = 'Deleted';
        }
        $username = $this->session->userdata('user_data')['initials'];
        $this->db->where('foldername',$foldername);
        if ($defaultFilter != '') {
            $this->db->where($defaultFilter);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($caseType == 'withcase') {
           $this->db->where('caseno IS NOT NULL',null, false);
           $this->db->where('caseno !=','0');
        } else if ($caseType == 'withoutcase') {
           //$this->db->where('caseno IS NULL',null, true);
           //$this->db->or_where('caseno','0');
            $where  = "(caseno IS NULL OR ";
            $where .= "caseno = 0)";
            $this->db->where($where);
        }
        
        if ($emailType == 'interemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom NOT LIKE '%@%' )");
        } else if ($emailType == 'extemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom LIKE '%@%'  )");
        }
        $this->db->from(strtolower($username).'_mymail');
        return $this->db->count_all_results();
    }
    
    public function filter_count_new(){ 
        $username = $this->session->userdata('user_data')['initials'];
       // $this->db->where('foldername','New Mail');
        $this->db->from(strtolower($username).'_mymail');
        //echo $this->db->last_query(); exit;
        return $this->db->count_all_results();
    }
    
    public function getParcelId($username='')
    {
        if(!empty($username)){
            $username = $username;
        }else{
            $username = $this->session->userdata('user_data')['initials'];
        }
        //$this->db->select('REPLACE(parcelid, "_$username", "") AS parcelid');
        $this->db->select("REPLACE(parcelid, '_$username', '') parcelid", false);
        $this->db->where("LOCATE('_$username',parcelid) >",0);
        $this->db->order_by('CAST(parcelid AS SIGNED INTEGER)', 'DESC');
        $result = $this->db->get(strtolower($username).'_mymail',1)->result();
        return $result[0]->parcelid; 
    }
    
    public function send_email_to($new_parcel_id, $folderid, $foldername, $urgent,$subject, $mailbody, $caseno, $whofrom ,$whoto,$user='',$external) {
        //print_r($urgent);
        date_default_timezone_set('America/Los_Angeles');
        $email_array = array(
            'parcelid' => $new_parcel_id.'_'.strtoupper($whofrom),
            'folderid' => $folderid,
            'foldername' => $foldername,
            'priority' => $urgent,
            'subject' => $subject,
            'body' => $mailbody,
            'caseno'=>$caseno,
            'datecreate' => date('Y-m-d H:i:s'),
            'datesent' => date('Y-m-d H:i:s'),
            'whofrom' => $whofrom,
            'whoto' => strtoupper($whoto),
            'whotoexternal' => $external,
        );
        if(!empty($user)){
            if ($this->db->insert(strtolower($user).'_mymail', $email_array)) {
                //echo $this->db->last_query();exit;
                $id = $this->db->insert_id();
            } else {
                $id = 0;
            }
        }else{
            $id = 0;
        }
        
        return $id;
    }
    
    public function update_email_read_new($current_user, $filename ,$current_list) {
        
        $foldername = '';
        if($current_list == 'new'){
        $foldername = 'New Mail';
        }
        if($current_list == 'old'){
            $foldername = 'Old Mail';
        }

        if($current_list == 'sent'){
            $foldername = 'Sent Mail';
        }

        if($current_list == 'delete'){
            $foldername = 'Deleted';
        }
        if($foldername != ''){
            $this->db->where('foldername',$foldername);
        }
        $this->db->where('parcelid',$filename);
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update(strtolower($current_user).'_mymail', array('readyet' => 'Y', 'dateread'=> $dateread))) {
             return true;
        } else {
            return false;
        }
    }
    
    public function update_email_unread_new($current_user, $filename,$current_list) {
        $foldername = '';
        if($current_list == 'new'){
        $foldername = 'New Mail';
        }
        if($current_list == 'old'){
            $foldername = 'Old Mail';
        }

        if($current_list == 'sent'){
            $foldername = 'Sent Mail';
        }

        if($current_list == 'delete'){
            $foldername = 'Deleted';
        }
        if($foldername != ''){
            $this->db->where('foldername',$foldername);
        }
        $this->db->where('parcelid',$filename);
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update(strtolower($current_user).'_mymail', array('readyet' => '', 'dateread'=> $dateread))) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_self_emails($singleFilter,$order,$dir, $limit = DEFAULT_LISTING_LENGTH, $offset = 0,$defaultFilter='',$caseType ='',$emailType) {
        $username = $this->session->userdata('user_data')['initials'];
        $this->db->where("whofrom",$username);
        $this->db->where("foldername",'New Mail');
        $where  = "(whoto LIKE '$username,%' OR ";
        $where .= "whoto LIKE '%,$username,%' OR ";
        $where .= "whoto LIKE '%,$username' OR ";
        $where .= "whoto LIKE '$username%') ";
        $this->db->where($where);
        if ($caseType == 'withcase') {
           $this->db->where('caseno IS NOT NULL',null, false);
           $this->db->where('caseno !=','0');
        } else if ($caseType == 'withoutcase') {
           //$this->db->where('caseno IS NULL',null, true);
           //$this->db->or_where('caseno','0');
            $where  = "(caseno IS NULL OR ";
            $where .= "caseno = 0)";
            $this->db->where($where);
        }
        if ($defaultFilter != '') {
            $this->db->where($defaultFilter);
        }
        if ($emailType == 'interemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom NOT LIKE '%@%' )");
        } else if ($emailType == 'extemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom LIKE '%@%'  )");
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->order_by('datecreate', 'desc');
        return $result = $this->db->get(strtolower($username).'_mymail', $limit, $offset)->result();
        //echo $this->db->last_query(); exit;
        
    }
    
    public function get_self_messages_count($singleFilter='',$defaultFilter='',$caseType ='',$emailType) {
        $username = $this->session->userdata('user_data')['initials'];
        $this->db->where("whofrom",$username);
        $this->db->where("foldername",'New Mail');
        $where  = "(whoto LIKE '$username,%' OR ";
        $where .= "whoto LIKE '%,$username,%' OR ";
        $where .= "whoto LIKE '%,$username' OR ";
        $where .= "whoto LIKE '$username%') ";
        $this->db->where($where);
        if ($caseType == 'withcase') {
           $this->db->where('caseno IS NOT NULL',null, false);
           $this->db->where('caseno !=','0');
        } else if ($caseType == 'withoutcase') {
           //$this->db->where('caseno IS NULL',null, true);
            $where  = "(caseno IS NULL OR ";
            $where .= "caseno = 0)";
            $this->db->where($where);
        }
        if ($emailType == 'interemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom NOT LIKE '%@%' )");
            $this->db->where('caseno !=','0');
        } else if ($emailType == 'extemails') {
            $this->db->where("(whofrom !='".$current_user."' AND whofrom LIKE '%@%'  )");
            $this->db->or_where('caseno','0');
        }
        if ($defaultFilter != '') {
            $this->db->where($defaultFilter);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        
        $this->db->from(strtolower($username).'_mymail');
        return $this->db->count_all_results();
        //echo $this->db->last_query(); exit;
        
    }
    
    public function get_unread_messages($current_user) {
        $this->db->where("readyet !=",'Y');
        $where  = "(foldername = 'New Mail')";
        $this->db->where($where);
        $this->db->from(strtolower($current_user).'_mymail');
        return $this->db->count_all_results(); 
        //echo $this->db->last_query(); exit;
        
    }
    
     public function get_received_emails_new($current_user,$replyfrom ='', $limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = '',$defaultFilter='',$emailType = '',$caseType ='',$singleFilter = '',$order='',$dir='',$filterType = 0) {
        $current_user = strtolower($current_user);
        $this->db->select($current_user."_mymail.*,CONCAT(s1.fname, ' ', s1.lname) whofrom_name", false);
        $this->db->join("staff s1", "s1.initials=".$current_user."_mymail.whofrom", "left");
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($current_listing == 'urgent') {
            $this->db->where($current_user.'_mymail.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where($current_user.'_mymail.readyet !=', 'Y');
        }else if($current_listing == 'read')
        {
            $this->db->where($current_user.'_mymail.readyet', 'Y');
        }
        else{
            $this->db->where($current_user.'_mymail.readyet !=', 'Y');
        }
        $this->db->where($current_user.'_mymail.foldername', 'New Mail');
        $this->db->order_by('datecreate','DESC');
        $result = $this->db->get($current_user.'_mymail', $limit, $offset);
        return $result->result();
        //echo $this->db->last_query(); exit;
        
    }
    
    public function send_email_to_ext($new_parcel_id, $folderid, $foldername, $urgent,$subject, $mailbody, $caseno, $whofrom ,$whoto,$user='',$external) {
        //print_r($urgent);
        date_default_timezone_set('America/Los_Angeles');
        $email_array = array(
            'parcelid' => $new_parcel_id.'_'.strtoupper($user),
            'folderid' => $folderid,
            'foldername' => $foldername,
            'priority' => $urgent,
            'subject' => $subject,
            'body' => $mailbody,
            'caseno'=>$caseno,
            'datecreate' => date('Y-m-d H:i:s'),
            'datesent' => date('Y-m-d H:i:s'),
            'whofrom' => $whofrom,
            'whoto' => strtoupper($whoto),
            'whotoexternal' => $external,
        );
        if(!empty($user)){
            if ($this->db->insert(strtolower($user).'_mymail', $email_array)) {
                // echo $this->db->last_query();exit;
                 $id = $this->db->insert_id();
            } else {
                $id = 0;
            }
        }else{
            $id = 0;
        }
        
        return $id;
    }
    
 public function get_single_email_case($caseno, $parcelid) {
     
        $usernm = explode('_',$parcelid);
        $username = $usernm[1];
        $this->db->where('caseno',$caseno);
        $this->db->where('parcelid',$parcelid);
        $this->db->order_by('datecreate', 'desc');
        return $result = $this->db->get(strtolower($username).'_mymail')->result();
        //echo $this->db->last_query(); exit;
    }
    
    public function get_case_emails($caseno) {
        $username = $this->session->userdata('user_data')['initials'];
        $this->db->where('caseno',$caseno);
        $this->db->where('foldername','New Mail');
        $this->db->order_by('datecreate', 'desc');
        return $result = $this->db->get(strtolower($username).'_mymail')->result();
    }
    
    public function update_email_new($current_user, $filename) {
        $this->db->where_in('parcelid', explode(',', $filename));
        $this->db->where('foldername', 'Deleted');
        if ($this->db->update(strtolower($current_user).'_mymail', array('foldername' => 'New Mail','folderid'=>'1'))) {
            return true;
        } else {
            return false;
        }
    }
    
    public function get_type_of_last_email($current_user) {
        $this->db->select("priority",false);
        $this->db->order_by("datesent",'DESC');
        $result = $this->db->get(strtolower($current_user).'_mymail',1,0);
        return $result->result()[0]->priority;
    }



}
