<?php
class Export_csv_model extends CI_Model
{
    function fetch_data()
    {
     $this->db->select("c.caseno,CONCAT(cd.first,',',cd.last) AS fullname,c.casetype,c.casestat,c.yourfileno,cd.social_sec,c.caption1", FALSE);
     $this->db->join('casecard cc', 'cc.caseno = c.caseno', 'left');
     $this->db->join('card cd', 'cc.cardcode = cd.cardcode', 'left');
     $this->db->where("casestat","OPEN");
     $this->db->where("cc.orderno","1");
     //$this->db->group_by("c.caseno");
     //$this->db->limit(20,0);
     $this->db->from('case c');
     return $this->db->get();
    }
    
    function fetch_category()
    {
     $this->db->select("casetype,actname,category,actorder,access,gocltcdudf,actgridx,actgridy,scnwidth,scnheight,scnsplit,userpageon,usertm,userlm,pagecap,edituserpg,userformat,usercolmax,usercolwid,accessadd,accessedit,usrorder,usrenabled,usrtop,usrleft,usrstyle,usrparty,usrheight,usrwidth,usraccess,usrtype", FALSE);
     $this->db->from('actdeflt');
     return $this->db->get();
    }
    
    function fetchcaseactno($case_id){
        $cond = array('caseno' => $case_id);
        $this->db->where($cond);
        $this->db->select('actno');
        $q = $this->db->order_by('actno','DESC');
        $q = $this->db->limit(1,0);
        $q = $this->db->get('caseact');
        //echo $this->db->last_query(); exit;
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return false;
        }
    }
}

?>
