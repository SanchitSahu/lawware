<?php

class Email_model_old extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_unread_messages($current_user, $current_listing = 'inbox') {
        $this->db->where(array(
            'e2.readyet != ' => 'Y',
            'e2.oldmail = ' => 0,
            'e2.whoto' => $current_user
        ));
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        
        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }
        $this->db->from('email e');
        //echo "$current_listing". $this->db->last_query(); exit;
        return $this->db->count_all_results();
    }

    public function get_type_of_last_email($current_user, $current_listing = 'inbox') {
        $this->db->select("e.urgent",false);
        $this->db->where(array(
            'e2.readyet != ' => 'Y',
            'e2.oldmail = ' => 0,
            'e2.whoto' => $current_user
        ));
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        
        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }
        $this->db->order_by('e2.datesent', 'DESC');
        $result = $this->db->get("email e", 1, 0);
        return $result->result()[0]->urgent;
    }

    public function get_all_messages_count($current_user, $current_listing = 'inbox', $replyfrom ='') {

        if ($current_listing == 'urgent') {
            if ($replyfrom !=''){
                $this->db->where("(e2.whoto = '".$current_user."' OR (e2.whoto $replyfrom ))");
                $this->db->where('e.whofrom !=', $current_user);
            }
            else {
                    $this->db->where('e2.whoto', $current_user);
            }
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
            if ($replyfrom !=''){
                $this->db->where("(e2.whoto = '".$current_user."' OR (e2.whoto $replyfrom ))");
                $this->db->where('e.whofrom !=', $current_user);
            }
            else {
                $this->db->where('e2.whoto', $current_user);
            }

        } else if ($current_listing == 'sent') {
            $this->db->where('e.whofrom', "$current_user");
        } else if ($current_listing == 'withcase') {
            $this->db->where("e2.whoto ='".$current_user."' AND e.caseno !='0' ");
        } else if ($current_listing == 'withoutcase') {
            $this->db->where("e2.whoto ='".$current_user."' AND e.caseno  ='0' ");
        } else {

            if ($replyfrom !=''){
                $this->db->where("(e2.whoto = '".$current_user."' OR (e2.whoto $replyfrom ))");
                $this->db->where('e.whofrom !=', $current_user);
            } else {
                $this->db->where('e2.whoto', $current_user);
            }
        }
        // $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        //echo $this->db->last_query(); exit;
        return $this->db->count_all_results();
    }
    /* (SELECT COUNT(filename) AS ethread FROM email AS em WHERE em.email_con_id = ec.email_con_id GROUP BY em.email_con_id) AS ethread,*/
    public function get_received_emails($current_user,$replyfrom ='', $limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = '',$defaultFilter='',$emailType = '',$caseType ='',$singleFilter = '',$order='',$dir='',$filterType = 0) {
        $filename_array = array();
        $email_con_id_array = "";
        $tmp_email_con_id_array = array();
        if($filterType > 0){
            $this->db->select("e.filename",false);
            if ($current_listing == 'old') {
                $result1 = $this->db->query('
                    SELECT e.email_con_id, MAX(e.filename) as max_filename FROM email e 
                    LEFT JOIN email2 e2 ON e.filename = e2.filename 
                    WHERE e2.whoto LIKE "'.$current_user.'" AND (LOCATE("' . $current_user . '", e2.deleted_by) = 0 
                    OR ISNULL(LOCATE("' . $current_user . '", e2.deleted_by))) 
                    AND e2.oldmail = 1
                    GROUP BY e.email_con_id 
                    ORDER BY e2.datesent DESC')->result();

                foreach ($result1 as $key1 => $value1) {
                    $tmp_email_con_id_array[] = $value1->email_con_id;
                    $filename_array[] = $value1->max_filename; 
                }
            }
        } else {
            $cnt_filename_result = $this->db->query("SELECT  email_con_id, COUNT(filename) AS cnt_filename FROM email GROUP BY email_con_id ORDER BY datecreate DESC LIMIT ".$offset.", ".$limit)->result();
            $email_thread = '';
            foreach ($cnt_filename_result as $key => $value) {
                if($value->cnt_filename > 1 && $value->email_con_id != '' && $value->email_con_id != null) {
                    $corresponding_filename = $this->db->query("SELECT filename FROM email WHERE email_con_id = ".$value->email_con_id)->result();
                    foreach ($corresponding_filename as $value1) {
                        $email_thread .= $value1->filename.',';
                    }
                }
            }
            if($email_thread != '') {
                $email_thread = substr($email_thread, 0, -1);
            }
            if ($current_listing == 'old') {
                $result1 = $this->db->query('
                    SELECT e.email_con_id, MAX(e.filename) as max_filename FROM email e 
                    LEFT JOIN email2 e2 ON e.filename = e2.filename 
                    WHERE e2.whoto LIKE "'.$current_user.'" AND (LOCATE("' . $current_user . '", e2.deleted_by) = 0 
                    OR ISNULL(LOCATE("' . $current_user . '", e2.deleted_by))) 
                    AND e2.oldmail = 1
                    GROUP BY e.email_con_id 
                    ORDER BY e2.datesent DESC')->result();

                foreach ($result1 as $key1 => $value1) {
                    $tmp_email_con_id_array[] = $value1->email_con_id;
                    $filename_array[] = $value1->max_filename; 
                }
            } else {
                $result1 = $this->db->query('
                SELECT e.email_con_id, MAX(e.filename) as max_filename FROM email e 
                LEFT JOIN email2 e2 ON e.filename = e2.filename 
                WHERE e2.whoto LIKE "'.$current_user.'" AND (LOCATE("' . $current_user . '", e2.deleted_by) = 0 
                OR ISNULL(LOCATE("' . $current_user . '", e2.deleted_by))) 
                GROUP BY e.email_con_id 
                ORDER BY e2.datesent DESC')->result();

                foreach ($result1 as $key1 => $value1) {
                    $tmp_email_con_id_array[] = $value1->email_con_id;
                    $filename_array[] = $value1->max_filename; 
                }
            }
            if($current_listing == 'Self') {
                foreach ($tmp_email_con_id_array as $key => $value) {
                    if($value != '') {
                        if($email_con_id_array == '') {
                            $email_con_id_array .= $value;
                        } else {
                            $email_con_id_array .= ','.$value;
                        }
                    }
                }
                $q = 'SELECT MAX(e.filename) as max_filename FROM email2 e2 LEFT JOIN email e ON e2.filename = e.filename';
                if($email_con_id_array != '') {
                    $q .= ' WHERE e.email_con_id IN ('.$email_con_id_array.')';
                }
                $q .= ' GROUP BY e.email_con_id ORDER BY e2.datesent DESC';
                $result2 = $this->db->query($q)->result();
                foreach ($result2 as $key1 => $value1) {
                    array_push($filename_array, $value1->max_filename);
                }
                arsort($filename_array);
            }
            
            /*$this->db->select("e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.email_con_id,(CASE WHEN CONCAT(s1.fname,' ', s1.lname) !='' THEN  CONCAT(s1.fname, ' ', s1.lname) ELSE e.whofrom END) AS whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, (SELECT readyet FROM email2 WHERE email2.filename=e.filename ORDER BY email2.filename DESC LIMIT 1)AS readyet,e2.detached,e2.iore,''as ethread,e2.id as e2id", false);*/
            $this->db->select("e.filename,e.urgent, e.subject,e.email_con_id,e.whofrom,(CASE WHEN CONCAT(s1.fname,' ', s1.lname) !='' THEN  CONCAT(s1.fname, ' ', s1.lname) ELSE e.whofrom END) AS whofrom_name,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e2.datesent, (SELECT readyet FROM email2 WHERE email2.filename=e.filename ORDER BY email2.filename DESC LIMIT 1)AS readyet,e2.detached,e2.iore,''as ethread,e2.id as e2id", false);
            /*$this->db->select("e.filename, e.urgent, e.subject,e.email_con_id,(CASE WHEN CONCAT(s1.fname,' ', s1.lname) !='' THEN  CONCAT(s1.fname, ' ', s1.lname) ELSE e.whofrom END) AS whofrom_name,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e2.datesent, e2.readyet,e2.detached,e2.iore,''as ethread,e2.id as e2id", false);*/
        }
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "ec.email_con_id=e.email_con_id", "left");

        if($current_listing != 'old') {
            $this->db->where('e2.whoto', $current_user);
        }
        if(count($filename_array) > 0) {
            $this->db->where_in('e.filename', $filename_array);
        }

        /*if ($current_listing == 'urgent' || $current_listing == 'unread')
        {
            $this->db->where('e.whofrom !=', $current_user);
            $this->db->where('e2.oldmail', '0');
        }*/
        if ($current_listing == 'new') {
            $this->db->where('e2.oldmail != ', '1');
            /*$this->db->where('e.whofrom !=', $current_user);*/
        } else if ($current_listing == 'old') { // old
            $this->db->where('e2.oldmail', '1');
            // $this->db->where('e.whofrom !=', $current_user);
            $this->db->where("(e.whofrom = '$current_user' OR e2.whoto = '$current_user')");
        } else if ($current_listing == 'Self') {// Self
            $this->db->where('e2.oldmail', '0');
            $this->db->where("(e.whofrom = '".$current_user."' AND e2.whoto LIKE '%".$current_user."%')");
        } else if ($current_listing == 'withcase') {// withcase
            $this->db->where("e.caseno != '0'");
            $this->db->where('e.whofrom !=', $current_user);
        } else if ($current_listing == 'withoutcase') {// withoutcase
            $this->db->where("e.caseno = '0' ");
            $this->db->where('e.whofrom !=', $current_user);
        } else if ($current_listing == 'interemails') {// interemails
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        } else if ($current_listing == 'extemails') {// extemails
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
        }

        /* Changes for filters combination */
        if ($caseType == 'withcase') {
            if ($emailType == '') {
                $this->db->where("e.caseno != '0'");
            } else if ($emailType == 'interemails') {
                $this->db->where("e.caseno != '0'");
                $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
            } else if ($emailType == 'extemails') {
                $this->db->where("e.caseno != '0'");
                $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
            }
        } else if ($caseType == 'withoutcase') {
            if ($emailType == '') {
                $this->db->where("e.caseno = '0' ");
            } else if ($emailType == 'interemails') {
                $this->db->where("e.caseno = '0'");
                $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
            } else if ($emailType == 'extemails') {
                $this->db->where("e.caseno = '0'");
                $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
            }
        } else if ($caseType == '') {
            if ($emailType == 'interemails') {
                $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
            } else if ($emailType == 'extemails') {
                $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
            }
        }

        if ($defaultFilter != '') {
            $this->db->where($defaultFilter);
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($order !='' && $dir !='' && $filterType ==0) {
            $this->db->order_by('e2.datesent', $dir);
            /*$this->db->order_by($order,$dir);
            $this->db->order_by("e2.readyet","ASC");*/

        } else {

            $this->db->order_by('e2.datesent', 'desc');
            /*$this->db->order_by('e.email_con_id', 'desc');
            $this->db->order_by('e.filename', 'desc');
            $this->db->order_by('e.urgent','DESC');
            $this->db->order_by('e.datecreate', 'desc');*/
        }
        
        //SANCHIT commented for the DELETE to work fine
        // $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');

        // if ($current_listing != 'Self') {
        //     /* $this->db->where('CASE e.filename WHEN (SELECT COUNT(e1.filename) FROM email e1 GROUP BY e1.email_con_id HAVING COUNT(e1.filename) > 1 LIMIT 1) THEN (SELECT MAX(email.filename) FROM email GROUP BY email_con_id LIMIT 1) ELSE (SELECT email.filename FROM email GROUP BY email_con_id LIMIT 1) END',NULL,false); */
        //     /* $this->db->where('e.filename IN ( SELECT MAX(filename) FROM email GROUP BY email_con_id ORDER BY datecreate DESC)'); */
        // }
        // $this->db->where('CASE WHEN count(e1.email_con_id) > 1 THEN  e.filename IN (SELECT MAX(e1.filename) FROM email e1 GROUP BY e1.email_con_id) END CASE');
        // if($email_thread != '') {
        //     /* Below code is commented by Kairavi, as its related code (also developed by Kairavi) was needed priorly, but not now. */
        //     /* $this->db->where('e.filename IN ( SELECT MAX(filename) FROM email GROUP BY email_con_id ORDER BY datecreate DESC)'); */
        // }
        // if ($this->input->post('list_type') != 'unread') {
            $this->db->group_by('e.email_con_id');
        // }

       /* if ($list_type == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($list_type == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }
        else if ($list_type == 'read') {
            $this->db->where('e2.readyet', 'Y');
        }*/

        if ($filterType > 0){
            $result = $this->db->get("email e");
            // echo "FIRST :: " . $this->db->last_query();
            return $result->num_rows();
        } else {
            $result = $this->db->get("email e", $limit, $offset);
            // echo "SECOND :: " . $this->db->last_query();
            return $result->result();
        }

        // $result = $this->db->get("email e", $limit, $offset);
        // echo $this->db->last_query(); exit;
        // return $result->result();
    }

    public function get_sent_emails($current_user, $limit = DEFAULT_LISTING_LENGTH, $offset = 0, $singleFilter,$filterType=0,$order ='',$dir='',$emailType='',$caseType='') {
        if($filterType ==0){

        //$this->db->select("e.filename, e.caseno, e.urgent, e.subject,ec.email_con_people,e.whofrom,e.email_con_id, (CASE WHEN CONCAT(s1.fname,' ', s1.lname) !='' THEN  CONCAT(s1.fname, ' ', s1.lname) ELSE e.whofrom END) AS whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, e2.readyet, e2.detached,e2.iore,e2.id as e2id", false);
        //}
        $this->db->select("max(e.filename) filename, e.caseno, e.urgent, e.subject,ec.email_con_people,e.whofrom,e.email_con_id, (CASE WHEN CONCAT(s1.fname,' ', s1.lname) !='' THEN  CONCAT(s1.fname, ' ', s1.lname) ELSE e.whofrom END) AS whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, e2.readyet, e2.detached,e2.iore,MAX(e2.id) as e2id", false);
        }
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        /*if($filterType ==0){*/
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "ec.email_con_id=e.email_con_id", "left");
        /*}*/
        $this->db->where('e.whofrom', $current_user);
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($caseType == 'withcase' && $emailType == '')
        {
            $this->db->where("e.caseno != '0'");
            $this->db->where('e.whofrom =', $current_user);
        }
        if ($caseType == 'withcase' && $emailType == 'interemails')
        {
            $this->db->where("e.caseno != '0'");
            $this->db->where("(e.whofrom ='".$current_user."' AND e2.whoto NOT LIKE '%@%' )");
        }
        if ($caseType == 'withcase' && $emailType == 'extemails')
        {
            $this->db->where("e.caseno != '0'");
            $this->db->where("(e.whofrom ='".$current_user."' AND e2.whoto LIKE '%@%'  )");
        }
        if ($caseType == 'withoutcase' && $emailType == '')
        {
            $this->db->where("e.caseno = '0' ");
            $this->db->where('e.whofrom =', $current_user);
        }
        if ($caseType == 'withoutcase' && $emailType == 'interemails')
        {
            $this->db->where("e.caseno = '0'");
            $this->db->where("(e.whofrom ='".$current_user."' AND e2.whoto NOT LIKE '%@%' )");
        }
        if ($caseType == 'withoutcase' && $emailType == 'extemails')
        {
            $this->db->where("e.caseno = '0'");
            $this->db->where("(e.whofrom ='".$current_user."' AND e2.whoto LIKE '%@%'  )");
        }

        if ($emailType == 'interemails' && $caseType == '')
        {
            $this->db->where("(e.whofrom ='".$current_user."' AND e2.whoto NOT LIKE '%@%' )");
        }

        if ($emailType == 'extemails' && $caseType == '')
        {
            $this->db->where("(e.whofrom ='".$current_user."' AND e2.whoto LIKE '%@%'  )");
        }

        // $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        // $this->db->where('e.filename IN (SELECT MAX(e1.filename) FROM email e1 WHERE e1.email_con_id = e.email_con_id GROUP BY e1.email_con_id)');
        $this->db->group_by('e.email_con_id');
         if ($order !='' && $dir !='' && $filterType ==0)
        {
             $this->db->order_by('e2.datesent', $dir);
            /*$this->db->order_by($order,$dir);*/
        }
        else{
            $this->db->order_by('e2.datesent', 'desc');
       /* $this->db->order_by('e.datecreate', 'desc');
        $this->db->order_by('e.filename', 'desc');*/
        }
        //$this->db->group_by('e.filename');
        /*$this->db->group_by('e.email_con_id');*/

        if($filterType > 0){
            $result = $this->db->get("email e");
            //echo $this->db->last_query(); exit;
            return $result->num_rows();
        }else{
            $result = $this->db->get("email e", $limit, $offset);
            // echo $this->db->last_query(); exit;
            return $result->result();
        }
        /*$result = $this->db->get("email e", $limit, $offset);
        //echo $this->db->last_query(); exit;
        return $result->result();*/
    }

    public function get_single_email($current_user, $email_id, $current_list,$iore='I',$e2id='') {

        //echo $current_user."<br>". $email_id."<br>".$current_list."<br>";
        $this->db->select("e.filename, e.caseno, e.urgent, e.subject, e.whofrom, e.mail_body,e.email_con_id,ec.email_con_people, CONCAT(s1.fname, ' ', s1.lname) whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, e2.readyet, e2.detached,e2.iore,e2.id as e2id", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "ec.email_con_id=e.email_con_id", "left");
        $this->db->where('e.filename', $email_id);
        if($e2id !=''){$this->db->where('e2.id', $e2id);}
        $this->db->order_by('e.datecreate', 'desc');
        $result = $this->db->get("email e")->result();
        // echo $this->db->last_query(); exit;


        $whoto = '';
        //if ($current_list == 'inbox' && $iore !='E') {
        if ($current_list == 'new' && $iore !='E') {
            $this->db->select('GROUP_CONCAT(e2.whoto) AS whoto');
            $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
            $this->db->where("e.email_con_id", $result[0]->email_con_id);
            $this->db->where("e.subject", $result[0]->subject);
            $whoto = $this->db->get('email e')->result()[0]->whoto;
            // echo $this->db->last_query();exit;
            // print_r($whoto);exit;
            //$this->db->or_where('e2.whoto', $current_user);
            //$this->db->or_where("ec.email_con_people like '%$current_user%'");
        } else if ($current_list == 'sent') {
            $new_query = $this->db->query('
                SELECT DISTINCT e2.whoto 
                FROM email2 e2 
                LEFT JOIN email e ON e2.filename = e.filename 
                WHERE e.email_con_id = (SELECT email.email_con_id FROM email WHERE email.filename = "'.$email_id.'") 
                AND e2.filename <= "'.$email_id.'"')->result();
            foreach($new_query as $new_value) {
                if($whoto != '') {
                    $whoto .= ',';
                }
                $whoto .= $new_value->whoto;
            }
        } else if($current_list == 'Self') {
            $new_query = $this->db->query('SELECT DISTINCT e2.whoto FROM email2 e2 LEFT JOIN email e ON e2.filename = e.filename WHERE e.email_con_id = (SELECT email.email_con_id FROM email WHERE email.filename = "'.$email_id.'") AND e2.filename >= "'.$email_id.'"')->result();
            foreach($new_query as $new_value) {
                if($whoto != '') {
                    $whoto .= ',';
                }
                $whoto .= $new_value->whoto;
            }
        } else if($current_list == 'delete') {
            $this->db->select('GROUP_CONCAT(e2.whoto) AS whoto');
            $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
            $this->db->where("e.email_con_id", $result[0]->email_con_id);
            $this->db->where("e.subject", $result[0]->subject);
            $whoto = $this->db->get('email e')->result()[0]->whoto;
        }
        if($whoto != '') {
            $result[0]->whoto = $whoto;
        }
        
        return $result;
    }

    public function update_email_read($current_user, $filename, $iore='I') {
        $filename_id = explode(',', $filename);
        $this->db->where_in('filename',$filename_id);
        //if ($iore !='E' ){$this->db->where('whoto', $current_user);}
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update('email2', array('readyet' => 'Y', 'dateread'=> $dateread))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_unread($current_user, $filename, $iore='I') {
        $filename_id = explode(',', $filename);
        $this->db->where_in('filename',$filename_id);
        //if ($iore !='E' ){$this->db->where('whoto', $current_user);}
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update('email2', array('readyet' => '', 'dateread'=> $dateread))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_old($current_user, $filename, $iore='I') {
        $this->db->where_in('filename', explode(',', $filename));
        //if ($iore !='E' ){$this->db->where('whoto', $current_user);}
        $dateread = date('Y-m-d h:i:s');
        if ($this->db->update('email2', array('oldmail' => '1'))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_urgent($current_user, $filename) {
        $this->db->where_in('filename', explode(',', $filename));
        // $this->db->where('whoto', $current_user);
        if ($this->db->update('email', array('urgent' => 'Y'))) {
            return true;
        } else {
            return false;
        }
    }

    public function update_email_trash($current_user, $filename) {
        // $this->db->where_in('filename', explode(',', $filename));
        // // $this->db->where('whoto', $current_user);
        // if ($this->db->update('email2', array('detached' => 1))) {
        //     return true;
        // } else {
        //     return false;
        // }


        if ($this->db->query('UPDATE email2 SET detached=1, deleted_by = 
            CASE
                WHEN deleted_by = "" OR ISNULL(deleted_by) THEN "' . $current_user . '"
                WHEN LOCATE("' . $current_user . '",deleted_by) > 0 THEN deleted_by
                ELSE CONCAT(deleted_by, ",' . $current_user . '")
            END
            WHERE filename IN ( ' . $filename . ')
        ')) {
            // echo $this->db->last_query();
            return true;
        } else {
            // echo $this->db->last_query();
            return false;
        }

    }

    public function send_email($whofrom, $whoto, $subject, $urgent, $mailbody, $caseno,$email_con_id=NULL,$iore='I') {
        //print_r($urgent);
        date_default_timezone_set('America/Los_Angeles');
        $email_array = array(
            'datecreate' => date('Y-m-d H:i:s'),
            'whofrom' => $whofrom,
            'subject' => $subject,
            'urgent' => $urgent,
            'mail_body' => $mailbody,
            'caseno' => $caseno,
            'email_con_id'=>$email_con_id,
        );


        //echo "<pre>";print_r($email_array);exit;

        if ($this->db->insert('email', $email_array)) {
            // echo $this->db->last_query();exit;
            $id = $this->db->insert_id();
        } else {
            $id = 0;
        }

        if ($id != 0) {
            $return = 0;

            //print_r($whoto);exit;
            //foreach ($whoto as $value){

            $email2_array = array(
                'datesent' => date('Y-m-d H:i:s'),
                'dateread' => '1899-12-30 00:00:00',
                'whoto' => $whoto,
                'readyet' => '',
                'detached' => 'true',
                'mail_body' => $mailbody,
                'iore' => $iore,
                'detached'=>'0'
            );

            //print_r($email2_array);exit;
            /* $res = $this->db->get_where('email2', array('filename' => $id));
              if(!empty($res->result_array)){
              $email2_array['filename']   =  $id;
              $this->db->where("filename",$id);
              $this->db->update('email2', $email2_array);
              //echo "sdf".$this->db->last_query();exit;
              $return =   $this->db->affected_rows();
              }else{ */
            $email2_array['filename'] = $id;

            if ($this->db->insert('email2', $email2_array)) {
                // echo $this->db->last_query();//exit;
                $return = $id;
            } else {
                $return = 0;
            }
            //}
            //}
            //echo $this->db->last_query();exit;
            return $return;
        }
    }

    /* ading function for external parties email conversation */
    public function get_email_conversation($email_con_id,$current_user,$current_from='',$caseno='',$limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = 'inbox') {
        $this->db->select("e2.id,e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.email_con_id,e2.whoto,e.datecreate, CONCAT(s1.fname, ' ', s1.lname) whofrom_name, CONCAT(s2.fname, ' ', s2.lname) whoto_name, e2.datesent, e2.dateread, e2.readyet, e2.detached,e2.iore, (SELECT GROUP_CONCAT(e2.whoto) AS whoto
        FROM email em
        LEFT JOIN email2 e2 ON em.filename = e2.filename
        WHERE em.email_con_id =  e.email_con_id
        AND em.subject =  e.subject) AS whoto", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
       //$this->db->where("e.email_con_id IN (select email_con_id from email where whofrom = '".$current_user."') ");
        $this->db->where("e.email_con_id",$email_con_id);
        if($current_from =='email_details'){
            if($current_listing != 'sent' && $current_listing != 'new' && $current_listing != 'delete') {    
                // $this->db->where("e.whofrom != '".$current_user."'");
                $this->db->where('e2.whoto', $current_user);
            }
        }
        
        if($caseno!=''){ $this->db->where('e.caseno', $caseno);}
        if($current_listing == 'sent' || $current_listing == 'new' || $current_listing == 'delete') {
            $query = "select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."'";
            if($current_listing == 'delete') {
                $query .= " and email2.detached = 1";
            }
            $query .= " group by email.whofrom order by email.datecreate desc";
            $tmp_filename_array_ethread = $this->db->query($query)->result();
            $filename_array_ethread = '';
            foreach ($tmp_filename_array_ethread as $key => $value) {
                if($filename_array_ethread == '') {
                    $filename_array_ethread .= $value->id;
                } else {
                    $filename_array_ethread .= ','.$value->id;
                }
            }
            if($filename_array_ethread != '') {
                $this->db->where('e2.id IN ('.$filename_array_ethread.')');
            }
        }
        if($current_listing == 'delete') {
            // $this->db->where('e2.detached', '1');
            $this->db->where('(e2.detached = 1 OR LOCATE("' . $current_user . '", e2.deleted_by) > 0)');
        }
        //$this->db->group_by('e.email_con_id');
		$this->db->order_by('e.datecreate', 'desc');
        
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }
        $result = $this->db->get("email e", $limit, $offset);
        // echo $this->db->last_query();exit;
        return $result->result();
    }

    public function get_case_email_conversation($email_con_id,$current_user,$current_from='',$caseno='',$limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = 'inbox') {
        $this->db->select("e2.id,e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.email_con_id,e.datecreate, CONCAT(s1.fname, ' ', s1.lname) whofrom_name, CONCAT(s2.fname, ' ', s2.lname) whoto_name, e2.datesent, e2.dateread, e2.readyet, e2.detached,e2.iore", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
       //$this->db->where("e.email_con_id IN (select email_con_id from email where whofrom = '".$current_user."') ");
        $this->db->where("e.email_con_id",$email_con_id);
        if($current_from =='email_details'){
        $this->db->where("e.whofrom != '".$current_user."'");
        $this->db->where('e2.whoto', $current_user);
        }
        
        if($caseno!=''){ $this->db->where('e.caseno', $caseno);}
        // $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        //$this->db->group_by('e.email_con_id');
		$this->db->order_by('e.datecreate', 'desc');
        
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }
        $result = $this->db->get("email e", $limit, $offset);
        //echo $this->db->last_query();exit;
        return $result->result();
    }
    
    public function update_email_filename($data_email, $filename) {
        $this->db->where('filename',$filename);
        // $this->db->where('whoto', $current_user);
        if ($this->db->update('email', $data_email)) {
            return true;
        } else {
            return false;
        }
    }

    public function get_received_emails_int_ext($current_user, $limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = 'inbox',$singleFilter = '') {
        $this->db->select("e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.email_con_id, CONCAT(s1.fname, ' ', s1.lname) whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, e2.readyet,e2.detached,e2.iore", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "e.email_con_id=ec.email_con_id", "left");
        //$this->db->where('e2.whoto', $current_user);
        $this->db->where("e2.whoto = '".$current_user."' OR e2.whoto NOT IN (SELECT initials FROM staff ) ");
        $this->db->where('e2.whoto !=', 'null');
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        // $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }else if($current_listing == 'read')
        {
            $this->db->where('e2.readyet', 'Y');
        }
        $this->db->order_by('e.email_con_id', 'desc');
        $this->db->order_by('e.datecreate', 'desc');

        $result = $this->db->get("email e", $limit, $offset);
        //echo $this->db->last_query(); exit;
        return $result->result();

    }

    public function iecount($current_user, $current_listing = 'inbox'){ /* internal external count */
        $this->db->where('e2.whoto', $current_user);
        if ($current_listing == 'interemails')
        {
         $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        }
        if ($current_listing == 'extemails')
        {
         $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%')");
        }
        //  $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        $this->db->join("email_conversion ec", "e.email_con_id=ec.email_con_id", "left");
        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        //$this->db->last_query(); exit;
        return $this->db->count_all_results();
    }

    public function get_case_emails($caseno) {
        $this->db->select("e.filename, e.caseno, e.urgent, e.subject,ec.email_con_people,e.whofrom,e.email_con_id, CONCAT(s1.fname, ' ', s1.lname) whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate,count(e.email_con_id) as ethread, e2.datesent, e2.dateread, e2.readyet, e2.detached,e2.iore", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "ec.email_con_id=e.email_con_id", "left");
        $this->db->where('e.caseno', $caseno);

        $tmp_filename_array_ethread = $this->db->query("select email2.filename from email left join email2 on email.filename = email2.filename where email.caseno='".$caseno."' group by email.email_con_id order by email.datecreate desc")->result();
        // echo $this->db->last_query();
        $filename_array_ethread = '';
        foreach ($tmp_filename_array_ethread as $key => $value) {
            if($filename_array_ethread == '') {
                $filename_array_ethread .= $value->filename;
            } else {
                $filename_array_ethread .= ','.$value->filename;
            }
        }
        if($filename_array_ethread != '') {
            $this->db->where('e.filename IN ('.$filename_array_ethread.')');
        }

        $this->db->where('e2.detached', '0');
        $this->db->order_by('e.datecreate', 'desc') ;
        $this->db->order_by('e.email_con_id', 'desc');
        $this->db->order_by('e.filename', 'desc');
        $this->db->group_by('e.email_con_id');
        $result = $this->db->get("email e");
        // echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function selectallemails($select ='',$condtions='') {
        $this->db->select($select);
        if($condtions !=''){$this->db->where($condtions);}
        $this->db->order_by('filename', 'asc');
        $result = $this->db->get("email");
    }

    public function getethreadcount($email_con_id='',$current_user='',$current_listing='') {
        $detached = ($current_listing == 'delete') ? 1 : 0;
        $whofrom = ($current_listing =='sent') ? /*"email.whofrom='$current_user'"*/"1=1" : /*"email.whofrom!='$current_user'"*/"1=1";
        $whoto = ($current_listing !='sent' && $current_listing !='delete' && $current_listing !='new') ? "and email2.whoto='".$current_user."'" : '';
        if($current_listing == 'sent') {
            // $tmp_filename_array_ethread = $this->db->query("select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."' and $whofrom $whoto and email2.detached=$detached group by email.whofrom")->result();
            $tmp_filename_array_ethread = $this->db->query("select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."' and $whofrom $whoto group by email.whofrom")->result();
            // echo $this->db->last_query();exit;
           
            $filename_array_ethread = '';
            foreach ($tmp_filename_array_ethread as $key => $value) {
                if($filename_array_ethread == '') {
                    $filename_array_ethread .= $value->id;
                } else {
                    $filename_array_ethread .= ','.$value->id;
                }
            }
            // $tmp_filename_array_etreadyet = $this->db->query("select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."' and $whofrom and email2.detached=$detached $whoto group by email.whofrom")->result();
            $tmp_filename_array_etreadyet = $this->db->query("select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."' and $whofrom $whoto group by email.whofrom")->result();
            // echo $this->db->last_query();
            $filename_array_etreadyet = '';
            foreach ($tmp_filename_array_etreadyet as $key => $value) {
                if($filename_array_etreadyet == '') {
                    $filename_array_etreadyet .= $value->id;
                } else {
                    $filename_array_etreadyet .= ','.$value->id;
                }
            }
            $query = "(select count(email2.id)as ethread from email2 where email2.id in (".$filename_array_ethread.") and email2.detached=$detached)as ethread, (select count(readyet) from email2 where email2.id in (".$filename_array_etreadyet.") and email2.detached=$detached AND email2.readyet!='Y') as etreadyet ";
        } else {
            if($current_listing == 'new') {
                $tmp_filename_array_ethread = $this->db->query("select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."' and (email2.whoto LIKE '".$current_user."' OR email.whofrom LIKE '".$current_user."') group by email.whofrom")->result();
                // echo '<br><br>';echo $this->db->last_query();
                // print_r($tmp_filename_array_ethread);
                $filename_array_ethread = '';
                foreach ($tmp_filename_array_ethread as $key => $value) {
                    if($filename_array_ethread == '') {
                        $filename_array_ethread .= $value->id;
                    } else {
                        $filename_array_ethread .= ','.$value->id;
                    }
                }
                $query = "(select count(email2.id)as ethread from email2 where email2.id in (".$filename_array_ethread.") and email2.detached=$detached)as ethread, (select count(readyet) from email2 where email2.id in (".$filename_array_ethread.") and email2.detached=$detached AND email2.readyet!='Y')as etreadyet ";
            } else {
                $query = "(select count(email2.id)as ethread from email2 where email2.id in (select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."' and $whofrom $whoto and email2.detached=$detached))as ethread, (select count(readyet) from email2 where email2.id in (select email2.id from email left join email2 on email.filename = email2.filename where email.email_con_id='".$email_con_id."' and $whofrom and email2.detached=$detached and readyet !='Y' $whoto and  email2.detached=$detached AND email2.readyet!='Y'))as etreadyet ";
            }
        }
        $this->db->select($query, false);
        //$this->db->where("e.whofrom !='".$current_user."'");
        //$this->db->where("e2.whoto ='".$current_user."'");
        //$this->db->group_by('e.email_con_id');
        $result = $this->db->get();
        // echo $this->db->last_query();
        return $result->result();
    }
    
    public function recent_email_for_admin($limit='5', $offset='0') {
        $this->db->select("e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.email_con_id,
             CONCAT(s1.fname, ' ', s1.lname) whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, e2.readyet,e2.detached,e2.iore", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "ec.email_con_id=e.email_con_id", "left");
        // $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        $this->db->order_by('e.urgent','DESC');
        $this->db->order_by('e.email_con_id', 'desc');
        $this->db->order_by('e.datecreate', 'desc');
        $this->db->group_by('e.email_con_id');
        $result = $this->db->get("email e", $limit, $offset);
        //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function get_received_emails2($current_user,$replyfrom ='', $limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = '',$singleFilter = '',$emailType,$caseType) {

        //echo '<pre> hello => ' . $current_listing; print_r($singleFilter); exit;
        $this->db->select("e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.email_con_id,
             CONCAT(s1.fname, ' ', s1.lname) whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, e2.readyet,e2.detached,e2.iore", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "ec.email_con_id=e.email_con_id", "left");
        if ($replyfrom!=''){
            $this->db->where("(e2.whoto = '".$current_user."' OR (e2.whoto $replyfrom ))");
        }
        else{
            $this->db->where('e2.whoto', $current_user);
        }
        if ($current_listing == 'inbox' || $current_listing == 'urgent' || $current_listing == 'unread')
        {
         $this->db->where('e.whofrom !=', $current_user);
        }
        if ($caseType == 'withcase')
        {
         $this->db->where("e.caseno != '0'");
         $this->db->where('e.whofrom !=', $current_user);
        }
        if ($caseType == 'withoutcase')
        {
         $this->db->where("e.caseno = '0' ");
         $this->db->where('e.whofrom !=', $current_user);
        }
        if ($emailType == 'interemails')
        {
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        }
        if ($emailType == 'extemails')
        {
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
        }
        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        $this->db->where('e2.detached', '0');
        $this->db->order_by('e2.datesent', 'desc');
        $this->db->order_by('e.urgent','DESC');
        $this->db->order_by('e.email_con_id', 'desc');
        $this->db->order_by('e.datecreate', 'desc');
        $this->db->group_by('e.email_con_id');
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        } else if($current_listing == 'read')
        {
            $this->db->where('e2.readyet', 'Y');
        }
        $result = $this->db->get("email e", $limit, $offset);
        //echo $this->db->last_query(); exit;
        return $result->result();
    }

    public function get_newemail_count($current_user) {
        $this->db->where(array(
            'e2.oldmail != ' => '1',
            'e2.whoto' => $current_user,
            'e2.detached' => '0'
        ));

        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        return $this->db->count_all_results();
    }

    public function get_oldemail_count($current_user) {
        $this->db->where(array(
            'e2.oldmail != ' => '0',
            'e2.whoto' => $current_user,
            'e2.detached' => '0'
        ));

        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        return $this->db->count_all_results();
    }

    public function get_urgentnew_count($current_user) {
        $this->db->where(array(
            'e2.oldmail' => '0',
            'e2.whoto' => $current_user,
            'e2.detached' => '0',
            'e.urgent' => 'Y'
        ));

        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        return $this->db->count_all_results();
    }

    public function get_urgentold_count($current_user) {
        $this->db->where(array(
            'e2.oldmail' => '1',
            'e2.whoto' => $current_user,
            'e2.detached' => '0',
            'e.urgent' => 'Y'
        ));

        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        //echo $this->db->last_query(); exit;

        return $this->db->count_all_results();
    }

    public function get_unreadnew_count($current_user) {
        $this->db->where(array(
            'e2.oldmail' => '0',
            'e2.whoto' => $current_user,
            'e2.detached' => '0',
            'e2.readyet !=' => 'Y'
        ));

        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        return $this->db->count_all_results();
    }

    public function get_unreadold_count($current_user) {
        $this->db->where(array(
            'e2.oldmail ' => '1',
            'e2.whoto' => $current_user,
            'e2.detached' => '0',
            'e2.readyet !=' => 'Y'
        ));

        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');
        return $this->db->count_all_results();
    }

    public function iecount_count($current_user, $current_listing = 'inbox', $getcount ='',$type = 'new'){ /* internal external count */
        $this->db->where('e2.whoto', $current_user);
        if ($current_listing == 'interemails')
        {
         $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        }
        if ($current_listing == 'extemails')
        {
         $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%')");
        }
        if($getcount == 'withoutcase')
        {
            $this->db->where("e.caseno = '0' ");
        }
        if($getcount == 'withcase')
        {
            $this->db->where("e.caseno != '0' ");
        }
        if($type == 'new')
        {
            $this->db->where("e2.oldmail != '1' ");
        }
        if($type == 'old')
        {
            $this->db->where("e2.oldmail = '1' ");
        }

        $this->db->where('e2.detached', '0');
        $this->db->join("email_conversion ec", "e.email_con_id=ec.email_con_id", "left");
        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        $this->db->from('email e');

        return $this->db->count_all_results();
         //echo $this->db->last_query();exit;
        // echo $this->db->last_query(); exit;
    }

    public function get_deleted_emails($current_user,$replyfrom ='', $limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = '',$defaultFilter='',$emailType = '',$caseType ='',$singleFilter = '',$order,$dir,$filterType=0) {

        if ($filterType > 0){
            $this->db->select("e.filename",false);
        } else {
            $this->db->select("e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.email_con_id,
             (CASE WHEN CONCAT(s1.fname,' ', s1.lname) !='' THEN  CONCAT(s1.fname, ' ', s1.lname) ELSE e.whofrom END) AS whofrom_name,e2.whoto,CONCAT(s2.fname, ' ', s2.lname) whoto_name, e.datecreate, e2.datesent, e2.dateread, (SELECT readyet FROM email2 WHERE email2.filename=e.filename ORDER BY email2.filename DESC LIMIT 1)AS readyet,e2.detached,e2.iore,''as ethread,e2.id as e2id", false);
        }
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->join("email_conversion ec", "ec.email_con_id=e.email_con_id", "left");

        $result1 = $this->db->query('SELECT MAX(e.filename) as max_filename FROM email e LEFT JOIN email2 e2 ON e.filename = e2.filename WHERE (e2.whoto =  "'.$current_user.'" OR  e.whofrom = "'.$current_user.'") AND (e2.detached = 1 OR LOCATE("'.$current_user.'", e2.deleted_by) > 0) GROUP BY e.subject ORDER BY e2.datesent DESC')->result();
        $filename_array = array();
        foreach ($result1 as $key1 => $value1) {
            $filename_array[] = $value1->max_filename; 
        }

        if ($current_listing == 'inbox' || $current_listing == 'urgent' || $current_listing == 'unread') {
            $this->db->where('e.whofrom !=', $current_user);
            $this->db->where('e2.oldmail', '0');
        }

        if ($current_listing == 'new') {
            $this->db->where('e2.oldmail != ', '1');
            $this->db->where('e.whofrom !=', $current_user);
        }

        if ($current_listing == 'delete') {
           $this->db->where("(e2.whoto =  '".$current_user."' OR  e.whofrom = '".$current_user."')");
           // $this->db->where('e2.whoto', $current_user);
           if (count($filename_array) > 0) {
                $this->db->where_in('e.filename', $filename_array);
           }
        }

        /* Changes for filters combination */
        if ($caseType == 'withcase' && $emailType == '')
        {
            $this->db->where("e.caseno != '0'");
            $this->db->where('e.whofrom !=', $current_user);
        }
        if ($caseType == 'withcase' && $emailType == 'interemails')
        {
            $this->db->where("e.caseno != '0'");
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        }
        if ($caseType == 'withcase' && $emailType == 'extemails')
        {
            $this->db->where("e.caseno != '0'");
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
        }
        if ($caseType == 'withoutcase' && $emailType == '')
        {
            $this->db->where("e.caseno = '0' ");
            $this->db->where('e.whofrom !=', $current_user);
        }
        if ($caseType == 'withoutcase' && $emailType == 'interemails')
        {
            $this->db->where("e.caseno = '0'");
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        }
        if ($caseType == 'withoutcase' && $emailType == 'extemails')
        {
            $this->db->where("e.caseno = '0'");
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
        }

        if ($emailType == 'interemails' && $caseType == '')
        {
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        }

        if ($emailType == 'extemails' && $caseType == '')
        {
            $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%'  )");
        }

        if ($singleFilter != '') {
            $this->db->where($singleFilter);
        }
        if ($defaultFilter != '') {
            $this->db->where($defaultFilter);
        }

        if ($order !='' && $dir !='' && $filterType ==0)
        {
            $this->db->order_by('e2.datesent', $dir);
           /* $this->db->order_by($order,$dir);
            $this->db->order_by('e.filename','DESC');*/
        }
        else{
          $this->db->order_by('e2.datesent', 'desc');  
      /*  $this->db->order_by('e2.datesent', 'desc');
        $this->db->order_by('e.urgent','DESC');
        $this->db->order_by('e.email_con_id', 'desc');
        $this->db->order_by('e.datecreate', 'desc');*/
        }
        if ($current_listing != 'delete') {
            $this->db->group_by('e.email_con_id');
        }
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }
        else if ($current_listing == 'read') {
            $this->db->where('e2.readyet', 'Y');
        }
        if($filterType > 0){
            $result = $this->db->get("email e");
            return $result->num_rows();
        }else{
            $result = $this->db->get("email e", $limit, $offset);
            // echo $this->db->last_query();
            return $result->result();
        }
        /*$result = $this->db->get("email e", $limit, $offset);
        //echo $this->db->last_query(); exit;
        return $result->result();*/
    }

    public function filter_count($current_user, $current_listing = 'new', $email_type ='', $getcount ='',$readunread = 'read'){ /* internal external count */

        //$this->db->select("COUNT(*) AS numrows");
        //$this->db->join("email_conversion ec", "e.email_con_id=ec.email_con_id", "left");
        $this->db->join('email2 e2', 'e.filename = e2.filename', 'LEFT');
        if($current_listing == 'sent')
        {
            $this->db->where('e.whofrom', $current_user);
        }
        if($current_listing =='new' || $current_listing =='old') {
            $this->db->where('e2.whoto', $current_user);
        }

        if ($email_type == 'interemails')
        {
         $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom NOT LIKE '%@%' )");
        }
        if ($email_type == 'extemails')
        {
         $this->db->where("(e.whofrom !='".$current_user."' AND e.whofrom LIKE '%@%')");
        }
        if($getcount == 'withoutcase')
        {
            $this->db->where("e.caseno = '0' ");
        }
        if($getcount == 'withcase')
        {
            $this->db->where("e.caseno != '0' ");
        }
        if($current_listing == 'new')
        {

            $this->db->where("e2.oldmail != '1' ");
        }
        if($current_listing == 'old')
        {
            $this->db->where("e2.oldmail = '1' ");
        }
        if ($readunread == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($readunread == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        } else if($readunread == 'read')
        {
            $this->db->where('e2.readyet', 'Y');
        }
        if($current_listing=='delete'){
            // $this->db->where('e2.detached', 1);
            $this->db->where('(e2.detached = 1 OR LOCATE("' . $current_user . '", e2.deleted_by) > 0)');
            $this->db->where("(e.whofrom = '".$current_user."' OR  e2.whoto = '".$current_user."' )");
        } else {
            // $this->db->where('e2.detached', 0);
            $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        }

        $this->db->group_by('e.email_con_id');
        $this->db->from('email e');//->get();
        //echo "list $current_listing". $this->db->last_query();exit;
        $q = $this->db->get();
         return $q->num_rows();
         //echo $this->db->last_query();exit;
        // echo $this->db->last_query(); exit;
    }

    /*Function to get filename*/
    public function get_filename($email_con_ids) {
        $this->db->select("e.filename as filename");
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->where('e2.detached', '0');
        $this->db->where("e.email_con_id IN ($email_con_ids)");
        $this->db->order_by("e2.id", "desc");
        $result = $this->db->get("email e", 1);
        //echo $this->db->last_query(); exit;
        return $result->result()[0]->filename;
    }

    /*Get Email conversation on the basis of filename*/
    public function get_email_conversation_by_filename($filename,$current_user,$current_from='',$caseno='',$limit = DEFAULT_LISTING_LENGTH, $offset = 0, $current_listing = 'inbox') {
        $this->db->select("e2.id,e.filename, e.caseno, e.urgent, e.subject,e.whofrom,e.filename,e2.whoto,e.datecreate, CONCAT(s1.fname, ' ', s1.lname) whofrom_name, CONCAT(s2.fname, ' ', s2.lname) whoto_name, e2.datesent, e2.dateread, e2.readyet, e2.detached,e2.iore", false);
        $this->db->join("email2 e2", "e2.filename=e.filename", "left");
        $this->db->join("staff s1", "s1.initials=e.whofrom", "left");
        $this->db->join("staff s2", "s2.initials=e2.whoto", "left");
        $this->db->where("e.filename",$filename);
        if($current_from =='email_details'){
        $this->db->where("e.whofrom != '".$current_user."'");
        $this->db->where('e2.whoto', $current_user);
        }
        
        if($caseno!=''){ $this->db->where('e.caseno', $caseno);}
        // $this->db->where('e2.detached', '0');
        $this->db->where('(e2.detached = 0 OR LOCATE("' . $current_user . '", e2.deleted_by) = 0)');
        //$this->db->group_by('e.email_con_id');
        $this->db->order_by('e.datecreate', 'desc');
        
        if ($current_listing == 'urgent') {
            $this->db->where('e.urgent', 'Y');
        } else if ($current_listing == 'unread') {
            $this->db->where('e2.readyet !=', 'Y');
        }
        $result = $this->db->get("email e", $limit, $offset);
        //echo $this->db->last_query();exit;
        return $result->result();
    }

    /*Function to check whoto as current user*/
    public function check_current_user_whoto($current_user, $email_con_id) {
        $this->db->select('e2.whoto');
        $this->db->where('e.email_con_id', $email_con_id);
        $this->db->join('email2 e2', 'e.filename=e2.filename', 'left');
        $result = $this->db->get('email e')->result();
        $flag = 0;
        foreach ($result as $value) {
            if($value->whoto == $current_user) {
                $flag++;
            }
        }
        return $flag;
    }

}
