<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $data = array();
        $this->load->model('email_model');
        $this->load->model('calendar_model');
        $this->load->model('case_model');
        $this->load->model('common_model');
        $this->data['username'] = $this->session->userdata('user_data')['username'];
        $this->data['loginUser'] = $this->session->userdata('user_data')['username'];
        $this->data['menu'] = 'dashboard';
        $this->data['url_seg'] = $this->uri->segment(1);
        $this->output->enable_profiler(false);
    }

    function __destruct() {

    }

    function getTopCasesForDashboard() {
        $whr = array('user' => $this->session->userdata('user_data')['username']);
        $getRecentCase = $this->common_model->getSingleRecordById('recent_cases', 'caseno', $whr);
        $casenos = $getRecentCase->caseno;
        $result = $this->case_model->getCaseListing(0, 5, array('c.case_status' => '1'), '', array(array('column' => 0, 'dir' => 'desc')),'',$casenos);

        $finalData = array();

        foreach ($result as $data) {
            if (!empty($data->last) || !empty($data->first)) {
                $usrnm = $data->first . ", " . $data->last;
            } else {
                $usrnm = '';
            }

            $link = '<a target="_blank" href = "'.base_url().'cases/documents/'.$data->caseno.'">Document</a>';

            $tmpArr = array(
                $data->caseno,
                $usrnm,
                $data->atty_resp,
                $data->atty_hand,
                $data->casetype,
                $data->casestat,
                $data->protected,
                $link
            );
            array_push($finalData, $tmpArr);
        }
        return $finalData;
    }

    public function index() {

        $this->data['pageTitle'] = 'Dashboard';
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $filters = $this->searchCases();
        $this->data['filters'] = $filters;
        $this->data['topCases'] = $this->getTopCasesForDashboard();
        $this->data['languageList'] = $this->config->item('languageList');
        $this->data['saluatation'] = explode(',', $this->data['system_data'][0]->popsal);
        $this->data['suffix'] = explode(',', $this->data['system_data'][0]->rolosuffix);
        $this->data['title'] = explode(',', $this->data['system_data'][0]->poptitle);
        $this->data['category'] = explode(',', $this->data['system_data'][0]->popclient);
        $main1 = json_decode($this->data['system_data'][0]->main1);
        $this->data['Venue'] = explode(',', $main1->venue);
        $this->data['tabindex'] = 0;
        $this->data['staffList'] = $this->common_model->getAllRecord('staff', 'DISTINCT(initials)', '', 'initials asc');
        $this->data['tabindex'] = 0;
        $getCaseCategory = $this->common_model->getAllRecord('actdeflt', 'actname,category', '', 'category asc');
        $this->data['caseactcategory'] = $getCaseCategory;
        $this->load->view('common/header', $this->data);
        $this->load->view('dashboard/dashboard');
        $this->load->view('common/dashboardmodal');
        $this->load->view('common/footer');
    }

    public function getTodayEventDataAjax() {
        $this->output->enable_profiler(false);
        extract($_POST);
        $singleFilter = '';
        if ($search['value'] != '') {
            $singleFilter = '(cal2.initials LIKE "%' . $search['value'] . '%" OR cal1.attyass LIKE "%' . $search['value'] . '%" OR cal1.initials LIKE "%' . $search['value'] . '%" OR cal1.event LIKE "%' . $search['value'] . '%" OR cal1.venue LIKE "%' . $search['value'] . '%" OR cal1.judge LIKE "%' . $search['value'] . '%")';
        }
        $today_event = $this->calendar_model->getTodayEvent($start, $length, $order, '', $singleFilter);
        $finalData = array();
        if ($today_event > 0) {
            foreach ($today_event as $event) {
                $nwTime = date('h:i A', strtotime($event->caltime));
                $tmpArr = array(
                    $nwTime,
                    $event->atty_hand,
                    $event->attyass,
                    $event->event,
                    $event->initials,
                    $event->venue,
                    $event->judge
                );
                array_push($finalData, $tmpArr);
            }
        }
        $relatedCaseCount = $this->calendar_model->getTodayEvent('', '', '', 1, '');
        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $relatedCaseCount,
            'recordsFiltered' => $relatedCaseCount,
            'data' => $finalData
        );
        echo json_encode($listingData);
    }
    public function searchCases() {
        $filters = $this->case_model->getFilterArray($this->data['system_data']);
        return $filters;
    }

}
