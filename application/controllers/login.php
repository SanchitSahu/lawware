<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('common_model');
        $scriptcheck = $this->uri->segment(2);
        if ($scriptcheck != 'test') {
            $this->common_functions->checkLogin();
        }
        $data = array();
        $this->data['pageTitle'] = APPLICATION_NAME.' | Login';
    }


    public function index() {
        // $this->data['loginUser'] = $this->login_model->getLoginUsername();
        $this->load->view('login/header', $this->data);
        $this->load->view('login');
        $this->load->view('login/footer');

    }

    public function test() {
        $this->load->view('emailscript');
    }

    public function loginAuthentication() {
        extract($_POST);
        $username = isset($username) ? $username : '';
        $password = isset($password) ? $password : '';
        $actlink = isset($actlink) ? $actlink : '';
        $remember = isset($remember) ? $remember : 'off';
        if ($username != '' && $password != '') {
            $whereAct1 = '(actstr = "' . $actlink . '" OR actstr IS NULL)';
            $whereAct = ($actlink != '') ? array('isActive' => 0, 'username' => $username) : array('isActive' => 1, 'username' => $username);

            $actChk = $this->login_model->checkActivationUser($whereAct, $whereAct1);

            if (count($actChk) > 0) {
                $firmData = $this->login_model->getFirmData();
                if (SAA != 'off') {
                    $curl = curl_init();
                    $UserSNS = array(
                        "id" => $firmData['firm_id'],
                        "fuid" => $firmData['uniqueid'],
                    );
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => SAIP . "/lawware_super_admin/public/api/checkaccess/" . $firmData['firm_id'] . "/" . $firmData['uniqueid'],
                        CURLOPT_USERAGENT => 'Lawware', /*In case this needs to be Application name, then use constant APPLICATION_NAME*/
                    ));

                    $response = curl_exec($curl);
                    $response = json_decode($response);
                    // echo SAIP."/lawware_super_admin/public/api/checkaccess/".$firmData['firm_id']."/".$firmData['uniqueid'];
                    // echo '<br/><pre>znnn'; print_r($response); exit;
                } else {
                    $response = '1';
                }

                if (!empty($response)) {
                    //echo 'in if' . print_r($response); exit;
                    $this->login_model->updateActlinkUser($actlink);

                    //$where = array('username' => $username, 'password' => $password);
                    $where = 'username = "' . $username . '" AND Binary password = "' . $password . '"';
                    $rec = $this->login_model->checkLoginUser($where);
                    if (count($rec) > 0) {

                        //$unique_id =time();
                        $unique_id = uniqid();
                        $this->login_model->addLogin($username);
                        $this->login_model->addLogin_newsession($username, $unique_id);
                        //$rec['matched_id'] = $unique_id;
                        $this->session->set_userdata('match_id', $unique_id);
                        $this->session->set_userdata('user_data', $rec);
                        $login_log = array('initials' => $username, 'signon' => date('Y-m-d H:i:s'));
                        $login_log_id = $this->common_model->insertData('loginfo', $login_log);
                        
                        $this->session->set_userdata('login_log_id', $login_log_id);
                        $this->session->set_flashdata('message', 'You have successfully logged in!');
                        if ($remember == 'on') {
                            //$this->input->set_cookie('loginUser', $username, time() + 86400 * 5);
                            setcookie("username", $username, time() + 60 * 60 * 24 * 100, "/");
                            setcookie("password", $password, time() + 60 * 60 * 24 * 100, "/");
                        } else {
                            setcookie("username", $username, time() - 60 * 60 * 24 * 100, "/");
                            setcookie("password", $password, time() - 60 * 60 * 24 * 100, "/");
                        }

                        $this->session->unset_userdata('loginUsername');
                        redirect('dashboard');
                    } else {
                        $this->session->set_userdata('loginUsername', $username);
                        $this->session->set_flashdata('message', 'Incorrect Username or Password!');
                    }
                } else {
                    $this->session->set_userdata('loginUsername', $username);
                    $this->session->set_flashdata('message', 'Access Denied..!');
                }
            } else {
                $this->session->set_userdata('loginUsername', $username);
                $this->session->set_flashdata('message', 'Incorrect Username or Password!');
            }
        } else {
            $this->session->set_flashdata('message', 'Please Fill All Required Fields!');
        }
        redirect('login');
        exit;
    }
    public function logout() {
        $user = $this->session->userdata('user_data')['username'];
        $login_log_id = $this->session->userdata['login_log_id'];
        $login_log = array('signoff' => date('Y-m-d H:i:s'));
        $this->common_model->updateData('loginfo', $login_log, array('id' => $login_log_id));
        $this->session->set_userdata(array(
            'login_log_id' => '',
            'loggedin_id' => ''
        ));
         $this->session->unset_userdata('user_data');
         $this->session->unset_userdata('login_log_id', "");
         $this->session->unset_userdata('login_log_id', "");
         $this->session->sess_destroy();
         $this->input->set_cookie('loginUser', '', time() - 3600);

        $this->login_model->setLogout($user);
        $this->common_functions->deleteRec('meta_data', array('staff' => $user));
        redirect('login');
    }

    public function forgotpass() {
        $email = $this->input->post('email');
        $exists = $this->login_model->check_email($email);
        $count = count($exists);
        if (!empty($count)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function resetpass() {
        $this->load->view('login/header', $this->data);
        $this->load->view('resetpass');
        $this->load->view('login/footer');
    }

    public function resetyourpass() {
        extract($_POST);
        $password = isset($password) ? $password : '';
        $fpasslink = isset($fpasslink) ? $fpasslink : '';
    }

    public function changepass() {
        $pass = $this->input->post('pass');
        $fstr = $this->input->post('fstr');

        $exists = $this->login_model->check_fstr($fstr);
        $count = count($exists);
        if (!empty($count)) {
            $where = array('fpassstr' => $fstr);
            $result = $this->common_functions->getSingleRecordById('staff', 'initials', $where);

            $initial = $result->initials;
            $this->login_model->change_pass($fstr, $pass);

            $newUserSNS = array(
                "username" => $initial,
                "password" => base64_encode($pass),
                "snsusersunqid" => $initial,
            );
            $urlsns = 'http://52.66.93.186/sns/api/user/update';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlsns);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($newUserSNS));
            $response = curl_exec($ch);
            $this->session->set_flashdata('message', 'Your Password is Changed Successfully.');
            echo '1';
        } else {
            echo 0;
        }
    }

    public function checkexistingemail() {
        $email = $this->input->post('email');
        $exists = $this->login_model->check_emailexist($email);
        if (count($exists) >= 1) {
            echo json_encode(false);
        } else {
            echo json_encode(true);
        }
    }

    public function checkAjaxSession() {
        if (!isset($this->session->userdata('user_data')['username']) || $this->session->userdata('user_data')['username'] == '') {
            return 1;
        } else {
            return 0;
        }
    }

    public function clear_all_cache() {
        $this->db->cache_delete_all();
        $CI = &get_instance();
        $path = $CI->config->item('cache_path');

        $cache_path = ($path == '') ? APPPATH . 'cache/' : $path;

        $handle = opendir($cache_path);
        while (($file = readdir($handle)) !== false) {
            if ($file != '.htaccess' && $file != 'index.html') {
                @unlink($cache_path . '/' . $file);
            }
        }
        closedir($handle);
    }

}
