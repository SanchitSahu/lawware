<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
// error_reporting(E_ALL);
class Script extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function mergeCaseact($folderNumber) {
		$this->load->model('script_model');
        $tablename = array();
        $result = $this->script_model->mergeCaseactQuery($folderNumber);
		
    }

    public function mergeInjury() {
        $this->load->model('script_model');
        $tableData = array();
        $result = $this->script_model->mergeInjuryQuery($tableData);
    }

    public function removeTable() {
        $tableFrom = 8500;
        $tableTo = 9000;
        $this->load->model('script_model');
        $this->script_model->removeTable($tableFrom, $tableTo);
    }

    public function addUserInOpenFireServer() {

        include APPPATH . 'third_party/php-openfire-restapi-master/vendor/autoload.php';
        $api = new Gidkom\OpenFireRestApi\OpenFireRestApi;
        $rec = $this->common_functions->getAllRecord('staff', 'initials,username,fname,lname');
        $users = array();
        $users['users'] = array();
        $users['pendingUsers'] = array();

        foreach ($rec as $key => $val) {
            try {
                $isUser = $api->getUser($val->username);

                if ($isUser['status'] == 1) {
                    array_push($users['users'], $val->username);
                }
            } catch (Exception $e) {
                $username = strtolower($val->username);
                $name = $val->fname . " " . $val->lname;
                $email = strtolower($val->username) . '@' . XMPP_HOST;

                try {
                    $api->addUser($username, 'welcome7', $name, $email);
                    array_push($users['users'], $val->username);
                } catch (Exception $e1) {
                    array_push($users['pendingUsers'], $val->username);
                }
            }
        }
    }

    public function deleteUser() {
        include APPPATH . 'third_party/php-openfire-restapi-master/vendor/autoload.php';
        $api = new Gidkom\OpenFireRestApi\OpenFireRestApi;

        $resp = array();
        try {
            $users = $api->getUsers();
            if ($users['status'] == 1) {
                if (count($users['message']->user) > 1) {
                    foreach ($users['message']->user as $key => $val) {
                        if ($val->username != 'admin') {
                            try {
                                $res = $api->deleteUser($val->username);

                                if ($res['status'] == 1) {
                                    echo $val->name . ' is Deleted' . '</br>';
                                }
                            } catch (Exception $e1) {
                                echo $val->name . ' is Not Deleted' . '</br>';
                            }
                        }
                    }
                } else {
                    echo 'Admin Record is Only pending' . "</br>";
                }
            }
        } catch (Exception $e) {
            echo 'Users Not Deleted';
        }
    }

    public function addsnsuser() {
        $rec = $this->common_functions->getAllRecord('staff', 'fname,lname,initials,username,password,title,emailid', array('isActive !=' => 0));
        if (!empty($rec)) {
            foreach ($rec as $key => $value) {
                $newUserSNS = array(
                    "firstname" => $value->fname,
                    "lastname" => $value->lname,
                    "snsusersunqid" => $value->initials,
                    "username" => $value->username,
                    "password" => $value->password,
                    "title" => $value->title,
                    "email" => $value->emailid,
                );
                $urlsns = SNSWEB + 'api/user/register';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $urlsns);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($newUserSNS));
                $response = curl_exec($ch);
            }
            die('SNS Users Created Successfully..');
        } else {
            die("System can't able to read user data ");
        }
    }

    public function checkAjaxSession() {

        $chksession = $this->session->userdata('user_data')['initials'];
        $this->load->database();

        $sql = $this->db->query("SELECT loggedin_id, forceoff FROM staff where initials='" . $chksession . "'");
        $data = $sql->result_array();

        if (count($data) > 0) {
            if ($this->session->userdata('user_data')['loggedin_id'] != $data[0]['loggedin_id']) {
                die(json_encode(array(
                    'status' => 'session_expire'
                )));
            } else if ($data[0]['forceoff'] > 0) {

                die(json_encode(array(
                    'status' => 'force_logout',
                    'time' => $data[0]['forceoff']
                )));
            } else {
                die(json_encode(array(
                    'status' => 'valid_session'
                )));
            }
        } else {
            echo json_encode(array(
                'status' => 'session_expire'
            ));
        }

        if ($data[0]['loggedin_id'] != '') {
            if ($this->session->userdata('user_data')['loggedin_id'] != $data[0]['loggedin_id']) {
                echo 1;
            } else {
                echo $data_force[0]['forceoff'];
            }
        }
    }

    public function clearloginsession() {
        $chksession = $this->session->userdata('user_data')['initials'];
        $this->load->database();

        $sql_force_update = $this->db->query("UPDATE staff set loggedin_id='',forceoff='0' where initials='" . $chksession . "'");
        $data_force_update = $sql_force_update->result_array();
    }
	
	public function copyfiles(){
        $folder =  $this->uri->segment(3); 
		//echo 'folder' . $folder; exit;
        if (!file_exists(DOCUMENTPATH . 'TEST')) {

                mkdir(DOCUMENTPATH . 'TEST', true);
        }
		
		if(!file_exists(DOCUMENTPATH.'TEST/'.$folder)){
				mkdir(DOCUMENTPATH .'TEST/'.$folder, true);
		}
        
        $files = glob('S:/A1Law/CLIENTS/'.$folder.'/*/caseact'."*");
		foreach($files as $key=>$val){
			$filearray = explode(".",$val);
			$casedetails = explode($folder.'/',$filearray[0]);
			$getcaseno = explode('/',$casedetails[1]);
			$caseno = $getcaseno[0];
			
				if(!copy($val,DOCUMENTPATH.'TEST/'.$folder.'/caseact'.$caseno.'.'.end($filearray)))
				{
					echo "<br/>failed to copy $val</br>";
				}
				
		}
		$injfiles = glob('S:/A1Law/CLIENTS/'.$folder.'/*/injury'."*");
		foreach($injfiles as $key1=>$val1){
			$filearray1 = explode(".",$val1);
			$casedetails1 = explode($folder.'/',$filearray1[0]);
			$getcaseno1 = explode('/',$casedetails1[1]);
			$caseno1 = $getcaseno1[0];
				if(!copy($val1,DOCUMENTPATH.'TEST/'.$folder.'/injury'.$caseno1.'.'.end($filearray1)))
				{
					echo "<br/>failed to copy $val1 to ".DOCUMENTPATH.'TEST/'.$folder.'/injury'.$caseno1.'.'.end($filearray1)."<br/>";
				}
				
		}
    }

	public function copyemailtext(){
		$this->load->database();
		//echo '<pre>'; print_r($data); exit;
		$data = array();
		$files = glob('S:/A1Law/MAIL/*');
		$i = 1;
		foreach($files as $key=>$val){
			
			$filenm  = explode("/",$val);
			$fileext = explode('.',end($filenm));
			$fh = fopen($val,'r');
			while ($line = fgets($fh)) {
			  // <... Do your work with the line ...>
			   $bodytext = $line;
			}
			$emailfile[$i]['filename'] = $fileext[0];
			$emailfile[$i]['mail_body'] = $bodytext;
			
			$data = $emailfile;
			fclose($fh);
			$i++;
		}
		$this->db->update_batch('email', $data, 'filename'); 
		
    }
    
    public function mergeInjurySingleTable() {
        $this->load->model('script_model');
        $this->script_model->mergeInjurySingleTable();
    }

    public function mergeCaseactSingleTable() {
        $this->load->model('script_model');
        $this->script_model->mergeCaseactSingleTable();
    }

    public function createEmailConvIdForSingleMail() {
        $this->load->model('script_model');
        $this->script_model->createEmailConvIdForSingleMail();
    }

    public function createEmailConversionEntries() {
        $this->load->model('script_model');
        $this->script_model->createEmailConversionEntries();
    }
    
    public function grabemails(){
        $this->load->model('script_model');
        $this->script_model->GrabExistingEmails();
    }

    public function createEmailTableForNewUser()
    {
        error_reporting(E_ALL);
        ini_set("display_errors", "ON");
        $this->load->model('script_model');
        $staff_listing = $this->common_functions->get_staff_listing('');
        // echo "<pre>";
        // print_r($staff_listing);
        // echo "</pre>";
        $this->script_model->createEmailTableForNewUser($staff_listing);
    }

    public function alterEmailRelatedTables()
    {
        error_reporting(E_ALL);
        ini_set("display_errors", "ON");
        $this->load->model('script_model');
        $staff_listing = $this->common_functions->get_staff_listing('');
        echo "<pre>";
        // print_r($staff_listing);
        echo "</pre>";

        $this->script_model->alterEmailRelatedTables($staff_listing);
    }
        
}
