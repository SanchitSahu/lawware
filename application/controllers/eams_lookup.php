<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Eams_lookup extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $this->load->model('eamslookup_modal');
        $data = array();
        $this->data['menu'] = 'eams_lookup';
    }

    public function index() {
        $this->data['pageTitle'] = 'Eams Lookup';

        $this->load->view('common/header',$this->data);
        $this->load->view('eams/eams_lookup');
        $this->load->view('common/footer');
    }

    public function getEamsAjaxData(){

        extract($_POST);
        $filterArray = array();
        $orCondition = '';

        if ($search['value'] != '') {
            parse_str($search['value'], $searcharray);

            foreach($searcharray as $key=>$val){
                if($key != '' && $val == ''){
                    $orCondition = array('eamsref LIKE '=>$key."%",'name LIKE'=>"%".$key."%",'address1 LIKE'=>"%".$key."%",'address2 LIKE'=>"%".$key."%",'city LIKE'=>"%".$key."%",'state LIKE'=>"%".$key."%",'zip LIKE'=>$key."%",'phone LIKE'=>$key."%");
                }
            }
        }


        $result = $this->eamslookup_modal->getEamsListing($start, $length, $filterArray, $orCondition, $order);

        $finalData = array();
        foreach ($result as $data) {

            $tmpArr = array(
                $data->eamsref,
                $data->name,
                $data->address1,
                $data->address2,
                $data->city,
                $data->state,
                $data->zip,
                $data->phone,
            );

            array_push($finalData, $tmpArr);
        }

        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $this->eamslookup_modal->getEamsListingCount($filterArray, $orCondition),
            'recordsFiltered' => $this->eamslookup_modal->getEamsListingCount($filterArray, $orCondition),
            'data' => $finalData
        );

        echo json_encode($listingData);
    }
    
    public function eamsrefDetails() {
        $eams_details = $this->eamslookup_modal->geteamsDetails($this->input->post('eamsref'));
        $eams_details1 = $this->eamslookup_modal->geteamsnumber($this->input->post('caseno'),$this->input->post('cardCode'));
        $eamsref = $this->input->post('eamsref');
        $firmcode = $eams_details1[0]->firmcode;
        $eamsDetach = $this->eamslookup_modal->updateeamsref($eamsref,$firmcode);
        
        if (count($eams_details)) {
            echo json_encode($eams_details[0]);
        } else {
            echo json_encode(false);
        }
    }
    
    public function eamsrefDetached() {
        
        $eams_details = $this->eamslookup_modal->geteamsnumber($this->input->post('caseno'),$this->input->post('cardCode'));
        //echo '<pre>'; print_r($eams_details); exit;
        $eamsref = $eams_details[0]->eamsref; 
        $firmcode = $eams_details[0]->firmcode;
        $eamsDetach = $this->eamslookup_modal->delteeamsref($eamsref,$firmcode);
        
        if (count($eams_details)) {
            echo json_encode($eams_details[0]);
        } else {
            echo json_encode(false);
        }
    }
}