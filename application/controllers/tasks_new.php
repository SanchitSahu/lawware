<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tasks_new extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $this->load->model('task_model');
        $this->load->model('common_model');
        $data = array();
        $this->data['menu'] = 'tasks';
        $this->data['username'] = $this->session->userdata('user_data')['username'];
        $this->data['loginUser'] = $this->session->userdata('user_data')['username'];
        $this->data['url_seg'] = $this->uri->segment(1);
    }

    function __destruct() {

    }

    public function index($caseNo = '') {

        $this->data['pageTitle'] = 'Task';
        $this->data['staffList'] = $this->task_model->getstaff();
        $this->data['colorCode'] = json_decode(color_codes, true);
        $this->data['caseNo'] = $caseNo;
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $this->data['groups'] = $this->common_functions->getGroups();
        $getStaffInfo = $this->common_functions->getAllRecord('staff', 'vacation', "initials ='" . $this->session->userdata('user_data')['username'] . "' ", 'initials asc');
        $getCaseCategory = $this->common_model->getAllRecord('actdeflt', 'actname,category', '', 'category asc');
        $this->data['caseactcategory'] = $getCaseCategory;
        $this->data['staff_details'] = $getStaffInfo;
        $this->load->view('common/header', $this->data);
        $this->load->view('tasks/tasks');
        $this->load->view('common/tasks_modals');
        $this->load->view('common/footer');
    }

    public function getTasksData($id = '') {

        extract($_POST);
        $filterArray = array();
        if ($id) {
            $filterArray['caseno'] = $id;
        }
        $orCondition = '(completed = "1899-12-30 00:00:00" OR completed = "0000-00-00 00:00:00")';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if ($val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);

                    if (key($filterData) == 'task-status' && $filterData[key($filterData)] != '') {
//                        var_dump($filterData[key($filterData)]);
                        if ($filterData[key($filterData)] == 'completed') {
                            $orCondition = '(completed != "1899-12-30 00:00:00" AND completed != "0000-00-00 00:00:00")';
                        } else if ($filterData[key($filterData)] == 'pending') {
                            $orCondition = '(completed = "1899-12-30 00:00:00" OR completed = "0000-00-00 00:00:00")';
                        } else if ($filterData[key($filterData)] == 'all') {
                            $orCondition = '';
                        }
                    } else {
                        if ($filterData[key($filterData)] != '') {
                            $filterArray[key($filterData)] = $filterData[key($filterData)];
                        }
                    }
                }
            }
        }

        if ($search['value'] != '') {
            $searchpriority = '';
            $searchReminder = '';
            
            if(stripos("high",$search['value']) !== false) {
                    $searchpriority = '1';
            }
            if(stripos("low",$search['value']) !== false) {
                    $searchpriority = '3';
            }
            if(stripos("medium",$search['value']) !== false) {
                    $searchpriority = '2';
            }
            if($search['value'] == 'Y' || $search['value'] == 'y'){
                $searchReminder = 'Y';
            }
            else if($search['value'] == 'N' || $search['value'] == 'n'){
                $searchReminder = 'N';
            }
            if(empty($searchpriority) && empty($searchReminder)){
                $singleFilter = '(dateass LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR whofrom LIKE "%' . $search['value'] . '%" OR whoto LIKE "%' . $search['value'] . '%" OR datereq LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR typetask LIKE "%' . $search['value'] . '%" OR event LIKE "%' . $search['value'] . '%" OR phase LIKE "%' . $search['value']. '%" OR completed LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%")';
            }else{
                if(!empty($searchReminder) && $searchReminder == 'Y'){
                    $singleFilter = '(reminder = 1 OR whofrom LIKE "%' . $search['value'] . '%" OR whoto LIKE "%' . $search['value'] . '%" OR phase LIKE "%' . $search['value']. '%" OR typetask LIKE "%' . $search['value'] . '%" OR event LIKE "%' . $search['value'] . '%")';
                }else if(!empty($searchReminder) && $searchReminder == 'N'){
                    $singleFilter = '(reminder = "" OR whofrom LIKE "%' . $search['value'] . '%" OR whoto LIKE "%' . $search['value'] . '%" OR phase LIKE "%' . $search['value']. '%" OR typetask LIKE "%' . $search['value'] . '%" OR event LIKE "%' . $search['value'] . '%")';
                }
                if(!empty($searchpriority)){
                    $singleFilter = '(Priority LIKE "%' . $searchpriority. '%" OR whofrom LIKE "%' . $search['value'] . '%" OR whoto LIKE "%' . $search['value'] . '%" OR phase LIKE "%' . $search['value']. '%" OR typetask LIKE "%' . $search['value'] . '%" OR event LIKE "%' . $search['value'] . '%")';
                }
            }
        }

        if (isset($taskBy)) {
            $filterArray[$taskBy] = $this->session->userdata('user_data')['initials'];
        }

        if (isset($whoto)) {
            $filterArray['whoto'] = $whoto;
        }

        if (isset($reminder) && $reminder == 1) {
            $filterArray['reminder'] = $reminder;
        }

        if (isset($caseno) && !empty($caseno)) {
            $filterArray['caseno'] = $caseno;
        }
        if (isset($prosno)) {
            $filterArray['prosno'] = $prosno;
        }

//        echo "<pre>";
////        print_R($orCondition);
//        var_dump($orCondition);
//        exit;
        $data = $this->task_model->getTasks($start, $length, $filterArray, $order, $orCondition, $singleFilter);
        //echo $this->db->last_query(); exit;
        $finalData = array();
        foreach ($data as $key => $val) {

            $val->dateass = ($val->dateass != '' && $val->dateass != '1899-12-30 00:00:00' && $val->dateass != '0000-00-00 00:00:00' ) ? date('m/d/Y', strtotime($val->dateass)) : '';

            $val->datereq = ($val->datereq != '' && $val->datereq != '1899-12-30 00:00:00' && $val->datereq != '0000-00-00 00:00:00' ) ? date('m/d/Y', strtotime($val->datereq)) : '';
//            $val->completed = ($val->completed != '' && $val->completed != '1899-12-30 00:00:00' && $val->completed != '0000-00-00 00:00:00' ) ? date('m/d/Y H:i A', strtotime($val->completed)) : '';
            $val->completed = ($val->completed != '' && $val->completed != '1899-12-30 00:00:00' && $val->completed != '0000-00-00 00:00:00' ) ? date("m/d/Y", strtotime($val->completed)) . "<br>" . date("h:i A", strtotime($val->completed)) : '';

            $colorCode = json_decode(color_codes, true);

            $color = ($val->color != 0) ? $colorCode[$val->color][2] : '';
            $textColor = ($val->color != 0) ? $colorCode[$val->color][1] : '';
            /*if ($val->datepopup != '1899-12-30 00:00:00' && $val->datepopup != '0000-00-00 00:00:00' && $val->datepopup != null && $val->datepopup != '1969-12-31 16:00:00') {
                $reminderdatetime = date('m/d/Y', strtotime($val->datepopup)) . "<br>".date('H:i A', strtotime($val->datepopup));
            } else {
                $reminderdatetime = 'NA';
            }*/
            /* Bellow changes as per client feedback*/
            if ($val->reminder == '1') {
                $reminderdatetime = 'Y';
            } else {
                $reminderdatetime = 'N';
            }

            if (strlen($val->event) > 28) {
                $event = substr($val->event, 0, 28) . '...';
            } else {
                $event = $val->event;
            }
            $priority = '';
            if ($val->priority == '1') {
                $priority = 'High';
            }
            if ($val->priority == '3') {
                $priority = 'Low';
            }
            if ($val->priority == '2') {
                $priority = 'Medium';
            }
            $finalData[$key] = array(
                $val->dateass,
                $event,
                $val->whofrom,
                $val->whoto,
                $val->datereq,
                ucfirst($val->typetask),
                $priority,
                $reminderdatetime,
                $val->completed,
                $val->mainkey,
                $color . "-" . $textColor,
                $val->caseno,
            );
        }

        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $this->task_model->getTaskCount($filterArray, $orCondition, $singleFilter),
            'recordsFiltered' => $this->task_model->getTaskCount($filterArray, $orCondition, $singleFilter),
            'data' => $finalData
        );

        echo json_encode($listingData);
    }

    public function createArray($taskArray) {

        //$taskArray['phase'] = '';
        if($taskArray['datereq'] != '')
        {
            $taskArray['datereq'] = date('Y-m-d H:i:s', strtotime($taskArray['datereq']));
        }
        $taskArray['caseno'] = isset($taskArray['caseno']) ? $taskArray['caseno'] : 0;
        $taskArray['prosno'] = isset($taskArray['prosno']) ? $taskArray['prosno'] : 0;
        $taskArray['notify'] = isset($taskArray['notify']) ? 1 : 0;
        $taskArray['completed'] = isset($taskArray['completed']) ? $taskArray['completed'] : '';
        $taskArray['order1'] = isset($taskArray['order1']) ? $taskArray['order1'] : 0;
        $taskArray['reminder'] = isset($taskArray['reminder']) ? $taskArray['reminder'] : '';
        if($taskArray['datepopup'] != '')
        {
            $taskArray['datepopup'] = isset($taskArray['datepopup']) ? $taskArray['datepopup'] : '';
        }

        return $taskArray;
    }

    public function addTask() {
        $taskArray = $_POST;
        $taskArray = $this->createArray($taskArray);
        $taskArray['dateass'] = date('Y-m-d H:i:s');

        if (isset($taskArray['whoto2'])) {

            $taskArray['whoto'] = $taskArray['whoto2'];
            unset($taskArray['whoto2']);
        }

        if (isset($taskArray['whoto3'])) {
            $taskArray['whoto'] = $taskArray['whoto3'];
            unset($taskArray['whoto3']);
        }

        if ($taskArray['rdate'] && array_key_exists('rdate',$taskArray)) {
            $taskArray['datepopup'] = $taskArray['rdate'] . "" . $taskArray['rtime'];
            unset($taskArray['rdate']);
            unset($taskArray['rtime']);
            $taskArray['datepopup'] = date("Y-m-d H:i:s", strtotime($taskArray['datepopup']));
        }
        else
        {
            $taskArray['datepopup'] = '';
            unset($taskArray['rdate']);
            unset($taskArray['rtime']);
        }

        if (isset($taskArray['reminder']) && $taskArray['reminder'] == 'on') {
            $taskArray['reminder'] = 1;
        }

        if (isset($taskArray['reminder1']) && $taskArray['reminder1'] == 'on') {
            unset($taskArray['reminder1']);
            $taskArray['reminder'] = 1;
        }

        //echo '<pre>'; print_r($taskArray); exit;
        unset($taskArray['taskid']);

        $taskId = $this->common_functions->insertRecord('tasks', $taskArray);

        $response = array();
        if ($taskId > 0) {
            if ($taskArray['notify'] > 0) {
                $res = $this->sendTaskEmail($taskArray['whofrom'], $taskArray['whoto'], $taskArray['event'], $taskArray['datereq'], $taskArray['caseno']);
                if (!$res) {
                    $response = array('status' => 1, 'task_id' => $taskId, 'message' => 'Task Added Successfully But Mail is not Sent Due to some Issue.');
                    echo json_encode($response);
                    exit;
                }
            }
            /*For adding case aactivity (Namrata) [starts]*/
            if(isset($taskArray['caseno']) && $taskArray['caseno'] != "") {
                $result1 = $this->task_model->getcompletedtask(array('mainkey' => $taskId));
                $record = array('caseno' => $caseno, 'initials0' => $this->session->userdata('user_data')['initials'], 'initials' => $this->session->userdata('user_data')['initials'], 'date' => date('Y-m-d H:i:s'), 'category' => 1, 'mainnotes' => 0, 'event' => 'Task-->  ' . $taskArray['event'] . '(Assigned:' . date('m/d/Y') . ')(from:' . $result1->whofrom . ') (to:' . $result1->whoto . ') (Finish By: ' . date('m/d/Y', strtotime($result1->datereq)) . ')', 'caseno' => $taskArray['caseno'], 'color' => $result1->color);
                $this->common_functions->insertCaseActRecord($record);
            }
            /*For adding case aactivity (Namrata) [ends]*/

            $response = array('status' => 1, 'task_id' => $taskId, 'message' => 'Task is successfully added.');
        } else {
            $response = array('status' => 0, 'message' => 'Task Not Added.');
        }

        echo json_encode($response);
    }

    public function getTask() {

        extract($_POST);

        $cond = array();
        $cond['mainkey'] = $taskId;

        $checkTask = $this->task_model->checkTaskCompletetion($cond);

        if (isset($checkTask->completed) && $checkTask->completed != '0000-00-00 00:00:00' && $checkTask->completed != '1899-12-30 00:00:00') {
            $response = array('status' => 0, 'message' => "Completed Tasks can't be Edited.");
            echo json_encode($response);
            exit;
        }

        $rec = $this->common_functions->getRecordById('tasks', '*', $cond);

        if ($rec->datereq != '1899-12-30 00:00:00' && $rec->datereq != '0000-00-00 00:00:00') {
            $rec->datereq = date('m/d/Y', strtotime($rec->datereq));
        }

        if ($rec->datepopup != '0000-00-00 00:00:00') {
            $popdttm = explode(" ", $rec->datepopup);
            $rec->popupdate = date('m/d/Y', strtotime($popdttm[0]));
            $rec->popuptime = date('h:i A', strtotime($popdttm[1]));
        }


        echo json_encode($rec);
    }

    public function editTask() {
        $taskArray = $_POST;
        //print_r($taskArray); exit;
        $taskId = $taskArray['taskid'];

        $cond = array();
        $cond['mainkey'] = $taskId;

        unset($taskArray['taskid']);

        $taskArray = $this->createArray($taskArray);

        foreach ($taskArray as $key => $value) {
            if ($key == 'whoto2') {
                $taskArray['whoto'] = $value;
                unset($taskArray[$key]);
            }

            if ($key == 'whoto3') {
                $taskArray['whoto'] = $value;
                unset($taskArray[$key]);
            }
        }

        if (isset($taskArray['rdate'])) {
            $taskArray['datepopup'] = $taskArray['rdate'] . "" . $taskArray['rtime'];
            unset($taskArray['rdate']);
            unset($taskArray['rtime']);
            $taskArray['datepopup'] = date("Y-m-d H:i:s", strtotime($taskArray['datepopup']));
        }

        $case_no = $taskArray['caseno'];
        if (isset($taskArray['reminder']) && $taskArray['reminder'] == 'on') {
            $taskArray['reminder'] = 1;
        }

        if (isset($taskArray['reminder1']) && $taskArray['reminder1'] == 'on') {
            unset($taskArray['reminder1']);
            $taskArray['reminder'] = 1;
        }

        if(isset($case_no) && $case_no != "") {
            $result1 = $this->task_model->getcompletedtask(array('mainkey' => $taskId));
            //echo '<pre>'; print_r($result1); exit;
            $record = array('caseno' => $case_no, 'initials0' => $this->session->userdata('user_data')['initials'], 'initials' => $this->session->userdata('user_data')['initials'], 'date' => date('Y-m-d H:i:s'), 'category' => 1, 'mainnotes' => 0, 'event' => 'Task-->  ' . $taskArray['event'] . '(Updated:' . date('m/d/Y') . ')(from:' . $taskArray['whofrom'] . ') (to:' . $taskArray['whoto'] . ') (Finish By: ' . date('m/d/Y', strtotime($taskArray['datereq'])) . ')', 'caseno' => $case_no, 'color' => $taskArray['color']);
            $this->common_functions->insertCaseActRecord($record);
        }
        

        //echo '<pre>'; print_r($taskArray); exit;

        $result = $this->common_functions->editRecord('tasks', $taskArray, $cond);

        $response = array();
        if ($result >= 0) {
            //,$taskArray['caseno']
            if ($taskArray['notify'] > 0) {
                $res = $this->sendTaskEmail($taskArray['whofrom'], $taskArray['whoto'], $taskArray['event'], $taskArray['datereq'], $taskArray['caseno']);
                if (!$res) {
                    $response = array('status' => 1, 'task_id' => $taskId, 'message' => 'Task Updated Successfully But Mail is not Sent Due to some Issue.');
                    echo json_encode($response);
                    exit;
                }
            }

            $response = array('status' => 1, 'task_id' => $taskId, 'message' => 'Task Edited Successfully.');
        } else {
            $response = array('status' => 0, 'message' => 'Task Not Added.');
        }

        echo json_encode($response);
    }

    public function deleteRec() {
        extract($_POST);

        $cond = array();
        $cond['mainkey'] = $taskId;
        $orCond = "(completed='0000-00-00 00:00:00' OR completed='1899-12-30 00:00:00')";

        $checkTask = $this->task_model->checkTaskCompletetion($cond);

        if (isset($checkTask->completed) && $checkTask->completed != '0000-00-00 00:00:00' && $checkTask->completed != '1899-12-30 00:00:00') {
            $response = array('status' => 0, 'message' => "Task is Completed So It can't be Deleted.");
            echo json_encode($response);
            exit;
        }

        $rec = $this->task_model->deleteTask($cond, $orCond);

        $response = array();
        if ($rec > 0) {
            $response = array('status' => 1, 'message' => 'Task Deleted Successfully.');
        } else {
            $response = array('status' => 0, 'message' => 'Oops! Something went wrong.');
        }

        echo json_encode($response);
    }

    public function completedTask() {
        extract($_POST);

        $taskId = $_POST['taskId'];
        $cond['mainkey'] = $taskId;
        $new_Arr = array();
        $rec = $this->common_functions->getRecordById('tasks', '*', $cond);
        //echo '<pre>'; print_r($rec);
        $new_Arr['caseno'] = $rec->caseno;
        $new_Arr['initials0'] = $this->session->userdata('user_data')['initials'];
        $new_Arr['initials'] = $this->session->userdata('user_data')['initials'];
        $new_Arr['date'] = date('m/d/Y');
        $new_Arr['event'] = "TASK->" . $rec->event . " (Completed: " . $new_Arr['date'] . ")  (from: " . $rec->whofrom . ")  (to: " . $rec->whoto . ")  (Finish by: " . date('m/d/Y', strtotime($rec->datereq)) . ")";
        $resAct = $this->common_functions->getRecordById('caseact', 'actno', 'caseno =' . $rec->caseno . ' order by actno desc limit 0,1');

        if (!empty($resAct)) {
            $caseActno = $resAct->actno;
        } else {
            $caseActno = 0;
        }
        $new_Arr['actno'] = $caseActno + 1;
        $new_Arr['access'] = 1;
        $new_Arr['category'] = 1;
        $new_Arr['mainnotes'] = 0;
        $new_Arr['color'] = $rec->color;
        //echo '<pre>'; print_r($new_Arr);exit;
        $data = array('completed' => date('Y-m-d H:i:s'));
        $result = $this->common_functions->editRecord('tasks', $data, array('mainkey' => $taskId));

        if ($result >= 0) {
            $this->common_functions->insertRecord('caseact', $new_Arr);
            $response = array('status' => 1, 'message' => 'Task set as Completed.');
        } else {
            $response = array('status' => 0, 'message' => 'Oops! Something went wrong.');
        }

        echo json_encode($response);
    }

    public function checkforlasttask()
    {
        $result1 = $this->task_model->getcompletedtask(array('mainkey' => $taskId));
        $cond = array('completed' => '0000-00-00 00:00:00', 'caseno' => $caseno);
        echo $checkRecord = $this->common_functions->check_record_exists('tasks', $cond);
    }

    public function completedTaskWithCase() {
        extract($_POST);
        // print_r($_POST);exit;


        $data = array('completed' => date('Y-m-d H:i:s'));
        $result = $this->common_functions->editRecord('tasks', $data, array('mainkey' => $taskId));

        if ($result >= 0) {


            $result1 = $this->task_model->getcompletedtask(array('mainkey' => $taskId));
            $cond = array('completed' => '0000-00-00 00:00:00', 'caseno' => $caseno);
            $checkRecord = $this->common_functions->check_record_exists('tasks', $cond);

            //print_r($result1);echo ;exit;
//            $record = array('caseno' => $caseno, 'initials0' => $this->session->userdata('user_data')['initials'], 'initials' => $this->session->userdata('user_data')['initials'], 'date' => date('Y-m-d H:i:s'), 'category' => $case_category, 'event' => 'Task-->  ' . $case_event . '(Assigned:' . date('m/d/Y', strtotime($result1->dateass)) . ')(from:' . $result1->whofrom . ') (to:' . $result1->whoto . ') (Finish by: ' . date('Y-m-d H:i:s', strtotime($result1->datereq)) . ')-->' . $case_extra_event, 'caseno' => $caseno, 'color' => $result1->color);
            $record = array('caseno' => $caseno, 'initials0' => $this->session->userdata('user_data')['initials'], 'initials' => $this->session->userdata('user_data')['initials'], 'date' => date('Y-m-d H:i:s'), 'category' => $case_category, 'mainnotes' => 0, 'event' => 'Task-->  ' . $case_event . '(Completed:' . date('m/d/Y', strtotime($result1->completed)) . ')(from:' . $result1->whofrom . ') (to:' . $result1->whoto . ') (Finish by: ' . date('m/d/Y', strtotime($result1->datereq)) . ')-->' . $case_extra_event, 'caseno' => $caseno, 'mainnotes' => 0, 'color' => $result1->color);
            $this->common_functions->insertCaseActRecord($record);
            if($checkRecord > 0)
            {
                $response = array('status' => 1, 'message' => 'Task set as Completed.');
            }else{
                $response = array('status' => 2, 'message' => 'Task set as Completed.');

            }
        } else {
            $response = array('status' => 0, 'message' => 'Oops! Something went wrong.');
        }

        echo json_encode($response);
    }

    public function printTask() {
        extract($_POST);


        // if(isset($printDay)){
        //
        // }

        $con = array();

        if (isset($start_printData) && $start_printData != '') {
            $con['datereq >='] = date('Y-m-d 00:00:00', strtotime($start_printData));
        }

        if (isset($end_printData) && $end_printData != '') {
            $con['datereq <='] = date('Y-m-d 23:59:59', strtotime($end_printData));
        }

        if (isset($searchBy)) {
            $searchBy = json_decode($searchBy, true);
            if (isset($searchBy['taskBy'])) {
                $con[$searchBy['taskBy']] = $this->data['username'];
            }
            if (isset($searchBy['caseno'])) {
                $con['caseno'] = $searchBy['caseno'];
            }
            if (isset($searchBy['prosno'])) {
                $con['prosno'] = $searchBy['prosno'];
            }
        }

        if (isset($print_type) && $print_type != 1) {
            if ($print_type == 3) {
                if (isset($selectedData) && count($selectedData) > 0) {
                    $con['selectedData'] = $selectedData;
                }
            }
        }



        //if(isset($namecheck)) ?
        $record = $this->task_model->getTaskPrintData($con);


        $page = '<style>@page { size: auto;  margin: 10mm; }</style><div id="printContent"><table style="width:100%;"><tr style="border-bottom:1pt solid black;height: 30px;">
                    <th style="width:15%">Date</th>
                    <th style="width:10%">From</th>
                    <th style="width:10%">To</th>
                    <th style="width:15%">Finish By</th>
                    <th style="width:10%">Case No</th>';
        if (isset($print_type) && $print_type == 5) {
            $page .= '<th style="width:7%;">Type</th>
                    <th style="width:7%;">Priority</th>
                    <th style="width:6%;">Phase</th>';
        }
        $page .= '<th style="width:40%">Event</th></tr>';

        foreach ($record as $key => $value) {
            $page .= '<tr style="height:30px;">';
            $page .= '<td>' . $value->date . '</td>';
            $page .= '<td>' . $value->whofrom . '</td>';
            $page .= '<td>' . $value->whoto . '</td>';
            $page .= '<td>' . $value->finishby . '</td>';
            $page .= '<td>' . $value->caseno . '</td>';
            if (isset($print_type) && $print_type == 5) {
                $page .= '<td>' . $value->typetask . '</td>';
                $page .= '<td>' . $value->priority . '</td>';
                $page .= '<td>' . $value->phase . '</td>';
            }
            $page .= '<td style="white-space:pre-line;">' . $value->event . '</td>';
            $page .= '</tr>';
        }
        $page .= '</table></div>';

        echo $page;
    }

    public function sendTaskEmail($whofrom, $whoto, $subject, $taskDate, $caseno = 0) {
        $mailsubject = "Task FOR " . (($caseno > 0) ? $whoto . '-' . $caseno : $whoto);
        $date = date('m/d/Y', strtotime($taskDate));
        $urgent = '';

        $this->load->model('email_model_new');

        $parcel_id = $this->email_model_new->getParcelId(); 
        $new_parcel_id = $parcel_id + 1;
        
        $date_sent = date("m/d/Y h:i:s a") . "\n";

        $mailbody = '';
        $mailbody .= "***********************************************************************\n";
        $mailbody .= "                      Task Assigned to " . $whoto . "                      \n";
        if ($caseno > 0) {
            $mailbody .= "Case No : " . $caseno . "                                              \n";
        }
        $mailbody .= "Task Date : " . $date . "                                              \n";
        $mailbody .= "Task : " . $subject . "\n";
        $mailbody .= "Task Assign Date : " . $date_sent . "\n";
        $mailbody .= "From : " . $whofrom . "\n";
        $mailbody .= "To : " . $whoto . "\n";
        $mailbody .= "-----------------------------------------------------------------------\n";


        //$res = $this->email_new_model->send_email($whofrom, $whoto, $mailsubject, $urgent, $mailbody, $caseno, $email_con_id, 'I');
        $res = $this->email_model_new->send_email_to($new_parcel_id, '2', 'Sent Mail', $urgent,$mailsubject, $mailbody, $caseno,$whofrom ,$whoto,$whofrom,'');
        $res = $this->email_model_new->send_email_to($new_parcel_id, '1', 'New Mail', $urgent,$mailsubject, $mailbody, $caseno,$whofrom ,$whoto,$whoto,'');
        if ($res != 0) {
            return true;
        } else {
            return false;
        }
    }

    public function remindernotify() {
        $checkTask = $this->task_model->checkTaskReminder();
        //$result = json_encode($checkTask);
        //echo '<pre>'; print_r($checkTask); exit;
        die(json_encode($checkTask));
    }
    public function crontasksdeleted()
    {
        $deletedtask=$this->task_model->tasksdeleteddayscount();
    }

    public function getGroupMembers() {
        echo json_encode($this->common_functions->getGroupMembers($_POST['group_id']));
    }

}
