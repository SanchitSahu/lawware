<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $data = array();
        $this->data['menu'] = 'calendar';
        $this->load->model('calendar_model');
        $this->load->model('case_model');
        $this->load->model('common_model');
        $this->data['username'] = $this->session->userdata('user_data')['username'];
    }

    public function index($case_no = '') {
        
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://108.222.150.121:8082/sns-lawware/api/EventsTemplates.php',
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        $response = json_decode($resp);
        curl_close($curl);
        $this->data['ticklerEvent'] = $response->result;
        
        $this->output->enable_profiler(false);
        $this->data['pageTitle'] = 'Calendar';
        $getAllStaff = $this->common_model->getAllRecord('staff', 'DISTINCT(initials)', '', 'initials asc');
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $getcaldata = array();
        $getcaldata[0]->calnotes = $this->data['system_data'][0]->calnotes;
        $getcaldata[0]->caltime = $this->data['system_data'][0]->caltime;
        $getcaldata[0]->calendars = $this->data['system_data'][0]->calendars;
        $getcaldata[0]->calattyass = $this->data['system_data'][0]->calattyass;
        $getcaldata[0]->calview = $this->data['system_data'][0]->calview;
        $main1 = json_decode($this->data['system_data'][0]->main1);
        $calDef = json_decode($this->data['system_data'][0]->caldeflt);

        $calDefAtty = '';
        $calDefAttass = '';

        $calYrRng = $calDef->yeartud;
        if ($case_no != '') {
            $case_details = $this->case_model->getCaseFullDetails($case_no);
            $this->data['display_details'] = $case_details[0];
            $defendant = [];
            
            $defendant_value = $this->checkDefendant();
            if(array_key_exists('emp', $defendant_value)) {
                $emp_exists = true;
            }
            if (array_key_exists('def', $defendant_value)) {
                $def_exists = true;
            }
            
            if($emp_exists && $def_exists) {
                foreach ($defendant_value as $key => $value) {
                    if($value == 'EMPLOYER') {
                        $def_value = 'EMPLOYER';
                    } elseif ($value == 'DEFENDANT') {
                        $def_value = 'DEFENDANT';
                    }
                    break;
                }
                foreach ($case_details as $data) {
                    if (($data->cardtype) == $def_value || ($data->cardtype) == strtolower($def_value)) {
                        if (!empty($data->firm)) {
                            $defendant[] = $data->firm;
                        } else {
                            $defendant[] = $data->first . ' ' . $data->last;
                        }
                    }
                }
                if(count($defendant) == 0) {
                    if($def_value == 'EMPLOYER') {
                        $another_def_value = 'DEFENDANT';
                    } elseif($def_value == 'DEFENDANT') {
                        $another_def_value = 'EMPLOYER';
                    }
                    foreach ($case_details as $data) {
                        if (($data->cardtype) == $another_def_value || ($data->cardtype) == strtolower($another_def_value)) {
                            if (!empty($data->firm)) {
                                $defendant[] = $data->firm;
                            } else {
                                $defendant[] = $data->first . ' ' . $data->last;
                            }
                        }
                    }
                }
            } elseif ($emp_exists) {
                $def_value = $defendant_value['emp'];
                foreach ($case_details as $data) {
                if (($data->cardtype) == $def_value || ($data->cardtype) == strtolower($def_value)) {
                        if (!empty($data->firm)) {
                            $defendant[] = $data->firm;
                        } else {
                            $defendant[] = $data->first . ' ' . $data->last;
                        }
                    }
                }
            } elseif ($def_exists) {
                $def_value = $defendant_value['def'];
                foreach ($case_details as $data) {
                if (($data->cardtype) == $def_value || ($data->cardtype) == strtolower($def_value)) {
                        if (!empty($data->firm)) {
                            $defendant[] = $data->firm;
                        } else {
                            $defendant[] = $data->first . ' ' . $data->last;
                        }
                    }
                }
            }
            
            //echo '<pre>';print_r($defendant); exit;
            if(!empty($defendant))
                $defendant = implode(';',$defendant);
            else
                $defendant = '';
            //echo 'defendant =>'.$defendant; exit;
            $this->data['defendant'] = $defendant;
        }
        if (isset($calDef->udifa) && $calDef->udifa = 'on') {
            $calDefAtty = $this->session->userdata('user_data')['initials'];
        }
        if (isset($calDef->udifaa) && $calDef->udifaa = 'on') {
            $calDefAttass = $this->session->userdata('user_data')['initials'];
        }

        $this->data['calYrRng'] = $calYrRng;
        $this->data['calDefAtty'] = $calDefAtty;
        $this->data['calDefAttass'] = $calDefAttass;

        $this->data['atty_hand'] = explode(',', trim($this->data['system_data'][0]->popattyh, ','));
        $this->data['atty_resp'] = explode(',', trim($this->data['system_data'][0]->popattyr, ','));
        /* $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh);
          $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh); */
        $this->data['staffList'] = $getAllStaff;
        $this->data['JudgeName'] = explode(',', $main1->judge);
        $this->data['Venue'] = explode(',', $main1->venue);
        $this->data['system_caldata'] = $getcaldata;
        $this->data['case_no'] = $case_no;

        $this->load->view('common/header', $this->data);
        $this->load->view('calendar/calendar');
        $this->load->view('common/eventmodal');
        $this->load->view('common/footer');
    }

    public function getCalendarEvents() {

        extract($_POST);

        $start_date = date('Y-m-d 00:00:00', strtotime($start));
        $end_date = date('Y-m-d 23:59:59', strtotime($end));
        $attys = $attya;
        $attyh = $attyh;
        $caltyp = $caltyp;
        $caseNo = $caseno;

        if (!empty($caseNo)) {
            $where1 = Array('c1.caseno' => $caseNo);
        } else {
            $where1 = '';
        }

        $where = array(
            'c1.event !=' => '',
            'c1.todo' => $caltyp
        );

        $where_and = "(c1.date >= '$start_date' AND c1.date <= '$end_date' )";

        if (!empty($attys)) {
            $where['c1.attyass'] = $attys;
        }
        if (!empty($attyh)) {
            $where['c2.initials'] = $attyh;
        }

        $orderby = 'date ASC';
        $result = $this->common_functions->getAllEventRecord($where, $orderby, $where1, $where_and);
        //echo $this->db->last_query();
        $rec = array();

        $color_codes = $this->config->item('color_codes');
        $this->data['color_codes'] = json_decode(color_codes, true);
        $json_color = $this->data['color_codes'];
        //echo '<pre>';  print_r($result); exit;
        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $rec[$key]['caseno'] = $val->caseno;
                $endtime = date('Y-m-d H:i:s', strtotime('+10 minutes', strtotime($val->date)));
                $eventTime = date("h:i A", strtotime($val->date));
                $rec[$key]['eventno'] = intval($val->eventno);
                $rec[$key]['title'] = $val->event;
                $rec[$key]['first'] = $val->first;
                $rec[$key]['last'] = $val->last;
                $rec[$key]['eventTime'] = $eventTime;
                $rec[$key]['todo'] = $val->todo;
                if (!empty($val->intl))
                    $rec[$key]['attyH'] = $val->intl;
                else
                    $rec[$key]['attyH'] = '---';
                if (!empty($val->attyass))
                    $rec[$key]['attyA'] = $val->attyass;
                else
                    $rec[$key]['attyA'] = '---';
                if (!empty(trim($val->defendant)))
                    $rec[$key]['defendant'] = $val->defendant;
                else
                    $rec[$key]['defendant'] = '---';
                if (!empty($val->venue))
                    $rec[$key]['venue'] = $val->venue;
                else
                    $rec[$key]['venue'] = '---';
                if (!empty($val->judge))
                    $rec[$key]['judge'] = $val->judge;
                else
                    $rec[$key]['judge'] = '---';
                if (!empty($val->notes)) {
                    if (strlen($val->notes) > 45)
                        $rec[$key]['notes'] = substr($val->notes, 0, 45) . " ...";
                    else
                        $rec[$key]['notes'] = $val->notes;
                } else
                    $rec[$key]['notes'] = '---';
                $rec[$key]['start'] = date('Y-m-d H:i:s', strtotime($val->date));

                if (array_key_exists($val->color, $json_color)) {
                    if ($json_color[$val->color][2] == '#FFFFFF') {
                        $rec[$key]['textColor'] = $json_color[$val->color][1];
                        $rec[$key]['color'] = '#f5f5f5';
                    } else {
                        $rec[$key]['textColor'] = $json_color[$val->color][1];
                        $rec[$key]['color'] = $json_color[$val->color][2];
                    }
                } else {
                    $rec[$key]['textColor'] = '#FFFFFF';
                    $rec[$key]['color'] = '#4d7ab1';
                }
            }
        }

        echo json_encode($rec);
    }

    public function getCalendarData() {

        extract($_POST);

        $result = $this->calendar_model->getCalendarDataByEvent($eventno);
        //  print_r($result);exit;

        if ($result->caldate != '' && $result->caldate != '1899-12-30') {
            $result->caldate = date('m/d/Y', strtotime($result->caldate));
        } else {
            $result->caldate = '';
        }
        if ($result->caltime == '' || $result->caltime == '00:00:00') {
            $result->caltime = '';
        }

        if ($result->tickdate != '' && $result->tickdate != '1899-12-30') {
            $result->tickdate = date('m/d/Y', strtotime($result->tickdate));
        } else {
            $result->tickdate = '';
        }

        if ($result->ticktime == '' || $result->ticktime == '00:00:00') {
            $result->ticktime = '';
        }

        if ($result->chgdate != '' && $result->chgdate != '1899-12-30') {
            $result->chgdate = date('m/d/Y', strtotime($result->chgdate));
        } else {
            $result->chgdate = '';
        }
        if ($result->chgtime == '' || $result->chgtime == '00:00:00') {
            $result->chgtime = '';
        }
		
        echo json_encode($result);
    }

    public function addCalendarData() {

        extract($_POST);
        //print_r($_POST);exit;
        //$color='';
        
	$tododata = $_POST['todo'];
        $result = array();
        //if ($first != '' && $last != '' && $caldate != '' && $caltime != '' && $event != '') {
        if ($caldate != '' && $caltime != '' && $event != '') {

            //$date = date('Y-m-d', strtotime($caldate)) . " " . date('H:i:s', strtotime($caltime));
            $date = date("Y-m-d H:i", strtotime($caldate.' '.$caltime));
            //echo 'date =>'.$date; exit;
            $protected = (isset($check_protected) && $check_protected == 1) ? $check_protected : 0;
            $rollover = (isset($rollover) && $rollover == 1) ? $rollover : 0;

            if ($tickledate != '') {
                $tickleDate = date('Y-m-d', strtotime($tickledate)) . " " . date('H:i:s', strtotime($tickletime));
            } else {
                $tickleDate = '1899-12-30 00:00:00';
            }

            $chglast = date('Y-m-d H:i:s');
            $caseno = ($caseno != '') ? $caseno : 0;
            $newcaltime = $caldate . ' ' . $caltime;
            
            if($caseno != 0){
                $caseNoArray[] = $caseno;
                $record = $this->case_model->getCaseDetailsforSNS($caseNoArray);
                $email = $record->email;
                if (!empty($record->home)) {
                    $phoneno = $record->home;
                } elseif (!empty($record->business)) {
                    $phoneno = $record->business;
                } elseif (!empty($record->fax)) {
                    $phoneno = $record->fax;
                }elseif (!empty($record->car)) {
                    $phoneno = $record->car;
                }
               
            }
            else{
                $email = '';
                $phoneno = '';
            }

            //$condition = array('first' => $first, 'last' => $last, 'DATE(`date`)' => date('Y-m-d', strtotime($caldate)), 'event' => $event);
            //$condition = array('cal2.initials' => $atty_hand, 'cal1.DATE(`date`)' => date('Y-m-d', strtotime($caldate)), 'cal1.event' => $event);
            $rec = $this->calendar_model->checkevent($attyass, $newcaltime, $tododata);
            //$rec = $this->common_functions->check_record_exists('cal1', $condition);
            //echo $this->db->last_query();exit;
            //echo 'count => ' . count($rec);
            //echo '<pre>'; print_r($rec); exit;
             $user_initials = $this->session->userdata('user_data')['initials'];

            $eventId = $this->common_functions->last_record('cal1', 'eventno');

            $cal_array = array('eventno' => $eventId + 1,
                'macroid' => 0,
                'calendar' => 1,
                /* 'calstat' => $calstat, */
                'judge' => $judge,
                'first' => $first,
                'last' => $last,
                'defendant' => $defendant,
                /*'attyass' => isset($attyass) ? $attyass : '',*/
                'venue' => $venue,
                'location' => $location,
                'date' => $date,
                'tottime' => $totime,
                'event' => $event,
                'notes' => $notes,
                'tickle' => $tickle,
                'tickledate' => $tickleDate,
                'whofrom' => isset($whofrom) ? $whofrom : $this->session->userdata('user_data')['username'],
                'whoto' => isset($whoto) ? $whoto : $this->session->userdata('user_data')['username'],
                'todo' => isset($todo) ? intval($todo) : '',
                'color' => intval($color),
                'caseno' => $caseno,
                'initials0' => $user_initials,
                'initials' => $user_initials,
                'chgwho' => $user_initials,
                'chglast' => $chglast,
                'protected' => intval($protected),
                'rollover' => intval($rollover),
                'reminder' => $calreminder,
                'doctor' => $doctor,
                'phone' => $phoneno,
                'email' => $email,
                'issync'=>0,
                'tevent'=>$tevent
            );

            if($attyass != 'Select Attorney Assigned') {
                $cal_array['attyass'] = $attyass;
            } else {
                $cal_array['attyass'] = '';
            }

            $calendar_event = 'Calendar--> '.$event.', '.$date.', Judge: '.$judge.', Defendant: '.$defendant.', Atty Assigned: '.$attyass;
            $case_act_array = array(
                'caseno' => $caseno,
                'date' => date('Y-m-d H:i:s'),
                'event' => $calendar_event,
                'initials' => $user_initials,
                'initials0' => $user_initials,
                'category' => 1,
                'mainnotes' => 0,
                'color' => intval($color)
            );

            if (count($rec) == 0) {
               $lastRecord = $this->common_functions->insertRecord('cal1', $cal_array);
               $res = $this->common_functions->insertCaseActRecord($case_act_array);

                if ($atty_hand != '') {
                    $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventId + 1, 'initials' => $atty_hand));
                } else{
                    $atty_hand = '';
                }
                $cal_array['atty_hand'] = $atty_hand;
                $cal_atty_ass = $this->case_model->check_attya_checkbox();
                if(isset($cal_atty_ass) && $cal_atty_ass != '') {
                    if($cal_atty_ass == 1) {
                        $cal_array['attyass'] = $attyass;
                    } elseif($cal_atty_ass == 2) {
                        if($caseno == 0) {
                            $cal_array['attyass'] = $attyass;
                        }
                    }/* elseif($cal_atty_ass == 3) {
                        $cal_array['attyass'] = '';
                    }*/
                }

                $result = array('status' => 1, 'eventno' => $eventId + 1, 'calendarData' => $cal_array, 'message' => 'Calendar Event Added Successfully');
            } else if(count($rec) > 0 && $tododata == 0) {
                
                $lastRecord = $this->common_functions->insertRecord('cal1', $cal_array);
                $res = $this->common_functions->insertCaseActRecord($case_act_array);

                if ($atty_hand != '') {
                    $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventId + 1, 'initials' => $atty_hand));
                } else {
                    $atty_hand = '';
                }
                $cal_array['atty_hand'] = $atty_hand;
                $cal_atty_ass = $this->case_model->check_attya_checkbox();
                if(isset($cal_atty_ass) && $cal_atty_ass != '') {
                    if($cal_atty_ass == 1) {
                        $cal_array['attyass'] = $attyass;
                    } elseif($cal_atty_ass == 2) {
                        if($caseno == 0) {
                            $cal_array['attyass'] = $attyass;
                        }
                    }/* elseif($cal_atty_ass == 3) {
                        $cal_array['attyass'] = '';
                    }*/
                }

                $result = array('status' => 1, 'eventno' => $eventId + 1, 'calendarData' => $cal_array, 'message' => 'Calendar Event Added Successfully');
                //$result = array('status' => 0, 'message' => 'Event Already Exists For a Day');
            }
            else if(count($rec) > 0 && $tododata == 1){
                $result = array('status' => 0, 'message' => 'Event Already Exists For a Day');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Please Fill Required Fields');
        }

        echo json_encode($result);
    }

    public function addCalendarDataD() {

        extract($_POST);
        //print_r($_POST);exit;
        //$color='';
        $result = array();
        //if ($first != '' && $last != '' && $caldate != '' && $caltime != '' && $event != '') {
        if ($caldate != '' && $caltime != '' && $event != '') {

            $date = date('Y-m-d', strtotime($caldate)) . " " . date('H:i:s', strtotime($caltime));

            $protected = (isset($check_protected) && $check_protected == 1) ? $check_protected : 0;
            $rollover = (isset($rollover) && $rollover == 1) ? $rollover : 0;

            if ($tickledate != '') {
                $tickleDate = date('Y-m-d', strtotime($tickledate)) . " " . date('H:i:s', strtotime($tickletime));
            } else {
                $tickleDate = '1899-12-30 00:00:00';
            }

            $chglast = date('Y-m-d H:i:s');
            $caseno = ($caseno != '') ? $caseno : 0;
            $newcaltime = $caldate . ' ' . $caltime;
            
            if($caseno != 0){
                $caseNoArray[] = $caseno;
                $record = $this->case_model->getCaseDetailsforSNS($caseNoArray);
                $email = $record->email;
                if (!empty($record->home)) {
                    $phoneno = $record->home;
                } elseif (!empty($record->business)) {
                    $phoneno = $record->business;
                } elseif (!empty($record->fax)) {
                    $phoneno = $record->fax;
                }elseif (!empty($record->car)) {
                    $phoneno = $record->car;
                }
               
            }
            else{
                $email = '';
                $phoneno = '';
            }

            $user_initials = $this->session->userdata('user_data')['initials'];

            $eventId = $this->common_functions->last_record('cal1', 'eventno');

            $cal_array = array('eventno' => $eventId + 1,
                'macroid' => 0,
                'calendar' => 1,
                /* 'calstat' => $calstat, */
                'judge' => $judge,
                'first' => $first,
                'last' => $last,
                'defendant' => $defendant,
                'attyass' => isset($attyass) ? $attyass : '',
                'venue' => $venue,
                'location' => $location,
                'date' => $date,
                'tottime' => $totime,
                'event' => $event,
                'notes' => $notes,
                'tickle' => $tickle,
                'tickledate' => $tickleDate,
                'whofrom' => isset($whofrom) ? $whofrom : $this->session->userdata('user_data')['username'],
                'whoto' => isset($whoto) ? $whoto : $this->session->userdata('user_data')['username'],
                'todo' => isset($todo) ? intval($todo) : '',
                'color' => intval($color),
                'caseno' => $caseno,
                'initials0' => $user_initials,
                'initials' => $user_initials,
                'chgwho' => $user_initials,
                'chglast' => $chglast,
                'protected' => intval($protected),
                'rollover' => intval($rollover),
                'reminder' => $calreminder,
                'doctor' => $doctor,
                'phone' => $phoneno,
                'email' => $email,
                'issync'=>0,
                'tevent'=>$tevent
            );

            $lastRecord = $this->common_functions->insertRecord('cal1', $cal_array);

            if ($atty_hand != '') {
                $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventId + 1, 'initials' => $atty_hand));
            }
            $cal_array['atty_hand'] = $atty_hand;
            $cal_array['attyass'] = $attyass;

            $result = array('status' => 1, 'eventno' => $eventId + 1, 'calendarData' => $cal_array, 'message' => 'Calendar Event Added Successfully');
        } else {
            $result = array('status' => 0, 'message' => 'Please Fill Required Fields');
        }

        echo json_encode($result);
    }

    public function editCalendarData() {

        extract($_POST);
		$tododata = $_POST['todo'];
        //print_r($first);exit;
        $result = array();
        if ($caldate != '' && $caltime != '' && $event != '') {

            //$first != '' && $last != '' && $caldate != '' && $caltime != '' && $event != ''
            //echo $caltime;exit;
            $date = date('Y-m-d', strtotime($caldate)) . " " . date('H:i:s', strtotime($caltime));
            $calTime = date('h:i A', strtotime($caltime));
            $protected = (isset($check_protected) && $check_protected == 1) ? $check_protected : 0;
            $rollover = (isset($rollover) && $rollover == 1) ? $rollover : 0;
            $attyass = (isset($attyass) && $attyass != '') ? $attyass : '';
            $atty_hand = (isset($atty_hand) && $atty_hand != '') ? $atty_hand : '';

            if ($tickledate != '') {
                $tickleDate = date('Y-m-d', strtotime($tickledate)) . " " . date('H:i:s', strtotime($tickletime));
            } else {
                $tickleDate = '1899-12-30 00:00:00';
            }

            $chglast = date('Y-m-d H:i:s');
            $caseno = ($caseno != '') ? $caseno : 0;
            
            if($caseno != 0){
                $caseNoArray[] = $caseno;
                $record = $this->case_model->getCaseDetailsforSNS($caseNoArray);
                $email = $record->email;
                if (!empty($record->home)) {
                    $phoneno = $record->home;
                } elseif (!empty($record->business)) {
                    $phoneno = $record->business;
                } elseif (!empty($record->fax)) {
                    $phoneno = $record->fax;
                }elseif (!empty($record->car)) {
                    $phoneno = $record->car;
                }
               
            }
            else{
                $email = '';
                $phoneno = '';
            }

            $condition = array('first' => $first, 'last' => $last, 'DATE(`date`)' => date('Y-m-d', strtotime($caldate)), 'event' => $event, 'eventno !=' => $eventno);

            //$rec = $this->common_functions->check_record_exists('cal1', $condition);
            $newcaltime = $caldate . ' ' . $caltime;
            $rec = $this->calendar_model->checkeventedit($attyass, $newcaltime, $eventno, $tododata);
            //echo $this->db->last_query();
            //echo count($rec); exit;
            $user_initials = $this->session->userdata('user_data')['initials'];

            $cal_array = array('eventno' => $eventno,
                'macroid' => 0,
                'calendar' => 1,
                /* 'calstat' => $calstat, */
                'judge' => $judge,
                'first' => $first,
                'last' => $last,
                'defendant' => $defendant,
                   /*'attyass' => $attyass,
                   'atty_hand'=>$atty_hand,*/
                'venue' => $venue,
                'location' => $location,
                'date' => $date,
                'tottime' => $totime,
                'event' => $event,
                'notes' => $notes,
                'tickle' => $tickle,
                'tickledate' => $tickleDate,
                'whofrom' => isset($whofrom) ? $whofrom : $this->session->userdata('user_data')['username'],
                'whoto' => isset($whoto) ? $whoto : $this->session->userdata('user_data')['username'],
                'todo' => isset($todo) ? intval($todo) : '',
                'color' => intval($color),
                'caseno' => $caseno,
                'chgwho' => $user_initials,
                'chglast' => $chglast,
                'protected' => intval($protected),
                'rollover' => intval($rollover),
                'reminder' => $calreminder,
                'doctor' => $doctor,
                'phone' => $phoneno,
                'email' => $email,
                'issync'=>0,
                'tevent'=>$tevent
            );

            $case_act_attyass = $this->calendar_model->get_attorney_assigned($caseno);
            if ($attyass != '') {
                if($attyass == 'Select Attorney Assigned') {
                    $attyass = '';
                }
                /*$cal_array['attyass'] = $attyass;*/
                $cal_atty_ass = $this->case_model->check_attya_checkbox();
                if(isset($cal_atty_ass) && $cal_atty_ass != '') {
                    if($cal_atty_ass == 1) {
                        $cal_array['attyass'] = $attyass;
                    } elseif($cal_atty_ass == 2) {
                        if($caseno == 0) {
                            $rec = $this->common_functions->check_record_exists('cal1', array('eventno' => $eventno));
                            if($rec = 0) {
                                $cal_array['attyass'] = $attyass;
                            }
                        }
                    }/* elseif($cal_atty_ass == 3) {
                        $cal_array['attyass'] = '';
                    }*/
                }
            }

            if($atty_hand == 'Select Attorney Handling') {
                $atty_hand = '';
            }

            $calendar_event = 'Edit Calendar--> '.$event.', '.$date.', Judge: '.$judge.', Defendant: '.$defendant.', Atty Assigned: ';
            if($attyass == '') {
                $calendar_event .= $case_act_attyass;
            } else {
                $calendar_event .= $attyass;
            }
            $case_act_array = array(
                'caseno' => $caseno,
                'date' => date('Y-m-d H:i:s'),
                'event' => $calendar_event,
                'initials' => $user_initials,
                'initials0' => $user_initials,
                'category' => 1,
                'mainnotes' => 0,
                'color' => intval($color)
            );

            $where = array('eventno' => $eventno);
            if (count($rec) == 0) {
                
                $lastRecord = $this->common_functions->editRecord('cal1', $cal_array, $where);
                $res = $this->common_functions->insertCaseActRecord($case_act_array);

                /*Commented below code because if in Edit, Select Attorney Handling is selected, then AttyH should be blank as per A1 Law.*/

                /*if ($atty_hand != '') {
                    $rec = $this->common_functions->check_record_exists('cal2', array('eventno' => $eventno));
                    if ($rec > 0) {
                        $lastAttyRecord = $this->common_functions->editRecord('cal2', array('initials' => $atty_hand), $where);
                    } else {
                        $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventno, 'initials' => $atty_hand));
                    }
                } else {
                    $atty_hand = '';
                }*/

                /*Instead of above code, this code has been updated.*/
                $rec = $this->common_functions->check_record_exists('cal2', array('eventno' => $eventno));
                if ($rec > 0) {
                    $lastAttyRecord = $this->common_functions->editRecord('cal2', array('initials' => $atty_hand), $where);
                } else {
                    $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventno, 'initials' => $atty_hand));
                }

                $cal_array['atty_hand'] = $atty_hand;
                $cal_array['attyass'] = $attyass;
                $cal_array['caltime'] = $calTime;

                $result = array('status' => 1, 'eventno' => $eventno, 'calendarData' => $cal_array, 'message' => 'Calendar Event  Updated Successfully');
            } else if(count($rec) > 0 && $tododata == 0){
                $lastRecord = $this->common_functions->editRecord('cal1', $cal_array, $where);
                $res = $this->common_functions->insertCaseActRecord($case_act_array);

                /*Commented below code because if in Edit, Select Attorney Handling is selected, then AttyH should be blank as per A1 Law.*/

                /*if ($atty_hand != '') {
                    $rec = $this->common_functions->check_record_exists('cal2', array('eventno' => $eventno));
                    if ($rec > 0) {
                        $lastAttyRecord = $this->common_functions->editRecord('cal2', array('initials' => $atty_hand), $where);
                    } else {
                        $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventno, 'initials' => $atty_hand));
                    }
                } else {
                    $atty_hand = '';
                }*/

                /*Instead of above code, this code has been updated.*/
                $rec = $this->common_functions->check_record_exists('cal2', array('eventno' => $eventno));
                if ($rec > 0) {
                    $lastAttyRecord = $this->common_functions->editRecord('cal2', array('initials' => $atty_hand), $where);
                } else {
                    $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventno, 'initials' => $atty_hand));
                }

                $cal_array['atty_hand'] = $atty_hand;
                $cal_array['attyass'] = $attyass;
                $cal_array['caltime'] = $calTime;

                $result = array('status' => 1, 'eventno' => $eventno, 'calendarData' => $cal_array, 'message' => 'Calendar Event  Updated Successfully');
                //$result = array('status' => 0, 'message' => 'Event Already Exists For a Day');
            }else if(count($rec) > 0 && $tododata == 1){
                $result = array('status' => 0, 'message' => 'Event Already Exists For a Day');
            }
        } else {
            $result = array('status' => 0, 'message' => 'Please Fill Required Fields');
        }

        echo json_encode($result);
    }

    public function editCalendarDataD() {

        extract($_POST);
        //print_r($first);exit;
        $result = array();
        if ($caldate != '' && $caltime != '' && $event != '') {

            //$first != '' && $last != '' && $caldate != '' && $caltime != '' && $event != ''
            //echo $caltime;exit;
            $date = date('Y-m-d', strtotime($caldate)) . " " . date('H:i:s', strtotime($caltime));
            $calTime = date('h:i A', strtotime($caltime));
            $protected = (isset($check_protected) && $check_protected == 1) ? $check_protected : 0;
            $rollover = (isset($rollover) && $rollover == 1) ? $rollover : 0;
            $attyass = (isset($attyass) && $attyass != '') ? $attyass : '';
            $atty_hand = (isset($atty_hand) && $atty_hand != '') ? $atty_hand : '';

            if ($tickledate != '') {
                $tickleDate = date('Y-m-d', strtotime($tickledate)) . " " . date('H:i:s', strtotime($tickletime));
            } else {
                $tickleDate = '1899-12-30 00:00:00';
            }

            $chglast = date('Y-m-d H:i:s');
            $caseno = ($caseno != '') ? $caseno : 0;
            
            if($caseno != 0){
                $caseNoArray[] = $caseno;
                $record = $this->case_model->getCaseDetailsforSNS($caseNoArray);
                $email = $record->email;
                if (!empty($record->home)) {
                    $phoneno = $record->home;
                } elseif (!empty($record->business)) {
                    $phoneno = $record->business;
                } elseif (!empty($record->fax)) {
                    $phoneno = $record->fax;
                }elseif (!empty($record->car)) {
                    $phoneno = $record->car;
                }
               
            }
            else{
                $email = '';
                $phoneno = '';
            }

            $condition = array('first' => $first, 'last' => $last, 'DATE(`date`)' => date('Y-m-d', strtotime($caldate)), 'event' => $event, 'eventno !=' => $eventno);


            $user_initials = $this->session->userdata('user_data')['initials'];

            $cal_array = array('eventno' => $eventno,
                'macroid' => 0,
                'calendar' => 1,
                /* 'calstat' => $calstat, */
                'judge' => $judge,
                'first' => $first,
                'last' => $last,
                'defendant' => $defendant,
//                    'attyass' => $attyass,
//                    'atty_hand'=>$atty_hand,
                'venue' => $venue,
                'location' => $location,
                'date' => $date,
                'tottime' => $totime,
                'event' => $event,
                'notes' => $notes,
                'tickle' => $tickle,
                'tickledate' => $tickleDate,
                'whofrom' => isset($whofrom) ? $whofrom : $this->session->userdata('user_data')['username'],
                'whoto' => isset($whoto) ? $whoto : $this->session->userdata('user_data')['username'],
                'todo' => isset($todo) ? intval($todo) : '',
                'color' => intval($color),
                'caseno' => $caseno,
                'chgwho' => $user_initials,
                'chglast' => $chglast,
                'protected' => intval($protected),
                'rollover' => intval($rollover),
                'reminder' => $calreminder,
                'doctor' => $doctor,
                'phone' => $phoneno,
                'email' => $email,
                'issync'=>0,
                'tevent'=>$tevent
            );

            if ($attyass != '') {
                $cal_array['attyass'] = $attyass;
            }

            $where = array('eventno' => $eventno);
            $lastRecord = $this->common_functions->editRecord('cal1', $cal_array, $where);

            if ($atty_hand != '') {
                $rec = $this->common_functions->check_record_exists('cal2', array('eventno' => $eventno));
                if ($rec > 0) {
                    $lastAttyRecord = $this->common_functions->editRecord('cal2', array('initials' => $atty_hand), $where);
                } else {
                    $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventno, 'initials' => $atty_hand));
                }
            }

            $cal_array['atty_hand'] = $atty_hand;
            $cal_array['attyass'] = $attyass;
            $cal_array['caltime'] = $calTime;

            $result = array('status' => 1, 'eventno' => $eventno, 'calendarData' => $cal_array, 'message' => 'Calendar Event  Updated Successfully');
        } else {
            $result = array('status' => 0, 'message' => 'Please Fill Required Fields');
        }

        echo json_encode($result);
    }

    public function removeEvent() {
        extract($_POST);
        $cond = array('eventno' => $eventno);
        $resp2 = $this->common_functions->getEventCaseNo($eventno);
        $resp = $this->common_functions->deleteRec('cal1', $cond);
        $resp1 = $this->common_functions->deleteRec('cal2', $cond);
        if ($resp2 != 'N/A') {
            $eventDate = $this->common_functions->getEventDate($resp2);
        } else {
            $eventDate = 'N/A';
        }


        //echo 'NextEvent => ' . $eventDate; exit;
        $res = array();
        if ($resp == 1) {
            $res = array('status' => 1, 'message' => 'Event Removed Successfully', 'eventDate' => $eventDate);
        } else {
            $res = array('status' => 0, 'message' => 'Event Not Removed', 'eventDate' => $eventDate);
        }

        echo json_encode($res);
    }

    public function addCalendarCloneData() {
        extract($_POST);

        $date1 = explode(',', $startend1);
        // echo "<pre>"; print_r($_POST);exit;
         // print_r($date1); exit;
        //  $event=$event_clone;
        if(count($date1) == 1){
            $date1[] = $startend1; 
         }
        // print_r($date1); exit;
        $result = array();
        $result1 = array();
        if ($addToday == 0)
            $i = 1;
        else if ($addToday == 1)
            $i = 0;
        for ($i; $i < count($date1); $i++) {
            //echo count($date1); exit;
            
            $caldate1 = $date1[$i];
            //echo  $caldate1;
            //echo  $caldate1;
            //if ($first != '' && $last != '' && $caldate1 != '' && $event != '') {
            if ($caldate1 != '' && $event != '') {

                $date = date('Y-m-d', strtotime($caldate1)) . " " . date('H:i:s', strtotime($caltime));
                //$date = date('Y-m-d', strtotime($caldate1)) . " 00:00:00";

                $protected = (isset($check_protected) && $check_protected == 1) ? $check_protected : 0;
                $rollover = (isset($rollover) && $rollover == 1) ? $rollover : 0;

                if ($tickledate != '') {
                    $tickleDate = date('Y-m-d', strtotime($tickledate)) . " " . date('H:i:s', strtotime($tickletime));
                } else {
                    $tickleDate = '1899-12-30 00:00:00';
                }

                $chglast = date('Y-m-d H:i:s');
                $caseno = ($caseno != '') ? $caseno : 0;

                $condition = array('first' => $first, 'last' => $last, 'DATE(`date`)' => date('Y-m-d', strtotime($caldate1)), 'event' => $event);
                $rec = $this->common_functions->check_record_exists('cal1', $condition);

                //if ($rec >= 1) {
                $user_initials = $this->session->userdata('user_data')['initials'];

                $eventId = $this->common_functions->last_record('cal1', 'eventno');
                if ($addNotes == 0) {
                    $notes = '';
                }

                $cal_array = array('eventno' => $eventId + 1,
                    'macroid' => 0,
                    'calendar' => 1,
                    /* 'calstat' => $calstat, */
                    'judge' => $judge,
                    'first' => $first,
                    'last' => $last,
                    'defendant' => $defendant,
                    'attyass' => $attyass,
                    'venue' => $venue,
                    'location' => $location,
                    'date' => $date,
                    'tottime' => $totime,
                    'event' => $event,
                    'notes' => $notes,
                    'tickle' => $tickle,
                    'tickledate' => $tickleDate,
                    'whofrom' => isset($whofrom) ? $whofrom : $this->session->userdata('user_data')['username'],
                    'whoto' => isset($whoto) ? $whoto : $this->session->userdata('user_data')['username'],
                    'todo' => isset($todo) ? intval($todo) : '',
                    'color' => intval($color),
                    'caseno' => $caseno,
                    'initials0' => $user_initials,
                    'initials' => $user_initials,
                    'chgwho' => $user_initials,
                    'chglast' => $chglast,
                    'protected' => intval($protected),
                    'rollover' => intval($rollover),
                );
                // print_R($cal_array);exit;
                $lastRecord = $this->common_functions->insertRecord('cal1', $cal_array);

                if ($atty_hand != '') {
                    $lastAttyRecord = $this->common_functions->insertRecord('cal2', array('eventno' => $eventId + 1, 'initials' => $atty_hand));
                }
                $cal_array['atty_hand'] = $atty_hand;
                $cal_array['attyass'] = $attyass;
                $result_new = array();
                $result = array('status' => 1, 'eventno' => $eventId + 1, 'calendarData' => $cal_array, 'message' => 'Calendar Event Added Successfully');
                $result_new = array('status' => 1, 'eventno' => $eventId + 1, 'calendarData' => $cal_array, 'message' => 'Calendar Event Added Successfully');

                $result1[] = $result_new;
                /* } else {
                  $result1 = array('status' => 0, 'message' => 'Event Already Exists For a Day');
                  } */
            } else {
                $result1 = array('status' => 0, 'message' => 'Please Fill Required Fields');
            }
        } //exit;
        // print_R($result1); exit;
        echo json_encode($result1);
    }

    public function validateDate($date, $format = 'Y-m-d') {
        $d = date($format, strtotime($date));
        return $d;
    }

    public function getEventsDatatable() {
        extract($_POST);
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'c1.date',
            1 => 'c2.initials',
            2 => 'attyass',
            3 => 'first',
            4 => 'event',
            5 => 'venue',
            6 => 'judge',
            7 => 'notes'
        );

        /*If else condition is for Calendar Export Data AJAX. Otherwise, it would have only $order and $dir assignment lines.*/
        if(isset($this->input->post('order')[0]['column']) && $this->input->post('order')[0]['column'] != '') {
            $order = $columns[$this->input->post('order')[0]['column']];
        } else {
            $order = 'c1.date';
        }
        if(isset($this->input->post('order')[0]['dir']) && $this->input->post('order')[0]['dir'] != '') {
            $dir = $this->input->post('order')[0]['dir'];
        } else {
            $dir = 'desc';
        }
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') {
            $dt = $this->validateDate($search, $format = 'm-d-Y');
            //echo $dt; exit;
            $singleFilter = '(date LIKE "%' . $dt . '%" OR c2.initials LIKE "%' . $search . '%" OR attyass LIKE "%' . $search . '" OR first LIKE "%' . $search . '" OR event LIKE "%' . $search . '" OR venue LIKE "%' . $search . '" OR judge LIKE "%' . $search . '" OR notes LIKE "%' . $search . '")';
        }

        if (!empty($caseno)) {
            $filterArray['c1.caseno'] = $caseno;
        }
//        $filterArray['c1.todo'] = $caltyp;

        if (!empty($attya)) {
            $filterArray['c1.attyass'] = $attya;
        }
        if (!empty($attyh)) {
            $filterArray['c2.initials'] = $attyh;
        }

        $color_codes = $this->config->item('color_codes');
        $this->data['color_codes'] = json_decode(color_codes, true);
        $json_color = $this->data['color_codes'];

        $rec = $this->calendar_model->getAjaxCalendarListing($caseno, $caltyp, $start, $length, $filterArray, $order, $dir, $singleFilter);
        $recCount = $this->calendar_model->getAjaxCalendarListingCount($caseno, $caltyp, $filterArray, $singleFilter);

        $finalData = array();
        $i = 1;
        foreach ($rec as $data) {
            $date = ($data->date != '' && $data->date != '1899-12-30 00:00:00' && $data->date != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($data->date)) : '';
            $links = '<a class="btn btn-danger btn-circle-action btn-circle cal-delete" title="Delete"><i class="fa fa-trash closeon" id="' . $data->eventno . '"></i></a>';
            if ($data->caseno != '' && $data->caseno != '0') {
                $links .= '<a class="btn btn-default btn-circle-action btn-circle cal-pullcase" data-caseno="' . $data->caseno . '" title="Pull Case"><i class="fa fa-folder-open calender-pullcase" data-caseno="' . $data->caseno . '" style="margin: 6px 6px; color: #4D7AB1;"></i></a>';
            }

            if (array_key_exists($data->color, $json_color)) {
                if ($json_color[$data->color][2] == '#FFFFFF') {
                    $textColor = $json_color[$data->color][1];
                    $color = '#f5f5f5';
                } else {
                    $textColor = $json_color[$data->color][1];
                    $color = $json_color[$data->color][2];
                }
            } else {
                $textColor = '#FFFFFF';
                $color = '#4d7ab1';
            }

            $tmpArr = array(
                date("m/d/Y", strtotime($data->date)) . "<br>" . date("h:i A", strtotime($data->date)), //0
                $data->intl, //1
                $data->attyass, //2
                $data->first . " " . $data->last . " v. " . $data->defendant, //3
                $data->event, //4
                $data->venue, //5
                $data->judge, //6
                $data->notes, //7
                $links, //8
                $data->eventno, //9
                $data->caseno, //10
                $color, //11
                $textColor //12
            );

            array_push($finalData, $tmpArr);
            $i++;
        }

        $totalData = $this->calendar_model->getAjaxCalendarListingCount($caseno, $caltyp);
        $listingData = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($recCount),
            "data" => $finalData,
        );

        echo json_encode($listingData);
    }

    /* Function to check and get defendant value*/
    public function checkDefendant() {
        $defendant_value = array();
        $result = $this->calendar_model->checkDefendant();
        $split_result = explode(',', $result);
        foreach ($split_result as $key => $value) {
            if($value == 'EMPLOYER') {
                $defendant_value['emp'] = 'EMPLOYER';
            } elseif($value == 'DEFENDANT') {
                $defendant_value['def'] = 'DEFENDANT';
            }
        }
        return $defendant_value;
    }

    public function getEventsForExport() {
        extract($_POST);
        $rec = $this->calendar_model->getCalendarEventForExport($caseno, $caltyp);
        echo json_encode($rec);
    }

    /*cURL call to node script*/
    public function callExportScript() {
        $payload = json_encode( array( "data"=> $_POST['res'] ) );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,'http://localhost:3000/createExportFile');
        /*curl_setopt($curl, CURLOPT_POST, 1);*/
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $resp = curl_exec($curl);
        curl_close($curl);
        echo "success";
    }

}
