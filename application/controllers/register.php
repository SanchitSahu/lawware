<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('login_model');
       $this->data['pageTitle'] = APPLICATION_NAME.' | Register';
    }

    public function adduser() {
        //extract($_POST);
       // echo 'in function'; exit();
        $chkExist = array('initials' => $_POST['initial']);
        $Chk = $this->login_model->checkExist($chkExist);
        if(count($Chk) > 0)
        {
            $this->session->set_flashdata('message', 'This user is already Exist. !!!');
        }
        else
        {
            $new_reg_lawware = $this->login_model->addUser($_POST,$_FILES);

            if ($new_reg_lawware == true){
                $newUserSNS = array(
                "firstname" => $_POST['fname'],
                "lastname" => $_POST['lname'],
                "snsusersunqid" => $_POST['initial'],
                "username" => $_POST['uname'],
                "password" => $_POST['pass'],
                "title"=>$_POST['designation'],
                "email" => $_POST['email'],
                );
                $urlsns = 'http://52.66.93.186/sns/api/user/register';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $urlsns);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($newUserSNS));
                $response = curl_exec($ch);
                $this->session->set_flashdata('message', 'You are registered successfully. Please check your email for activation link !!!');
            }

        }



        redirect('login');
    }




}
