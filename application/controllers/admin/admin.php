<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_login_model');
        //$this->login();
        $this->load->helper(array('url', 'html', 'form'));
        $this->load->library('session');
        $this->common_functions->checkAdminLogin();
        $this->load->model('email_model');
        $this->load->model('calendar_model');
        $this->load->model('common_model');
        $this->load->model('case_model');
        // $this->load->model('login_model');

        $data = array();
        $this->data['username'] = '';
        $this->data['pageTitle'] = 'Administrator';
    }

    public function index() {
        //$this->data['loginUser'] = $this->login_model->getLoginUsername();
        $this->load->view('admin/admin_login_view', $this->data);
    }

    public function login() {
        $this->data['pageTitle'] = 'Administrator';
        $username = $this->input->post('admin_username');
        $password = $this->input->post('admin_password');
        $remember = $this->input->post('admin_remember');
        $username = isset($username) ? $username : '';
        $password = isset($password) ? $password : '';
        $remember = isset($remember) ? $remember : 'off';

        if ($username != '' && $password != '') {

            $rec = $this->admin_login_model->resolve_admin_user_login($username, $password);
            //echo 'hello => ' . $rec; exit;
            if (count($rec) > 0) {
                $this->session->set_userdata('admin_user_data', $rec);
                $this->session->set_flashdata('message', 'You have successfully logged in!');
                if ($remember == 'on') {
                    $this->input->set_cookie('login_admin_user', $username, time() + 60 * 60 * 24 * 100, "/");
                    $this->input->set_cookie('login_admin_password', $password, time() + 60 * 60 * 24 * 100, "/");
                } else {
                    setcookie("login_admin_user", $username, time() - 60 * 60 * 24 * 100, "/");
                    setcookie("login_admin_password", $password, time() - 60 * 60 * 24 * 100, "/");
                }
                // Admin user login success
                /* $this->load->view('admin/header');
                  $this->load->view('admin/dashboard', $this->data);
                  $this->load->view('admin/footer'); */
                redirect('admin/dashboard');
            } else {
                // login failed
                $this->session->set_flashdata('message', 'Wrong username or password.');
                $this->load->view('admin/admin_login_view', $this->data);
            }
        } else {

            // login failed
            $this->session->unset_userdata('message');
            $this->session->set_flashdata('message', 'username and password required.');
            $this->load->view('admin/admin_login_view', $this->data);
        }
    }

    public function forcelogout() {
        $this->common_functions->checkAdminSession();
        $this->data['pageTitle'] = 'Force Users Off '.APPLICATION_NAME;

        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/forceuseroff');
        $this->load->view('admin/common/footer');
    }

    public function forceuseroff() {

        $userinitials = $_POST['userinitials'];
        $minutes = $_POST['minutes'] * 60;

        $userlist = explode(",", $userinitials);

        if ($userinitials == '*' && $minutes == '0') {
            $data = array('forceoff' => '10');
            $this->db->update('staff', $data);

            /*  $data_second = array('loggedin_id' => NULL);
              $this->db->update('staff', $data_second); */
        }
        if ($userinitials == '*' && $minutes != '0') {
            $data = array('forceoff' => $minutes);
            $this->db->update('staff', $data);

            /* $data_second = array('loggedin_id' => NULL);
              $this->db->update('staff', $data_second); */
        }
        if ($userinitials != '*' && $minutes == '0') {
            $data = array('forceoff' => '10');
            //$data_second = array('loggedin_id' => NULL);

            foreach ($userlist as $setuserlist) {
                $this->db->where('initials', $setuserlist);
                $this->db->update('staff', $data);

                /*  $this->db->where('initials', $setuserlist);
                  $this->db->update('staff', $data_second); */
            }
        }
        if ($userinitials != '*' && $minutes != '0') {
            $data = array('forceoff' => $minutes);
            //$data_second = array('loggedin_id' => NULL);

            foreach ($userlist as $setuserlist) {
                $this->db->where('initials', $setuserlist);
                $this->db->update('staff', $data);

                /* $this->db->where('initials', $setuserlist);
                  $this->db->update('staff', $data_second); */
            }
        }
    }

    public function logout() {

        $this->session->userdata('admin_user_data')['sa_user_name'];
        $this->session->userdata('admin_user_data')['sa_password'];
       // $this->session->sess_destroy();
        $this->session->unset_userdata('admin_user_data');
        //echo '<pre>'; print_r($this->session->userdata); exit;
        $this->input->set_cookie('login_admin_user', '', time() - 3600);
        $this->input->set_cookie('login_admin_password', '', time() - 3600);
        redirect('admin/login');
    }

    public function timeclock() {
        $this->common_functions->checkAdminSession();
        $this->data['pageTitle'] = 'Timestamp Information';

        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/timeclock');
        $this->load->view('admin/common/footer');
    }

    public function timeclockdata() {
        extract($_POST);

        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            /* 0 => 'Location', */
            0 => 'initials',
            1 => 'Signon',
            2 => 'signoff',
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        if ($search['value'] != '') {
            $singleFilter = '(signon LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR signoff LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR initials LIKE "%' . $search['value'] . '%" OR location LIKE "%' . $search['value'] . '%")';
        }

        $rec = $this->admin_login_model->get_timeclock($length, $start, $singleFilter, $order, $dir);

        foreach ($rec as $data) {

            $tmpArr = array(
                /* $data->location, */
                $data->initials,
                $data->signon,
                $data->signoff,
            );

            array_push($finalData, $tmpArr);
            $i++;
        }
        $totalData = $this->admin_login_model->get_all_loginfo_count($singleFilter);

        $listingData = array(
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $finalData
        );

        echo json_encode($listingData);
    }

    public function dashboard() {

        //echo '<pre>'; print_r($this->session->userdata('admin_user_data')); exit;
        $this->common_functions->checkAdminSession();
        $this->data['pageTitle'] = 'Dashboard';
        //$this->data['received_emails'] = $this->email_model->recent_email_for_admin();
        $this->data['latest_tasks'] = $this->common_functions->getlatestTaskadmin();
        $this->data['calendar_event'] = $this->calendar_model->getrecenteventadmin();
        $this->data['user_active_cnt'] = $this->common_model->getcount('staff', '*', array('isActive' => '1'));
        $this->data['user_non_active_cnt'] = $this->common_model->getcount('staff', '*', array('isActive' => '0'));
        $this->data['case_active_cnt'] = $this->common_model->getcount('case', '*', array('case_status' => '1'));
        $this->data['case_del_active_cnt'] = $this->common_model->getcount('case', '*', array('case_status' => '0'));
        $this->data['email_cnt'] = $this->common_model->getcount('email2', 'filename', array('detached' => b'1'));
        $this->data['caseact_cnt'] = $this->common_model->getcount('caseact', 'id');
        $this->data['pnc_cnt'] = $this->common_model->getcount('intake', 'prospect_id', "intake.status IS NULL OR intake.status ='0'");
        $this->data['total_task_cnt'] = $this->common_model->getcount('tasks', '*');
        $this->data['completed_task_cnt'] = $this->common_model->getcount('tasks', '*', '(completed !="1899-12-30 00:00:00" AND completed !="0000-00-00 00:00:00")');
        $this->data['card_cnt'] = $this->common_model->getcount('card', '*');

        //echo '<pre>'; print_r($this->data); exit;

        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/dashboard');
        $this->load->view('admin/common/footer');
        //$this->load->view('admin/chartscript.php');
    }

    public function forgotpass() {
        $email = $this->input->post('email');
        $exists = $this->admin_login_model->check_email($email);
        $count = count($exists);
        if (!empty($count)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function resetpass() {
        $this->data['pageTitle'] = 'ResetPassword';
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/resetpass');
        $this->load->view('admin/common/footer');
    }

    public function resetyourpass() {
        extract($_POST);
        $password = isset($password) ? $password : '';
        $fpasslink = isset($fpasslink) ? $fpasslink : '';
    }

    public function passchange() {
        $this->data['pageTitle'] = APPLICATION_NAME.' | Change Password';
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/changepass');
        $this->load->view('admin/footer');
    }

    public function changepass() {
        $pass = $this->input->post('pass');
        $fstr = $this->input->post('fstr');

        $exists = $this->admin_login_model->check_fstr($fstr);
        //echo $this->db->last_query(); exit;
        $count = count($exists);
        if (!empty($count)) {
            $this->admin_login_model->change_pass($fstr, $pass);

            $this->session->set_flashdata('message', 'Your Password is Changed Successfully.');
            echo '1';
        } else {
            echo 0;
        }
    }

    public function resetpassword() {
        $pass = $this->input->post('pass');
        $oldpass = $this->input->post('oldpass');
        $initial = $this->session->userdata('admin_user_data')['sa_user_name'];
        $where = array('sa_password' => md5($oldpass), 'sa_user_name' => $initial);
        $exists = $this->admin_login_model->check_pass($where);
        //echo $this->db->last_query(); exit;
        $count = count($exists);
        if (!empty($count)) {
            $this->admin_login_model->change_password($pass);
            $this->session->set_flashdata('message', 'Your Password is Changed Successfully.');
            echo '1';
        } else {
            echo '0';
        }
    }

    public function firm() {
        //echo "hi";exit;
        $this->common_functions->checkAdminSession();
        $this->data['pageTitle'] = 'Firm';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        // echo '<pre>'; print_r($this->data); exit;;
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/firm');
        $this->load->view('admin/common/footer');
    }

    public function groupmanagement() {

        $this->common_functions->checkAdminSession();
        $this->data['pageTitle'] = 'Group Management';
        $this->data['groupname'] = $this->admin_login_model->get_groupname();
//        $this->data['initialname_old'] = $this->admin_login_model->get_initialname();
        $this->data['initialname'] = json_encode($this->admin_login_model->get_initialname());
//        echo "<pre>";
//        echo json_encode($this->data['initialname']);print_r($this->data['initialname']);exit;

        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/groupmanagement');
        $this->load->view('admin/common/footer');
    }

    public function groupmembermanagement() {

        $groupname = $_POST['groupname'];
        $groupnamemember = $this->data['groupname'] = $this->admin_login_model->get_groupmemberByid($groupname);
        $groupnamedisplay = $this->data['groupnamedisplay'] = $this->admin_login_model->get_groupnamebyID($groupname);
        
        $i = 1;

        if ($groupnamemember) {
            $data = "";
            foreach ($groupnamemember as $groupmemberlist) {
                $data .= '<tr class="">
                    <td class="mail-contact"><input class="checkbox1" type="checkbox" data-groupid="' . $groupmemberlist->group_id . '" data-id="' . $groupmemberlist->member_id . '"></td>
                    <td class="mail-contact">' . $i . '</td>
                    <td class="mail-subject">' . $groupnamedisplay[0]->group_name . '</td>                    
                    <td class="mail-subject">' . $groupmemberlist->initials . '</td>
                    <td><a href="javascript:void(0)" class="marg-left5 btn btn-danger btn-circle-action btn-circle group_member_delete" data-groupmemberid="' . $groupmemberlist->member_id . '" data-groupid="' . $groupmemberlist->group_id . '"><i class="fa fa-trash"></i></a></td>
                </tr>';
                $i++;
            }
            $response = array(
                "data" => $data,
                "status" => true
            );
            die(json_encode($response));
        } else {
            $data = '<tr class="">
                <td class="mail-contact" align="center" colspan="4">No Record Found</td>
            </tr>';
            $response = array(
                "data" => $data,
                "status" => false
            );
            die(json_encode($response));
        }
    }

    public function deletegroup() {
        $groupid = $_POST['groupid'];
        $deletegroupid = $this->data['deletegroupid'] = $this->admin_login_model->deletegroupname($groupid);
    }

    public function deletegroupmember() {
        $groupmemberdelete = $_POST['groupmemberid'];
        $deletegroupmemberid = $this->data['deletegroupmemberid'] = $this->admin_login_model->deletegroupmembername($groupmemberdelete);
    }

    public function deletemultiplegroupmember() {

        $ids = $this->input->post('ids');

        $this->db->where_in('member_id', explode(",", $ids));
        $this->db->delete('group_member');

        echo json_encode(['success' => "Item Deleted successfully."]);
    }

    public function general() {
        $this->common_functions->checkAdminSession();
        $this->data['pageTitle'] = 'General';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/general');
        $this->load->view('admin/common/footer');
    }

    public function updatefirm() {

        $firm = $this->admin_login_model->get_firm_details();
        $main_array = $firm['main1'];
        $main_array1 = json_decode($main_array, true);
        //print_r($main_array1);exit;
        $formdata = $_POST;
        //print_r($formdata);exit;
        $system = array();
        $sudf = array();
        $YOURFILEN1 = array();
        $gen2 = array();
        $arr1 = array();

        $Xnote = array();
        $Datealertd = array();
        $Birthdays = array();
        $Dropdown = array();
        $Injuryd = array();
        $casedates = array();

        foreach ($formdata as $key => $val) {


            $keyVar = explode("_", $key);
            // print_R($keyVar);exit;
//                if (count($keyVar) > 2) {
//                    $keyVar[1] = $keyVar[1] . "_" . $keyVar[2];
//                }
            if ($keyVar[0] == 'system') {
                // if($keyVar[1]=='cdfolupcal'){ print_r($system[$keyVar[1]] = $val);exit; }
                if ($keyVar[1] == 'main') {
                    //echo "hi"; exit;
                    $arr1[$keyVar[2]] = $val;
                    $system[$keyVar[2]] = $val;
                }
                if ($keyVar[1] == 'SUDF') {
                    //print_r($keyVar[2]);exit;
                    $sudf[$keyVar[2]] = $val;
                    $system[$keyVar[2]] = $val;
                }
                if ($keyVar[1] == 'GEN2') {
                    //print_r($keyVar[2]);exit;
                    $gen2[$keyVar[2]] = $val;
                    $system[$keyVar[2]] = $val;
                }

                if ($keyVar[1] == 'YOURFILEN1') {
                    //print_r($keyVar[2]);exit;
                    $YOURFILEN1[$keyVar[2]] = $val;
                    $system[$keyVar[2]] = $val;
                }

                if ($keyVar[1] == 'xnote') {
                    //print_r($keyVar[2]);exit;
                    $Xnote[$keyVar[2]] = $val;
                    //  $system[$keyVar[2]]=$val;
                }
                if ($keyVar[1] == 'datealertd') {
                    //print_r($keyVar[2]);exit;
                    $Datealertd[$keyVar[2]] = $val;
                    //  $system[$keyVar[2]]=$val;
                }

                if ($keyVar[1] == 'birthdays') {
                    //print_r($keyVar[2]);exit;
                    $Birthdays[$keyVar[2]] = $val;
                    //  $system[$keyVar[2]]=$val;
                }


                if ($keyVar[1] == 'pncdd') {
                    //print_r($keyVar[2]);exit;
                    $Dropdown[$keyVar[2]] = $val;
                    //  $system[$keyVar[2]]=$val;
                }



                if ($keyVar[1] == 'injuryd') {

                    //print_r($keyVar[2]);exit;
                    $Injuryd[$keyVar[2]] = $val;
                    //$system[$keyVar[2]]=$val;
                }

                if ($keyVar[1] == 'casedates') {
                    $casedates[$keyVar[2]] = $val;
                }


                $system[$keyVar[1]] = $val;
            }
        }
        //print_r($casedates);exit;
        if (!empty($main_array1)) {

            $result = array_diff_assoc($arr1, $main_array1);
            $finaal = array_merge($main_array1, $arr1);
            $main1 = json_encode($finaal);
        } else {
            $main1 = json_encode($arr1);
        }
        unset($system['main']);
        //print_r($system);exit;
        $pos = array_key_exists('country', $system);
        if ($pos !== FALSE) {
            unset($system['EAMSRefNo']);
            unset($system['firmTextid']);
            unset($system['country']);
            unset($system['main']);
            //unset($system['userlog']);
            //print_r($main1);
            $system['main1'] = $main1;
        }
        $pos = array_key_exists('userlog', $system);
        if ($pos !== FALSE) {
            unset($system['userlog']);
            unset($system['rindexre']);
            unset($system['main']);
            unset($system['check']);
            unset($system['SUDF']);
            unset($system['YOURFILEN1']);
            unset($system['UDF1']);
            unset($system['UDF1n']);
            unset($system['UDF2']);
            unset($system['UDF2n']);
            unset($system['UDF3']);
            unset($system['UDF3n']);
            unset($system['UDF4']);
            unset($system['UDF4n']);
            unset($system['msg1']);
            unset($system['msg2']);
            unset($system['msg3']);
            unset($system['msg1h']);
            unset($system['msg2h']);
            unset($system['msg3h']);
            unset($system['msg1c']);
            unset($system['msg2c']);
            unset($system['msg1c']);
            unset($system['msg2c']);
            unset($system['msg3c']);
            unset($system['cardoptional']);
            unset($system['cardlabel']);
            unset($system['pwdp']);
            unset($system['nudc']);
            unset($system['rosm']);
            unset($system['ddti']);
            unset($system['scpp']);
            unset($system['esiw']);
            unset($system['snif']);
            unset($system['scdo']);
            unset($system['font']);
            unset($system['Captionstyle']);
            unset($system['Pickevery']);
            unset($system['scdo']);
            unset($system['pagecaseDate']);
            unset($system['QuickForm']);
            unset($system['clientcw']);
            //unset($system['cdfolupcal']);
            unset($system['aeir']);
            unset($system['usti']);
            unset($system['eaemails']);
            unset($system['srnmf']);
            unset($system['asdemails']);
            unset($system['shortpost']);
            unset($system['keepan']);
            unset($system['una1email']);
            unset($system['enadis']);
            unset($system['ctpt']);
            unset($system['dta1lesy']);
            unset($system['submac']);
            unset($system['enddel']);
            unset($system['stenboun']);
            unset($system['startdel']);
            unset($system['feecostinit']);
            unset($system['vieintst']);
            unset($system['feecostinit']);
            unset($system['gridstyle']);
            unset($system['watermark']);
            unset($system['wsize']);
            unset($system['dispcate']);
            unset($system['fileacccat']);
            unset($system['paidbutton']);
            unset($system['markevent']);
            unset($system['skipcd']);
            unset($system['asacaadd']);
            unset($system['keepodate']);
            unset($system['keepoddatefpaid']);
            unset($system['outlookbutton']);
            unset($system['altfile']);
            unset($system['outlookbutton']);
            unset($system['outlookbutton1']);
            unset($system['outlookbutton2']);
            //your file
            unset($system['enuni']);
            unset($system['enyofnl']);
            unset($system['diyfint']);
            unset($system['didaent']);
            //unset($system['']); didaop
            unset($system['didaop']);
            unset($system['ensecur']);
            unset($system['elispa']);
            unset($system['format']);
            unset($system['defausta']);
            unset($system['promp']);
            unset($system['pristatus']);
            unset($system['runprnewc']);
            unset($system['shyoufileno']);
            // unset($system['']);
            //unset($system['']);
            // unset($system['']);
            $system['main1'] = $main1;
            // print_R($main1);exit;
            $system['ccudf1'] = json_encode($sudf);
            $system['yourfilen1'] = json_encode($YOURFILEN1);
        }
        $pos = array_key_exists('attyhand', $system);
        if ($pos !== FALSE) {
            $system['atty_hand'] = $system['attyhand'];
            $system['para_hand'] = $system['parahand'];
            $system['atty_resp'] = $system['attyresp'];
            $system['sec_hand'] = $system['sechand'];
            $system['main1'] = $main1;
            unset($system['attyhand']);
            unset($system['parahand']);
            unset($system['attyresp']);
            unset($system['sechand']);
            unset($system['necaactcatde']);
        }

        $pos = array_key_exists('backup', $system);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;
            unset($system['backup']);
            unset($system['blockIM']);
            unset($system['logoff']);
            unset($system['awnml']);
            unset($system['vcms']);
            unset($system['rpcmp']);
            unset($system['instance']);
            unset($system['piu']);
            unset($system['prompt']);
            unset($system['vcms']);
        }
        $pos = array_key_exists('side', $system);
        if ($pos !== FALSE) {

            $system['main1'] = $main1;
            unset($system['side']);
            unset($system['pstdown']);
            unset($system['othercasdrop']);
            unset($system['otherCases']);
            unset($system['cafeedrop']);
        }
        $pos = array_key_exists('ccofcc', $system);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;
            unset($system['ccofcc']);
            unset($system['odccbc']);
        }
        $pos = array_key_exists('dpartytuse', $arr1);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;
            //unset($system['wiaodays']);
            unset($system['udfo']);
            unset($system['udfv']);
            unset($system['utocfs']);
            unset($system['uaaafcase']);
            unset($system['GEN2']);
            unset($system['udifa']);
            unset($system['udifaa']);
            unset($system['confAtt']);
            unset($system['confAttA']);
            unset($system['ocwsave']);
            unset($system['fptcaseact']);
            unset($system['ieflastch']);
            unset($system['ust2ediscr']);
            unset($system['ucinre']);
            unset($system['creaufile']);
            unset($system['dpartytuse']);
            unset($system['tranforcon']);
            unset($system['yeartud']);
            unset($system['caseactcatpt']);
            unset($system['defsta']);
            unset($system['repcap']);
            unset($system['judge']);
            unset($system['venue']);
            unset($system['event']);
            unset($system['status']);
            /* Commenting given below lines as they are commented from Admin UI as well.
            unset($system['atthcal']);
            unset($system['attrcal']);*/
            unset($system['useattpoincalv']);
            unset($system['useectcal']);
            unset($system['extcalfolname']);
            unset($system['foexcalinlocalcat']);
            unset($system['builpass']);
            unset($system['insurcarrier']);
            unset($system['courtreporter']);
            unset($system['interpreter']);
            unset($system['carriercode']);
            unset($system['calsleeplable']);
            $system['caldeflt'] = json_encode($gen2);
        }
        //print_r($gen2);exit;
        $pos = array_key_exists('openwith', $system);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;
            unset($system['xnote']);
            unset($system['openwith']);


            $system['xnotes'] = json_encode($Xnote);
        }
        $pos = array_key_exists('birthdays', $system);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;

            unset($system['birthdays']);
            unset($system['datealertd']);
            unset($system['bialaddcalevent']);
            $content1 = nl2br(trim($system["holidays"]));
            //echo  $content1; exit;
            $content = str_replace("\n", "##", $system["holidays"]);
            unset($system["holidays"]);
            $system["holidays"] = $content;
            $system['birthday'] = json_encode($Birthdays);
            $system['datealert'] = json_encode($Datealertd);
        }


        //print_r($Dropdown);exit;
        $pos = array_key_exists('toc', $Dropdown);
        //echo 'hii-'.$pos; exit;
        if ($pos !== FALSE) {
            $system['main1'] = $main1;

            unset($system['toc']);
            unset($system['status']);
            unset($system['refc']);
            unset($system['refsrc']);
            unset($system['refby']);
            unset($system['assoc']);
            unset($system['refto']);
            unset($system['ors']);
            unset($system['t1']);
            unset($system['t2']);
            unset($system['t3']);
            unset($system['t4']);
            unset($system['t5']);
            unset($system['t6']);
            unset($system['d1']);
            unset($system['d2']);
            unset($system['d3']);
            unset($system['d4']);
            unset($system['d5']);
            unset($system['d6']);
            unset($system['d7']);
            unset($system['d8']);
            $system['pncdd'] = json_encode($Dropdown);
        }

        $pos = array_key_exists('cacn', $system);
        if ($pos !== FALSE) {
            $system['cacn'] = $system['cacn'];
            $system['dflc'] = $system['dflc'];
            $system['dflc'] = $system['wen'];
            $system['dflc'] = $system['dc'];
            $system['saa'] = $system['saa'];
            $system['main1'] = $main1;
            unset($system['cacn']);
            unset($system['dflc']);
            unset($system['wen']);
            unset($system['dc']);
            unset($system['saa']);
        }

        //exit;
        //print_r($system);exit;
        // $where = array('system_id' => 1);

        $pos = array_key_exists('tdind', $Injuryd);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;

            unset($system['almanuorofin']);
            unset($system['usexIoprintedapp']);
            unset($system['autopostcat']);
            unset($system['applicantattorney']);
            unset($system['injurystatus']);
            unset($system['injuryd']);
            //  unset($system['bialaddcalevent']);
            $system['injury9'] = json_encode($Injuryd);
        }

        $pos = array_key_exists('def', $casedates);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;
            unset($system['refdt']);
            unset($system['ref']);
            unset($system['pns']);
            unset($system['pnsdt']);
            //  unset($system['bialaddcalevent']);
            $system['casedates'] = json_encode($casedates);
        }

        $pos = array_key_exists('tdind', $Injuryd);
        if ($pos !== FALSE) {
            $system['main1'] = $main1;

            unset($system['almanuorofin']);
            unset($system['usexIoprintedapp']);
            unset($system['autopostcat']);
            unset($system['applicantattorney']);
            unset($system['injurystatus']);
            unset($system['injuryd']);
            //  unset($system['bialaddcalevent']);
            $system['injury9'] = json_encode($Injuryd);
        }
        $system['keeptaskco'] = $_POST['keeptaskco'];
        //exit;
        $where = array('system_id' => 1);

        $this->common_functions->editRecord('system', $system, $where);

        $response = array('status' => 1, 'message' => 'Record Edited Successfully !!!');

        echo json_encode($response);

        //  $this->data['firm'] = $this->admin_login_model->get_firm_details();
    }

    public function drives() {
        $this->data['pageTitle'] = 'Drives';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/drives');
        $this->load->view('admin/common/footer');
    }

    public function newcases() {

        $this->data['caseTypelist'] = $this->admin_login_model->getcasetypelist();
        $this->data['caseStatuslist'] = $this->admin_login_model->getcasestatusist();
        $this->data['Stafflist'] = $this->admin_login_model->getstafflist();
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->data['pageTitle'] = 'New Case';
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/newcases');
        $this->load->view('admin/common/footer');
    }

    public function security() {
        $this->data['pageTitle'] = 'Security';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/security');
        $this->load->view('admin/common/footer');
    }

    public function adminfunction() {
        $this->data['pageTitle'] = 'Function';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/function');
        $this->load->view('admin/common/footer');
    }

    public function image() {
        $this->data['pageTitle'] = 'Image';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/image');
        $this->load->view('admin/common/footer');
    }

    public function popup() {
        $this->data['Stafflist'] = $this->admin_login_model->getstafflist();

        $this->data['pageTitle'] = 'Popup';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();

        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/popup');
        $this->load->view('admin/common/footer');
    }

    public function caseactivity() {
        $this->data['pageTitle'] = 'Case Activity';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/caseactivity');
        $this->load->view('admin/common/footer');
    }

    public function colors() {
        $this->data['pageTitle'] = 'Color';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/colors');
        $this->load->view('admin/common/footer');
    }

    public function calendar() {
        $this->data['pageTitle'] = 'Calender';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/calendar');
        $this->load->view('admin/common/footer');
    }

    public function screen() {
        $this->data['pageTitle'] = 'Screen';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->data['userdefined'] = $this->admin_login_model->get_userdefined_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/screen');
        $this->load->view('admin/common/footer');
    }

    public function clientcard() {
        $this->data['pageTitle'] = 'ClientCard';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/clientcard');
        $this->load->view('admin/common/footer');
    }

    public function program() {
        $this->data['pageTitle'] = 'Program';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/program');
        $this->load->view('admin/common/footer');
    }

    public function forms() {
        $this->data['pageTitle'] = 'Forms';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/forms');
        $this->load->view('admin/common/footer');
    }

    public function injuries() {
        $this->data['pageTitle'] = 'Injuries';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        // print_R( $this->data['firm']['injury9']);exit;
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/injuries');
        $this->load->view('admin/common/footer');
    }

    public function holidays() {
        $this->data['pageTitle'] = 'Holidays';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/holidays');
        $this->load->view('admin/common/footer');
    }

    public function tasksmacro() {
        $this->data['pageTitle'] = 'TasksMacro';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/tasksmacro');
        $this->load->view('admin/common/footer');
    }

    public function rolodexudf() {
        $this->data['pageTitle'] = 'Rolodex UDF';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/rolodexudf');
        $this->load->view('admin/common/footer');
    }

    public function clientcardudf() {
        $this->data['pageTitle'] = 'ClientCard UDF';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/clientcardudf');
        $this->load->view('admin/common/footer');
    }

    public function injuryudf() {
        $this->data['pageTitle'] = 'Injury UDF';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/injuryudf');
        $this->load->view('admin/common/footer');
    }

    public function injuryudfview() {
        $this->data['pageTitle'] = 'Injury UDF View';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/injuryudfview');
        $this->load->view('admin/common/footer');
    }

    public function prospects() {
        $this->data['pageTitle'] = 'Prospects';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/prospects');
        $this->load->view('admin/common/footer');
    }

    public function reports() {
        $this->data['pageTitle'] = 'Reports';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/reports');
        $this->load->view('admin/common/footer');
    }

    public function checklist() {
        $this->data['pageTitle'] = 'Support Checklist';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->data['checklist'] = $this->admin_login_model->get_checklist_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/support_checklist');
        $this->load->view('admin/common/footer');
    }

    public function featureRequest() {
        $this->data['pageTitle'] = 'Feature Request';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/featureRequest');
        $this->load->view('admin/common/footer');
    }

    public function rolodexsecurity() {
        $this->data['pageTitle'] = 'Rolodex Security';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/rolodexsecurity');
        $this->load->view('admin/common/footer');
    }

    public function staff() {

        $this->data['pageTitle'] = 'Staff Defaults';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->data['staff'] = $this->admin_login_model->get_staff_details();
        $this->data['group'] = $this->common_model->getAllRecord('staff3', 'groupname', $cond = '', $orderby = '', $limit = '');
        //echo "<pre>"; print_r($this->data['group']); exit;
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/staff');
        $this->load->view('admin/common/footer');
    }

    public function updatechklist() {

        $formdata = extract($_POST);
        //print_r($formdata);exit;

        if (isset($install_xampp))
            $system['install_xampp'] = 1;
        else
            $system['install_xampp'] = 0;
        if (isset($code))
            $system['code'] = 1;
        else
            $system['code'] = 0;
        if (isset($data_migration))
            $system['database_migration'] = 1;
        else
            $system['database_migration'] = 0;
        if (isset($chat_server))
            $system['chat_server'] = 1;
        else
            $system['chat_server'] = 0;
        if (isset($chat_user))
            $system['created_users'] = 1;
        else
            $system['created_users'] = 0;

        $system['public_domain'] = $public_domain;
        $system['ip_address'] = $ip_addr;
        $system['updated_date'] = date('Y-m-d H:i:s');


        //print_r($system);exit;
        $check = $this->common_functions->checkRecord('support_checklist');
        //echo $check; exit;
        if ($check > 0) {
            $this->common_functions->editchklist('support_checklist', $system);
        } else {
            $system['created_date'] = date('Y-m-d H:i:s');
            $this->common_functions->insertRecord('support_checklist', $system);
        }
        $response = array('status' => 1, 'message' => 'Record Edited Successfully !!!');
        echo json_encode($response);
    }

    public function saveRequest() { // feature request ajax call
        $formdata = extract($_POST);


        if (isset($submodule) && isset($lawware_description) && !empty($lawware_description)) {
            $system['subModuleName'] = $submodule;
            $system['moduleName'] = 'Lawware'; /*In case this needs to be Application name, then use constant APPLICATION_NAME*/
            $system['featureRequest'] = $lawware_description;
            $system['last_updated'] = date('Y-m-d H:i:s');
            $this->common_functions->insertRecord('support_request', $system);
        } else {
            $system['subModuleName'] = '';
            $system['moduleName'] = '';
            $system['featureRequest'] = '';
        }

        if (isset($sns_description) && !empty($sns_description)) {
            $system['moduleName'] = 'SNS';
            $system['subModuleName'] = '';
            $system['featureRequest'] = $sns_description;
            $system['last_updated'] = date('Y-m-d H:i:s');
            $this->common_functions->insertRecord('support_request', $system);
        } else {
            $system['moduleName'] = '';
            $system['featureRequest'] = '';
        }

        if (isset($crm_description) && !empty($crm_description)) {
            $system['moduleName'] = 'CRM';
            $system['subModuleName'] = '';
            $system['featureRequest'] = $crm_description;
            $system['last_updated'] = date('Y-m-d H:i:s');
            $this->common_functions->insertRecord('support_request', $system);
        } else {
            $system['moduleName'] = '';
            $system['featureRequest'] = '';
        }
        $response = array('status' => 1, 'message' => 'Record Added Successfully !!!');
        echo json_encode($response);
        //print_r($system);exit;
    }

    public function getAjaxStaffDetails() {
        $case = $this->admin_login_model->getStaffSpecific($this->input->post('initials'));
        echo json_encode($case);
    }

    public function addstaff() {
        $formdata = $_POST;
        //print_r($formdata);exit;
        $system = array();
        $arr1 = array();
        foreach ($formdata as $key => $val) {

            $keyVar = explode("-", $key);
            if ($keyVar[0] == 'staff') {
                if ($keyVar[1] == 'main') {
                    $arr1[$keyVar[2]] = $val;
                }
                $system[$keyVar[1]] = $val;
            }
        }
        $main1 = json_encode($arr1);
        $system['main1'] = $main1;
        unset($system['main']);

        $system['emailid'] = (isset($formdata['staff-main-email'])) ? $formdata['staff-main-email'] : "";
        $checkInitial = $this->admin_login_model->checkduplicateinitial($system['initials']);
        if (!empty($checkInitial)) {
            $response = array('status' => 3, 'message' => 'Record with this initial already exist !');
        } else {
            if ($system['emailid'] != "") {
                $checkMail = $this->admin_login_model->checkduplicateemail($system['emailid']);
                if (!empty($checkMail)) {
                    $response = array('status' => 2, 'message' => 'Email Already Exists !');
                } else {
                    $this->common_functions->insertRecord('staff', $system);
                    $response = array('status' => 1, 'message' => 'Record Added Successfully !!!');
                }
            } else {
                $this->common_functions->insertRecord('staff', $system);
                $response = array('status' => 1, 'message' => 'Record Added Successfully !!!');
            }
            $this->admin_login_model->createmailtable($system['initials']);
        }
        echo json_encode($response);
    }

    public function editstaff() {

        $formdata = $_POST;
        $system = array();
        $arr1 = array();
        $inits = $formdata['staffinis'];
        $emailid = $formdata['estaff-main-email'];
        $staff = $this->data['staff'] = $this->admin_login_model->getStaffSpecific($inits);
        $main_array = $staff['main1'];
        $main_array1 = json_decode($main_array, true);

        foreach ($formdata as $key => $val) {

            $keyVar = explode("-", $key);
            if ($keyVar[0] == 'estaff') {
                if ($keyVar[1] == 'main') {
                    $arr1[$keyVar[2]] = $val;
                }
                $system[$keyVar[1]] = $val;
            }
        }
        if (!empty($main_array1)) {
            $result = array_diff_assoc($arr1, $main_array1);
            $finaal = array_merge($main_array1, $arr1);
            $main1 = json_encode($finaal);
        } else {
            $main1 = json_encode($arr1);
        }
        unset($system['main']);
        $system['main1'] = $main1;
        $content = str_replace("\n", "##", $system['vacation']);
        unset($system['vacation']);
        $system['vacation'] = $content;
        $where = array('initials' => $inits);

        $system['emailid'] = (isset($formdata['estaff-main-email'])) ? $formdata['estaff-main-email'] : "";
        $checkInitial = $this->admin_login_model->checkduplicateinitial($system['initials'], $inits);
        if (!empty($checkInitial)) {
            $response = array('status' => 3, 'message' => 'Record with this initial already exist !');
        } else {
            if ($system['emailid'] != "") {
                $checkMail = $this->admin_login_model->checkduplicateemail($system['emailid'], $inits);
                if (!empty($checkMail)) {
                    $response = array('status' => 2, 'message' => 'Email Already Exists !');
                } else {
                    $this->common_functions->editRecord('staff', $system, $where);
                    $response = array('status' => 1, 'message' => 'Record Updated Successfully !!!');
                }
            } else {
                $this->common_functions->editRecord('staff', $system, $where);
                $response = array('status' => 1, 'message' => 'Record Updated Successfully !!!');
            }
        }

        echo json_encode($response);
    }

    public function deleteAjaxStaff() {
        $staff = $this->admin_login_model->deleteStaffMember($this->input->post('initials'));
        if ($staff > 0) {
            $response = array('status' => 0, 'message' => 'You can not delete this Staff as he/she has some cases onhand !!!');

            echo json_encode($response);
        } else {
            $response = array('status' => 1, 'message' => 'Staff Deleted Successfully !!!');

            echo json_encode($response);
        }
        //echo json_encode($staff);
    }

    public function changeStatus() {
        $ini = $this->input->post('initials');
        $status = $this->input->post('status');
        $system['isActive'] = $status;
        $where = array('initials' => $ini);
        $this->common_functions->editRecord('staff', $system, $where);

        $response = array('status' => 1, 'message' => 'Status Updated Successfully !!!');
        echo json_encode($response);
    }

    public function group_profile() {
        $this->data['pageTitle'] = 'Group Profile';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->data['groups'] = $this->admin_login_model->get_group_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/group_profile');
        $this->load->view('admin/common/footer');
    }

    public function group_general() {
        $this->data['pageTitle'] = 'General Information';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/group_general');
        $this->load->view('admin/common/footer');
    }

    public function edit_general() {
        $this->data['pageTitle'] = 'Update General Information';
        $this->data['firm'] = $this->admin_login_model->get_firm_details();

        $cond = "groupkey = " . $this->uri->segment(3);
        $this->data['group'] = $this->common_model->getAllRecord('staff3', 'groupname,casetype,casestat', $cond, $orderby = '', $limit = '');

        //echo '<pre>'; print_r($this->data['group']); exit;
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/edit_general');
        $this->load->view('admin/common/footer');
    }

    public function addGroup() {
        $formdata = $_POST;
        //print_r($formdata);exit;
        $system = array();
        //$arr1 = array();
        foreach ($formdata as $key => $val) {


            $keyVar = explode("_", $key);
            //print_R($keyVar);
            if ($keyVar[0] == 'staff3') {
                $system[$keyVar[1]] = $val;
            }
        }
        //print_R($system);exit;
        $this->common_functions->insertRecord('staff3', $system);

        $response = array('status' => 1, 'message' => 'Record Added Successfully !!!');

        echo json_encode($response);
    }

    public function editGroup() {
        $formdata = $_POST;
        //print_r($formdata);exit;
        $groupkey = $this->input->post('groupid');
        $system = array();
        //$arr1 = array();
        foreach ($formdata as $key => $val) {


            $keyVar = explode("_", $key);
            //print_R($keyVar);
            if ($keyVar[0] == 'staff3') {
                $system[$keyVar[1]] = $val;
            }
        }
        //print_R($system);exit;
        //$this->common_functions->insertRecord('staff3', $system);
        $where = array('groupkey' => $groupkey);
        $this->common_functions->editRecord('staff3', $system, $where);

        $response = array('status' => 1, 'message' => 'Record Updated Successfully !!!');

        echo json_encode($response);
    }

    public function global_reassign() {
        $this->data['system_data'] = $this->common_functions->getsystemData();

        $systemDropdown = $this->case_model->getFilterArray($this->data['system_data']);
        $this->data['systemDropdown'] = $systemDropdown;

        $Calenderdropdown = [];
        $Calenderdropdown['popattyh'] = (isset($this->data['system_data'][0]->popattyh) && $this->data['system_data'][0]->popattyh != "") ? explode(',', $this->data['system_data'][0]->popattyh) : [];
        $Calenderdropdown['popattyr'] = (isset($this->data['system_data'][0]->popattyr) && $this->data['system_data'][0]->popattyr != "") ? explode(',', $this->data['system_data'][0]->popattyr) : [];

        $main1 = ($this->data['system_data'][0]->main1 != "") ? json_decode($this->data['system_data'][0]->main1) : [];
        $Calenderdropdown['Venue'] = ($main1->venue != "") ? explode(",", $main1->venue) : [];
        $Calenderdropdown['event'] = ($main1->event != "") ? explode(",", $main1->event) : [];

        $this->data['Calenderdropdown'] = $Calenderdropdown;
        $this->data['Stafflist'] = $this->admin_login_model->getstafflist();
        $this->data['pageTitle'] = 'Global Reassign';

        $this->data['firm'] = $this->admin_login_model->get_firm_details();
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/global_reassign');
        $this->load->view('admin/common/footer');
    }

    public function bug_report() {
        $this->data['pageTitle'] = 'Report Bug';
        $this->data['firm_name'] = $this->common_model->getAllRecord('system', 'firmname')[0];
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/bug_report');
        $this->load->view('admin/common/footer');
    }

    public function bug_report_list() {
        $this->data['pageTitle'] = 'Bug Report List ';
        $this->data['i'] = 0;
        $this->data['buglist'] = $this->common_model->getAllRecord('bug_report', '*,(SELECT firmname FROM system AS firmname LIMIT 0,1) as firmname', '', 'bug_report.id desc');
        //echo "<pre>"; print_r($this->data['buglist']); exit;
        $this->load->view('admin/common/header', $this->data);
        $this->load->view('admin/bug_report_list');
        $this->load->view('admin/common/footer');
    }

    public function send_bug_report() {
        if (!empty($_FILES['file_name']['name'])) {
            $config['upload_path'] = 'assets/bug_image/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['file_name']['name'];
            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file_name')) {
                $uploadData = $this->upload->data();
                $rimage = $uploadData['file_name'];
            } else {
                $rimage = '';
            }
        } else {
            $rimage = '';
        }
        $userData = array(
            'summary' => $this->input->post('summary'),
            'descriptions' => $this->input->post('bug_desciptions'),
            'repoter' => $this->input->post('repoter'),
            'priority' => $this->input->post('priority'),
            'module' => $this->input->post('module'),
            'submodule' => $this->input->post('sub_module'),
            'images' => 'assets/bug_image/' . $rimage,
            'datecreated' => date('Y-m-d H:i:s'),
            'status' => '0'
        );
        $insertUserData = $this->common_model->insertData('bug_report', $userData);
        //Storing insertion status message.
        if ($insertUserData) {
            $this->session->set_flashdata('success_msg', 'Bug have been raised successfully.');
            redirect('/admin/bug_report_list');
        } else {
            $this->session->set_flashdata('error_msg', 'Some problems occured, please try again.');
        }
    }

    public function view_bug_report() {
        $id = $this->input->post('bug_id');
        $data = '';
        $viewbug = $this->common_model->getSingleRecordById('bug_report', '*', array('id' => $id));
        if (!empty($viewbug)) {
            $data .= "<table class='table'>";
            $data .= "<tr>";
            $data .= "<td class='text-info' width='30%'>Summary :</td>";
            $data .= "<td>";
            $data .= $viewbug->summary;
            $data .= "</td>";
            $data .= "</tr>";

            $data .= "<tr>";
            $data .= "<td class='text-info'>Descriptions:</td>";
            $data .= "<td>";
            $data .= $viewbug->descriptions;
            $data .= "</td>";
            $data .= "</tr>";

            $data .= "<tr>";
            $data .= "<td class='text-info'>Reported By Firm :</td>";
            $data .= "<td>";
            $data .= $viewbug->repoter;
            $data .= "</td>";
            $data .= "</tr>";

            $data .= "<tr>";
            $data .= "<td class='text-info'>Module :</td>";
            $data .= "<td>";
            $data .= $viewbug->module;
            $data .= "</td>";
            $data .= "</tr>";

            $data .= "<tr>";
            $data .= "<td class='text-info'>Sub Module :</td>";
            $data .= "<td>";
            $data .= $viewbug->submodule;
            $data .= "</td>";
            $data .= "</tr>";

            $data .= "<tr>";
            $data .= "<td class='text-info'>Image :</td>";
            $data .= "<td>";
            $data .= "<img src='" . base_url() . $viewbug->images . "'width='600px'>";
            $data .= "</td>";
            $data .= "</tr>";

            $data .= "<tr>";
            $data .= "<td class='text-info'>Status :</td>";
            $data .= "<td>";
            if ($viewbug->status == 0) {
                $data .= "Pending";
            } else if ($viewbug->status == 1) {
                $data .= "Viewed";
            } else if ($viewbug->status == 2) {
                $data .= "Resolved";
            } else {
                $data .= "On Hold";
            }
            $data .= "</td>";
            $data .= "</tr>";
            $data .= "<tr>";
            $data .= "<td class='text-info'>Posted on:</td>";
            $data .= "<td>";
            $data .= date('m-d-Y H:i', strtotime($viewbug->datecreated));
            $data .= "</td>";
            $data .= "</tr>";

            $data .= "</table>";
            die(json_encode(array('data' => $data, 'status' => 1, 'repoter' => $viewbug->repoter)));
        } else {
            die(json_encode(array('status' => 0)));
        }
    }

    public function changeCaptionAccess() {
        $ini = $this->input->post('initials');
        $status = $this->input->post('status');
        $system['captionAccess'] = $status;
        $where = array('initials' => $ini);
        $this->common_functions->editRecord('staff', $system, $where);

        $response = array('status' => 1, 'message' => 'Caption Access Updated Successfully !!!');
        echo json_encode($response);
    }

    public function storegroupdata() {

        $groupData = array(
            'group_name' => $this->input->post('groupname'),
            'module_name' => $this->input->post('modulename'),
            'created_by' => 'AWS',
            'datetime' => date('Y-m-d H:i:s')
        );

        $checckduplicategroup = $this->admin_login_model->get_duplicategroup($this->input->post('groupname'), $this->input->post('modulename'));
        if ($checckduplicategroup > 0) {
            echo "This Group Already Exist";
        } else {
            $insertGroupData = $this->common_model->insertData('group', $groupData);
        }
    }

    public function checkduplicatevalue() {
        $groupid = $_POST['groupid'];
        $groupnameexitingmember = $this->data['groupnameexitingmember'] = $this->admin_login_model->get_existinginitialname($groupid);
        $initialnamewithoutexitingmember = $this->data['initialnamewithoutexitingmember'] = $this->admin_login_model->existingmemberreduce($groupid);

        $json['exitingmember'] = [];
        $json['newmember'] = [];

        foreach ($initialnamewithoutexitingmember as $values) {
            $json['newmember'][] = array('initials' => $values->initials);
        }

        foreach ($groupnameexitingmember as $value) {
            $json['exitingmember'][] = array('initials' => $value->initials);
        }
        $data = json_encode($json);
        echo $data;
    }

    public function storegroupmemberdata() {

        $initialarray = explode(',', $_POST['initials']);


        $this->data['deletegroupmemberid'] = $this->admin_login_model->deletegroupmemberbygroup($this->input->post('groupnameid'));



        foreach ($initialarray as $initiallist) {

            $groupData = array(
                'group_id' => $this->input->post('groupnameid'),
                'initials' => $initiallist,
                'role' => 'member',
                'created_datetime' => date('Y-m-d H:i:s')
            );

            $insertGroupData = $this->common_model->insertData('group_member', $groupData);
        }
    }

}

?>