<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class GlobalReassign extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('common_model');
        $this->load->model('admin_login_model');
    }

    function caseReassign() {
        extract($_POST);
        $casetype = $case_case_type;
        $casestatus = $case_case_status;
        $atty_resp = $case_atty_resp;
        $atty_hand = $case_atty_hand;
        $para_hand = $case_para_hand;
        $sec_hand = $case_sec_hand;
        $primary_party = $case_primary;
        $new_atty_resp = $case_new_atty_resp;
        $new_atty_hand = $case_new_atty_hand;
        $new_para_hand = $case_new_para_hand;
        $new_sec_hand = $case_new_sec_hand;

        $andwhere = [];
        $andwhere = [];
        if (isset($casetype) && $casetype != "") {
            $andwhere['case.casetype'] = $casetype;
        }
        if (isset($casestatus) && $casestatus != "") {
            $andwhere['case.casestat'] = $casestatus;
        }
        if (isset($atty_resp) && $atty_resp != "") {
            $andwhere['case.atty_resp'] = $atty_resp;
        }
        if (isset($atty_hand) && $atty_hand != "") {
            $andwhere['case.atty_hand'] = $atty_hand;
        }
        if (isset($para_hand) && $para_hand != "") {
            $andwhere['case.para_hand'] = $para_hand;
        }
        if (isset($sec_hand) && $sec_hand != "") {
            $andwhere['case.sec_hand'] = $sec_hand;
        }

        $first_name = '';
        $last_name = '';
        $andwhere['first_name'] = "";
        $andwhere['last_name'] = "";
        if (!empty($primary_party)) {
            $primary_party = explode("-", $primary_party);
            $first_name = $primary_party[0];
            $last_name = $primary_party[1];
            $andwhere['first_name'] = $first_name;
            $andwhere['last_name'] = $last_name;
        }

        echo $getRecords = $this->admin_login_model->get_cases($andwhere);
    }

    function updatecases() {
        extract($_POST);

        $casetype = $case_case_type;
        $casestatus = $case_case_status;
        $atty_resp = $case_atty_resp;
        $atty_hand = $case_atty_hand;
        $para_hand = $case_para_hand;
        $sec_hand = $case_sec_hand;
        $primary_party = $case_primary;

        $new_atty_resp = $case_new_atty_resp;
        $new_atty_hand = $case_new_atty_hand;
        $new_para_hand = $case_new_para_hand;
        $new_sec_hand = $case_new_sec_hand;


        $andwhere = [];
        if (isset($casetype) && $casetype != "") {
            $andwhere['case.casetype'] = $casetype;
        }
        if (isset($casestatus) && $casestatus != "") {
            $andwhere['case.casestat'] = $casestatus;
        }
        if (isset($atty_resp) && $atty_resp != "") {
            $andwhere['case.atty_resp'] = $atty_resp;
        }
        if (isset($atty_hand) && $atty_hand != "") {
            $andwhere['case.atty_hand'] = $atty_hand;
        }
        if (isset($para_hand) && $para_hand != "") {
            $andwhere['case.para_hand'] = $para_hand;
        }
        if (isset($sec_hand) && $sec_hand != "") {
            $andwhere['case.sec_hand'] = $sec_hand;
        }

        $first_name = '';
        $last_name = '';
        $andwhere['first_name'] = "";
        $andwhere['last_name'] = "";
        if (!empty($primary_party)) {
            $primary_party = explode("-", $primary_party);
            $first_name = $primary_party[0];
            $last_name = $primary_party[1];
            $andwhere['first_name'] = $first_name;
            $andwhere['last_name'] = $last_name;
        }
        $fields = [];
        if ($new_atty_resp != "") {
            $fields['case.atty_resp'] = strtoupper($new_atty_resp);
        }
        if ($new_atty_hand != "") {
            $fields['case.atty_hand'] = strtoupper($new_atty_hand);
        }
        if ($new_para_hand != "") {
            $fields['case.para_hand'] = strtoupper($new_para_hand);
        }
        if ($new_sec_hand != "") {
            $fields['case.sec_hand'] = strtoupper($new_sec_hand);
        }

        echo $getRecords = $this->admin_login_model->reassign_cases($andwhere, $fields);
        exit;
    }

    function taskReassign() {
        extract($_POST);
        //echo '<pre>'; print_r($_POST); exit;
//        $case_type = $task_case_type;
//        $case_status = $task_case_status;
//        $atty_resp = $task_atty_resp;
//        $atty_hand = $task_atty_hand;
//        $para_hand = $task_para_hand;
//        $sec_hand = $task_sec_hand;
//        $task_to = $task_task_to;
        $new_task_to = $new_task_task_to;

        $where = [];

        if ($task_task_to != "") {
            $where['tasks.whoto'] = $task_task_to;
        }
        if ($task_case_type != "") {
            $where['case.casetype'] = $task_case_type;
        }
        if ($task_case_status != "") {
            $where['case.casestat'] = $task_case_status;
        }
        if ($task_atty_resp != "") {
            $where['case.atty_resp'] = $task_atty_resp;
        }
        if ($task_atty_hand != "") {
            $where['case.atty_hand'] = $task_atty_hand;
        }
        if ($task_para_hand != "") {
            $where['case.para_hand'] = $task_para_hand;
        }
        if ($task_sec_hand != "") {
            $where['case.sec_hand'] = $task_sec_hand;
        }

        echo $getRecords = $this->admin_login_model->get_task($where);
    }

    function updatetasks() {
        extract($_POST);
        $case_type = $task_case_type;
        $case_status = $task_case_status;
        $atty_resp = $task_atty_resp;
        $atty_hand = $task_atty_hand;
        $para_hand = $task_para_hand;
        $sec_hand = $task_sec_hand;
        $task_to = $task_task_to;

        $new_task_to = $new_task_task_to;

        $where = [];
        $fields = [];

        if ($task_to != "") {
            $where['tasks.whoto'] = $task_to;
        }
        if ($case_type != "") {
            $where['case.casetype'] = $case_type;
        }
        if ($case_status != "") {
            $where['case.casestat'] = $case_status;
        }
        if ($atty_resp != "") {
            $where['case.atty_resp'] = $atty_resp;
        }
        if ($atty_hand != "") {
            $where['case.atty_hand'] = $atty_hand;
        }
        if ($para_hand != "") {
            $where['case.para_hand'] = $para_hand;
        }
        if ($sec_hand != "") {
            $where['case.sec_hand'] = $sec_hand;
        }

        $fields['whoto'] = (isset($new_task_to) && $new_task_to != "") ? strtoupper($new_task_to) : "";
        echo $getRecords = $this->admin_login_model->reassign_tasks($where, $fields);
        exit;
    }

    function eventReassign() {
        extract($_POST);
        $status = $cal_status;
        $start = date("Y-m-d", strtotime($cal_start_dt));
        $end = date("Y-m-d", strtotime($cal_end_dt));
        $event = $cal_event;
        $venue = $cal_venue;
        $attr = $cal_attr;
        $attr_ass = $cal_attr_ass;
        $join = '';
        $where = '';
        $where .= "cal1.date LIKE '%$start%'";

        $where = [];
        if ($status != "") {
            $where['cal1.calstat'] = $status;
        }
        if ($event != "") {
            $where['cal1.event'] = $event;
        }
        if ($venue != "") {
            $where['cal1.venue'] = $venue;
        }
        if ($attr != "") {
            $where['cal2.initials'] = $attr;
        }
        if ($attr_ass != "") {
            $where['cal1.attyass'] = $attr_ass;
        }
        if ($start != "" && $end != "") {
            $where['cal1.date >='] = date('Y-m-d 00:00:00', strtotime($start));
            $where['cal1.date <='] = date('Y-m-d 23:59:59', strtotime($end));
        } elseif ($start != "" && $end == "") {
            $where['cal1.date >='] = date('Y-m-d 00:00:00', strtotime($start));
        } elseif ($start == "" && $end != "") {
            $where['cal1.date <='] = date('Y-m-d 23:59:59', strtotime($end));
        }


        echo $getRecords = $this->admin_login_model->get_events($where);
    }

    function updateevents() {
        extract($_POST);
        $status = $cal_status;
        $start = date("Y-m-d", strtotime($cal_start_dt));
        $end = date("Y-m-d", strtotime($cal_end_dt));
        $event = $cal_event;
        $venue = $cal_venue;
        $attr = $cal_attr;
        $attr_ass = $cal_attr_ass;

        $new_cal_attr = $new_cal_attr;
        $new_cal_attr_ass = $new_cal_attr_ass;

        $where = [];
        if ($status != "") {
            $where['cal1.calstat'] = $status;
        }
        if ($event != "") {
            $where['cal1.event'] = $event;
        }
        if ($venue != "") {
            $where['cal1.venue'] = $venue;
        }
        if ($attr != "") {
            $where['cal2.initials'] = $attr;
        }
        if ($attr_ass != "") {
            $where['cal1.attyass'] = $attr_ass;
        }
        if ($start != "" && $end != "") {
            $where['cal1.date >='] = date('Y-m-d 00:00:00', strtotime($start));
            $where['cal1.date <='] = date('Y-m-d 23:59:59', strtotime($end));
        } elseif ($start != "" && $end == "") {
            $where['cal1.date >='] = date('Y-m-d 00:00:00', strtotime($start));
        } elseif ($start == "" && $end != "") {
            $where['cal1.date <='] = date('Y-m-d 23:59:59', strtotime($end));
        }

        $fields = [];
        if ($new_cal_attr != "") {
            $fields['cal2.initials'] = (isset($new_cal_attr) && $new_cal_attr != "") ? strtoupper($new_cal_attr) : "";
        }
        if ($new_cal_attr_ass != "") {
            $fields['cal1.attyass'] = (isset($new_cal_attr_ass) && $new_cal_attr_ass != "") ? strtoupper($new_cal_attr_ass) : "";
        }

        echo $getRecords = $this->admin_login_model->reassign_eventss($where, $fields);
    }

    public function caseactReassign() {
        extract($_POST);

        $where = [];
        if ((isset($caseact_casetype)) && $caseact_casetype != "") {
            $where['case.casetype'] = $caseact_casetype;
        }
        if ((isset($caseact_casestatus)) && $caseact_casestatus != "") {
            $where['case.casestat'] = $caseact_casestatus;
        }
        if ((isset($caseact_atty_resp)) && $caseact_atty_resp != "") {
            $where['case.atty_resp'] = $caseact_atty_resp;
        }
        if ((isset($caseact_atty_hand)) && $caseact_atty_hand != "") {
            $where['case.atty_hand'] = $caseact_atty_hand;
        }
        if ((isset($caseact_para_hand)) && $caseact_para_hand != "") {
            $where['case.para_hand'] = $caseact_para_hand;
        }
        if ((isset($caseact_sec_hand)) && $caseact_sec_hand != "") {
            $where['case.sec_hand'] = $caseact_sec_hand;
        }
        if ((isset($caseact_old_category)) && $caseact_old_category != "") {
            $where['caseact.category'] = $caseact_old_category;
        }

        $fields = [];
        if ((isset($caseact_new_category)) && $caseact_new_category != "") {
            $fields['caseact.category'] = $caseact_new_category;
        }

        echo $getRecords = $this->admin_login_model->getcaseactReassign($where);
    }

    public function updatecaseactassign() {
        extract($_POST);
        $where = [];
        if ((isset($caseact_casetype)) && $caseact_casetype != "") {
            $where['case.casetype'] = $caseact_casetype;
        }
        if ((isset($caseact_casestatus)) && $caseact_casestatus != "") {
            $where['case.casestat'] = $caseact_casestatus;
        }
        if ((isset($caseact_atty_resp)) && $caseact_atty_resp != "") {
            $where['case.atty_resp'] = $caseact_atty_resp;
        }
        if ((isset($caseact_atty_hand)) && $caseact_atty_hand != "") {
            $where['case.atty_hand'] = $caseact_atty_hand;
        }
        if ((isset($caseact_para_hand)) && $caseact_para_hand != "") {
            $where['case.para_hand'] = $caseact_para_hand;
        }
        if ((isset($caseact_sec_hand)) && $caseact_sec_hand != "") {
            $where['case.sec_hand'] = $caseact_sec_hand;
        }
        if ((isset($caseact_old_category)) && $caseact_old_category != "") {
            $where['caseact.category'] = $caseact_old_category;
        }

        $fields = [];
        if ((isset($caseact_new_category)) && $caseact_new_category != "") {
            $fields['caseact.category'] = $caseact_new_category;
        }

        echo $getRecords = $this->admin_login_model->updatecaseactassign($where, $fields);
    }

}

?>
