<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Prospects extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('prospects_model');
        $this->load->model('common_model');
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $data = array();
        $this->data['menu'] = 'prospect';
        $this->data['username'] = $this->session->userdata('user_data')['username'];
        $this->data['loginUser'] = $this->session->userdata('user_data')['username'];
        $this->data['url_seg'] = $this->uri->segment(1);

    }
    function __destruct() {
    }
    public function index($caseId = '') {
        $this->output->enable_profiler(false);
        $this->data['languageList'] = $this->config->item('languageList');
        $this->data['saluatation'] = $this->config->item('saluatation');
        $this->data['suffix'] = $this->config->item('suffix');
        $this->data['title'] = $this->config->item('title');
//        $this->data['category'] = $this->config->item('category');
//        $this->data['category_type'] = $this->config->item('category_type');

        $this->data['pageTitle'] = 'Prospect';
        $this->data['caseId'] = $caseId;
        //staffList

        $getAllStaff = $this->common_model->getAllRecord('staff', 'DISTINCT(initials)', '', 'initials asc');
        $this->data['staffList'] = $getAllStaff;

        $system_data = $this->common_functions->getsystemData();
        $this->data['system_data'] = $system_data;
        $getAttyResp=explode(',', $system_data[0]->popattyr);

        foreach($getAttyResp as $keyaar=>$valueaar)
        {
            $attrResp[] = $valueaar;
        }
        $this->data['atty_resp'] = $attrResp;

        $this->data['atty_hand']=explode(',', $system_data[0]->popattyh);

        $this->data['para_hand']=explode(',', $system_data[0]->poppara);

        $this->data['sec_hand']=explode(',', $system_data[0]->popsec);
        $this->data['category'] = explode(',', $this->data['system_data'][0]->popclient);
        //Case Type

        $getCaseDetails = json_decode($system_data[0]->pncdd);


        $this->data['casetype'] = $getCaseDetails->toc != '' ? explode(',', $getCaseDetails->toc) : '';

        $this->data['casetypevalidate'] = (isset($getCaseDetails->chk1) && $getCaseDetails->chk1 == 'on') ? 1 : 0;

        $this->data['casetypedrp'] = (isset($getCaseDetails->drp1) && $getCaseDetails->drp1 == 'on') ? 1 : 0;


        //Case Status

        $this->data['casestat'] = $getCaseDetails->status != '' ? explode(',', $getCaseDetails->status) : '';

        $this->data['casestatvalidate'] = (isset($getCaseDetails->chk2) && $getCaseDetails->chk2 == 'on') ? 1 : 0;

        $this->data['casestatusdrp'] = (isset($getCaseDetails->drp2) && $getCaseDetails->drp2 == 'on') ? 1 : 0;

        //case category

        $getCardType = $this->common_model->getAllRecord('card', 'DISTINCT(type)', 'type != ""', 'type asc');
        $this->data['type'] = $getCardType;

        //para

        $this->data['record'] = $this->common_model->getRecord();
      //  $this->data['Prospect_data'] = $this->common_model->getAllRecordProspect();
        $this->data['Prospect_data'] = $this->common_model->getsingleRecordProspect($caseId);

        $this->data['Prospect_mainkey'] = $this->common_model->getMainKey();

        $this->data['casestat'] = $getCaseDetails->status != '' ? explode(',', $getCaseDetails->status) : '';

        $this->data['referral'] = $getCaseDetails->refc != '' ? explode(',', $getCaseDetails->refc) : '';

        $this->data['referralvalidate'] = (isset($getCaseDetails->chk3) && $getCaseDetails->chk3 == 'on') ? 1 : 0;

        $this->data['casereferraldrp'] = (isset($getCaseDetails->drp3) && $getCaseDetails->drp3 == 'on') ? 1 : 0;


        //Referral Source


        $this->data['referralsrc'] = $getCaseDetails->refsrc != '' ? explode(',', $getCaseDetails->refsrc) : '';

        $this->data['casereferraldrp'] = (isset($getCaseDetails->drp3) && $getCaseDetails->drp3 == 'on') ? 1 : 0;

        $this->data['referralsrcvalidate'] = (isset($getCaseDetails->chk4) && $getCaseDetails->chk4 == 'on') ? 1 : 0;

        $this->data['referralsrcdrp'] = (isset($getCaseDetails->drp4) && $getCaseDetails->drp4 == 'on') ? 1 : 0;


        //Referred By

        $this->data['referredby'] = $getCaseDetails->refby != '' ? explode(',', $getCaseDetails->refby) : '';

        $this->data['referredbyvalidate'] = (isset($getCaseDetails->chk5) && $getCaseDetails->chk5 == 'on') ? 1 : 0;

        $this->data['referredbydrp'] = (isset($getCaseDetails->drp5) && $getCaseDetails->drp5 == 'on') ? 1 : 0;


        //Association

        $this->data['association'] = $getCaseDetails->assoc != '' ? explode(',', $getCaseDetails->assoc) : '';

        $this->data['associationvalidate'] = (isset($getCaseDetails->chk6) && $getCaseDetails->chk6 == 'on') ? 1 : 0;

        $this->data['associationdrp'] = (isset($getCaseDetails->drp6) && $getCaseDetails->drp6 == 'on') ? 1 : 0;


        //Referred To

        $this->data['referredto'] = $getCaseDetails->refto != '' ? explode(',', $getCaseDetails->refto) : '';

        $this->data['referredtovalidate'] = (isset($getCaseDetails->chk7) && $getCaseDetails->chk7 == 'on') ? 1 : 0;

        $this->data['referredtodrp'] = (isset($getCaseDetails->drp7) && $getCaseDetails->drp7 == 'on') ? 1 : 0;

        //Our Ref Status

        $this->data['referalstatus'] = $getCaseDetails->ors != '' ? explode(',', $getCaseDetails->ors) : '';

        $this->data['rstatusvalidate'] = (isset($getCaseDetails->chk8) && $getCaseDetails->chk8 == 'on') ? 1 : 0;

        $this->data['rstatusdrp'] = (isset($getCaseDetails->chk8) && $getCaseDetails->drp8 == 'on') ? 1 : 0;


        //Mailinag list title 1 - 6 Start

        $this->data['mtitle1'] = $getCaseDetails->t1 != '' ? $getCaseDetails->t1 : '';

        $this->data['mtitleval1'] = (isset($getCaseDetails->d1) && $getCaseDetails->d1 == 'on') ? 1 : 0;

        $this->data['mtitle2'] = $getCaseDetails->t2 != '' ? $getCaseDetails->t2 : '';

        $this->data['mtitleval2'] = (isset($getCaseDetails->d2) && $getCaseDetails->d2 == 'on') ? 1 : 0;

        $this->data['mtitle3'] = $getCaseDetails->t3 != '' ? $getCaseDetails->t3 : '';

        $this->data['mtitleval3'] = (isset($getCaseDetails->d3) && $getCaseDetails->d3 == 'on') ? 1 : 0;

        $this->data['mtitle4'] = $getCaseDetails->t4 != '' ? $getCaseDetails->t4 : '';

        $this->data['mtitleval4'] = (isset($getCaseDetails->d4) && $getCaseDetails->d4 == 'on') ? 1 : 0;

        $this->data['mtitle5'] = $getCaseDetails->t5 != '' ? $getCaseDetails->t5 : '';

        $this->data['mtitleval5'] = (isset($getCaseDetails->d5) && $getCaseDetails->d5 == 'on') ? 1 : 0;

        $this->data['mtitle6'] = $getCaseDetails->t6 != '' ? $getCaseDetails->t6 : '';

        $this->data['mtitleval6'] = (isset($getCaseDetails->d6) && $getCaseDetails->d6 == 'on') ? 1 : 0;


        //Mailinag list title 1 - 6 End

        //Get details for notes//

        $getCaseDetails1 = json_decode($system_data[0]->main1);
        $main1=json_decode($this->data['system_data'][0]->main1);
        $this->data['Venue'] = explode(',', $main1->venue);
        $this->data['specialty'] = explode(',', $system_data[0]->specialty);

        if(isset($getCaseDetails1->wen))
        {
            $this->data['notesPosition'] = $getCaseDetails1->wen;
        }

        $getStaffInfo= $this->common_functions->getAllRecord('staff', 'vacation', "initials ='".$this->session->userdata('user_data')['username']."' ", 'initials asc');
        $this->data['staff_details'] = $getStaffInfo;

        $this->load->view('common/header',$this->data);
        $this->load->view('prospects/prospects');
        $this->load->view('common/pncmodals');
        //$this->load->view('common/tasks_modals');
        $this->load->view('common/footer');
    }
    public function addProspects(){
        $prospectArray = $_POST;
        $prospectArray1 = $this->createArray($prospectArray);
        //echo '<pre>'; print_r($prospectArray1); exit;
        $intakeId = $this->common_functions->insertRecord('intake',$prospectArray1);
       // echo $intakeId; exit;
        $response = array();
        if($intakeId  > 0){
           $response = array('status'=>1,'prospect_id_last'=>$intakeId,'message'=>'Prospect Added Successfully.');
        }else{
           $response = array('status'=>0,'message'=>'Prospect Not Added.');
        }
           echo json_encode($response);
    }
    public function editProspects(){
        $prospectArray = $_POST;
        //echo '<pre>'; print_r($prospectArray); exit;
        $prospectId = $prospectArray['prospectId'];
        $cond = array();
        $cond['prospect_id'] = $prospectId;
        unset($prospectArray['taskid']);
        $prospectArray = $this->createArray($prospectArray);
        //echo '<pre>'; print_r($prospectArray); exit;
        $result = $this->common_functions->editRecord('intake',$prospectArray,$cond);
        $response = array();
        if($result >= 0){
            $response = array('status'=>1,'prospect_id_last'=>$prospectId,'message'=>'Prospect Edited Successfully.');
        }else{
            $response = array('status'=>0,'message'=>'Prospect Not Added.');
        }
        echo json_encode($response);
    }
    public function createArray($prospectArray)
    {
        //tab1
        $prospectArray_new=array();
        $prospectArray_new['salutation'] = isset($prospectArray['pro_salutation']) ? $prospectArray['pro_salutation'] : '';
        $prospectArray_new['first']= isset($prospectArray['pro_first']) ? $prospectArray['pro_first'] : '';
        $prospectArray_new['last']= isset($prospectArray['pro_last']) ? $prospectArray['pro_last'] : '';
        $prospectArray_new['suffix']= isset($prospectArray['pro_suffix']) ? $prospectArray['pro_suffix'] : '';
        $prospectArray_new['cell']= isset($prospectArray['pro_cell']) ? $prospectArray['pro_cell'] : '';
        $prospectArray_new['business']= isset($prospectArray['pro_business']) ? $prospectArray['pro_business'] : '';
        $prospectArray_new['home']= isset($prospectArray['pro_home']) ? $prospectArray['pro_home'] : '';
        $prospectArray_new['address1']= isset($prospectArray['pro_address1']) ? $prospectArray['pro_address1'] : '';
        $prospectArray_new['city']= isset($prospectArray['pro_city']) ? $prospectArray['pro_city'] : '';
        $prospectArray_new['state']= isset($prospectArray['pro_state']) ? $prospectArray['pro_state'] : '';
        $prospectArray_new['zip']= isset($prospectArray['pro_zip']) ? $prospectArray['pro_zip'] : '';
        $prospectArray_new['social_sec']= isset($prospectArray['pro_social_sec']) ? $prospectArray['pro_social_sec'] : '';
        $prospectArray_new['email']= isset($prospectArray['pro_email']) ? $prospectArray['pro_email'] : '';
        if(!empty($prospectArray['pro_birthdate']) && $prospectArray['pro_birthdate'] != "0000-00-00 00:00:00" )
            $prospectArray_new['birthdate']= isset($prospectArray['pro_birthdate'])?date('Y-m-d H:i:s',strtotime($prospectArray['pro_birthdate'])):"0000-00-00 00:00:00";
        else
            $prospectArray_new['birthdate']="0000-00-00 00:00:00";
        $prospectArray_new['type']= isset($prospectArray['pro_type']) ? $prospectArray['pro_type'] : '';
        $prospectArray_new['casetype']= isset($prospectArray['pro_casetype']) ? $prospectArray['pro_casetype'] : '';
        $prospectArray_new['initials0']= isset($prospectArray['pro_initials0']) ? $prospectArray['pro_initials0'] : '';
        $prospectArray_new['casestat']= isset($prospectArray['pro_casestat']) ? $prospectArray['pro_casestat'] : '';
        $prospectArray_new['pendwith']= isset($prospectArray['pro_pendwith']) ? $prospectArray['pro_pendwith'] : '';
        if(!empty($prospectArray['pro_daterefin']) && $prospectArray['pro_daterefin'] != "0000-00-00 00:00:00")
            $prospectArray_new['daterefin']= isset($prospectArray['pro_daterefin'])?date('Y-m-d H:i:s',strtotime($prospectArray['pro_daterefin'])):"0000-00-00 00:00:00";
        else
            $prospectArray_new['daterefin']= '0000-00-00 00:00:00:00';
        $prospectArray_new['atty']= isset($prospectArray['pro_atty']) ? $prospectArray['pro_atty'] : '';
        $prospectArray_new['datelastcn']= isset($prospectArray['pro_datelastcn'])?date('Y-m-d H:i:s',strtotime($prospectArray['pro_datelastcn'])):'0000-00-00 00:00:00';
        $prospectArray_new['mailingh']= isset($prospectArray['pro_mailingh']) ? '1' :'' ;
        $prospectArray_new['mailing1']= isset($prospectArray['pro_mailing1']) ? '1' :'' ;
        $prospectArray_new['mailing2']= isset($prospectArray['pro_mailing2']) ? '1' :'' ;
        $prospectArray_new['mailing3']= isset($prospectArray['pro_mailing3']) ? '1' :'' ;
        $prospectArray_new['mailing4']= isset($prospectArray['pro_mailing4']) ? '1' :'' ;
        $prospectArray_new['mailing5']= isset($prospectArray['pro_mailing5']) ? '1' :'' ;
        $prospectArray_new['todonext']= isset($prospectArray['pro_todonext']) ? $prospectArray['pro_todonext'] : '';
        //tab2
        $prospectArray_new['refcode']= isset($prospectArray['pro_refcode']) ? $prospectArray['pro_refcode'] : '';
        $prospectArray_new['refto']= isset($prospectArray['pro_refto']) ? $prospectArray['pro_refto'] : '';
        $prospectArray_new['refsource']=isset($prospectArray['pro_refsource']) ? $prospectArray['pro_refsource'] : '';
        if(!empty($prospectArray['pro_dateref'])  && $prospectArray['pro_dateref'] != "0000-00-00 00:00:00")
             $prospectArray_new['dateref']= isset($prospectArray['pro_dateref'])?date('Y-m-d H:i:s',strtotime($prospectArray['pro_dateref'])):'0000-00-00 00:00:00:00';
        else
            $prospectArray_new['dateref']= '0000-00-00 00:00:00:00';
        if(!empty($prospectArray['pro_datepaid']) && $prospectArray['pro_datepaid'] != "0000-00-00 00:00:00")
            $prospectArray_new['datepaid']= isset($prospectArray['pro_datepaid'])?date('Y-m-d H:i:s',strtotime($prospectArray['pro_datepaid'])):'0000-00-00 00:00:00';
        else
            $prospectArray_new['datepaid'] = '0000-00-00 00:00:00';
        $prospectArray_new['refby']= isset($prospectArray['pro_refby']) ? $prospectArray['pro_refby'] : '';
        $prospectArray_new['outrefstat']= isset($prospectArray['pro_outrefstat']) ? $prospectArray['pro_outrefstat'] : '';
        $prospectArray_new['amount']= isset($prospectArray['pro_amount']) ? $prospectArray['pro_amount'] : '';

        // print_r($prospectArray); exit;
        //tab 3 notes'
        $pro_notes = str_replace("\r\n",'', $prospectArray['pro_notes']);
        $prospectArray_new['notes']=isset($prospectArray['pro_notes']) ? $pro_notes : '';
        //tab 4 injury and all
        $pro_reffee= isset($prospectArray['pro_reffee']) ? $prospectArray['pro_reffee']:'NO';
        $pro_injn_emp=isset($prospectArray['pro_injn_emp']) ? $prospectArray['pro_injn_emp'] : '';
        $pro_injn_date=isset($prospectArray['pro_injn_date']) ? $prospectArray['pro_injn_date'] : '';
        $pro_injn_body=isset($prospectArray['pro_injn_body']) ? $prospectArray['pro_injn_body'] : '';
        $pro_accociation=isset($prospectArray['pro_accociation']) ? $prospectArray['pro_accociation'] : '';
        $pro_followup_date=isset($prospectArray['pro_followup_date']) ? $prospectArray['pro_followup_date'] : '0000-00-00 00:00:00';
        $pro_per_reffee=isset($prospectArray['pro_per_reffee']) ? $prospectArray['pro_per_reffee'] :0;
        $prospectArray_new['fieldsudf']=$pro_reffee.",".$pro_injn_emp.",".$pro_injn_date.",".$pro_injn_body.','.$pro_accociation.','.$pro_followup_date.','.$pro_per_reffee;
        // print_r($prospectArray_new['fieldsudf']);
        // tab 5 Adavaced
        $prospectArray_new['caseno']=isset($prospectArray['pro_caseno']) ? $prospectArray['pro_caseno'] : '';
        $prospectArray_new['prosno']=isset($prospectArray['pro_prosno']) ? $prospectArray['pro_prosno'] : '';
        $prospectArray_new['proskey']=isset($prospectArray['pro_proskey']) ? $prospectArray['pro_proskey'] : '';
        return $prospectArray_new;
    }
    public function getProspectsdata()
    {
        extract($_POST);
      //  print_r($prospect_id);exit;
        $prospectsdata['data'] = $this->prospects_model->getProspectsdata($prospect_id);
        $prospectsdata['Prospect_mainkey'] = $this->common_model->getMainKey();
        die(json_encode($prospectsdata));
    }
    public function checkDuplicateProspect() {
        $res = $this->prospects_model->checkDuplicateProspect($this->input->post('first_name'), $this->input->post('last_name'));
        if (!empty($res)) {
            echo json_encode(array("status" => 1, "data" => $res));
        } else {
            echo json_encode(array("status" => 0, "data" => []));
        }
    }
    public function deleteRec() {
        extract($_POST);
        $res = $this->common_functions->editRecord('intake', array('status' => $status), array('prospect_id' => $ProspectId));
        if ($res > 0) {
            $response = array('status' => 1, 'message' => 'Prospect successfully deleted.');
            //$this->session->set_flashdata('case_message', 'Your Case Removed Successfully');
        } else {
            $response = array('status' => 0, 'message' => 'Oops! Something went wrong.');
        }
         echo json_encode($response);
    }
    function searchForProspects(){
        extract($_POST);
        $filterArray = array();
        if (isset($pro_casetype1) && $pro_casetype1 != '') {
            $filterArray['casetype' . ' like'] = $pro_casetype1 . "%";
        }
        if (isset($pro_home1) && $pro_home1 != '') {
            $filterArray['home' . ' like'] = $pro_home1. "%";
        }
        if (isset($pro_casestat1) && $pro_casestat1!= '') {
            $filterArray['casestat' . ' like'] = $pro_casestat1 . "%";
        }
        if (isset($pro_business1) && $pro_business1!= '') {
            $filterArray['business' . ' like'] = $business1 . "%";
        }
        if (isset($pro_initials01) && $pro_initials01 != '') {

             $filterArray['initials0' . ' like'] = $pro_initials01. "%";
        }
        if (isset($pro_case1) && $pro_case1 != '') {
            if ($pro_case1 == 'attach') {
                $filterArray['caseno >'] = 0;
            } else if ($pro_case1 == 'unattach' ) {
                $filterArray['caseno <='] = 0;
            } else {
               // $filterArray['cd.cardcode'] = $searcharray['searchByCardNumber'];
            }
        }
        if (isset($pro_pendwith1) && $pro_pendwith1!= '') {
            $filterArray['pendwith' . ' like '] = "%" . $pro_pendwith1 . "%";
        }
       if (isset($pro_first1_s) && $pro_first1_s != '') {
            $filterArray['first' . ' like'] = $pro_first1_s. "%";
        }
        if (isset($pro_atty1) && $pro_atty1 != '') {
            $filterArray['atty' . ' like'] = $pro_atty1. "%";
        }
        if (isset($pro_last1_s) && $pro_last1_s != '') {
            $filterArray['last' . ' like'] = $pro_last1_s. "%";
        }
        if (isset($pro_daterefin1) && $pro_daterefin1 != '') {
            $filterArray['daterefin' . ' like'] = date('Y-m-d',strtotime($pro_daterefin1)). "%";
        }
        if (isset($pro_datelastcn1) && $pro_datelastcn1 != '') {
            $filterArray['datelastcn' . ' like'] = date('Y-m-d',strtotime($pro_datelastcn1)). "%";
        }
        if (isset($pro_mailingh_1) && $pro_mailingh_1 != '') {
            $filterArray['mailingh' . ' like'] = "1";
        }
        if (isset($pro_mailing1_1) && $pro_mailing1_1 != '') {
            $filterArray['mailing1' . ' like'] = "1";
        }
        if (isset($pro_mailing2_1) && $pro_mailing2_1 != '') {
            $filterArray['mailing2' . ' like'] = "1";
        }
        if (isset($pro_mailing3_1) && $pro_mailing3_1 != '') {
            $filterArray['mailing3' . ' like'] = "1";
        }
        if (isset($pro_mailing4_1) && $pro_mailing4_1 != '') {
            $filterArray['mailing4' . ' like'] = "1";
        }
        if (isset($pro_mailing5_1) && $pro_mailing5_1 != '') {
            $filterArray['mailing5' . ' like'] = "1";
        }


        if (isset($pro_refcode1) && $pro_refcode1 != '') {
            $filterArray['refcode' . ' like']  = $pro_refcode1. "%";
        }

        if (isset($pro_accociation1) && $pro_accociation1 != '') {
            $filterArray['accociation' . ' like']  = $pro_accociation1. "%";
        }

        if (isset($pro_refby1) && $pro_refby1 != '') {
            $filterArray['refby' . ' like']  = $pro_refby1. "%";
        }
        if (isset($pro_refto1) && $pro_refto1 != '') {
            $filterArray['refto' . ' like']  = $pro_refto1. "%";
        }
        if (isset($pro_refsource1) && $pro_refsource1 != '') {
            $filterArray['refsource' . ' like']  = $pro_refsource1. "%";
        }
        if (isset($pro_outrefstat1) && $pro_outrefstat1 != '') {
            $filterArray['outrefstat' . ' like']  = $pro_outrefstat1. "%";
        }
        if (isset($pro_amount1) && $pro_amount1 != '') {
            $filterArray['amount' . ' like']  = $pro_amount1. "%";
        } //print_R($filterArray);exit;
        $res= $this->prospects_model->getProspectListing($filterArray);
        if (!empty($res)) {
            echo json_encode(array("status" => 1, "data" => $res));
        } else {
            echo json_encode(array("status" => 0, "data" => []));
        }
    }
    function CheckCaseno() {
      extract($_POST);
      $res= $this->common_functions->getCaseno($pro_caseno);
      if(!empty($res))
      {
          echo json_encode(TRUE);
      }
      else {
          echo json_encode(FALSE);
       }
    }
}