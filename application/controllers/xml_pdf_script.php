<?php
    if (!defined('BASEPATH')) {
        exit('No direct script access allowed');
    }

    class Xml_pdf_script extends CI_Controller {    
        public function __construct() {
            parent::__construct();
            $this->load->library('session');
            $this->common_functions->checkLogin();
            $this->common_functions->checkSessionId();
            $this->output->enable_profiler(false);
        }
    
        public function __destruct() {
    
        }
    
        public function index() {
            $files1 = glob('S:/A1Law/EXTRAS/*.xml');
            $files2 = glob('S:/A1Law/EXTRAS/*.XML');
            $files = array_merge($files1, $files2);
            foreach ($files as $key => $value) {
                $tmp = explode('/', $value);
                $new_value = explode('.', $tmp[count($tmp) - 1]);
                $f2 = $new_value[0];
                $folder_number = '6000';
                $case_no = '5581';
                $f1 = 'f811';
                $s_drive_path = 'S:/A1Law/EXTRAS/';
                $file_1 = FCPATH.'assets/clients/'.$folder_number.'/'.$case_no.'/'.$f1.'.xml';
                $file_2 = $s_drive_path.$f2.'.xml';
                $file_2_pdf = $s_drive_path.$f2.'.pdf';
                $tmp_fdf = $s_drive_path.'tmp-'.$f2.'-data.fdf';
                $new_pdf = FCPATH.'assets/clients/'.$folder_number.'/'.$case_no.'/'.$f1.'.pdf';
                $files_are_equal = $this->files_are_equal($file_1, $file_2);
                if($files_are_equal['flag'] == 0) {
                    $key_array = $files_are_equal['key1'];
                    $value_array = $files_are_equal['value1'];
                    $output = shell_exec('pdftk '.$file_2_pdf.' generate_fdf output '.$tmp_fdf);
                    $file_contents = file_get_contents($tmp_fdf);
                    $tmp_static_content[0] = explode('/Fields [', $file_contents);
                    $tmp_static_content[1] = explode('endobj', $tmp_static_content[0][1]);
                    $content = $tmp_static_content[0][0] . "/Fields [\n";
                    foreach ($value_array as $key => $value) {
                        if($value == '\n') {
                            $value = '';
                        }
                        $value = str_replace('\/', '/', $value);
                        $content .= "<<\n/V (" . $value . ")\n/T (" . $key . ")\n>>\n";
                    }
                    $content .= "]\n>>\n>>\nendobj\n" . $tmp_static_content[1][1];
                    file_put_contents($tmp_fdf, $content);
                    $output = shell_exec('pdftk '.$file_2_pdf.' fill_form '.$tmp_fdf.' output '.$new_pdf);
                    
                    for($i = 0; $i <= 12; $i++) {
                        echo '<br>';
                    }
                    echo '<h2 style="text-align: center;">New PDF is generated!</h2>';
                    break;
                } else {
                    continue;
                }
            }
        }
        
        function files_are_equal($file_1, $file_2)
        {
            $key1_array = $value1_array = $key2_array = $value2_array = array();
            $res1 = $this->key_value_array($file_1);
            $key1_array = $res1['key'];
            $value1_array = $res1['value'];
            $res2 = $this->key_value_array($file_2);
            $key2_array = $res2['key'];
            $value2_array = $res2['value'];
        
            $flag = 0;
            foreach ($key1_array as $tmp_key => $tmp_value) {
                if($key2_array[$tmp_key] != $tmp_value) {
                    if($flag == 0) {
                        $flag = 1;
                    }
                }
            }
            $result = array(
                'flag' => $flag,
                'key1' => $key1_array,
                'key2' => $key2_array,
                'value1' => $value1_array,
                'value2' => $value2_array
            );
            return $result;
        }

        function key_value_array($file) {
            $result = array();
            $xml_file_contents = json_encode(file_get_contents($file));
            $exploded_content = explode('fields', $xml_file_contents);
            $xml_exploded = explode('<', $exploded_content[1]);
            foreach ($xml_exploded as $key => $value) {
                if(strpos($value, '>') > 0) {
                    $tmp_value = explode('>', $value);
                    $new_key = str_replace('\/', '', $tmp_value[0]);
                    if(!in_array($new_key, $key_array)) {
                        array_push($key_array, $new_key);
                        $value_array[$new_key] = $tmp_value[1];
                    }
                }
            }
            $result['key'] = $key_array;
            $result['value'] = $value_array;
            return $result;
        }
    }
?>