<?php

class Xmpp extends CI_Controller {

    function __construct() {
        parent::__construct();
        include APPPATH . 'third_party/XmppPrebind.php';
        include APPPATH . 'third_party/php-openfire-restapi-master/vendor/autoload.php';
    }

    function index($username = '', $password = 'welcome7') {

        $username = strtolower($this->session->userdata('user_data')['username']);

        $xmppPrebind = new XmppPrebind(XMPP_HOST, XMPP_BOSH_URL, XMPP_RESOURCE, XMPP_USE_SSL, XMPP_USE_DEBUG);
        $xmppPrebind->connect($username, $password);
        $xmppPrebind->auth();
        $sessionInfo = $xmppPrebind->getSessionInfo();

        $this->addRoster($username);
        //$this->addRoster();
        die(json_encode($sessionInfo));
    }

    function addRoster($fromuser) {
        $api = new Gidkom\OpenFireRestApi\OpenFireRestApi;

        $alreadyUser = array();
        $getUserRoster = $api->getRoster(strtolower($fromuser));

        if (isset($getUserRoster['message']->rosterItem)) {
            $rosterArray = $getUserRoster['message']->rosterItem;

            if (count($rosterArray) == 1) {
                array_push($alreadyUser, $rosterArray->jid);
            } else {
                foreach ($rosterArray as $v) {
                    array_push($alreadyUser, $v->jid);
                }
            }
        }

        $rec = $this->common_functions->getAllRecord('staff', 'initials', array('initials !=' => $fromuser));
        foreach ($rec as $key => $val) {

            if (!in_array(strtolower($val->initials . '@' . XMPP_HOST), $alreadyUser)) {
                $touser = $val->initials;

                $res = $api->addToRoster(strtolower($fromuser), strtolower($touser . '@' . XMPP_HOST), '', 3);
//                $res1 = $api->addToRoster(strtolower($touser), strtolower($fromuser.'@solulab.ddns.net'),'',3);
            }
        }

        return true;
        //echo json_encode($res);
    }

    function addNewUser($userArray) {
        // Create the Openfire Rest api object
        $api = new Gidkom\OpenFireRestApi\OpenFireRestApi;

        $username = strtolower($this->session->userdata('user_data')['username']);
        $user = $api->getUser('mom');


        // Add a new user to OpenFire and add to a group
        $result = $api->addUser('Username', 'Password', 'Real Name', 'johndoe@domain.com');

        // Check result if command is succesful
        if ($result['status']) {
            // Display result, and check if it's an error or correct response
            echo 'Success: ';
            echo $result['message'];
        } else {
            // Something went wrong, probably connection issues
            echo 'Error: ';
            echo $result['message'];
        }
    }

}

?>