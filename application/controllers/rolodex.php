<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rolodex extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('rolodex_model');
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $data = array();
        $this->data['menu'] = 'rolodex';
        $this->data['url_seg'] = $this->uri->segment(1);
    }

    function __destruct() {

    }

    public function index() {
        $this->output->enable_profiler(false);
        $this->data['pageTitle'] = 'Rolodex';

        $getCardType = $this->common_functions->getAllRecord('card', 'DISTINCT(type)', 'type != ""', 'type asc');

        $this->data['types'] = $getCardType;

        $this->data['languageList'] = $this->config->item('languageList');
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $this->data['saluatation'] = explode(',', $this->data['system_data'][0]->popsal);
        $this->data['suffix'] = explode(',', $this->data['system_data'][0]->rolosuffix);
        $this->data['title'] = explode(',', $this->data['system_data'][0]->poptitle);
        $this->data['category'] = explode(',', $this->data['system_data'][0]->popclient);
        $this->data['specialty'] = explode(',', $this->data['system_data'][0]->specialty);
        $this->data['pageTitle'] = 'Rolodex';
        $main1=json_decode($this->data['system_data'][0]->main1);
        $this->data['Venue'] = explode(',', $main1->venue);
        $this->load->view('common/header', $this->data);
        $this->load->view('rolodex/rolodex');
        //$this->load->view('common/common_modals');
        $this->load->view('common/contactmodal');
        //$this->load->view('common/commonjs.php');
        $this->load->view('common/footer');
    }

    public function addCardData() {

        $caseId = 0;
        if (isset($_POST['form_submit']) && $_POST['form_submit'] == 1) {
            $formdata = $_POST;
            //echo '<pre>'; print_r($formdata); exit;
            $whereflowupCond = "prospect_id ='" . $formdata['prospect_id'] . "'";
            $flowupdata = $this->common_functions->getRecordById('intake', 'fieldsudf', $whereflowupCond);
            $followupdatedata=  explode(',', $flowupdata->fieldsudf);


            if ($formdata['card_first'] == '' && $formdata['card2_firm'] == '') {
                $response = array('status' => 0, 'message' => 'Please enter First Name or Firm Name.');
            } else {
                $card2 = array();
                $card = array();
                $arr = array();
                foreach ($formdata as $key => $val) {
                    $keyVar = explode("_", $key);
                    if (count($keyVar) > 2) {
                        $keyVar[1] = $keyVar[1] . "_" . $keyVar[2];
                    }

                    if ($keyVar[0] == 'card') {
                        if ($keyVar[1] == 'notes' || $keyVar[1] == 'comments') {
                            $arr1 = array();
                            if ($keyVar[1] == 'notes') {
                                $arr1['notes'] = $val;
                            }
                            if ($keyVar[1] == 'comments') {
                                $arr1['comments'] = $val;
                            }
                            $arr[] = $arr1;
                        }
                        $card[$keyVar[1]] = $val;
                    }
                    if ($keyVar[0] == 'card2') {
                        $card2[$keyVar[1]] = $val;
                    }
                }

                $status = 0;
                //$card['firmcode'] = 0;
                $comment = json_encode($arr);

                $pos = array_key_exists('comments', $card);
                if ($pos !== FALSE) {
                    unset($card['notes']);
                    $card['comments'] = $comment;
                }

                /* if ($card2['firm'] != '' && $card2['address1'] != '' && $card2['city'] != '' && $card2['state'] != '' && $card2['zip'] != '') { */

                if ($this->session->userdata('user_data') != '') {
                    $card2['lastchg'] = $this->session->userdata('user_data')['initials'];
                    $card2['origchg'] = $this->session->userdata('user_data')['initials'];
                }
                $card2['origdt'] = date('Y-m-d H:i:s');
                $card2['lastdt'] = date('Y-m-d H:i:s');

                if ($card2['eamsref'] == '') {
                    $card2['eamsref'] = NULL;
                }

                //unset($card['firmcode']);
                if ($card['firmcode'] != 0) {
                    $where = array('firmcode' => $card['firmcode']);
                    $this->common_functions->editRecord('card2', $card2, $where);
                    $card['firmcode'] = $card['firmcode'];
                } else {
                    $card['firmcode'] = $this->common_functions->insertRecord('card2', $card2);
                }

                //$firmId = $this->common_functions->insertRecord('card2', $card2);
                //$card['firmcode'] = $firmId;
                $status = 1;
                //}
                if ($card['birth_date'] == '') {
                    $card['birth_date'] = NULL;
                } else {
                    $card['birth_date'] = date('Y-m-d 00:00:00', strtotime($card['birth_date']));
                }

                if ($formdata['date_of_hire'] == '') {
                    $card['date_of_hire'] = NULL;
                } else {
                    $card['date_of_hire'] = date('Y-m-d 00:00:00', strtotime($formdata['date_of_hire']));
                }

                unset($card['cardcode']);

                if ($this->session->userdata('user_data') != '') {
                    $card['lastchg'] = $this->session->userdata('user_data')['initials'];
                    $card['origchg'] = $this->session->userdata('user_data')['initials'];
                }
                $card['origdt'] = date('Y-m-d H:i:s');
                $card['lastdt'] = date('Y-m-d H:i:s');

                $cardId = $this->common_functions->insertRecord('card', $card);

                $defDetails = $this->common_functions->getRecordById('system', 'firmname,casetype,casestat,atty_resp,atty_hand,para_hand,sec_hand');
                $whereCond = "initials ='" . $this->session->userdata("user_data")["initials"] . "'";
                $staffdefDetails = $this->common_functions->getRecordById('staff', 'casetype,casestat,atty_resp,atty_hand,para_hand,sec_hand', $whereCond);
                if (!empty($staffdefDetails->atty_resp)) {
                    $atty_resp = $staffdefDetails->atty_resp;
                } elseif (!empty($defDetails->atty_resp)) {
                    $atty_resp = $defDetails->atty_resp;
                } else {
                    $atty_resp = '';
                }
                if (!empty($staffdefDetails->atty_hand)) {
                    $atty_hand = $staffdefDetails->atty_hand;
                } elseif (!empty($defDetails->atty_hand)) {
                    $atty_hand = $defDetails->atty_hand;
                } else {
                    $atty_hand = '';
                }
                if (!empty($staffdefDetails->para_hand)) {
                    $para_hand = $staffdefDetails->para_hand;
                } elseif (!empty($defDetails->para_hand)) {
                    $para_hand = $defDetails->para_hand;
                } else {
                    $para_hand = '';
                }
                if (!empty($staffdefDetails->sec_hand)) {
                    $sec_hand = $staffdefDetails->sec_hand;
                } elseif (!empty($defDetails->sec_hand)) {
                    $sec_hand = $defDetails->sec_hand;
                } else {
                    $sec_hand = '';
                }
                if ($formdata['prospect_id'] != '') {
                    $PncDetail = $this->common_functions->getRecordById('intake', 'casestat,casetype', ['prospect_id' => $_POST['prospect_id']]);
                    $PncCaseType = (isset($PncDetail->casetype)) ? $PncDetail->casetype : "";
                    $PncCaseSattus = (isset($PncDetail->casestat)) ? $PncDetail->casestat : "";
                }
                if ($formdata['prospect_id'] != '') {
                    $casetype = "PNC";
                } else {
                    if (!empty($staffdefDetails->casetype)) {
                        $casetype = $staffdefDetails->casetype;
                    } elseif (!empty($defDetails->casetype)) {
                        $casetype = $defDetails->casetype;
                    } else {
                        $casetype = '';
                    }
                }

//                if ($formdata['prospect_id'] != '') {
//                    $casestat = $PncCaseSattus;
//                } else {
                    if (!empty($staffdefDetails->casestat)) {
                        $casestat = $staffdefDetails->casestat;
                    } elseif (!empty($defDetails->casestat)) {
                        $casestat = $defDetails->casestat;
                    } else {
                        $casestat = '';
                    }
//                }

                if ($formdata['newRolodexCard'] == 1) {
                    $caseNO = $formdata['oldCaseNo'];
                    $casecard = array(
                        "caseno" => $caseNO,
                        "cardcode" => $cardId,
                        "type" => $formdata['card_type'],
                    );

                    $where1 = array('caseno' => $caseNO, 'cardcode' => $formdata['oldCardValue']);
                    $this->common_functions->editRecord('casecard', $casecard, $where1);
                    //echo $this->db->last_query(); exit;
                }
//               echo $followupdatedata[5];
               $datefollow = new DateTime($followupdatedata[5]);

               if ($formdata['new_case'] == 1) {
                    $newCase = array(
                        "casetype" => $casetype,
                        "casestat" => $casestat,
                        "atty_resp" => $atty_resp,
                        "atty_hand" => $atty_hand,
                        "para_hand" => $para_hand,
                        "sec_hand" => $sec_hand,
                        "followup" => $datefollow->format('Y-m-d H:i:s'),
                        "dateenter" => date("Y-m-d h:i:s"),
                        "caption1" => $formdata['card_first'] . " " . $formdata['card_last'] . " v. Straussner & Sherman"
                    );


                    $caseId = $this->common_functions->insertRecord('case', $newCase);
                    
                    $file_no = array('yourfileno' => $caseId);
                    $where = array('caseno' => $caseId);
                    $this->common_functions->editRecord('case', $file_no, $where);
                    
                    $casecard = array(
                        "caseno" => $caseId,
                        "cardcode" => $cardId,
                        "type" => $formdata['card_type'],
                        "orderno" => 1
                    );
                    $pnccaseupdate=$this->rolodex_model->updatepnccase($_POST['prospect_id'],$caseId);

                    $this->common_functions->insertRecord('casecard', $casecard);
//                    $rb=array('rb'=>$cardId);
//                    $where = array('caseno' =>$caseId);
//                    $this->common_functions->editRecord('case', $rb, $where);
                    
                    $keyArr = array(
                        "caseno" => $caseId,
                        "related" => 0,
                        "actno" => 0
                    );
                    $this->db->insert('casekey', $keyArr);
                    $ii = 1;
                    for ($i = 1000001; $i <= 1000021; $i++) {
                        if ($i == 1000021) {
                           /* $event = $formdata['card_type'] . " Entered Into Computer";*/
                            $event = "APPLICANT Entered Into Computer";
                        } else {
                            $event = '';
                        }
                        $caseActArr = array(
                            "actno" => $ii++,
                            "caseno" => $caseId,
                            "initials0" => $this->session->userdata('user_data')['username'],
                            'initials' => '',
                            "date" => date('Y-m-d H:i:s'),
                            'event' => $event,
                            'atty' => '',
                            'title' => '0',
                            'cost' => '0.00',
                            'minutes' => '0',
                            'rate' => '0',
                            'arap' => '1',
                            'mainnotes' => '0',
                            'redalert' => '0',
                            'upontop' => '0',
                            'type' => '',
                            "category" => $i,
                            'color' => '1',
                            'access' => '1',
                            'wordstyle' => '0',
                            'ole3style' => '0',
                            'frmtm' => '0.00',
                            'frmbm' => '0.00',
                            'frmlm' => '0.00',
                            'frmrm' => '0.00',
                            'frmwidth' => '0.00',
                            'frmheight' => '0.00',
                            'orient' => '0',
                            'copies' => '0',
                            'typeact' => '0',
                            'bistyle' => '0',
                            'biteamno' => '0',
                            'bicycle' => '1899-12-30 00:00:00',
                            'bitime' => '0',
                            'bihourrate' => '0',
                            'bifee' => '0.00',
                            'bicost' => '0.00',
                            'bidesc' => '',
                            'bicheckno' => '',
                            'bipmt' => '0.00',
                            'bipmtdate' => '1899-12-30 00:00:00',
                            'bipmtduedt' => '1899-12-30 00:00:00',
                            'bilatefee' => '0.00'
                        );
                        $this->common_functions->insertRecord('caseact', $caseActArr);
                    }
                }
                //update intake(prospect) when case add from prospect
                if ($formdata['prospect_id'] != '') {
                    $case_no = array('caseno' => $caseId);
                    $where = array('prospect_id' => $formdata['prospect_id']);
                    $this->common_functions->editRecord('intake', $case_no, $where);
                    $whereCond = "prospect_id ='" . $formdata['prospect_id'] . "'";
                    $ProN = $this->common_functions->getRecordById('intake', 'prosno', $whereCond);
                    $PN = $ProN->prosno;
                    $where1 = array('prosno' => $PN);
                    $this->common_functions->editRecord('tasks', $case_no, $where1);
                   // echo $this->db->last_query(); exit;
                }


                $status = 1;

                if ($status == 1) {
                    $response = array('status' => 1, 'message' => 'Record successfully inserted!', "caseid" => $caseId, "editcardid" => $cardId);
                } else {
                    $response = array('status' => 0, 'message' => 'Please fill in all required fields');
                }
            }

            echo json_encode($response);
        }
    }

    public function editCardData() {
        if (isset($_POST['form_submit']) && $_POST['form_submit'] == 1) {
            $formdata = $_POST;

//            echo '<pre>'; print_r($_POST);exit;
//            print_r($_FILES);


            $card2 = array();
            $card = array();
            $arr = array();
            foreach ($formdata as $key => $val) {


                $keyVar = explode("_", $key);
                if (count($keyVar) > 2) {
                    $keyVar[1] = $keyVar[1] . "_" . $keyVar[2];
                }
                if ($keyVar[0] == 'card') {
                    if ($keyVar[1] == 'notes' || $keyVar[1] == 'comments') {
                        $arr1 = array();
                        if ($keyVar[1] == 'notes') {
                            $arr1['notes'] = $val;
                        }
                        if ($keyVar[1] == 'comments') {
                            $arr1['comments'] = $val;
                        }
                        $arr[] = $arr1;
                    }
                    $card[$keyVar[1]] = $val;
                }
                if ($keyVar[0] == 'card2') {
                    $card2[$keyVar[1]] = $val;
                }
            }
            $comment = json_encode($arr);
            // echo $comment;exit;
            $pos = array_key_exists('comments', $card);
            if ($pos !== FALSE) {
                unset($card['notes']);
                $card['comments'] = $comment;
            }

            if ($card2['firm'] != '' && $card2['address1'] != '' && $card2['city'] != '' && $card2['state'] != '' && $card2['zip'] != '') {
                if ($this->session->userdata('user_data') != '') {
                    $card2['lastchg'] = $this->session->userdata('user_data')['initials'];
                }
                $card2['lastdt'] = date('Y-m-d H:i:s');

                if ($card2['eamsref'] == '') {
                    $card2['eamsref'] = NULL;
                }

                if ($card['firmcode'] != 0) {
                    $where = array('firmcode' => $card['firmcode']);
                    $this->common_functions->editRecord('card2', $card2, $where);
                    $card['firmcode'] = $card['firmcode'];
                } else {
                    if ($this->session->userdata('user_data') != '') {
                        $card2['origchg'] = $this->session->userdata('user_data')['initials'];
                    }
                    $card2['origdt'] = date('Y-m-d H:i:s');
                    $card['firmcode'] = $this->common_functions->insertRecord('card2', $card2);
                }
            }

            if ($card2['eamsref'] == '') {
                $card2['eamsref'] = NULL;
            }

            if ($card['firmcode'] != 0) {
                $where = array('firmcode' => $card['firmcode']);
                $this->common_functions->editRecord('card2', $card2, $where);
                $card['firmcode'] = $card['firmcode'];
            } else {
                $card2['origchg'] = $this->session->userdata('user_data')['initials'];
                $card2['origdt'] = date('Y-m-d H:i:s');
                $card['firmcode'] = $this->common_functions->insertRecord('card2', $card2);
            }

            if ($this->session->userdata('user_data') != '') {
                $card['lastchg'] = $this->session->userdata('user_data')['initials'];
            }

            if ($card['birth_date'] == '') {
                $card['birth_date'] = NULL;
            } else {
                $card['birth_date'] = date('Y-m-d 00:00:00', strtotime($card['birth_date']));
            }

            if ($formdata['date_of_hire'] == '') {
                $card['date_of_hire'] = NULL;
            } else {
                $card['date_of_hire'] = date('Y-m-d 00:00:00', strtotime($formdata['date_of_hire']));
            }

            $card['lastdt'] = date('Y-m-d H:i:s');

            if ($formdata['newRolodexCard'] == 1) {
                unset($card['cardcode']);
                $card['origdt'] = date('Y-m-d H:i:s');
                $card['origchg'] = $this->session->userdata('user_data')['initials'];
                $cardId = $this->common_functions->insertRecord('card', $card);
                $caseNO = $formdata['oldCaseNo'];
                $casecard = array(
                    "caseno" => $caseNO,
                    "cardcode" => $cardId,
                    "type" => $formdata['card_type'],
                );

                $caseRefBy = array(
                    "rb" => $cardId,
                );


                $caseid = $cardId;

                $where1 = array('caseno' => $caseNO, 'cardcode' => $formdata['oldCardValue']);
                $this->common_functions->editRecord('casecard', $casecard, $where1);
                $where1 = array('caseno' => $caseNO, 'rb' => $formdata['oldCardValue']);
                $this->common_functions->editRecord('case', $caseRefBy, $where1);
                //echo $this->db->last_query(); exit;
            } else {
                $where1 = array('cardcode' => $card['cardcode']);
                $caseid = $card['cardcode'];
                unset($card['cardcode']);
                $this->common_functions->editRecord('card', $card, $where1);
            }

            $response = array('status' => 1, 'message' => 'Record Edited Successfully !!!', 'caseid' => $caseid, "editcardid" => $caseid);
        }
        echo json_encode($response);
    }

    public function getRolodoxAjaxData() {
        extract($_POST);

        $filterArray = array();

        $orCondition = '';
        if ($search['value'] != '') {

            parse_str($search['value'], $searcharray);

            if (isset($searcharray['searchByFirmName']) && $searcharray['searchByFirmName'] != '') {
                $filterArray['cd2.firm' . ' like'] = $searcharray['searchByFirmName'] . "%";
            }
            if (isset($searcharray['searchByFirstName']) && $searcharray['searchByFirstName'] != '') {
                $filterArray['cd.first' . ' like'] = $searcharray['searchByFirstName'] . "%";
            }
            if (isset($searcharray['searchByLastName']) && $searcharray['searchByLastName'] != '') {
                $filterArray['cd.last' . ' like'] = $searcharray['searchByLastName'] . "%";
            }
            if (isset($searcharray['searchByEmailId']) && $searcharray['searchByEmailId'] != '') {
                $filterArray['cd.email' . ' like'] = $searcharray['searchByEmailId'] . "%";
            }
            if (isset($searcharray['searchBySSNo']) && $searcharray['searchBySSNo'] != '') {
                $filterArray['cd.social_sec' . ' like'] = "%" . $searcharray['searchBySSNo'] . "%";
            }
            if (isset($searcharray['searchByCardNumber']) && $searcharray['searchByCardNumber'] != '') {
                if ($searcharray['cardRel'] == 'less') {
                    $filterArray['cd.cardcode <'] = $searcharray['searchByCardNumber'];
                } else if ($searcharray['cardRel'] == 'more') {
                    $filterArray['cd.cardcode >'] = $searcharray['searchByCardNumber'];
                } else {
                    $filterArray['cd.cardcode' . ' like'] = $searcharray['searchByCardNumber'] . "%";
                }
            }

            if (isset($searcharray['searchByContactType']) && $searcharray['searchByContactType'] != '') {
                if ($searcharray['searchByContactType'] == 'individual') {
                    //$filterArray['cd.first !='] = '';
                    $filterArray['cd2.firm'] = '';
                } else if ($searcharray['searchByContactType'] == 'firm') {
                    $filterArray['cd2.firm !='] = '';
                }
            }

            if (isset($searcharray['searchByLicenseNo']) && $searcharray['searchByLicenseNo'] != '') {
                $filterArray['cd.licenseno' . ' like '] = "%" . $searcharray['searchByLicenseNo'] . "%";
            }
            if (isset($searcharray['searchByPhoneNumber']) && $searcharray['searchByPhoneNumber'] != '') {
                $orCondition = '(cd2.phone1 like "%' . $searcharray['searchByPhoneNumber'] . '%" OR cd.home like "%' . $searcharray['searchByPhoneNumber'] . '%" OR cd.business like "%' . $searcharray['searchByPhoneNumber'] . '%" OR cd.fax like "%' . $searcharray['searchByPhoneNumber'] . '%" OR cd.car like "%' . $searcharray['searchByPhoneNumber'] . '%" OR cd.beeper like "%' . $searcharray['searchByPhoneNumber'] . '%" )';
            }

            if (isset($searcharray['start_birthdate']) && $searcharray['start_birthdate'] != '') {
                $filterArray['cd.birth_date  >='] = date('Y-m-d 00:00:00', strtotime($searcharray['start_birthdate']));
            }

            if (isset($searcharray['end_birthdate']) && $searcharray['end_birthdate'] != '') {
                $filterArray['cd.birth_date  <='] = date('Y-m-d 23:59:59', strtotime($searcharray['end_birthdate']));
            }

            if (isset($searcharray['searchCardType']) && $searcharray['searchCardType'] != '') {
                $filterArray['cd.type'] = $searcharray['searchCardType'];
            }
        }

        $result = $this->rolodex_model->getRolodexListing($start, $length, $filterArray, $orCondition, $order);



        $finalData = array();
        foreach ($result as $data) {

            $date = ($data->birth_date != '' && $data->birth_date != '1899-12-30 00:00:00' && $data->birth_date != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($data->birth_date)) : '';
            // $casecnt = $this->rolodex_model->getCaseCount($data->cardcode);
            $first_last = '';
            if($data->last != '') {
                $first_last = $data->last . ", " . $data->first;
            } else {
                $first_last = $data->first;
            }
            $tmpArr = array(
                $data->cardcode,
                $first_last,
                $data->caseCount,
                $data->city,
                $data->firm,
                $data->home,
                $data->phone1,
                $data->business,
                $data->social_sec,
                $date,
                $data->type,
                $data->licenseno,
                $data->specialty,
                $data->cardcode,
            );


            array_push($finalData, $tmpArr);
        }
        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $this->rolodex_model->getRolodexCount($filterArray, $orCondition),
            'recordsFiltered' => $this->rolodex_model->getRolodexCount($filterArray, $orCondition),
            'data' => $finalData
        );

        echo json_encode($listingData);
    }

    public function getEditRolodex() {
        extract($_POST);

        $this->data['languageList'] = $this->config->item('languageList');
//        $this->data['saluatation'] = $this->config->item('saluatation');
//        $this->data['suffix'] = $this->config->item('suffix');
//        $this->data['title'] = $this->config->item('title');
//        $this->data['category'] = $this->config->item('category');
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $this->data['saluatation'] = explode(',', $this->data['system_data'][0]->popsal);
        $this->data['suffix'] = explode(',', $this->data['system_data'][0]->rolosuffix);
        $this->data['title'] = explode(',', $this->data['system_data'][0]->poptitle);
        $this->data['category'] = explode(',', $this->data['system_data'][0]->popclient);
        $this->data['specialty'] = explode(',', $this->data['system_data'][0]->specialty);
        $record = $this->rolodex_model->getRolodexRecord($cardcode);

        $record->birth_date = isset($record->birth_date) ? $this->common_functions->checkDateIsNull($record->birth_date) : 'NA';
        $record->date_of_hire = isset($record->date_of_hire) ? $this->common_functions->checkDateIsNull($record->date_of_hire) : 'NA';
        $record->card_origdt = isset($record->card_origdt) ? $this->common_functions->checkDateIsNull($record->card_origdt) : 'NA';
        $record->card_lastdt = isset($record->card_lastdt) ? $this->common_functions->checkDateIsNull($record->card_lastdt) : 'NA';
        $record->card2_origdt = isset($record->card2_origdt) ? $this->common_functions->checkDateIsNull($record->card2_origdt) : 'NA';
        $record->card2_lastdt = isset($record->card2_lastdt) ? $this->common_functions->checkDateIsNull($record->card2_lastdt) : 'NA';

        $record->card_origchg = ($record->card_origchg != '') ? $record->card_origchg : 'NA';
        $record->card_lastchg = ($record->card_lastchg != '') ? $record->card_lastchg : 'NA';
        $record->card2_origchg = ($record->card2_origchg != '') ? $record->card2_origchg : 'NA';
        $record->card2_lastchg = ($record->card2_lastchg != '') ? $record->card2_lastchg : 'NA';

        /* $record['birth_date'] = isset($record['birth_date']) ? $this->common_functions->checkDateIsNull($record['birth_date']) : 'NA';
          $record['card_origdt'] = isset($record['card_origdt']) ? $this->common_functions->checkDateIsNull($record['card_origdt']) : 'NA';
          $record['card_lastdt'] = isset($record['card_lastdt']) ? $this->common_functions->checkDateIsNull($record['card_lastdt']) : 'NA';
          $record['card2_origdt'] = isset($record['card2_origdt']) ? $this->common_functions->checkDateIsNull($record['card2_origdt']) : 'NA';
          $record['card2_lastdt'] = isset($record['card2_lastdt']) ? $this->common_functions->checkDateIsNull($record['card2_lastdt']) : 'NA';

          $record['card_origchg'] = ($record['card2_lastdt'] !='') ? $record['card2_lastdt'] : 'NA';
          $record['card_lastchg'] = ($record['card_lastchg'] !='') ? $record['card_lastchg'] : 'NA';
          $record['card2_origchg'] = ($record['card2_origchg'] !='') ? $record['card2_origchg'] : 'NA';
          $record['card2_lastchg'] = ($record['card2_lastchg'] !='') ? $record['card2_lastchg'] : 'NA'; */

        if (isset($record->picture) && file_exists('assets/rolodexprofileimages/' . $record->picture)) {
            $record->picturePath = base_url('assets/rolodexprofileimages/' . $record->picture);
        } else {
            $record->picturePath = base_url('assets/profileimages/users.png');
        }

        echo json_encode($record);
    }

    public function deleteRec() {
        extract($_POST);

        $cond = array('cardcode' => $cardcode);

        $res = $this->common_functions->getRecordById('card', 'firmcode', $cond);

        /*if ($res->firmcode > 0) {
            $cond1 = array('firmcode' => $res->firmcode);
            $res = $this->common_functions->deleteRec('card2', $cond1);
        }*/

        $res = $this->common_functions->deleteRec('card', $cond);

        echo $res;
    }

    public function checkCase() {
        extract($_POST);

        $cond = array('cardcode' => $cardcode);

        $res = $this->common_functions->getRecordById('casecard', 'count(*) AS CNT', $cond);
        echo $res->CNT;
    }

    public function editRolodexdetail() {
        extract($_POST);
        $casecard = $this->common_functions->getCardSpecific($cardcode, $caseNO);
        if ($casecard > 0) {
            //$case_details = $this->common_functions->getCaseFullDetails($caseNO,$cardcode);
            $case_details = $this->rolodex_model->getRolodexRecord($cardcode);

            if ($status == 'Yes') {
                $response = array('status' => 0, 'data' => $case_details);
            } else {
                $casecard1 = $this->common_functions->getCardSpecificdata($cardcode, $caseNO);
                $response = array('status' => 0, 'data' => $case_details, 'message' => 'This Card  is Currently Attached to' . $casecard1->caseno . 'cases changes to this card changes will affect' . $casecard1->caseno . '. Tf you want changes deatail this card from that case  at the parties sheet and attach another card');
            }
            echo json_encode($response);
        } else {
            $record = $this->rolodex_model->getRolodexRecord($cardcode);
            $record->birth_date = isset($record->birth_date) ? $this->common_functions->checkDateIsNull($record->birth_date) : 'NA';
            $record->card_origdt = isset($record->card_origdt) ? $this->common_functions->checkDateIsNull($record->card_origdt) : 'NA';
            $record->card_lastdt = isset($record->card_lastdt) ? $this->common_functions->checkDateIsNull($record->card_lastdt) : 'NA';
            $record->card2_origdt = isset($record->card2_origdt) ? $this->common_functions->checkDateIsNull($record->card2_origdt) : 'NA';
            $record->card2_lastdt = isset($record->card2_lastdt) ? $this->common_functions->checkDateIsNull($record->card2_lastdt) : 'NA';

            $record->card_origchg = ($record->card2_lastdt != '') ? $record->card2_lastdt : 'NA';
            $record->card_lastchg = ($record->card_lastchg != '') ? $record->card_lastchg : 'NA';
            $record->card2_origchg = ($record->card2_origchg != '') ? $record->card2_origchg : 'NA';
            $record->card2_lastchg = ($record->card2_lastchg != '') ? $record->card2_lastchg : 'NA';
            if (isset($record->picture) and file_exists('assets/rolodexprofileimages/' . $record->picture)) {
                $record->picturePath = base_url('assets/rolodexprofileimages/' . $record->picture);
            } else {
                $record->picturePath = base_url('assets/profileimages/users.png');
            }
            $response = array('status' => 1, 'data' => $record);
            echo json_encode($response);
        }
    }

    function updateprofilepic() {
        $this->load->library('upload');
        $nwfile = explode(".", $_FILES["file"]['name']);
        $ext = end($nwfile);
        $cardCode = $this->uri->segment(3);
        $oldImg = $this->uri->segment(4);
        if (!is_dir(FCPATH . 'assets/rolodexprofileimages')) {
            mkdir(FCPATH . 'assets/rolodexprofileimages', 0777);
        }

        $config['upload_path'] = trim(FCPATH . '/assets/rolodexprofileimages/'); //Use relative or absolute path

        $new_name = time() . "." . $ext;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = trim($new_name);
        $config['max_size'] = '4096000'; // Can be set to particular file size , here it is 2 MB(2048 Kb)
        //  $config['max_width'] = '1024';
        // $config['max_height'] = '768';
        $config['overwrite'] = FALSE;
        //If the file exists it will be saved with a progressive number appended
        //Initialize
        $this->upload->initialize($config);
        if (!$this->upload->do_upload("file")) {
            echo $this->upload->display_errors();
            exit;
        } else {
            $exists = $this->rolodex_model->rolodex_profile($new_name, $cardCode);
            if ($this->uri->segment(4) != '') {
                $file_located = trim(FCPATH . 'assets/rolodexprofileimages/' . $this->uri->segment(4));
                if (file_exists($file_located)) {
                    unlink($file_located);
                }
            }
            echo $new_name;
        }
    }

    public function getCaseData() {
        extract($_POST);
        //echo '<pre>'; print_r($_POST); exit;
        $filterArray = array();

        $filterArray['cc.cardcode'] = $cardcode;
        $orCondition = array();
        if ($search['value'] != '') {
            $orCondition['c.caseno' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['cd.first' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['c.atty_resp' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['c.atty_hand' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['c.casetype' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['c.casestat' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['c.yourfileno' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['cd.social_sec' . ' like'] = "'%" . $search['value'] . "%'";
            $orCondition['cc.type' . ' like'] = "'%" . $search['value'] . "%'";
        }
        $result = $this->rolodex_model->getCaseListing($start, $length, $filterArray, $orCondition, $order);

//       $result = $result[0];
//        print_r($result);
//        exit;

        $finalData = array();
        foreach ($result as $data) {
            $link = '<a href="javascript:void(0)" data-card="' . $data->cardcode . '" data-case="' . $data->caseno . '" class="viewDetails">' . $data->caseno . '</a>';
            $link2 = '<a href="'.base_url().'cases/case_details/'.$data->caseno.'" target="_blank" class="btn btn-circle-action btn-circle btn-info" ><i class="icon fa fa-eye"></i></a>';;
            $tmpArr = array(
                $link,
                $data->first . " " . $data->last,
                $data->atty_resp,
                $data->atty_hand,
                $data->casetype,
                $data->cardtype,
                $data->casestat,
                $data->yourfileno,
                $data->social_sec,
                $link2,
                $data->caseno,
            );


            array_push($finalData, $tmpArr);
        }
        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $this->rolodex_model->getCaseCount($filterArray, $orCondition),
            'recordsFiltered' => $this->rolodex_model->getCaseCount($filterArray, $orCondition),
            'data' => $finalData
        );

        echo json_encode($listingData);
    }

    public function getCaseAjaxData() {
        extract($_POST);
        $result = $this->rolodex_model->getCaseDetails($caseno, $cardcode);
        echo json_encode($result);
    }
    
    public function checkDuplicateRolodex() {
        $res = $this->rolodex_model->checkDuplicateRolodex($this->input->post('fname'), $this->input->post('lname'));
        if (!empty($res)) {
            echo json_encode(array("status" => 1,"data" => $res));
        } else {
            echo json_encode(array("status" => 0, "data" => []));
        }
    }

}
