<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $this->load->model('Report_model');

        $data = array();
        $this->data['menu'] = 'reports';
        $this->data['loginUser'] = $this->session->userdata('user_data')['username'];
    }

    function __destruct() {
        
    }

    //*************************************************//
    // * @name   : index
    // * @todo   : 
    // * @Date   : 22-May-2018
    //************************************************//
    public function index() {

        $this->load->model('common_model');
        $this->load->model('case_model');
        $this->data['pageTitle'] = 'Reports';
        $this->data['months'] = $this->common_functions->get_month_names();
        $this->data['reportName'] = json_decode(report_name, true);
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $filters = $this->case_model->getFilterArray($this->data['system_data']);
        $this->data['filters'] = $filters;

//        //GET LIST OF CASE TYPE
//        $getCaseType = $this->common_model->getAllRecord('case', 'DISTINCT(casetype)', 'casetype != ""', 'casetype asc');
//        $this->data['casetype'] = $getCaseType;
//
//
//        //GET LIST OF CASE STATUS
//        $getCaseStat = $this->common_model->getAllRecord('case', 'DISTINCT(casestat)', 'casestat != ""', 'casestat asc');
//        $this->data['casestat'] = $getCaseStat;
//
//        //GET LIST OF CARD TYPES
//        $getCardType = $this->common_model->getAllRecord('casecard', 'DISTINCT(type)', 'type != ""', 'type ASC');
//        $this->data['cardType'] = $getCardType;
//
//        //GET ATTORNEY HAND
//        $getAttyHand = $this->common_model->getAllRecord('case', 'DISTINCT(atty_hand)', 'atty_hand != ""', 'atty_hand asc');
//        $this->data['atty_hand'] = $getAttyHand;
//
//        //GET ATTORNEY RESPONSE
//        $getAttyResp = $this->common_model->getAllRecord('case', 'DISTINCT(atty_resp)', 'atty_resp != ""', 'atty_resp asc');
//        $this->data['atty_resp'] = $getAttyResp;
//
//        //GET PARA HAND
//        $getParaHand = $this->common_model->getAllRecord('case', 'DISTINCT(para_hand)', 'para_hand != ""', 'para_hand ASC');
//        $this->data['para_hand'] = $getParaHand;
//
//        //GET SEC HAND
//        $getSecHand = $this->common_model->getAllRecord('case', 'DISTINCT(sec_hand)', 'sec_hand != ""', 'sec_hand ASC');
//        $this->data['sec_hand'] = $getSecHand;

        //GET CASE CATEGORY
        $getCaseCategory = $this->common_model->getAllRecord('actdeflt', 'actname,category', '', 'category asc');
        $this->data['category'] = $getCaseCategory;

        $this->data['Stafflist'] = $this->common_model->getAllRecord('staff', 'DISTINCT(initials)', '', 'initials asc');


        $this->load->view('common/header', $this->data);
        $this->load->view('report/report');
        $this->load->view('common/footer');
        $this->load->view('common/casesmodals');
    }

    //*************************************************//
    // * @name   : getFilterCasecount
    // * @todo   : Get data of case count report by filter
    // * @Date   : 22-May-2018
    //************************************************//

    public function getFilterCasecount() {

        extract($_POST);
        $filterArray = array();

        if (isset($searchData) && !empty($searchData)) {
            foreach ($searchData as $val) {
                if ($val['name'] == 'isType') {
                    $filterArray['isType'] = (isset($val['value'])) ? $val['value'] : "";
                }
                if ($val['name'] == 'searchBy') {
                    $filterArray['searchBy'] = (isset($val['value'])) ? $val['value'] : "";
                }
                if ($val['name'] == 'month') {
                    $filterArray['month'] = (isset($val['value'])) ? $val['value'] : "";
                }
                if ($val['name'] == 'year') {
                    $filterArray['year'] = (isset($val['value'])) ? $val['value'] : "";
                }
                if ($val['name'] == 'types') {
                    $filterArray['types'] = (isset($val['value'])) ? $val['value'] : "";
                }
                if ($val['name'] == 'unassign_attorneys') {
                    $filterArray['unassign_attorneys'] = (isset($val['value'])) ? $val['value'] : "";
                }
            }
        }

        if (isset($filterArray['types']) && $filterArray['isType'] == 'byMonth') {
            unset($filterArray['year']);
            unset($filterArray['types']);
            unset($filterArray['isType']);
            $CasecountReport = $this->Report_model->getCasecountReport($filterArray, $start, $length, $order);
        } else if (isset($filterArray['c.types']) && $filterArray['c.isType'] == 'byType') {
            unset($filterArray['c.isType']);
        } else {
            
        }
        exit;
        $count = $this->report_model->getFilterMarkupReportCount($filterArray);
        $totalCount = $this->report_model->getFilterMarkupReportCount();

//        echo "<pre>";
//        print_r($basicCaseReport);exit;

        $report = array();
        if (count($basicCaseReport) > 0) {
            foreach ($basicCaseReport as $key => $rec) {
                $quickNote = $this->common_functions->getQuickNote($rec->caseno);
                $datetime = '';

                $nextCalEvent = '';
                $client = '';
                if (isset($quickNote->last) && $quickNote->last != '') {
                    $client .= $quickNote->last;
                }
                if (isset($quickNote->first) && $quickNote->first != '') {
                    $client .= ($client != '') ? ', ' . $quickNote->first : $quickNote->first;
                }

                $notes = isset($quickNote->event) ? $quickNote->event : '';


                $rec->date = $this->common_functions->checkDateIsNull($rec->date);
                $rec->date = ($rec->date != '') ? date('m/d/Y H:i:s', strtotime($rec->date)) : '';

                $rec->followup = $this->common_functions->checkDateIsNull($rec->followup);
                $rec->followup = ($rec->followup != '') ? date('m/d/Y', strtotime($rec->followup)) : '';

                $rec->dateopen = $this->common_functions->checkDateIsNull($rec->dateopen);
                $rec->dateopen = ($rec->dateopen != '') ? date('m/d/Y', strtotime($rec->dateopen)) : '';

                if (isset($nextEvent->date)) {
                    $datetime = date('m/d/Y H:i:s', strtotime($nextEvent->date));
                    $nextCalEvent = $nextEvent->event;
                }
                $venueCard = '';
                $venueCal = '';
                $udf = '';
                $attyAssign = '';

                $tempArr = array(
                    $rec->caseno,
                    $rec->followup,
                    $client,
                    $notes,
                    $rec->date,
                    $rec->event,
                    $venueCard,
                    $venueCal,
                    $rec->casetype,
                    $rec->casestat,
                    $rec->atty_resp,
                    $rec->atty_hand,
                    $rec->para_hand,
                    $rec->caption1,
                    $rec->dateopen,
                    $udf,
                    $rec->yourfileno,
                    $rec->attyass
                );

                array_push($report, $tempArr);
            }
        }
        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $report
        );

        echo json_encode($listingData);
    }

    //*************************************************//
    // * @name   : getFilterCaseReport
    // * @todo   : Get data of case report by filter
    // * @Date   : 22-May-2018
    //************************************************//

    public function getFilterCaseReport() {
        extract($_POST);

        $filterArray = array();
        $orCondition = '';
        $searcharray = array();
        $shortdefault = "";
        $CaseUnique = "";

        $filterArray['c.case_status'] = '1';
        if (isset($_POST['searchData']) && !empty($_POST['searchData'])) {
            foreach ($_POST['searchData'] as $val) :
                if (isset($val['value']) && $val['value'] != "") {

                    if ($val['name'] == 'casetype') {
                        $searcharray['casetype'] = $val['value'];
                    }
                    if ($val['name'] == 'case_status') {
                        $searcharray['case_status'] = $val['value'];
                    }
                    if ($val['name'] == 'card_type') {
                        $searcharray['card_type'] = $val['value'];
                    }
                    if ($val['name'] == 'searchBy') {
                        $searcharray['searchBy'] = $val['value'];
                    }
                    if ($val['name'] == 'searchText') {
                        $searcharray['searchText'] = $val['value'];
                        $shortdefault = $searcharray['searchText'];
                    }
                    if ($val['name'] == 'atty_resp') {
                        $searcharray['atty_resp'] = $val['value'];
                    }
                    if ($val['name'] == 'atty_hand') {
                        $searcharray['atty_hand'] = $val['value'];
                    }
                    if ($val['name'] == 'para_hand') {
                        $searcharray['para_hand'] = $val['value'];
                    }
                    if ($val['name'] == 'sec_hand') {
                        $searcharray['sec_hand'] = $val['value'];
                    }
                    if ($val['name'] == 'primary_news') {
                        $searcharray['primary_news'] = $val['value'];
                    }
                    if ($val['name'] == 'close_window') {
                        $searcharray['close_window'] = $val['value'];
                    }
                    if ($val['name'] == 'deleted_cases') {
                        $searcharray['deleted_cases'] = $val['value'];
                    }
                    if ($val['name'] == 'Ctable_unique') {
                        $searcharray['Ctable_unique'] = $val['value'];
                    }
                    if ($val['name'] == 'primary_names') {
                        $searcharray['primary_names'] = $val['value'];
                    }
                }
            endforeach;
        }

        if (isset($searcharray['searchBy']) && $searcharray['searchBy'] != '' && isset($searcharray['searchText']) && $searcharray['searchText'] != '') {
            $isShortcut = false;
            $shortTxt = '';
            if ($searcharray['searchBy'] == 'shortcuts') {
                $shortArr = explode('-', $searcharray['searchText']);

                if (count($shortArr) > 1) {
                    if (count($shortArr) > 2) {
                        $shortArr[1] = trim($shortArr[1]) . "-" . trim($shortArr[2]);
                    }
                    $searcharray = $this->getSearchCase($shortArr);
                } else {
                    $searcharray['searchBy'] = 'cd.last';
                    $searcharray['searchText'] = $shortArr[0];
                }
            }
            if ($searcharray['searchBy'] == 'cd.birth_date') {
                $searchContent = explode('-', $searcharray['searchText']);
                $filterArray['cd.birth_date >='] = date('Y-m-d 00:00:00', strtotime($searchContent[0]));
                if (count($searchContent) > 1) {
                    $filterArray['cd.birth_date <='] = date('Y-m-d 23:59:59', strtotime($searchContent[1]));
                } else {
                    $filterArray['cd.birth_date <='] = date('Y-m-d 23:59:59', strtotime($searchContent[0]));
                }
            } else if ($searcharray['searchBy'] == 'cd.contact') {
                $orCondition = '(cd.home like "%' . $searcharray['searchText'] . '%" OR cd.business like "%' . $searcharray['searchText'] . '%" OR cd.fax like "%' . $searcharray['searchText'] . '%" OR cd.car like "%' . $searcharray['searchText'] . '%")';
            } else if ($searcharray['searchBy'] == 'cd.birth_month') {
                if ($searcharray['searchText'] != '0') {
                    $filterArray['MONTH(cd.birth_date)'] = $searcharray['searchText'];
                }
            } else if ($searcharray['searchBy'] == 'c.followup' || $searcharray['searchBy'] == 'c.dateenter' || $searcharray['searchBy'] == 'c.dateopen' || $searcharray['searchBy'] == 'c.dateclosed') {
                if (strpos($searcharray['searchText'], "+") !== false) {
                    $filterArray[$searcharray['searchBy'] . ' >='] = date('Y-m-d 00:00:00', strtotime(trim(str_replace("+", "", $searcharray['searchText']))));
                } else if (strpos($searcharray['searchText'], "-") !== false) {
                    $filterArray[$searcharray['searchBy'] . ' <='] = date('Y-m-d 23:59:59', strtotime(trim(str_replace("-", "", $searcharray['searchText']))));
                } else if (strpos($searcharray['searchText'], "to") !== false) {
                    $searchContent = explode('to', $searcharray['searchText']);
                    $filterArray[$searcharray['searchBy'] . ' >='] = date('Y-m-d 00:00:00', strtotime(trim($searchContent[0])));
                    $filterArray[$searcharray['searchBy'] . ' <='] = date('Y-m-d 23:59:59', strtotime(trim($searchContent[1])));
                } else {
                    $filterArray['DATE(' . $searcharray['searchBy'] . ')'] = date('Y-m-d', strtotime($searcharray['searchText']));
                }
            } else if ($searcharray['searchBy'] == 'c.caseno_less') {
                $filterArray['c.caseno <'] = $searcharray['searchText'];
            } else if ($searcharray['searchBy'] == 'c.caseno_more') {
                $filterArray['c.caseno >'] = $searcharray['searchText'];
            } else if ($searcharray['searchBy'] == 'c.caseno') {
                $filterArray[$searcharray['searchBy']] = $searcharray['searchText'];
            } else if ($searcharray['searchBy'] == 'cd.last') {
                $lastNameArray = explode(",", $searcharray['searchText']);
                $lastName = $lastNameArray[0];
                $filterArray['cd.last like '] = trim($lastName) . "%";

                if (count($lastNameArray) > 1) {
                    $filterArray['cd.first like'] = trim($lastNameArray[1]) . "%";
                }
            } else if ($searcharray['searchBy'] == 'cd.first') {
                $firstNameArray = explode(",", $searcharray['searchText']);

                $firstName = $firstNameArray[0];
                $filterArray['cd.first like '] = trim($firstName) . "%";

                if (count($firstNameArray) > 1) {
                    $filterArray['cd.last like'] = trim($firstNameArray[1]) . "%";
                }
            } elseif ($searcharray['searchBy'] == "cd.social_sec") {
                $filterArray['cd.social_sec'] = $searcharray['searchText'];
            } else {
                $filterArray[$searcharray['searchBy'] . ' like'] = $searcharray['searchText'] . "%";
            }
        }

        if (isset($searcharray['casetype']) && $searcharray['casetype'] != '') {
            $searchArrtp = explode('-', $searcharray['casetype']);
            $searchTp = $searchArrtp[0];
            $searchTpVal = $searchArrtp[1];
            if ($searchTp != 'type')
                $filterArray['c.' . $searchTp] = $searchTpVal;
            else
                $filterArray['cd.' . $searchTp] = $searchTpVal;
        }
        if (isset($searcharray['atty_resp']) && $searcharray['atty_resp'] != 'atty_resp-' && $searcharray['atty_resp'] != '') {
            $searchArr = explode('-', $searcharray['atty_resp']);
            $searchKey = $searchArr[0];
            $searchVal = $searchArr[1];
            $filterArray['c.' . $searchKey] = $searchVal;
        }

        if (isset($searcharray['atty_hand']) && $searcharray['atty_hand'] != 'atty_hand-' && $searcharray['atty_hand'] != '') {
            $searchArr = explode('-', $searcharray['atty_hand']);
            $searchKey = $searchArr[0];
            $searchVal = $searchArr[1];
            $filterArray['c.' . $searchKey] = $searchVal;
        }
        if (isset($searcharray['para_hand']) && $searcharray['para_hand'] != 'para_hand-' && $searcharray['para_hand'] != '') {
            $searchArr = explode('-', $searcharray['para_hand']);
            $searchKey = $searchArr[0];
            $searchVal = $searchArr[1];
            $filterArray['c.' . $searchKey] = $searchVal;
        }
        if (isset($searcharray['sec_hand']) && $searcharray['sec_hand'] != 'sec_hand-' && $searcharray['sec_hand'] != '') {
            $searchArr = explode('-', $searcharray['sec_hand']);
            $searchKey = $searchArr[0];
            $searchVal = $searchArr[1];
            $filterArray['c.' . $searchKey] = $searchVal;
        }
        if (isset($searcharray['card_type']) && $searcharray['card_type'] != '') {
            $filterArray['cd.type'] = $searcharray['card_type'];
        }
        if (isset($searcharray['case_status']) && $searcharray['case_status'] != '') {
            $filterArray['c.casestat'] = $searcharray['case_status'];
        }
//        if ((isset($searcharray['Ctable_unique']) && $searcharray['Ctable_unique'] != '') || (isset($searcharray['primary_names']) && $searcharray['primary_names'] != '')) { 
//            $filterArray['cc.orderno'] = 1;
//        }
        if (isset($searcharray['primary_names']) && $searcharray['primary_names'] != '') { 
            $filterArray['cc.orderno'] = 1;
        }
        if (isset($searcharray['Ctable_unique']) && $searcharray['Ctable_unique'] != '') { 
            $CaseUnique = 1;
        } else {
            $CaseUnique = 0;
        }

        if (isset($searcharray['deleted_cases'])) {
            unset($filterArray['c.case_status']);
        }

        $CaseReport = $this->Report_model->getFilterCaseReport($start, $length, $filterArray, $orCondition, $order, $CaseUnique);
        $count = $this->Report_model->getFilterCaseReportCount($filterArray, $CaseUnique);
        $totalCount = $this->Report_model->getFilterCaseReportCount();

        $FinalData = [];
        if (!empty($CaseReport)) {
            foreach ($CaseReport as $key => $value) :
                $Injurydates = "";
                $eamsno = "";
                $wcabno = "";
                $dateentered = "";
                $followup = "";
                $phone = "";
                
                //get injury dates of case
                $Injury = $this->Report_model->getinjuryDates($value['caseno']);
                if (!empty($Injury)) {
                    foreach ($Injury as $k1 => $v1) :
                        $eamsno = ($v1['eamsno'] != "") ? " - ADJ" . $v1['eamsno'] : "";
                        $wcabno = ($v1['case_no'] != "") ? " - " . $v1['case_no'] : "";
                        $Injurydates .= "<p class='injurydates'><strong>" . ++$k1 . ". </strong>" . $v1['adj1c'] . $eamsno . $wcabno . "</p>";
                    endforeach;
                }

                if (isset($value['dateenter']) && $value['dateenter'] != "") {
                    if ($value['dateenter'] == '1899-12-30 00:00:00') {
                        $dateentered = "-";
                    } else {
                        $dateentered = new DateTime($value['dateenter']);
                        $dateentered = $dateentered->format('d/m/Y');
                    }
                } else {
                    $dateentered = "-";
                }
                if (isset($value['followup']) && $value['followup'] != "") {
                    if ($value['followup'] == '1899-12-30 00:00:00') {
                        $followup = "-";
                    } else {
                        $followup = new DateTime($value['followup']);
                        $followup = $followup->format('d/m/Y');
                    }
                } else {
                    $followup = "-";
                }

                if (isset($value['home']) && $value['home'] != "") {
                    $phone = $value['home'];
                } elseif (isset($value['business']) && $value['business'] != "") {
                    $phone = $value['business'];
                } else {
                    $phone = "-";
                }
                $name = "";
                $last = "";
                $first = "";
                if ($value['last'] != "" && $value['first'] != "") {
                    $last = $value['last'] . ", ";
                    $first = $value['first'];
                } if ($value['last'] != "" && $value['first'] == "") {
                    $last = $value->last;
                } if ($value['last'] == "" && $value['first'] != "") {
                    $first = $value['first'];
                }
                $name = $last . $first;

                $FinalData[] = [
                    (isset($value['caseno']) && $value['caseno'] != "") ? $value['caseno'] : "-",
                    ($name != "") ? $name : "-",
                    (isset($value['atty_resp']) && $value['atty_resp'] != "") ? $value['atty_resp'] : "-",
                    (isset($value['atty_hand']) && $value['atty_hand'] != "") ? $value['atty_hand'] : "-",
                    (isset($value['casetype']) && $value['casetype'] != "") ? $value['casetype'] : "-",
                    (isset($value['casestat']) && $value['casestat'] != "") ? $value['casestat'] : "-",
                    ($phone != "") ? $phone : "-",
                    (isset($value['yourfileno']) && $value['yourfileno'] != "") ? $value['yourfileno'] : "-",
                    ($followup != "") ? $followup : "",
                    ($dateentered != "") ? $dateentered : "",
                    (isset($value['para_hand']) && $value['para_hand'] != "") ? $value['para_hand'] : "-",
                    (isset($value['sec_hand']) && $value['sec_hand'] != "") ? $value['sec_hand'] : "-",
                    (isset($value['social_sec']) && $value['social_sec'] != "") ? $value['social_sec'] : "-",
                    (isset($value['type']) && $value['type'] != "") ? $value['type'] : "-",
                    (isset($value['cardcode']) && $value['cardcode'] != "") ? $value['cardcode'] : "-",
                    (isset($value['firm']) && $value['firm'] != "") ? $value['firm'] : "-",
                    ($shortdefault != "") ? $shortdefault : "",
                    (isset($value['caption1']) && $value['caption1'] != "") ? $value['caption1'] : "",
                    ($value['address1'] != "" || $value['city'] || $value['address2'] || $value['state'] != "" || $value['zip'] != "") ? $value['address1'] . " " . $value['address2'] . ' <br />' . $value['city'] . " " . $value['state'] . " " . $value['zip']: "-",
//                    (isset($value['mailing1']) && $value['mailing1'] != "") ? $value['mailing1'] : "-",
                    (isset($value['case_status']) && $value['case_status'] == 1) ? "Active" : "Deleted",
                    (isset($Injurydates) && $Injurydates != "") ? $Injurydates : "-"
                ];
            endforeach;
        }

        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $FinalData
        );

        echo json_encode($listingData);
        exit;
    }

    public function getSearchCase($shortArr) {
        switch (trim($shortArr[0])) {
            case "F":
                $searcharray['searchBy'] = 'card.first';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "C":
                $searcharray['searchBy'] = 'card2.firm';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "Y":
                $searcharray['searchBy'] = 'case.yourfileno';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "S":
                $searcharray['searchBy'] = 'card.social_sec';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "D":
                $searcharray['searchBy'] = 'case.followup';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "P":
                $searcharray['searchBy'] = 'card.contact';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "H":
                $searcharray['searchBy'] = 'card.first';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "B":
                $searcharray['searchBy'] = 'card.birth_date';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "M":
                $searcharray['searchBy'] = 'card.birth_month';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            default:
                $searcharray['searchBy'] = 'card.last';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
        }

        return $searcharray;
    }

    //*************************************************//
    // * @name   : getSearchReferralReport
    // * @todo   : Get Data of Referral report
    // * @Date   : 25-May-2018
    //************************************************//

    public function getSearchReferralReport() {

        extract($_POST);
        $filterArray = array();
        foreach ($_POST['searchData'] as $val) {
            if ($val['value'] != '') {
                if ($val['name'] == 'casetype') {
                    $filterArray['c.casetype'] = $val['value'];
                }
                if ($val['name'] == 'case_status') {
                    $filterArray['c.casestat'] = $val['value'];
                }
                if ($val['name'] == 'atty_hand') {
                    $filterArray['c.atty_hand'] = $val['value'];
                }
                if ($val['name'] == 'rb') {
                    $filterArray['c.rb'] = $val['value'];
                }
                if ($val['name'] == 'start_d_r') {
                    $start_d_r = $val['value'];
                }
                if ($val['name'] == 'end_d_r') {
                    $end_d_r = $val['value'];
                }
            }
        }
        if ($start_d_r != "" && $end_d_r != "") {
            $filterArray['c.d_r >='] = date('Y-m-d 00:00:00', strtotime($start_d_r));
            $filterArray['c.d_r <='] = date('Y-m-d 23:59:59', strtotime($end_d_r));
        } elseif ($start_d_r != "" && $end_d_r == "") {
            $filterArray['c.d_r >='] = date('Y-m-d 00:00:00', strtotime($start_d_r));
        } elseif ($start_d_r == "" && $end_d_r != "") {
            $filterArray['c.d_r <='] = date('Y-m-d 23:59:59', strtotime($end_d_r));
        }

        $rec = $this->Report_model->getSearchReferralReport($filterArray, $start, $length, $order);

        $count = $this->Report_model->getSearchReferralReportCount($filterArray);
        $totalCount = $this->Report_model->getSearchReferralReportCount();
        $record = array();
        if (!empty($rec)) {
            foreach ($rec as $key => $val) {
                $casetype = $casestat =  $atty_hand = $d_r = $rb = $caption1 = "";
                
                $val->d_r = $this->common_functions->checkDateIsNull($val->d_r);
                $casetype = ($val->casetype != '') ? $val->casetype : '-';
                $casestat = ($val->casestat != '') ? $val->casestat : '-';
                $atty_hand = ($val->atty_hand != '') ? $val->atty_hand : '-';
                $d_r = ($val->d_r != "") ? date('m/d/Y', strtotime($val->d_r)) : "-";
                $rb = ($val->rb != '') ? $val->rb : '-';
                $caption1 = ($val->caption1 != '') ? $val->caption1 : '-';
//                $case_cards = $this->case_model->getCaseFullDetails($val->caseno);
                $referby = "";
                $last = "";
                $first = "";
                if (isset($val->firm) && $val->firm != "") {
                    $referby = $val->firm;
                } else {
                    $referby = "";
                    if ($val->last != "" && $val->first != "") {
                        $last = $val->last . ", ";
                        $first = $val->first;
                    } if ($val->last != "" && $val->first == "") {
                        $last = $val->last;
                    } if ($val->last == "" && $val->first != "") {
                        $first = $val->first;
                    }
                    $referby = $last . $first;
                }

                $record[] = array(
                    ($val->caseno != "") ? $val->caseno : "-",
                    ($casetype != "") ? $casetype : "-",
                    ($casestat != "") ? $casestat : "-",
                    ($atty_hand != "") ? $atty_hand : "-",
                    ($d_r != "") ? $d_r : "-",
                    ($referby != "") ? $referby : "-",
                    ($caption1 != "") ? $caption1 : "-",
                );
            }
        }
        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $record
        );

        echo json_encode($listingData);
        exit;
    }

    //*************************************************//
    // * @name   : getSearchInjuryReport
    // * @todo   : Get Data of Case Injury report
    // * @Date   : 09-Aug-2018
    //************************************************//

    public function getSearchInjuryReport() {

        extract($_POST);
        $filterArray = array();
        $searchData = $_POST['searchData'];
        foreach ($searchData as $val) :
            if ($val['value'] != '') {
                if ($val['name'] == 'casetype') {
                    $filterArray['casetype'] = $val['value'];
                }
                if ($val['name'] == 'case_status') {
                    $filterArray['case_status'] = $val['value'];
                }
                if ($val['name'] == 'atty_hand') {
                    $filterArray['atty_hand'] = $val['value'];
                }
                if ($val['name'] == 'injury_status') {
                    $filterArray['app_status'] = $val['value'];
                }
                if ($val['name'] == 'i_wcab') {
                    $filterArray['i_wcab'] = trim($val['value']);
                }
                if ($val['name'] == 'i_claimno') {
                    $filterArray['i_claimno'] = trim($val['value']);
                }
                if ($val['name'] == 'doi') {
                    $filterArray['doi'] = $val['value'];
                }
                if ($val['name'] == 'pob') {
                    $filterArray['pob'] = $val['value'];
                }
                if ($val['name'] == 'body_parts') {
                    $filterArray['body_parts'] = $val['value'];
                }
                if ($val['name'] == 'doi_my') {
                    $filterArray['doi_my'] = $val['value'];
                }
                if ($val['name'] == 'no_injury') {
                    $filterArray['no_injury'] = $val['value'];
                }
            }
        endforeach;

        $rec = $this->Report_model->getSearchInjuryReport($filterArray, $start, $length, $order);
        $count = $this->Report_model->getSearchInjuryReportCount($filterArray);
//        $totalCount = $this->Report_model->getSearchInjuryReportCount();
        $totalCount = 0;
        $bodyParts = json_decode(bodyParts);
        $record = array();
        if (!empty($rec)) {
            foreach ($rec as $key => $val) {
                $howoccured = $case_no =  $wcabno = $claimno = $doi2 = $fileno = $status2 = $venue = $occupation = $caseno = $casetype = $status = $atty_hand = $client = $employer = $carrier = $attorney = $doi = $other_pob = "";
                
                $caseno = (isset($val->caseno)) ? $val->caseno : "-";
                $casetype = (isset($val->casetype)) ? $val->casetype : "-";
                $status = (isset($val->casestat)) ? $val->casestat : "-";
                $atty_hand = (isset($val->atty_hand)) ? $val->atty_hand : "-";
                $client = $val->first . " " . $val->last;
                $employer = ($val->e_name != "") ? $val->e_name : $val->e2_name;
                $carrier = ($val->i_name != "") ? $val->i_name : $val->i2_name;
                $attorney = ($val->d1_first != "" || $val->d1_last != "") ? $val->d1_first . "" . $val->d1_last : $val->d2_first . "" . $val->d2_last;
                
                $doi = (isset($val->adj1c) && $val->adj1c != "") ? $val->adj1c : "-";
                $other_pob = (isset($val->adj1e)) ? $val->adj1e : "-";


                $bodyParts = json_decode(bodyParts);
                $pob = '';
                foreach ($bodyParts as $k2 => $v2) {

                    if (!empty($val->pob1) && $k2 == $val->pob1) {
                        $pob .= $k2 . ' ' . $v2 . ",<br />";
                    }

                    if (!empty($val->pob2) && $k2 == $val->pob2) {
                        $pob .= $k2 . ' ' . $v2 . ",";
                    }

                    if (!empty($val->pob3) && $k2 == $val->pob3) {
                        $pob .= $k2 . ' ' . $v2 . ",<br />";
                    }

                    if (!empty($val->pob4) && $k2 == $val->pob4) {
                        $pob .= $k2 . ' ' . $v2 . ",";
                    }

                    if (!empty($val->pob5) && $k2 == $val->pob5) {
                        $pob .= $k2 . ' ' . $v2 . ",<br />";
                    }
                }

                if (!empty($val->adj1e)) {
                    $pob .= $val->adj1e . ",<br />";
                }

                $howoccured = (isset($val->adj2a)) ? $val->adj2a : "-";
                $case_no = (isset($val->case_no) && $val->case_no != "") ? " (" . $val->case_no . ")" : "-";
                $wcabno = $val->eamsno . $case_no;
                $claimno = ($val->i_claimno != "") ? $val->i_claimno : $val->i2_claimno;
                $val->doi2 = $this->common_functions->checkDateIsNull($val->doi2);
                $doi2 = ($val->doi2 != "") ? date('m/d/Y', strtotime($val->doi2)) : "-";
                $fileno = (isset($val->yourfileno)) ? $val->yourfileno : "-";
                $status2 = (isset($val->status2)) ? $val->status2 : "-";
                $venue = (isset($val->venue)) ? $val->venue : "-";
                $occupation = (isset($val->adj1a)) ? $val->adj1a : "-";

                $record[] = array(
                    ($caseno != "") ? $caseno : "-",
                    ($casetype != "") ? $casetype : "-",
                    ($status != "") ? $status : "-",
                    ($atty_hand != "") ? $atty_hand : "-",
                    ($client != "") ? $client : "-",
                    ($employer != "") ? $employer : "-",
                    ($carrier != "") ? $carrier : "-",
                    ($attorney != "") ? $attorney : "-",
                    ($doi != "") ? $doi : "-",
                    ($pob != "") ? $pob : "-",
                    ($howoccured != "") ? $howoccured : "-",
                    ($wcabno != "") ? $wcabno : "-",
                    ($claimno != "") ? $claimno : "-",
                    ($doi2 != "") ? $doi2 : "-",
                    ($fileno != "") ? $fileno : "-",
                    ($status2 != "") ? $status2 : "-",
                    ($venue != "") ? $venue : "-",
                    ($occupation != "") ? $occupation : "-"
                );
                $partsofbody = [];
                $pob = [];
            }
        }

        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $record
        );
        echo json_encode($listingData);
        exit;
    }

    //*************************************************//
    // * @name   : getCaseactivity
    // * @todo   : Get Data of Case Activity Report
    // * @Date   : 10-Aug-2018
    //************************************************//

    public function getCaseactivity() {

        extract($_POST);
        $filterArray = array();
        $searchData = $_POST['searchData'];
        $isFLName = "";
        foreach ($searchData as $val) {
            if ($val['value'] != '') {
                if ($val['name'] == 'casetype') {
                    $filterArray['casetype'] = $val['value'];
                }
                if ($val['name'] == 'case_status') {
                    $filterArray['casestat'] = $val['value'];
                }
                if ($val['name'] == 'atty_hand') {
                    $filterArray['atty_hand'] = $val['value'];
                }
                if ($val['name'] == 'categories') {
                    $filterArray['category'] = $val['value'];
                }
                if ($val['name'] == 'start_ca_date') {
                    $filterArray['start_ca_date'] = $val['value'];
                }
                if ($val['name'] == 'end_ca_date') {
                    $filterArray['end_ca_date'] = $val['value'];
                }
                if ($val['name'] == 'event') {
                    $filterArray['event'] = $val['value'];
                }
                if ($val['name'] == 'max_category') {
                    $filterArray['max_category'] = $val['value'];
                }
                if ($val['name'] == 'documents') {
                    $filterArray['documents'] = $val['value'];
                }
                if ($val['name'] == 'noActivity') {
                    $filterArray['noActivity'] = $val['value'];
                }
                if ($val['name'] == 'most_recent') {
                    $filterArray['most_recent'] = $val['value'];
                }
                if ($val['name'] == 'reportWith') {
//                    $filterArray['reportWith'] = $val['value'];
                }
                if ($val['name'] == 'event_contains') {
                    $filterArray['event_contains'] = $val['value'];
                }

                if ($val['name'] == 'isFLName') {
                    $isFLName = 1;
                }
            }
        }

        $rec = $this->Report_model->getCaseactivity($filterArray, $start, $length, $order);
        $count = $this->Report_model->getCaseactivityCount($filterArray);

        $totalCount = 0;
        $record = array();
        if (!empty($rec)) {
            foreach ($rec as $key => $val) {
          
                $filelink = [];
                $filename = "";
                $filelinks = "";
                $files = [];
                $name = "";
                
                //hidden temporary with multiple line comment.
                /*$files = glob(DOCUMENTPATH . 'clients/' . $val->caseno . "/f" . $val->actno . "*");
                if (!empty($files)) {
                    foreach ($files as $key => $value) :
                        $fileArr = explode("/", $value);
                       // $link = $filelink[] = base_url() . 'assets/clients/' . $val->caseno . '/' . $fileArr[7];
                       $link = base_url() . 'assets/clients/' . $val->caseno . '/' . $fileArr[7];
                        $filelinks .= '<a  class="btn btn-xs btn-primary caseactdocs-report view_attach"   data-filename="one"  data-actid="' . $val->actno . '" href="' . $link . '" target="_blank"><i class = "icon fa fa-eye"></i>' . $fileArr[7] . '</a>';
//                        $filename[] = $fileArr[7];
                    endforeach;
                }*/
                $val->date = $this->common_functions->checkDateIsNull($val->date);
                $val->date = ($val->date != '') ? date('m/d/Y', strtotime($val->date)) : '';

                $name = ($isFLName == 1) ? $val->first . " " . $val->last : $val->last . ", " . $val->first;
                
//                $event = ($val->event != "") ? $val->event . " " . $filelinks : "-";
                $event = ($val->event != "") ? $val->event : "-";
                $record[] = array(($val->caseno != "") ? $val->caseno : "-",
                    ($val->casetype != "") ? $val->casetype : "",
                    ($val->casestat != "") ? $val->casestat : "-",
                    ($val->atty_hand != "") ? $val->atty_hand : "-",
                    ($val->date != "") ? $val->date : "-",
                    ($val->initials != "") ? $val->initials : "-",
                    ($name != "") ? $name : "-",
                    ($event != "") ? $event : "-",
                    $filelinks
                );
            }
        }

        $listingData = [
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $record
        ];
        echo json_encode($listingData);
        exit;
    }

    //*************************************************//
    // * @name   : getFilterTaskReport
    // * @todo   : Get Data of tASK report
    // * @Date   : 26-May-2018
    //************************************************//

    public function getFilterTaskReport() {
        
        extract($_POST);
        //echo "<pre>"; print_r($_POST); exit;
        $filterArray = array();

        $isContain = "";
        foreach ($searchData as $val) {

            if ($val['value'] != '') {

                if ($val['name'] == 'Tcasetype') {
                    $filterArray['c.casetype'] = $val['value'];
                }
                if ($val['name'] == 'Tcase_status') {
                    $filterArray['c.casestat'] = $val['value'];
                }
                if ($val['name'] == 'Tatty_hand') {
                    $filterArray['c.atty_hand'] = $val['value'];
                }
                
                if ($val['name'] == 'start_task_date') {
                    $start_task_date = $val['value'];
                }
                if ($val['name'] == 'end_task_date') {
                    $end_task_date = $val['value'];
                }
                if ($val['name'] == 'filterType') {
                    $filterType = $val['value'];
                }

                if ($start_task_date != "" && $end_task_date != "") {
                    $filterArray['t.datereq >='] = date('Y-m-d 00:00:00', strtotime($start_task_date));
                    $filterArray['t.datereq <='] = date('Y-m-d 23:59:59', strtotime($end_task_date));
                } elseif ($start_task_date != "" && $end_d_r == "") {
                    $filterArray['t.datereq >='] = date('Y-m-d 00:00:00', strtotime($start_task_date));
                } elseif ($start_task_date == "" && $end_d_r != "") {
                    $filterArray['t.datereq <='] = date('Y-m-d 23:59:59', strtotime($end_task_date));
                }

                if ($filterType != 'noact') {
                    if ($val['name'] == 'Twhofrom') {
                    $filterArray['t.whofrom'] = $val['value'];
                    }
                    if ($val['name'] == 'Twhoto') {
                        $filterArray['t.whoto'] = $val['value'];
                    }
                }
                else{
                    unset($filterArray['t.whofrom']);
                    unset($filterArray['t.whoto']);
                }
                
                if ($val['name'] == 'Tevent') {
                    $filterArray['t.event'] = $val['value'];
                }
                
                if ($val['name'] == "Tcontains") {
                    $isContain = 1;
                }

                if ($filterArray['t.event'] != '') {
                    if ($isContain == 1) {
                        $filterArray['t.event LIKE '] = "%" . $filterArray['t.event'] . "%";
                        unset($filterArray['t.event']);
                    } 
                }

                if ($val['name'] == 'TisHighPriority') {
                    $filterArray['t.priority'] = '1';
                }
                $completed == '';
                if ($val['name'] == 'TisCompleted') {
                   //$filterArray['t.completed'] = '(completed != "1899-12-30 00:00:00" AND completed != "0000-00-00 00:00:00")';
                    $completed = 'completed';
                }
            }
        }
        //echo '<pre>'; print_r($filterArray); exit;
        $rec = $this->Report_model->getFilterTaskReport($filterArray, $start, $length, $order, $filterType,$completed);
        $count = $this->Report_model->getFilterTaskReportCount($filterArray,$filterType,$completed);
        //echo '<pre>'; print_r($rec); exit;
        $totalCount = 0;
        $record = array();
        if (!empty($rec)) {
            foreach ($rec as $key => $val) :
                $name = "";
                $val->datereq = $this->common_functions->checkDateIsNull($val->datereq);
                $val->datereq = ($val->datereq != '') ? date('m/d/Y', strtotime($val->datereq)) : '';

                $val->dateass = $this->common_functions->checkDateIsNull($val->dateass);
                $val->dateass = ($val->dateass != '') ? date('m/d/Y', strtotime($val->dateass)) : '';

                $val->caseno = ($val->caseno !== '') ? $val->caseno : 0;
                $val->priority = ($val->priority !== '') ? (($val->priority == '2') ? 'Medium' : (($val->priority == '1') ? 'High' : 'Low') ) : '';

                $last = "";
                $first = "";
                if ($val->last != "" && $val->first != "") {
                    $last = $val->last . ", ";
                    $first = $val->first;
                } if ($val->last != "" && $val->first == "") {
                    $last = $val->last;
                } if ($val->last == "" && $val->first != "") {
                    $first = $val->first;
                }
                $name = $last . $first;
                
                $record[] = array(
                    (isset($val->caseno) && $val->caseno != "") ? $val->caseno : "-",
                    (isset($val->casetype) && $val->casetype != "") ? $val->casetype : "-",
                    (isset($val->casestat) && $val->casestat != "") ? $val->casestat : "-",
                    (isset($val->atty_hand) && $val->atty_hand != "") ? $val->atty_hand : "-",
                    (isset($val->datereq) && $val->datereq != "") ? $val->datereq : "-",
                    (isset($val->whofrom) && $val->whofrom != "") ? $val->whofrom : "-",
                    (isset($val->whoto) && $val->whoto != "") ? $val->whoto : "-",
                    (isset($val->event) && $val->event != "") ? $val->event : "-",
                    ($name != "") ? $name : "-",
                    (isset($val->typetask) && $val->typetask != "") ? $val->typetask : "-",
                    (isset($val->priority) && $val->priority != "") ? $val->priority : "-",
                    (isset($val->dateass) && $val->dateass != "") ? $val->dateass : "-",
                    ($val->completed != '' && $val->completed != '1899-12-30 00:00:00' && $val->completed != '0000-00-00 00:00:00' ) ? date('m/d/Y H:i A', strtotime($val->completed)) : '-',
                );
                
            endforeach;
        }

        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $record
        );
        echo json_encode($listingData);
        exit;
    }
    
    //*************************************************//
    // * @name   : getTaskpoolrepot
    // * @todo   : Get Task Count for Pool Report
    // * @Date   : 27-Sep-2018
    //************************************************//
    
    public function getTaskpoolrepot() {
        
        extract($_POST);
        $filterArray = array();
        $PoolReport = $this->Report_model->getTaskpoolrepot($start, $length, $order);
        $PoolReportCount = $this->Report_model->getTaskpoolrepotCount();
        
        $totalCount = 0;
        $record = array();
        if (!empty($PoolReport)) {
            foreach ($PoolReport as $key => $val) :
                $total_task = 0;
                $TotalCompletedTasks = 0;
                $TotalPendingTask = 0;
                
                $total_task = (isset($val['total_task']) && $val['total_task'] != "") ? (int)$val['total_task'] : "0";
                $TotalCompletedTasks = (isset($val['TotalCompletedTasks']) && $val['TotalCompletedTasks'] != "") ? (int)$val['TotalCompletedTasks'] : "0";
                $TotalPendingTask = $total_task - $TotalCompletedTasks;
                $record[] = array(
                    (isset($val['initials']) && $val['initials'] != "") ? $val['initials'] : "-",
                    $total_task,
                    $TotalPendingTask,
                    (isset($val['Last3MonthComplTasks']) && $val['Last3MonthComplTasks'] != "") ? (int)$val['Last3MonthComplTasks'] : "0",
                    $TotalCompletedTasks,
                );
            endforeach;
        }

        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $PoolReportCount,
            'data' => $record
        );
        echo json_encode($listingData);
        exit;
        
    }
    
    //*************************************************//
    // * @name   : getFilterMarkupReport
    // * @todo   : Get Data of maRKUP report
    // * @Date   : 30-May-2018
    //************************************************//

    public function getFilterMarkupReport() {
        extract($_POST);
        $filterArray = array();
        if (isset($searchData) && !empty($searchData)) {
            foreach ($searchData as $val):
                if ($val['value'] != '') {
                    if ($val['name'] == 'case_type') {
                        $filterArray['c.casetype'] = $val['value'];
                    }
                    if ($val['name'] == 'case_status') {
                        $filterArray['c.casestat'] = $val['value'];
                    }
                    if ($val['name'] == 'atty_hand') {
                        $filterArray['c.atty_hand'] = $val['value'];
                    }
                    if ($val['name'] == 'atty_resp') {
                        $filterArray['c.atty_resp'] = $val['value'];
                    }
                    if ($val['name'] == 'para_hand') {
                        $filterArray['c.para_hand'] = $val['value'];
                    }
                    if ($val['name'] == 'venue') {
                        $filterArray['c.venue'] = $val['value'];
                    }

                    if ($val['name'] == 'start_followup') {
                        $filterArray['c.followup >='] = date('Y-m-d 00:00:00', strtotime($val['value']));
                    }
                    if ($val['name'] == 'end_followup') {
                        $filterArray['c.followup <='] = date('Y-m-d 23:59:59', strtotime($val['value']));
                    }

                    if ($val['name'] == 'start_date_open') {
                        $filterArray['c.dateopen >='] = date('Y-m-d 00:00:00', strtotime($val['value']));
                    }
                    if ($val['name'] == 'end_date_open') {
                        $filterArray['c.dateopen <='] = date('Y-m-d 23:59:59', strtotime($val['value']));
                    }
                }
            endforeach;
        }

        if (isset($filterArray['c.followup >=']) && !isset($filterArray['c.followup <='])) {
            $filterArray['c.followup <='] = date('Y-m-d 23:59:59');
        }
        if (isset($filterArray['c.followup <=']) && !isset($filterArray['c.followup >='])) {
            $filterArray['c.followup >='] = date('Y-m-d 00:00:00', strtotime($filterArray['c.followup <=']));
        }

        if (isset($filterArray['c.dateopen >=']) && !isset($filterArray['c.dateopen <='])) {
            $filterArray['c.dateopen <='] = date('Y-m-d 23:59:59');
        }
        if (isset($filterArray['c.dateopen <=']) && !isset($filterArray['c.dateopen >='])) {
            $filterArray['c.dateopen >='] = date('Y-m-d 00:00:00', strtotime($filterArray['c.dateopen <=']));
        }

        $filterArray['cc.orderno'] = 1;

        $basicCaseReport = $this->Report_model->getFilterMarkupReport($filterArray, $start, $length, $order);
        $count = $this->Report_model->getFilterMarkupReportCount($filterArray);
        $totalCount = $this->Report_model->getFilterMarkupReportCount();

        $report = array();
        if (count($basicCaseReport) > 0) {
            foreach ($basicCaseReport as $key => $rec) {
                $quickNote = $this->common_functions->getQuickNote($rec->caseno);
                $datetime = '';

                $nextCalEvent = '';
                $client = '';
                if (isset($quickNote->last) && $quickNote->last != '') {
                    $client .= $quickNote->last;
                }
                if (isset($quickNote->first) && $quickNote->first != '') {
                    $client .= ($client != '') ? ', ' . $quickNote->first : $quickNote->first;
                }

                $notes = isset($quickNote->event) ? $quickNote->event : '';


                $rec->date = $this->common_functions->checkDateIsNull($rec->date);
                $rec->date = ($rec->date != '') ? date('m/d/Y H:i:s', strtotime($rec->date)) : '';

                $rec->followup = $this->common_functions->checkDateIsNull($rec->followup);
                $rec->followup = ($rec->followup != '') ? date('m/d/Y', strtotime($rec->followup)) : '';

                $rec->dateopen = $this->common_functions->checkDateIsNull($rec->dateopen);
                $rec->dateopen = ($rec->dateopen != '') ? date('m/d/Y', strtotime($rec->dateopen)) : '';

                if (isset($nextEvent->date)) {
                    $datetime = date('m/d/Y H:i:s', strtotime($nextEvent->date));
                    $nextCalEvent = $nextEvent->event;
                }
                $venueCard = '';
                $venueCal = '';
                $udf = '';
                $attyAssign = '';

                $tempArr = array(
                    $rec->caseno,
                    $rec->followup,
                    $client,
                    $notes,
                    $rec->date,
                    $rec->event,
                    $venueCard,
                    $venueCal,
                    $rec->casetype,
                    $rec->casestat,
                    $rec->atty_resp,
                    $rec->atty_hand,
                    $rec->para_hand,
                    $rec->caption1,
                    $rec->dateopen,
                    $udf,
                    $rec->yourfileno,
                    $rec->attyass
                );

                array_push($report, $tempArr);
            }
        }

        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $report
        );

        echo json_encode($listingData);
    }

}
