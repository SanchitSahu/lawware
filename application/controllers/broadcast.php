<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Broadcast extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->common_functions->checkLogin();
        $this->load->model('chat_model');
        $data = array();
        $this->data['menu'] = 'broadcast';
    }
    function __destruct() {
    }
    public function index(){
        $this->data['pageTitle'] = 'Broadcast';
        $this->data['loginUser'] = $this->session->userdata('user_data')['initials'];
        
        //$this->data['chatUsers'] = $this->chat_model->getAllChatUser();
        
        $this->load->view('common/header',$this->data);
        $this->load->view('broadcast/broadcast');
        $this->load->view('common/footer');
    }
    
    public function send_message(){
        
        extract($_POST);
        
        $messageArray = array('whofrom'=>$initials,'message'=>$message,'chat_type'=>'broadcast','timestamp'=>time());
        
        $this->chat_model->addNewChat($messageArray);
        
        $this->_setOutput($message);
    }
    
    public function getUsers(){
        $timestamp = $this->input->get('timestamp', null);
        $allUsers = $this->chat_model->getAllChatUser($timestamp);
        
        echo json_encode($allUsers);
    }
    
    public function get_messages(){
        
        $length = $this->input->get('length',null); 
        $timestamp = $this->input->get('timestamp', null);
        $messages = $this->chat_model->get_messages($timestamp,$length);
        
        $lastData = end($messages);
        $this->data['lastData'] = $lastData['id'];
        
        $this->_setOutput($messages);
    }
    
    private function _setOutput($data)
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');

        echo json_encode($data);
    }
}