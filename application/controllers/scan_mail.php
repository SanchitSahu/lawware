<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Scan_mail extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('export_csv_model');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('FTP/SFTP');
    }

    function __destruct()
    { }

    function index()
    {
        $ftp = new SFTP(FTP_HOST, FTP_USERNAME, FTP_PASSWORD);
        $ftp->passive = true;
        $ftp->ssl = false;

        if ($ftp->connect()) {
            print "Connection successful";
        } else {
            print "Connection failed: " . $ftp->error;
        }
        $zipFilesOnServer = $ftp->ls('/');
        echo "<pre>";
        print_r($zipFilesOnServer);

        $zipFiles = array();
        if (!empty($zipFilesOnServer)) {
            foreach ($zipFilesOnServer as $file) {
                $fileParts = explode('.', $file);
                var_dump($fileParts[count($fileParts) - 1] == 'zip');
                if ($fileParts[count($fileParts) - 1] == 'zip') {
                    if ($ftp->get($file, 'assets/scanmail/' . $file)) {
                        $ftp->rename($file, 'downloaded/' . $file);
                        $zipFiles[] = $file;
                    }
                }
            }
        }
        exit;
        foreach ($zipFiles as $folder) {
            $zip = new ZipArchive;
            $res = $zip->open($folder);
            if ($res === TRUE) {

                // Unzip path
                $path = 'assets/scanmail/';
                // Extract file
                $zip->extractTo($path);
                $zip->close();
                echo 'Unzip!';
            } else {
                echo 'failed!';
            }
            $folder = explode(".", $folder);
            $dirfiles = '';
            $pdfdirfiles = '';
            foreach (glob("assets/scanmail/$folder[0]/*.TXT") as $dirfile) {
                $dirfiles = $dirfile;
            }
            foreach (glob("assets/scanmail/$folder[0]/*.pdf") as $pdfdirfile) {
                $pdfdirfiles = $pdfdirfile;
            }
            $searchthis = 'PDF';
            $matches = '';
            $handle = @fopen("$dirfiles", "r");
            while (!feof($handle)) // Loop til end of file.
            {
                $buffer = fgets($handle);
                if (strpos($buffer, $searchthis) !== FALSE) {
                    $matches = $buffer;
                }
                $content = explode("~", $matches);
            }
            $CaseActdata = $this->export_csv_model->fetchcaseactno($content[1]);
            $newDate = date("Y-m-d H:i:s");
            $newDateTime = $newDate;
            $act_array = array( //'actno'      =>  $caseActno + 1,
                'caseno'    =>  $content[1],
                'date' => $newDateTime,
                'event' => $content[3],
                'category' => $content[2],
            );
            if (!empty($CaseActdata)) {
                $res = $this->common_functions->getRecordById('caseact', 'actno', 'caseno =' . $content[1] . ' order by actno desc limit 0,1');
                if (!empty($res)) {
                    $act_array['actno']  = $res->actno + 1;
                } else {
                    $act_array['actno']  = 1;
                }
            } else {

                $act_array['actno'] = 1;
            }
            $this->db->insert('caseact', $act_array);
            if (!file_exists(DOCUMENTPATH . 'clients/' . $content[1])) {
                mkdir(DOCUMENTPATH . 'clients/' . $content[1], 0777, true);
                copy($pdfdirfiles, DOCUMENTPATH . 'clients/' . $content[1] . '/F' . $act_array['actno'] . '.pdf');
            } else {
                copy($pdfdirfiles, DOCUMENTPATH . 'clients/' . $content[1] . '/F' . $act_array['actno'] . '.pdf');
            }
        }
        //echo '<pre>'; print_r($zipFiles); exit;
        print $ftp->error;
    }

    function generateCsv()
    {
        // create the ZIP file
        $zip_name = '_UPLOAD_SSY.zip';
        $zip = new ZipArchive;
        $zip->open($zip_name, ZipArchive::CREATE);
        $files = array('ExTmp1.csv', 'ExTmp2.csv');
        $files_txt = array('ExTmp1.txt', 'ExTmp2.txt');
        $temp_directory = '';
        $Client_data = $this->export_csv_model->fetch_data();
        $cat_data = $this->export_csv_model->fetch_category();
        $count = 1;
        foreach ($files as $file) {
            $handle = fopen($file, 'w');
            $data = array(); // data maybe from MySQL to add to your CSV file
            if ($count == 1) {
                $data = $Client_data;
                $header = array('caseno', 'name', 'cstype', 'csstat', 'yourfileno', 'ssno', 'caption1', 'caption2', 'caption3', 'caption4', 'caption5', 'sort1');
            } else {
                $data = $cat_data;
                $header = array('casetype', 'actname', 'category', 'actorder', 'access', 'gocltcdudf', 'actgridx', 'actgridy', 'scnwidth', 'scnheight', 'scnsplit', 'userpageon', 'usertm', 'userlm', 'pagecap', 'edituserpg', 'userformat', 'usercolmax', 'usercolwid', 'accessadd', 'accessedit', 'usrorder', 'usrenabled', 'usrtop', 'usrleft', 'usrstyle', 'usrparty', 'usrheight', 'usrwidth', 'usraccess', 'usrtype');
            }
            fputcsv($handle, $header);
            foreach ($data->result_array() as $d) {
                fputcsv($handle, $d);
            }
            fclose($handle);
            $count++;
        }
        $fileObj = fopen('ExTmp1.csv', "rt");
        $File = "ExTmp1.txt";
        $Handle = fopen($File, 'w');
        while (($line = fgets($fileObj))) // by default this will read one line at a time
        {

            $line = $line . "\r\n";
            fwrite($Handle, $line);
        }
        $zip->addFile($File);
        $fileObj2 = fopen('ExTmp2.csv', "rt");
        $File2 = "ExTmp2.txt";
        $Handle = fopen($File2, 'w');
        while (($line = fgets($fileObj2))) // by default this will read one line at a time
        {

            $line = $line . "\r\n";
            fwrite($Handle, $line);
        }
        $zip->addFile($File2);
        $zip->close();
        if (is_file('ExTmp1.csv')) {
            unlink('ExTmp1.csv');
            unlink('ExTmp2.csv');
        }

        if (is_file('ExTmp1.txt')) {
            unlink('ExTmp1.txt');
            unlink('ExTmp2.txt');
        }
        header('Content-Type: application/zip');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $zip_name . "\"");
        header('Expires: 0');
        readfile($zip_name);
        exit;
    }
}
