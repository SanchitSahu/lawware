<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Injury extends CI_Controller
{
    public $case_cards;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $this->load->model('injury_model');
        $this->load->model('case_model');
        $data = array();
        $this->data['menu'] = 'injury';
        $this->data['loginUser'] = $this->session->userdata('user_data')['username'];
        $this->data['username'] = $this->session->userdata('user_data')['username'];
        $this->output->enable_profiler(false);
    }
    public function __destruct()
    {
    }

    public function index($case_no = '')
    {
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $this->data['display_details'] = '';
        $case_details = $this->case_model->getCaseFullDetails($case_no);


        $this->data['display_details'] = $case_details[0];
        $this->data['display_details'] = $case_details[0];
        if (!empty($this->data['display_details']->atty_hand)) {
            $attorneyname = $this->case_model->getAttNm($this->data['display_details']->atty_hand);
        } else {
            $attorneyname = '';
        }
        //echo '<pre>'; print_r($case_details[0]); exit;
        $this->data['attorneyname'] = $attorneyname;
        $this->data['pageTitle'] = 'Injury ';
        $this->data['case_no'] = $case_no;
        $this->data['injury_cnt'] = $this->injury_model->getInjuryListing($case_no);
        $this->data['saluatation'] = explode(',', $this->data['system_data'][0]->popsal);
        $this->load->view('common/header', $this->data);
        $this->load->view('injury/injury');
        $this->load->view('common/footer');
    }

}
