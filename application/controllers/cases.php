<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cases extends CI_Controller {

    public $case_cards;

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->common_functions->checkLogin();
        $this->common_functions->checkSessionId();
        $this->load->model('case_model');
        $this->load->model('calendar_model');
        $this->load->model('common_model');
        $this->load->model('email_model');
        $data = array();
        $this->data['menu'] = 'cases';
        $this->data['loginUser'] = $this->session->userdata('user_data')['username'];
        $this->data['username'] = $this->session->userdata('user_data')['username'];
        $this->data['url_seg'] = $this->uri->segment(1);
        $this->output->enable_profiler(false);
    }

    public function __destruct() {

    }

    public function index() {
        //  $this->common_model->update_note();
        $this->data['pageTitle'] = 'Search Cases';
        //$this->data['latest_tasks'] = $this->common_functions->getlatestTask();
        $this->data['system_data'] = $this->common_functions->getsystemData();
        $filters = $this->searchCases();
        $this->data['filters'] = $filters;
        $this->data['languageList'] = $this->config->item('languageList');
        //  $this->data['saluatation'] = $this->config->item('saluatation');
        // $this->data['suffix'] = $this->config->item('suffix');
        // $this->data['title'] = $this->config->item('title');
        // $this->data['category'] = $this->config->item('category');
        $this->data['saluatation'] = explode(',', $this->data['system_data'][0]->popsal);
        $this->data['suffix'] = explode(',', $this->data['system_data'][0]->rolosuffix);
        $this->data['title'] = explode(',', $this->data['system_data'][0]->poptitle);
        $this->data['category'] = explode(',', $this->data['system_data'][0]->popclient);
        $this->data['specialty'] = explode(',', $this->data['system_data'][0]->specialty);
        $main1 = json_decode($this->data['system_data'][0]->main1);
        $this->data['Venue'] = explode(',', $main1->venue);
        $this->data['staff_list'] = $this->common_functions->get_staff_listing_Case();
        $this->load->view('common/header', $this->data);
        $this->load->view('cases/cases');
        $this->load->view('common/casesmodals');
        $this->load->view('common/footer');
    }

    public function searchCases() {
        if (!isset($this->data['system_data'])) {
            $this->data['system_data'] = $this->common_functions->getsystemData();
            $filters = $this->case_model->getFilterArray($this->data['system_data']);
        } else {
            $filters = $this->case_model->getFilterArray($this->data['system_data']);
        }
        return $filters;
    }

    public function getSearchCase($shortArr) {
        switch (trim($shortArr[0])) {
            case "F":
                $searcharray['searchBy'] = 'cd.first';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "C":
                $searcharray['searchBy'] = 'cd2.firm';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "Y":
                $searcharray['searchBy'] = 'c.yourfileno';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "S":
                $searcharray['searchBy'] = 'cd.social_sec';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "D":
                $searcharray['searchBy'] = 'c.followup';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "P":
                $searcharray['searchBy'] = 'cd.contact';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "H":
                $searcharray['searchBy'] = 'cd.first';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "B":
                $searcharray['searchBy'] = 'cd.birth_date';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            case "M":
                $searcharray['searchBy'] = 'cd.birth_month';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
            default:
                $searcharray['searchBy'] = 'cd.last';
                $searcharray['searchText'] = trim($shortArr[1]);
                break;
        }

        return $searcharray;
    }

    public function getDashboardCaseDataAjax() {

        extract($_POST);
        
        $filterArray = array();

        $orCondition = '';
        $filterArray['c.case_status'] = '1';
        if ($search['value'] != '') {

            parse_str($search['value'], $searcharray);

            if (isset($searcharray['searchBy']) && $searcharray['searchBy'] != '' && $searcharray['searchText'] != '') {
                $isShortcut = false;
                $shortTxt = '';
                if ($searcharray['searchBy'] == 'shortcuts') {
                    $shortArr = explode('~', $searcharray['searchText']);

                    if (count($shortArr) > 1) {
                        if (count($shortArr) > 2) {
                            $shortArr[1] = trim($shortArr[1]) . "~" . trim($shortArr[2]);
                        }
                        $searcharray = $this->getSearchCase($shortArr);
                    } else {
                        $searcharray['searchBy'] = 'cd.last';
                        $searcharray['searchText'] = $shortArr[0];
                    }
                }

                if ($searcharray['searchBy'] == 'cd.birth_date') {
                    $searchContent = explode('~', $searcharray['searchText']);
                    $filterArray['cd.birth_date >='] = date('Y-m-d 00:00:00', strtotime($searchContent[0]));
                    if (count($searchContent) > 1) {
                        $filterArray['cd.birth_date <='] = date('Y-m-d 23:59:59', strtotime($searchContent[1]));
                    } else {
                        $filterArray['cd.birth_date <='] = date('Y-m-d 23:59:59', strtotime($searchContent[0]));
                    }
                } else if ($searcharray['searchBy'] == 'cd.contact') {
                    $orCondition = '(cd.home like "%' . $searcharray['searchText'] . '%" OR cd.business like "%' . $searcharray['searchText'] . '%")';
                } else if ($searcharray['searchBy'] == 'cd.birth_month') {
                    if ($searcharray['searchText'] != '0') {
                        $filterArray['MONTH(cd.birth_date)'] = $searcharray['searchText'];
                    }
                } else if ($searcharray['searchBy'] == 'c.followup' || $searcharray['searchBy'] == 'c.dateenter' || $searcharray['searchBy'] == 'c.dateopen' || $searcharray['searchBy'] == 'c.dateclosed') {
                    if (strpos($searcharray['searchText'], "+") !== false) {
                        $filterArray[$searcharray['searchBy'] . ' >='] = date('Y-m-d 00:00:00', strtotime(trim(str_replace("+", "", $searcharray['searchText']))));
                    } else if (strpos($searcharray['searchText'], "-") !== false) {
                        $filterArray[$searcharray['searchBy'] . ' <='] = date('Y-m-d 23:59:59', strtotime(trim(str_replace("-", "", $searcharray['searchText']))));
                    } else if (strpos($searcharray['searchText'], "to") !== false) {
                        $searchContent = explode('to', $searcharray['searchText']);
                        $filterArray[$searcharray['searchBy'] . ' >='] = date('Y-m-d 00:00:00', strtotime(trim($searchContent[0])));
                        $filterArray[$searcharray['searchBy'] . ' <='] = date('Y-m-d 23:59:59', strtotime(trim($searchContent[1])));
                    } else {
                        $filterArray['DATE(' . $searcharray['searchBy'] . ')'] = date('Y-m-d', strtotime($searcharray['searchText']));
                    }
                } else if ($searcharray['searchBy'] == 'c.caseno_less') {
                    $filterArray['c.caseno <'] = $searcharray['searchText'];
                } else if ($searcharray['searchBy'] == 'c.caseno_more') {
                    $filterArray['c.caseno >'] = $searcharray['searchText'];
                } else if ($searcharray['searchBy'] == 'c.caseno') {
                    $filterArray[$searcharray['searchBy']] = $searcharray['searchText'];
                } else if ($searcharray['searchBy'] == 'cd.last') {
                    $lastNameArray = explode(",", $searcharray['searchText']);

                    $lastName = $lastNameArray[0];
                    $filterArray['cd.last like '] = trim($lastName) . "%";

                    if (count($lastNameArray) > 1) {
                        $filterArray['cd.first like'] = trim($lastNameArray[1]) . "%";
                        // $arr1 = str_split($lastNameArray[1]);
                        // $filterArray['cd.first like'] = $arr1[0] . "%";
                        // $filterArray['cd.last like'] = $arr1[1] . "%";
                    }
                } else if ($searcharray['searchBy'] == 'cd.first') {
                    $firstNameArray = explode(",", $searcharray['searchText']);

                    $firstName = $firstNameArray[0];
                    $filterArray['cd.first like '] = trim($firstName) . "%";

                    if (count($firstNameArray) > 1) {
                        $filterArray['cd.last like'] = trim($firstNameArray[1]) . "%";
                        // $arr1 = str_split($lastNameArray[1]);
                        // $filterArray['cd.first like'] = $arr1[0] . "%";
                        // $filterArray['cd.last like'] = $arr1[1] . "%";
                    }
                } else if ($searcharray['searchBy'] == 'c.casestat') {
                    $filterArray['c.casestat LIKE '] = trim($searcharray['searchText']);
                } else {
                    $filterArray[$searcharray['searchBy'] . ' like'] = $searcharray['searchText'] . "%";
                }
            }

            if (isset($searcharray['casetype']) && $searcharray['casetype'] != '') {
                $searchArrtp = explode('~', $searcharray['casetype']);
                $searchTp = $searchArrtp[0];
                $searchTpVal = $searchArrtp[1];
                if ($searchTp != 'type') {
                    $filterArray['c.' . $searchTp] = $searchTpVal;
                } else {
                    $filterArray['cc.' . $searchTp] = $searchTpVal;
                }
            }

            if (isset($searcharray['atty_resp']) && $searcharray['atty_resp'] != 'atty_resp-' && $searcharray['atty_resp'] != '') {
                $searchArr = explode('~', $searcharray['atty_resp']);
                $searchKey = $searchArr[0];
                $searchVal = $searchArr[1];
                $filterArray['c.' . $searchKey] = $searchVal;
            }

            if (isset($searcharray['deleted_cases'])) {
                unset($filterArray['c.case_status']);
            }
        }
        if(!isset($searcharray['valueRecent']) || empty($searcharray['valueRecent']) || $searcharray['valueRecent'] == 'recent'){
            $whr = array('user' => $this->session->userdata('user_data')['username']);
            $getRecentCase = $this->common_model->getSingleRecordById('recent_cases', 'caseno', $whr);
            $casenos = $getRecentCase->caseno;
        }
        
        $result = $this->case_model->getCaseListing($start, $length, $filterArray, $orCondition, $order ,'', $casenos);

        $finalData = array();

        foreach ($result as $data) {
            $usrnm = "";
            if (!empty($data->last) || !empty($data->first)) {
                if (isset($searcharray['searchBy']) && $searcharray['searchBy'] == 'cd.last') {
                    if ($data->last != "" && $data->first != "") {
                        $usrnm = $data->last . ", " . $data->first;
                    } else if ($data->last != "" && $data->first == "") {
                        $usrnm = $data->last;
                    } else if ($data->last == "" && $data->first != "") {
                        $usrnm = $data->first;
                    }
                } else {
                    if ($data->last != "" && $data->first != "") {
                        $usrnm = $data->last . ", " . $data->first;
                    } else if ($data->first != "" && $data->last == "") {
                        $usrnm = $data->first;
                    } else if ($data->last != "" && $data->first == "") {
                        $usrnm = $data->last;
                    }
                }
            } else {
                $usrnm = '';
            }


            $link = '<a target="_blank" href = "' . base_url() . 'cases/documents/' . $data->caseno . '">Document</a>';

            $tmpArr = array(
                $data->caseno,
                $usrnm,
                $data->atty_resp,
                $data->atty_hand,
                $data->casetype,
                $data->casestat,
                $link,
                $data->protected,
                $data->case_status
            );
            array_push($finalData, $tmpArr);
        }

        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $this->case_model->getCaseCount(),
            'recordsFiltered' => $this->case_model->getCaseCount($filterArray, $orCondition, '',$casenos ),
            'data' => $finalData,
        );
        //echo '<pre>'; print_r($listingData); exit;
        die(json_encode($listingData));
    }

    public function getCaseDataAjax() {

        extract($_POST);

        $filterArray = array();

        $orCondition = '';
        $filterArray['c.case_status'] = '1';
        $unique = "";
        if ($search['value'] != '') {

            parse_str($search['value'], $searcharray);

            if (isset($searcharray['searchBy']) && $searcharray['searchBy'] != '' && $searcharray['searchText'] != '') {
                $isShortcut = false;
                $shortTxt = '';
                if ($searcharray['searchBy'] == 'shortcuts') {
                    $shortArr = explode('~', $searcharray['searchText']);

                    if (count($shortArr) > 1) {
                        if (count($shortArr) > 2) {
                            $shortArr[1] = trim($shortArr[1]) . "~" . trim($shortArr[2]);
                        }
                        $searcharray = $this->getSearchCase($shortArr);
                    } else {
                        $searcharray['searchBy'] = 'cd.last';
                        $searcharray['searchText'] = $shortArr[0];
                    }
                }

                if ($searcharray['searchBy'] == 'cd.birth_date') {
                    $searchContent = explode('~', $searcharray['searchText']);
                    $filterArray['cd.birth_date >='] = date('Y-m-d 00:00:00', strtotime($searchContent[0]));
                    if (count($searchContent) > 1) {
                        $filterArray['cd.birth_date <='] = date('Y-m-d 23:59:59', strtotime($searchContent[1]));
                    } else {
                        $filterArray['cd.birth_date <='] = date('Y-m-d 23:59:59', strtotime($searchContent[0]));
                    }
                } else if ($searcharray['searchBy'] == 'cd.contact') {
                    $orCondition = '(cd.home like "%' . $searcharray['searchText'] . '%" OR cd.business like "%' . $searcharray['searchText'] . '%")';
                } else if ($searcharray['searchBy'] == 'cd.birth_month') {
                    if ($searcharray['searchText'] != '0') {
                        $filterArray['MONTH(cd.birth_date)'] = $searcharray['searchText'];
                    }
                } else if ($searcharray['searchBy'] == 'c.followup' || $searcharray['searchBy'] == 'c.dateenter' || $searcharray['searchBy'] == 'c.dateopen' || $searcharray['searchBy'] == 'c.dateclosed') {
                    if (strpos($searcharray['searchText'], "+") !== false) {
                        $filterArray[$searcharray['searchBy'] . ' >='] = date('Y-m-d 00:00:00', strtotime(trim(str_replace("+", "", $searcharray['searchText']))));
                    } else if (strpos($searcharray['searchText'], "-") !== false) {
                        $filterArray[$searcharray['searchBy'] . ' <='] = date('Y-m-d 23:59:59', strtotime(trim(str_replace("-", "", $searcharray['searchText']))));
                    } else if (strpos($searcharray['searchText'], "to") !== false) {
                        $searchContent = explode('to', $searcharray['searchText']);
                        $filterArray[$searcharray['searchBy'] . ' >='] = date('Y-m-d 00:00:00', strtotime(trim($searchContent[0])));
                        $filterArray[$searcharray['searchBy'] . ' <='] = date('Y-m-d 23:59:59', strtotime(trim($searchContent[1])));
                    } else {
                        $filterArray['DATE(' . $searcharray['searchBy'] . ')'] = date('Y-m-d', strtotime($searcharray['searchText']));
                    }
                } else if ($searcharray['searchBy'] == 'c.caseno_less') {
                    $filterArray['c.caseno <'] = $searcharray['searchText'];
                } else if ($searcharray['searchBy'] == 'c.caseno_more') {
                    $filterArray['c.caseno >'] = $searcharray['searchText'];
                } else if ($searcharray['searchBy'] == 'c.caseno') {
                    $filterArray[$searcharray['searchBy']] = $searcharray['searchText'];
                } else if ($searcharray['searchBy'] == 'cd.last') {
                    $lastNameArray = explode(",", $searcharray['searchText']);

                    $lastName = $lastNameArray[0];
                    $filterArray['cd.last like '] = trim($lastName) . "%";

                    if (count($lastNameArray) > 1) {
                        $filterArray['cd.first like'] = trim($lastNameArray[1]) . "%";
                    }
                } else if ($searcharray['searchBy'] == 'cd.first') {
                    $firstNameArray = explode(",", $searcharray['searchText']);

                    $firstName = $firstNameArray[0];
                    $filterArray['cd.first like '] = trim($firstName) . "%";

                    if (count($firstNameArray) > 1) {
                        $filterArray['cd.last like'] = trim($firstNameArray[1]) . "%";
                    }
                } else if ($searcharray['searchBy'] == 'c.casestat') {
                    $filterArray['c.casestat LIKE '] = trim($searcharray['searchText']);
                } else {
                    $filterArray[$searcharray['searchBy'] . ' like'] = $searcharray['searchText'] . "%";
                }
            }

            if (isset($searcharray['casetype']) && $searcharray['casetype'] != '') {
                $searchArrtp = explode('~', $searcharray['casetype']);
                $searchTp = $searchArrtp[0];
                $searchTpVal = $searchArrtp[1];
                if ($searchTp != 'type') {
                    $filterArray['c.' . $searchTp] = $searchTpVal;
                } else {
                    $filterArray['cc.' . $searchTp] = $searchTpVal;
                }
            }
            if (isset($searcharray['atty_resp']) && $searcharray['atty_resp'] != 'atty_resp-' && $searcharray['atty_resp'] != '') {
                $searchArr = explode('~', $searcharray['atty_resp']);
                $searchKey = $searchArr[0];
                $searchVal = $searchArr[1];
                $filterArray['c.' . $searchKey] = $searchVal;
            }

            if (isset($searcharray['deleted_cases'])) {
                unset($filterArray['c.case_status']);
            }

            if (isset($searcharray['unique_cases']) && $searcharray['unique_cases'] != '') {
                $unique = 1;
            } else {
                $unique = 0;
            }
        }

        $result = $this->case_model->getCaseListing($start, $length, $filterArray, $orCondition, $order, $unique);
        $finalData = array();

        foreach ($result as $data) {
            $usrnm = "";
            if (!empty($data->last) || !empty($data->first)) {
                if (isset($searcharray['searchBy']) && $searcharray['searchBy'] == 'cd.last') {
                    if ($data->last != "" && $data->first != "") {
                        $usrnm = $data->last . ", " . $data->first;
                    } else if ($data->last != "" && $data->first == "") {
                        $usrnm = $data->last;
                    } else if ($data->last == "" && $data->first != "") {
                        $usrnm = $data->first;
                    }
                } else {
                    if ($data->last != "" && $data->first != "") {
                        $usrnm = $data->last . ", " . $data->first;
                    } else if ($data->first != "" && $data->last == "") {
                        $usrnm = $data->first;
                    } else if ($data->last != "" && $data->first == "") {
                        $usrnm = $data->last;
                    }
                }
            } else {
                $usrnm = '';
            }
            $followupdate = explode(" ", $data->followup);
            if ($followupdate[0] == '1899-12-30') {
                $followupdate = '-';
            } else {
                $followupdate = $followupdate[0];
            }
            $dateenter = explode(" ", $data->dateenter);
            if ($dateenter[0] == '1899-12-30') {
                $dateenter = '-';
            } else {
                $dateenter = $dateenter[0];
            }
            $dateopen = explode(" ", $data->dateopen);
            if ($dateopen[0] == '1899-12-30') {
                $dateopen = '-';
            } else {
                $dateopen = $dateopen[0];
            }
            $dateclosed = explode(" ", $data->dateclosed);
            if ($dateclosed[0] == '1899-12-30') {
                $dateclosed = '-';
            } else {
                $dateclosed = $dateclosed[0];
            }

            $address = "";
            $address .= trim($data->address1);

            $address .= ($data->address2 != '') ? ", " . $data->address2 : "";

            $address .= ($address != "") ? ",<br>" : '';

            $address .= ($data->city != '') ? $data->city : '';

            $address .= ($data->state != '') ? ", " . $data->state : "";

            $address .= ($data->zip != '') ? ", " . $data->zip : "";

            $address = trim($address);
            $address = trim($address, '<br>');
            $address = trim($address, ',');

            $tmpArr = array(
                $data->caseno,
                $usrnm,
                $data->atty_resp,
                $data->atty_hand,
                $data->casetype,
                $data->casestat,
                $data->social_sec,
                $data->firm,
                $followupdate,
                $data->home,
                $data->yourfileno,
                str_replace(" 00:00:00", '', $data->birth_date),
                $dateenter,
                $dateopen,
                $dateclosed,
                $data->type,
                $data->cardcode,
                $data->caption1,
                $address,
                $data->protected,
                $data->case_status
            );

            array_push($finalData, $tmpArr);
        }

        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $this->case_model->getCaseCount(),
            'recordsFiltered' => $this->case_model->getCaseCount($filterArray, $orCondition, $unique),
            'data' => $finalData,
        );
        //echo '<pre>'; print_r($listingData); exit;
        die(json_encode($listingData));
    }

    public function getCaseDetails() {
        extract($_POST);
        $searchRecord = explode("-", $id);

        $next_calendar_event = $this->case_model->getNextCalendarEvent($id);

        $date = $next_calendar_event->date;
        $eventdate = date('d,M Y h:i:s A', strtotime($date));

        $record = $this->case_model->getCaseDetailsByIdNew($searchRecord);
        if (!empty($record)) {
            $event_activity = $this->case_model->getEventActivity($record->caseno);
            $record->event = $event_activity[0]->event;
        }

        if ($record->followup == '1899-12-30 00:00:00') {
            $followupdate = 'NA';
        } else {
            $followupdate = $record->followup . ' - Court Hearing';
        }
        $data = '<div id="data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <a href="javascript:void(0);" id="close" class="glyphicon glyphicon-remove" style="color: #C7C3C3"></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <span><strong>CaseNo:</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <span><strong>' . $record->caseno . '</strong></span>
                        </div>
                        <div class="col-sm-4">
                            <span><strong>Name:</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <span>' . ((isset($record->first) && $record->first != '') ? ($record->first . " " . $record->last) : 'NA') . '</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span><strong>Card Type:</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <span>' . ((isset($record->type) && $record->type != '') ? ($record->type) : 'NA') . '</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span><strong>Phone:</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <span>' . ((isset($record->home) && $record->home != '') ? ($record->home) : 'NA') . '</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span><strong>Type of Case:</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <span>' . ((isset($record->casetype) && $record->casetype != '') ? ($record->casetype) : 'NA') . '</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span><strong>Case Status:</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <span>' . ((isset($record->casestat) && $record->casestat != '') ? ($record->casestat) : 'NA') . '</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span><strong>Staff:</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <span>' . ((isset($record->atty_resp) && $record->atty_resp != '') ? ($record->atty_resp) : 'NA') . '</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span><strong>Next Appointment:</strong></span>
                        </div>
                        <div class="col-sm-8">';
        /*  <span>' . str_replace(' 00:00:00', '', $followupdate) . '</span> */

        if ($next_calendar_event) {
            $data .= '<span><strong>' . $eventdate . '</strong><br /><strong>Event :</strong> ' . $next_calendar_event->event . ', <strong>Event Venue : </strong>' . $next_calendar_event->venue . ', <strong>Event Attorney : </strong>' . $next_calendar_event->attyass . '</span></div>';
        } else {
            $data .= '<span>No Event Available</span>';
        }
        $data .= '</div></div>

                    <div class="row">
                        <div class="col-sm-4">
                            <span><strong>Quick Notes</strong></span>
                        </div>
                        <div class="col-sm-8">
                            <textarea style="width:300px; height:200px;" readonly>' . ((isset($record->event) && $record->event != '') ? ($record->event) : 'NA') . '</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <a class="btn pull-right marg-top15 btn-sm btn-primary" href="' . base_url('cases/case_details/' . $id) . '">More Details</a>
                        </div>
                    </div>
                </div>
            </div>';
        echo $data;
    }

    public function case_injurysummary($case_id = 0) {

        if ($this->input->post('caseno') != null) {
            $case_id = $this->input->post('caseno');
        }
        $bodyParts = json_decode(bodyParts);

        $case_summary_injury = $this->case_model->getInjuryListing($case_id);
        $cond = array('caseno' => $case_id);

        $injury_caption = $this->common_functions->getRecordById('case', 'caption1', $cond);

        $html = '<div class="panel-body" style="background-color:#fff;">
                            <div class="ibox float-e-margins" style="background-color:#fff;">';

        if (count($case_summary_injury) > 0) {
            $ij = 0;
            $cnt = 1;

            $html .= '<div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="ibox-title">
                                                             <h5>';
            if ($case_summary_injury[0]->caseno) {
                $html .= '(Case ' . $case_summary_injury[0]->caseno . ') ';
            }if ($case_summary_injury[0]->first) {
                $html .= $case_summary_injury[0]->first;
            }if ($case_summary_injury[0]->last) {
                $html .= ' ' . $case_summary_injury[0]->last;
            }$html .= '  - Injury Information</h5>
                                                        </div>
                                                          <hr />';

            foreach ($case_summary_injury as $key => $val) {

                $html .= '<div class="row">
                                            <div class="col-lg-4">
                                                        <div class="ibox-content marg-bot15">';

                if ($val->e_name) {
                    $html .= '<strong class="colorblack">';
                    $html .= strtoupper($val->e_name);
                    $html .= '</strong><br /><br />';
                }
                if ($val->adj1c) {
                    $html .= '<div><strong class="colorblack">DOI : </strong> <span  class="colorgrey">';
                    $html .= $val->adj1c;
                    $html .= '</span></div>';
                }
                if ($val->aoe_coe_status) {

                    $html .= '<div><strong class="colorblack">AOE/COE Status : </strong> <span class="colorgrey">';
                    $html .= $val->aoe_coe_status;
                    $html .= '</span></div>';
                }
                $html .= '</div>
                                                                        </div>
                                                                        <div class="col-lg-4">

                                                                    <div class="ibox-content marg-bot15">';

                if ($val->i_name) {
                    $html .= '<strong class="colorblack">';
                    $html .= strtoupper($val->i_name);
                    $html .= '</strong><br /><br />';
                }
                if ($val->case_no) {
                    $html .= '<div><strong class="colorblack">WCAB No. : </strong>  <span class="colorgrey">';
                    $html .= $val->case_no;
                    $html .= '</span></div>';
                }
                if ($val->eamsno) {
                    $html .= '<div><strong class="colorblack">EAMS # </strong>  <span class="colorgrey">';
                    $html .= 'ADJ' . $val->eamsno;
                    $html .= '</span></div>';
                }
                if ($val->app_status) {
                    $html .= '<div><strong class="colorblack">Status : </strong>  <span class="colorgrey">';
                    $html .= $val->app_status;
                    $html .= '</span></div>';
                }
                $html .= '</div>
                                                                        </div>
                                                                        <div class="col-lg-4">

                                                                    <div class="ibox-content marg-bot15">';

                if ($val->d1_firm) {
                    $html .= '<strong class="colorblack">';
                    $html .= strtoupper($val->d1_firm);
                    $html .= '</strong><br /><br />';
                }
                if ($val->i_claimno) {
                    $html .= '<div><strong class="colorblack">Cln # </strong>  <span class="colorgrey">';
                    $html .= $val->i_claimno;
                    $html .= '</span></div>';
                }
                if ($val->adj1e) {
                    $html .= '<div><strong class="colorblack">POB. : </strong> <span class="colorgrey">';
                    $pob = '';
                    /*foreach ($bodyParts as $key => $value) {

                        if (!empty($val->pob1) && $key == $val->pob1) {
                            $pob .= $key . ' ' . $value . ",";
                        }

                        if (!empty($val->pob2) && $key == $val->pob2) {
                            $pob .= $key . ' ' . $value . ",";
                        }

                        if (!empty($val->pob3) && $key == $val->pob3) {
                            $pob .= $key . ' ' . $value . ",";
                        }

                        if (!empty($val->pob4) && $key == $val->pob4) {
                            $pob .= $key . ' ' . $value . ",";
                        }

                        if (!empty($val->pob5) && $key == $val->pob5) {
                            $pob .= $key . ' ' . $value . ",";
                        }
                    }*/

                    if (!empty($val->adj1e)) {
                        $pob = $val->adj1e;
                    }

                    $html .= $pob;
                    $html .= '</span></div>';
                }
                $html .= '</div>
                                                                        </div>
                                                                       <div class="clearfix">&nbsp;</div>
                                                                       <div class="col-lg-12">
                                                                           <ul>';
                if ($val->fld1) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">Injury Status : </strong> <span class="colorgrey">';
                    $html .= $val->fld1;
                    $html .= '</span></li>';
                }
                if ($val->fld2) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">5402 Date (POS+93) : </strong> <span class="colorgrey">';
                    $html .= $val->fld2;
                    $html .= '</span></li>';
                }
                if ($val->fld3) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">4850 Start Date : </strong> <span class="colorgrey">';
                    $html .= $val->fld3;
                    $html .= '</span></li>';
                }
                if ($val->fld4) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">4658(d) Date (P&S+60) : </strong> <span class="colorgrey">';
                    $html .= $val->fld4;
                    $html .= '</span></li>';
                }
                if ($val->fld5) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">Notice of Offer Date : </strong> <span class="colorgrey">';
                    $html .= $val->fld5;
                    $html .= '</span></li>';
                }
                if ($val->fld11) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">Current PTP : </strong> <span class="colorgrey">';
                    $html .= $val->fld11;
                    $html .= '</span></li>';
                }
                if ($val->fld12) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">Prior PTP : </strong> <span class="colorgrey">';
                    $html .= $val->fld12;
                    $html .= '</span></li>';
                }
                if ($val->fld13) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">Prior Awards : </strong> <span class="colorgrey">';
                    $html .= $val->fld13;
                    $html .= '</span></li>';
                }
                if ($val->fld14) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">Prior Attorney : </strong> <span class="colorgrey">';
                    $html .= $val->fld14;
                    $html .= '</span></li>';
                }
                if ($val->fld20) {
                    $html .= '<li style="list-style-type: disc;"><strong class="colorblack">Add&#10076;l Notes : </strong> <span class="colorgrey">';
                    $html .= $val->fld20;
                    $html .= '</span></li>';
                }
                $html .= '</ul>
                                                                       </div>
                                                                    </div>
                                                                            <br />
                                                                   <div style="border: solid 1px;"></div>
                                                                   <br />';

                $cnt++;
                $ij++;
            }
            if ($case_summary_injury[0]->caseno) {
                $html .= '<div style="padding: 10px;line-height: 20px;"><strong class="colorblack">Case Number : </strong> <span class="colorgrey">';
                $html .= $case_summary_injury[0]->caseno;
                $html .= '</span></div>';
            }
            if ($injury_caption->caption1) {
                $html . '<div style="padding: 10px;line-height: 20px;">';
                $html .= '<div style="padding-left:10px !important;">' . $injury_caption->caption1 . '</div>';
                $html .= '</div>';
            }
            $html .= '</div>
                                                            </div>';
        } else {

            $html .= '<div class="row">
                                           <div class="col-lg-12 text-center padding-left-none padding-right-none padding-ten">No Injury Record</div>
                                       </div>';
        }
        $html .= '</div>
                        </div>';

        echo $html;
    }

    public function case_details($case_id = 0) {
		$UserCases = $this->case_model->getRecentCases();
		if(count($UserCases) == 0){
			$case = $case_id;
			$addRecent = $this->case_model->addRecentCases($case);
		} else  {
			$caseArr = explode(",",$UserCases[0]->caseno);
			
			$newArray = array_slice(array_diff($caseArr,[$case_id]),0,9);;
			$arrString = implode(",",$newArray);
			$cases = $case_id.','.$arrString;
			$updateRecent = $this->case_model->updateRecentCases(rtrim($cases,','));
		}
		$this->output->enable_profiler(false);
        $metaArray = array();

        if ($this->input->post('caseId') != null) {
            $case_id = $this->input->post('caseId');
        }

        $this->data['system_data'] = $this->common_functions->getsystemData();
        $filters = $this->searchCases();
        $this->data['filters'] = $filters;

        $this->data['display_details'] = '';
        $this->data['case_no'] = $case_id;
        $this->data['filterArray'] = $this->calendar_model->getFilterArray();
        $users = array();
        $case_details = $this->case_model->getCaseFullDetails($case_id);
        if($case_details) {
            /*echo $case_details[0]->mainVenue; exit;*/
            if (!empty($case_details[0]->mainVenue)) {
                $MainVenue = $this->case_model->getVenue($case_details[0]->mainVenue);
            } else {
                $MainVenue = '';
            }

            if ($MainVenue != '') {
                $case_details[0]->Displayvenue = $MainVenue[0]->venue != '' ? $MainVenue[0]->venue : $MainVenue[0]->last;
            } else {
                $case_details[0]->Displayvenue = '';
            }

            /*echo '<pre>'; print_r($case_details[0]); exit;*/
            $this->data['pageTitle'] = $case_details[0]->first . " " . $case_details[0]->last;
            $case_injury = $this->case_model->getInjuryListing($case_id);
            $next_calendar_event = $this->case_model->getNextCalendarEvent($case_id);
            $event_activity = $this->case_model->getEventActivity($case_id);
            $this->data['category_array'] = $event_activity[0]->event;
            $this->data['record'] = $this->common_model->getRecord();
            $this->data['Prospect_mainkey'] = $this->common_model->getMainKey();
            $getAllStaff = $this->common_model->getAllRecord('staff', 'DISTINCT(initials)', '', 'initials asc');
            $whr = array('username' => $this->session->userdata('user_data')['username']);
            $getStaffAllInfo = $this->common_model->getSingleRecordById('staff', 'captionAccess,vacation', $whr);

            /*Get name of refered by*/
            if (!empty($case_details[0]->rb)) {
                $getRefName = $this->case_model->getRefName($case_details[0]->rb);
                if (!empty($getRefName[0]->firm)) {
                    $RefName = $getRefName[0]->firm;
                } else {
                    $RefName = $getRefName[0]->first . " " . $getRefName[0]->last;
                }
            } else {
                $RefName = '';
            }
            $this->data['RefName'] = $RefName;
            $checkCaptionAccess->captionAccess = $getStaffAllInfo->captionAccess;
            $this->data['CaptionAccess'] = $checkCaptionAccess;
            /*Get case related emails*/
            $this->data['case_email'] = $this->case_model->getCaseEmailsCount($case_id);
            $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh);
            $this->data['atty_resp'] = explode(',', $this->data['system_data'][0]->popattyr);
            $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh);
            $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh);
            $this->data['staffList'] = $getAllStaff;
            $getcaldata = array();
            $getcaldata[0]->calnotes = $this->data['system_data'][0]->calnotes;
            $getcaldata[0]->caltime = $this->data['system_data'][0]->caltime;
            $getcaldata[0]->calendars = $this->data['system_data'][0]->calendars;
            $getcaldata[0]->calattyass = $this->data['system_data'][0]->calattyass;
            $getcaldata[0]->calview = $this->data['system_data'][0]->calview;
            $this->data['system_caldata'] = $getcaldata;

            $getStaffInfo[0]->vacation = $getStaffAllInfo->vacation;

            $this->data['staff_details'] = $getStaffInfo;
            $getCaseDetails = json_decode($this->data['system_data'][0]->pncdd);

            $this->data['casetype'] = $getCaseDetails->toc != '' ? explode(',', $getCaseDetails->toc) : '';
            $this->data['casetypevalidate'] = (isset($getCaseDetails->chk1) && $getCaseDetails->chk1 == 'on') ? 1 : 0;
            $this->data['casetypedrp'] = (isset($getCaseDetails->drp1) && $getCaseDetails->drp1 == 'on') ? 1 : 0;
            $getCaseDates = json_decode($this->data['system_data'][0]->casedates);
            $this->data['refdt'] = (isset($getCaseDates->refdt) && $getCaseDates->refdt == 'on') ? 1 : 0;
            $this->data['ref'] = (isset($getCaseDates->ref) && $getCaseDates->ref == 'on') ? 1 : 0;
            $this->data['pns'] = (isset($getCaseDates->pns) && $getCaseDates->pns == 'on') ? 1 : 0;
            $this->data['pnsdt'] = (isset($getCaseDates->pnsdt) && $getCaseDates->pnsdt == 'on') ? 1 : 0;

            /*Case Status*/
            $this->data['casestat'] = ($getCaseDetails->status != '') ? explode(',', $getCaseDetails->status) : '';

            $this->data['casestatvalidate'] = (isset($getCaseDetails->chk2) && $getCaseDetails->chk2 == 'on') ? 1 : 0;

            $this->data['casestatusdrp'] = (isset($getCaseDetails->drp2) && $getCaseDetails->drp2 == 'on') ? 1 : 0;

            /*Referral Code*/
            $this->data['referral'] = ($getCaseDetails->refc != '') ? explode(',', $getCaseDetails->refc) : '';
            $this->data['referralvalidate'] = (isset($getCaseDetails->chk3) && $getCaseDetails->chk3 == 'on') ? 1 : 0;
            $this->data['casereferraldrp'] = (isset($getCaseDetails->drp3) && $getCaseDetails->drp3 == 'on') ? 1 : 0;
            /*Referral Source*/
            $this->data['referralsrc'] = ($getCaseDetails->refsrc != '') ? explode(',', $getCaseDetails->refsrc) : '';

            $this->data['referralsrcvalidate'] = (isset($getCaseDetails->chk4) && $getCaseDetails->chk4 == 'on') ? 1 : 0;

            $this->data['referralsrcdrp'] = (isset($getCaseDetails->drp4) && $getCaseDetails->drp4 == 'on') ? 1 : 0;

            /*Referred By*/
            $this->data['referredby'] = ($getCaseDetails->refby != '') ? explode(',', $getCaseDetails->refby) : '';
            $this->data['referredbyvalidate'] = (isset($getCaseDetails->chk5) && $getCaseDetails->chk5 == 'on') ? 1 : 0;
            if (isset($getCaseDetails->chk5) && $getCaseDetails->chk5 == 'on') {
                $this->data['referredbyvalidate'] = 1;
            } else {
                $this->data['referredbyvalidate'] = 0;
            }

            $this->data['referredbydrp'] = (isset($getCaseDetails->drp5) && $getCaseDetails->drp5 == 'on') ? 1 : 0;

            /*Association*/
            $this->data['association'] = ($getCaseDetails->assoc != '') ? explode(',', $getCaseDetails->assoc) : '';
            $this->data['associationvalidate'] = (isset($getCaseDetails->chk6) && $getCaseDetails->chk6 == 'on') ? 1 : 0;
            $this->data['associationdrp'] = (isset($getCaseDetails->drp6) && $getCaseDetails->drp6 == 'on') ? 1 : 0;
            /*Referred To*/
            $this->data['referredto'] = ($getCaseDetails->refto != '') ? explode(',', $getCaseDetails->refto) : '';
            $this->data['referredtovalidate'] = (isset($getCaseDetails->chk7) && $getCaseDetails->chk7 == 'on') ? 1 : 0;
            $this->data['referredtodrp'] = (isset($getCaseDetails->drp7) && $getCaseDetails->drp7 == 'on') ? 1 : 0;
            /*Our Ref Status*/
            $this->data['referalstatus'] = ($getCaseDetails->ors != '') ? explode(',', $getCaseDetails->ors) : '';
            $this->data['rstatusvalidate'] = (isset($getCaseDetails->chk8) && $getCaseDetails->chk8 == 'on') ? 1 : 0;
            $this->data['rstatusdrp'] = (isset($getCaseDetails->drp8) && $getCaseDetails->drp8 == 'on') ? 1 : 0;
            /*Mailinag list title 1 - 6 Start*/
            $this->data['mtitle1'] = ($getCaseDetails->t1 != '') ? $getCaseDetails->t1 : '';
            $this->data['mtitleval1'] = (isset($getCaseDetails->d1) && $getCaseDetails->d1 == 'on') ? 1 : 0;
            $this->data['mtitle2'] = ($getCaseDetails->t2 != '') ? $getCaseDetails->t2 : '';
            $this->data['mtitleval2'] = (isset($getCaseDetails->d2) && $getCaseDetails->d2 == 'on') ? 1 : 0;
            $this->data['mtitle3'] = ($getCaseDetails->t3 != '') ? $getCaseDetails->t3 : '';
            $this->data['mtitleval3'] = (isset($getCaseDetails->d3) && $getCaseDetails->d3 == 'on') ? 1 : 0;
            $this->data['mtitle4'] = ($getCaseDetails->t4 != '') ? $getCaseDetails->t4 : '';
            $this->data['mtitleval4'] = (isset($getCaseDetails->d4) && $getCaseDetails->d4 == 'on') ? 1 : 0;
            $this->data['mtitle5'] = ($getCaseDetails->t5 != '') ? $getCaseDetails->t5 : '';
            $this->data['mtitleval5'] = (isset($getCaseDetails->d5) && $getCaseDetails->d5 == 'on') ? 1 : 0;
            $this->data['mtitle6'] = ($getCaseDetails->t6 != '') ? $getCaseDetails->t6 : '';
            $this->data['mtitleval6'] = (isset($getCaseDetails->d6) && $getCaseDetails->d6 == 'on') ? 1 : 0;
           /*$cat_array = array();*/
            $message1 = '';
            $message2 = '';
            $message3 = '';
            $message4 = '';
            foreach ($event_activity as $k => $v) {
                if ($v->category == 1000002) {
                    $message1 = $v->event;
                }
                if ($v->category == 1000005) {
                    $message2 = $v->event;
                }
                if ($v->category == 1000006) {
                    $message3 = $v->event;
                }
                if ($v->category == 1000007) {
                    $message4 = $v->event;
                }
            }

           /*$this->data['category_array'] = $cat_array;*/
            $this->data['message1'] = $message1;
            $this->data['message2'] = $message2;
            $this->data['message3'] = $message3;
            $this->data['message4'] = $message4;

            if (isset($next_calendar_event->date) && $next_calendar_event->date != '') {
                $this->data['next_calendar_event'] = date('d M Y', strtotime($next_calendar_event->date));
                $this->data['next_calendar_event_tym'] = date('h:i A', strtotime($next_calendar_event->date));
            }
            $this->data['next_calendar_event_event'] = (isset($next_calendar_event->event) && $next_calendar_event->event != '') ? $next_calendar_event->event : '---';
            $this->data['next_calendar_event_venue'] = (isset($next_calendar_event->venue) && $next_calendar_event->venue != '') ? $next_calendar_event->venue : '---';
            $this->data['next_calendar_event_from'] = (isset($next_calendar_event->attyass) && $next_calendar_event->attyass != '') ? $next_calendar_event->attyass : '---';

            $this->data['case_cards'] = $case_details;

            if ($case_details != '') {
                foreach ($case_details as $data) {
                    if (!isset($users[strtolower($data->cardtype)])) {
                        $users[strtolower($data->cardtype)] = $data;
                    }
                }
            } else {
                $case_details = array();
                $defaultArray = $this->setBlankCase();
                $case_details = $this->case_model->getCaseDataByCaseId($case_id);
                $case_details[0] = (object) array_merge($defaultArray, $case_details[0]);
            }

            $this->data['display_details'] = $case_details[0];
            $main1 = json_decode($this->data['system_data'][0]->main1);
            $priparty = (explode(',', $main1->dpartytuse));
            $this->data['Venue'] = explode(',', $main1->venue);

            $calDef = json_decode($systemdata[0]->caldeflt);
            $calDef = json_decode($this->data['system_data'][0]->caldeflt);
            $calDefAtty = '';
            $calDefAttass = '';

            $calYrRng = $calDef->yeartud;
            if (isset($main1->uaaafcase) && $main1->uaaafcase = 'on') {
                if (!empty($case_details[0]->atty_resp)) {
                    $calDefAtty = $case_details[0]->atty_resp;
                }

                if (!empty($case_details[0]->atty_hand)) {
                    $calDefAttass = $case_details[0]->atty_hand;
                }
            } else {
                if (isset($calDef->udifa) && $calDef->udifa = 'on') {
                    $calDefAtty = $this->session->userdata('user_data')['initials'];
                }
                if (isset($calDef->udifaa) && $calDef->udifaa = 'on') {
                    $calDefAttass = $this->session->userdata('user_data')['initials'];
                }
            }
            $this->data['calYrRng'] = $calYrRng;
            $this->data['calDefAtty'] = $calDefAtty;
            $this->data['calDefAttass'] = $calDefAttass;
            $this->data['types'] = $this->common_functions->getAllRecord('card', 'DISTINCT(type)', 'type != ""', 'type asc');
            $this->data['languageList'] = $this->config->item('languageList');
            $this->data['saluatation'] = explode(',', $this->data['system_data'][0]->popsal);
            $this->data['suffix'] = explode(',', $this->data['system_data'][0]->rolosuffix);
            $this->data['title'] = explode(',', $this->data['system_data'][0]->poptitle);
            $this->data['category'] = explode(',', $this->data['system_data'][0]->popclient);
            $this->data['specialty'] = explode(',', $this->data['system_data'][0]->specialty);
            $this->data['sub_type'] = json_decode(sub_type, true);
            $this->data['side'] = explode(',', $main1->side);
            $this->data['JudgeName'] = (explode(',', $main1->judge));
            $this->data['Venue'] = explode(',', $main1->venue);
            $this->data['users'] = $users;
            $this->data['casetypedd'] = $this->data['system_data'][0]->casetypedd;
            $this->data['casestatdd'] = $this->data['system_data'][0]->casestatdd;
            $this->data['cdstaffdd'] = $this->data['system_data'][0]->cdstaffdd;

            $this->data['case_activity'] = $case_activity;
            $this->data['case_id'] = $case_id;
            $this->data['injury_details'] = $case_injury;
            $this->data['injury_cnt'] = count($case_injury);
            $case_intake = $this->common_model->getProspectId($case_id);
            /*echo '<pre>'; print_r($case_intake); exit;*/
            $this->data['Prospect_data'] = $case_intake;
            $this->data['birthdaynotification'] = '';
            if ($this->data['display_details']->birth_date) {
                $bdaynoti = json_decode($systemdata[0]->birthday, true);
                $this->data['birthdaynotification'] = $this->birthdateNotification($case_id, $this->data['display_details']->birth_date, $bdaynoti['before'], $bdaynoti['after']);
            }
            
            $case_details_atn = $this->case_model->getCaseFullDetails2($case_id);
            if ($case_details_atn != '') {
                foreach ($case_details_atn as $data) {
                    if (!isset($users[strtolower($data->cardtype)])) {
                        if ($data->cardcode != 853)
                            $users[strtolower($data->cardtype)] = $data;
                    }
                }
            } else {
                $case_details_atn = array();
                $defaultArray = $this->setBlankCase();
                $case_details_atn = $this->case_model->getCaseDataByCaseId($case_id);
                $case_details_atn[0] = (object) array_merge($defaultArray, $case_details[0]);
            }
            
            $metaArray['case_no'] = $case_id;
            $metaArray['display_details'] = $this->data['display_details'];
            /*echo '<pre>'; print_r($data); exit;*/
            $this->data['users'] = $users;
            $metaArray['users'] = $this->data['users'];
            $metaArray['injury_details'] = $case_injury;
            $metaArray['client_defendant'] = $this->data['client_defendant'];
            $metaArray['display_all_details'] = $case_details_atn;
            $metaArray['system_data'] = $systemdata;

            $case_information = $this->removeBlankArray($metaArray);

            $metadata = array('staff' => $this->data['loginUser'], 'data' => json_encode($case_information), 'caseno' => $case_id);

            $this->common_functions->insertMetadata($metadata);

            if ($this->input->post('caseId') != null) {
                echo json_encode($this->data);
                die();
            }
           /*if($case_id != 0 || $case_id != "") {*/
            $this->data['staff_list'] = $this->common_functions->get_staff_listing_Case();
           /*}
                   $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);*/
            $getCaseCategory = $this->common_model->getAllRecord('actdeflt', 'actname,category', '', 'category asc');
            $this->data['caseactcategory'] = $getCaseCategory;
            $this->load->view('common/header', $this->data);
            $this->load->view('cases/case_details');
            $this->load->view('common/common_modals');
            $this->load->view('common/footer');
        } else {
            $this->load->view('common/header', $this->data);
            $this->load->view('cases/case_not_found');
            $this->load->view('common/footer');
        }
    }
    
    public function case_details_new($case_id = 0) {
		$UserCases = $this->case_model->getRecentCases();
		if(count($UserCases) == 0){
			$case = $case_id;
			$addRecent = $this->case_model->addRecentCases($case);
		} else  {
			$caseArr = explode(",",$UserCases[0]->caseno);
			
			$newArray = array_slice(array_diff($caseArr,[$case_id]),0,9);;
			$arrString = implode(",",$newArray);
			$cases = $case_id.','.$arrString;
			$updateRecent = $this->case_model->updateRecentCases(rtrim($cases,','));
		}
		$this->output->enable_profiler(false);
        $metaArray = array();

        if ($this->input->post('caseId') != null) {
            $case_id = $this->input->post('caseId');
        }

        $this->data['system_data'] = $this->common_functions->getsystemData();
        $filters = $this->searchCases();
        $this->data['filters'] = $filters;

        $this->data['display_details'] = '';
        $this->data['case_no'] = $case_id;
        $this->data['filterArray'] = $this->calendar_model->getFilterArray();
        $users = array();
        $case_details = $this->case_model->getCaseFullDetails($case_id);
        if($case_details) {
            /*echo $case_details[0]->mainVenue; exit;*/
            if (!empty($case_details[0]->mainVenue)) {
                $MainVenue = $this->case_model->getVenue($case_details[0]->mainVenue);
            } else {
                $MainVenue = '';
            }

            if ($MainVenue != '') {
                $case_details[0]->Displayvenue = $MainVenue[0]->venue != '' ? $MainVenue[0]->venue : $MainVenue[0]->last;
            } else {
                $case_details[0]->Displayvenue = '';
            }

            /*echo '<pre>'; print_r($case_details[0]); exit;*/
            $this->data['pageTitle'] = $case_details[0]->first . " " . $case_details[0]->last;
            $case_injury = $this->case_model->getInjuryListing($case_id);
            $next_calendar_event = $this->case_model->getNextCalendarEvent($case_id);
            $event_activity = $this->case_model->getEventActivity($case_id);
            $this->data['category_array'] = $event_activity[0]->event;
            $this->data['record'] = $this->common_model->getRecord();
            $this->data['Prospect_mainkey'] = $this->common_model->getMainKey();
            $getAllStaff = $this->common_model->getAllRecord('staff', 'DISTINCT(initials)', '', 'initials asc');
            $whr = array('username' => $this->session->userdata('user_data')['username']);
            $getStaffAllInfo = $this->common_model->getSingleRecordById('staff', 'captionAccess,vacation', $whr);

            /*Get name of refered by*/
            if (!empty($case_details[0]->rb)) {
                $getRefName = $this->case_model->getRefName($case_details[0]->rb);
                if (!empty($getRefName[0]->firm)) {
                    $RefName = $getRefName[0]->firm;
                } else {
                    $RefName = $getRefName[0]->first . " " . $getRefName[0]->last;
                }
            } else {
                $RefName = '';
            }
            $this->data['RefName'] = $RefName;
            $checkCaptionAccess->captionAccess = $getStaffAllInfo->captionAccess;
            $this->data['CaptionAccess'] = $checkCaptionAccess;
            /*Get case related emails*/
            $this->data['case_email'] = $this->case_model->getCaseEmailsCount($case_id);
            $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh);
            $this->data['atty_resp'] = explode(',', $this->data['system_data'][0]->popattyr);
            $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh);
            $this->data['atty_hand'] = explode(',', $this->data['system_data'][0]->popattyh);
            $this->data['staffList'] = $getAllStaff;
            $getcaldata = array();
            $getcaldata[0]->calnotes = $this->data['system_data'][0]->calnotes;
            $getcaldata[0]->caltime = $this->data['system_data'][0]->caltime;
            $getcaldata[0]->calendars = $this->data['system_data'][0]->calendars;
            $getcaldata[0]->calattyass = $this->data['system_data'][0]->calattyass;
            $getcaldata[0]->calview = $this->data['system_data'][0]->calview;
            $this->data['system_caldata'] = $getcaldata;

            $getStaffInfo[0]->vacation = $getStaffAllInfo->vacation;

            $this->data['staff_details'] = $getStaffInfo;
            $getCaseDetails = json_decode($this->data['system_data'][0]->pncdd);

            $this->data['casetype'] = $getCaseDetails->toc != '' ? explode(',', $getCaseDetails->toc) : '';
            $this->data['casetypevalidate'] = (isset($getCaseDetails->chk1) && $getCaseDetails->chk1 == 'on') ? 1 : 0;
            $this->data['casetypedrp'] = (isset($getCaseDetails->drp1) && $getCaseDetails->drp1 == 'on') ? 1 : 0;
            $getCaseDates = json_decode($this->data['system_data'][0]->casedates);
            $this->data['refdt'] = (isset($getCaseDates->refdt) && $getCaseDates->refdt == 'on') ? 1 : 0;
            $this->data['ref'] = (isset($getCaseDates->ref) && $getCaseDates->ref == 'on') ? 1 : 0;
            $this->data['pns'] = (isset($getCaseDates->pns) && $getCaseDates->pns == 'on') ? 1 : 0;
            $this->data['pnsdt'] = (isset($getCaseDates->pnsdt) && $getCaseDates->pnsdt == 'on') ? 1 : 0;

            /*Case Status*/
            $this->data['casestat'] = ($getCaseDetails->status != '') ? explode(',', $getCaseDetails->status) : '';

            $this->data['casestatvalidate'] = (isset($getCaseDetails->chk2) && $getCaseDetails->chk2 == 'on') ? 1 : 0;

            $this->data['casestatusdrp'] = (isset($getCaseDetails->drp2) && $getCaseDetails->drp2 == 'on') ? 1 : 0;

            /*Referral Code*/
            $this->data['referral'] = ($getCaseDetails->refc != '') ? explode(',', $getCaseDetails->refc) : '';
            $this->data['referralvalidate'] = (isset($getCaseDetails->chk3) && $getCaseDetails->chk3 == 'on') ? 1 : 0;
            $this->data['casereferraldrp'] = (isset($getCaseDetails->drp3) && $getCaseDetails->drp3 == 'on') ? 1 : 0;
            /*Referral Source*/
            $this->data['referralsrc'] = ($getCaseDetails->refsrc != '') ? explode(',', $getCaseDetails->refsrc) : '';

            $this->data['referralsrcvalidate'] = (isset($getCaseDetails->chk4) && $getCaseDetails->chk4 == 'on') ? 1 : 0;

            $this->data['referralsrcdrp'] = (isset($getCaseDetails->drp4) && $getCaseDetails->drp4 == 'on') ? 1 : 0;

            /*Referred By*/
            $this->data['referredby'] = ($getCaseDetails->refby != '') ? explode(',', $getCaseDetails->refby) : '';
            $this->data['referredbyvalidate'] = (isset($getCaseDetails->chk5) && $getCaseDetails->chk5 == 'on') ? 1 : 0;
            if (isset($getCaseDetails->chk5) && $getCaseDetails->chk5 == 'on') {
                $this->data['referredbyvalidate'] = 1;
            } else {
                $this->data['referredbyvalidate'] = 0;
            }

            $this->data['referredbydrp'] = (isset($getCaseDetails->drp5) && $getCaseDetails->drp5 == 'on') ? 1 : 0;

            /*Association*/
            $this->data['association'] = ($getCaseDetails->assoc != '') ? explode(',', $getCaseDetails->assoc) : '';
            $this->data['associationvalidate'] = (isset($getCaseDetails->chk6) && $getCaseDetails->chk6 == 'on') ? 1 : 0;
            $this->data['associationdrp'] = (isset($getCaseDetails->drp6) && $getCaseDetails->drp6 == 'on') ? 1 : 0;
            /*Referred To*/
            $this->data['referredto'] = ($getCaseDetails->refto != '') ? explode(',', $getCaseDetails->refto) : '';
            $this->data['referredtovalidate'] = (isset($getCaseDetails->chk7) && $getCaseDetails->chk7 == 'on') ? 1 : 0;
            $this->data['referredtodrp'] = (isset($getCaseDetails->drp7) && $getCaseDetails->drp7 == 'on') ? 1 : 0;
            /*Our Ref Status*/
            $this->data['referalstatus'] = ($getCaseDetails->ors != '') ? explode(',', $getCaseDetails->ors) : '';
            $this->data['rstatusvalidate'] = (isset($getCaseDetails->chk8) && $getCaseDetails->chk8 == 'on') ? 1 : 0;
            $this->data['rstatusdrp'] = (isset($getCaseDetails->drp8) && $getCaseDetails->drp8 == 'on') ? 1 : 0;
            /*Mailinag list title 1 - 6 Start*/
            $this->data['mtitle1'] = ($getCaseDetails->t1 != '') ? $getCaseDetails->t1 : '';
            $this->data['mtitleval1'] = (isset($getCaseDetails->d1) && $getCaseDetails->d1 == 'on') ? 1 : 0;
            $this->data['mtitle2'] = ($getCaseDetails->t2 != '') ? $getCaseDetails->t2 : '';
            $this->data['mtitleval2'] = (isset($getCaseDetails->d2) && $getCaseDetails->d2 == 'on') ? 1 : 0;
            $this->data['mtitle3'] = ($getCaseDetails->t3 != '') ? $getCaseDetails->t3 : '';
            $this->data['mtitleval3'] = (isset($getCaseDetails->d3) && $getCaseDetails->d3 == 'on') ? 1 : 0;
            $this->data['mtitle4'] = ($getCaseDetails->t4 != '') ? $getCaseDetails->t4 : '';
            $this->data['mtitleval4'] = (isset($getCaseDetails->d4) && $getCaseDetails->d4 == 'on') ? 1 : 0;
            $this->data['mtitle5'] = ($getCaseDetails->t5 != '') ? $getCaseDetails->t5 : '';
            $this->data['mtitleval5'] = (isset($getCaseDetails->d5) && $getCaseDetails->d5 == 'on') ? 1 : 0;
            $this->data['mtitle6'] = ($getCaseDetails->t6 != '') ? $getCaseDetails->t6 : '';
            $this->data['mtitleval6'] = (isset($getCaseDetails->d6) && $getCaseDetails->d6 == 'on') ? 1 : 0;
           /*$cat_array = array();*/
            $message1 = '';
            $message2 = '';
            $message3 = '';
            $message4 = '';
            foreach ($event_activity as $k => $v) {
                if ($v->category == 1000002) {
                    $message1 = $v->event;
                }
                if ($v->category == 1000005) {
                    $message2 = $v->event;
                }
                if ($v->category == 1000006) {
                    $message3 = $v->event;
                }
                if ($v->category == 1000007) {
                    $message4 = $v->event;
                }
            }

           /*$this->data['category_array'] = $cat_array;*/
            $this->data['message1'] = $message1;
            $this->data['message2'] = $message2;
            $this->data['message3'] = $message3;
            $this->data['message4'] = $message4;

            if (isset($next_calendar_event->date) && $next_calendar_event->date != '') {
                $this->data['next_calendar_event'] = date('d M Y', strtotime($next_calendar_event->date));
                $this->data['next_calendar_event_tym'] = date('h:i A', strtotime($next_calendar_event->date));
            }
            $this->data['next_calendar_event_event'] = (isset($next_calendar_event->event) && $next_calendar_event->event != '') ? $next_calendar_event->event : '---';
            $this->data['next_calendar_event_venue'] = (isset($next_calendar_event->venue) && $next_calendar_event->venue != '') ? $next_calendar_event->venue : '---';
            $this->data['next_calendar_event_from'] = (isset($next_calendar_event->attyass) && $next_calendar_event->attyass != '') ? $next_calendar_event->attyass : '---';

            $this->data['case_cards'] = $case_details;

            if ($case_details != '') {
                foreach ($case_details as $data) {
                    if (!isset($users[strtolower($data->cardtype)])) {
                        $users[strtolower($data->cardtype)] = $data;
                    }
                }
            } else {
                $case_details = array();
                $defaultArray = $this->setBlankCase();
                $case_details = $this->case_model->getCaseDataByCaseId($case_id);
                $case_details[0] = (object) array_merge($defaultArray, $case_details[0]);
            }

            $this->data['display_details'] = $case_details[0];
            $main1 = json_decode($this->data['system_data'][0]->main1);
            $priparty = (explode(',', $main1->dpartytuse));
            $this->data['Venue'] = explode(',', $main1->venue);

            $calDef = json_decode($systemdata[0]->caldeflt);
            $calDef = json_decode($this->data['system_data'][0]->caldeflt);
            $calDefAtty = '';
            $calDefAttass = '';

            $calYrRng = $calDef->yeartud;
            if (isset($main1->uaaafcase) && $main1->uaaafcase = 'on') {
                if (!empty($case_details[0]->atty_resp)) {
                    $calDefAtty = $case_details[0]->atty_resp;
                }

                if (!empty($case_details[0]->atty_hand)) {
                    $calDefAttass = $case_details[0]->atty_hand;
                }
            } else {
                if (isset($calDef->udifa) && $calDef->udifa = 'on') {
                    $calDefAtty = $this->session->userdata('user_data')['initials'];
                }
                if (isset($calDef->udifaa) && $calDef->udifaa = 'on') {
                    $calDefAttass = $this->session->userdata('user_data')['initials'];
                }
            }
            $this->data['calYrRng'] = $calYrRng;
            $this->data['calDefAtty'] = $calDefAtty;
            $this->data['calDefAttass'] = $calDefAttass;
            $this->data['types'] = $this->common_functions->getAllRecord('card', 'DISTINCT(type)', 'type != ""', 'type asc');
            $this->data['languageList'] = $this->config->item('languageList');
            $this->data['saluatation'] = explode(',', $this->data['system_data'][0]->popsal);
            $this->data['suffix'] = explode(',', $this->data['system_data'][0]->rolosuffix);
            $this->data['title'] = explode(',', $this->data['system_data'][0]->poptitle);
            $this->data['category'] = explode(',', $this->data['system_data'][0]->popclient);
            $this->data['specialty'] = explode(',', $this->data['system_data'][0]->specialty);
            $this->data['sub_type'] = json_decode(sub_type, true);
            $this->data['side'] = explode(',', $main1->side);
            $this->data['JudgeName'] = (explode(',', $main1->judge));
            $this->data['Venue'] = explode(',', $main1->venue);
            $this->data['users'] = $users;
            $this->data['casetypedd'] = $this->data['system_data'][0]->casetypedd;
            $this->data['casestatdd'] = $this->data['system_data'][0]->casestatdd;
            $this->data['cdstaffdd'] = $this->data['system_data'][0]->cdstaffdd;

            $this->data['case_activity'] = $case_activity;
            $this->data['case_id'] = $case_id;
            $this->data['injury_details'] = $case_injury;
            $this->data['injury_cnt'] = count($case_injury);
            $case_intake = $this->common_model->getProspectId($case_id);
            /*echo '<pre>'; print_r($case_intake); exit;*/
            $this->data['Prospect_data'] = $case_intake;
            $this->data['birthdaynotification'] = '';
            if ($this->data['display_details']->birth_date) {
                $bdaynoti = json_decode($systemdata[0]->birthday, true);
                $this->data['birthdaynotification'] = $this->birthdateNotification($case_id, $this->data['display_details']->birth_date, $bdaynoti['before'], $bdaynoti['after']);
            }
            
            $case_details_atn = $this->case_model->getCaseFullDetails2($case_id);
            if ($case_details_atn != '') {
                foreach ($case_details_atn as $data) {
                    if (!isset($users[strtolower($data->cardtype)])) {
                        if ($data->cardcode != 853)
                            $users[strtolower($data->cardtype)] = $data;
                    }
                }
            } else {
                $case_details_atn = array();
                $defaultArray = $this->setBlankCase();
                $case_details_atn = $this->case_model->getCaseDataByCaseId($case_id);
                $case_details_atn[0] = (object) array_merge($defaultArray, $case_details[0]);
            }
            
            $metaArray['case_no'] = $case_id;
            $metaArray['display_details'] = $this->data['display_details'];
            /*echo '<pre>'; print_r($data); exit;*/
            $this->data['users'] = $users;
            $metaArray['users'] = $this->data['users'];
            $metaArray['injury_details'] = $case_injury;
            $metaArray['client_defendant'] = $this->data['client_defendant'];
            $metaArray['display_all_details'] = $case_details_atn;
            $metaArray['system_data'] = $systemdata;

            $case_information = $this->removeBlankArray($metaArray);

            $metadata = array('staff' => $this->data['loginUser'], 'data' => json_encode($case_information), 'caseno' => $case_id);

            $this->common_functions->insertMetadata($metadata);

            if ($this->input->post('caseId') != null) {
                echo json_encode($this->data);
                die();
            }
           /*if($case_id != 0 || $case_id != "") {*/
            $this->data['staff_list'] = $this->common_functions->get_staff_listing_Case();
           /*}
                   $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);*/
            $getCaseCategory = $this->common_model->getAllRecord('actdeflt', 'actname,category', '', 'category asc');
            $this->data['caseactcategory'] = $getCaseCategory;
            $this->load->view('common/header', $this->data);
            $this->load->view('cases/case_details_new');
            $this->load->view('common/common_modals');
            $this->load->view('common/footer');
        } else {
            $this->load->view('common/header', $this->data);
            $this->load->view('cases/case_not_found');
            $this->load->view('common/footer');
        }
    }


    public function removeBlankArray($array) {
        if (is_object($array)) {
            $array = (array) $array;
        }

        foreach ($array as $key => &$value) {
            if (empty($value)) {
                unset($array[$key]);
            } else {
                if (is_object($value)) {
                    $value = (array) $value;
                }

                if (is_array($value)) {
                    $value = $this->removeBlankArray($value);
                    if (empty($value)) {
                        unset($array[$key]);
                    }
                }
            }
        }

        return $array;
    }

    public function getCaseConfigAjax() {
        $filters = $this->searchCases();
        die(json_encode($filters));
    }

   /* public function getTaskDetails() {
        extract($_POST);
        $task_details = $this->case_model->tasksDetailsByDate($caseno, $this->data['loginUser']);
        die(json_encode($task_details));
    }*/

    public function getTodayTask() {
        extract($_POST);

        $caseId = $caseId;
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'Priority',
            1 => 'From',
            2 => 'Event',
            3 => 'Finish By',
            4 => 'Link',
        );
        if ($search['value'] != '') {
            $searchpriority = '';
            if ($search['value'] == 'h' || $search['value'] == 'hi' || $search['value'] == 'hig' || $search['value'] == 'high') {
                    $searchpriority = '1';
            }
            if ($search['value'] == 'l' || $search['value'] == 'lo' || $search['value'] == 'low') {
                    $searchpriority = '3';
            }
            if ($search['value'] == 'm' || $search['value'] == 'me' || $search['value'] == 'med' || $search['value'] == 'medi'|| $search['value'] == 'mediu' || $search['value'] == 'medium') {
                    $searchpriority = '2';
            }
           
            if($searchpriority == ''){
                $singleFilter = '(dateass LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR whofrom LIKE "%' . $search['value'] . '%" OR whoto LIKE "%' . $search['value'] . '%" OR datereq LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR typetask LIKE "%' . $search['value'] . '%" OR event LIKE "%' . $search['value'] . '%" OR phase LIKE "%' . $search['value']. '%" OR completed LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%")';
            }else{
                $singleFilter = '(Priority LIKE "%' . $searchpriority. '%")';
            }
        }

        $rec = $this->case_model->tasksDetailsToday($length, $start, $this->data['loginUser'], $singleFilter, $order);
        $totalData = $this->case_model->get_all_today_tasks_count($this->data['loginUser'], $singleFilter);
        $colorCode = json_decode(color_codes, true);

        $finalData = array();
        if (!empty($rec)) {
            foreach ($rec as $data) :
                $link = "";
                $color = "";
                $textColor = "";
                $backgroundColor = "";
                $textColor = ($data->color != 0) ? $colorCode[$data->color][1] : '';
                $backgroundColor = ($data->color != 0) ? $colorCode[$data->color][2] : '';

                if ($data->caseno == "0" || $data->caseno == "") {
                    $link = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onclick="editTask(' . $data->mainkey . ')" style="margin-bottom:5px !important;"><i class="fa fa-edit"></i></button>' .
                            '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onclick="deleteTask(' . $data->mainkey . ')" style="margin-bottom:5px !important;margin-left: 5px;"><i class="fa fa-trash"></i></button>' .
                            '<br><button class="btn btn-info btn-circle-action btn-circle task-completed-nocase" title="Complete" type="button" id="' . $data->mainkey . '" data-taskid="' . $data->mainkey . '" data-event="' . $data->event . '" data-caseno="' . $data->caseno . '"><i class="fa fa-check"></i></button>';
                } else {
                    $link = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onclick="editTask(' . $data->mainkey . ')" style="margin-bottom:5px !important;"><i class="fa fa-edit"></i></button>' .
                            '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onclick="deleteTask(' . $data->mainkey . ')" style="margin-bottom:5px !important;margin-left: 5px;"><i class="fa fa-trash"></i></button>' .
                            '<br><button class="btn btn-info btn-circle-action btn-circle task-completed" title="Complete" type="button" id="' . $data->mainkey . '" data-taskid="' . $data->mainkey . '" data-event="' . $data->event . '" data-caseno="' . $data->caseno . '"><i class="fa fa-check"></i></button>' .
                            '<a style="margin-left:5px;" class="btn btn-info btn-circle-action btn-circle" title="Pull Case" target="_blank" href="' . base_url('/cases/case_details/') . '/' . $data->caseno . '"><i class="fa fa-folder-open"></i></a>';
                }

                $eventlength = strlen($data->event);
                if ($eventlength > 50) {
                    $eventdetail = substr($data->event, 0, 50) . '...';
                } else {
                    $eventdetail = $data->event;
                }
                $finishbydate = explode(" ", $data->datereq);
			
				$priority = '';
				if ($data->priority == '1') {
					$priority = 'High';
				}
				if ($data->priority == '3') {
					$priority = 'Low';
				}
				if ($data->priority == '2') {
					$priority = 'Medium';
				}

                if($data->datereq == "0000-00-00 00:00:00") {
                    $finish_by_date = "";
                } else {
                    $finish_by_date = date('m/d/Y', strtotime($data->datereq));
                }
				
                $finalData[] = array(
                    $priority,
                    $data->whofrom,
                    $eventdetail,
                    $finish_by_date, //$finishbydate[0],
                    $link,
                    $backgroundColor . "-" . $textColor,
                );
            endforeach;
        }

        $listingData = array(
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $finalData,
        );

        echo json_encode($listingData);
    }



    public function getcaseTask() {
        extract($_POST);
		
        $caseno = $_POST['caseno'];
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'Tasks Date',
            1 => 'From',
            2 => 'Event',
            3 => 'Link',
        );
        if ($search['value'] != '') {
            $singleFilter = '(dateass LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR whofrom LIKE "%' . $search['value'] . '%" OR whoto LIKE "%' . $search['value'] . '%" OR datereq LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%" OR typetask LIKE "%' . $search['value'] . '%" OR event LIKE "%' . $search['value'] . '%" OR phase LIKE "%' . $search['value'] . '%" OR completed LIKE "%' . date('Y-m-d', strtotime($search['value'])) . '%")';
        }

       // $rec = $this->case_model->tasksDetailsToday($length, $start, $this->data['loginUser'], $singleFilter, $order);
		$rec = $this->case_model->tasksDetailsByDate($length, $start, $this->data['loginUser'], $singleFilter, $order, $caseno);
        $totalData = $this->case_model->get_all_case_tasks_count($this->data['loginUser'],$caseno, $singleFilter);
        $colorCode = json_decode(color_codes, true);
	
        $finalData = array();
        if (!empty($rec)) {
            foreach ($rec as $data) :
                $link = "";
                $color = "";
                $textColor = "";
                $backgroundColor = "";
                $textColor = ($data->color != 0) ? $colorCode[$data->color][1] : '';
                $backgroundColor = ($data->color != 0) ? $colorCode[$data->color][2] : '';

                    $link = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onclick="editTask(' . $data->mainkey . ')" style="margin-bottom:5px !important;"><i class="fa fa-edit"></i></button>' .
                            '<button class="btn btn-danger btn-circle-action btn-circle deletetask" title="Delete" type="button" data-taskId="'.$data->mainkey.'" style="margin-bottom:5px !important;margin-left: 5px;"><i class="fa fa-trash"></i></button>' .
                            '<a class="btn btn-info btn-circle-action btn-circle task-completed" title="Complete" type="button" id="'.$data->mainkey.'" data-taskId="'.$data->mainkey.'" data-event="'.htmlspecialchars($data->event, ENT_QUOTES, 'UTF-8').'" data-caseno="'.$caseno.'" style="margin-bottom:5px !important;margin-left: 5px;"><i class="fa fa-check"></i></a>';
            
                $eventlength = strlen($data->event);
                if ($eventlength > 50) {
                    $eventdetail = substr($data->event, 0, 50) . '<a data-toggle="tooltip" data-placement="top" title="' . $data->event . '">...</a>';
                } else {
                    $eventdetail = $data->event;
                }
                $finishbydate = explode(" ", $data->datereq);
			
				
                $finalData[] = array(
                    date('m/d/Y', strtotime($data->datereq)),
                    $data->whofrom,
                    $eventdetail,
                    $link,
                    $backgroundColor . "-" . $textColor,
                );
            endforeach;
        }

        $listingData = array(
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $finalData,
        );

        echo json_encode($listingData);
    }



    public function getAjaxCaseDetail() {
        $casedetail = $this->common_functions->getCaseSpecificDetails($_POST['caseno']);
        $eventDetais = $this->case_model->getEventActivity($_POST['caseno']);
        $category = json_decode(CATEGORY_ID, true);
//        $casedetail->category_events = ;
        if (count($casedetail) > 0) {
            foreach ($eventDetais as $k => $v) {
//                $casedetail[0] = new stdClass();
                if ($v->event != '') {
                    $casedetail[0]->category_events[$category[$v->category]] = $v->event;
                }
            }
        }

        die(json_encode($casedetail));
    }

    public function getAjaxCardDetails() {
        $card_details = $this->case_model->getCardDetails($this->input->post('cardcode'), $this->input->post('caseno'));
        if (count($card_details)) {
            echo json_encode($card_details[0]);
        } else {
            echo json_encode(false);
        }
    }

    public function getPartyAjaxData() {
        extract($_POST);
        $result = $this->case_model->getCaseFullDetails($caseno);
        echo json_encode($result);
    }

    public function addEditActivity() {
        //echo "<pre>";print_r($_POST);print_r($_FILES);exit;
        //echo "here".$this->uri->segment(2);exit;
        extract($_REQUEST);

        $casedetail = $this->case_model->addEditActivity($_REQUEST, $_FILES);
        if ($_REQUEST['activity_no'] != "") {
            $actno = $_REQUEST['activity_no'];
        } else {
            $actno = $casedetail;
        }

//            $dir = base_url().'assets/clients/'. $_REQUEST['caseActNo'];
        $dir = ASSPATH . 'clients/' . $_REQUEST['caseActNo'];
        //print_R($dir); print_r($_FILES);exit;
        $filteredArray = array();
        foreach (glob($dir . "/F" . $actno . "*") as $filename) {
            $filteredArray[] = $filename;
        }
        $f_name = '';
        //print_r($filename);exit;
        if (!empty($filteredArray)) {
            $file = count($filteredArray);
            if ($file == 1) {
                $f_name = explode('/', $filteredArray[0]);
                $f_name = $f_name[7];
            }
        } else {
            $file = 0;
        }

        if ($casedetail == "updated" || $casedetail == "edited") {
            $result = array('status' => 2, "actno" => $_REQUEST['activity_no'], "fileExist" => $file, "actData" => $_REQUEST, 'file_name' => $f_name);
            echo json_encode($result);
        } else if ($casedetail) {
            $result = array('status' => 1, "actno" => $casedetail, "fileExist" => $file, "actData" => $_REQUEST, 'file_name' => $f_name);
            echo json_encode($result);
        } else {
            $result = array('status' => 0, "actno" => $_REQUEST['activity_no'], "fileExist" => $file, "actData" => $_REQUEST, 'file_name' => $f_name);
            echo json_encode($result);
        }
        exit;
        // echo ' hello '.base_url().'cases/case_details/'. $_REQUEST['caseActNo']; exit;
        //view_all_attach(base_url() . 'cases/case_details/' . $_REQUEST['caseActNo']);
    }

    public function getEditCalendar() {
        extract($_REQUEST);
        //$eventno;

        $res = $this->case_model->getCalendarDataByEvent($eventno);
        $jsoncolor_codes = json_decode(color_codes, true);
        if ($res) {

            $style = isset($jsoncolor_codes[$res->color]) ? "background-color:{$jsoncolor_codes[$res->color][2]};color: {$jsoncolor_codes[$res->color][1]}" : "background-color:#FFFFFF;color: #000000";

            $html = "<tr class='gradeX' id='calEvent_$res->eventno' style='$style'>";
            $html .= "<td>" . date('d/m/Y H:i', strtotime($res->date)) . "</td>";
            $html .= "<td>" . $res->attyh . "</td>";
            $html .= "<td>" . $res->attyass . "</td>";
            $html .= "<td>" . $res->first . ' ' . $res->last . ' vs ' . $res->defendant . "</td>";
            $html .= "<td>" . $res->event . "</td>";
            $html .= "<td>" . $res->venue . "</td>";
            $html .= "<td>" . $res->judge . "</td>";
            $html .= "<td>" . $res->notes . "</td>";
            $html .= "<td>";
            $html .= "<a class='btn btn-xs btn-info mar-right4 event_edit' data-eventno=" . $eventno . " href = 'javascript:;' title='Edit'><i class = 'icon fa fa-edit'></i> Edit</a>";
            $html .= "<a class='btn btn-xs btn-danger event_delete' data-eventno=" . $eventno . " href = 'javascript:;' title='Delete'><i class = 'icon fa fa-times'></i> Delete</a>";
            $html .= "</td>";
            $html .= "</tr>";
            echo $html;
        } else {
            echo "fail";
        }
    }

    public function setBillingDefaults() {
        //echo "<pre>";print_r($this->input->post());//exit;
        $defaults = $this->case_model->setBillingDefaults($this->input->post());
        if ($defaults) {
            echo json_encode($this->input->post());
        } else {
            echo 0;
        }
    }

    public function getAjaxActivityDetails() {
        $activity_details = $this->case_model->getActivityDetails($this->input->post('actno'), $this->input->post('caseno'));
        if (count($activity_details)) {
            //var/www/html/lawware/
            $directory = 'clients/' . $activity_details[0]->caseno . '/F' . $activity_details[0]->actno . "*";
            $files = glob($directory);

            //print_r($files);exit;
            if (!empty($files)) {
                $activity_details[0]->document = $files; //$files[0]
            } else {
                $activity_details[0]->document = '';
            }

            echo json_encode($activity_details[0]);
        } else {
            echo json_encode(false);
        }
    }

    public function viewAttechment() {
        $caseNo = $this->input->post('caseno');
        $actNo = $this->input->post('actno');

        $folder = 500 * (ceil($caseNo / 500));
        if ($this->input->post('filename') != 'one') {
            $files = glob(DOCUMENTPATH . 'clients/' . $caseNo . "/" . $this->input->post('filename'));
        } else {
            $files = glob(DOCUMENTPATH . 'clients/' . $caseNo . "\\f" . $actNo . "*");
        }
        if (!empty($files)) {
            exec('start "" /max ' . $files[0]);
        } else {
            echo "false";
        }
    }

    public function deleteActivity() {
        extract($_POST);

        $cond = array('actno' => $actno, 'caseno' => $caseno);
        $res = $this->common_functions->deleteRec('caseact', $cond);

        echo $res;
    }

    public function getBillingDefault() {
        $case_billing_default = $this->case_model->getBillingDefault($this->input->post('caseno'));
        //print_r($case_billing_default);exit;
        if (count($case_billing_default)) {
            echo json_encode($case_billing_default[0]);
        } else {
            echo json_encode(false);
        }
    }

    public function dropzone() {
        $this->load->view('common/header');
        $this->load->view('cases/dropzone');
        $this->load->view('common/footer');
    }

    public function addAttchment() {
        $directory = 'clients/10/f99' . ".*";
        $files = glob($directory);
    }

    public function viewAllAttechment() {

        $caseNo = $this->input->post('caseno');
        $actNo = $this->input->post('actno');
        //$oledoc = $this->input->post('oledoc');
        $activity_details = $this->case_model->getActivityDetails($this->input->post('actno'), $this->input->post('caseno'));
        $folder = 500 * (ceil($caseNo / 500));
        if(!empty($activity_details[0]->ole3) && $activity_details[0]->ole3 != '')
            $Olefiles = glob(DOCUMENTPATH . 'clients/' . $caseNo . "/" . $activity_details[0]->ole3);
        else
            $Olefiles = '';

        // $files = glob(DOCUMENTPATH . 'clients/' . $caseNo . '/F' . $this->input->post('actno') . '.*');
        $Olelink = base_url() . 'assets/clients/' . $caseNo . '/';
        $olefilelink = "";
		//echo '<pre>'; print_r($Olefiles); exit;
        if (!empty($Olefiles)) {
            foreach ($Olefiles as $olefile):
                $OlefileArr = explode("/", $olefile); //echo '<pre>'; print_r($olefile);
               /* if ($OlefileArr[7] != "") {
                    $olefilelink = '<a  download=' . $olefile . ' class="btn btn-md btn-primary marginClass view_attach" data-filename=' . $OlefileArr[7] . 'data-actid=' . $actNo . ' href=' . $Olelink . $OlefileArr[7] . '><i class = "icon fa fa-file-text-o"></i> View ' . $OlefileArr[7] . ' </a>';
                } else {
                    $olefilelink = "";
                }*/
                $fileArr = explode("/", $OlefileArr[7]);// print_r($fileArr);
                $ext = explode(".",$fileArr[0]);
                $olefilelink .= '<div class="btn-group" style="margin-left: 5px;"> <span class="btn btn-success btn-sm">'.$OlefileArr[7].'</span>';
                if($ext[1] == 'docx'){
                if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                    $olefilelink .= '<a class="btn btn-outline btn-success btn-sm case_video" data-actid="' . $actNo . '" target="_blank" href = "' . base_url() . 'document/doceditor.php?fileID='.$OlefileArr[7].'&user='.$this->session->userdata('user_data')['initials'].'&filePath='.$caseNo.'/'.$OlefileArr[7].'" title="View"><i class = "icon fa fa-eye"></i></a>';}
                else{
                    $olefilelink .= '<a class="btn btn-outline btn-success btn-sm case_video" data-actid="' . $actNo . '" href = "' . base_url() . 'assets/clients/' . $caseNo . '/' . $OlefileArr[7] . '" title="View"><i class = "icon fa fa-eye"></i></a>';
                }
                }
                 $olefilelink .= '<a  download=' . $OlefileArr[7] . ' class="btn btn-outline btn-success btn-sm case_video" data-filename=' . $fileArr[7] . 'data-actid=' . $actNo . ' href=' . $Olelink . $OlefileArr[7] . '><i class="fa fa-download " ></i></a></div>';
            endforeach;
        }
        
        $origfiles = glob(DOCUMENTPATH . 'clients/' . $caseNo . "/F" . $actNo . ".*");
        $multipleFiles = glob(DOCUMENTPATH . 'clients/' . $caseNo . "/F" . $actNo .'_*');
        $link = base_url() . 'assets/clients/' . $caseNo . '/';

        $files = array_merge($origfiles, $multipleFiles);

        $table = '<div class="form-group">';

        foreach ($files as $file) {
            //<span><a href='$file'>View File</a></span>
            $fileArr = explode("/", $file);// print_r($fileArr);
            $ext = explode(".",$fileArr[7]);
            $table .= '<div class="btn-group" style="margin-left: 5px;"> <span class="btn btn-success btn-sm">'.$fileArr[7].'</span>';
            if($ext[1] == 'docx'){
                if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                    $table .= '<a class="btn btn-outline btn-success btn-sm case_video" data-actid="' . $actNo . '" target="_blank" href = "' . base_url() . 'document/doceditor.php?fileID='.$fileArr[7].'&user='.$this->session->userdata('user_data')['initials'].'&filePath='.$caseNo.'/'.$fileArr[7].'" title="View"><i class = "icon fa fa-eye"></i></a>';}
                else{
                    $table .= '<a class="btn btn-outline btn-success btn-sm case_video" data-actid="' . $actNo . '" href = "' . base_url() . 'assets/clients/' . $caseNo . '/' . $fileArr[7] . '" title="Download"><i class = "icon fa fa-download"></i></a><a class="btn btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $actNo . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $caseNo . '/' . $fileArr[7] . '" title="View"><i class = "icon fa fa-eye"></i></a>';
                }
            //$table .= '<a  download=' . $file . ' class="btn btn-outline btn-success btn-sm case_video" data-filename=' . $fileArr[7] . 'data-actid=' . $actNo . ' href=' . $link . $fileArr[7] . '><i class="fa fa-eye " ></i> </a>';
            }
            $table .= '<a  download=' . $fileArr[7] . ' class="btn btn-outline btn-success btn-sm case_video" data-filename=' . $fileArr[7] . 'data-actid=' . $actNo . ' href=' . $link . $fileArr[7] . '><i class="fa fa-download " ></i></a></div>'; 
            
            
        }
        $table .= $olefilelink;
        $table .= "</div>";
        echo $table;
    }

    public function deleteActivityAttachment() {
        $file = $this->input->post('id');
        if (!unlink($file)) {
            echo ("Error deleting $file");
        } else {
            echo ("Deleted $file");
        }
    }

    public function getAjaxinjuryDetails() {
        extract($_POST);

        $con = array('caseno' => $caseno, 'injury_id' => $injury_id);

        $rec = $this->case_model->getInjuryData($con);

        $bodyParts = json_decode(bodyParts);
        //echo bodyParts; ;

        if (isset($rec->injury_id)) {

            $ajstname = '';
            $d1name = '';
            $i2name = '';
            $pob = '';
            /*foreach ($bodyParts as $key => $value) {
                if (!empty($rec->pob1) && $key == $rec->pob1) {
                    $pob .= $key . ' ' . $value . ",";
                }

                if (!empty($rec->pob2) && $key == $rec->pob2) {
                    $pob .= $key . ' ' . $value . ",";
                }

                if (!empty($rec->pob3) && $key == $rec->pob3) {
                    $pob .= $key . ' ' . $value . ",";
                }

                if (!empty($rec->pob4) && $key == $rec->pob4) {
                    $pob .= $key . ' ' . $value . ",";
                }

                if (!empty($rec->pob5) && $key == $rec->pob5) {
                    $pob .= $key . ' ' . $value . ",";
                }
            }*/
            if (!empty($rec->adj1e)) {
                $pob = $rec->adj1e ;
            }

//            if (!empty($pob)) {
//                $pob = substr($pob, 0, -1);
//            }
            $rec->adj1e = $pob;
            if ($rec->i_adjsal != '') {
                $ajstname .= $rec->i_adjsal . " ";
            }
            if ($rec->i_adjfst != '') {
                $ajstname .= $rec->i_adjfst . " ";
            }
            if ($rec->i_adjuster != '') {
                $ajstname .= $rec->i_adjuster . " ";
            }

            if ($rec->d1_first != '') {
                $d1name .= $rec->d1_first . " ";
            }
            if ($rec->d1_last != '') {
                $d1name .= $rec->d1_last . " ";
            }

            if ($rec->i2_adjsal != '') {
                $i2name .= $rec->i2_adjsal . " ";
            }
            if ($rec->i2_adjfst != '') {
                $i2name .= $rec->i2_adjfst . " ";
            }
            if ($rec->i2_adjuste != '') {
                $i2name .= $rec->i2_adjuste . " ";
            }
            if ($rec->dor_date != '' && $rec->dor_date != '1899-12-30 00:00:00' && $rec->dor_date != '0000-00-00 00:00:00') {
                $rec->dor_date = date('m/d/Y', strtotime($rec->dor_date));
            } else {
                $rec->dor_date = '';
            }
            /* if ($rec->adj1a != '' && $rec->adj1a != '1899-12-30 00:00:00' && $rec->adj1a != '0000-00-00 00:00:00') {
              $rec->adj1a = date('m/d/Y', strtotime($rec->adj1a));
              } else {
              $rec->adj1a = '';
              } */
            if ($rec->doi != '' && $rec->doi != '1899-12-30 00:00:00' && $rec->doi != '0000-00-00 00:00:00') {
                $rec->doi = date('m/d/Y', strtotime($rec->doi));
            } else {
                $rec->doi = '';
            }
            if ($rec->doi2 != '' && $rec->doi2 != '1899-12-30 00:00:00' && $rec->doi2 != '0000-00-00 00:00:00') {
                $rec->doi2 = date('m/d/Y', strtotime($rec->doi2));
            } else {
                $rec->doi2 = '';
            }
            //if($rec->adj1c != '') { $rec->adj1c = date('m/d/Y',strtotime($rec->adj1c)); }
            if ($rec->adj5d != '' && $rec->adj5d != '1899-12-30 00:00:00' && $rec->adj5d != '0000-00-00 00:00:00') {
                $rec->adj5d = date('m/d/Y', strtotime($rec->adj5d));
            } else {
                $rec->adj5d = '';
            }
            if ($rec->adj7b != '' && $rec->adj7b != '1899-12-30 00:00:00' && $rec->adj7b != '0000-00-00 00:00:00') {
                $rec->adj7b = date('m/d/Y', strtotime($rec->adj7b));
            } else {
                $rec->adj7b = '';
            }
            if ($rec->adj10a != '' && $rec->adj10a != '1899-12-30 00:00:00' && $rec->adj10a != '0000-00-00 00:00:00') {
                $rec->adj10a = date('m/d/Y', strtotime($rec->adj10a));
            } else {
                $rec->adj10a = '';
            }
            if ($rec->s_w != '' && $rec->s_w != '1899-12-30 00:00:00' && $rec->s_w != '0000-00-00 00:00:00') {
                $rec->s_w = date('m/d/Y', strtotime($rec->s_w));
            } else {
                $rec->s_w = '';
            }
            if ($rec->other_sol != '' && $rec->other_sol != '1899-12-30 00:00:00' && $rec->other_sol != '0000-00-00 00:00:00') {
                $rec->other_sol = date('m/d/Y', strtotime($rec->other_sol));
            } else {
                $rec->other_sol = '';
            }
            if ($rec->follow_up != '' && $rec->follow_up != '1899-12-30 00:00:00' && $rec->follow_up != '0000-00-00 00:00:00') {
                $rec->follow_up = date('m/d/Y', strtotime($rec->follow_up));
            } else {
                $rec->follow_up = '';
            }
            if ($rec->liab != '' && $rec->liab != '1899-12-30 00:00:00' && $rec->liab != '0000-00-00 00:00:00') {
                $rec->liab = date('m/d/Y', strtotime($rec->liab));
            } else {
                $rec->liab = '';
            }

            if (!empty($rec->aoe_coe_status)) {
                $rec->aoe_coe_status = $rec->aoe_coe_status;
            } else {
                $rec->aoe_coe_status = '';
            }

            $rec->adjustername = $ajstname;
            $rec->d1name = $d1name;
            $rec->i2name = $i2name;

            echo json_encode($rec);
        } else {
            echo false;
        }
    }

    public function getAjaxinjuryDetailsEdit() {
        extract($_POST);

        $con = array('caseno' => $caseno, 'injury_id' => $injury_id);

        $rec = $this->case_model->getInjuryData($con);

        $bodyParts = json_decode(bodyParts);
        //echo bodyParts; ;

        if (isset($rec->injury_id)) {

            $ajstname = '';
            $d1name = '';
            $i2name = '';
            $pob = '';
            foreach ($bodyParts as $key => $value) {
                if (!empty($rec->pob1) && $key == $rec->pob1) {
                    $pob .= $value . ",";
                }

                if (!empty($rec->pob2) && $key == $rec->pob2) {
                    $pob .= $value . ",";
                }

                if (!empty($rec->pob3) && $key == $rec->pob3) {
                    $pob .= $value . ",";
                }

                if (!empty($rec->pob4) && $key == $rec->pob4) {
                    $pob .= $value . ",";
                }

                if (!empty($rec->pob5) && $key == $rec->pob5) {
                    $pob .= $value . ",";
                }
            }
            if (!empty($rec->adj1e)) {
                $adj1e = $rec->adj1e;
            }

            if (!empty($pob)) {
                $pob = substr($pob, 0, -1);
            }
            $rec->adj1e = $adj1e;
            if ($rec->i_adjsal != '') {
                $ajstname .= $rec->i_adjsal . " ";
            }
            if ($rec->i_adjfst != '') {
                $ajstname .= $rec->i_adjfst . " ";
            }
            if ($rec->i_adjuster != '') {
                $ajstname .= $rec->i_adjuster . " ";
            }

            if ($rec->d1_first != '') {
                $d1name .= $rec->d1_first . " ";
            }
            if ($rec->d1_last != '') {
                $d1name .= $rec->d1_last . " ";
            }

            if ($rec->i2_adjsal != '') {
                $i2name .= $rec->i2_adjsal . " ";
            }
            if ($rec->i2_adjfst != '') {
                $i2name .= $rec->i2_adjfst . " ";
            }
            if ($rec->i2_adjuste != '') {
                $i2name .= $rec->i2_adjuste . " ";
            }
            if ($rec->dor_date != '' && $rec->dor_date != '1899-12-30 00:00:00' && $rec->dor_date != '0000-00-00 00:00:00') {
                $rec->dor_date = date('m/d/Y', strtotime($rec->dor_date));
            } else {
                $rec->dor_date = '';
            }
            /* if ($rec->adj1a != '' && $rec->adj1a != '1899-12-30 00:00:00' && $rec->adj1a != '0000-00-00 00:00:00') {
              $rec->adj1a = date('m/d/Y', strtotime($rec->adj1a));
              } else {
              $rec->adj1a = '';
              } */
            if ($rec->doi != '' && $rec->doi != '1899-12-30 00:00:00' && $rec->doi != '0000-00-00 00:00:00') {
                $rec->doi = date('m/d/Y', strtotime($rec->doi));
            } else {
                $rec->doi = '';
            }
            if ($rec->doi2 != '' && $rec->doi2 != '1899-12-30 00:00:00' && $rec->doi2 != '0000-00-00 00:00:00') {
                $rec->doi2 = date('m/d/Y', strtotime($rec->doi2));
            } else {
                $rec->doi2 = '';
            }
            //if($rec->adj1c != '') { $rec->adj1c = date('m/d/Y',strtotime($rec->adj1c)); }
            if ($rec->adj5d != '' && $rec->adj5d != '1899-12-30 00:00:00' && $rec->adj5d != '0000-00-00 00:00:00') {
                $rec->adj5d = date('m/d/Y', strtotime($rec->adj5d));
            } else {
                $rec->adj5d = '';
            }
            if ($rec->adj7b != '' && $rec->adj7b != '1899-12-30 00:00:00' && $rec->adj7b != '0000-00-00 00:00:00') {
                $rec->adj7b = date('m/d/Y', strtotime($rec->adj7b));
            } else {
                $rec->adj7b = '';
            }
            if ($rec->adj10a != '' && $rec->adj10a != '1899-12-30 00:00:00' && $rec->adj10a != '0000-00-00 00:00:00') {
                $rec->adj10a = date('m/d/Y', strtotime($rec->adj10a));
            } else {
                $rec->adj10a = '';
            }
            if ($rec->s_w != '' && $rec->s_w != '1899-12-30 00:00:00' && $rec->s_w != '0000-00-00 00:00:00') {
                $rec->s_w = date('m/d/Y', strtotime($rec->s_w));
            } else {
                $rec->s_w = '';
            }
            if ($rec->other_sol != '' && $rec->other_sol != '1899-12-30 00:00:00' && $rec->other_sol != '0000-00-00 00:00:00') {
                $rec->other_sol = date('m/d/Y', strtotime($rec->other_sol));
            } else {
                $rec->other_sol = '';
            }
            if ($rec->follow_up != '' && $rec->follow_up != '1899-12-30 00:00:00' && $rec->follow_up != '0000-00-00 00:00:00') {
                $rec->follow_up = date('m/d/Y', strtotime($rec->follow_up));
            } else {
                $rec->follow_up = '';
            }
            if ($rec->liab != '' && $rec->liab != '1899-12-30 00:00:00' && $rec->liab != '0000-00-00 00:00:00') {
                $rec->liab = date('m/d/Y', strtotime($rec->liab));
            } else {
                $rec->liab = '';
            }

            if (!empty($rec->aoe_coe_status)) {
                $rec->aoe_coe_status = $rec->aoe_coe_status;
            } else {
                $rec->aoe_coe_status = '';
            }

            $rec->adjustername = $ajstname;
            $rec->d1name = $d1name;
            $rec->i2name = $i2name;

            echo json_encode($rec);
        } else {
            echo false;
        }
    }

    public function searchForRolodex() {
        //print_r($_POST);
        extract($_POST);
        $where = '';
        $orderby = '';
        $searchResult = array();

        $serch_name = explode("-", "$rolodex_search_parties");

        if ($rolodex_search_parties == 'rolodex-firm') {
            $where = 'c2.firm like "' . $rolodex_search_text . '%" OR c.first like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-first-last") {
            if (strpos($rolodex_search_text, ',') !== false) {
                $splitedVal = explode(",", $rolodex_search_text);
                $where = 'c.last  like "' . trim($splitedVal[0]) . '%" AND c.first like "' . trim($splitedVal[1]) . '%"';
            } else {
                $where = 'c.first  like "' . $rolodex_search_text . '%" OR c.last like "' . $rolodex_search_text . '%"';
            }
            $orderby = 'c.first asc , c.last asc';
        } else if ($rolodex_search_parties == "rolodex-company") {
            $where = 'c2.firm  like "' . $rolodex_search_text . '%"';
            $orderby = 'c2.firm asc,c.first asc , c.last asc';
        } else if ($rolodex_search_parties == "rolodex-cardno") {
            if (substr($rolodex_search_text, 0, 1) === '<') {
                $str = ltrim($rolodex_search_text, '<');
                $where = 'c.cardcode = "' . $str . '"';
            } else {
                $where = 'c.cardcode  like "' . $rolodex_search_text . '%"';
            }
        } else if ($rolodex_search_parties == "rolodex-social_sec") {
            $where = 'c.social_sec  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-email") {
            $where = 'c.email  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-licenseno") {
            $where = 'c.licenseno  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-type") {
            $where = 'c.type  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-first") {
            $where = 'c.first  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-details") {
            $where = '';
        } else if ($rolodex_search_parties == "rolodex-relational") {
            $where = '';
        } else if ($rolodex_search_parties == "rolodex-query") {
            $where = '';
        } else {
            $where = 'c.' . $serch_name[1] . '  like "' . $rolodex_search_text . '%"';
        }

        if ($search['value'] != '') {
            $singleFilter = '(c2.firm LIKE "%' .$search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.phone1 LIKE "%' . $search['value'] . '%" OR c2.address1 LIKE "%' . $search['value'] .'%" OR c.first LIKE "%' . $search['value'] . '%" OR c.last LIKE "%' . $search['value'] . '%" OR c2.address2 LIKE "%' . $search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.state LIKE "%' . $search['value'] . '%" OR c2.zip LIKE "%' . $search['value']. '%")';
        }

        $result = $this->case_model->getRolodexSearchParties($where, $start, $length, $order , $singleFilter);
        $count = $this->case_model->getRolodexSearchPartiesCount($where,$singleFilter);
        //echo $this->db->last_query(); exit;
        //print_r($result);

        $finalData = array();
        foreach ($result as $data) {
            $Name = '';
            if (isset($data->first) && !empty($data->first)) {
                $Name = $data->first . " , " . $data->last;
            } else {
                $Name = $data->last;
            }
            $tmpArr = array(
                "cardcode" => $data->cardcode,
                "name" => $Name,
                "type" => $data->type,
                "firm" => $data->firm,
                "phone" => $data->phone1,
                "address" => $data->address1,
                "address2" => $data->address2,
                "city" => $data->city,
                "state" => $data->state,
                "zip" => $data->zip,
            );
            array_push($finalData, $tmpArr);
        }

        echo json_encode($finalData);
    }

    public function searchForRolodexByfirm() {

        extract($_POST);
        $where = '';
        $orderby = '';
        $searchResult = array();

        $serch_name = explode("-", "$rolodex_search_parties");
        if ($search['value'] != '') {
            $singleFilter = '(c2.firm LIKE "%' .$search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.phone1 LIKE "%' . $search['value'] . '%" OR c2.address1 LIKE "%' . $search['value'] .'%" OR c.first LIKE "%' . $search['value'] . '%" OR c.first LIKE "%' . $search['value'] . '%" OR c2.address2 LIKE "%' . $search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.state LIKE "%' . $search['value'] . '%" OR c2.zip LIKE "%' . $search['value']. '%")';
        }

        if ($rolodex_search_parties == 'rolodex-firm') {
            $where = 'c2.firm like "' . $rolodex_search_text . '%" OR c.first like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-first-last") {
            if (strpos($rolodex_search_text, ',') !== false) {
                $splitedVal = explode(",", $rolodex_search_text);
                $where = 'c.last  like "' . trim($splitedVal[0]) . '%" AND c.first like "' . trim($splitedVal[1]) . '%"';
            } else {
                $where = 'c.first  like "' . $rolodex_search_text . '%" OR c.last like "' . $rolodex_search_text . '%"';
            }
            $orderby = 'c.first asc , c.last asc';
        } else if ($rolodex_search_parties == "rolodex-company") {
            $where = 'c2.firm  like "' . $rolodex_search_text . '%"';
            $orderby = 'c2.firm asc,c.first asc , c.last asc';
        } else if ($rolodex_search_parties == "rolodex-cardno") {
            if (substr($rolodex_search_text, 0, 1) === '<') {
                $str = ltrim($rolodex_search_text, '<');
                $where = 'c.cardcode = "' . $str . '"';
            } else {
                $where = 'c.cardcode  like "' . $rolodex_search_text . '%"';
            }
        } else if ($rolodex_search_parties == "rolodex-social_sec") {
            $where = 'c.social_sec  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-email") {
            $where = 'c.email  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-licenseno") {
            $where = 'c.licenseno  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-type") {
            $where = 'c.type  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-first") {
            $where = 'c.first  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-details") {
            $where = '';
        } else if ($rolodex_search_parties == "rolodex-relational") {
            $where = '';
        } else if ($rolodex_search_parties == "rolodex-query") {
            $where = '';
        } else {
            $where = 'c.' . $serch_name[1] . '  like "' . $rolodex_search_text . '%"';
        }
        
        if ($search['value'] != '') {
            $singleFilter = '(c2.firm LIKE "%' .$search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.phone1 LIKE "%' . $search['value'] . '%" OR c2.address1 LIKE "%' . $search['value'] .'%" OR c.first LIKE "%' . $search['value'] . '%" OR c.last LIKE "%' . $search['value'] . '%" OR c2.address2 LIKE "%' . $search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.state LIKE "%' . $search['value'] . '%" OR c2.zip LIKE "%' . $search['value']. '%")';
        }

        $result = $this->case_model->getRolodexSearchParties($where, $start, $length, $order , $singleFilter);
        $count = $this->case_model->getRolodexSearchPartiesCount($where,$singleFilter);
        //print_r($result);

        $finalData = array();
        if (!empty($result)) {
            foreach ($result as $data) :
                $Name = '';
                if (isset($data->first) && !empty($data->first)) {
                    $Name = $data->first . " , " . $data->last;
                } else {
                    $Name = $data->last;
                }
                $finalData[] = array(
                    $data->firm,
                    $data->city,
                    $data->address1,
                    $data->address2,
                    $data->phone1,
                    $Name,
                    $data->state,
                    $data->zip,
                    $data->cardcode,
                    $data->type,
                );
            endforeach;
        }

        $totalCount = 0;
        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $finalData
        );

        echo json_encode($listingData);
    }

    public function searchForRolodex2() {
        //print_r($_POST); exit;
        extract($_POST);
        $where = '';
        $orderby = '';
        $searchResult = array();
        if ($add1 != '' && $add2 == '' && $city == '' && $state == '') {
            $where = array('c2.address1' => $add1);
        }
        if ($add1 == '' && $add2 != '' && $city == '' && $state == '') {
            $where = array('c2.address2' => $add2);
        }
        if ($add1 == '' && $add2 == '' && $city != '' && $state == '') {
            $where = array('c2.city' => $city);
        }
        if ($add1 == '' && $add2 == '' && $city == '' && $state != '') {
            $where = array('c2.state' => $state);
        }
        if ($add1 != '' && $add2 != '' && $city == '' && $state == '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2);
        }
        if ($add1 != '' && $add2 != '' && $city != '' && $state == '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2, 'c2.city' => $city);
        }
        if ($add1 != '' && $add2 != '' && $city != '' && $state != '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2, 'c2.city' => $city, 'c2.state' => $state);
        }
        if ($add1 == '' && $add2 != '' && $city != '' && $state == '') {
            $where = array('c2.address2' => $add2, 'c2.city' => $city);
        }
        if ($add1 == '' && $add2 != '' && $city != '' && $state != '') {
            $where = array('c2.address2' => $add2, 'c2.city' => $city, 'c2.state' => $state);
        }
        if ($add1 != '' && $add2 == '' && $city != '' && $state == '') {
            $where = array('c2.address1' => $add1, 'c2.city' => $city);
        }
        if ($add1 == '' && $add2 == '' && $city != '' && $state != '') {
            $where = array('c2.state' => $state, 'c2.city' => $city);
        }
        if ($add1 != '' && $add2 == '' && $city == '' && $state != '') {
            $where = array('c2.address1' => $add1, 'c2.state' => $state);
        }
        if ($add1 == '' && $add2 != '' && $city == '' && $state != '') {
            $where = array('c2.address2' => $add2, 'c2.state' => $state);
        }
        if ($add1 != '' && $add2 == '' && $city != '' && $state != '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2, 'c2.state' => $state);
        }
        
        if ($search['value'] != '') {
            $singleFilter = '(c2.firm LIKE "%' .$search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.phone1 LIKE "%' . $search['value'] . '%" OR c2.address1 LIKE "%' . $search['value'] .'%" OR c.first LIKE "%' . $search['value'] . '%" OR c.last LIKE "%' . $search['value'] . '%" OR c2.address2 LIKE "%' . $search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.state LIKE "%' . $search['value'] . '%" OR c2.zip LIKE "%' . $search['value']. '%")';
        }

        $result = $this->case_model->getRolodexSearchParties($where, $start, $length, $order , $singleFilter);
        //print_r($result);

        $finalData = array();
        foreach ($result as $data) {
            $Name = '';
            if (isset($data->first) && !empty($data->first)) {
                $Name = $data->first . " , " . $data->last;
            } else {
                $Name = $data->last;
            }
            $tmpArr = array(
                "cardcode" => $data->cardcode,
                "name" => $Name,
                "type" => $data->type,
                "firm" => $data->firm,
                "phone" => $data->phone1,
                "address" => $data->address1,
                "address2" => $data->address2,
                "city" => $data->city,
                "state" => $data->state,
                "zip" => $data->zip,
            );
            array_push($finalData, $tmpArr);
        }

        echo json_encode($finalData);
    }

    public function searchForRolodex1() {
        //print_r($_POST);
        extract($_POST);
        $where = '';
        $orderby = '';
        $searchResult = array();

        $serch_name = explode("-", "$rolodex_search_parties");

        if ($rolodex_search_parties == 'rolodex-firm') {
            $where = 'c2.firm like "' . $rolodex_search_text . '%" OR c.first like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-first-last") {
            if (strpos($rolodex_search_text, ',') !== false) {
                $splitedVal = explode(",", $rolodex_search_text);
                $where = 'c.last  like "' . trim($splitedVal[0]) . '%" AND c.first like "' . trim($splitedVal[1]) . '%"';
            } else {
                $where = 'c.first  like "' . $rolodex_search_text . '%" OR c.last like "' . $rolodex_search_text . '%"';
            }
            $orderby = 'c.first asc , c.last asc';
        } else if ($rolodex_search_parties == "rolodex-company") {
            $where = 'c2.firm  like "' . $rolodex_search_text . '%"';
            $orderby = 'c2.firm asc,c.first asc , c.last asc';
        } else if ($rolodex_search_parties == "rolodex-cardno") {
            if (substr($rolodex_search_text, 0, 1) === '<') {
                $str = ltrim($rolodex_search_text, '<');
                $where = 'c.cardcode = "' . $str . '"';
            } else {
                $where = 'c.cardcode  like "' . $rolodex_search_text . '%"';
            }
        } else if ($rolodex_search_parties == "rolodex-social_sec") {
            $where = 'c.social_sec  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-email") {
            $where = 'c.email  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-licenseno") {
            $where = 'c.licenseno  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-type") {
            $where = 'c.type  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-first") {
            $where = 'c.first  like "' . $rolodex_search_text . '%"';
        } else if ($rolodex_search_parties == "rolodex-details") {
            $where = '';
        } else if ($rolodex_search_parties == "rolodex-relational") {
            $where = '';
        } else if ($rolodex_search_parties == "rolodex-query") {
            $where = '';
        } else {
            $where = 'c.' . $serch_name[1] . '  like "' . $rolodex_search_text . '%"';
        }

        $result = $this->case_model->getRolodexSearchParties1($where, $orderby);
        //print_r($result);

        $finalData = array();
        foreach ($result as $data) {
            $Name = '';
            if (isset($data->first) && !empty($data->first)) {
                $Name = $data->last . " , " . $data->first;
            } else {
                $Name = $data->last;
            }
            $tmpArr = array(
                "cardcode" => $data->cardcode,
                "name" => ($Name != "") ? $Name : '',
                "type" => ($data->type) ? $data->type : '',
                "firm" => ($data->firm) ? $data->firm : '',
                "city" => ($data->city) ? $data->city : '',
                "phone" => ($data->phone1) ? $data->phone1 : '',
                "address" => trim(trim($data->address1 . ', ' . $data->address2), ','),
                "zip" => ($data->zip) ? $data->zip : ''
            );
            array_push($finalData, $tmpArr);
        }

        echo json_encode($finalData);
    }

    public function searchForRolodex_email() {
        //print_r($_POST);
        extract($_POST);
        $where = '';
        $orderby = '';
        $searchResult = array();

        $serch_name = explode("-", "$rolodex_search_parties_for_partyemail");

        if ($rolodex_search_parties_for_partyemail == 'rolodex-firm') {
            $where = 'c2.firm like "' . $rolodex_search_text_for_partyemail . '%" OR c.first like "' . $rolodex_search_text_for_partyemail . '%"';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-first-last") {
            if (strpos($rolodex_search_text_for_partyemail, ',') !== false) {
                $splitedVal = explode(",", $rolodex_search_text_for_partyemail);
                $where = 'c.last  like "' . trim($splitedVal[0]) . '%" AND c.first like "' . trim($splitedVal[1]) . '%"';
            } else {
                $where = 'c.first  like "' . $rolodex_search_text_for_partyemail . '%" OR c.last like "' . $rolodex_search_text_for_partyemail . '%"';
            }
            $orderby = 'c.first asc , c.last asc';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-company") {
            $where = 'c2.firm  like "' . $rolodex_search_text_for_partyemail . '%"';
            $orderby = 'c2.firm asc,c.first asc , c.last asc';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-cardno") {
            if (substr($rolodex_search_text_for_partyemail, 0, 1) === '<') {
                $str = ltrim($rolodex_search_text_for_partyemail, '<');
                $where = 'c.cardcode = "' . $str . '"';
            } else {
                $where = 'c.cardcode  like "' . $rolodex_search_text_for_partyemail . '%"';
            }
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-social_sec") {
            $where = 'c.social_sec  like "' . $rolodex_search_text_for_partyemail . '%"';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-email") {
            $where = 'c.email  like "' . $rolodex_search_text_for_partyemail . '%"';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-licenseno") {
            $where = 'c.licenseno  like "' . $rolodex_search_text_for_partyemail . '%"';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-type") {
            $where = 'c.type  like "' . $rolodex_search_text_for_partyemail . '%"';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-first") {
            $where = 'c.first  like "' . $rolodex_search_text_for_partyemail . '%"';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-details") {
            $where = '';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-relational") {
            $where = '';
        } else if ($rolodex_search_parties_for_partyemail == "rolodex-query") {
            $where = '';
        } else {
            $where = 'c.' . $serch_name[1] . '  like "' . $rolodex_search_text_for_partyemail . '%"';
        }

        $result = $this->case_model->getRolodexSearchParties1($where, $orderby);
        //print_r($result);

        $finalData = array();
        foreach ($result as $data) {
            $Name = '';
            if (isset($data->first) && !empty($data->first)) {
                $Name = $data->last . " , " . $data->first;
            } else {
                $Name = $data->last;
            }
            $tmpArr = array(
                "cardcode" => $data->cardcode,
                "name" => ($Name != "") ? $Name : '',
                "type" => ($data->type) ? $data->type : '',
                "firm" => ($data->firm) ? $data->firm : '',
                "city" => ($data->city) ? $data->city : '',
                "phone" => ($data->phone1) ? $data->phone1 : '',
                "address" => trim(trim($data->address1 . ', ' . $data->address2), ','),
                "zip" => ($data->zip) ? $data->zip : ''
            );
            array_push($finalData, $tmpArr);
        }

        echo json_encode($finalData);
    }

    public function viewSearchedPartieDetails() {

        $card_details = $this->case_model->getSearchedPartieCardDetails($this->input->post('cardcode'), $this->input->post('caseno') == '' ? $this->input->post('caseno') : "");
        if (count($card_details)) {
            echo json_encode($card_details[0]);
        } else {
            echo json_encode(false);
        }
    }

    public function addInjuries() {
        $formData = $_POST;

        $rec = array();
        foreach ($formData as $key => $val) {
            $key = str_replace("field_", "", $key);
            $rec[$key] = $val;
        }

        if ($rec['dor_date'] != '') {
            $rec['dor_date'] = date('Y-m-d 00:00:00', strtotime($rec['dor_date']));
        }
        if ($rec['adj1a'] != '') {
            //$rec['adj1a'] = date('Y-m-d 00:00:00', strtotime($rec['adj1a']));
            $rec['adj1a'] = $rec['adj1a'];
        }
        if ($rec['doi'] != '') {
            $rec['doi'] = date('Y-m-d 00:00:00', strtotime($rec['doi']));
        }
        if ($rec['doi2'] != '') {
            $rec['doi2'] = date('Y-m-d 00:00:00', strtotime($rec['doi2']));
        }
        //if($rec->adj1c != '') { $rec->adj1c = date('m/d/Y',strtotime($rec->adj1c)); }
		
        if ($rec['adj5d'] != '') {
            $rec['adj5d'] = $rec['adj5d'];
        }

        if ($rec['adj7b'] != '') {
            $rec['adj7b'] = date('Y-m-d 00:00:00', strtotime($rec['adj7b']));
        }
        if ($rec['adj10a'] != '') {
            $rec['adj10a'] = date('Y-m-d 00:00:00', strtotime($rec['adj10a']));
        }
        if ($rec['s_w'] != '') {
            $rec['s_w'] = date('Y-m-d 00:00:00', strtotime($rec['s_w']));
        }
        if ($rec['other_sol'] != '') {
            $rec['other_sol'] = date('Y-m-d 00:00:00', strtotime($rec['other_sol']));
        }
        if ($rec['follow_up'] != '') {
            $rec['follow_up'] = date('Y-m-d 00:00:00', strtotime($rec['follow_up']));
        }
        if ($rec['liab'] != '') {
            $rec['liab'] = date('Y-m-d 00:00:00', strtotime($rec['liab']));
        }
        if (isset($rec['ct1'])) {
            $rec['ct1'] = 1;
        } else {
            $rec['ct1'] = 0;
        }

        $response = array();
        if ($rec['injury_id'] != 0) {
            $res = $this->common_functions->editRecord('injury', $rec, array('injury_id' => $rec['injury_id']));
		
            if ($res >= 0) {
                $response = array('status' => 1, 'injury_id' => $rec['injury_id'], 'results' => $rec, 'message' => 'Injury is successfully edited.');
            } else {
                $response = array('status' => 0, 'message' => 'Oops! Something went wrong.');
            }
        } else {
            unset($rec['injury_id']);

            $res = $this->common_functions->insertRecord('injury', $rec);

            if ($res > 0) {
                $rec['injury_id'] = $res;
                $response = array('status' => 1, 'injury_id' => $res, 'results' => $rec, 'message' => 'Injury is successfully added.');
            } else {
                $response = array('status' => 0, 'message' => 'Oops! Something went wrong.');
            }
        }
	
        echo json_encode($response);
    }

    public function RemoveInjury() {
        extract($_POST);
        $res = $this->common_functions->deleteRec('injury', array('injury_id' => $injury_id));

        if ($res > 0) {
            $response = array('status' => 1, 'message' => 'Injury is successfully removed.');
        } else {
            $response = array('status' => 0, 'message' => 'Oops! Something went Wrong');
        }

        echo json_encode($response);
    }

    public function cloneCase() {
        extract($_POST);
        $resp = array();
        if (in_array('cParties', $CopyData)) {
            //$getPartiesList = $this->common_functions->getAllRecord('casecard', '*', array('caseno' => $caseFrom));
            $where = "caseno = '$caseFrom' AND orderno != 1";
            $getPartiesList = $this->common_functions->getAllRecord('casecard', '*', $where);
            //echo $this->db->last_query(); exit;
            if (count($getPartiesList) > 0) {
                foreach ($getPartiesList as $key => $val) {
                    $val->caseno = $caseTo;
                    $res = $this->case_model->cloneParties($val);
                    if (isset($res['status']) && $res['status'] == 0) {
                        $resp = array('status' => 0, 'message' => 'Something went wrong when cloning the Parties.Please try again.');
                        echo json_encode($resp);
                        exit;
                    }
                }
            }
        }

        if (in_array('cInjuries', $CopyData)) {
            $file_names = array();
            $getInjuriesList = $this->common_functions->getAllRecord('injury', '*', array('caseno' => $caseFrom));
            // print_r($getInjuriesList);exit;
            if (count($getInjuriesList) > 0) {
                $caseCond = array('caseno' => $caseTo);
                $lastcaseAct = $this->common_functions->last_caseact_record($caseCond);
                if (!file_exists(DOCUMENTPATH . 'clients/' . $caseTo)) {
                    mkdir(DOCUMENTPATH . 'clients/' . $caseTo, 0777, true);
                }
                foreach ($getInjuriesList as $key => $clone_val) {
                    $lastcaseAct++;
                    $clone_val->caseno = $caseTo;
                    $filename = '';
                    $injury_id = $clone_val->injury_id;
                    $res = $this->case_model->cloneInjuries($clone_val);
                    if(!is_array($res)) {
                        $all_files = glob(DOCUMENTPATH.'clients/'.$caseFrom.'/I*'.$injury_id.'.*');
                        foreach($all_files as $k => $v) {
                            $old_file = $v;
                            if(strpos($old_file, $injury_id) != false) {
                                if (in_array('cCaseActivity', $CopyData)) {
                                    $new_filename = str_replace([$caseFrom, pathinfo($v)['filename']], [$caseTo, "I".$lastcaseAct.'-'.$res], $v);
                                } else {
                                    $new_filename = str_replace([$caseFrom, pathinfo($v)['filename']], [$caseTo, "I".$res], $v);
                                }
                                $latest_new_filename = $this->checkExistenceOfFile($new_filename, "I".$res, 1);
                                copy($old_file, $latest_new_filename);
                            }
                        }
                    } elseif(isset($res['status']) && $res['status'] == 0) {
                        $resp = array('status' => 0, 'message' => 'Something went wrong when cloning the Injuries.Please try again.');
                        echo json_encode($resp);
                        exit;
                    }
                }
            }
        }
        if (in_array('cCaseActivity', $CopyData)) {
            $file_names = array();
            $getCaseActivityList = $this->case_model->getCaseActivityAll($caseFrom, $category = '', $chackvalue = '');

            $caseCond = array('caseno' => $caseTo);
            $lastcaseAct = $this->common_functions->last_caseact_record($caseCond);
            if (count($getCaseActivityList) > 0) {
                if (!file_exists(DOCUMENTPATH . 'clients/' . $caseTo)) {
                    mkdir(DOCUMENTPATH . 'clients/' . $caseTo, 0777, true);
                }
                $i = 0;
                foreach ($getCaseActivityList as $key => $val) {
                    $clone_val = $val;
                    $lastcaseAct++;
                    $clone_val->caseno = $caseTo;
                    $clone_val->actno = $lastcaseAct;
                    $filename = '';

                    $dir = ASSPATH . 'clients/' . $clone_val->caseno;
                    if($val->filename != '') {
                        $filename = $dir . "/F" . $clone_val->actno.'.'.pathinfo($val->filename, PATHINFO_EXTENSION);
                        $clone_val->filename = 'F'.$clone_val->actno.'.'.pathinfo($val->filename, PATHINFO_EXTENSION);
                    } else {
                        $clone_val->filename = '';
                    }
                    if($clone_val->filename != '') {
                        $file_names[$i] = $filename;
                        $i++;
                    }

                    $res = $this->case_model->cloneCaseActivity($clone_val);

                    if (isset($res['status']) && $res['status'] == 0) {
                        $resp = array('status' => 0, 'message' => 'Something went wrong when cloning the Case Activity.Please try again.');
                        echo json_encode($resp);
                        exit;
                    }
                }
                $scan_dir = scandir(DOCUMENTPATH.'clients/'.$caseFrom);
                $i = 0;
                $src_files = array();
                foreach($scan_dir as $key => $value) {
                    if($value != '.' && $value != '..') {
                        $src_files[$i] = $value;
                        $i++;
                    }
                }
                foreach ($src_files as $key => $value) {
                    copy(DOCUMENTPATH.'clients/'.$caseFrom.'/'.$value, $file_names[$key]);
                }
            }
        }

        $resp = array('status' => 1, 'message' => 'Case is successfully cloned');
        echo json_encode($resp);
    }

    public function checkExistenceOfFile($filename, $name, $cnt) {
        if(!file_exists($filename)) {
            return $filename;
        } else {
            $new_filename = str_replace($name, $name.'_'.$cnt, $filename);
            $new_name = $name.'_'.$cnt;
            return $this->checkExistenceOfFile($new_filename, $new_name, $cnt++);
        }
    }

    public function addSearchedPartyToCase() {
        $defendat_cnt = $this->case_model->countdefendant($this->input->post('caseno'));
        $employer_cnt = $this->case_model->countemployer($this->input->post('caseno'));
        if (($this->input->post('type') == 'DEFENDANT' || $this->input->post('type') == 'EMPLOYER') && $defendat_cnt == 0 && $employer_cnt == 0) {
            $cond = array('caseno' => $this->input->post('caseno'));
            $cardcode = $this->common_functions->getRecordById('casecard', 'cardcode', $cond);
            $cond2 = array('cardcode' => $cardcode->cardcode);
            $cardcode = $this->common_functions->getRecordById('card', 'first,last', $cond2);
            $fullname = $cardcode->first . ' ' . $cardcode->last;
            $cond3 = array('cardcode' => $this->input->post('cardcode'));
            $defcardcode = $this->common_functions->getRecordById('card', 'firmcode', $cond3);
            $cond4 = array('firmcode' => $defcardcode->firmcode);
            $defcardfirm = $this->common_functions->getRecordById('card2', 'firm', $cond4);
            $cond5 = array('firmcode' => $defcardcode->firmcode);
            $defname = $this->common_functions->getRecordById('card', 'first,last', $cond5);

            if ($defcardfirm->firm != '') {
                $deffullname = $defcardfirm->firm;
            } else {
                $deffullname = $defname->first . ' ' . $defname->last;
            }

            $newCap = $fullname . ' v. ' . $deffullname;
            $where = array('caseno' => $this->input->post('caseno'));
            $lastRecord = $this->common_functions->editRecord('case', array('caption1' => $newCap), $where);
        }

        $card_details = $this->case_model->addSearchedPartyToCase($this->input->post('cardcode'), $this->input->post('caseno'), $this->input->post('type'));

        echo $card_details;
    }

    public function removeParty() {
        $cond = array('cardcode' => $this->input->post('cardcode'), 'caseno' => $this->input->post('caseno'));
        $cardtyp = $this->common_functions->getRecordById('casecard', 'type', $cond);
        $cond1 = array('caseno' => $this->input->post('caseno'),'orderno'=>'1');
        $cardcode1 = $this->common_functions->getRecordById('casecard', 'cardcode', $cond1);
        $cond2 = array('cardcode' => $cardcode1->cardcode);
        $cardcode = $this->common_functions->getRecordById('card', 'first,last', $cond2);
        $fullname = $cardcode->first . ' ' . $cardcode->last;
        $where = array('caseno' => $this->input->post('caseno'));
        $captionArr = $this->common_functions->getRecordById('case', 'caption1', $where);
        $def = explode("v. ",$captionArr->caption1);
        
        
        $cond_def = array('caseno' => $this->input->post('caseno'));
        $cardcode_def = $this->common_functions->getRecordById('casecard', 'cardcode', $cond_def);
        $cond2_def = array('cardcode' => $cardcode_def->cardcode);
        $cardcode_def = $this->common_functions->getRecordById('card', 'first,last', $cond2_def);
        $fullname_def = $cardcode->first . ' ' . $cardcode_def->last;
        $cond3_def = array('cardcode' => $this->input->post('cardcode'));
        $defcardcode_def = $this->common_functions->getRecordById('card', 'firmcode', $cond3_def);
        
        $cond4_def = array('firmcode' => $defcardcode_def->firmcode);
        $defcardfirm_def = $this->common_functions->getRecordById('card2', 'firm', $cond4_def);
        $cond5_def = array('firmcode' => $defcardfirm_def->firmcode);
        $defname_case = $this->common_functions->getRecordById('card', 'first,last', $cond5_def);

        if ($defcardfirm_def->firm != '') {
            $deffullname = $defcardfirm_def->firm;
        } else {
            $deffullname = $defname_case->first . ' ' . $defname_case->last;
        }
        
        
        if ((strtolower($cardtyp->type) == 'defendant' || strtolower($cardtyp->type) == 'employer') && trim($def[1]) == trim($deffullname)) {
            $newCap = $fullname . ' v. Straussner & Sherman';
            $lastRecord = $this->common_functions->editRecord('case', array('caption1' => $newCap), $where);
        } else {
            $newCap = $captionArr->caption1;
        }
        $card_details = $this->case_model->removeParty($this->input->post('cardcode'), $this->input->post('caseno'), $newCap);
        echo $card_details;
    }

    public function addNewCase() {
        //echo "<pre>";print_r($_REQUEST);
        $case = $this->case_model->addNewCase($this->input->post('first_name'), $this->input->post('last_name'));
        echo $case;
    }

    public function editCase() {
        $case = $this->case_model->editCase($this->input->post());

        if ($case >= 0) {
            $response = array('status' => 1, 'message' => 'Case is successfully edited!');
        } else {
            $response = array('status' => 0, 'message' => 'Case editing failed!');
        }

        echo json_encode($response);
    }

    public function setPartiesCaseSpecific() {

        $case = $this->case_model->setPartiesCaseSpecific($this->input->post());

        if ($case != 0) {
            $response = array('status' => 1, 'message' => 'Case is successfully edited!');
        } else {
            $response = array('status' => 0, 'message' => 'Case editing failed!');
        }
        echo json_encode($response);
    }

    public function getCaseSpecific() {
        $case = $this->case_model->getCaseSpecific($this->input->post('caseno'), $this->input->post('cardcode'));

        echo json_encode($case);
    }

    public function checkDuplicateCase() {
        $res = $this->case_model->checkDuplicateCase($this->input->post('first_name'), $this->input->post('last_name'));
        if (!empty($res)) {
            echo json_encode(array("status" => 1, "prospect_id" => $this->input->post('prospect_id'), "data" => $res));
        } else {
            echo json_encode(array("status" => 0, "data" => []));
        }
    }

    public function printCaseDetails() {

        if (count($_POST) == 0) {
            $caseNo = $this->uri->segment(3);
        } else {
            extract($_POST);
        }

        $this->data['caseNo'] = $caseNo;

        $this->data['case_details'] = $this->case_model->getCaseFullDetails($this->data['caseNo']);

        if ($this->data['case_details'] == '') {
            $case_details = array();
            $defaultArray = $this->setBlankCase();
            $case_details = $this->case_model->getCaseDataByCaseId($this->data['caseNo']);

            $this->data['case_details'][0] = (object) array_merge($defaultArray, $case_details[0]);
        }
        $this->data['case_details'][0]->quickNotes = $this->case_model->getQuickNotes($this->data['caseNo']);

        $this->data['case_details'][0]->myMessage1 = $this->case_model->getMyMessage1($this->data['caseNo']);
        $this->data['case_details'][0]->myMessage2 = $this->case_model->getMyMessage2($this->data['caseNo']);
        $this->data['case_details'][0]->myMessage3 = $this->case_model->getMyMessage3($this->data['caseNo']);
        $this->data['case_details'][0]->myMessage4 = $this->case_model->getMyMessage4($this->data['caseNo']);

        $this->data['case_details'][0]->venueName = '';

        if (isset($this->data['case_details'][0]->mainVenue) && $this->data['case_details'][0]->mainVenue != 0 && $this->data['case_details'][0]->mainVenue != '') {
            foreach ($this->data['case_details'] as $key => $val) {
                if ($this->data['case_details'][0]->mainVenue == $val->cardcode) {
                    $this->data['case_details'][0]->venueName = $val->venue;
                }
            }
        }

        $this->load->view('cases/print_casedetail', $this->data);
    }

    public function setBlankCase() {
        $caseDetails = array
            (
            'caseno' => 4,
            'cardcode' => '',
            'orderno' => '',
            'officeno' => '',
            'side' => '',
            'type' => '',
            'flags' => 0,
            'notes' => '',
            'casetype' => '',
            'casestat' => '',
            'yourfileno' => '',
            'venue' => '',
            'location' => '',
            'atty_resp' => '',
            'atty_hand' => '',
            'para_hand' => '',
            'sec_hand' => '',
            'dateenter' => '',
            'dateopen' => '',
            'dateclosed' => '',
            'd_r' => '',
            'rb' => '',
            'followup' => '',
            'ps' => '',
            'psdate' => '',
            'protected' => '',
            'photototal' => '',
            'relatotal' => '',
            'datepdf1' => '',
            'caption1' => '',
            'udfall' => '',
            'udfall2' => '',
            'firmcode' => '',
            'letsal' => '',
            'salutation' => '',
            'first' => '',
            'middle' => '',
            'last' => '',
            'suffix' => '',
            'social_sec' => '',
            'title' => '',
            'home' => '',
            'business' => '',
            'fax' => '',
            'car' => '',
            'beeper' => '',
            'email' => '',
            'birth_date' => '',
            'interpret' => '',
            'language' => '',
            'licenseno' => '',
            'specialty' => '',
            'mothermaid' => '',
            'lastchg' => '',
            'origchg' => '',
            'origdt' => '',
            'lastdt' => '',
            'comments' => '',
            'fld1' => '',
            'fld2' => '',
            'fld3' => '',
            'fld4' => '',
            'fld5' => '',
            'fld6' => '',
            'fld7' => '',
            'fld8' => '',
            'fld9' => '',
            'fld10' => '',
            'fld11' => '',
            'fld12' => '',
            'fld13' => '',
            'fld14' => '',
            'fld15' => '',
            'fld16' => '',
            'fld17' => '',
            'fld18' => '',
            'fld19' => '',
            'fld20' => '',
            'spinst1' => '',
            'firm' => '',
            'tax_id' => '',
            'address1' => '',
            'address2' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'phone1' => '',
            'phone2' => '',
            'fax2' => '',
            'firmkey' => '',
            'color' => '',
            'eamsref' => '',
            'mailing1' => '',
            'mailing2' => '',
            'mailing3' => '',
            'mailing4' => '',
            'mainVenue' => '',
        );

        return $caseDetails;
    }

    public function getCaseCaption() {

        extract($_POST);

        $cond = array('caseno' => $caseNo);

        $captionArr = $this->common_functions->getRecordById('case', 'caption1', $cond);

        $resp = array('status' => 0, 'caption' => '');
        if (count($captionArr) > 0) {
            $resp = array('status' => 1, 'caption' => $captionArr->caption1);
        }

        echo json_encode($resp);
    }

    //remove case
    public function RemoveCase() {
        extract($_POST);

        $res = $this->common_functions->editRecord('case', array('case_status' => $caseStatus), array('caseno' => $caseNo));
        if ($res > 0) {
            if ($caseStatus == 1) {
                $response = array('status' => 1, 'message' => 'Case Recalled Successfully !!!');
            } else {
                $response = array('status' => 1, 'message' => 'Case successfully removed!');
            }
            // $this->session->set_flashdata('case_message', 'Your Case Removed Successfully');
        } else {
            $response = array('status' => 0, 'message' => 'Oops! Something went Wrong');
        }

        echo json_encode($response);
    }

    public function attach_name() {

        $cardcode = $this->input->post('cardcode');
        $caseno = $this->input->post('caseno');
        $prospect_id = $this->input->post('prospect_id');
        //$getcardDetails = $this->common_functions->getAllRecord('card', '*', array('cardcode' => $cardcode));
        $getcardDetails = $this->case_model->getcardType($cardcode,$caseno);
        //echo $this->db->last_query();
        //echo "<pre>"; print_r($getcardDetails); exit;
        /* echo "<pre>";print_r($getcardDetails);
          unset($getcardDetails[0]->cardcode);
          $cardcode = $this->common_functions->insertRecord('card',$getcardDetails[0]); */

        $cardcode = $getcardDetails->cardcode;
        $case = $this->case_model->addNewCase($this->input->post('first_name'), $this->input->post('last_name'), $this->input->post('casetype'), $this->input->post('casestat'));
        $data = array('cardcode' => $cardcode,
            'caseno' => $case,
            'type' => $getcardDetails->cardtype,
            'orderno' => 1);
        $id = $this->common_functions->insertRecord('casecard', $data);
        $ii = 1;
        for ($i = 1000001; $i <= 1000021; $i++) {
                        if ($i == 1000021) {
                           /* $event = $formdata['card_type'] . " Entered Into Computer";*/
                            $event = "APPLICANT Entered Into Computer";
                        } else {
                            $event = '';
                        }
                        $caseActArr = array(
                            "actno" => $ii++,
                            "caseno" => $case,
                            "initials0" => $this->session->userdata('user_data')['username'],
                            'initials' => '',
                            "date" => date('Y-m-d H:i:s'),
                            'event' => $event,
                            'atty' => '',
                            'title' => '0',
                            'cost' => '0.00',
                            'minutes' => '0',
                            'rate' => '0',
                            'arap' => '1',
                            'mainnotes' => '0',
                            'redalert' => '0',
                            'upontop' => '0',
                            'type' => '',
                            "category" => $i,
                            'color' => '1',
                            'access' => '1',
                            'wordstyle' => '0',
                            'ole3style' => '0',
                            'frmtm' => '0.00',
                            'frmbm' => '0.00',
                            'frmlm' => '0.00',
                            'frmrm' => '0.00',
                            'frmwidth' => '0.00',
                            'frmheight' => '0.00',
                            'orient' => '0',
                            'copies' => '0',
                            'typeact' => '0',
                            'bistyle' => '0',
                            'biteamno' => '0',
                            'bicycle' => '1899-12-30 00:00:00',
                            'bitime' => '0',
                            'bihourrate' => '0',
                            'bifee' => '0.00',
                            'bicost' => '0.00',
                            'bidesc' => '',
                            'bicheckno' => '',
                            'bipmt' => '0.00',
                            'bipmtdate' => '1899-12-30 00:00:00',
                            'bipmtduedt' => '1899-12-30 00:00:00',
                            'bilatefee' => '0.00'
                        );
                        $this->common_functions->insertRecord('caseact', $caseActArr);
                    }
        
        
        $pnccaseupdate = $this->case_model->updatepnccase($prospect_id, $case);
        if ($id != 0) {
            echo json_encode(array("status" => 1, "caseno" => $case, "cardcode" => $cardcode));
        } else {
            echo json_encode(array("status" => 0, "caseno" => $case, "cardcode" => $cardcode));
        }
    }

    public function getReletedDataAjax() {
        extract($_POST);
        $singleFilter = '';
        if ($search['value'] != '') {
            $singleFilter = '(cd.caseno LIKE "%' . $search['value'] . '%" OR cd.casetype LIKE "%' . $search['value'] . '%" OR cd.casestat LIKE "%' . $search['value'] . '%" OR cd.atty_hand LIKE "%' . $search['value'] . '%" OR cd.caption1 LIKE "%' . $search['value'] . '%")';
        }

        $result = $this->case_model->getReletedCaseListing($start, $length, $caseNo, $order, '', $singleFilter);

        $finalData = array();
        if ($result > 0) {
            foreach ($result as $data) {
                $tmpArr = array(
                    $data->caseno,
                    $data->casetype,
                    $data->casestat,
                    $data->atty_hand,
                    $data->caption1,
                    $data->caseno,
                );
                array_push($finalData, $tmpArr);
            }
        }

        $relatedCaseCount = $this->case_model->getReletedCaseListing('', '', $caseNo, '', 1);

        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $relatedCaseCount,
            'recordsFiltered' => $relatedCaseCount,
            'data' => $finalData,
        );
        echo json_encode($listingData);
    }

    //add related case
    public function addrelatedcase() {
        extract($_POST);
        $checkCaseExits = $this->common_functions->check_record_exists('case', array('caseno' => $relatedcasenumber));

        /* if($checkCaseClone == 0)
          { */
        $checkCaseClone = $this->common_functions->check_case_clone($caseno, $relatedcasenumber);
        $response = array();
        if ($checkCaseExits > 0) {
            $currentRelatedNo = $this->common_functions->getRecordById('casekey', 'related', array('caseno' => $caseno));
            $attachRelatedNo = $this->common_functions->getRecordById('casekey', 'related', array('caseno' => $relatedcasenumber));
            if (!empty($currentRelatedNo)) {
                $currentRelatedNo1 = $currentRelatedNo->related;
            } else {
                $currentRelatedNo1 = 0;
            }
            if (!empty($attachRelatedNo)) {
                $attachRelatedNo1 = $attachRelatedNo->related;
            } else {
                $attachRelatedNo1 = 0;
            }

            if ($currentRelatedNo1 != 0 && $attachRelatedNo1 != 0) {
                $relatedKey = $currentRelatedNo->related;
                $caseArray = $this->common_functions->getAllRecord('casekey', 'caseno', array('related' => $attachRelatedNo->related));

                if (count($caseArray) > 0) {
                    foreach ($caseArray as $cases) {
                        $res = $this->common_functions->editRecord('casekey', array('related' => $relatedKey), array('caseno' => $cases->caseno));
                        if ($res >= 0) {
                            $resp = 1;
                        } else {
                            echo json_encode(array('status' => 0, 'message' => 'Oops! Something went Wrong'));
                            exit;
                        }
                    }
                    if ($resp == 1 && $checkCaseClone == 0) {
                        $response = array('status' => 1, 'message' => 'Case successfully related to "Current Case"!');
                    } else if ($checkCaseClone == 1 && $resp == 1) { {
                            echo json_encode(array('status' => 0, 'message' => 'This case is already attached!'));
                            exit;
                        }
                    } else {
                        echo json_encode(array('status' => 0, 'message' => 'Oops! Something went Wrong'));
                        exit;
                    }
                }
            } else if ($currentRelatedNo1 > 0 && $attachRelatedNo1 == 0) {
                $relatedKey = $currentRelatedNo->related;
                $res = $this->common_functions->editRecord('casekey', array('related' => $relatedKey), array('caseno' => $relatedcasenumber));
                if ($res >= 0 && $checkCaseClone == 0) {
                    $response = array('status' => 1, 'message' => 'Case successfully related to "Current Case"!');
                } else if ($checkCaseClone == 1 && $resp >= 0) { {
                        echo json_encode(array('status' => 0, 'message' => 'This case is already attached!'));
                        exit;
                    }
                } else {
                    echo json_encode(array('status' => 0, 'message' => 'Oops! Something went Wrong'));
                    exit;
                }
            } else if ($currentRelatedNo1 == 0 && $attachRelatedNo1 > 0) {
                $relatedKey = $attachRelatedNo->related;
                $res = $this->common_functions->editRecord('casekey', array('related' => $relatedKey), array('caseno' => $caseno));
                if ($res >= 0) {
                    $response = array('status' => 1, 'message' => 'Case successfully related to "Current Case"!');
                } else {
                    echo json_encode(array('status' => 0, 'message' => 'Oops! Something went Wrong'));
                    exit;
                }
            } else {
                $relatedKey = $this->case_model->getMaxRelationKey();

                $res = $this->common_functions->editRecord('casekey', array('related' => $relatedKey + 1), array('caseno' => $caseno));
                $res = $this->common_functions->editRecord('casekey', array('related' => $relatedKey + 1), array('caseno' => $relatedcasenumber));
                if ($res >= 0) {
                    $response = array('status' => 1, 'message' => 'Case successfully related to "Current Case"!');
                } else {
                    echo json_encode(array('status' => 0, 'message' => 'Oops! Something went Wrong'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('status' => 0, 'message' => 'Attached case not found !!!'));
            exit;
        }
        //}
        /* else
          {
          echo json_encode(array('status'=>0,'message'=>'This case is already attached!'));
          exit;
          } */

        echo json_encode($response);
        exit;
    }

    public function deleteRelatedcase() {
        extract($_POST);
        $data = array('related' => 0);
        $result = $this->common_functions->editRecord('casekey', $data, array('caseno' => $caseId));

        if ($result >= 0) {
            $response = array('status' => 1, 'message' => 'Related Case is Deleted.');
        } else {
            $response = array('status' => 0, 'message' => 'Oops! Something went wrong.');
        }

        echo json_encode($response);
    }

    public function SameSocialSecurityCase() {
        extract($_POST);

        $card = $this->common_functions->getAllRecord('card', 'cardcode', array('social_sec' => $social_sec));

        //echo '<pre>'; print_r($card);
        $response = array();
        $cases = array();
        if (count($card) > 0) {
            $i = 0;
            foreach ($card as $key1 => $val1) {
                //if (isset($card->cardcode) && $card->cardcode != '') {
                //echo $val1->cardcode;
                $casesArray = $this->common_functions->getSingleRecordById('casecard', 'caseno', array('cardcode' => $val1->cardcode));
                $casesfnameArray = $this->common_functions->getSingleRecordById('card', 'first', array('cardcode' => $val1->cardcode));
                $caseslnameArray = $this->common_functions->getSingleRecordById('card', 'last', array('cardcode' => $val1->cardcode));
                $casescaptionArray = $this->common_functions->getSingleRecordById('case', 'caption1', array('caseno' => $casesArray->caseno));
                //echo $this->db->last_query(); exit;
                //echo '<pre>'; print_r($casesArray);
                //foreach ($casesArray as $key => $val) {
                if ($casesArray->caseno != $caseNO && !empty($casesArray->caseno)) {
                    //                    array_push($cases, $val->caseno);
                    $cases[$i]['checkbox'] = '<input type="checkbox" id="relatedcase" name="relatedcase" value="' . $casesArray->caseno . '" />';
                    $cases[$i]['caseno'] = $casesArray->caseno;
                    $cases[$i]['first'] = $casesfnameArray->first;
                    $cases[$i]['last'] = $caseslnameArray->last;
                    $cases[$i]['caption'] = $casescaptionArray->caption1;
                }
                //}
                //}
                $i++;
            }
        }

        echo json_encode(array('data' => $cases));
    }

    public function AddSocialSecurity() {
        extract($_POST);
        $currentRelatedNo = $this->common_functions->getRecordById('casekey', 'related', array('caseno' => $caseno));
        $relatedKey = $this->case_model->getMaxRelationKey();

        array_push($related, $caseno);
        foreach ($related as $cases) {
            $all_related[] = $cases;
            $related1 = $this->common_functions->getRecordById('casekey', 'related', array('caseno' => $cases));
            if (!empty($related1->related)) {
                $all = $this->common_functions->getRecordById('casekey', 'caseno', array('related' => $related1->related));

                //echo $this->db->last_query();
                if (!empty($all)) {
                    foreach ($all as $rale) {
                        if ($all->caseno != '') {
                            $all_related[] = $all->caseno;
                        }
                    }
                }
            }
        }
        $response = array();
        //echo '<pre>'; print_r($all_related); exit;
        foreach ($all_related as $cases) {
            //echo $cases;
            $res = $this->common_functions->editRecord('casekey', array('related' => $relatedKey + 1), array('caseno' => $cases));
            if ($res >= 0) {
                $resp = 1;
            } else {
                
                echo json_encode(array('status' => 0, 'message' => 'Oops! Something went Wrong'));
                exit;
            }
        }
        if ($resp == 1) {
            $response = array('status' => 1, 'message' => 'Case successfully related to "Current Case"!');
        } else {
            echo json_encode(array('status' => 0, 'message' => 'Oops! Something went Wrong'));
            exit;
        }

        echo json_encode($response);
        exit;
    }

    public function createDoc() {
        extract($_POST);
        //echo '<pre>'; print_r($_POST);exit;
        $this->load->library('tbs/tbs_functions', null, 'tbsClass');
        $Logindetail = $this->session->userdata('user_data')['fname'] . " " . $this->session->userdata('user_data')['lname'];
        $this->caseData = array();
        $this->caseData['today'] = date('F d, Y');
        $this->caseData['enum'] = $enum;
        $this->caseData['Client_suffix'] = isset($Client_suffix) ? $Client_suffix : '';
        $this->caseData['my_middle'] = isset($my_middle) ? $my_middle : '';
        $this->caseData['my_email'] = isset($my_email) ? $my_email : '';
        $this->caseData['my_occupation'] = isset($my_occupation) ? $my_occupation : '';
        $this->caseData['my_title'] = isset($my_title) ? $my_title : '';
        $this->caseData['licenceNo'] = isset($licenceNo) ? $licenceNo : '';
        $this->caseData['Client_fname'] = isset($Client_fname) ? $Client_fname : '';
        $this->caseData['Client_lname'] = isset($Client_lname) ? $Client_lname : '';
        $this->caseData['depo_date'] = isset($depo_date) ? $depo_date : '';
        $this->caseData['depo_time'] = isset($depo_time) ? $depo_time : '';
        $this->caseData['fee_amt'] = isset($fee_amt) ? $fee_amt : '';
        $this->caseData['depo_time_hours'] = isset($depo_time_hours) ? $depo_time_hours : '';
        $this->caseData['depo_time_minutes'] = isset($depo_time_minutes) ? $depo_time_minutes : '';
        $this->caseData['attorney_fees'] = isset($attorney_fees) ? $attorney_fees : '';
        $this->caseData['trip_mileage'] = isset($trip_mileage) ? $trip_mileage : '';
        $this->caseData['cc_party'] = isset($cc_party) ? $cc_party : '';
        $this->caseData['fileName'] = $fileName;
		$this->caseData['firmname'] = $firmname;
        $this->caseData['fileNumber'] = $docNumber;
        $this->caseData['clientName'] = $clientName;
        $this->caseData['clientAddress1'] = $clientAddress;
        $this->caseData['clientAddress2'] = $clientAddress2;
        $this->caseData['logindetail'] = $Logindetail;
        $this->caseData['pl_title'] = $pl_title;
        $this->caseData['my_pl_title'] = $my_pl_title;
        $this->caseData['date_App'] = $date_App;
        $this->caseData['date_Tri'] = $date_Tri;
        $this->caseData['time_Tri'] = $time_Tri;
        $this->caseData['boardCity'] = $board_City;
        $this->caseData['dr_splt'] = $dr_splt;
        $this->caseData['e_type'] = $e_type;
        $this->caseData['e_day'] = $e_day;
        $this->caseData['e_date'] = $e_date;
        $this->caseData['e_time'] = $e_time;
        $this->caseData['judgeNM'] = isset($judgeNM) ? $judgeNM : '';
        $this->caseData['addinj'] = isset($addinj) ? $addinj : '';
        $this->caseData['pro_unit'] = isset($pro_unit) ? $pro_unit : '';
        $this->caseData['pro_doc_typ'] = isset($pro_doc_typ) ? $pro_doc_typ : '';
        $this->caseData['doc_title'] = isset($doc_title) ? $doc_title : '';
        $this->caseData['selected_ins_party'] = isset($selected_ins_party) ? $selected_ins_party : '';
        $this->caseData['selected_defatt_party'] = isset($selected_defatt_party) ? $selected_defatt_party : '';
        $this->caseData['selected_phy_party'] = isset($selected_phy_party) ? $selected_phy_party : '';
        $this->caseData['selected_emp_party'] = isset($selected_emp_party) ? $selected_emp_party : '';
        $this->caseData['attorney_fees'] = isset($attorney_fees) ? $attorney_fees : '';
        $this->caseData['trip_mileage'] = isset($trip_mileage) ? $trip_mileage : '';
        $this->caseData['daily_wage'] = isset($daily_wage) ? $daily_wage : '';
        $this->caseData['total_mileage'] = isset($total_mileage) ? $total_mileage : '';
        $this->caseData['total_mileage_amount'] = isset($total_mileage_amount) ? $total_mileage_amount : '';
        $this->caseData['travel_time'] = isset($travel_time) ? $travel_time : '';
        $this->caseData['depo_prep_time'] = isset($depo_prep_time) ? $depo_prep_time : '';
        $this->caseData['total_time'] = isset($total_time) ? $total_time : '';
        $this->caseData['total_request'] = isset($total_request) ? $total_request : '';
        $this->caseData['dr_speciality'] = isset($dr_speciality) ? $dr_speciality : '';
        $this->caseData['body_partexamined'] = isset($body_partexamined) ? $body_partexamined : '';
        
        if (!empty($party) && $party != '853') {
            $cond = array('cc.caseno' => $caseno, 'cc.cardcode' => $party);
        } else {
            $cond = array('cc.caseno' => $caseno, 'cc.orderno' => 1);
        }
        $result = $this->case_model->getPartiesDetais($cond);

        $this->caseData['result'] = $result[0];
        //echo '<pre>'; print_r($result);exit;
        $metaData = $this->common_functions->getRecordById('meta_data', 'data', array('staff' => $this->data['loginUser'], 'caseno' => $caseno));

        if (!empty($this->caseData['result']->atty_hand)) {
            $attorneyname = $this->case_model->getAttNm($this->caseData['result']->atty_hand);
            $attorney = explode(" ", $attorneyname);
            $atty_first = $attorney[0];
            $atty_last = $attorney[1];
            $atty_hand = $this->caseData['result']->atty_hand;
        } else {
            $attorneyname = '';
            $atty_hand = '';
            $atty_first = '';
            $atty_last = '';
        }

		if (!empty($this->caseData['result']->atty_resp)) {
            $attorneynameresp = $this->case_model->getAttNm($this->caseData['result']->atty_resp);
            $attorneyresp = explode(" ", $attorneynameresp);
            $atty_firstresp = $attorney[0];
            $atty_lastresp = $attorney[1];
            $atty_resp = $this->caseData['result']->atty_resp;
        } else {
            $attorneynameresp = '';
            $atty_resp = '';
            $atty_firstresp = '';
            $atty_lastresp = '';
        }
        $this->caseData['caseno'] = $caseno;
        $this->caseData['attorneyname'] = $attorneyname;
        $this->caseData['attorneyresname'] = $attorneynameresp;
        $this->caseData['atty_first'] = $atty_first;
        $this->caseData['atty_last'] = $atty_last;
        $this->caseData['atty_hand'] = $atty_hand;


        $this->caseData['lusername'] = $this->case_model->getAttNm($this->session->userdata('user_data')['initials']);

        //echo "<pre>"; print_R($this->caseData); exit;
        $records = json_decode($metaData->data, true);
        $records['system_data'] = $this->common_functions->getsystemData();
        $records['case_data'] = $this->caseData;
        if (!empty($party1)) {
            $records['party_location'] = $party1;
        }

        if (!empty($cc_party)) {
            $cc_party = explode(",", $cc_party);
        }
        $records['cc_party'] = $cc_party;
        if (!empty($lastParty)) {
            $parties = explode(",", $lastParty);
        }
        $records['lastParty'] = $parties;
        //echo '<pre>'; print_r($this->caseData);exit;

        if(isset($this->caseData['addinj']) && $this->caseData['addinj'] != "") {
            $this->caseData['addinj'] = explode(',', $this->caseData['addinj']);
            $SelectedInjuryid = $this->caseData['addinj'][0];
        } else {
            $FirstInjuryid = $this->case_model->getfirstinjuryid($this->caseData['caseno']);
            $SelectedInjuryid  = (isset($FirstInjuryid['0']['injury_id'])) ? $FirstInjuryid['0']['injury_id'] : "";
        }
        $SeectedInjuryDetails = $this->case_model->getinjurydetailsbyid($SelectedInjuryid);
        $records['SeectedInjuryDetails'] = $SeectedInjuryDetails;
        //echo '<pre>'; print_r($selectedtinsparty); exit;
        //echo 'ins party' . $selected_ins_party; exit;
        if(isset($selected_ins_party) && $selected_ins_party != "") {
            $selected_ins_party = $selected_ins_party;
            $selectedtinsparty = $this->case_model->getinsuranceparty($this->caseData['caseno'], $selected_ins_party);
            $records['selectedtinsparty'] = $selectedtinsparty;
        } else if(isset($selected_ins_party) && $selected_ins_party == 'N') {
            //$selectedtinsparty = $this->case_model->getdefinsuranceparty($this->caseData['caseno']);
            //$records['selectedtinsparty'] = $selectedtinsparty;
            $records['selectedtinsparty'] = [];
        }
        else{
            $selectedtinsparty = $this->case_model->getdefinsuranceparty($this->caseData['caseno']);
            $records['selectedtinsparty'] = $selectedtinsparty;
        }
        //echo '<pre>'; print_r($selectedtinsparty); exit;
        if(isset($selected_emp_party) && $selected_emp_party != "") {
            $selectedempparty = $this->caseData['selected_emp_party'];
            $selectedempparty = $this->case_model->getempparty($this->caseData['caseno'], $selectedempparty);
            $records['selectedempparty'] = $selectedempparty;
        } else if(isset($selected_emp_party) && $selected_emp_party == 'N') {
            //$selectedtinsparty = $this->case_model->getdefinsuranceparty($this->caseData['caseno']);
            //$records['selectedtinsparty'] = $selectedtinsparty;
            $records['selectedempparty'] = [];
        }else {
            $selectedempparty = $this->case_model->getdefempparty($this->caseData['caseno']);
            $records['selectedempparty'] = $selectedempparty;
        }
        
        if(isset($selected_defatt_party) && $selected_defatt_party != "") {
            $selecteddefatt = $this->caseData['selected_defatt_party'];
            $selecteddefattdATA = $this->case_model->getdefatt($this->caseData['caseno'], $selecteddefatt);
            $records['selecteddefatt'] = $selecteddefattdATA;
        } else if(isset($selected_defatt_party) && $selected_defatt_party == 'N') {
            //$selectedtinsparty = $this->case_model->getdefinsuranceparty($this->caseData['caseno']);
            //$records['selectedtinsparty'] = $selectedtinsparty;
            $records['selecteddefatt'] = [];
        }else {
            $selecteddefattdATA = $this->case_model->getdedefatt($this->caseData['caseno']);
            $records['selecteddefatt'] = $selecteddefattdATA;
        }
        
        if(isset($selected_phy_party) && $selected_phy_party != "") {
            $selectedphy = $this->caseData['selected_phy_party'];
            $selectedphyparty = $this->case_model->getdr($this->caseData['caseno'], $selectedphy);
            $records['selectedphy'] = $selectedphyparty;
        } else if(isset($selected_phy_party) && $selected_phy_party == 'N') {
            //$selectedtinsparty = $this->case_model->getdefinsuranceparty($this->caseData['caseno']);
            //$records['selectedtinsparty'] = $selectedtinsparty;
            $records['selectedphy'] = [];
        }else {
            $selectedphyparty = $this->case_model->getdefdr($this->caseData['caseno']);
            $records['selectedphy'] = $selectedphyparty;
        }
        
        $data = $this->tbsClass->setDocs($records);
        if (!empty($data)) {
            echo $caseno.'/'.$data;
        } else {
            echo "false";
        }
    }

    public function createEnvelope(){
        extract($_POST);

        if(count($parties) > 0){
            $partiesDetails = $this->case_model->getPartiesDetails($parties); 

            if($partiesDetails){
                $i = 0;
                foreach($partiesDetails as $k=>$v){
                    $address1 = $v->address1;
                    $address2 = $v->address2;
                    $city = $v->city;
                    $state = $v->state;
                    $zip = $v->zip;
                    $salutation = $v->salutation;
                    $first = $v->first;
                    $last = $v->last;
                    $suffix = $v->suffix;
                    $firm = $v->firm;

                    if($address1 != '' || $address2 != '' || $city != '' || $state != '' || $zip != '' || $saluatation != '' || $first != '' || $last != '' || $suffix != '' || $firm != ''){
                        $res[$i]['address1'] = $address1;
                        $res[$i]['address2'] = $address2;
                        $res[$i]['city'] = $city;
                        $res[$i]['state'] = $state;
                        $res[$i]['zip'] = $zip;
                        $res[$i]['salutation'] = $salutation;
                        $res[$i]['first'] = $first;
                        $res[$i]['last'] = $last;
                        $res[$i]['suffix'] = $suffix;
                        $res[$i]['firm'] = $firm;
                        if(!empty(trim( $res[$i]['first'])) || !empty(trim($res[$i]['last'])) ){
                            $res[$i]['full_address'] .= trim($res[$i]['salutation'] . ' ' . $res[$i]['first']. ' ' .$res[$i]['last']. ', ' .  $res[$i]['suffix']);
                        }
                        if(!empty(trim($res[$i]['firm']))){
                            if(!empty( trim($res[$i]['first'])) || !empty(trim( $res[$i]['last'] ))){
                                $res[$i]['full_address'] .= "\r\n".$res[$i]['firm'];
                            }else{
                                $res[$i]['full_address'].= $res[$i]['firm'];
                            }
                        }
                        else{
                            $res[$i]['full_address'] .= "\r\n";
                        }
                        if(!empty(trim( $res[$i]['address1']))){
                            if(!empty( $res[$i]['firm'])){
                                $res[$i]['full_address'] .= "\r\n".$res[$i]['address1'];
                            }else{
                                $res[$i]['full_address'] .=  $res[$i]['address1'];
                            }
                        }
                        if(!empty(trim( $res[$i]['address2']))){
                            if(!empty(trim($res[$i]['address1']))){
                                $res[$i]['full_address'] .= "\r\n".$res[$i]['address2'];
                            }else{
                                $res[$i]['full_address'] .=  $res[$i]['address2'];
                            }
                        }
                        if(!empty( trim($res[$i]['city'])) || !empty( trim($res[$i]['state'])) || !empty( trim($res[$i]['zip']))){
                            if(!empty(trim($res[$i]['address2']))){
                                $res[$i]['full_address'] .= "\r\n".$res[$i]['city']. ', ' . $res[$i]['state']. ' ' .  $res[$i]['zip'] ;
                            }else{
                                $res[$i]['full_address'] .= "\r\n".$res[$i]['city']. ', ' . $res[$i]['state'] . ' ' . $res[$i]['zip'];
                            }
                        }
                         $i++;
                    }
                }  
                $res['caseNo'] = $_POST['caseNo'];
                $this->load->library('tbs/tbs_functions', null, 'tbsClass');
                $envelope_file_path = $this->tbsClass->setEnvelope($res);
                if (!empty($envelope_file_path)) {
                    echo $envelope_file_path;
                } else {
                    echo "false";
                }
            }
            
        }
        // echo 2; exit;
        //  echo '<pre>'; print_r($envelope_file_path);exit;
    }

    public function getcategoryeventlist() {
        extract($_POST);
        if ($status == 0) {
            $case_activity = $this->case_model->getCaseActivityByEvent($case_id, $category_list);
        } else {
            $case_activity = $this->case_model->getEventActivityList($case_id);
        }
        $finalData = array();
        foreach ($case_activity as $data) {
            $tmpArr = array(
                "category" => $data->category,
                "event" => $data->event,
            );
            array_push($finalData, $tmpArr);
        }

        echo json_encode($finalData);
    }

    public function getcasepartyDetails() {
        extract($_POST);
        $result = $this->case_model->getCaseFullDetails($caseno);
        echo json_encode($result);
    }

    public function printInjuryDetails() {
        extract($_POST);
        $con = array('caseno' => $caseno, 'injury_id' => $injuryId);
        $attorney = $this->case_model->getcaseAttorney($caseno);
        $attorneyname = $this->case_model->getAttNm($attorney[0]->atty_hand);
        $this->data['attorney'] = $attorneyname;
        if ($form_data_type == 'data-only') {

            $this->data['injury'] = $this->case_model->getInjuryData($con);
            $this->data['form_data_type'] = 'data-only';
            $this->load->view('cases/print_injury_details', $this->data);
        } else if ($form_data_type == 'form-only') {
            $this->data['form_data_type'] = 'form-only';
            $this->load->view('cases/print_injury_details', $this->data);
        } else {
            $this->data['form_data_type'] = 'form-data';
            $this->data['injury'] = '';
            $this->data['injury'] = $this->case_model->getInjuryData($con);
            $this->load->view('cases/print_injury_details', $this->data);
        }
    }

    public function UploadPhoto() {
        extract($_POST);
        //print_r($_FILES); echo $caseno; exit;
        if (is_array($_FILES)) {
            if (!file_exists(DOCUMENTPATH . 'casePhotoVideo/' . $caseno . '/photo')) {

                mkdir(DOCUMENTPATH . 'casePhotoVideo/' . $caseno . '/photo', 0777, true);
            }
            $f_name = array();
            foreach ($_FILES as $name => $value) {
                $temp = explode(".", $value['name']);
                $newfilename = $temp[0] . '_' . round(microtime(true)) . '.' . end($temp);
                $f_name[] = $newfilename;
                // echo $newfilename;
                move_uploaded_file($value['tmp_name'], DOCUMENTPATH . 'casePhotoVideo' . DIRECTORY_SEPARATOR . $caseno . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . $newfilename);
            }
            $msg = (count($_FILES) > 1) ? 'Images are successfully uploaded!' : 'Image Uploaded  Successfully !!!';
            echo json_encode(array('status' => 1, 'message' => $msg, 'file' => $f_name));
            exit;
        }
    }

    public function Uploadvideo() {
        extract($_POST);
        //print_r($_FILES); echo $caseno; exit;
        if (is_array($_FILES)) {
            if (!file_exists(DOCUMENTPATH . 'casePhotoVideo/' . $caseno . '/video')) {

                mkdir(DOCUMENTPATH . 'casePhotoVideo/' . $caseno . '/video', 0777, true);
            }
            $uploaded_video = array();
            foreach ($_FILES as $name => $value) {
                $temp = explode(".", $value['name']);

                $newfilename = $temp[0] . '_' . round(microtime(true)) . '.' . end($temp);
                // echo $newfilename;
                if (move_uploaded_file($value['tmp_name'], DOCUMENTPATH . 'casePhotoVideo' . DIRECTORY_SEPARATOR . $caseno . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $newfilename)) {
                    $file_to_convert = DOCUMENTPATH . 'casePhotoVideo' . DIRECTORY_SEPARATOR . $caseno . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $newfilename;
                    $file_info = pathinfo($file_to_convert);
                    $file_con_name = $file_info['filename'] . '.mp4';
                    if ($file_info['extension'] != 'mp4') {
                        exec("ffmpeg -i " . $file_to_convert . " -c:v libx264 " . DOCUMENTPATH . 'casePhotoVideo' . DIRECTORY_SEPARATOR . $caseno . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $file_con_name . " ");
                        unlink($file_to_convert);
                    }
                    $uploaded_video[] = $file_con_name;
                }
            }
            $msg = (count($_FILES) > 1) ? 'Videos are successfully uploaded!' : 'Video Uploaded  Successfully !!!';
            echo json_encode(array('status' => 1, 'message' => $msg, 'file' => $uploaded_video));
            exit;
        }
        // return $return;
    }

    public function injurydoc() {
        $count = 0;
        $case_no = $_POST['caseno'];
        $injury_id = $_POST['injury_id'];
        $files = glob(DOCUMENTPATH . 'clients/' . $case_no . '/I*'.$injury_id.'[_*.|.]*');
        $html = '';

        if (count($files) > 0) {
            foreach ($files as $file) {
                $fileArr = explode("/", $file);
                $filename = end($fileArr);
                $injuryfilename = explode('-', $filename);
                $injuryfile = isset($injuryfilename[1]) ? $injuryfilename[1] : substr($injuryfilename[0], 1);
                $findinjuryno = explode('.', $injuryfile);
                $explode_injuryno = explode('_', $findinjuryno[0]);
                $injuryno = $explode_injuryno[0];
                $checkfilename = substr($filename, 0, 1);

                if ($checkfilename == 'I' && $injuryno == $injury_id) {

                    $html .= '<div class="btn-group">';
                    $html .= '<a class="btn btn-outline btn-success btn-sm case_video" target="_blank" href="' . base_url() . 'assets/clients/' . $case_no . '/' . $filename . '">' . $filename . '</a>';
                    $html .= '<button class="btn btn-outline btn-danger btn-sm" id="del_injDoc" data-id="' . DOCUMENTPATH . 'clients/' . $case_no . '/' . $filename . '" data-injuryid="' . $injury_id . '"><i class="fa fa-close"></i></button>';
                    $html .= '</div>';
                }
            }
        }

        echo $html;
    }

    public function UploadinjDocs() {
        extract($_POST);
        //print_r($_POST); echo $injuryno; exit;
        if (is_array($_FILES)) {
            /* if (!file_exists(INJDOCUMENTPATH .'injDocs/'.$caseno)) {

              mkdir(INJDOCUMENTPATH .'injDocs/'. $caseno, 0777, true);
              } */

            if (!file_exists(DOCUMENTPATH . 'clients/' . $caseno)) {

                mkdir(DOCUMENTPATH . 'clients/' . $caseno, 0777, true);
            }
            $uploaded_video = array();

            $record['caseno'] = $caseno;
            $record['initials0'] = $this->session->userdata('user_data')['initials'];
            $record['initials'] = $this->session->userdata('user_data')['initials'];
            $record['date'] = date('Y/m/d H:i:s');
            $record['event'] = 'Injury Document';

            foreach ($_FILES as $name => $value) {
                $actno = $this->common_functions->last_caseact_record(array('caseno' => $caseno));
                $nwactno = $actno + 1;
                $record['actno'] = $nwactno + 1;
                $record['category'] = 24;
                $record['mainnotes'] = 0;

                $temp = explode(".", $value['name']);
                //echo 'new act no => ' . $nwactno;
                //$newfilename =$temp[0].'_'.round(microtime(true)) . '.' . end($temp);
                $newfilename = 'I' . $nwactno . "-" . $injuryno . "." . end($temp);
                $record['filename'] = $newfilename;

                //echo 'new filename => ' . $newfilename; exit;
                $f_name[] = $newfilename;
                //echo '<pre>'; print_r($record); exit;
                move_uploaded_file($value['tmp_name'], DOCUMENTPATH . 'clients' . DIRECTORY_SEPARATOR . $caseno . DIRECTORY_SEPARATOR . $newfilename);
                $uploaded_video[] = $newfilename;
                $this->common_functions->insertCaseActRecord($record);
            }

            $msg = (count($_FILES) > 1) ? 'File is successfully uploaded!' : 'File Uploaded  Successfully !!!';
            echo json_encode(array('status' => 1, 'message' => $msg, 'file' => $uploaded_video));
            //redirect('injury/'.$caseno);
            exit;
        }
        // return $return;
    }

    public function updateorderno() {
        extract($_POST);
        foreach ($position as $value) {
            $where = array('caseno' => $caseno, 'casecard_no' => $value[1], 'cardcode' => $value[2]);
            $lastRecord = $this->common_functions->editRecord('casecard', array('orderno' => $value[0]), $where);
        }
        echo $lastRecord;
        exit;
    }

    public function updateinjuryorderno() {
        extract($_POST);
        foreach ($position as $value) {
            $where = array('caseno' => $caseno, 'injury_id' => $value[1]);
            $lastRecord = $this->common_functions->editRecord('injury', array('orderno' => $value[0]), $where);
        }
        echo $lastRecord;
        exit;
    }

    public function getCasrcardDetails() {
        $cardCode = $_POST['cardCode'];
        $orCondition = '(cd.cardcode = "' . $cardCode . '")';
        $results = $this->case_model->getCaseListing2($orCondition);
        $html = '<div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Case Number</th>
                                    <th>Name</th>
                                    <th>AttyR</th>
                                    <th>AttyH</th>
                                    <th>Case Type</th>
                                    <th>Status</th>
                                    <!--<th>Card Type</th>-->
                                </tr>
                            </thead>';
        foreach ($results as $result => $value) {
            $html .= '
                                <tr>
                                  <td><a target="_blank" href="' . base_url() . 'cases/case_details/' . $value->caseno . '">' . $value->caseno . '</a></td>
                                  <td>' . $value->first . ' ' . $value->last . '</td>
                                  <td>' . $value->atty_resp . '</td>
                                  <td>' . $value->atty_hand . '</td>
                                  <td>' . $value->type . '</td>
                                  <td>' . $value->casestat . '</td>
                                </tr>';
        }
        $html .= '<tbody></tbody>
                        </table>
                    </div>';

        echo $html;
    }

    public function delete_video_photo() {
        $vp = $this->input->post('link');
        $data = array();
        if ($vp != '' && file_exists($vp)) {
            if (unlink($vp)) {
                $data['status'] = 1;
                die(json_encode($data));
            } else {
                $data['status'] = 0;
                die(json_encode($data));
            }
        } else {
            $data['status'] = 0;
            die(json_encode($data));
        }
    }

    public function delete_injury_doc() {
        $vp = $this->input->post('link');
        $data = array();
        if ($vp != '' && file_exists($vp)) {
            if (unlink($vp)) {
                $data['status'] = 1;
                die(json_encode($data));
            } else {
                $data['status'] = 0;
                die(json_encode($data));
            }
        } else {
            $data['status'] = 0;
            die(json_encode($data));
        }
    }

//    public function editCasecaption()
    //    {
    //        $casecaption = $this->case_model->editCasecaption($this->input->post());
    //         if ($casecaption >= 0) {
    //            $response = array('status' => 1, 'message' => 'Case Caption  Edited Successfully !!!');
    //        } else {
    //            $response = array('status' => 0, 'message' => 'Fail to Edited Case Caption!!!');
    //        }
    //        echo json_encode($response);
    //
    //    }

    public function download() {

        $file_name = $this->uri->segment(3);
        $caseNo = $this->uri->segment(4);
        $file_url = base_url() . 'assets/injDocs/' . $caseNo . '/' . $file_name;
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $file_name . "\"");
        readfile($file_url);
        exit;
    }

    public function autosearch() {
        $CaseId = $this->uri->segment(3);
        if ($CaseId == 'undefined') {
            $CaseId = 0;
        } //echo $CaseId;exit;
        $term = $_REQUEST['term'];
        $response = array();
        $cases = array();
        $casesArray = $this->common_functions->getAllRecord('case', 'caseno', 'caseno != ' . $CaseId . ' AND caseno LIKE "%' . $term . '%"');
        //echo $this->db->last_query(); exit;
        foreach ($casesArray as $key => $val) {
            //$cases[$key]['caseno'] =$val->caseno;
            $data[] = $val->caseno;
        }
        echo json_encode($data);
    }

    public function printpde() {
        $caseno = $this->input->post('caseno');
        //print_r($post_data);
        require_once FCPATH . 'application/third_party/mpdf/vendor/autoload.php';

        $mpdf = new \Mpdf\Mpdf();
        //$this->load->library('pdf');
        //$pdf = $this->pdf->load();
        //$dt = '<div style="height:20px; text-align:center; clear:both; padding:10px;">Group Analysis -'.date('m-d-Y').'</div>';
        if (!is_dir(FCPATH . 'application/third_party/mpdf/tmp')) {
            mkdir(FCPATH . 'application/third_party/mpdf/tmp', 0777);
        }
        if (!is_dir(FCPATH . 'assets/printCaseAct')) {
            mkdir(FCPATH . 'assets/printCaseAct', 0777);
            // $pdfFilePath1 = "CaseAct-" . time() . "-Report.pdf";
        }
        $pdfFilePath = FCPATH . 'assets/printCaseAct/CaseAct-' . time() . '-Report.pdf';
        $pdfFilePath1 = "CaseAct-" . time() . "-Report.pdf";

        $post_data = '<div><h2 style="text-align: center;"><u>Case Activity Individual Note</u></h2></div>
                      <table width="100%" style="border-collapse:collapse;">
                      <tr>
                        <th align="left">Unattached Documents</th>
                        <th width="25%" align="left">Page</th>
                      </tr>
                      <tr>
                        <td align="left">Case Number:' . $caseno . '</td>
                        <td width="25%" align="left">Today' . date('m/d/Y') . '</td>
                      </tr>
                      <tr>
                        <td style="border-bottom: 1px solid #000; padding-bottom:20px;" align="left">Your File Number: Unattached
                        <td style="border-bottom: 1px solid #000; padding-bottom:20px;" width="25%" align="left">AttyR/H</td>
                      </tr>
                      </table>
                      <table width="100%" style="border-collapse:collapse;">
                      <tr>
                        <th align="left" style="border-bottom: 1px solid #000; padding-bottom:10px; padding-top:10px;">Event no</th>
                        <th align="left" style="border-bottom: 1px solid #000; padding-bottom:10px; padding-top:10px;">Initials</th>
                        <th align="left" style="border-bottom: 1px solid #000; padding-bottom:10px; padding-top:10px;">Date</th>
                        <th align="left" style="border-bottom: 1px solid #000; padding-bottom:10px; padding-top:10px;">Time</th>
                        <th align="left" style="border-bottom: 1px solid #000; padding-bottom:10px; padding-top:10px;">Original Initials</th>
                      </tr>
                      <tr>
                        <td align="left" style="padding-top:10px;">563</td>
                        <td align="left" style="padding-top:10px;">TCC</td>
                        <td align="left" style="padding-top:10px;">' . date('m/d/Y') . '</td>
                        <td align="left" style="padding-top:10px;">' . date('H:m:s') . '</td>
                        <td align="left" style="padding-top:10px;">tcc</td>
                      </tr>
                      </table>
            ';
        // $pdf->SetHTMLHeader($dt,'',true);
        //  $stylesheet = file_get_contents(APPPATH . '../assets/css/analysis_pdf.css'); // external css
        //$mpdf->SetHTMLFooter($footer);
        /* $pdf->AddPage('', // L - landscape, P - portrait
          '', '', '', '',
          5, // margin_left
          5, // margin right
          5, // margin top
          5, // margin bottom
          0, // margin header
          5); */
        //  $pdf->WriteHTML($stylesheet, 1);

        $mpdf->WriteHTML($post_data, 2);
        $mpdf->Output($pdfFilePath, "F");
        echo $pdfFilePath1;
    }

    public function getRedAlertActivity() {
        $caseno = $_POST['caseno'];
        $Condition = '(caseno = "' . $caseno . '" AND redalert = 1 AND event != "")';
        $results = $this->case_model->getCaseRActivity($Condition);
        //echo '<pre>'; print_r(); exit;
        $html = '';
        if (!empty($results)) {
            //$html .= '<span style="background-color:#FF0000"><font color="#FFFFFF"><marquee>';
            //$string = [];
            $html .= '<ul class="list-group">';
            foreach ($results as $result => $value) {
                if (count($value->event) > 50) {
                    $html .= '<li class="list-group-item">' . substr($value->event, 0, 50) . '...' . '</li>';
                } else {
                    $html .= '<li class="list-group-item">' . $value->event . '</li>';
                }
            }
            $html .= '</ul>';
            // $newstrng = implode("",$string);
            // $html .= $newstrng;
            //$html .= '</marquee></font></span>';
            echo json_encode($html);
        } else {
            echo $html;
        }
    }

    public function getCaseActivity() {
        extract($_POST);
//            print_r($_POST);exit;
        //        $filterArray = array();
        //        $orCondition = '';
        //        $singleFilter = '';
        //
        //        if ($columns != "" ){
        //            foreach($columns as $key=>$val){
        //                if($val['search']['value'] != ''){
        //                    $filterData = json_decode($val['search']['value'],true);
        //           //print_r( $filterData);exit;
        //                    if(key($filterData) == 'category' && $filterData[key($filterData)] != ''){
        //                      //  $orCondition = 'category ="'+$filterData['category'];
        //                        $orCondition =  array('category' => $filterData['category']);
        //                    }
        //                    }
        //
        //            }
        //          }// print_r($orCondition);exit;
        //        if($search['value'] != ''){
        //            $singleFilter = '(date LIKE "%'.date('Y-m-d',strtotime($search['value'])).'%" OR event LIKE "%'.$search['value'].'%" OR initials0 LIKE "%'.$search['value'].'%" OR initials LIKE "%'.$search['value'].'%" )';
        //        }
        //
        //        if(isset($taskBy)) {
        //          $filterArray[$taskBy] = $this->session->userdata('user_data')['username'];
        //        }
        //
        //        if(isset($caseno)) { $filterArray['caseno'] = $caseno; }

        $data = $this->case_model->getCaseActivity($caseno, $category, $chackvalue); //$start,$length,$filterArray,$order,$orCondition,$singleFilter);
        $finalData = array();
        $fileArr = '';
        $link = '';

        $colorCode = json_decode(color_codes, true);
        $html = '';
        $viewmail = '';
        foreach ($data as $key => $val) {
            $color = ($val->color != 0) ? $colorCode[$val->color][2] : '';
            $textColor = ($val->color != 0) ? $colorCode[$val->color][1] : '';
            $style = 'background-color:' . $color . '; color:' . $textColor;

            //if (($val->category == 26 || $val->category == 27 || $val->category == 1) && $val->email_con_id != null) {
            if ($val->email_con_id != null) {
                $viewmail = '<a class="btn btn-xs btn-info marginClass activity_email" data-actid=' . $val->email_con_id . ' href = "javascript:;"><i class = "icon fa fa-envelope"></i></a>';
            }
            $files_main = glob(DOCUMENTPATH . 'clients/' . $caseno . "/F" . $val->actno . ".*");
            $files_sub = glob(DOCUMENTPATH . 'clients/' . $caseno . "/F" . $val->actno . "_*");
            $files = array_merge($files_main, $files_sub);
            if (!empty($files) && count($files) == 1) {
                $fileArr = explode("/", $files[0]);
                $ext = explode(".",$fileArr[7]);
                //$link = base_url() . 'assets/clients/' . $caseno . '/' . $fileArr[7];
                //$document = '<a  class="btn btn-xs btn-primary marginClass view_attach"  download=' . $fileArr[7] . ' data-filename="one"  data-actid=' . $val->actno . ' href=' . $link . ' title="View"><i class = "icon fa fa-eye"></i> View</a>';
                if($ext[1] == 'docx'){
                if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                    $document = '<a class="btn btn-xs btn-primary marginClass view_attach" data-actid="' . $val->actno . '" target="_blank" href = "' . base_url() . 'document/doceditor.php?fileID='.$fileArr[7].'&user='.$this->session->userdata('user_data')['initials'].'&filePath='.$caseNo.'/'.$fileArr[7].'" title="View"><i class = "icon fa fa-eye"></i></a>';}
                else{
                    $document = '<a class="btn btn-xs btn-primary marginClass view_attach" data-actid="' . $val->actno . '" href = "' . base_url() . 'assets/clients/' . $caseNo . '/' . $fileArr[7] . '" title="Download"><i class = "icon fa fa-download"></i></a><a class="btn btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $val->actno . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $caseNo . '/' . $fileArr[7] . '" title="View"><i class = "icon fa fa-eye"></i></a>';
                }
                }
            } else if (!empty($files) && count($files) > 1) {
                $document = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' . $val->actno . ' href="javascript:void(0)" data-toggle="modal" title="View All"><i class = "icon fa fa-eye"></i> View All</a>';
            } else {
                $document = '';
            }

            if ($val->access == 3) {
                if ($val->initials0 == $this->session->userdata('user_data')['username']) {

                    $html .= '
                               <tr class="edit_caseAct"id="caseAct' . $val->actno . '" style="' . $style . '">
                                <td>' . date("m-d-Y", strtotime($val->date)) . '</td>
                                <td>' . date("H:m a", strtotime($val->date)) . '</td>
                                <td class="caseact_event">' . $val->event . '</td>
                                <td>' . $val->initials0 . '</td>
                                <td>' . $val->initials . '</td>

                                <td>' . $document . '</td>
                                <td><a class="btn btn-xs btn-info marginClass activity_edit" data-actid=' . $val->actno . ' href = "javascript:;"><i class = "icon fa fa-paste"></i> Edit</a><a class="btn btn-xs btn-danger marginClass activity_delete" data-actid=' . $val->actno . ' href = "javascript:;"><i class = "icon fa fa-times"></i> Delete</a>' . $viewmail . '</td>
                                </tr>';
                }
            } else {
                $html .= '
                               <tr class="edit_caseAct"id="caseAct' . $val->actno . '" style="' . $style . '">
                                <td>' . date("m-d-Y", strtotime($val->date)) . '</td>
                                <td>' . date("H:m a", strtotime($val->date)) . '</td>
                                <td class="caseact_event">' . $val->event . '</td>
                                <td>' . $val->initials0 . '</td>
                                <td>' . $val->initials . '</td>

                                <td>' . $document . '</td>
                                <td><a class="btn btn-xs btn-info marginClass activity_edit" data-actid=' . $val->actno . ' href = "javascript:;"><i class = "icon fa fa-paste"></i> Edit</a><a class="btn btn-xs btn-danger marginClass activity_delete" data-actid=' . $val->actno . ' href = "javascript:;"><i class = "icon fa fa-times"></i> Delete</a>' . $viewmail . '</td>
                                </tr>';
            }
        }

        echo $html;
    }

    public function viewactivityemail() {
        $caseno = $this->input->post('caseno');
        $filename = $this->input->post('filename');
        /*$email_con_id = $this->input->post('email_con_id');*/
        //$email_activity = $this->email_model->getviewactivityemail($caseno,$email_con_id);
        /*$email_activity = $this->email_model->get_email_conversation($email_con_id, '', '', $caseno);*/
        $email_activity = $this->email_model->get_email_conversation_by_filename($filename, '', '', $caseno);
        $data = '';
        /*$cspan = '8';
        $data .= "<tr class='read rclose'><td colspan='" . $cspan . "'>";
        $data .= "<table class='table'style='border:1px solid #337ab7; width:100%;'>";*/
        foreach ($email_activity as $email) {
            $ruclass = ($email->readyet == 'Y') ? 'read' : 'unread';
            $datafilename = $email->filename;
            $email_con_id = $email->email_con_id;
            $iore = $email->iore;
            $subject = $email->subject;
            $date = date('M d, Y', strtotime($email->datesent));
            $whofromname = (isset($email->whofrom_name) ? $email->whofrom_name : ucfirst(strstr($email->whofrom, '@', true)));
            /*$data .= "<tr>";
            $data .= '<td style="width: 20%;" class="mail-ontact sorting_1">';*/
            $data .= "<a style='display: none;' class='read-email' href='javascript:;' data-filename='$datafilename' email_con_id='$email_con_id' ethread='' iore='".$iore."'>$whofromname</a></td>";
            /*$data .= "<td style='width: 50%;' class='mail-subject'><a class='read-email' href='javascript:;' data-filename='$datafilename' email_con_id='' ethread='' iore='I'>$subject <span class='pull-right'></span></a></td>";*/
            $data .= "<a style='display: none;' class='read-email' href='javascript:;' data-filename='$datafilename' email_con_id='' ethread='' iore='".$iore."'>$subject <span class='pull-right'></span></a>";
            /*$data .= "<td style='width: 20%;' class='text-right mail-date'>$date</td></tr>";*/
        }
        /*$data .= "</table></td></tr>";
        $data .= "</div></td></tr>";*/
        //echo "<pre>"; print_r($email_activity); exit;
        die($data);
    }

    public function birthdateNotification($case_id, $birth_date1, $before, $after) {
        $today = date('d-m-Y');
        $birth_date = date('d-m-Y', strtotime($birth_date1));
        $birth_date = explode('-', $birth_date);
        //print_r($birth_date);
        $birthdate = $birth_date[0] . '-' . $birth_date[1] . '-' . date("Y");

        if (strtotime($birthdate) == strtotime($today)) {
            $result = array('title' => "Happy Birthday !", "type" => "Today is " . date('l', strtotime($today)) . ',' . date('m/d/Y'), "type1" => "The client`s birthday is on  " . date('l', strtotime($birthdate)) . ',' . date('m/d/Y', strtotime($birthdate)));
            return json_encode($result);
        } else {
            $before1 = date('d-m-Y', strtotime('-' . $before . 'days', strtotime($birthdate)));
            $after1 = date('d-m-Y', strtotime('+' . $after . 'days', strtotime($birthdate)));

            $date1_ts = strtotime($today);
            $date2_ts = strtotime($birthdate);
            $diff = $date2_ts - $date1_ts;
            $interval1 = round($diff / 86400);

            $date1_tsafter = strtotime($birthdate);
            $date2_tsafter = strtotime($today);
            $diff1 = $date2_tsafter - $date1_tsafter;
            $intervalafter = round($diff1 / 86400);
            if ((($interval1 > 0) && ($before > 0)) && $interval1 <= $before) {
//
                //           $date1_ts = strtotime($today);
                //       $date2_ts = strtotime($birthdate);
                //       $diff = $date2_ts - $date1_ts;
                //           $interval3=round($diff / 86400);
                //         //echo  $interval3."hj ".$before1."hjk".$today; exit;
                $result = array('title' => "Happy Birthday! in " . $interval1 . " day", "type" => "Today is " . date('l', strtotime($today)) . ',' . date('m/d/Y'), "type1" => "The client`s birthday is on " . date('l', strtotime($birthdate)) . ',' . date('m/d/Y', strtotime($birthdate)));
                return json_encode($result);
            } else if ((($intervalafter > 0) && ($after > 0)) && $after >= $intervalafter) {
//         $date2_ts = strtotime($today);
                //       $date1_ts = strtotime($birthdate);
                //       $diff = $date2_ts - $date1_ts;
                //           $interval=round($diff / 86400);
                $result = array('title' => "Happy Belated Birthday  " . $intervalafter . " days ago", "type" => "Today is  " . date('l') . "," . date('m/d/Y') . ".", "type1" => "The client birthday was  on " . date('l', strtotime($birthdate)) . ',' . date('m/d/Y', strtotime($birthdate)));
                return json_encode($result);
            } else {

            }
        }
    }

    public function protectCase() {
        //echo '<pre>'; print_r($_POST);  exit;
        extract($_POST);

        $where = array('caseno' => $pro_case_no);
        if (!empty($propassword)) {
            $protected = 1;
        } else {
            $protected = 0;
        }
        $lastRecord = $this->common_functions->editRecord('case', array('protected' => $protected, 'password' => $propassword), $where);

        //$resp = array('status' => 0, 'caption' => '');
        if (count($lastRecord) > 0) {
            if ($protected == 1) {
                $resp = array('status' => 1, 'message' => 'Case is successfully protected! ');
            } else if ($protected == 0) {
                $resp = array('status' => 1, 'message' => 'Case protection is successfully reset.');
            }
        } else {
            $resp = array('status' => 0, 'message' => 'Oops! There is some issue in case protection.Please try again.');
        }

        echo json_encode($resp);
    }

    public function getProtectPass() {
        extract($_POST);
        $caseid = $id;
        $pass = $pass;

        $record = $this->case_model->getCasePass($id, $pass);
        //print_r($record); exit;
        echo $record->cnt;
    }

    public function viewcaseactdoc() {
        extract($_POST);
        $filename = $filename;
        $caseno = $caseno;
        $filePath = DOCUMENTPATH . 'clients/' . $caseno . "/" . $filename;

        header('Content-disposition: inline');
        header('Content-type: application/msword'); // not sure if this is the correct MIME type
        readfile($filePath);
        exit;
    }

    public function getAllInjuries() {
        extract($_POST);
        $caseId = $caseId;
        $finalData = array();
        $filterArray = array();
        //$i = 1;
        $columns = array(
            0 => 'adj1c',
            1 => 'i_claimno',
            2 => 'eamsno'
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') {
            $dt = $this->validateDate($search, $format = 'm-d-Y');
            //echo $dt; exit;
            $singleFilter = '(adj1c LIKE "%' . $search . '%" OR i_claimno LIKE "%' . $search . '%" OR eamsno LIKE "%' . $search . '%" OR case_no LIKE "%' . $search . '%")';
        }
        $rec = $this->case_model->getAjaxInjuryListing($caseId, $start, $length, $filterArray, $order, $dir, $singleFilter);
        $finalData = array();
        //$i = $start + 1;
        foreach ($rec as $data) {

            $date = ($data->adj1c != '' && $data->adj1c != '1899-12-30 00:00:00' && $data->adj1c != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($data->adj1c)) : '';
            $links = '<a href="#" class="btn btn-circle-action btn-circle btn-info injury_view" id="' . $data->injury_id . '" data-id="' . $data->injury_id . '" title="View"><i class="icon fa fa-eye"></i></a> <a href="#" class="btn btn-danger btn-circle-action btn-circle injury_delete" data-injuryid="' . $data->injury_id . '"  id="' . $data->injury_id . '" title="Delete"><i class="fa fa-trash"></i></a>';
            if ($data->eamsno) {
                $eamsno = $data->eamsno;
            } elseif ($data->case_no) {
                $eamsno = $data->case_no;
            } else {
                $eamsno = '';
            }
            $tmpArr = array(
                //$i,
                $data->adj1c,
                $data->i_claimno,
                $eamsno,
                $links,
                $data->injury_id,
                $data->app_status,
            );

            array_push($finalData, $tmpArr);
            $i++;
        }

        $totalData = $this->case_model->get_all_injury_count($caseId, $singleFilter);
        $listingData = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $finalData,
        );
        //echo '<pre>'; print_r($finalData); exit;
        /* $listingData = array(
          'data' => $finalData
          ); */

        echo json_encode($listingData);
    }

    public function getAllInjuriesview2() {
        extract($_POST);
        $caseId = $caseId;
        $finalData = array();
        $filterArray = array();
        //$i = 1;
        $columns = array(
            /*0 => 'injury_id',*/
            0 => 'adj1c',
            1 => 'i_claimno',
            2 => 'status2',
            3 => 'eamsno',
            4 => 'e_name',
            5 => 'i_name',
            6 => 'd1_firm', // NEED TO CHECK SORTING ON THIS
            7 => 'i_adjfst', // NEED TO CHECK SORTING ON THIS
            /*9 => 'd2_firm', // NEED TO CHECK SORTING ON THIS*/
            8 => 'pob1', // NEED TO CHECK SORTING ON THIS
            9 => 'app_status',
            /*12 => 'i2_name',
            13 => 'i2_adjfst',
            14 => 'i2_claimno',*/
            10 => 'case_no',
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') {
            $dt = $this->validateDate($search, $format = 'm-d-Y');
            //echo $dt; exit;
            $singleFilter = '(adj1c LIKE "%' . $search . '%" OR i_claimno LIKE "%' . $search . '%" OR eamsno LIKE "%' . $search . '%")';
        }
        $rec = $this->case_model->getAjaxInjuryListing($caseId, $start, $length, $filterArray, $order, $dir, $singleFilter);
        $finalData = array();
        $i = $start + 1;
        foreach ($rec as $data) {

            $date = ($data->adj1c != '' && $data->adj1c != '1899-12-30 00:00:00' && $data->adj1c != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($data->adj1c)) : '';
            $links = '<a href="#" title="Edit" class="injury_edit btn btn-info btn-circle-action btn-circle" id="' . $data->injury_id . '"><i class="fa fa-edit"></i></a> <a href="#" title="Copy" class="btn btn-info btn-circle-action btn-circle" id="injuryclone"  data-id="' . $data->injury_id . '"><i class="fa fa-files-o"></i></a> <a href="#" class="btn btn-danger btn-circle-action btn-circle injury_delete" title="Delete"  id="' . $data->injury_id . '" data-injuryid="' . $data->injury_id . '"><i class="fa fa-trash"></i></a>';

            $bodyParts = json_decode(bodyParts);
            $pob = '';
            /*foreach ($bodyParts as $key => $value) {

                if (!empty($data->pob1) && $key == $data->pob1) {
                    $pob .= $key . ' ' . $value . ",";
                }

                if (!empty($data->pob2) && $key == $data->pob2) {
                    $pob .= "<br />" . $key . ' ' . $value . ",";
                }

                if (!empty($data->pob3) && $key == $data->pob3) {
                    $pob .= "<br />" . $key . ' ' . $value . ",";
                }

                if (!empty($data->pob4) && $key == $data->pob4) {
                    $pob .= "<br />" . $key . ' ' . $value . ",";
                }

                if (!empty($data->pob5) && $key == $data->pob5) {
                    $pob .= "<br />" . $key . ' ' . $value . ",";
                }
            }*/

            if (!empty($data->adj1e)) {
                $pob =  $data->adj1e;
            }

            $tmpArr = array(
                //$i,
                $data->adj1c,
                $data->i_claimno,
                $data->status2,
                $data->eamsno,
                $data->e_name,
                $data->i_name,
                trim($data->d1_firm . ', ' . $data->d1_first . ' ' . $data->d1_last, ', '),
                trim($data->i_adjfst . ' ' . $data->i_adjuster),
                /*trim($data->d2_firm . ', ' . $data->d2_first . ' ' . $data->d2_last, ', '),*/
                trim($pob),
                $data->app_status,
                /*$data->i2_name,
                $data->i2_adjfst . ' ' . $data->i2_adjuste,
                $data->i2_claimno,*/
                $data->case_no,
                $links,
                $data->injury_id,
            );

            array_push($finalData, $tmpArr);
            $i++;
        }

        $totalData = $this->case_model->get_all_injury_count($caseId, $singleFilter);
        $listingData = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $finalData,
        );
        //echo '<pre>'; print_r($finalData); exit;
        /* $listingData = array(
          'data' => $finalData
          ); */

        echo json_encode($listingData);
    }

    public function getAllCaseEvents() {
        extract($_POST);
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'date',
            1 => 'attyh ',
            2 => 'attyass',
            3 => 'first',
            4 => 'event',
            5 => 'venue',
            6 => 'judge',
            7 => 'notes',
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') {
            $dt = $this->validateDate($search, $format = 'Y-m-d');
            $singleFilter = '(c1.date LIKE "%' . $dt . '%" OR c2.initials LIKE "%' . $search . '%" OR c1.attyass LIKE "%' . $search . '%" OR c1.first LIKE "%' . $search . '%" OR c1.event LIKE "%' . $search . '%" OR c1.venue LIKE "%' . $search . '%" OR c1.judge LIKE "%' . $search . '%" OR c1.notes LIKE "%' . $search . '%")';
            //$singleFilter = '( DAY(c1.date) LIKE "%' . $search .'" OR MONTH(c1.date) LIKE "%' . $search .'" OR YEAR(c1.date) LIKE "%' . $search .'" OR c2.initials LIKE "%' . $search . '%" OR c1.attyass LIKE "%'.$search.'%" OR c1.first LIKE "%'.$search.'%" OR c1.event LIKE "%'.$search.'%" OR c1.venue LIKE "%'.$search.'%" OR c1.judge LIKE "%'.$search.'%" OR c1.notes LIKE "%'.$search.'%")';
        }
        //echo "<pre>"; print_r($filterArray); exit;
        $rec = $this->case_model->getCaseCalendar($caseId, $start, $length, $filterArray, $order, $dir, $singleFilter);
        // echo "<pre>"; print_r($rec); exit;
        foreach ($rec as $data) {

            $date = ($data->date != '' && $data->date != '1899-12-30 00:00:00' && $data->date != '0000-00-00 00:00:00') ? date('m/d/Y h:i A', strtotime($data->date)) : '';
            $links = "<a class='btn btn-xs btn-info event_edit' data-eventno='" . $data->eventno . "' title='Edit'><i class = 'icon fa fa-edit'></i> </a>&nbsp;<a class='btn btn-xs btn-danger event_delete btn-circle btn-circle-action' data-eventno='" . $data->eventno . "' title='Delete'><i class='fa fa-trash'></i></a>";
            $name = $data->first . " " . $data->last . " v." . $data->defendant;
            $jsoncolor_codes = json_decode(color_codes, true);
            if ($data->color != 0) {
                $bgColor = $jsoncolor_codes[$data->color][2];
                $fcolor = $jsoncolor_codes[$data->color][1];
            } else {
                $bgColor = 0;
                $fcolor = 0;
            }

            if (strlen($data->notes) > 30) {
                $notes = substr($data->notes, 0, 27) . '...';
            } else {
                $notes = $data->notes;
            }
            $tmpArr = array(
                $date,
                $data->attyh,
                $data->attyass,
                $name,
                $data->event,
                $data->venue,
                $data->judge,
                $notes,
                $links,
                $data->eventno,
                $bgColor,
                $fcolor,
            );

            array_push($finalData, $tmpArr);
            $i++;
        }

        $totalData = $this->case_model->get_all_event_count($caseId, $filterArray);
        $listingData = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $finalData,
        );
        //echo '<pre>'; print_r($finalData); exit;
        /* $listingData = array(
          'data' => $finalData
          ); */

        echo json_encode($listingData);
    }

    public function getAllActivity() {
        extract($_POST);
        if (isset($_POST['category'])) {
            $category = $_POST['category'];
        } else {
            $category = '';
        }
        if (isset($_POST['chackvalue'])) {
            $chackvalue = $_POST['chackvalue'];
        } else {
            $chackvalue = '';
        }
        $caseId = $_POST['caseId'];
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'date',
            1 => 'event',
            2 => 'initials0',
            3 => 'initials',
            4 => 'actno'
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') {
            $dt = $this->validateDate($search, $format = 'Y-m-d');
            $singleFilter = '(date LIKE "%' . $dt . '%" OR initials LIKE "%' . $search . '%" OR initials0 LIKE "%' . $search . '%" OR event LIKE "%' . $search . '%")';
        }
        //echo '<pre> controller'; print_r($singleFilter); exit;
        $rec = $this->case_model->getCaseActivity($caseId, $category, $chackvalue, $start, $length, $order, $dir, $filterArray, $singleFilter);
        $finalData = array();
        $i = 1;
        foreach ($rec as $data) {

            $date = ($data->date != '' && $data->date != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($data->date)) : '';
            $time = ($data->date != '' && $data->date != '0000-00-00 00:00:00') ? date('h:i A', strtotime($data->date)) : '';

            /* OLE DOC LINK */
            $Olefilelink = "";
            $Olelink = "";
            $Olefiles = array();
            if(trim($data->ole3) != null && trim($data->ole3) != '') {
                $Olefiles = glob(DOCUMENTPATH . 'clients/' . $caseId . "/" . $data->ole3);
            }
            $files_main = array();
            $files_sub = array();
            $files = array();
            $files_main = glob(DOCUMENTPATH . 'clients/' . $caseId . "/[fF]" . $data->actno . ".*");
            $files_sub = glob(DOCUMENTPATH . 'clients/' . $caseId . "/[fF]" . $data->actno . "_*");
            $files = array_merge($files_main, $files_sub);
            $filelink = '';
            $Olelink='';
            
            if(count($Olefiles) > 0 && count($files) > 0) {
                $filelink = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid="' . $data->actno . '" href="javascript:void(0)" data-toggle="modal" title="View All"><i class = "icon fa fa-eye"></i> View All</a>';
            } else if(count($Olefiles) > 0) {
                $OlefileArr = explode("/", $Olefiles[0]);
				
				$fileArr = explode("/", $OlefileArr[7]);// print_r($fileArr);
                $extole = explode(".",$fileArr[0]);
                $Olelink = base_url() . 'assets/clients/' . $caseId . '/' . $OlefileArr[count($OlefileArr) - 1];
                 
                if($extole[1] == 'docx'){
                    if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                         $filelink = '<a  class="btn btn-xs btn-primary marginClass view_attach"  data-filename="one" target="_blank"  data-actid="' . $data->ole3 . '" href="' . base_url() . 'document/doceditor.php?fileID='.$fileArr[count($fileArr) - 1].'&user='.$this->session->userdata('user_data')['initials'].'&filePath='.$caseId.'/'.$fileArr[count($fileArr) - 1]. '" title="View"><i class = "icon fa fa-eye"></i> View</a>';
                    }else{
                        $filelink = '<a class="btn btn-xs btn-primary marginClass view_attach" data-actid="' . $data->actno . '" target="_blank" href = "' . $Olelink . '" title="View"><i class = "icon fa fa-eye"></i> View</a>';
                    }
                }
                     $filelink .= '<a  class="btn btn-xs btn-primary marginClass view_attach"  download="' . $OlefileArr[count($OlefileArr) - 1] . '" data-filename="one"  data-actid="' . $data->ole3 . '" href="' . $Olelink . '" title="Download"><i class = "icon fa fa-download"></i> Download</a>';
                     
                } else if(count($files) > 0) {
                if(count($files) == 1) {
                    $fileArr = explode("/", $files[0]);
                    $ext = explode(".",$fileArr[7]);
                    $link = base_url() . 'assets/clients/' . $caseId . '/' . $fileArr[count($fileArr) - 1];
                    /*$filelink = '<a  class="btn btn-xs btn-primary marginClass view_attach"  download="' . $fileArr[count($fileArr) - 1] . '" data-filename="one"  data-actid="' . $data->actno . '" href="' . $link . '" title="View"><i class = "icon fa fa-eye"></i> View</a>
                    <a  class="btn btn-xs btn-primary marginClass view_attach"  download="' . $OlefileArr[count($OlefileArr) - 1] . '" data-filename="one"  data-actid="' . $data->ole3 . '" href="' . $Olelink . '" title="Download"><i class = "icon fa fa-download"></i> Download</a>';*/
                    if($ext[1] == 'docx'){
                        if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                            $filelink = '<a class="btn btn-xs btn-primary marginClass view_attach" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'document/doceditor.php?fileID='.$fileArr[count($fileArr) - 1].'&user='.$this->session->userdata('user_data')['initials'].'&filePath='.$caseId.'/'.$fileArr[count($fileArr) - 1].'" title="View"><i class = "icon fa fa-eye"></i> View</a>';}
                        else{
                            $filelink = '<a class="btn btn-xs btn-primary marginClass view_attach" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $caseId . '/' . $fileArr[count($fileArr) - 1] . '" title="View"><i class = "icon fa fa-eye"></i> View</a>';
                        }
                    }
                    $filelink .= '<a  class="btn btn-xs btn-primary marginClass view_attach"  download="' . $OlefileArr[count($OlefileArr) - 1] . '" data-filename="one"  data-actid="' . $data->actno . '" href="' . $link . '" title="Download"><i class = "icon fa fa-download"></i> Download</a>';
                } else {
                    $filelink = '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid="' . $data->actno . '" href="javascript:void(0)" data-toggle="modal" title="View All"><i class = "icon fa fa-eye"></i> View All</a>';
                }
            } else {
                $filelink = '';
            }
//            echo $Olefilelink; exit;

            //if (($data->category == 26 || $data->category == 27) && $data->email_con_id != null) {
            if ($data->email_con_id != null) {
                $edlink = '<a class="btn btn-xs btn-info marginClass activity_edit btn-circle btn-circle-action" data-actid="' . $data->actno . '" href = "javascript:;" title="Edit"><i class = "icon fa fa-edit"></i> </a>
                    <a class="btn btn-xs btn-danger marginClass btn-circle btn-circle-action activity_delete" data-actid="' . $data->actno . '" href = "javascript:;" title="Delete"><i class="fa fa-trash"></i></a>
                    <a class="btn btn-xs btn-info marginClass btn-circle btn-circle-action activity_email" data-actid="' . $data->email_con_id . '" href = "javascript:;" title="Email"><i class = "icon fa fa-envelope"></i></a>';
            } else {
                $edlink = '<a class="btn btn-xs btn-info marginClass activity_edit btn-circle btn-circle-action" data-actid="' . $data->actno . '" href = "javascript:;" title="Edit"><i class = "icon fa fa-edit"></i> </a>
                    <a class="btn btn-xs btn-danger marginClass btn-circle btn-circle-action activity_delete" data-actid="' . $data->actno . '" href = "javascript:;" title="Delete"><i class="fa fa-trash"></i></a>';
            }

            $jsoncolor_codes = json_decode(color_codes, true);
            if ($data->color != 0) {
                $bgColor = $jsoncolor_codes[$data->color][2];
                $fcolor = $jsoncolor_codes[$data->color][1];
            } else {
                $bgColor = 0;
                $fcolor = 0;
            }
            if ($data->category == 1000021) {
                $data->initials = $data->initials0;
            } else {
                $data->initials = $data->initials;
            }
            /*if(strlen($data->event) > 90) {
                $data->event = substr($data->event, 0, 90) . " ...";
            }*/
            $data->event = nl2br($data->event);
            if ($data->access == 3) {
                if ($data->initials0 == $this->session->userdata('user_data')['username']) {

                    $tmpArr = array(
                        date('M d, Y', strtotime($date)) . '<br />' . $time,
                        $data->event,
                        $data->initials0,
                        $data->initials,
                        $filelink,
                        $edlink,
                        $data->actno,
                        $bgColor,
                        $fcolor,
                    );
                }
            } else {
                $tmpArr = array(
                    date('M d, Y', strtotime($date)) . '<br />' . $time,
                    $data->event,
                    $data->initials0,
                    $data->initials,
                    $filelink,
                    $edlink,
                    $data->actno,
                    $bgColor,
                    $fcolor,
                );
            }

            array_push($finalData, $tmpArr);
            $i++;
        }
        $totalData = $this->case_model->get_all_activity_count($caseId, $category, $chackvalue, '', $filterArray, $singleFilter);
        $listingData = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $finalData,
        );
        echo json_encode($listingData);
    }

    public function validateDate($date, $format = 'Y-m-d') {
        $d = date($format, strtotime($date));
        return $d;
    }

    public function getCaseParties() {

        extract($_POST);

        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'orderno',
            1 => 'cname',
            2 => 'type',
        );
        $orCondition = '';
        $singleFilter = '';
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if ($val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);

                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }

        if ($search['value'] != '') {
            $singleFilter = '(c.cardcode LIKE "%' . $search['value'] . '%" OR c.first LIKE "%' . $search['value'] . '%"  OR cd.firm LIKE "%' . $search['value'] . '%" OR c.last LIKE "%' . $search['value'] . '%" OR cc.type LIKE "%' . $search['value'] . '%")';
        }

        if (isset($caseId) && !empty($caseId)) {
            $filterArray['caseno'] = $caseId;
        }
        $data = $this->case_model->getcaseParties($start, $length, $filterArray, $order,$dir, $orCondition, $singleFilter);
        //echo $this->db->last_query();exit;
        //echo '<pre>'; print_r($data); exit;
        $finalData = array();
        $id = $start + 1;
        foreach ($data as $key => $val) {
            /* if($id == 1)
              {
              $edlink = '<a href="javascript:void(0);" data-casecard_no="'.$val->casecard_no.'" data-card="'.$val->cardcode.'" data-case="'.$val->caseno.'" class="btn btn-xs btn-info view_party"><i class="icon fa fa-eye"></i>View</a>';
              }
              else
              { */
            $edlink = '<a href="javascript:void(0);" data-casecard_no="' . $val->casecard_no . '" data-card="' . $val->cardcode . '" data-case="' . $val->caseno . '" class="btn btn-circle-action btn-circle btn-info view_party" title="View"><i class="icon fa fa-eye"></i></a> <a href="javascript:void(0);" data-card="' . $val->cardcode . '" data-case="' . $val->caseno . '" class="btn btn-danger btn-circle-action btn-circle remove_party" title="Delete"><i class="fa fa-trash"></i></a>';
            //}

            if ($val->firm != '') {
                $name = '<a href="javascript:void(0);" data-casecard_no="' . $val->casecard_no . '" data-card="' . $val->cardcode . '" data-case="' . $val->caseno . '"class="view_party">' . $val->firm . '</a>';
            } else {
                $name = '<a href="javascript:void(0);" data-casecard_no="' . $val->casecard_no . '" data-card="' . $val->cardcode . '" data-case="' . $val->caseno . '"class="view_party">' . $val->cname . '</a>';
            }
            $finalData[$key] = array(
                $id,
//                $reminderdatetime,
                $name,
                $val->type,
                $edlink,
                $val->cardcode,
            );
            $id++;
        }

        $listingData = array(
            'draw' => $draw,
            'recordsTotal' => $this->case_model->getPartyCount($filterArray, $orCondition, $singleFilter),
            'recordsFiltered' => $this->case_model->getPartyCount($filterArray, $orCondition, $singleFilter),
            'data' => $finalData,
        );
        // echo "<pre>"; print_r($listingData); exit();
        echo json_encode($listingData);
    }

    public function documents($case_id = 0) {
        $this->output->enable_profiler(false);
        $metaArray = array();

        if ($this->input->post('caseId') != null) {
            $case_id = $this->input->post('caseId');
        } else {

        }
        //echo 'case id = ' . $case_id; exit;
        $this->data['case_no'] = $case_id;

        $this->data['system_data'] = $this->common_functions->getsystemData();
        $main1 = json_decode($this->data['system_data'][0]->main1);
        $priparty = (explode(',', $main1->dpartytuse));
        $this->data['client_defendant'] = $this->common_functions->getCaseDefendantdetails($case_id, $priparty[0]);
        $getCaseCategory = $this->common_model->getAllRecord('actdeflt', 'actname,category', '', 'category asc');
        $this->data['caseactcategory'] = $getCaseCategory;
        $case_details = $this->case_model->getCaseFullDetails2($case_id);
        if ($case_details != '') {
            foreach ($case_details as $data) {
                if (!isset($users[strtolower($data->cardtype)])) {
                    if ($data->cardcode != 853)
                        $users[strtolower($data->cardtype)] = $data;
                }
            }
        } else {
            $case_details = array();
            $defaultArray = $this->setBlankCase();
            $case_details = $this->case_model->getCaseDataByCaseId($case_id);
            $case_details[0] = (object) array_merge($defaultArray, $case_details[0]);
        }
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'date',
            1 => 'date ',
            2 => 'event',
            3 => 'initials0',
            4 => 'initials',
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') {
            $dt = $this->validateDate($search, $format = 'Y-m-d');
            $singleFilter = '(date LIKE "%' . $dt . '%" OR initials LIKE "%' . $search . '%" OR initials0 LIKE "%' . $search . '%" OR event LIKE "%' . $search . '%")';
        }
        $case_injury = $this->case_model->getInjuryListing($case_id);
        $metaArray['case_no'] = $case_id;
        $metaArray['display_details'] = $this->data['display_details'];
        // echo '<pre>'; print_r($case_details); exit;
        $metaArray['users'] = $users;
        $metaArray['injury_details'] = $case_injury;
        $metaArray['client_defendant'] = $this->data['client_defendant'];
        $metaArray['display_all_details'] = $case_details;
        $metaArray['system_data'] = $systemdata;

        $this->data['pageTitle'] = $case_details[0]->first . " " . $case_details[0]->last;
        
        $this->data['display_details'] = $case_details[0];
        $case_activity = $this->case_model->getCaseActivity($case_id, $category = '', $chackvalue = '', $start, $length, $order, $dir, $filterArray, $singleFilter, 'document');
        //$metaArray['case_activity'] = $case_activity;
        $case_information = $this->removeBlankArray($metaArray);
        $this->data['case_activity'] = $case_activity;
        $this->data['injury_details'] = $case_injury;
//        echo $this->db->last_query();
        //echo '<pre>'; print_r($metaArray['display_all_details']); exit;

        $metadata = array('staff' => $this->data['loginUser'], 'data' => json_encode($case_information), 'caseno' => $case_id);
        $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);
        $this->common_functions->insertMetadata($metadata);
        $this->data['case_cards'] = $case_details;
        $this->data['print_letters'] = json_decode(letters_form_content, true);
        $this->data['form_name'] = json_decode(form_name, true);
        $this->data['product_delivery_unit'] = json_decode(product_delivery_unit, true);
        $this->data['doc_type'] = json_decode(doc_type, true);
        $this->data['doc_title'] = json_decode(doc_title, true);
        $this->load->view('common/header', $this->data);
        $this->load->view('cases/documents');
        $this->load->view('common/form_models');
        $this->load->view('common/footer');
    }

    public function getFormActivity() {
        extract($_POST);

        $caseId = $_REQUEST['caseId'];
        //echo $caseId; exit;
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'initials0',
            1 => 'actno ',
            2 => 'date',
            3 => 'event',
            4 => 'initials',
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') { //OR filename != ""
            $dt = $this->validateDate($search, $format = 'Y-m-d');
            $singleFilter = '(date LIKE "%' . $dt . '%" OR initials LIKE "%' . $search . '%" OR initials0 LIKE "%' . $search . '%" OR event LIKE "%' . $search . '%" )';
        }/* else {
            $singleFilter = '(filename != "")';
        }*/

        $rec = $this->case_model->getCaseActivity($caseId, $category, $chackvalue, $start, $length, $order, $dir, $filterArray, $singleFilter, 'document');
        $finalData = array();
        $i = 1;
        $cnt = 1;
        //echo '<pre>'; print_r($recs); exit;
        $actno = array();
        foreach ($rec as $data) {
            $ViewDownloadLink = '';

            $date = ($data->date != '' && $data->date != '0000-00-00 00:00:00') ? date('m/d/Y', strtotime($data->date)) : '';
            $time = ($data->date != '' && $data->date != '0000-00-00 00:00:00') ? date('h:i A', strtotime($data->date)) : '';
            
                
            //echo $data->actno;
            $files_main = glob(DOCUMENTPATH . 'clients/' . $caseId . "/[fF]" . $data->actno . ".*");
            $files_sub = glob(DOCUMENTPATH . 'clients/' . $caseId . "/[fF]" . $data->actno . "_*");
            $files = array_merge($files_main, $files_sub);
            //echo '<pre>'; print_r($files);
            $ViewDownloadLink = '';
            if (!empty($files) && count($files) == 1) {
                $fileArr = explode("/", $files[0]);
                $ext = explode(".",$fileArr[7]);
                //$link = base_url() . 'assets/clients/' . $caseno . '/' . $fileArr[7];
                //$document = '<a  class="btn btn-xs btn-primary marginClass view_attach"  download=' . $fileArr[7] . ' data-filename="one"  data-actid=' . $val->actno . ' href=' . $link . ' title="View"><i class = "icon fa fa-eye"></i> View</a>';
                $actno[] = $data->actno;
                if($ext[1] == 'docx' || $ext[1] == 'wpd' || $ext[1] == 'pdf' || $ext[1] == 'msg' || $ext[1] == 'xml' ) {
                    if($ext[1] == 'docx') {
                        if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                            $ViewDownloadLink = '<a class="btn btn-xs btn-circle btn-info btn-circle-action marg-right5 view_attach" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'document/doceditor.php?fileID='.$fileArr[7].'&user='.$this->session->userdata('user_data')['initials'].'&filePath='.$caseId.'/'.$fileArr[7].'" title="View"><i class = "icon fa fa-eye"></i></a><a class="btn btn-xs btn-circle btn-info btn-circle-action marg-right5 view_attach" data-actid="' . $data->actno . '" href = "' . base_url() . 'assets/clients/' . $caseId . '/' . $fileArr[7] . '" title="Download"><i class = "icon fa fa-download"></i></a>';
                        } else {
                            $ViewDownloadLink = '<a class="btn btn-xs btn-primary marginClass view_attach" data-actid="' . $data->actno . '" href = "' . base_url() . 'assets/clients/' . $caseId . '/' . $fileArr[7] . '" title="Download"><i class = "icon fa fa-download"></i></a><a class="btn btn-xs btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $caseId . '/' . $fileArr[7] . '" title="View"><i class = "icon fa fa-eye"></i></a>';
                        }
                    } else if($ext[1] == 'wpd' || $ext[1] == 'pdf' || $ext[1] == 'msg' || $ext[1] == 'xml') {
                        $ViewDownloadLink = '<a class="btn btn-xs btn-circle btn-info btn-circle-action marg-right5 view_attach" data-actid="' . $data->actno . '" href = "' . base_url() . 'assets/clients/' . $caseId . '/' . $fileArr[7] . '" title="Download"><i class = "icon fa fa-download"></i>';
                    }
                    $ViewDownloadLink .= '<a class="btn btn-xs btn-circle btn-info btn-circle-action marg-right5 activity_edit" data-actid="' . $data->actno . '" href = "javascript:;" title="Edit"><i class = "icon fa fa-edit"></i> </a><a class="btn btn-xs btn-danger btn-circle btn-circle-action  activity_delete" data-actid="' . $data->actno . '" href = "javascript:;" title="Delete"><i class = "icon fa fa-trash"></i> </a>';
                    /*$EV = '';
                    if (strlen($data->event) > 70) {
                        $EV = substr($data->event, 0, 70) . '...';
                    } else {
                        $EV = $data->event;
                    }*/
                    $jsoncolor_codes = json_decode(color_codes, true);
                    if ($data->color != 0) {
                        $bgColor = $jsoncolor_codes[$data->color][2];
                        $fcolor = $jsoncolor_codes[$data->color][1];
                    } else {
                        $bgColor = 0;
                        $fcolor = 0;
                    }
                    $tmpArr = array(
                        $data->initials,
                        $data->actno,
                        $date,
                        //$EV,
                        $data->event,
                        $ViewDownloadLink,
                        $bgColor,
                        $fcolor,
                    );

                    array_push($finalData, $tmpArr);
                    $i++;
                }
            } 
                    
                    
            
            /* if (!empty($data->filename)) {
                //echo DOCUMENT_AUTOSAVE_ENABLED; exit;
                $ext = explode(".",$data->filename);
                if($ext[1] == 'docx'){
                if (DOCUMENT_AUTOSAVE_ENABLED == 'true') {
                $ViewDownloadLink = '<a class="btn btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $data->caseno . '/' . $data->filename . '" title="Download"><i class = "icon fa fa-download"></i><a class="btn btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'document/doceditor.php?fileID='.$data->filename.'&user='.$this->session->userdata('user_data')['initials'].'&filePath='.$data->caseno.'/'.$data->filename.'" title="View"><i class = "icon fa fa-eye"></i></a>';}
                else{
                    $ViewDownloadLink = '<a class="btn btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $data->caseno . '/' . $data->filename . '" title="Download"><i class = "icon fa fa-download"></i></a><a class="btn btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $data->caseno . '/' . $data->filename . '" title="View"><i class = "icon fa fa-eye"></i></a>';
                }
                }else if($ext[1] == 'wpd' || $ext[1] == 'pdf'){
                    $ViewDownloadLink = '<a class="btn btn-circle btn-info btn-circle-action marg-right5" data-actid="' . $data->actno . '" target="_blank" href = "' . base_url() . 'assets/clients/' . $data->caseno . '/' . $data->filename . '" title="Download"><i class = "icon fa fa-download"></i></a>';
                }
            } else {
                $ViewDownloadLink = '';
            }*/
            
        }
    
        // $totalData = $this->case_model->get_all_activity_count($caseId, $category, $chackvalue, true);
        $arr_slice = array_slice($finalData,$start,$length);
        $listingData = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval(count($finalData)),
            "recordsFiltered" => intval(count($finalData)),
            "data" => $arr_slice,
        );
        //echo '<pre>'; print_r($finalData); exit;
        /* $listingData = array(
        'data' => $finalData
        ); */
        
        echo json_encode($listingData);
    }

    /*     * **** Get party details for adding in injury form (Namrata Dubey) ****************** */

    public function getmainpartydetail() {
        $caseno = (isset($_POST['caseno'])) ? $_POST['caseno'] : "";
        $Mainpartydetail = $this->case_model->getmainpartydetail($caseno);
        if (!empty($Mainpartydetail)) {
            $result = array('status' => true, "result" => $Mainpartydetail[0]);
        } else {
            $result = array('status' => false, "result" => []);
        }
        echo json_encode($result);
    }

    public function searchForRolodexAddress() {
        //echo '<pre>'; print_r($_POST); exit;
        extract($_POST);
        $where = '';

        $searchResult = array();
        if ($add1 != '' && $add2 == '' && $city == '' && $state == '') {
            $where = array('c2.address1' => $add1);
        }
        if ($add1 == '' && $add2 != '' && $city == '' && $state == '') {
            $where = array('c2.address2' => $add2);
        }
        if ($add1 == '' && $add2 == '' && $city != '' && $state == '') {
            $where = array('c2.city' => $city);
        }
        if ($add1 == '' && $add2 == '' && $city == '' && $state != '') {
            $where = array('c2.state' => $state);
        }
        if ($add1 != '' && $add2 != '' && $city == '' && $state == '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2);
        }
        if ($add1 != '' && $add2 != '' && $city != '' && $state == '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2, 'c2.city' => $city);
        }
        if ($add1 != '' && $add2 != '' && $city != '' && $state != '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2, 'c2.city' => $city, 'c2.state' => $state);
        }
        if ($add1 == '' && $add2 != '' && $city != '' && $state == '') {
            $where = array('c2.address2' => $add2, 'c2.city' => $city);
        }
        if ($add1 == '' && $add2 != '' && $city != '' && $state != '') {
            $where = array('c2.address2' => $add2, 'c2.city' => $city, 'c2.state' => $state);
        }
        if ($add1 != '' && $add2 == '' && $city != '' && $state == '') {
            $where = array('c2.address1' => $add1, 'c2.city' => $city);
        }
        if ($add1 == '' && $add2 == '' && $city != '' && $state != '') {
            $where = array('c2.state' => $state, 'c2.city' => $city);
        }
        if ($add1 != '' && $add2 == '' && $city == '' && $state != '') {
            $where = array('c2.address1' => $add1, 'c2.state' => $state);
        }
        if ($add1 == '' && $add2 != '' && $city == '' && $state != '') {
            $where = array('c2.address2' => $add2, 'c2.state' => $state);
        }
        if ($add1 != '' && $add2 == '' && $city != '' && $state != '') {
            $where = array('c2.address1' => $add1, 'c2.state' => $state);
        }
        if ($add1 != '' && $add2 != '' && $city == '' && $state != '') {
            $where = array('c2.address1' => $add1, 'c2.address2' => $add2, 'c2.state' => $state);
        }

        if ($search['value'] != '') {
            $singleFilter = '(c2.firm LIKE "%' .$search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.phone1 LIKE "%' . $search['value'] . '%" OR c2.address1 LIKE "%' . $search['value'] .'%" OR c.first LIKE "%' . $search['value'] . '%" OR c.last LIKE "%' . $search['value'] . '%" OR c2.address2 LIKE "%' . $search['value'] . '%" OR c2.city LIKE "%' . $search['value'] . '%" OR c2.state LIKE "%' . $search['value'] . '%" OR c2.zip LIKE "%' . $search['value']. '%")';
        }

        $result = $this->case_model->getRolodexSearchParties($where, $start, $length, $order , $singleFilter);
        $count = $this->case_model->getRolodexSearchPartiesCount($where,$singleFilter);

        $finalData = array();
        if (!empty($result)) {
            foreach ($result as $data) {
                $Name = '';
                if (isset($data->first) && !empty($data->first)) {
                    $Name = $data->first . " , " . $data->last;
                } else {
                    $Name = $data->last;
                }
                $finalData[] = array(
                    $data->firm,
                    $data->city,
                    $data->address1,
                    $data->address2,
                    $data->phone1,
                    $Name,
                    $data->state,
                    $data->zip,
                    $data->cardcode,
                    $data->type
                );
            }
        }
        $totalCount = 0;
        $listingData = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $count,
            'data' => $finalData
        );

        echo json_encode($listingData);
        exit;
    }

    
    public function countinsuranceparty() {
        $caseno = (isset($_POST['caseno'])) ? $_POST['caseno'] : "";
        $InsPArtycount = $this->case_model->countinsuranceparty($caseno);
        $response = array('status' => 1, 'count' => $InsPArtycount);
        echo json_encode($response);
        exit;
    }
    
    public function countemployerparty() {
        $caseno = (isset($_POST['caseno'])) ? $_POST['caseno'] : "";
        $InsPArtycount = $this->case_model->countemployer($caseno);
        $response = array('status' => 1, 'count' => $InsPArtycount);
        echo json_encode($response);
        exit;
    }
    
    public function countdefenseatt() {
        $caseno = (isset($_POST['caseno'])) ? $_POST['caseno'] : "";
        $defenseattcount = $this->case_model->countdefenseatt($caseno);
        $response = array('status' => 1, 'count' => $defenseattcount);
        echo json_encode($response);
        exit;
    }
    
    public function countdoc() {
        $caseno = (isset($_POST['caseno'])) ? $_POST['caseno'] : "";
        $doccount = $this->case_model->countdoc($caseno);
        $response = array('status' => 1, 'count' => $doccount);
        echo json_encode($response);
        exit;
    }

    public function get_party_info() {
        $result = $this->case_model->get_party_info($_POST['caseno']);
        echo json_encode($result);
    }

    public function get_details_for_emailparty(){
       $res =  $this->case_model->get_details_for_emailparty($_POST['card_code']);
       echo json_encode($res);
    }

    public function remove_case_activity_attachment() {
        $case_no = $_POST['caseNo'];
        $activity_no = $_POST['actNo'];
        $file_name = $_POST['fileName'];
        $file = APPLICATION_PATH.'/assets/clients/'.$case_no.'/'.$file_name;
        if ($file_name != '' && file_exists($file)) {
            $result = $this->case_model->remove_case_activity_attachment($case_no, $activity_no, $file);
            if($result > 0) {
                if (unlink($file)) {
                    $data['status'] = 1;
                    die(json_encode($data));
                } else {
                    $data['status'] = 2;
                    die(json_encode($data));
                }
            } else {
                $data['status'] = 3;
                die(json_encode($data));
            }
        } else {
            $data['status'] = 4;
            die(json_encode($data));
        }
    }

    public function get_uploaded_files() {
        $dir = ASSPATH.'clients/'.$_POST['caseNo'];
        $actno = $_POST['actNo'];
        $filteredArray = array();
        foreach (glob($dir . "/[F|I]" . $actno . "*") as $key => $filename) {
            $f_tmp_name = explode('/', $filename);
            $f_name = $f_tmp_name[7];
            $filteredArray[$key]['name'] = $f_name;
            $filteredArray[$key]['path'] = $filename;
        }
        echo json_encode($filteredArray);
    }

    public function remove_clicked_file() {
        $case_no = $_POST['caseNo'];
        $file_name = $_POST['fileName'];
        $file = APPLICATION_PATH.'assets/clients/'.$case_no.'/'.$file_name;
        if(file_exists($file)) {
            if (unlink($file)) {
                $data = 1;
                die(json_encode($data));
            } else {
                $data = 2;
                die(json_encode($data));
            }
        } else {
            $data = 3;
            die(json_encode($data));
        }
    }
    
    public function viewactivityemailNew() {
        $caseno = $this->input->post('caseno');
        $filename = $this->input->post('filename');
        $this->load->model('email_model_new');
        $email_activity = $this->email_model_new->get_single_email_case($caseno, $this->input->post('filename'));
        //echo $this->db->last_query(); exit;
        //echo '<pre>'; print_r($email_activity[0]); exit;
        $data = '';
        /*$cspan = '8';
        $data .= "<tr class='read rclose'><td colspan='" . $cspan . "'>";
        $data .= "<table class='table'style='border:1px solid #337ab7; width:100%;'>";*/
        foreach ($email_activity as $email) {
            
            $ruclass = ($email->readyet == 'Y') ? 'read' : 'unread';
            $datafilename = $email->parcelid;
            $email_con_id = $email->parcelid;
            $subject = $email->subject;
            $date = date('M d, Y', strtotime($email->datesent));
            $members = explode(",",$email->whoto);
            //echo "<pre>"; print_r($members); exit;
            $internal_party = array();
            foreach ($members as $key => $value) {
                if ($this->session->userdata('user_data')['initials'] != $value) {
                    array_push($internal_party, $value);
                }
                // array_push($internal_party, $value);
            }
            $internal_party = implode(",",$internal_party);
            if($email->whotoexternal != ''){
                $internal_party .= ','.$email->whotoexternal;
            }
            $internal_party = trim($internal_party, ", ");
            /*$data .= "<tr>";
            $data .= '<td style="width: 20%;" class="mail-ontact sorting_1">';*/
            $data .= "<a style='display: none;' class='read-email' href='javascript:;' data-filename='$datafilename' email_con_id='$email_con_id' ethread='' '>$internal_party</a></td>";
            /*$data .= "<td style='width: 50%;' class='mail-subject'><a class='read-email' href='javascript:;' data-filename='$datafilename' email_con_id='' ethread='' iore='I'>$subject <span class='pull-right'></span></a></td>";*/
            $data .= "<a style='display: none;' class='read-email' href='javascript:;' data-filename='$datafilename' email_con_id='' ethread='' iore='".$iore."'>$subject <span class='pull-right'></span></a>";
            /*$data .= "<td style='width: 20%;' class='text-right mail-date'>$date</td></tr>";*/
        }
        /*$data .= "</table></td></tr>";
        $data .= "</div></td></tr>";*/
        //echo "<pre>"; print_r($email_activity); exit;
        die($data);
    }
    
    public function documents_New($case_id = 0) {
        $this->output->enable_profiler(false);
        $metaArray = array();

        if ($this->input->post('caseId') != null) {
            $case_id = $this->input->post('caseId');
        } else {

        }
        //echo 'case id = ' . $case_id; exit;
        $this->data['case_no'] = $case_id;

        $this->data['system_data'] = $this->common_functions->getsystemData();
        $main1 = json_decode($this->data['system_data'][0]->main1);
        $priparty = (explode(',', $main1->dpartytuse));
        $this->data['client_defendant'] = $this->common_functions->getCaseDefendantdetails($case_id, $priparty[0]);
        $getCaseCategory = $this->common_model->getAllRecord('actdeflt', 'actname,category', '', 'category asc');
        $this->data['caseactcategory'] = $getCaseCategory;
        $case_details = $this->case_model->getCaseFullDetails2($case_id);
        if ($case_details != '') {
            foreach ($case_details as $data) {
                if (!isset($users[strtolower($data->cardtype)])) {
                    if ($data->cardcode != 853)
                        $users[strtolower($data->cardtype)] = $data;
                }
            }
        } else {
            $case_details = array();
            $defaultArray = $this->setBlankCase();
            $case_details = $this->case_model->getCaseDataByCaseId($case_id);
            $case_details[0] = (object) array_merge($defaultArray, $case_details[0]);
        }
        $finalData = array();
        $filterArray = array();
        $i = 1;
        $columns = array(
            0 => 'date',
            1 => 'date ',
            2 => 'event',
            3 => 'initials0',
            4 => 'initials',
        );
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = isset($this->input->post('search')['value']) ? $this->input->post('search')['value'] : '';
        $singleFilter = '';
        if ($columns != "") {
            foreach ($columns as $key => $val) {
                if (isset($val['search']['value']) && $val['search']['value'] != '') {
                    $filterData = json_decode($val['search']['value'], true);
                    if ($filterData[key($filterData)] != '') {
                        $filterArray[key($filterData)] = $filterData[key($filterData)];
                    }
                }
            }
        }
        if ($search != '') {
            $dt = $this->validateDate($search, $format = 'Y-m-d');
            $singleFilter = '(date LIKE "%' . $dt . '%" OR initials LIKE "%' . $search . '%" OR initials0 LIKE "%' . $search . '%" OR event LIKE "%' . $search . '%")';
        }
        $case_injury = $this->case_model->getInjuryListing($case_id);
        $metaArray['case_no'] = $case_id;
        $metaArray['display_details'] = $this->data['display_details'];
        // echo '<pre>'; print_r($case_details); exit;
        $metaArray['users'] = $users;
        $metaArray['injury_details'] = $case_injury;
        $metaArray['client_defendant'] = $this->data['client_defendant'];
        $metaArray['display_all_details'] = $case_details;
        $metaArray['system_data'] = $systemdata;

        $this->data['pageTitle'] = $case_details[0]->first . " " . $case_details[0]->last;
        
        $this->data['display_details'] = $case_details[0];
        $case_activity = $this->case_model->getCaseActivity($case_id, $category = '', $chackvalue = '', $start, $length, $order, $dir, $filterArray, $singleFilter, 'document');
        //$metaArray['case_activity'] = $case_activity;
        $case_information = $this->removeBlankArray($metaArray);
        $this->data['case_activity'] = $case_activity;
        $this->data['injury_details'] = $case_injury;
//        echo $this->db->last_query();
        //echo '<pre>'; print_r($metaArray['display_all_details']); exit;

        $metadata = array('staff' => $this->data['loginUser'], 'data' => json_encode($case_information), 'caseno' => $case_id);
        $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);
        $this->common_functions->insertMetadata($metadata);
        $this->data['case_cards'] = $case_details;
        $this->data['print_letters'] = json_decode(letters_form_content, true);
        $this->data['form_name'] = json_decode(form_name, true);
        $this->data['product_delivery_unit'] = json_decode(product_delivery_unit, true);
        $this->data['doc_type'] = json_decode(doc_type, true);
        $this->data['doc_title'] = json_decode(doc_title, true);
        $this->load->view('common/header', $this->data);
        $this->load->view('cases/documents');
        $this->load->view('common/form_models');
        $this->load->view('common/footer');
    }


}
