<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Email_old extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $defaultFilter = '';
        $this->load->model('case_model');
        $this->load->model('email_model_old');
        $this->load->model('email_conversation_model');
        $inboundcheck = $this->uri->segment(2);
        if ($inboundcheck != 'inboundemails') {
            $this->common_functions->checkLogin();
            $this->common_functions->checkSessionId();
        }
        $this->data['url_seg'] = $this->uri->segment(1);

        $data = array();
        $this->data['menu'] = 'email';
        $this->data['withoutcno_count'] = 0;
        $this->data['internal_count'] = 0;
        $this->data['external_count'] = 0;
        $this->data['newemail_cnt'] = 0;
        $this->data['oldemail_cnt'] = 0;
        $this->data['urgentnew_cnt'] = 0;
        $this->data['urgentold_cnt'] = 0;
        $this->data['unreadnew_cnt'] = 0;
        $this->data['unreadold_cnt'] = 0;
        $this->data['new_ext_woc_cnt'] = 0;
        $this->data['new_ext_wc_cnt'] = 0;
        $this->data['old_ext_woc_cnt'] = 0;
        $this->data['old_ext_wc_cnt'] = 0;
        $this->data['new_int_woc_cnt'] = 0;
        $this->data['new_int_wc_cnt'] = 0;
        $this->data['old_int_woc_cnt'] = 0;
        $this->data['old_int_wc_cnt'] = 0;
        $this->data['old_int_cnt'] = 0;
        $this->data['new_int_cnt'] = 0;
        $this->data['old_ext_cnt'] = 0;
        $this->data['unread_count'] = 0;
    }

    public function __destruct() {
        
    }

    public function index_old() {
        $this->output->enable_profiler(false);
        $this->data['pageTitle'] = 'Emails';
        $emailType = '';
        $caseType = '';
        $defaultFilter = '';
        $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);
        $this->load->view('common/header', $this->data);
        //$this->load->view('email/email');
        $this->load->view('email/email_dash');
        $this->load->view('common/footer');
    }

    public function index() {
        $this->output->enable_profiler(false);
        $this->data['pageTitle'] = 'Emails';
        $emailType = '';
        $caseType = '';
        $defaultFilter = '';
        $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);
        $this->data['signature'] = $this->common_functions->getSignaturedetails($this->session->userdata('user_data')['initials']);
        $this->load->view('common/header', $this->data);
        //$this->load->view('email/email');
        $this->load->view('email_new/email_dash');
        $this->load->view('common/footer');
    }

    public function markreadview($email_id = '') {
        
        $this->data['emails'] = $this->email_model_old->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'), $this->input->post('iore'), $this->input->post('e2id'));
            
            $this->email_model_old->update_email_read($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->data['emails']->iore);
            $this->data['unread_count'] = $this->email_model_old->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
            die(json_encode($this->data));
    }
	
    public function markunreadview($email_id = '') {
        
        $this->data['emails'] = $this->email_model_old->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'), $this->input->post('iore'), $this->input->post('e2id'));
            
            $this->email_model_old->update_email_unread($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->data['emails']->iore);
            $this->data['unread_count'] = $this->email_model_old->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
        die(json_encode($this->data));
    }
    
    public function view($email_id = '') {
        //echo 'in function'; exit;
        $this->data['emails'] = $this->email_model_old->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'), $this->input->post('iore'), $this->input->post('e2id'))[0];
        //echo $this->db->last_query();
        //echo "<pre>";print_r($this->data['emails']);exit;
        if ($this->data['emails']->readyet != 'Y') {
           // $this->email_model_old->update_email_read($this->session->userdata('user_data')['initials'], $this->data['emails']->filename, $this->data['emails']->iore);
            //$this->data['unread_count'] = $this->email_model_old->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
        }
        // if (!empty($this->data['emails']->mail_body) && $this->data['emails']->iore == 'E' && $this->input->post('current_list') != 'sent') {
        if (!empty($this->data['emails']->mail_body) && strpos($this->data['emails']->whofrom, '@') !== false) {
           $parse_mail = $this->common_functions->mimemailparse($this->data['emails']->mail_body, $this->input->post('filename'));
            $this->data['emails']->mail_body = str_replace("\n","<br>",$parse_mail['text']);
            $who_form_name = explode('<', $parse_mail['from']);
            $this->data['emails']->whofrom_name = $who_form_name[0];
        } else {
            if(strpos($this->data['emails']->subject, 'Task') !== false && ($this->input->post('current_list') == 'sent' || $this->input->post('current_list') == 'Self' || $this->input->post('current_list') == 'new')) {
                $this->data['emails']->mail_body = nl2br($this->data['emails']->mail_body);
            }
        }
        $directory = FCPATH . '/assets/attachment/' . $this->input->post('filename');
        $link = base_url() . 'assets/attachment/' . $this->input->post('filename') . '/';
        $removelink = 'assets/attachment/' . $this->input->post('filename') . '/';
        $file_data = '';
        if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
            $files = glob($directory . "/*");
            $file_data .= '<h4>Attachment</h4><ul class="attatchement_thumbnail" style="margin-left: -15px;">';
            foreach ($files as $value) {
                $file_name = basename($value);
                $filenameextension = explode('.',$file_name);
                
                //// set this for remove attachment - <i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>

                if($filenameextension[1]== 'tif' || $filenameextension[1]== 'tiff' || $filenameextension[1]== 'bmp' || $filenameextension[1]== 'jpg' || $filenameextension[1]== 'jpeg' || $filenameextension[1]== 'gif' || $filenameextension[1]== 'png' || $filenameextension[1]== 'svg' || $filenameextension[1]== 'eps')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><img src='".$link . $file_name."' width='100'></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'webm' || $filenameextension[1]== 'mkv' || $filenameextension[1]== 'flv' || $filenameextension[1]== 'vob' || $filenameextension[1]== 'ogv' || $filenameextension[1]== 'ogg' || $filenameextension[1]== 'mpeg' || $filenameextension[1]== 'mpg' || $filenameextension[1]== 'mp4' || $filenameextension[1]== 'avi' || $filenameextension[1]== '3gp')
				{
					$file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><video controls><source src='".$link . $file_name."' type='video/". $filenameextension[1] ."'></video></div>" . $filenameextension[0] . "</a>  </div></li>";
				}
				else if($filenameextension[1]== 'mp3' || $filenameextension[1]== 'wma' || $filenameextension[1]== 'au' || $filenameextension[1]== 'ea' || $filenameextension[1]== 'aif')
				{
					$file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><audio  width='320' height='240' controls><source src='".$link . $file_name."' type='video/". $filenameextension[1] ."'></video></div>" . $filenameextension[0] . "</a>  </div></li>";
				}
				
				else if($filenameextension[1]== 'txt')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-text-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'zip' || $filenameextension[1]== 'rar')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-zip-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'rar')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-archive-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'doc' || $filenameextension[1]== 'docx')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-word-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'pdf')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-pdf-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'xls' || $filenameextension[1]== 'xlsx' || $filenameextension[1]== 'csv')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-excel-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'ppt' || $filenameextension[1]== 'pptx')
                {
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-powerpoint-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else
				{
					$file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
				}
                
            }   
            $file_data .= '</ul>';
        }
        if (!empty($file_data)) {
            $this->data['file_with_attachment'] = $file_data;
        } else {
            $this->data['file_with_attachment'] = '';
        }
        
        $to_New = explode(',', $this->data['emails']->email_con_people);
        /*if ($this->data['emails']->whofrom == $this->session->userdata('user_data')['initials'] && ($key = array_search($this->session->userdata('user_data')['initials'], $to_New)) !== false) {
            unset($to_New[$key]);
        }
        $i = 0;
        $new_to_array = array();
        foreach ($to_New as $key1 => $value1) {
            if(!in_array($value1, $new_to_array)) {
                $new_to_array[$i] = $value1;
                $i++;
            }
        }*/
        $vals = array_count_values($to_New);
        $flag = 0;
        foreach ($vals as $key => $value) {
            if($key == $this->session->userdata('user_data')['initials'] && $value > 1) {
                $flag = 1;
            }
        }
        if($flag == 1) {
            $key1 = array_search($this->session->userdata('user_data')['initials'], $to_New);
            unset($to_New[$key1]);
        }
        if(sizeof($to_New) > 1) {
            $pass_new = implode(",", $to_New);
        } else {
            $pass_new = $to_New[0];
        }
        $this->data['emails']->email_con_people = $pass_new;
        if (!empty($this->data['emails']->email_con_id)) {
            $reply_all = $this->email_conversation_model->grab_conversation_from_id($this->data['emails']->email_con_id);
            if (!empty($reply_all)) {
                $members = rtrim($reply_all[0]->email_con_people, ',');
                $members = explode(',', $members);
                if (count($members) > 1) {
                    $this->data['reply_all'] = 1;
                }
            } else {
                $this->data['reply_all'] = 0;
            }
        } else {
            $this->data['reply_all'] = 0;
        }
        die(json_encode($this->data));
    }

    public function compose() {
        $this->data['pageTitle'] = 'Compose Emails';
        $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);
        $this->load->view('common/header', $this->data);
        $this->load->view('email/compose_email');
        $this->load->view('common/footer');
    }

    public function refresh_listing() {
        $columns = array(
            0 => 'filename',
            1 => 'whofrom',
         /*   2 => 'subject',
            3 => 'datesent',*/
        );

        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = $this->input->post('search')['value'];
        $current_list = ($this->input->post('current_list') != '') ? $this->input->post('current_list') : 'new';
        $start = ($this->input->post('start') != 0) ? $this->input->post('start') : 0;
        $length = ($this->input->post('length') != 0) ? $this->input->post('length') : DEFAULT_LISTING_LENGTH;
        $list_type = ($this->input->post('list_type') != '') ? $this->input->post('list_type') : '';
        if ($this->input->post('emailType') != '') {
            $emailType = $this->input->post('emailType');
        } else {
            $emailType = '';
        }
        if ($this->input->post('caseType') != '') {
            $caseType = $this->input->post('caseType');
        } else {
            $caseType = '';
        }

        $totalFiltered = 0;

        $result = array();
        $singleFilter = '';
        if ($search != '') {
            $singleFilter = '(e.subject LIKE "%' . $search . '%" OR CONCAT(s1.fname, " ", s1.lname) LIKE "%' . $search . '%" OR e.whofrom LIKE "%' . $search . '%" OR e2.whoto LIKE "%' . $search . '%")';
        }
        if ($current_list == 'sent') {
            /* $singleFilter = '(e.subject LIKE "%' . $this->input->post('search_data') . '%" OR CONCAT(s2.fname, " ", s2.lname) LIKE "%' . $this->input->post('search_data') . '%" OR e.whofrom LIKE "%'.$this->input->post('search_data').'%" OR e2.whoto LIKE "%'.$this->input->post('search_data').'%")'; */
            $emails = $this->email_model_old->get_sent_emails($this->session->userdata('user_data')['initials'], $length, $start, $singleFilter, '0', $order, $dir, $emailType, $caseType);
            $totalFiltered = $this->email_model_old->get_sent_emails($this->session->userdata('user_data')['initials'], $length, $start, $singleFilter, '1', '', '', $emailType, $caseType);
            // exit;
        } else {
            if ($current_list == 'new') {
                if (!empty($list_type) && $list_type == 'urgent') {
                    $defaultFilter = array('e2.oldmail != ' => '1', 'e.urgent' => 'Y');
                } else if (!empty($list_type) && $list_type == 'unread') {
                    $defaultFilter = array('e2.oldmail != ' => '1', 'e2.readyet !=' => 'Y');
                } else {
                    $defaultFilter = array('e2.oldmail !=' => '1');
                }

                $emails = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir);
                // print_r($emails);exit;
                $totalFiltered = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir, 1);
            } else if ($current_list == 'old') {
                if (!empty($list_type) && $list_type == 'urgent') {
                    $defaultFilter = array('e2.oldmail' => '1', 'e.urgent' => 'Y');
                } else if (!empty($list_type) && $list_type == 'unread') {
                    $defaultFilter = array('e2.oldmail' => '1', 'e2.readyet !=' => 'Y');
                } else {
                    $defaultFilter = array('e2.oldmail' => '1');
                }
                $emails = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir);
                $totalFiltered = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir, 1);
            } else if ($current_list == 'delete') {
                // $defaultFilter = array("e2.detached" => 1, 'LOCATE("' . $this->session->userdata('user_data')['initials'] . '", e2.deleted_by) >', 0);

                $defaultFilter = "( e2.detached = 1 OR LOCATE('{$this->session->userdata('user_data')['initials']}', e2.deleted_by) > 0)";
                $emails = $this->email_model_old->get_deleted_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir);

                $totalFiltered = $this->email_model_old->get_deleted_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir, 1);
            } else if ($current_list == 'Self') {
                if (!empty($list_type) && $list_type == 'urgent') {
                    $defaultFilter = array('e2.oldmail != ' => '1', 'e.urgent' => 'Y');
                } else if (!empty($list_type) && $list_type == 'unread') {
                    $defaultFilter = array('e2.oldmail != ' => '1', 'e2.readyet !=' => 'Y');
                } else {
                    $defaultFilter = array('e2.oldmail !=' => '1');
                }
                $emails = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir);
                $totalFiltered = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType, $singleFilter, $order, $dir, 1);
            }
        }
        $dispatch_data = array();
        $dispatch_ajax = array();
        // echo "<pre>"; print_r($emails);
		$i=1;
        foreach ($emails as $key => $value) {
            $unreadcls = '';
            
            $directory = FCPATH . '/assets/attachment/' . $value->filename;
            $attachment = '';
            if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                $attachment = '<span class="pull-right"><i class="fa fa-paperclip"></i></span>';
            }

            if ($value->readyet != 'Y'/* && $current_list != 'sent'*/) {
                $unreadcls = 'unread';
            }
            $getemailthread = $this->email_model_old->getethreadcount($value->email_con_id, $this->session->userdata('user_data')['initials'], $current_list)[0];
            // echo "<pre>"; print_r($getemailthread);echo "</pre>";
            $value->ethread = $getemailthread->ethread;
            $dispatch_data['e_id'] = "<label class='container-checkbox'><input type='checkbox' class='email-check-id $unreadcls' value='" . $value->filename . "' data-ethread='" . (($value->ethread > 1) ? $value->ethread : 0) . "' email_con_id='" . $value->email_con_id . "' data-ids = '". $i ."'><span class='checkmark'  style='padding-right: 5px;'></span></label>";

            $html = '';
            $notreadyet = '';
            if ($value->urgent == 'Y'/* && $current_list != 'sent'*/) {
                $html .= '<i class="fa fa-circle text-danger"></i>&nbsp;';
                $notreadyet = ($value->readyet != 'Y' || $getemailthread->etreadyet > 0) ? 'notreadyet' : '';
            } else if (($value->readyet != 'Y' || $getemailthread->etreadyet > 0)/* && $current_list != 'sent'*/) {
                $html .= '<i class="fa fa-circle text-navy"></i>&nbsp;';
                $notreadyet = 'notreadyet';
            } else {
                    $html .= '<i class="fa fa-circle text-warning"></i>&nbsp;';
            }
            // print_R($getemailthread);
            // print_r($value);exit;
            $ecount = ($value->ethread > 1) ? '&nbsp;(' . $value->ethread . ')' : '';

            $dispatch_data['e_from'] = "<a class='emailaddresswidth read-email $notreadyet' href='javascript:;' data-filename='" . $value->filename . "' data-e2id='" . $value->e2id . "' email_con_id='" . $value->email_con_id . "' ethread='" . (($value->ethread > 1) ? $value->ethread : 0) . "' iore='" . $value->iore . "'  data-ids = '". $i ."'>";
            if ($current_list == 'sent') {
                $user_init = $this->session->userdata('user_data')['initials'];
                $check_whoto = $this->email_model_old->check_current_user_whoto($this->session->userdata('user_data')['initials'], $value->email_con_id);
                if($check_whoto >= 1) {
                    $user_init = '';
                }
                $efrom = '';
                $sbstr = substr($value->subject,0,3);
                if($sbstr == 'RE:') {
                    $efrom = $this->session->userdata('user_data')['initials'];
                } else {
                    $email_con_people = implode(',',array_unique(explode(',',$value->email_con_people)));
                    $efrom = ($value->email_con_people != '') ? trim(preg_replace("/((?<=^)|(?<=,))\\Q$user_init\\E(,|$)/", "", $email_con_people), ",") : $value->whofrom;
                    $substr_count = substr_count($efrom, ','.$this->session->userdata('user_data')['initials']);
                    if($substr_count > 1) {
                        $efrom = substr_replace($efrom, '', -4, 4);
                    }
                    if ($efrom == '' && in_array($this->session->userdata('user_data')['initials'], explode(',', $value->email_con_people))) {
                        $efrom = $this->session->userdata('user_data')['initials'];
                    }
                }
                $dispatch_data['e_from'] .= $html . '<span class="initialwidth">'.str_replace(',', ', ', $efrom).' '.$ecount.'</span>';
                //$dispatch_data['e_from'] .= ($value->email_con_people !='') ? trim(preg_replace("/((?<=^)|(?<=,))\\Q$user_init\\E(,|$)/", "", $value->email_con_people),",") : $value->whofrom_name;
            } else {
                $dispatch_data['e_from'] .= $html .'<span class="initialwidth">'.(($value->whofrom == null) ? ucfirst(explode('@', $value->whofrom)[0]) : $value->whofrom).' '.$ecount.'</span>';
            }
            //$dispatch_data['e_from'] .= $ecount . '</a>' . '<span class="datetimealign"><span class="date">'.date('M d, Y', strtotime($value->datesent)) . '</span><span class="time">' . date('g:i A', strtotime($value->datesent)).'</span></span>'. "<br /><a class='read-email $notreadyet' href='javascript:;' data-filename='" . $value->filename . "' email_con_id='" . $value->email_con_id . "' data-e2id='" . $value->e2id . "' ethread='" . (($value->ethread > 1) ? $value->ethread : 0) . "' iore='" . $value->iore . "'>" . ucfirst($value->subject) . $attachment . '</a>';
            $dispatch_data['e_from'] .= '<span class="date">'.date('M d, Y', strtotime($value->datesent)) . ' ' . date('g:i A', strtotime($value->datesent)).'</span>'. "<br /><span class='subject'>" . $value->subject . $attachment . '</span></a>';
            
            //$dispatch_data['e_subject'] = "<a class='read-email $notreadyet' href='javascript:;' data-filename='" . $value->filename . "' email_con_id='" . $value->email_con_id . "' data-e2id='" . $value->e2id . "' ethread='" . (($value->ethread > 1) ? $value->ethread : 0) . "' iore='" . $value->iore . "'>" . ucfirst($value->subject) . $attachment . '</a>' . '<span class="date">'.date('M d, Y', strtotime($value->datesent)) . '</span><span class="time">' . date('g:i A', strtotime($value->datesent)).'</span>';

          //  $dispatch_data['e_date'] = '<span class="date">'.date('M d, Y', strtotime($value->datesent)) . '</span><span class="time">' . date('g:i A', strtotime($value->datesent)).'</span>';
            array_push($dispatch_ajax, $dispatch_data);
			
			$i++;
			
        }
        //      $totalData = $this->data['count'] = $this->email_model_old->get_all_messages_count($this->session->userdata('user_data')['initials'], $this->input->post('current_list'),'');
        /* $totalFiltered = $this->email_model_old->filter_count($this->session->userdata('user_data')['initials'], $current_list,$emailType,$caseType,$list_type); */
        $json_data = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $dispatch_ajax,
        );
        
        die(json_encode($json_data));
    }

    public function case_email_listing() {
        $caseno = $this->input->post('caseno');
        $this->data['result'] = array();
        $result = array();
        $singleFilter = '';
        /* $emails = $this->email_model_old->get_case_emails($caseno);
          echo "<pre>"; print_r($emails); exit;
          foreach ($emails as $key => $value) {
          $value->ethread = $this->email_model_old->getethreadcount($value->email_con_id,$this->session->userdata('user_data')['initials'])[0]->ethread;
          } */
        $this->data['result'] = $this->email_model_old->get_case_emails($caseno);
        // print_r($this->data['result']);
        // exit;

        if (!empty($this->data['result'])) {
            //$this->data['result'] = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'],'', $length, $start, $current_list, $singleFilter);
            foreach ($this->data['result'] as $key => $value) {
                $directory = FCPATH . '/assets/attachment/' . $value->filename;
                if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                    $this->data['result'][$key]->attachment_file = 1;
                } else {
                    $this->data['result'][$key]->attachment_file = 0;
                }
            }
        }
        die(json_encode($this->data));
    }

    public function mark_as_read() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_old->update_email_read($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        $this->data['unread_count'] = $this->email_model_old->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
        die(json_encode($this->data));
    }

    public function mark_as_old() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_old->update_email_old($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        $this->effectcounts();
        die(json_encode($this->data));
    }

    public function mark_as_urgent() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_old->update_email_urgent($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        $this->effectcounts();
        die(json_encode($this->data));
    }

    public function move_to_trash() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_old->update_email_trash($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        $this->effectcounts();
        die(json_encode($this->data));
    }

    public function get_unread_count() {
        $data = array();
        $data['count'] = $this->email_model_old->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
        $data['emails'] = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', 5, 0, 'inbox');
        //echo "<pre>"; print_r($data); exit;
        die(json_encode($data));
    }

    public function send_email() {
        $this->load->model('common_model');
        //echo "<pre>"; print_r($_POST); exit;
        $ext_emails = array();
        if ($this->input->post('ext_emails') != '') {
            $ext_emails = explode(',', $this->input->post('ext_emails'));
        }
        $res = 0;
        $subject = $this->input->post('subject');
        $flname = $this->input->post('filenameVal');
        $email_con_id = null;
        $tmp_att_files = isset($_POST['fileGlobalVariable']) ? $_POST['fileGlobalVariable'] : '';
        if($tmp_att_files != '') {
            $tmp_decoded = json_decode($tmp_att_files);
            foreach ($tmp_decoded as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $exp = explode(":", $value1);
                    $decoded[$key][$exp[0]] = $exp[1];
                }
            }
        } else {
            $_POST['files'] = '';
            foreach ($_FILES as $key1 => $value1) {
                if($_POST['files'] != '') {
                    $_POST['files'] .= ','.$value1['name'];
                } else {
                    $_POST['files'] .= $value1['name'];
                }
                $temp_att_files[$key1] = $value1;
            }
            $decoded = json_decode(json_encode($temp_att_files), true);
        }
        foreach ($decoded as $key => $value) {
            $att_files[$key] = $value;
        }
		$att_files_chk = explode(',',$_POST['files']);
		
		$whoto = $this->input->post('whoto');
        $emailtoext = '';
        $this->data['attachment_issue'] = '';
        if ($this->input->post('caseNo') != 0) {
            $caseno = $this->input->post('caseNo');
        } else {
            $caseno = '';
        }
        /* validate attahcment if any */
        /*if ($att_files != '') {
            $check_extension = array('ADE', 'ADP', 'BAT', 'CHM', 'CMD', 'COM', 'CPL', 'EXE', 'HTA', 'INS', 'ISP', 'JAR', 'JS', 'JSE', 'LIB', 'LNK', 'MDE', 'MSC', 'MSI', 'MSP', 'MST', 'NSH', 'PIF', 'SCR', 'SCT', 'SHB', 'SYS', 'VB', 'VBE', 'VBS', 'VXD', 'WSC', 'WSF', 'WSH');
            $filesize = 0;
            foreach ($att_files as $key => $value) {
                $checkspace = $value['name'];
                $checkspace = strpos($checkspace, ' ');
                $check_file = pathinfo($value['name'], PATHINFO_EXTENSION);
                //if (in_array(strtoupper($check_file), $check_extension) || $checkspace <= 0) {
                if (in_array(strtoupper($check_file), $check_extension)) {
                    $this->data['result'] = "false";
                    if ($checkspace <= 0) {
                        $this->data['attachment_issue'] = "You can't attach file with SPACE " . $value['name'];
                    } else {
                        $this->data['attachment_issue'] = "You can't attach" . $check_file . " File Extenstion";
                    }
                    die(json_encode($this->data));
                }
                $filesize += $value['size'];
            }
            if ($filesize > 20971520) {
                $this->data['result'] = "false";
                $this->data['attachment_issue'] = "You Can not upload file(s) more then 20MB";
                die(json_encode($this->data));
            }
        }*/
        /* ends here */
        if ($whoto != 'null' && !empty($whoto)) {
            if ($this->input->post('filenameVal') != "" && $this->input->post('mailType') == 'reply') {
                $whofrom = $this->session->userdata('user_data')['initials'];
                $whoto = $this->input->post('whoto');
                $subject = "RE: " . $this->input->post('subject');
                //$urgent = ($this->input->post('urgent')=='undefined') ? '' : $this->input->post('urgent') ;
                if ($this->input->post('urgent') == 'undefined') {
                    $urgent = '';
                } else if ($this->input->post('urgent') == 'on' || $this->input->post('urgent') == 'Y') {
                    $urgent = 'Y';
                } else {
                    $urgent = '';
                }
                $basic_mail = $this->email_model_old->get_single_email($whoto, $this->input->post('filenameVal'), $this->input->post('current_list'));
				$email_con_id = $basic_mail[0]->email_con_id;
                if ($email_con_id == '') {
					
                    $whoto = explode(",", $whoto);
                    $email_con_people = '';
                    if (!empty($this->input->post('ext_emails'))) {
                        $email_con_people = $this->input->post('ext_emails') . ',';
                    }
                    foreach ($whoto as $who) {
                        $email_con_people .= $who . ',';
                    }
                    $email_con_id = $this->email_conversation_model->create_email_conversation($subject, $email_con_people, $whofrom);
                }

                //$email_mail_parse_body = $this->common_functions->mimemailparse($basic_mail[0]->mail_body, '');
				
                $date_sent = date("m/d/Y h:i:s a") . "\n";
                $mailbody = $this->input->post('mailbody');
                /*$mailbody .= "----------------------------------------------------------------------\n";
                $mailbody .= "In a message dated " . $date_sent . "you wrote:" . "\n";
                $mailbody .= "<< Subj:    " . $basic_mail[0]->subject . "\n";
                $mailbody .= "Date: " . $date_sent . "\n";
                $mailbody .= "From: " . $basic_mail[0]->whofrom . " (" . $basic_mail[0]->whofrom_name . ")" . "\n";
                $mailbody .= "To: " . $basic_mail[0]->whoto . "\n";
                if ($caseno != '') {
                    $mailbody .= "Case Number : " . $caseno;
                }
                $mailbody .= "----------------------------------------------------------------------\n";
                //$mailbody .= $basic_mail[0]->mail_body . "\n";
                $mailbody .= '<pre>' . $email_mail_parse_body . "\n" . '</pre>';
                $mailbody .= " >>";*/
                $whoto = explode(",", $whoto);
                foreach ($whoto as $key2 => $who) {
                    $res = $this->email_model_old->send_email($whofrom, $who, $subject, $urgent, $mailbody, $caseno, $email_con_id, 'I');
					
                    if ($res != 0 && !empty($att_files_chk)) {
                        if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                            mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        }
                        foreach ($att_files_chk as $key => $value) {
                            if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
                            {
                                copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
                            }
                            elseif(file_exists(FCPATH.'../../tmp/'.$value)) {
                                copy(FCPATH.'../../tmp/'.$value, $path_int . $value);
                            }
                            elseif(file_exists(FCPATH . 'assets/attachment/'.$res.'/'.$value)) {
                                unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
                                unlink(FCPATH.'../../tmp/'.$value);
                            }
                            else{ echo 'File not found'; }
						}
                    }
                }
                if ($res != 0) {
                    $this->data['result'] = "true";
                } else {
                    $this->data['result'] = "false";
                }
            } else if ($this->input->post('filenameVal') != "" && $this->input->post('mailType') == 'forward') {
                $whofrom = $this->session->userdata('user_data')['initials'];
                $whoto = $this->input->post('whoto');
                $subject = "FW: " . $this->input->post('subject');
                //$urgent = $this->input->post('urgent');
                // $urgent = ($this->input->post('urgent')=='undefined') ? '' : $this->input->post('urgent');
                if ($this->input->post('urgent') == 'undefined') {
                    $urgent = '';
                } else if ($this->input->post('urgent') == 'on' || $this->input->post('urgent') == 'Y') {
                    $urgent = 'Y';
                } else {
                    $urgent = '';
                }

                $basic_mail = $this->email_model_old->get_single_email($whofrom, $this->input->post('filenameVal'), $this->input->post('current_list'));
                //echo "<pre>";print_r($basic_mail);exit;
                /*$email_con_id = $basic_mail[0]->email_con_id;*/
                //$email_mail_parse_body = $this->common_functions->mimemailparse($basic_mail[0]->mail_body,'');
                /*if ($email_con_id == '') {*/
                    $whoto_emails = array();
                    $ext_emails = array();
                    $email_con_people = '';

                    $whoto_emails = explode(",", $whoto);
                    if (!empty($this->input->post('ext_emails'))) {
                        $ext_emails = explode(",", $this->input->post('ext_emails'));
                    }
                    $email_con_people_array = array_merge($whoto_emails, $ext_emails);
                    
                    $email_con_people = implode(",", array_map("trim",array_filter($email_con_people_array)));

                    $whoto = explode(",", $whoto);
                    // $email_con_people = '';
                    // if (!empty($this->input->post('ext_emails'))) {
                    //     $email_con_people = $this->input->post('ext_emails') . ',';
                    // }
                    // if(sizeof($whoto) > 1) {
                    //     foreach ($whoto as $key => $who) {
                    //         $email_con_people .= $who;
                    //         if($key == sizeof($whoto) - 1) {
                    //             $email_con_people .= ',';
                    //         }
                    //     }
                    // } else {
                    //     $email_con_people .= $whoto[0];
                    // }
                    $email_con_id = $this->email_conversation_model->create_email_conversation($subject, $email_con_people, $whofrom);
                /*}*/
                $date_sent = date("m/d/Y h:i:s a") . "\n";
                $mailbody = $this->input->post('mailbody');
                /*$mailbody .= "***********************************************************************\n";
                $mailbody .= "                      Mail Being Forwarded To You                      \n";
                $mailbody .= "Subj:    " . $basic_mail[0]->subject . "\n";
                $mailbody .= "Date: " . $date_sent . "\n";
                $mailbody .= "From: " . $basic_mail[0]->whoto . " (" . $basic_mail[0]->whoto_name . ")" . "\n";
                $mailbody .= "To: " . $basic_mail[0]->whofrom . "\n";
                if ($caseno != '') {
                    $mailbody .= "Case Number : " . $caseno;
                }
                $mailbody .= "-----------------------------------------------------------------------\n";*/
                $path_int = '';

                foreach ($whoto as $key2 => $who) {
                    $res = $this->email_model_old->send_email($whofrom, $who, $subject, $urgent, $mailbody, $caseno, $email_con_id, 'I');
                    if ($res != 0) {
                        if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                            mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        }
						foreach ($att_files_chk as $key => $value) {
								if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
								{
									copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
                                    if(count($ext_emails) == 0) {
                                        if($key2 == count($whoto) - 1) {
    									   unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
                                        }
                                    }
								}
								else{ echo 'File not found'; }
						}
                    }
                }
                if ($res != 0) {
                    $this->data['result'] = "true";
                } else {
                    $this->data['result'] = "false";
                }
                //die(json_encode($this->data));
            } else {
			
                $whofrom = $this->session->userdata('user_data')['initials'];
                $whoto = $this->input->post('whoto');
                $subject = $this->input->post('subject');
                //$urgent = ($this->input->post('urgent')=='undefined') ? '' : $this->input->post('urgent') ;
                if ($this->input->post('urgent') == 'undefined') {
                    $urgent = '';
                } else if ($this->input->post('urgent') == 'on' || $this->input->post('urgent') == 'Y') {
                    $urgent = 'Y';
                } else {
                    $urgent = '';
                }
                $mailbody=$this->input->post('mailbody');
                if ($caseno != '') {
                    $case_info = '';
                    $result_case_info = $this->case_model->get_party_info($caseno);
                    if($result_case_info->firm != '') {
                        $case_info = $result_case_info->firm;
                    } else {
                        $case_info = $result_case_info->first.' '.$result_case_info->last;
                    }
                    $mailbody .= "Case Number : " . $caseno . " " . $case_info;
                }
                if ($email_con_id == '') {
                    $whoto = explode(",", $whoto);
                    $email_con_people = '';
                    if (!empty($this->input->post('ext_emails'))) {
                        $email_con_people = $this->input->post('ext_emails') . ',';
                    }
                    foreach ($whoto as $who) {
                        $email_con_people .= $who . ',';
                    }
                    $email_con_people .= $whofrom;
                    $email_con_id = $this->email_conversation_model->create_email_conversation($subject, $email_con_people, $whofrom);

                    foreach ($whoto as $key1 => $who) {
                        $res = $this->email_model_old->send_email($whofrom, $who, $subject, $urgent, $mailbody, $caseno, $email_con_id, 'I');
                        if ($res != 0) {
							
                            $path_int = '';
                            if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                                mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                                $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                            } else {
                                $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                            }
                            foreach ($att_files_chk as $key => $value) {
								if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
								{
									copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
								}
                                elseif(file_exists(FCPATH.'../../tmp/'.$value)) {
                                    copy(FCPATH.'../../tmp/'.$value, $path_int . $value);
                                }
                                elseif(file_exists(FCPATH . 'assets/attachment/'.$res.'/'.$value)) {
                                    unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
                                    unlink(FCPATH.'../../tmp/'.$value);
                                }
								else{ echo 'File not found'; }
                            } 
                        }
                    }
                }
                //$email_con_id = $this->email_conversation_model->create_email_conversation($subject);
            }
        } else {

            $whofrom = $this->session->userdata('user_data')['initials'];
            $whoto = $this->input->post('whoto');
            $subject = $this->input->post('subject');
            //$urgent = ($this->input->post('urgent')=='undefined') ? '' : $this->input->post('urgent') ;
            if ($this->input->post('urgent') == 'undefined') {
                $urgent = '';
            } else if ($this->input->post('urgent') == 'on' || $this->input->post('urgent') == 'Y') {
                $urgent = 'Y';
            } else {
                $urgent = '';
            }
            $mailbody = $this->input->post('mailbody');
            if (!empty($this->input->post('ext_emails'))) {
                $email_con_people = $this->input->post('ext_emails');
            }
            $email_con_people .= ',' . $whofrom;
            $email_con_id = $this->email_conversation_model->create_email_conversation($subject, $email_con_people, $whofrom);
            if ($whoto != 'null') {
                $whoto = explode(",", $whoto);
                foreach ($whoto as $who) {
                    $res = $this->email_model_old->send_email($whofrom, $who, $subject, $urgent, $mailbody, $caseno, $email_con_id, 'I');
                    if ($res != 0) {
                        $path_int = '';
                        if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                            mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        } else {
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        }
                        foreach ($att_files_chk as $key => $value) {
							if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
                            {
                                copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
                            }
                            elseif(file_exists(FCPATH.'../../tmp/'.$value)) {
                                copy(FCPATH.'../../tmp/'.$value, $path_int . $value);
                            }
                            elseif(file_exists(FCPATH . 'assets/attachment/'.$res.'/'.$value)) {
                                unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
                                unlink(FCPATH.'../../tmp/'.$value);
                            }
                            else{ echo 'File not found'; }
						}
                    }
                }
            }
        }
        if ($res != 0) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        if (!empty($whoto)) {
            foreach ($whoto as $who) {
                $emailtoext .= $who . ', ';
            }
        }
        if (is_array($ext_emails) && count($ext_emails) > 1) {
            $emailtoext .= implode(", ", $ext_emails) . ' ';
        } else if(is_array($ext_emails) && count($ext_emails) == 1) {
            $emailtoext .= $ext_emails[0] . ', ';
        }

        /* adding the logic of sending emails to the external parties */
        if(isset($_POST['caseCategory']) && $_POST['caseCategory'] != '') {
            $event_category = $_POST['caseCategory'];
        } else {
            $event_category = 26;
        }
        $event_color = 1;
        if (!empty($ext_emails)) {
            $event_color = 1;
            $number_of_internal_party = 0;
            $number_of_internal_party = count($whoto);
            /*if (count($whoto) > 1) {
                $mailbody .= "<pre><hr><p>There are $number_of_internal_party internal users in loop for this communication and in case you want to send email to individual user , please use their email address as below</p>";
                $mailbody .= "<ul>";

                foreach ($whoto as $who) {
                    if ($email_con_id != 0 && $caseno != 0) {
                        $mailbody .= "<li>" . $who . '-' . $email_con_id . '-' . $caseno . "-@" . LAW_PUBLIC_DOMAIN . "</li>";
                    } elseif ($email_con_id != 0 && $caseno == '') {
                        $mailbody .= "<li>" . $who . '-' . $email_con_id . "-@" . LAW_PUBLIC_DOMAIN . "</li>";
                    }
                }
                $mailbody .= "</ul>";
                if ($email_con_id != 0 && $caseno != 0) {
                    $mailbody .= "Email Initiated By : " . $this->session->userdata('user_data')['initials'] . '-' . $email_con_id . '-' . $caseno . "-@" . LAW_PUBLIC_DOMAIN;
                } elseif ($email_con_id != 0 && $caseno == '') {
                    $mailbody .= "Email Initiated By : " . $this->session->userdata('user_data')['initials'] . '-' . $email_con_id . "-@" . LAW_PUBLIC_DOMAIN;
                }
                $mailbody .= "</pre>";
            } else {*/
                if ($caseno != '' && $whoto == 'null') {
                    $case_info = '';
                    $result_case_info = $this->case_model->get_party_info($caseno);
                    if($result_case_info->firm != '') {
                        $case_info = $result_case_info->firm;
                    } else {
                        $case_info = $result_case_info->first.' '.$result_case_info->last;
                    }
                    $mailbody .= "Case Number : " . $caseno . " " . $case_info;
                }
            /*}*/
            $whofrom = $this->session->userdata('user_data')['initials'];
            $date_sent = date("m/d/Y h:i:s a") . "\n";

            $list_file_to_att = array();

            $init_name = array('initials' => $whofrom);
            $whofrom_e_name = $this->common_model->getSingleRecordById('staff', 'CONCAT(fname," ",lname) whofrom_name', $init_name)->whofrom_name;
            if (count($ext_emails) > 1) {
                foreach ($ext_emails as $key => $value) {
                    $res = $this->email_model_old->send_email($whofrom, $value, $subject, $urgent, $mailbody, $caseno, $email_con_id, 'E');
                    if ($res != 0) {
                        $path_int = '';
                        if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                            mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        } else {
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        }
                        foreach ($att_files_chk as $key1 => $value1) {
                            if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value1))
                            {
                                copy(FCPATH . 'assets/attachment/tempfolder/'.$value1, $path_int . $value1);
                            }
                            elseif(file_exists(FCPATH.'../../tmp/'.$value1)) {
                                copy(FCPATH.'../../tmp/'.$value1, $path_int . $value1);
                            }
                            elseif(file_exists(FCPATH . 'assets/attachment/'.$res.'/'.$value1)) {
                                unlink(FCPATH . 'assets/attachment/tempfolder/'.$value1);
                                unlink(FCPATH.'../../tmp/'.$value1);
                            }
                            else{ echo 'File not found'; }
                        }
                    }
                }
            } else {
                $res = $this->email_model_old->send_email($whofrom, $ext_emails[0], $subject, $urgent, $mailbody, $caseno, $email_con_id, 'E');
                if ($res != 0) {
                    $path_int = '';
                    if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                        mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                        $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                    } else {
                        $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                    }
                    $prev_res = $res - 1;
                    foreach ($att_files_chk as $key => $value) {
                        if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
                        {
                            copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH.'../../tmp/'.$value)) {
                            copy(FCPATH.'../../tmp/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH . 'assets/attachment/'.$res.'/'.$value)) {
                            unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
                            unlink(FCPATH.'../../tmp/'.$value);
                        }
                        else{ echo 'File not found'; }
                    }
                }
            }
            $status = $this->common_functions->ext_send($ext_emails, $subject, $whofrom, $mailbody, $att_files, $caseno, $email_con_id, $urgent, $whofrom_e_name, $res);
            /*if ($status == 202) {*/
                /*if (count($ext_emails) > 1) {
                    foreach ($ext_emails as $key => $value) {
                        $res = $this->email_model_old->send_email($whofrom, $value, $subject, $urgent, $mailbody, $caseno, $email_con_id, 'E');
                        if ($res != 0) {
                            $path_int = '';
                            if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                                mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                                $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                            } else {
                                $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                            }
                            foreach ($att_files_chk as $key => $value) {
								if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
								{
									copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
									unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
								}
								else{ echo 'File not found'; }
							}
                        }
                    }
                } else {
                    $res = $this->email_model_old->send_email($whofrom, $ext_emails[0], $subject, $urgent, $mailbody, $caseno, $email_con_id, 'E');
                    if ($res != 0) {
                        $path_int = '';
                        if (!is_dir(FCPATH . 'assets/attachment/' . $res)) {
                            mkdir(FCPATH . 'assets/attachment/' . $res, 0777);
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        } else {
                            $path_int = FCPATH . 'assets/attachment/' . $res . '/';
                        }
                        foreach ($att_files_chk as $key => $value) {
								if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
								{
									copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
									unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
								}
								else{ echo 'File not found'; }
							}
                    }
                }*/

                $data['ext_party'] = '1';
                $this->data['result'] = "true";
            /*} else {
                $data['ext_party'] = '0';
                $this->data['result'] = "false";
            }*/
        } /* external parties email ends here */
        $latest_filename = $this->email_model_old->get_filename($email_con_id);
        $record = array('caseno' => $caseno, 'initials0' => $this->session->userdata('user_data')['initials'], 'initials' => $this->session->userdata('user_data')['initials'], 'date' => date('Y-m-d H:i:s'), 'category' => $event_category, 'mainnotes' => 0, 'event' => 'Email to : ' . trim($emailtoext, ", ").'<br>'.strip_tags($mailbody), 'color' => $event_color, 'email_con_id' => $latest_filename);
        $r = $this->common_functions->insertCaseActRecord($record);
        die(json_encode($this->data));
    }

    public function reply_email() {
        $this->data['emails'] = $this->email_model_old->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'), $this->input->post('iore'));
        //echo "<pre>"; print_r($this->data); exit;
        die(json_encode($this->data));
    }

    public function forward_email() {
        $this->data['emails'] = $this->email_model_old->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'), $this->input->post('iore'));

        if (!empty($this->data['emails'][0]->mail_body) && $this->data['emails'][0]->iore == 'E') {
            $parse_mail = $this->common_functions->mimemailparse($this->data['emails'][0]->mail_body, $this->input->post('filename'));
            $this->data['emails'][0]->mail_body = ($parse_mail['text'] != false) ? $parse_mail['text'] : $this->data['emails'][0]->mail_body;
            $who_form_name = explode('<', $parse_mail['from']);
            $this->data['emails'][0]->whofrom_name = $who_form_name[0];
        }
        $directory = FCPATH . '/assets/attachment/' . $this->input->post('filename');
        $link = base_url() . 'assets/attachment/' . $this->input->post('filename') . '/';
        $removelink = 'assets/attachment/' . $this->input->post('filename') . '/';
        $file_data = '';
        $fwd_files = array();
        if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
            $files = glob($directory . "/*");
            $file_data .= '<h4>Attachment</h4><ul>';
            foreach ($files as $value) {
                $file_name = basename($value);
                $file_data .= "<li><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'>" . $file_name . "</a>  <i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i></li>";
                array_push($fwd_files, $file_name);
            }
            $file_data .= '</ul>';
        }
        if (!empty($file_data)) {
            $this->data['file_with_attachment'] = $file_data;
            $this->data['fwd_files'] = $fwd_files;
        } else {
            $this->data['file_with_attachment'] = '';
        }

        die(json_encode($this->data));
    }

    public function emailimgupload() {
        
    }

    public function printEmail() {

        // extract($_POST);
        $this->data['emails'] = $this->email_model_old->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'), $this->input->post('iore'))[0];
        if (!empty($this->data['emails']->mail_body) && $this->data['emails']->iore == 'E') {
            $parse_mail = $this->common_functions->mimemailparse($this->data['emails']->mail_body, $this->input->post('filename'));
            $this->data['emails']->mail_body = $parse_mail['text'];
            $who_form_name = explode('<', $parse_mail['from']);
            $this->data['emails']->whofrom_name = $who_form_name[0];
        }
        //print_r($this->data['emails']);exit;
        $this->load->view('email_new/print_emaildetails', $this->data);
    }

    /* this function will receive response as inbound parsing email from sendgrid */

    public function inboundemails() {

        $this->load->library('sendgrid/SendgridParse');
        $parsed = new SendgridParse();
        $urgent = '';
        $caseno = 0;
        $case_no = 0;
        $caseact_parties = '';
        $email_con_id = null;
        $to = explode('-', $parsed->to[0]['email']);
        if (preg_match('#[0-9]#', $to[0])) {
            $email_con_id = $to[0];
            $case_no = $to[1];

            if(ctype_digit($to[1])){
                $_to = $to[2];
            }else{
                $_to = $to[1];
            }
            
            //$whoto = $parsed->to[0]['email'];
            $whoto = substr($_to, 0, strpos($_to, "@")); ///$this->email_conversation_model->grab_conversation_from_id($email_con_id)[0]->email_con_people;
            //$whoto = $this->email_conversation_model->grab_conversation_from_id($email_con_id)[0]->email_con_people;
            $whoto = rtrim($whoto, ',');
            $whoto = explode(',', $whoto);
        } else {
            /*$email_con_id = (!empty($to[1])) ? $to[1] : '';
            $case_no = (!empty($to[2])) ? $to[2] : 0;
            $whoto = strtoupper($to[0]);
            if($email_con_id == '') {
                $whoto = explode('@', $whoto);
                $email_con_people = $whoto[0];
                unset($whoto[1]);
                $email_con_id = $this->email_conversation_model->create_email_conversation($parsed->subject, $email_con_people, $parsed->from['email']);;
            }*/
            $email_con_id = (!empty($to[1])) ? $to[1] : '';
            $case_no = (!empty($to[2])) ? $to[2] : 0;
            $whoto = strtoupper($to[0]);
        }
        if (empty($to)) {
            $email_con_id = '';
            $case_no = 0;
            $whoto = strtoupper(substr($parsed->to[0]['email'], 3));
        }
        if ($case_no != 0 && is_numeric($case_no)) {
            $case_no = $case_no;
        } else {
            $case_no = 0;
        }
        $whofrom = $parsed->from['email'];
        if (!empty($parsed->post['email'])) {
            $mail_body = json_encode($parsed->post['email']);
            if (is_array($whoto)) {
                foreach ($whoto as $key => $whoval) {
                    if (!preg_match("/\b@\b/i", $whoval)) {
                        $this->email_model_old->send_email($whofrom, $whoval, $parsed->subject, 'N', $mail_body, $case_no, $email_con_id, 'E');
                        $caseact_parties .= $whoval . ',';
                    }
                }
                $caseact_parties = rtrim($caseact_parties, ',');
            } else {
                $this->email_model_old->send_email($whofrom, $whoto, $parsed->subject, 'N', $mail_body, $case_no, $email_con_id, 'E');
                $caseact_parties = $whoto;
            }
        }
        if ($case_no != 0) {
            $latest_filename = $this->email_model_old->get_filename($email_con_id);
            $event_category = 26;
            $event_color = 2;
            $get_parse_mail = $this->common_functions->mimemailparse( $mail_body, $new_parcel_id.'_'.strtoupper($whoto[0]));
            $mailBody = str_replace("\n","<br>",$get_parse_mail['text']);
            $ext_to = "Email received from : $whofrom  To : $caseact_parties"."<br/>".$mailBody;
            $record = array('caseno' => $case_no, 'initials0' => $this->session->userdata('user_data')['initials'], 'initials' => $this->session->userdata('user_data')['initials'], 'date' => date('Y-m-d H:i:s'), 'color' => $event_color, 'category' => $event_category, 'mainnotes' => 0, 'event' => $ext_to, 'email_con_id' => $latest_filename);
            $this->common_functions->insertCaseActRecord($record);
        }
        exit;
    }

    /* inbound email parsing ends here */
    /* getting email with threads */

    public function emailThreads() {
		
        $current_list = $this->input->post('current_list');
		$threadcount = $this->input->post('thread');
        $filename = $this->input->post('filename');
        $email_con_id = $this->input->post('email_con_id');
        $current_from = $this->input->post('vfrom');
		$ids = $this->input->post('ids');
        $current_user = $this->session->userdata('user_data')['initials'];

        $received_email_threads = $this->email_model_old->get_email_conversation($email_con_id, $current_user, $current_from, '', DEFAULT_LISTING_LENGTH, 0, $current_list);
        $data = '';
        $cspan = ($current_from == 'email_details') ? '4' : '3';
        $data .= "<tr class='read rclose' id='".$email_con_id."'><td colspan='" . $cspan . "'><table style='border: 1px solid #e7eaec;width:100%;'>";
        // print_r($received_email_threads);
        foreach ($received_email_threads as $tmp_key => $email) {
            if($email->iore == 'I' && !in_array($current_user, explode(',', $email->whoto)) && $email->whofrom != $current_user) {
                continue;
            }
            if($email->detached == 1) {
//                continue;
            }
            $ruclass = ($email->readyet == 'Y') ? 'read' : 'unread';
            $notreadyet = ($email->readyet != 'Y') ? 'notreadyet' : '';
            $datafilename = $email->filename;
            $email_con_id = $email->email_con_id;
            $iore = $email->iore;
            $date = date('M d, Y g:i A', strtotime($email->datesent));
            
            if($current_list == 'sent') {
                $tmp_all_subjects = $this->db->query("select DISTINCT email.subject from email where email.email_con_id='".$email_con_id."' order by email.filename asc")->result();
                // echo "<br>111".$this->db->last_query();
                $tmp_all_whoto = array();
                foreach ($tmp_all_subjects as $key => $value) {
                    $tmp_all_whoto[$key] = $this->db->query("select whoto from email2 left join email on email2.filename = email.filename where email.subject LIKE '".$value->subject."' and email.email_con_id = '".$email_con_id."' order by email.filename asc")->result();
                    // echo "<br>222".$this->db->last_query();
                }
                $new_whoto = array();
                // print_r($tmp_all_whoto);
                foreach ($tmp_all_whoto as $key => $value) {
                    if(count($value) > 1) {
                        $tempNewWhoTo = '';
                        foreach($value as $k => $v) {
                            $tempNewWhoTo .= $v->whoto . ',';    
                        }
                        $new_whoto[$key] = trim($tempNewWhoTo , ',');
                    } else {
                        $new_whoto[$key] = $value[0]->whoto;
                    }
                }
                // print_r($new_whoto);exit;
                $email->whoto = $new_whoto[$tmp_key];
                $whofromname = (isset($email->whoto) ? $email->whoto : strstr($email->whoto, '@', true));
            } else {
                // echo 'whofrom =>' . $whofromname;
                $whofromname = (isset($email->whofrom) ? $email->whofrom : strstr($email->whofrom, '@', true));
            }
            $directory = FCPATH . '/assets/attachment/' . $datafilename;
            $attachment = '';

            if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                $attachment = '<span class="pull-right"><i class="fa fa-paperclip"></i></span>';
            }

            $html = '';
            $notreadyet = '';

            if ($email->urgent == 'Y') {
                $html .= '<i class="fa fa-circle text-danger"></i>&nbsp;';
                $notreadyet = ($email->readyet != 'Y') ? 'notreadyet' : '';
            } else if ($email->readyet != 'Y') {
                $html .= '<i class="fa fa-circle text-navy"></i>&nbsp;';
                $notreadyet = 'notreadyet';
            } else {
                $html .= '<i class="fa fa-circle text-warning"></i>&nbsp;';
            }

            $data .= "<tr>";

            if ($current_from == 'email_details') {
                $data .= "<td style='width: 5%;' class='check-mail'><label class='container-checkbox'><input type='checkbox' value='$datafilename' data-ethread='0' email_con_id='" . $email_con_id . "' class='childThread' data-ids='".$ids."' eethread='".$threadcount."'><span class='checkmark'  style='padding-right: 5px;'></span></label></td>";
            }
            $data .= '<td style="width: 29.5%;" class="mail-ontact sorting_1">';
            $data .= "<a class='emailaddresswidth read-email $notreadyet' href='javascript:;' data-filename='$datafilename' eethread='".$threadcount."' data-ids='".$ids."' email_con_id='$email_con_id' ethread='' iore='".$iore."'>$html <span class='initialwidth'> $whofromname</span> <br /><span class='subject'>" . $email->subject . "<span class='pull-right'>$attachment</span></a><span class='date'>" .$date."</span></a></td>";
            //$data .= "<td style='width: 48.3%;' class='mail-subject'><a class='read-email $notreadyet' href='javascript:;' data-filename='$datafilename' email_con_id='' ethread='' iore='I'>" . ucfirst($email->subject) . " <span class='pull-right'>$attachment</span></a></td>";
            //$data .= "<td style='width: 20%;' class=' mail-date'>$date</td></tr>";
        }

        $data .= "</table></td></tr>";
        $data .= "</div></td></tr>";
        die(json_encode($data));
    }
	
    public function caseemailThreads() {
        $current_list = $this->input->post('current_list');
        $filename = $this->input->post('filename');
        $email_con_id = $this->input->post('email_con_id');
        $current_from = $this->input->post('vfrom');
        $current_user = $this->session->userdata('user_data')['initials'];

        $received_email_threads = $this->email_model_old->get_case_email_conversation($email_con_id, $current_user, $current_from);

        $data = '';
        $cspan = ($current_from == 'email_details') ? '4' : '3';
        $data .= "<tr class='read rclose' id='".$email->email_con_id."'><td colspan='" . $cspan . "'><table style='border: 1px solid #e7eaec;width:100%;'>";

        foreach ($received_email_threads as $email) {
            $ruclass = ($email->readyet == 'Y') ? 'read' : 'unread';
            $notreadyet = ($email->readyet != 'Y') ? 'notreadyet' : '';
            $datafilename = $email->filename;
            $email_con_id = $email->email_con_id;
            $iore = $email->iore;
            $date = date('M d, Y g:i A', strtotime($email->datesent));
            
            /*$whofromname = (isset($email->sendwhoto) ? $email->sendwhoto : strstr($email->sendwhoto, '@', true));*/
            $whofromname = $email->whofrom;
            $directory = FCPATH . '/assets/attachment/' . $datafilename;
            $attachment = '';

            if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                $attachment = '<span class="pull-right"><i class="fa fa-paperclip"></i></span>';
            }

            $html = '';
            $notreadyet = '';

            if ($email->urgent == 'Y') {
                $html .= '<i class="fa fa-circle text-danger"></i>&nbsp;';
                $notreadyet = ($email->readyet != 'Y') ? 'notreadyet' : '';
            } else if ($email->readyet != 'Y') {
                $html .= '<i class="fa fa-circle text-navy"></i>&nbsp;';
                $notreadyet = 'notreadyet';
            } else {
                $html .= '<i class="fa fa-circle text-warning"></i>&nbsp;';
            }

            $data .= "<tr>";

            if ($current_from == 'email_details') {
                $data .= "<td style='width: 5%;' class='check-mail'><label class='container-checkbox'><input type='checkbox' value='$datafilename' data-ethread='0' email_con_id='" . $email_con_id . "'><span class='checkmark'  style='padding-right: 5px;'></span></label></td>";
            }
            $data .= '<td style="width: 29.5%;" class="mail-ontact sorting_1">';
            $data .= "$html<a class='read-email $notreadyet' href='javascript:;' data-filename='$datafilename' email_con_id='$email_con_id' ethread='' iore='".$iore."'>$whofromname</a></td>";
            $data .= "<td style='width: 48.3%;' class='mail-subject'><a class='read-email $notreadyet' href='javascript:;' data-filename='$datafilename' email_con_id='' ethread='' iore='".$iore."'>" . $email->subject . " <span class='pull-right'>$attachment</span></a></td>";
            $data .= "<td style='width: 20%;' class=' mail-date'>$date</td></tr>";
        }

        $data .= "</table></td></tr>";
        $data .= "</div></td></tr>";
        die(json_encode($data));
    }
    
    public function reply_all_email() {
        $this->data['emails'] = $this->email_model_old->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'), $this->input->post('iore'));
        //echo "<pre>"; print_r($this->data); exit;
        $members = $this->email_conversation_model->grab_conversation_from_id($this->input->post('email_con_id'));
        $members = rtrim($members[0]->email_con_people, ',');
        $members = explode(',', $members);
        $external_party = '';
        $internal_party = array();
        foreach ($members as $key => $value) {
            if (strlen($value) > 3) {
                $external_party .= $value . ',';
            } else {
                if ($this->session->userdata('user_data')['initials'] != $value) {
                    array_push($internal_party, $value);
                }
            }
        }
        array_push($internal_party, $this->data['emails'][0]->whofrom);
        $this->data['external_party'] = rtrim($external_party, ',');
        $this->data['internal_party'] = $internal_party;
        die(json_encode($this->data));
    }

    public function checkUnreadEmails() {
        $data = array();
        $data['ucount'] = $this->email_model_old->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
        $data['mail_category'] = $this->email_model_old->get_type_of_last_email($this->session->userdata('user_data')['initials'], 'unread');
        die(json_encode($data));
    }

    public function removeattachment() {
        $remove_file = FCPATH . $this->input->post('remove_file');
        $this->data['status'] = (unlink($remove_file)) ? true : false;
        die(json_encode($this->data));
    }

    public function generateconversion() {
        $allemails = $this->email_model_old->selectallemails('*', "email_con_id = ''");
        echo "<pre>";
        print_r($allemails);
        exit;
    }

    public function old_email_listing() {
        $current_list = ($this->input->post('current_list') != '') ? $this->input->post('current_list') : 'read';
        $emailType = $this->input->post('emailType');
        if ($this->input->post('caseType') != '') {
            $caseType = $this->input->post('caseType');
        } else {
            $caseType = '';
        }

        $start = ($this->input->post('start') != 0) ? $this->input->post('start') : 0;
        $length = ($this->input->post('length') != 0) ? $this->input->post('length') : DEFAULT_LISTING_LENGTH;

        $this->data['result'] = array();
        $result = array();
        $singleFilter = '';
        if ($this->input->post('search_data') != '') {
            $singleFilter = '(e.subject LIKE "%' . $this->input->post('search_data') . '%" OR CONCAT(s1.fname, " ", s1.lname) LIKE "%' . $this->input->post('search_data') . '%" OR e.whofrom LIKE "%' . $this->input->post('search_data') . '%" OR e2.whoto LIKE "%' . $this->input->post('search_data') . '%")';
        }
        if ($current_list == 'sent') {
            $singleFilter = '(e.subject LIKE "%' . $this->input->post('search_data') . '%" OR CONCAT(s2.fname, " ", s2.lname) LIKE "%' . $this->input->post('search_data') . '%" OR e.whofrom LIKE "%' . $this->input->post('search_data') . '%" OR e2.whoto LIKE "%' . $this->input->post('search_data') . '%")';
            $this->data['result'] = $this->email_model_old->get_sent_emails($this->session->userdata('user_data')['initials'], $length, $start, $singleFilter);
        } else {
            $defaultFilter = array('e2.oldmail' => '1');
            $emails = $this->email_model_old->get_received_emails2($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType);
            foreach ($emails as $key => $value) {
                $value->ethread = $this->email_model_old->getethreadcount($value->email_con_id, $this->session->userdata('user_data')['initials'])[0]->ethread;
            }
            $this->data['result'] = $emails;
        }
        if (!empty($this->data['result'])) {
            foreach ($this->data['result'] as $key => $value) {
                $directory = FCPATH . '/assets/attachment/' . $value->filename;
                if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                    $this->data['result'][$key]->attachment_file = 1;
                } else {
                    $this->data['result'][$key]->attachment_file = 0;
                }
            }
        }

        $this->effectcounts();
        die(json_encode($this->data));
    }

    public function new_email_listing() {
        $current_list = ($this->input->post('current_list') != '') ? $this->input->post('current_list') : 'inbox';
        if ($this->input->post('emailType') != '') {
            $emailType = $this->input->post('emailType');
        } else {
            $emailType = '';
        }

        if ($this->input->post('caseType') != '') {
            $caseType = $this->input->post('caseType');
        } else {
            $caseType = '';
        }

        $start = ($this->input->post('start') != 0) ? $this->input->post('start') : 0;
        $length = ($this->input->post('length') != 0) ? $this->input->post('length') : DEFAULT_LISTING_LENGTH;

        $this->data['result'] = array();
        $result = array();
        $singleFilter = '';
        if ($this->input->post('search_data') != '') {
            $singleFilter = '(e.subject LIKE "%' . $this->input->post('search_data') . '%" OR CONCAT(s1.fname, " ", s1.lname) LIKE "%' . $this->input->post('search_data') . '%" OR e.whofrom LIKE "%' . $this->input->post('search_data') . '%" OR e2.whoto LIKE "%' . $this->input->post('search_data') . '%")';
        }
        if ($current_list == 'sent') {
            $singleFilter = '(e.subject LIKE "%' . $this->input->post('search_data') . '%" OR CONCAT(s2.fname, " ", s2.lname) LIKE "%' . $this->input->post('search_data') . '%" OR e.whofrom LIKE "%' . $this->input->post('search_data') . '%" OR e2.whoto LIKE "%' . $this->input->post('search_data') . '%")';
            $this->data['result'] = $this->email_model_old->get_sent_emails($this->session->userdata('user_data')['initials'], $length, $start, $singleFilter);
        } else {
            $defaultFilter = '';
            $emails = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType);
            foreach ($emails as $key => $value) {
                $value->ethread = $this->email_model_old->getethreadcount($value->email_con_id, $this->session->userdata('user_data')['initials'])[0]->ethread;
            }
            $this->data['result'] = $emails;

            // $this->data['result'] = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'],'', $length, $start, $current_list, $singleFilter);
        }
        if (!empty($this->data['result'])) {
            //$this->data['result'] = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'],'', $length, $start, $current_list, $singleFilter);
            foreach ($this->data['result'] as $key => $value) {
                $directory = FCPATH . '/assets/attachment/' . $value->filename;
                if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                    $this->data['result'][$key]->attachment_file = 1;
                } else {
                    $this->data['result'][$key]->attachment_file = 0;
                }
            }
        }
        /* if (empty($this->data['result'])){
          $this->data['result'] = array('No Data found.');
          } */
        $this->effectcounts();
        die(json_encode($this->data));
    }

    public function seacrh_email() {
        //$current_list = ($this->input->post('current_list') != '') ? $this->input->post('current_list') : 'inbox';
        $current_list = $this->input->post('current_list');
        if ($this->input->post('emailType') != '') {
            $emailType = $this->input->post('emailType');
        } else {
            $emailType = '';
        }

        if ($this->input->post('caseType') != '') {
            $caseType = $this->input->post('caseType');
        } else {
            $caseType = '';
        }

        $start = ($this->input->post('start') != 0) ? $this->input->post('start') : 0;
        $length = ($this->input->post('length') != 0) ? $this->input->post('length') : DEFAULT_LISTING_LENGTH;

        $this->data['result'] = array();
        $result = array();
        $singleFilter = '';
        if ($this->input->post('search_data') != '') {
            $singleFilter = '(e.subject LIKE "%' . $this->input->post('search_data') . '%" OR CONCAT(s1.fname, " ", s1.lname) LIKE "%' . $this->input->post('search_data') . '%" OR e.whofrom LIKE "%' . $this->input->post('search_data') . '%" OR e2.whoto LIKE "%' . $this->input->post('search_data') . '%")';
        }
        if ($current_list == 'sent') {
            $singleFilter = '(e.subject LIKE "%' . $this->input->post('search_data') . '%" OR CONCAT(s2.fname, " ", s2.lname) LIKE "%' . $this->input->post('search_data') . '%" OR e.whofrom LIKE "%' . $this->input->post('search_data') . '%" OR e2.whoto LIKE "%' . $this->input->post('search_data') . '%")';
            $this->data['result'] = $this->email_model_old->get_sent_emails($this->session->userdata('user_data')['initials'], $length, $start, $singleFilter);
        } else {
            if ($current_list == 'new') {
                $defaultFilter = array('e2.oldmail' => '0');
                $emails = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType);
            }
            if ($current_list == 'old') {
                $defaultFilter = array('e2.oldmail' => '1');
                $emails = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'], '', $length, $start, $current_list, $defaultFilter, $emailType, $caseType);
            }

            foreach ($emails as $key => $value) {
                $value->ethread = $this->email_model_old->getethreadcount($value->email_con_id, $this->session->userdata('user_data')['initials'])[0]->ethread;
            }
            $this->data['result'] = $emails;

            // $this->data['result'] = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'],'', $length, $start, $current_list, $singleFilter);
        }
        if (!empty($this->data['result'])) {
            //$this->data['result'] = $this->email_model_old->get_received_emails($this->session->userdata('user_data')['initials'],'', $length, $start, $current_list, $singleFilter);
            foreach ($this->data['result'] as $key => $value) {
                $directory = FCPATH . '/assets/attachment/' . $value->filename;
                if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                    $this->data['result'][$key]->attachment_file = 1;
                } else {
                    $this->data['result'][$key]->attachment_file = 0;
                }
            }
        }
        /* if (empty($this->data['result'])){
          $this->data['result'] = array('No Data found.');
          } */
        $this->effectcounts();
        die(json_encode($this->data));
    }

    public function effectcounts() {
        $this->data['withcno_count'] = 0; //$this->email_model_old->get_all_messages_count($this->session->userdata('user_data')['initials'], 'withcase');
        $this->data['withoutcno_count'] = 0; //$this->email_model_old->get_all_messages_count($this->session->userdata('user_data')['initials'], 'withoutcase');
        $this->data['internal_count'] = 0; // $this->email_model_old->iecount($this->session->userdata('user_data')['initials'], 'interemails');
        $this->data['external_count'] = 0; //$this->email_model_old->iecount($this->session->userdata('user_data')['initials'], 'extemails');
        $this->data['newemail_cnt'] = 0; // $this->email_model_old->get_newemail_count($this->session->userdata('user_data')['initials']);
        $this->data['oldemail_cnt'] = 0; //$this->email_model_old->get_oldemail_count($this->session->userdata('user_data')['initials']);
        $this->data['urgentnew_cnt'] = 0; //$this->email_model_old->get_urgentnew_count($this->session->userdata('user_data')['initials']);
        $this->data['urgentold_cnt'] = 0; //$this->email_model_old->get_urgentold_count($this->session->userdata('user_data')['initials']);
        $this->data['unreadnew_cnt'] = 0; //$this->email_model_old->get_unreadnew_count($this->session->userdata('user_data')['initials']);
        $this->data['unreadold_cnt'] = 0; //$this->email_model_old->get_unreadold_count($this->session->userdata('user_data')['initials']);
        $this->data['new_ext_woc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'extemails','withoutcase','new');
        $this->data['new_ext_wc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'extemails','withcase','new');
        $this->data['old_ext_woc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'extemails','withoutcase','old');
        $this->data['old_ext_wc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'extemails','withcase','old');
        $this->data['new_int_woc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'interemails','withoutcase','new');
        $this->data['new_int_wc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'interemails','withcase','new');
        $this->data['old_int_woc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'interemails','withoutcase','old');
        $this->data['old_int_wc_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'interemails','withcase','old');
        $this->data['old_int_cnt'] = 0; // $this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'interemails','','old');
        $this->data['new_int_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'interemails','','new');
        $this->data['old_ext_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'extemails','','old');
        $this->data['new_ext_cnt'] = 0; //$this->email_model_old->iecount_count($this->session->userdata('user_data')['initials'], 'extemails','','new','');
        $this->data['unread_count'] = '';
        $this->data['unread_count'] = $this->email_model_old->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
    }
	
	public function emailattaupload(){
		foreach($_FILES as $index => $file)
		{
			$fileName     = time().'_'.$file['name'];
			$fileTempName = $file['tmp_name'];
			
			if(!empty($file['error'][$index]))
			{
				return false;
			}
 
			if(!empty($fileTempName) && is_uploaded_file($fileTempName))
			{
				move_uploaded_file($fileTempName, APPPATH."../assets/attachment/tempfolder/" . $fileName);
                $_FILES[$index]['tmp_name'] = $fileName;
                $html = json_encode($_FILES[$index]);
				echo "<div class='fileremove'><input type='hidden' value='".$fileName."' name='afiles1[]' id='afiles1'><div class='displayattatchment'><a href='".base_url()."assets/attachment/tempfolder/" . $fileName . "' target='_blank'>" . $fileName . "</a><i class='fa fa-times pull-right remove' data-image='" . $fileName . "' style='font-size:16px; padding:0px 5px 0px 0px;cursor:pointer;'></i><br clear='left'/></div></div><input type='hidden' name='files_global_var[]' id='' class='files_global_var' value='".$html."'>";
			}
		}
	}
	
	public function emailattaremove(){
		$checkfile = file_exists(APPPATH.'../assets/attachment/tempfolder/'.$_POST['imagename']);
		if($checkfile){
            $filename = APPPATH.'../assets/attachment/tempfolder/'.$_POST['imagename'];
            $fileSize = filesize($filename)/1024;
            unlink(APPPATH."../assets/attachment/tempfolder/".$_POST['imagename']); echo 'Successfully Remove file with size from folder. Filesize :'.$fileSize;
        }
		else{ echo 'Not Remove file from folder';}
	}
}
