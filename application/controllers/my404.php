<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class My404 extends CI_Controller {

    function __construct() {
        parent::__construct();
        $data = array();
        $this->data['pageTitle'] = '404: Page not found';
    }

    public function index() {
        $this->load->view('login/header', $this->data);
        $this->load->view('common/404');
        $this->load->view('login/footer');
    }

}
