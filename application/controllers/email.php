<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Email extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $defaultFilter = '';
        $this->load->model('case_model');
        $this->load->model('email_model_new');
        $inboundcheck = $this->uri->segment(2);
        if ($inboundcheck != 'inboundemails') {
            $this->common_functions->checkLogin();
            $this->common_functions->checkSessionId();
        }
        $this->data['url_seg'] = $this->uri->segment(1);

        $data = array();
        $this->data['menu'] = 'email';
        $this->data['withoutcno_count'] = 0;
        $this->data['internal_count'] = 0;
        $this->data['external_count'] = 0;
        $this->data['newemail_cnt'] = 0;
        $this->data['oldemail_cnt'] = 0;
        $this->data['urgentnew_cnt'] = 0;
        $this->data['urgentold_cnt'] = 0;
        $this->data['unreadnew_cnt'] = 0;
        $this->data['unreadold_cnt'] = 0;
        $this->data['new_ext_woc_cnt'] = 0;
        $this->data['new_ext_wc_cnt'] = 0;
        $this->data['old_ext_woc_cnt'] = 0;
        $this->data['old_ext_wc_cnt'] = 0;
        $this->data['new_int_woc_cnt'] = 0;
        $this->data['new_int_wc_cnt'] = 0;
        $this->data['old_int_woc_cnt'] = 0;
        $this->data['old_int_wc_cnt'] = 0;
        $this->data['old_int_cnt'] = 0;
        $this->data['new_int_cnt'] = 0;
        $this->data['old_ext_cnt'] = 0;
        $this->data['unread_count'] = 0;
    }

    public function __destruct() {
        
    }

    public function index() {
        $this->output->enable_profiler(false);
        $this->data['pageTitle'] = 'Emails';
        $emailType = '';
        $caseType = '';
        $defaultFilter = '';
        $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);
        $this->data['signature'] = $this->common_functions->getSignaturedetails($this->session->userdata('user_data')['initials']);
        //$this->effectcounts();
        $this->load->view('common/header', $this->data);
        //$this->load->view('email/email');
        $this->load->view('email_new/email_dash');
        $this->load->view('common/footer');
    }

   
    

    public function compose() {
        $this->data['pageTitle'] = 'Compose Emails';
        $this->data['staff_list'] = $this->common_functions->get_staff_listing($this->session->userdata('user_data')['initials']);
        $this->load->view('common/header', $this->data);
        $this->load->view('email/compose_email');
        $this->load->view('common/footer');
    }

    
    
    public function mark_as_read() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_new->update_email_read($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        //$this->data['unread_count'] = $this->email_model_new->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
        die(json_encode($this->data));
    }

    public function mark_as_old() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_new->update_email_old($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        //$this->effectcounts();
        die(json_encode($this->data));
    }

    public function mark_as_urgent() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_new->update_email_urgent($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        //$this->effectcounts();
        die(json_encode($this->data));
    }

    public function move_to_trash() {
        $filenames = $this->input->post('filenames');
        $foldername = $this->input->post('current_list');
        if ($this->email_model_new->update_email_trash($this->session->userdata('user_data')['initials'], $filenames,$foldername)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        //$this->effectcounts();
        die(json_encode($this->data));
    }

    public function get_unread_count() {
        $data = array();
        $data['count'] = $this->email_model_new->get_unread_messages($this->session->userdata('user_data')['initials']);
        $data['emails'] = $this->email_model_new->get_received_emails_new($this->session->userdata('user_data')['initials'], '', 5, 0, 'new');
        //echo "<pre>"; print_r($data); exit;
        die(json_encode($data));
    }

    public function send_email() {
        $this->load->model('common_model');
        $external = $this->input->post('ext_emails');
        $ext_emails = array();
        if ($this->input->post('ext_emails') != '') {
            $ext_emails = explode(',', $this->input->post('ext_emails'));
        }
        else{
             $ext_emails = '';
        }
        //echo 'hello' . $ext_emails; exit;
        $res = 0;
        $whofrom = $this->session->userdata('user_data')['initials'];
        if($this->input->post('mailType') == 'reply'){
            $subject = "RE: " . $this->input->post('subject');
        }else if($this->input->post('mailType') == 'forward'){
            $subject = "FW: " . $this->input->post('subject');
        }else{
            $subject = $this->input->post('subject');
        }
        
        $flname = $this->input->post('filenameVal');
        $tmp_att_files = isset($_POST['fileGlobalVariable']) ? $_POST['fileGlobalVariable'] : '';
        $date_sent = date("m/d/Y h:i:s a") . "\n";
        $mailbody = $this->input->post('mailbody');
        $whoto = $this->input->post('whoto');
        $flname = $this->input->post('filenameVal');
        if ($this->input->post('caseNo') != 0) {
            $caseno = $this->input->post('caseNo');
        } else {
            $caseno = '';
        }
        if ($caseno != '') {
            $case_info = '';
            $result_case_info = $this->case_model->get_party_info($caseno);
            if($result_case_info->firm != '') {
                $case_info = $result_case_info->firm;
            } else {
                $case_info = $result_case_info->first.' '.$result_case_info->last;
            }
            if(strpos($mailbody, 'Case Number : ') == false){
                $mailbody .= "Case Number : " . $caseno . " " . $case_info;
            }
        }
        if(isset($_POST['caseCategory']) && $_POST['caseCategory'] != '') {
            $event_category = $_POST['caseCategory'];
        } else {
            $event_category = 26;
        }
        if($tmp_att_files != '') {
            $tmp_decoded = json_decode($tmp_att_files);
            foreach ($tmp_decoded as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $exp = explode(":", $value1);
                    $decoded[$key][$exp[0]] = $exp[1];
                }
            }
        } else {
            $_POST['files'] = '';
            foreach ($_FILES as $key1 => $value1) {
                if($_POST['files'] != '') {
                    $_POST['files'] .= ','.$value1['name'];
                } else {
                    $_POST['files'] .= $value1['name'];
                }
                $temp_att_files[$key1] = $value1;
            }
            $decoded = json_decode(json_encode($temp_att_files), true);
        }
        foreach ($decoded as $key => $value) {
            $att_files[$key] = $value;
        }
	$att_files_chk = explode(',',$_POST['files']);
        
        $parcel_id = $this->email_model_new->getParcelId(); 
        $new_parcel_id = $parcel_id + 1;
        $urgent = ($this->input->post('urgent')=='undefined') ? '' : $this->input->post('urgent') ;
        if(!empty($whoto)){
            $whoto_I = explode(",", $whoto);
            foreach ($whoto_I as $key2 => $who) {
                $res = $this->email_model_new->send_email_to($new_parcel_id, '1', 'New Mail', $urgent,$subject, $mailbody, $caseno,$whofrom ,$whoto,$who,$external);
                //if ($res != 0 && !empty($att_files_chk)) {
                /*if ($res != 0 && $att_files_chk[0] != "") {
                    if (!is_dir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$who)) {
                        mkdir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$who, 0777);
                        $path_int = FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$who . '/';
                    }
                    foreach ($att_files_chk as $key => $value) {
                        if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
                        {
                            copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH.'../../tmp/'.$value)) {
                            copy(FCPATH.'../../tmp/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH . 'assets/attachment/'.$new_parcel_id.'_'.$who.'/'.$value)) {
                            unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
                            unlink(FCPATH.'../../tmp/'.$value);
                        }

                    }
                }
                if( $res != 0 && $this->input->post('mailType') == 'forward'){
                    if (!is_dir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$who)) {
                        mkdir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$who, 0777);
                        $path_int = FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$who . '/';
                    }
                   
                    foreach ($att_files_chk as $key => $value) {
                        if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$_POST['filenameVal'].'/'.$value))
                        {
                            copy(FCPATH . 'assets/attachment/tempfolder/'.$_POST['filenameVal'].'/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH . 'assets/attachment/'.$_POST['filenameVal'].'/'.$value)) {
                            unlink(FCPATH . 'assets/attachment/tempfolder/'.$_POST['filenameVal'].'/'.$value);
                        }
                    }
                }*/
            }
        }
        else
        {
            $whoto = '';
        }
        $res_self = $this->email_model_new->send_email_to($new_parcel_id, '2', 'Sent Mail', $urgent,$subject, $mailbody, $caseno,$whofrom ,$whoto,$whofrom,$external);
	if ($res_self != 0 ) {
            if($att_files_chk[0] != ""){
            if (!is_dir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$whofrom)) {
                    mkdir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$whofrom, 0777);
                }
                $path_int = FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$whofrom . '/';
                    foreach ($att_files_chk as $key => $value) {
                        if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value))
                        {
                            copy(FCPATH . 'assets/attachment/tempfolder/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH.'../../tmp/'.$value)) {
                            copy(FCPATH.'../../tmp/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH . 'assets/attachment/'.$new_parcel_id.'_'.$whofrom.'/'.$value)) {
                            unlink(FCPATH . 'assets/attachment/tempfolder/'.$value);
                            unlink(FCPATH.'../../tmp/'.$value);
                        }
                        //else{ echo 'File not found'; }
                    }
                
            }
            if($this->input->post('mailType') == 'forward'){
                    if (!is_dir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$whofrom)) {
                        mkdir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$whofrom, 0777);
                    }
                    $path_int = FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$whofrom . '/';
                    
                    foreach ($att_files_chk as $key => $value) {
                        if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$_POST['filenameVal'].'/'.$value))
                        {
                            copy(FCPATH . 'assets/attachment/tempfolder/'.$_POST['filenameVal'].'/'.$value, $path_int . $value);
                        }
                        elseif(file_exists(FCPATH . 'assets/attachment/'.$_POST['filenameVal'].'/'.$value)) {
                            unlink(FCPATH . 'assets/attachment/tempfolder/'.$_POST['filenameVal'].'/'.$value);
                        }
                    }
               }
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
        $emailtoext = '';
        $emailtoext .= $whoto . ', ';
        $init_name = array('initials' => $whofrom);
        $whofrom_e_name = $this->common_model->getSingleRecordById('staff', 'CONCAT(fname," ",lname) whofrom_name', $init_name)->whofrom_name;
        if(!empty($ext_emails)){
            $status = $this->common_functions->ext_send_new($ext_emails, $subject, $whofrom, $mailbody, $att_files, $caseno, $email_con_id, $urgent, $whofrom_e_name, $new_parcel_id.'_'.$whofrom);
//            if ($res_self != 0) {
//                $path_int = '';
//                if (!is_dir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$this->session->userdata('user_data')['initials'])) {
//                    mkdir(FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$this->session->userdata('user_data')['initials'], 0777);
//                }
//                $path_int = FCPATH . 'assets/attachment/' . $new_parcel_id.'_'.$this->session->userdata('user_data')['initials'] . '/';
//                
//                foreach ($att_files_chk as $key1 => $value1) {
//                    if(file_exists(FCPATH . 'assets/attachment/tempfolder/'.$value1))
//                    {
//                        copy(FCPATH . 'assets/attachment/tempfolder/'.$value1, $path_int . $value1);
//                    }
//                    elseif(file_exists(FCPATH.'../../tmp/'.$value1)) {
//                        copy(FCPATH.'../../tmp/'.$value1, $path_int . $value1);
//                    }
//                    elseif(file_exists(FCPATH . 'assets/attachment/'.$new_parcel_id.'_'.$this->session->userdata('user_data')['initials'].'/'.$value1)) {
//                        unlink(FCPATH . 'assets/attachment/tempfolder/'.$value1);
//                        unlink(FCPATH.'../../tmp/'.$value1);
//                    }
//                }
//            }
        }
        
        if (is_array($ext_emails) && count($ext_emails) > 1) {
            $emailtoext .= implode(", ", $ext_emails) . ' ';
        } else if(is_array($ext_emails) && count($ext_emails) == 1) {
            $emailtoext .= $ext_emails[0] . ', ';
        }
        
        if(!empty($caseno)){
            $latest_filename = $new_parcel_id.'_'.$this->session->userdata('user_data')['initials'];
            $record = array('caseno' => $caseno, 'initials0' => $this->session->userdata('user_data')['initials'], 'initials' => $this->session->userdata('user_data')['initials'], 'date' => date('Y-m-d H:i:s'), 'category' => $event_category, 'mainnotes' => 0, 'event' => 'Email to : ' . trim($emailtoext, ", ").'<br>'.strip_tags($mailbody), 'color' => $event_color, 'email_con_id' => $latest_filename);
            $r = $this->common_functions->insertCaseActRecord($record);
        }
        
        
        
        //echo '<pre>'; print_r($this->data); exit;
        //$r = $this->common_functions->insertCaseActRecord($record);
        die(json_encode($this->data));
    }

    public function reply_email() {
        $this->data['emails'] = $this->email_model_new->get_single_email($this->input->post('current_list'), $this->input->post('filename'));
        if($this->input->post('current_list') == 'sent'){
            $members = explode(",",$this->data['emails'][0]->whoto);
            //echo "<pre>"; print_r($members); exit;
            $internal_party = array();
            foreach ($members as $key => $value) {
                if ($this->session->userdata('user_data')['initials'] != $value) {
                    array_push($internal_party, $value);
                }
                // array_push($internal_party, $value);
            }
            //array_push($internal_party, $this->data['emails'][0]->whofrom);
            $this->data['internal_party'] = $internal_party;
        }
        if (!empty($this->data['emails'][0]->body) && strpos($this->data['emails'][0]->whofrom, '@') !== false) {
            $parse_mail = $this->common_functions->mimemailparse($this->data['emails'][0]->body, $this->input->post('filename'));
            $this->data['emails'][0]->body = '<br>----------------------------------------<br>'.str_replace("\n","<br>",$parse_mail['text']);
            if(strpos($this->data['emails'][0]->whofrom, '@') !== false){
                $this->data['emails'][0]->whotoexternal = $this->data['emails'][0]->whofrom;
                $this->data['emails'][0]->whofrom = '';  
            }
        }
        else{
            if(strpos($this->data['emails'][0]->subject, 'Task') !== false) {
                $this->data['emails'][0]->body = '<br>----------------------------------------<br>'.nl2br($this->data['emails'][0]->body);
            }else{
                 $this->data['emails'][0]->body = '<br>----------------------------------------<br>'.$this->data['emails'][0]->body;
            }
        }
        die(json_encode($this->data));
    }

    public function forward_email() {
        $this->data['emails'] = $this->email_model_new->get_single_email($this->input->post('current_list'), $this->input->post('filename'));

        if (!empty($this->data['emails'][0]->body) && strpos($this->data['emails'][0]->whofrom, '@') !== false) {
            //echo 'in if'; exit;
            $parse_mail = $this->common_functions->mimemailparse($this->data['emails'][0]->body, $this->input->post('filename'));
            $this->data['emails'][0]->body = str_replace("\n","<br>",$parse_mail['text']);
            $who_form_name = explode('<', $parse_mail['from']);
            $this->data['emails'][0]->whofrom_name = $who_form_name[0];
        }
        else{
            if(strpos($this->data['emails'][0]->subject, 'Task') !== false) {
                $this->data['emails'][0]->body = nl2br($this->data['emails'][0]->body);
            }
        }
        
        if (is_dir(FCPATH . 'assets/attachment/' . $this->input->post('filename'))) {
            $dir = FCPATH . 'assets/attachment/'.$this->input->post('filename');//"path/to/targetFiles";
            mkdir(FCPATH . 'assets/attachment/tempfolder/' .$this->input->post('filename'), 0777);
            $dirNew = FCPATH . 'assets/attachment/tempfolder/'.$this->input->post('filename');//path/to/destination/files
            // Open a known directory, and proceed to read its contents
            if (is_dir($dir)) {
                if ($dh = opendir($dir)) {
                    while (($file = readdir($dh)) !== false) {
                        copy($dir . '/' . $file, $dirNew . '/' . $file);
                    }
                    closedir($dh);
                }
            }
        }

        
        $directory = FCPATH . '/assets/attachment/tempfolder/'.$this->input->post('filename');
        
        $file_data = '';
        $fwd_files = array();
        $folderName = $this->input->post('filename');
        if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
            
            $files = glob($directory . "/*");
            foreach ($files as $value) {
                $filepath = explode("/",$value);
                $fileNM = end($filepath);
                $link = base_url() . 'assets/attachment/tempfolder/'.$fileNM ;
                $removelink = 'assets/attachment/tempfolder/'.$fileNM;
                $file_name = basename($fileNM);
                $file_data .= '<div class="fileremove_'.$file_name.'"><div class="displayattatchment"><input type="hidden" value="'.$file_name.'" name="afiles1[]" id="afiles1"><a href="'.$link.'" target="_blank">'.$file_name.'</a><i class="fa fa-times pull-right fwdremove" data-image="'.$file_name.'" data-folder="'.$folderName.'" style="font-size:16px; padding:0px 5px 0px 0px;cursor:pointer;"></i><br clear="left"></div></div>';
                array_push($fwd_files, $file_name);
            }
        }
        if (!empty($file_data)) {
            $this->data['file_with_attachment'] = $file_data;
            $this->data['fwd_files'] = $fwd_files;
        } else {
            $this->data['file_with_attachment'] = '';
        }

        die(json_encode($this->data));
    }

    
    public function printEmail() {

        $this->data['emails'] = $this->email_model_new->get_single_email($this->input->post('current_list'), $this->input->post('filename'));
        if (!empty($this->data['emails'][0]->body) && strpos($this->data['emails'][0]->whofrom, '@') !== false) {
            //echo 'in if'; exit;
            $parse_mail = $this->common_functions->mimemailparse($this->data['emails'][0]->body, $this->input->post('filename'));
            $this->data['emails'][0]->body = str_replace("\n","<br>",$parse_mail['text']);
             if(strpos($this->data['emails'][0]->whofrom, '@') !== false){
                $this->data['emails'][0]->whotoexternal = $this->data['emails'][0]->whofrom;
                $this->data['emails'][0]->whofrom = '';  
            }
        }
        $this->load->view('email_new/print_emaildetails_new', $this->data);
    }

    /* this function will receive response as inbound parsing email from sendgrid */

    public function inboundemails() {

        $this->load->library('sendgrid/SendgridParse');
        $parsed = new SendgridParse();
        $urgent = '';
        $caseno = 0;
        $case_no = 0;
        $caseact_parties = '';
        $email_con_id = null;
        $to = explode('-', $parsed->to[0]['email']);
        if (preg_match('#[0-9]#', $to[0])) {
            $email_con_id = $to[0];
            $case_no = $to[1];

            if(ctype_digit($to[1])){
                $_to = $to[2];
            }else{
                $_to = $to[1];
            }
            
            $whoto = substr($_to, 0, strpos($_to, "@")); ///$this->email_conversation_model->grab_conversation_from_id($email_con_id)[0]->email_con_people;
            $whoto = rtrim($whoto, ',');
            $whoto = explode(',', $whoto);
            $caseact_partie = $whoto;
        } else {
            
            $email_con_id = (!empty($to[1])) ? $to[1] : '';
            $case_no = (!empty($to[2])) ? $to[2] : 0;
            $whoto = strtoupper($to[0]);
        }
        
        
        if (empty($to)) {
            $email_con_id = '';
            $case_no = 0;
            $whoto = strtoupper(substr($parsed->to[0]['email'], 3));
        }
        if ($case_no != 0 && is_numeric($case_no)) {
            $case_no = $case_no;
        } else {
            $case_no = 0;
        }
        
        $whofrom = $parsed->from['email'];
        $parcel_id = $this->email_model_new->getParcelId($whoto[0]);
         
        $new_parcel_id = $parcel_id + 1;
        
        if (!empty($parsed->post['email'])) {
            $mail_body = json_encode($parsed->post['email']);
            if (is_array($whoto)) {
                 foreach ($whoto as $key => $whoval) {
                    if (!preg_match("/\b@\b/i", $whoval)) {
                         $this->email_model_new->send_email_to_ext($new_parcel_id, '1', 'New Mail', 'N',$parsed->subject, $mail_body, $case_no,$whofrom,$whoval,$whoval,'');
                        $caseact_parties .= $whoval . ',';
                    }
                }
                $caseact_parties = rtrim($caseact_parties, ',');
            } else {
                 $this->email_model_new->send_email_to_ext($new_parcel_id, '1', 'New Mail', 'N',$parsed->subject, $mail_body, $case_no,$whofrom,$whoto[0],$whoto[0],'');
                $caseact_parties = $whoto;
            }
        }
        if ($case_no != 0) {
            $event_category = 26;
            $event_color = 2;
            $get_parse_mail = $this->common_functions->mimemailparse( $mail_body, $new_parcel_id.'_'.strtoupper($whoto[0]));
            $mailBody = str_replace("\n","<br>",$get_parse_mail['text']);
            $ext_to = "Email received from : $whofrom  To : $caseact_parties"."<br/>".$mailBody;
            $record = array('caseno' => $case_no, 'initials0' => $whoto[0], 'initials' => $whoto[0], 'date' => date('Y-m-d H:i:s'), 'color' => $event_color, 'category' => $event_category, 'mainnotes' => 0, 'event' => $ext_to, 'email_con_id' => $new_parcel_id.'_'.strtoupper($whoto[0]));
            $this->common_functions->insertCaseActRecord($record);
        }
        exit;
    }
    
    public function reply_all_email() {
        $this->data['emails'] = $this->email_model_new->get_single_email($this->input->post('current_list'), $this->input->post('filename'));
        //echo "<pre>"; print_r($this->data); exit;
        $members = explode(",",$this->data['emails'][0]->whoto);
        //echo "<pre>"; print_r($members); exit;
        $internal_party = array();
        foreach ($members as $key => $value) {
            if ($this->session->userdata('user_data')['initials'] != $value) {
                array_push($internal_party, $value);
            }
            // array_push($internal_party, $value);
        }
        if (!empty($this->data['emails'][0]->body) && strpos($this->data['emails'][0]->whofrom, '@') !== false) {
            //echo 'in if'; exit;
            $parse_mail = $this->common_functions->mimemailparse($this->data['emails'][0]->body, $this->input->post('filename'));
            $this->data['emails'][0]->body = '<br>----------------------------------------<br>'.str_replace("\n","<br>",$parse_mail['text']);
             if(strpos($this->data['emails'][0]->whofrom, '@') !== false){
                $this->data['emails'][0]->whotoexternal = $this->data['emails'][0]->whofrom;
                $this->data['emails'][0]->whofrom = '';  
            }
        }
        else{
            if(strpos($this->data['emails'][0]->subject, 'Task') !== false) {
                $this->data['emails'][0]->body = '<br>----------------------------------------<br>'.nl2br($this->data['emails'][0]->body);
            }
            else{
                 $this->data['emails'][0]->body = '<br>---------------------------------------<br>'.$this->data['emails'][0]->body;
            }
        }
        if($this->input->post('current_list') != 'sent'){
        array_push($internal_party, $this->data['emails'][0]->whofrom);}
        $this->data['external_party'] = rtrim($this->data['emails'][0]->whotoexternal, ',');
        $this->data['internal_party'] = $internal_party;
        die(json_encode($this->data));
    }

    public function checkUnreadEmails() {
        $data = array();
        $data['ucount'] = $this->email_model_new->get_unread_messages($this->session->userdata('user_data')['initials']);
        $data['mail_category'] = $this->email_model_new->get_type_of_last_email($this->session->userdata('user_data')['initials'], 'unread');
        die(json_encode($data));
    }

    public function refresh_listing_new() {
        //echo '<pre>'; print_r($_POST); exit;
         $columns = array(
            0 => 'datecreate',
            1 => 'whofrom',
        );
        $dispatch_ajax = array();
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
        $search = $this->input->post('search')['value'];
        $start = ($this->input->post('start') != 0) ? $this->input->post('start') : 0;
        $length = ($this->input->post('length') != 0) ? $this->input->post('length') : DEFAULT_LISTING_LENGTH;
        $list_type = ($this->input->post('list_type') != '') ? $this->input->post('list_type') : '';
        $current_list = ($this->input->post('current_list') != '') ? $this->input->post('current_list') : 'new';
        if ($this->input->post('caseType') != '') {
            $caseType = $this->input->post('caseType');
        } else {
            $caseType = '';
        }
        if ($this->input->post('emailType') != '') {
            $emailType = $this->input->post('emailType');
        } else {
            $emailType = '';
        }
        $singleFilter = '';
        $defaultFilter = '';
        if ($search != '' && $current_list != 'sent') {
            $singleFilter = '(subject LIKE "%' . $search . '%" or whofrom LIKE "%' . $search . '%")';
        }
        elseif($search != '' && $current_list == 'sent'){
            $singleFilter = '(subject LIKE "%' . $search . '%" or whoto LIKE "%' . $search . '%" or whoto LIKE "'.$search.'%" or whoto LIKE "%'.$search.'" or whotoexternal LIKE "%'.$search.'%")';
        }
        if (!empty($list_type) && $list_type == 'urgent') {
            $defaultFilter = array('priority' => 'Y');
        } else if (!empty($list_type) && $list_type == 'unread') {
            $defaultFilter = array('readyet !=' => 'Y');
        } 
        if($this->input->post('current_list') != 'Self'){
            $emails = $this->email_model_new->get_new_emails($singleFilter,$order,$dir,$length,$start,$defaultFilter,$caseType,$emailType);
        }else {
            $emails = $this->email_model_new->get_self_emails($singleFilter,$order,$dir,$length,$start,$defaultFilter,$caseType,$emailType);
        }
        //echo '<pre>'; print_r($emails); exit;
        foreach ($emails as $key => $value) {
            $unreadcls = '';
            $html = '';
            $dispatch_data['whofrom'] = '';
            $directory = FCPATH . '/assets/attachment/' . $value->parcelid;
            $attachment = '';
            if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                $attachment = '<span class="pull-right"><i class="fa fa-paperclip"></i></span>';
            }

            if ($value->readyet != 'Y') {
                $unreadcls = 'unread';
            }
            
            $notreadyet = '';
            if ($value->priority == 'Y') {
                $html .= '<i class="fa fa-circle text-danger"></i>&nbsp;';
                if (($value->readyet != 'Y')) {
                    $notreadyet = 'notreadyet';
                }
            }else if (($value->readyet != 'Y')) {
                $html .= '<i class="fa fa-circle text-navy"></i>&nbsp;';
                $notreadyet = 'notreadyet';
            } else {
                    $html .= '<i class="fa fa-circle text-warning"></i>&nbsp;';
            }
            $dispatch_data['parcelid'] = "<label class='container-checkbox'><input type='checkbox' class='email-check-id $unreadcls' value='" . $value->parcelid . "' data-ids = '". $i ."'><span class='checkmark'  style='padding-right: 5px;'></span></label>";
            if ($current_list == 'sent') {
                $dispatch_data['whofrom'] = "<a class='emailaddresswidth read-email $notreadyet' href='javascript:;' data-parcelid = '".$value->parcelid."' data-ids = '". $i ."'>";
                $dispatch_data['whofrom'] .= $html ."<span class='initialwidth'>".(($value->whoto == null) ? '' : $value->whoto);
                if($value->whotoexternal != '' && $value->whoto!= ''){
                    $dispatch_data['whofrom'] .= ','.$value->whotoexternal;
                }
                else{
                    $dispatch_data['whofrom'] .= $value->whotoexternal;
                }
                $dispatch_data['whofrom'] .= '</span>';
                $dispatch_data['whofrom'] .= "<span class='date'>".date('M d, Y', strtotime($value->datesent)) . ' ' . date('g:i A', strtotime($value->datesent)).'</span>'. "<br /><span class='subject'>" . $value->subject .$attachment. '</span></a>';
            } else{
                $dispatch_data['whofrom'] = "<a class='emailaddresswidth read-email $notreadyet' href='javascript:;' data-parcelid = '".$value->parcelid."' data-ids = '". $i ."'>";
                $dispatch_data['whofrom'] .= $html ."<span class='initialwidth'>".(($value->whofrom == null) ? ucfirst(explode('@', $value->whofrom)[0]) : $value->whofrom).'</span>';
                $dispatch_data['whofrom'] .= "<span class='date'>".date('M d, Y', strtotime($value->datesent)) . ' ' . date('g:i A', strtotime($value->datesent)).'</span>'. "<br /><span class='subject'>" . $value->subject .$attachment. '</span></a>';
            }
            array_push($dispatch_ajax, $dispatch_data);
			
			$i++;
			
        }
        
        if($this->input->post('current_list') != 'Self'){
            $totalData = $this->data['count'] = $this->email_model_new->get_new_messages_count($singleFilter,$defaultFilter,$caseType,$emailType);
            $totalFiltered = $this->email_model_new->get_new_messages_count($singleFilter,$defaultFilter,$caseType,$emailType); 
        }else{
            $totalData = $this->data['count'] = $this->email_model_new->get_self_messages_count($singleFilter,$defaultFilter,$caseType,$emailType);
            $totalFiltered = $this->email_model_new->get_self_messages_count($singleFilter,$defaultFilter,$caseType,$emailType);
        }
        $json_data = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $dispatch_ajax,
        );
        
        die(json_encode($json_data));
        
    }
    
    public function view($email_id = '') {
        $this->data['emails'] = $this->email_model_new->get_single_email($this->input->post('current_list'), $this->input->post('parcelid'));
        $directory = FCPATH . '/assets/attachment/' . $this->input->post('parcelid');
        $link = base_url() . 'assets/attachment/' . $this->input->post('parcelid') . '/';
        $removelink = 'assets/attachment/' . $this->input->post('parcelid') . '/';
        $file_data = '';
        if (!empty($this->data['emails'][0]->body) && strpos($this->data['emails'][0]->whofrom, '@') !== false) {
            //echo 'in if'; exit;
            $parse_mail = $this->common_functions->mimemailparse($this->data['emails'][0]->body, $this->input->post('parcelid'));
            $this->data['emails'][0]->body = str_replace("\n","<br>",$parse_mail['text']);
            $who_form_name = explode('<', $parse_mail['from']);
            $this->data['emails'][0]->whofrom_name = $who_form_name[0];
        }
        else{
            if(strpos($this->data['emails'][0]->subject, 'Task') !== false) {
                $this->data['emails'][0]->body = nl2br($this->data['emails'][0]->body);
            }
        }
        if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
            $files = glob($directory . "/*");
            $file_data .= '<h4>Attachment</h4><ul class="attatchement_thumbnail" style="margin-left: -15px;">';
            foreach ($files as $value) {
                $file_name = basename($value);
                $filenameextension = explode('.',$file_name);
                if($filenameextension[1]== 'tif' || $filenameextension[1]== 'tiff' || $filenameextension[1]== 'bmp' || $filenameextension[1]== 'jpg' || $filenameextension[1]== 'jpeg' || $filenameextension[1]== 'gif' || $filenameextension[1]== 'png' || $filenameextension[1]== 'svg' || $filenameextension[1]== 'eps')
                { //<i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><img src='".$link . $file_name."' width='100'></div>" . $filenameextension[0] . "</a> </div></li>";
                }
				else if($filenameextension[1]== 'webm' || $filenameextension[1]== 'mkv' || $filenameextension[1]== 'flv' || $filenameextension[1]== 'vob' || $filenameextension[1]== 'ogv' || $filenameextension[1]== 'ogg' || $filenameextension[1]== 'mpeg' || $filenameextension[1]== 'mpg' || $filenameextension[1]== 'mp4' || $filenameextension[1]== 'avi' || $filenameextension[1]== '3gp')
				{   //<i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
					$file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><video controls><source src='".$link . $file_name."' type='video/". $filenameextension[1] ."'></video></div>" . $filenameextension[0] . "</a>  </div></li>";
				}
				else if($filenameextension[1]== 'mp3' || $filenameextension[1]== 'wma' || $filenameextension[1]== 'au' || $filenameextension[1]== 'ea' || $filenameextension[1]== 'aif')
				{ //<i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
					$file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><audio  width='320' height='240' controls><source src='".$link . $file_name."' type='video/". $filenameextension[1] ."'></video></div>" . $filenameextension[0] . "</a>  </div></li>";
				}
				
				else if($filenameextension[1]== 'txt')
                { // <i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-text-o'></span></div>" . $filenameextension[0] . "</a> </div></li>";
                }
				else if($filenameextension[1]== 'zip' || $filenameextension[1]== 'rar')
                { //<i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-zip-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'rar')
                {   // <i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-archive-o'></span></div>" . $filenameextension[0] . "</a> </div></li>";
                }
				else if($filenameextension[1]== 'doc' || $filenameextension[1]== 'docx')
                { // <i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-word-o'></span></div>" . $filenameextension[0] . "</a> </div></li>";
                }
				else if($filenameextension[1]== 'pdf')
                {   //<i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-pdf-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'xls' || $filenameextension[1]== 'xlsx' || $filenameextension[1]== 'csv')
                {   //<i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-excel-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else if($filenameextension[1]== 'ppt' || $filenameextension[1]== 'pptx')
                {   //<i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
                    $file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file-powerpoint-o'></span></div>" . $filenameextension[0] . "</a>  </div></li>";
                }
				else
				{ // <i class='fa fa-times-circle' data='" . $removelink . $file_name . "' style='font-size:20px; padding:5px;cursor:pointer;'></i>
					$file_data .= "<li><div><a download='" . $file_name . "' href='" . $link . $file_name . "' target ='_blank'><div class='attatchement_preview'><span class='icon fa fa-2x fa-file'></span></div>" . $filenameextension[0] . "</a> </div></li>";
				}
                
            }   
            $file_data .= '</ul>';
        }
        if (!empty($file_data)) {
            $this->data['file_with_attachment'] = $file_data;
        } else {
            $this->data['file_with_attachment'] = '';
        }
        die(json_encode($this->data));
    }
    
    public function markreadviewNew($email_id = '') {
        
        $this->data['emails'] = $this->email_model_new->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'));
            
            $this->email_model_new->update_email_read_new($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'));
            $this->data['unread_count'] = $this->email_model_new->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
            die(json_encode($this->data));
    }
    
    public function markunreadviewnew($email_id = '') {
        
        $this->data['emails'] = $this->email_model_new->get_single_email($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'));
            
            $this->email_model_new->update_email_unread_new($this->session->userdata('user_data')['initials'], $this->input->post('filename'), $this->input->post('current_list'));
            $this->data['unread_count'] = $this->email_model_new->get_unread_messages($this->session->userdata('user_data')['initials'], 'unread');
        die(json_encode($this->data));
    }
    

    public function emailfwdremove(){
            $checkfile = file_exists(APPPATH.'../assets/attachment/tempfolder/'.$_POST['folder'].'/'.$_POST['imagename']);
		if($checkfile){
                    $filename = APPPATH.'../assets/attachment/tempfolder/'.$_POST['folder'].'/'.$_POST['imagename'];
                    $fileSize = filesize($filename)/1024;
                    unlink(APPPATH."../assets/attachment/tempfolder/".$_POST['folder'].'/'.$_POST['imagename']); 
                    echo 'Successfully Remove file with size from folder. Filesize :'.$fileSize;
                }
	}
    public function case_email_listing() {
        $caseno = $this->input->post('caseno');
        $this->data['result'] = array();
        $result = array();
        $singleFilter = '';
        $this->data['result'] = $this->email_model_new->get_case_emails($caseno);
        //echo '<pre>';print_r($this->data['result']); exit;
        if (!empty($this->data['result'])) {
            foreach ($this->data['result'] as $key => $value) {
                $directory = FCPATH . '/assets/attachment/' . $value->parcelid;
                if (is_dir($directory) && count(glob($directory . "/*")) > 0) {
                    $this->data['result'][$key]->attachment_file = 1;
                } else {
                    $this->data['result'][$key]->attachment_file = 0;
                }
            }
        }
        die(json_encode($this->data));
    }
    
    public function emailattaupload(){
            foreach($_FILES as $index => $file)
            {
                    $fileName     = time().'_'.$file['name'];
                    $fileTempName = $file['tmp_name'];

                    if(!empty($file['error'][$index]))
                    {
                            return false;
                    }

                    if(!empty($fileTempName) && is_uploaded_file($fileTempName))
                    {
                            move_uploaded_file($fileTempName, APPPATH."../assets/attachment/tempfolder/" . $fileName);
            $_FILES[$index]['tmp_name'] = $fileName;
            $html = json_encode($_FILES[$index]);
                            echo "<div class='fileremove'><input type='hidden' value='".$fileName."' name='afiles1[]' id='afiles1'><div class='displayattatchment'><a href='".base_url()."assets/attachment/tempfolder/" . $fileName . "' target='_blank'>" . $fileName . "</a><i class='fa fa-times pull-right remove' data-image='" . $fileName . "' style='font-size:16px; padding:0px 5px 0px 0px;cursor:pointer;'></i><br clear='left'/></div></div><input type='hidden' name='files_global_var[]' id='' class='files_global_var' value='".$html."'>";
                    }
            }
    }
	
    
    public function emailattaremove(){
		$checkfile = file_exists(APPPATH.'../assets/attachment/tempfolder/'.$_POST['imagename']);
		if($checkfile){
            $filename = APPPATH.'../assets/attachment/tempfolder/'.$_POST['imagename'];
            $fileSize = filesize($filename)/1024;
            unlink(APPPATH."../assets/attachment/tempfolder/".$_POST['imagename']); echo 'Successfully Remove file with size from folder. Filesize :'.$fileSize;
        }
    }
    
    public function move_to_new() {
        $filenames = $this->input->post('filenames');
        if ($this->email_model_new->update_email_new($this->session->userdata('user_data')['initials'], $filenames)) {
            $this->data['result'] = "true";
        } else {
            $this->data['result'] = "false";
        }
         die(json_encode($this->data));
    }
}
