<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->common_functions->checkLogin();
        $this->load->model('profile_model');
        $data = array();
        $this->data['menu'] = 'profile';
        $this->data['username'] = $this->session->userdata('user_data')['username'];
        $this->data['loginUser'] = $this->session->userdata('user_data')['username'];

    }

    function index()
    {
        $this->data['pageTitle'] = 'Profile';
        $initial = $this->session->userdata('user_data')['initials'];
        $data['profile'] = $this->profile_model->getprofile($initial);
        //echo '<pre>'; print_r($profile); exit;
        $this->load->view('common/header',$this->data);
        $this->load->view('profile/profile',$data);
        $this->load->view('common/footer');
    }
    
    function correctImageOrientation($filename) {
        if (function_exists('exif_read_data')) {
          $exif = exif_read_data($filename);
          if($exif && isset($exif['Orientation'])) {
            $orientation = $exif['Orientation'];
            if($orientation != 1){
              $img = imagecreatefromjpeg($filename);
              $deg = 0;
              switch ($orientation) {
                case 3:
                  $deg = 180;
                  break;
                case 6:
                  $deg = 270;
                  break;
                case 8:
                  $deg = 90;
                  break;
              }
              if ($deg) {
                $img = imagerotate($img, $deg, 0);       
              }
              // then rewrite the rotated image back to the disk as $filename
              imagejpeg($img, $filename, 95);
            } // if there is some rotation necessary
          } // if have the exif orientation info
        } // if function exists     
      }

    function updateprofilepic()
    {
        $this->load->library('upload');
        $nwfile =  explode(".",$_FILES["file"]['name']);
        $ext = end($nwfile);

        if(!is_dir(FCPATH.'assets/profileimages')){
                    mkdir(FCPATH.'assets/profileimages',0777);
                }

        $config['upload_path'] = FCPATH.'/assets/profileimages/'; //Use relative or absolute path

        $new_name = time().".".$ext;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = $new_name;
        $config['max_size'] = '4096000';// Can be set to particular file size , here it is 2 MB(2048 Kb)
        //$config['max_width'] = '1024';
        //$config['max_height'] = '768';
        $config['overwrite'] = FALSE;
        //If the file exists it will be saved with a progressive number appended

        //Initialize
        $this->upload->initialize($config);
        if( ! $this->upload->do_upload("file")){
            echo $this->upload->display_errors();exit;
        }
        else {
            $exists = $this->profile_model->change_profile($new_name);
            $this->session->set_flashdata('message', 'You have successfully updated your Profile.');
            //$this->session->set_userdata('user_data')['profilepic'] = $new_name;
            $arr = $this->session->userdata('user_data');
            $arr['profilepic'] = $new_name;
            $this->session->set_userdata('user_data',$arr);
            //echo '<pre>'; print_r($arr); exit;
            
            $fileLocation = FCPATH.'assets/profileimages/'.$new_name;
            $this->correctImageOrientation($fileLocation);
            
            if($this->uri->segment(3) != '')
            {


                $file_located = FCPATH.'assets/profileimages/'.$this->uri->segment(3);
                if (file_exists($file_located))
                {   unlink($file_located);  }
            }
            echo 1;

        }
    }


     public function resetpass()
    {
        $pass = $this->input->post('pass');
        $oldpass = $this->input->post('oldpass');
        $initial = $this->session->userdata('user_data')['initials'];
        $where = array('password' => $oldpass, 'initials' => $initial);
        $exists = $this->profile_model->check_pass($where);

        $count = count($exists);
        if(!empty($count)) {
            $this->profile_model->change_pass($pass);
            $newUserSNS = array(
                "username" => $initial,
                "password" => base64_encode($pass),
                "snsusersunqid" => $initial,
                 );
            //echo '<pre>'; print_r($newUserSNS); exit;
            $urlsns = 'http://52.66.93.186/sns/api/user/update';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlsns);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($newUserSNS));
            $response = curl_exec($ch);
            $this->session->set_flashdata('message', 'Your Password is Changed Successfully.');
            echo '1';

        } else {
            echo '0';
        }
    }


    public function updatesignature()
    {
          $resforsignatureupdate =  $this->profile_model->updatesignature($this->input->post('mailbody'),$this->input->post('onoffsignature'));
          if($resforsignatureupdate >0 )
          {
             $data["status"] = 'success';
          }
          $initial = $this->session->userdata('user_data')['initials'];
            $data["results"] = $this->profile_model->getsignaturedata($initial);
            echo json_encode($data["results"]); 
    }
    
// controller end
}

