<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');

        $data = array();
        $this->load->model('email_model');
        $this->load->model('calendar_model');
        $this->load->model('email_conversation_model');
        $this->load->model('common_model');
        $this->load->model('case_model');
        $this->load->model('admin_login_model');

    }
    public function getchecklist()
    {
        $checklist = $this->common_model->getAllRecord('support_checklist','*');
        if(!empty($checklist))
        {
            $this->data['status'] = 1;
            $this->data['checklist'] = $checklist;
            die(json_encode($this->data));
        }
        else{
            $this->data['status'] = 0;
            $this->data['checklist'] = 'No Data Found';
            die(json_encode($this->data));
        }
    }
    public function getusers()
    {
        $staff = $this->common_model->getAllRecord("staff","initials,username,fname,lname,title,emailid,isActive");
        if(!empty($staff))
        {
            return $staff;
        }
        else{
            $staff = 'No Data Found';
            return $staff;
        }
    }
    public function getcasesofuser()
    {
        $user = $this->input->post('initials');
        $condition = "atty_resp ='".$user."' OR para_hand ='".$user."' OR atty_hand ='".$user."' OR sec_hand ='".$user."' "; 
        $allcases = $this->common_model->getAllRecord("case","caseno,casestat,dateopen,dateclosed,caption1,case_status",$condition);    
        if(!empty($allcases))
        {
            $this->data['status'] = 1;
            $this->data['allcases'] = $allcases;
            die(json_encode($this->data));
        }
        else{
            $this->data['status'] = 0;
            $this->data['allcases'] = 'No Data Found';
            die(json_encode($this->data));
        }
    }
    public function getcasecount()
    {
        return  $totalcase = $this->common_model->getcount('case','caseno');   
        //die(json_encode($totalcase));
    }
    public function getfirmdetails()
    {
        $admin = $this->common_model->getSingleRecordById('system_admin','sa_user_name,sa_password,sa_email', array('sa_role'=>'1','sa_status'=>'1'));
        $this->data['status'] = 1;
        $this->data['admin_data'] = $admin;
        $this->data['casecount'] =$this->getcasecount();
        $this->data['staff'] = $this->getusers();
        $this->data['feature_request'] = $this->common_model->getcount('support_request','id');
        die(json_encode($this->data));
    }
    public function getfeaturelist()
    {
        $this->data['featurelist'] = $this->common_model->getAllRecord('support_request','*','','id desc, moduleName');
        die(json_encode($this->data));
    }
    public function updatedata(){
        $table = $this->input->post('table'); 
        $fields = $this->input->post('fields');
        $condition = $this->input->post('condition');
        $result = $this->common_model->updatetabledata($table,$fields,$condition);
        die(json_encode($result));   
    }
    public function getfeaturerequest()
    {
        $requestlist = $this->common_model->getAllRecord('support_request','*','','id desc');
        die(json_encode($requestlist));   
    }
    public function getcasereport()
    {
        $year = isset($_REQUEST['year']) ? $_REQUEST['year'] : date('Y');
        $case_report_data = $this->common_model->getAllRecord('case',"count(caseno) as nocase,CONCAT(MONTHNAME(dateenter),'-',YEAR(dateenter))monthyear","YEAR(dateenter)= '".$year."'",'dateenter asc','','monthyear');
        $total_count = $this->common_model->getcount('case','*',"YEAR(dateenter)= '".$year."'");
        $data = array('case_report_data'=>$case_report_data,'total_count'=>$total_count);
       die(json_encode($data));   
    }
    public function getuserlog()
    {
         $requestlist = $this->common_model->getAllRecord('loginfo','*','','initials asc');
         die(json_encode($requestlist));  
    }
    public function getbugreport(){
        $id = isset($_REQUEST['id']) ? $this->input->post('id') : '';
        $condition = '';
        if($id !=''){$condition = array('id'=>$id);
            $requestlist = $this->common_model->getSingleRecordById('bug_report','*',$condition,'id desc');
        }else{
        $requestlist = $this->common_model->getAllRecord('bug_report','*','','id desc');
        }
        die(json_encode($requestlist));  
    }
    
}