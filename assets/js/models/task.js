function getBaseURL() {
    var a = (e = location.href).substring(0, e.indexOf("/", 14));
    if (-1 != a.indexOf("http://localhost")) {
        var e = location.href,
                t = location.pathname,
                d = e.indexOf(t),
                i = e.indexOf("/", d + 1);
        return e.substr(0, i)
    }
    return a
}
var baseUrl = getBaseURL(),
        uri_seg = "tasks",
        myDropzone = "",
        selectedArray = [],
        rolodexSearchDatatable = "",
        rolodexSearch = "",
        categoryeventlist = "",
        casepartylist_data = "",
        venue_table_data = "",
        caseactTable = "",
        swalFunction = "",
        CaseHID = $("#hdnCaseId").val(),
        ProspectListDatatable, searchBy1 = {};

function setValidation() {
    return $("#addRolodexForm").validate({
        ignore: [],
        rules: {
            card_first: {
                required: function (a) {
                    return "" == $("input[name=card2_firm]").val()
                },
                letterwithspace: !0
            },
            card2_firm: {
                required: function (a) {
                    return "" == $("input[name=card_first]").val()
                }
            },
            card2_address1: {
                required: function (a) {
                    return "" != $("input[name=card2_firm]").val()
                }
            },
            card2_city: {
                required: function (a) {
                    return "" != $("input[name=card2_firm]").val()
                }
            },
            card2_state: {
                required: function (a) {
                    return "" != $("input[name=card2_firm]").val()
                },
                stateUS: !0
            },
            card2_zip: {
                required: function (a) {
                    return "" != $("input[name=card2_firm]").val()
                },
                zipcodeUS: !0
            },
            card_type: {
                required: true
                /*function (a) {
                    return "" != $("input[name=card_first]").val()
                }*/
            },
            card_middle: {
                letterwithspace: !0
            },
            card_last: {
                letterwithspace: !0
            },
            card_business: {
                phoneUS: !0
            },
            card_home: {
                phoneUS: !0
            },
            card_fax: {
                phoneUS: !0,
                maxlength: 15
            },
            card_email: {
                email: !0
            },
            card2_phone1: {
                phoneUS: !0
            },
            card2_fax: {
                phoneUS: !0
            },
            card2_phone2: {
                phoneUS: !0
            },
            card2_fax2: {
                phoneUS: !0
            },
            card2_tax_id: {
                alphanumeric: !0,
                maxlength: 15
            },
            card_social_sec: {
                ssId: !0
            },
            card_speciality: {
                letterswithbasicpunc: !0
            }
        },
        messages: {
            card_first: {
                required: "Please Fill First Name or Firm Name"
            },
            card2_firm: {
                required: "Please Fill First Name or Firm Name"
            },
            card2_fax: {
                phoneUS: "Please specify a valid fax number"
            },
            card2_fax2: {
                phoneUS: "Please specify a valid fax number"
            },
            card_fax: {
                phoneUS: "Please specify a valid fax number"
            }
        },
        invalidHandler: function (a, e) {
            var t = e.invalidElements().get(0),
                    d = $(t).closest(".tab-pane").attr("id");
            $('#addcontact a[href="#' + d + '"]').tab("show")
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "card_type") {
                error.insertAfter(element.next());
            } else {
                error.insertAfter(element);
            }
        }
    })
}

function setCalendarValidation() {
    return $("#calendarForm").validate({
        ignore: [],
        rules: {
            calstat: {
                letterwithspace: !0
            },
            first: {
                required: !0,
                letterswithbasicpunc: !0
            },
            last: {
                required: !0,
                letterswithbasicpunc: !0
            },
            defendant: {
                letterswithbasicpunc: !0
            },
            judge: {
                letterswithbasicpunc: !0
            },
            venue: {
                letterswithbasicpunc: !0
            },
            caldate: {
                required: !0
            },
            caltime: {
                required: !0
            },
            event: {
                required: !0
            },
            caseno: {
                number: !0,
                remote: {
                    url: baseUrl + "/Prospects/CheckCaseno",
                    type: "post",
                    data: {
                        pro_caseno: function () {
                            return $('#calendarForm :input[name="caseno"]').val()
                        }
                    }
                }
            }
        },
        messages: {
            caseno: {
                remote: "This caseno is not exist"
            }
        },
        invalidHandler: function (a, e) {
            var t = e.invalidElements().get(0),
                    d = $(t).closest(".tab-pane").attr("id");
            $('#addcalendar a[href="#' + d + '"]').tab("show")
        }
    })
}

function editContact(a) {
    $.blockUI(), $.ajax({
        url: baseUrl + "/rolodex/getEditRolodex",
        method: "POST",
        data: {
            cardcode: a
        },
        success: function (a) {
            var e = $.parseJSON(a);
            if (e.card_comments)
                var t = $.parseJSON(e.card_comments);
            else
                t = "";
            $("#addcontact .modal-title").html("Edit Contact"), $("#card_salutation").val(e.salutation), $("#card_suffix").val(e.suffix), $("select[name=card_title]").val(e.title), $("#card_type").val(e.type), $("input[name=card_first]").val(e.first), $("input[name=card_middle]").val(e.middle), $("input[name=card_last]").val(e.last), $("input[name=card_popular]").val(e.popular), $("input[name=card_letsal]").val(e.letsal), $("input[name=card_occupation]").val(e.occupation), $("input[name=card_employer]").val(e.employer), $("input[name=hiddenimg]").val(e.picture), e.picture ? (document.getElementById("profilepicDisp").style.display = "block", document.getElementById("dispProfile").src = e.picturePath) : document.getElementById("dispProfile").src = "", $("input[name=card2_firm]").val(e.firm), $("input[name=card2_address1]").val(e.address1), $("input[name=card2_address2]").val(e.address2), $("input[name=card2_city]").val(e.city), $("input[name=card2_state]").val(e.state), $("input[name=card2_zip]").val(e.zip), $("input[name=card2_venue]").val(e.venue), $("input[name=card2_eamsref]").val(e.eamsref), $("input[name=card_business]").val(e.business), $("input[name=card_fax]").val(e.card_fax), $("input[name=card_home]").val(e.home), $("input[name=card_car]").val(e.car), $("input[name=card_email]").val(e.email), "" != t ? $("input[name=card_notes]").val(t[0].notes) : $("input[name=card_notes]").val(), $("input[name=card2_phone1]").val(e.phone1), $("input[name=card2_phone2]").val(e.phone2), $("input[name=card2_tax_id]").val(e.tax_id), $("input[name=card2_fax]").val(e.card_fax), $("input[name=card2_fax2]").val(e.card2_fax), $("input[name=card_social_sec]").val(e.social_sec), "NA" == e.birth_date ? $("input[name=card_birth_date]").val("") : $("input[name=card_birth_date]").val(moment(e.birth_date).format("MM/DD/YYYY")), $("input[name=card_licenseno]").val(e.licenseno), $("select[name=card_specialty]").val(e.specialty), $("input[name=card_mothermaid]").val(e.mothermaid), $("input[name=card_cardcode]").val(e.cardcode), $("input[name=card_firmcode]").val(e.firmcode), $("select[name=card_interpret]").val(e.interpret), $("select[name=card_language]").val(e.language), $("#form_datetime1").val(e.card_origchg + " - " + e.card_origdt), $("#form_datetime2").val(e.card_lastchg + " - " + e.card_lastdt), $("#form_datetime3").val(e.card2_origchg + " - " + e.card2_origdt), $("#form_datetime4").val(e.card2_lastchg + " - " + e.card2_lastdt), "" != t ? $("textarea[name=card_comments]").val(t[1].comments) : $("textarea[name=card_comments]").val(), $("textarea[name=card2_comments]").val(e.card2_comments), $("textarea[name=card2_mailing1]").val(e.mailing1), $("textarea[name=card2_mailing2]").val(e.mailing2), $("textarea[name=card2_mailing3]").val(e.mailing3), $("textarea[name=card2_mailing4]").val(e.mailing4), $("#new_case").val("parties"), $('input[name="card_cardcode"]').val(e.cardcode), $("#addcontact").modal({
                backdrop: "static",
                keyboard: !1
            }), $("#addcontact").modal("show"), $("#addRolodexForm select").trigger("change"), $.unblockUI()
        }
    })
}

function editCaseDetails(a, e) {
    $.ajax({
        url: baseUrl + "/cases/getAjaxCaseDetail",
        data: "caseno=" + a,
        method: "POST",
        dataType: "json",
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            if ("1970-01-01 05:30:00" != (a = a[0]).dateenter && "1899-12-30 00:00:00" != a.dateenter && "0000-00-00 00:00:00" != a.dateenter && null != a.dateenter) {
                var t = moment(a.dateenter).format("MM/DD/YYYY");
                $("#casedate_entered").val(t)
            }
            if ("1970-01-01 05:30:00" != a.dateopen && "1899-12-30 00:00:00" != a.dateopen && "0000-00-00 00:00:00" != a.dateopen && null != a.dateopen) {
                var d = moment(a.dateopen).format("MM/DD/YYYY");
                $("#casedate_open").val(d)
            }
            if ("1970-01-01 05:30:00" != a.followup && "1899-12-30 00:00:00" != a.followup && "0000-00-00 00:00:00" != a.followup && null != a.followup) {
                var i = moment(a.followup).format("MM/DD/YYYY");
                $("#casedate_followup").val(i)
            }
            if ("1970-01-01 05:30:00" != a.dateclosed && "1899-12-30 00:00:00" != a.dateclosed && "0000-00-00 00:00:00" != a.dateclosed && null != a.dateclosed) {
                var s = moment(a.dateclosed).format("MM/DD/YYYY");
                $("#casedate_closed").val(s)
            }
            if ("1970-01-01 05:30:00" != a.psdate && "1899-12-30 00:00:00" != a.psdate && "0000-00-00 00:00:00" != a.psdate && null != a.psdate) {
                var l = moment(a.psdate).format("MM/DD/YYYY");
                $("#casedate_psdate").val(l)
            }
            if ("1970-01-01 05:30:00" != a.d_r && "1899-12-30 00:00:00" != a.d_r && "0000-00-00 00:00:00" != a.d_r && null != a.d_r) {
                var n = moment(a.d_r).format("MM/DD/YYYY");
                $("#casedate_referred").val(n)
            }
            if (null != a.udfall2) {
                var r = a.udfall2.split(",");
                $("#closedfileno").val(r[0]), $("select[name=editOtherClass]").val(r[1])
            }
            $("#fileno").val(a.yourfileno), $("#editCaseVenue1").val(a.first), $("#editCaseVenue").val(a.venue), $("select[name=editCaseSubType]").val(a.udfall), $("#editCaseType").hasClass("select2Class") ? $("#editCaseType option:contains(" + a.casetype + ")").length ? $("select[name=editCaseType]").val(a.casetype) : a.casetype && $("#editCaseType").append("<option value='" + a.casetype + "' selected='seleceted'>" + a.casetype + "</option>") : $("#editCaseType").hasClass("cstmdrp") && a.casetype && $("#editCaseType").val(a.casetype), $("#editCaseStat").hasClass("select2Class") ? $("#editCaseStat option:contains(" + a.casetype + ")").length ? $("select[name=editCaseStat]").val(a.casetype) : a.casestat && $("#editCaseStat").append("<option value='" + a.casestat + "' selected='seleceted'>" + a.casestat + "</option>") : $("#editCaseStat").hasClass("cstmdrp") && a.casestat && $("#editCaseStat").val(a.casestat), $("#editCaseAttyResp").hasClass("select2Class") ? $("#editCaseAttyResp option:contains(" + a.atty_resp + ")").length ? $("select[name=editCaseAttyResp]").val(a.atty_resp) : a.atty_resp && $("#editCaseAttyResp").append("<option value='" + a.atty_resp + "' selected='seleceted'>" + a.atty_resp + "</option>") : $("#editCaseAttyResp").hasClass("cstmdrp") && a.atty_resp && $("#editCaseAttyResp").val(a.atty_resp), $("#editCaseAttyHand").hasClass("select2Class") ? $("#editCaseAttyHand option:contains(" + a.atty_hand + ")").length ? $("select[name=editCaseAttyHand]").val(a.atty_hand) : a.atty_hand && $("#editCaseAttyHand").append("<option value='" + a.atty_hand + "' selected='seleceted'>" + a.atty_hand + "</option>") : $("#editCaseAttyHand").hasClass("cstmdrp") && a.atty_hand && $("#editCaseAttyHand").val(a.atty_hand), $("#editCaseAttyPara").hasClass("select2Class") ? $("#editCaseAttyPara option:contains(" + a.para_hand + ")").length ? $("select[name=editCaseAttyPara]").val(a.para_hand) : a.para_hand && $("#editCaseAttyPara").append("<option value='" + a.para_hand + "' selected='seleceted'>" + a.para_hand + "</option>") : $("#editCaseAttyPara").hasClass("cstmdrp") && a.para_hand && $("#editCaseAttyPara").val(a.para_hand), $("#editCaseAttySec").hasClass("select2Class") ? $("#editCaseAttySec option:contains(" + a.sec_hand + ")").length ? $("select[name=editCaseAttySec]").val(a.sec_hand) : a.sec_hand && $("#editCaseAttySec").append("<option value='" + a.sec_hand + "' selected='seleceted'>" + a.sec_hand + "</option>") : $("#editCaseAttySec").hasClass("cstmdrp") && a.sec_hand && $("#editCaseAttySec").val(a.sec_hand), $("#case_ps").val(a.ps), $("#fileloc").val(a.location), $("select[name=case_reffered_by]").val(a.rb), void 0 !== a.category_events && (void 0 !== a.category_events.quickNote && $("#editcase_quicknote").val(a.category_events.quickNote), void 0 !== a.category_events.message1 && $("#caseedit_message1").val(a.category_events.message1), void 0 !== a.category_events.message2 && $("#caseedit_message2").val(a.category_events.message2), void 0 !== a.category_events.message3 && $("#caseedit_message3").val(a.category_events.message3)), $("#editcase_caption").val(a.caption1), $("#editcasedetail").modal("show"), $("#edit_case_details select").trigger("change"), $("a[href='#" + e + "']").trigger("click")
        }
    })
}

function setTaskValidation() {
    return $("#addTaskForm").validate({
        ignore: [],
        rules: {
            whofrom: {
                required: !0
            },
            whoto2: {
                required: !0
            },
            datereq: {
                required: !0
            },
            event: {
                required: !0,
                noBlankSpace: !0
            }
        }
    })
}

function deleteTask(a) {
    swal({
        title: "Alert",
        text: "Are you sure? Want to Remove This Task ",
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: !1
    }, function () {
        $.blockUI(), $.ajax({
            url: baseUrl + "/tasks/deleteRec",
            data: {
                taskId: a
            },
            method: "POST",
            success: function (a) {
                $.unblockUI();
                var e = $.parseJSON(a);
                1 == e.status ? (taskTable1.ajax.reload(), taskTable.ajax.reload(null, !1), swal("Success !", e.message, "success")) : swal("Oops...", e.message, "error")
            }
        })
    })
}

function completeTask(a) {
    swal({
        title: "Are you sure? Want to Set Task as Completed ",
        showCancelButton: !0,
        confirmButtonColor: "#1ab394",
        confirmButtonText: "Yes, Set as Complete!",
        closeOnConfirm: !1,
        showLoaderOnConfirm: !0
    }, function () {
        $.blockUI(), $.ajax({
            url: baseUrl + "/tasks/completedTask",
            data: {
                taskId: a
            },
            method: "POST",
            success: function (a) {
                $.unblockUI();
                var e = $.parseJSON(a);
                1 == e.status ? (swal("Success !", e.message, "success"), taskTable.ajax.reload(null, !1)) : swal("Oops...", e.message, "error")
            }
        })
    })
}

function editTask(a) {
    $.blockUI(), $.ajax({
        url: baseUrl + "/tasks/getTask",
        data: {
            taskId: a
        },
        method: "POST",
        success: function (e) {
            var t = $.parseJSON(e);
            $.unblockUI(), 0 == t.status ? swal("Oops...", t.message, "error") : ($("#addtask1 .modal-title").html("Edit Task"), $("input[name=taskid]").val(a), $("select[name=whofrom]").val(t.whofrom), $("select[name=whoto3]").val(t.whoto), $("input[name=datereq]").val(t.datereq), $("textarea[name=event]").val(t.event), $("select[name=color]").val(t.color), $("select[name=priority]").val(t.priority), $("select[name=typetask]").val(t.typetask), $("select[name=phase]").val(t.phase), 1 == t.notify ? $("input[name=notify]").iCheck("check") : $("input[name=notify]").iCheck("uncheck"), 1 == t.reminder ? ($('input[type="checkbox"][name="reminder1"]').prop("checked", !0).change(), $(".rdate").val(t.popupdate), $(".rtime").val(t.popuptime), $(".rmdt").show()) : ($('input[type="checkbox"][name="reminder1"]').prop("checked", !1).change(), $(".rmdt").hide()), $("#addtask1").modal({
                backdrop: "static",
                keyboard: !1
            }), $("#addtask1").modal("show"), $("#addTaskForm1 select").trigger("change"))
        }
    })
}

function setPartyViewData(a, e) {
    $(".edit-party").show(), $.ajax({
        url: baseUrl + "/cases/getAjaxCardDetails",
        method: "POST",
        dataType: "json",
        data: {
            cardcode: a,
            caseno: e
        },
        beforeSend: function (a) {},
        complete: function (a, e) {},
        success: function (t) {
            if (0 == t)
                swal({
                    title: "Alert",
                    text: "Oops! Some error has occured while fetching details. Please try again later.",
                    type: "warning"
                });
            else {
                if (t.comment_per)
                    var d = $.parseJSON(t.comment_per);
                else
                    d = "";
                $(".edit-party").attr("data-casecard", a), $(".edit-party").attr("data-case", e), $("#party_name").html(t.fullname), $("#party_name").data("cardcodeWarning", t.cardcode), $("#party_type").data("cardcodeWarning", t.cardcode), $("#partiesCardCode").val(t.cardcode), $("#party_type").html(t.type), $("#firm_name").html(t.firm), $(".addressVal").attr("href", "https://maps.google.com/?q=" + t.address1 + t.address2), $("#party_address1").html(t.address1), $("#party_address2").html(t.address2), $("#party_city").html(t.city), $("#party_phn_home").html(t.home), $("#party_phn_office").html(t.business), $("#party_fax").html(t.fax), $("#party_email").html(t.email), $("#party_side").html(t.side), $("#party_offc_no").html(t.officeno), $("#party_notes").html(""), t.eamsref ? $("#party_eams").html(t.name + " " + t.eamsref) : $("#party_eams").html(""), $("#party_cell").html(t.car), $("#party_beeper").html(t.beeper), $("#party_notes").html(t.notes), "" != t.comment_bus ? ($(".busComment").css({
                    visibility: "visible"
                }), $("#comment_bus").html(t.comment_bus)) : ($(".busComment").css({
                    visibility: "hidden"
                }), $("#comment_bus").html(t.comment_bus)), t.comment_per && d[1] ? ($(".busComment").css({
                    visibility: "visible"
                }), $("#comment_per").html(d[1].comments)) : ($(".busComment").css({
                    visibility: "hidden"
                }), $("#comment_per").html("")), t.atty_hand && ($("#case_act_atty option:contains(" + t.atty_hand + ")").length ? $("select[name=case_act_atty]").val(t.atty_hand) : $("#case_act_atty").append("<option value='" + t.atty_hand + "' selected='seleceted'>" + t.atty_hand + "</option>"))
            }
        }
    })
}

function category_list(a, e) {
    var t = "";
    t = 1 == $("#activity_list").prop("checked") ? 1 : 0, $.ajax({
        url: baseUrl + "/cases/getcategoryeventlist",
        method: "POST",
        data: {
            category_list: a,
            case_id: e,
            status: t
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            categoryeventlist.clear();
            var e = $.parseJSON(a);
            $.each(e, function (a, e) {
                var t = "";
                t += '<tr class="categorylistevent" data-event="' + e.event + '" data-category=' + e.category + "><td>" + e.event + "</td>", t += "</tr>", categoryeventlist.row.add($(t))
            }), categoryeventlist.draw()
        }
    })
}

function attachParty(a) {
    var e = $("#hdnCaseId").val();
    $("#casepartyDetaillist").modal("show"), $.ajax({
        url: baseUrl + "/cases/getcasepartyDetails",
        method: "POST",
        data: {
            caseno: e
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (e) {
            casepartylist_data.clear();
            var t = $.parseJSON(e);
            t.length < 1 && location.reload(), $.each(t, function (e, t) {
                var d = t.salutation + " " + t.first + " " + t.middle + " " + t.last;
                if (t.state)
                    var i = t.state;
                else
                    i = "";
                if (t.zip)
                    var s = t.zip;
                else
                    s = "";
                if (t.city)
                    var l = t.city;
                else
                    l = "";
                if (t.address1)
                    var n = t.address1;
                else
                    n = "";
                if (t.firm)
                    var r = t.firm;
                else
                    r = "";
                if (t.fax)
                    var c = t.fax;
                else
                    c = "";
                if (t.phone1)
                    var o = t.phone1;
                else
                    o = "";
                var m = "";
                m += '<tr class="casepartydetails"  data-first="' + t.first + '" data-state="' + i + '" data-zip="' + s + '"data-last="' + t.last + '" data-active_data="' + a + '"data-social_sec="' + t.social_sec + '"data-fax="' + c + '"data-phone="' + o + '"data-salutation="' + t.salutation + '" ><td id="name">' + d + "</td>", m += '<td id="firm">' + r + "</td>", m += "<td>" + l + "</td>", m += "<td>" + n + "</td>", m += "</tr>", casepartylist_data.row.add($(m))
            }), casepartylist_data.draw()
        }
    })
}

function caseDetails(a) {
    $.ajax({
        url: baseUrl + "/cases/getCasrcardDetails",
        method: "POST",
        data: {
            cardCode: a
        },
        success: function (a) {
            $("#viewAllCases").html(a), $("#caseCardDetails").modal("show")
        }
    })
}

function fileCheck(a) {
    if (-1 == $.inArray($(a).val().split(".").pop().toLowerCase(), ["jpeg", "jpg", "png", "gif", "bmp"]))
        return document.getElementById("imgerror").innerHTML = "Only '.jpeg','.jpg', '.png', '.gif', '.bmp' formats are allowed.", !1
}
$(document.body).on("ifToggled", "input#field_ct1", function () {
    $(this).is(":checked") ? ($("#field_doi2").removeAttr("readonly"), $("#field_adj1c").mask("CT - 99/99/9999 ", {
        placeholder: "CT - mm/dd/yyyy"
    })) : ($("#field_doi2").prop("readonly", !0), $("#field_adj1c").val(""))
}), $(document.body).on("change", "input#field_doi", function () {
    if ($("#field_ct1").is(":checked"))
        $("#field_doi2").removeAttr("readonly"), $("#field_adj1c").mask("CT - 99/99/9999 ", {
            placeholder: "CT - mm/dd/yyyy"
        });
    else {
        var a = $("#field_doi").val();
        $("#field_adj1c").val(a)
    }
}), $(function () {
    $("#relatedcasenumber,.caseno").autocomplete({
        source: baseUrl + "/cases/autosearch/" + CaseHID
    })
}), $(document).ready(function () {
    ProspectListDatatable = $("#ProspectListDatatable").DataTable({scrollX:true}), $("input.numberinput").bind("keypress", function (a) {
        return !(8 != a.which && 0 != a.which && (a.which < 48 || a.which > 57) && 46 != a.which)
    });
    $("#calYearRange").val();
    venue_table_data = $("#venue_table").DataTable(), categoryeventlist = $("#caseEventlist").DataTable(), casepartylist_data = $("#casePartylist").DataTable(), rolodaxdataTable = $(".dataTables-rolodex").DataTable(), $("#print_datepicker").hide(), $(".i-checks").iCheck({
        checkboxClass: "icheckbox_square-green",
        radioClass: "iradio_square-green"
    }), $(".datepicker").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        endDate: new Date
    }), $("#datepicker").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        endDate: new Date
    }), $(".dob").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        endDate: "+0d"
    }), $("#taskdatepicker").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }), $("#taskdatepicker2").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }), $("#caldate").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }), $(".rdate").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    });
    (new Date).getHours();
    var a = (new Date).toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
    $("#caltime").val(a), /*$("#caltime").mdtimepicker({
        autoclose: !0
    }), $(".rtime").mdtimepicker({
        autoclose: !0
    }), $("#oletime").mdtimepicker({
        autoclose: !0,
        default: a
    }),*/ $("#tickledate").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }),/* $("#tickletime").mdtimepicker({
        autoclose: !0
    }),*/ $(".preinjurydate").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        endDate: new Date
    }), $(".injurydate").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }), $("#field_adj1a").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        endDate: new Date
    }), $("#start_printData").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0
    }).on("changeDate", function (a) {
        $("#end_printData").datetimepicker("setStartDate", a.date)
    }), $("#end_printData").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0
    }).on("changeDate", function (a) {
        $("#start_printData").datetimepicker("setEndDate", a.date)
    }), $("select[name=print_type]").on("change", function () {
        4 == this.value || 5 == this.value ? $("#print_datepicker").show() : ($("#start_printData").val("").datetimepicker("update"), $("#end_printData").val("").datetimepicker("update"))
    }), $("#casedate_followup").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }), "rolodex" != uri_seg && "dashboard" != uri_seg && "email" != uri_seg && "case" != uri_seg && (taskTable = $(".task-dataTable").DataTable({
        processing: !0,
        serverSide: !0,
		stateSave: true,
		stateSaveParams: function (settings, data) {
			data.search.search = "";
			data.start = 0;
			delete data.order;
			data.order = [0, "desc"];
		},
        bFilter: !0,
        order: [
            [0, "DESC"]
        ],
        autoWidth: !1,
        ajax: {
            url: baseUrl + "/tasks/getTasksData",
            type: "POST",
            data: function (a) {
                var e = $("ul#task-navigation").find("li.active").attr("id");
                if (1 == $("#task_reminder").is(":checked"))
                    var t = 1;
                else
                    t = "";
                var d = {
                    caseno: $("#caseNo").val()
                };
                d.taskBy = e, d.reminder = t, $.extend(a, d)
            }
        },
        fnRowCallback: function (a, e, t, d) {
            if ($(a).attr("id", e[9]), "" != e[e.length - 2]) {
                var i = e[e.length - 2].split("-");
                "" != i[0] ? $(a).css({
                    "background-color": i[0],
                    color: i[1]
                }) : $(a).css({
                    "background-color": "#ffffff",
                    color: "#000000"
                })
            }
        },
        columnDefs: [{
                render: function (a, e, t) {
                    if (null != a) {
                        var d = "",
                                i = "";
                        "" == t[8] && (t[1] === $.cookie("username") && (i = '<button class="btn btn-danger btn-circle-action btn-circle" title="Delete" type="button" onClick="deleteTask(' + a + ')"><i class="fa fa-trash"></i></button>'), d = '<button class="btn btn-info btn-circle-action btn-circle" title="Edit" type="button" onClick="editTask(' + a + ')"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;' + i, d += '&nbsp;&nbsp;<button class="btn btn-primary btn-circle-action btn-circle" title="Complete" type="button" onClick="completeTask(' + a + ')"><i class="fa fa-check"></i></button>', 0 != t[11] && (d += '&nbsp;&nbsp;<a class="btn btn-primary btn-circle-action btn-circle" title="PullCase" target="_blank"  href="' + baseUrl + "/cases/case_details/" + t[11] + '"><i class="fa fa-folder-open"></i></a>'))
                    }
                    return d
                },
                targets: 9,
                bSortable: !1,
                bSearchable: !1
            }, {
                targets: 6,
                className: "textwrap-line"
            }]
    })), $("#addtask.modal").on("hidden.bs.modal", function () {
        $("#addtask h4.modal-title").text("Add a Task"), $("#addTaskForm")[0].reset();
        setTaskValidation();
        $("input[name=taskid]").val(0), $('#addtask a[href="#name"]').tab("show")
    }), setTaskValidation(), $(".task-dataTable tbody").on("click", "tr", function () {
        $(this).hasClass("table-selected") ? ($(this).removeClass("table-selected"), selectedArray.splice($.inArray(this.id, selectedArray), 1)) : selectedArray.push(this.id)
    }), setCalendarValidation(), $.fn.modal.Constructor.prototype.enforceFocus = function () {}, $(".contact").mask("(000) 000-0000"), $(".social_sec").mask("000-00-0000"), $("#addcontact.modal").on("show.bs.modal", function () {
        document.getElementById("imgerror").innerHTML = "", document.getElementById("hiddenimg").value = "", $(".chosen-language").chosen()
    }), $('#addRolodexForm a[data-toggle="tab"]').on("click", function (a) {
        setValidation(), a.preventDefault()
    }), $("#addcontact.modal").on("hidden.bs.modal", function () {
        $("#addcontact.modal-title").text("Add New Contact"), $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
            this.value = ""
        }), document.getElementById("profilepicDisp").style.display = "none", document.getElementById("dispProfile").src = "", $('#addcontact a[href="#name"]').tab("show"), setValidation().resetForm()
    }), $('#addcalendar a[data-toggle="tab"]').on("click", function (a) {
        if ($("#cal_typ").length > 0) {
            var e = $("#cal_typ").val();
            $("#todo").val(e).trigger("change")
        }
        if (!$("#calendarForm").valid() && "0" == $('input[name="eventno"]').val())
            return a.preventDefault(), !1
    }), $("#addcalendar.modal").on("show.bs.modal", function () {
        $("#CalAddEvt").attr("data-attr")
    }), $("#addcalendar.modal").on("hidden.bs.modal", function () {
        $("#addcalendar h4.modal-title").text("Add Appointment/Event To Calendar"), $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
            this.value = ""
        }), setCalendarValidation().resetForm(), $("input[name=eventno]").val(0), $('#addcalendar a[href="#general1"]').tab("show"), $("#addcalendar .tab-content .tab-pane").not("#general1").removeClass("active"), $("#addcalendar .tab-content div#general1").addClass("active")
    }), $("#addContact").on("click", function () {
        var a = $('input[name="card_cardcode"]').val();
        if ("" == a && !$("#addRolodexForm").valid())
            return !1;
        if (!$("#addRolodexForm").valid())
            return !1;
        $.blockUI();
        var e = baseUrl + "/rolodex/addCardData";
        return "" != $('input[name="card_cardcode"]').val() && (e = baseUrl + "/rolodex/editCardData"), $.ajax({
            url: e,
            method: "POST",
            data: $("#addRolodexForm").serialize(),
            success: function (e) {
                var t = $.parseJSON(e),
                        d = t.editcardid;
                if ("" != $("#profilepic").val()) {
                    var i = document.getElementById("hiddenimg").value,
                            s = "",
                            l = $("#profilepic").prop("files")[0],
                            n = new FormData;
                    n.append("file", l), $.ajax({
                        url: baseUrl + "/rolodex/updateprofilepic/" + d + "/" + i,
                        dataType: "text",
                        cache: !1,
                        contentType: !1,
                        processData: !1,
                        data: n,
                        type: "post",
                        success: function (a) {
                            if (a) {
                                var e = baseUrl + "/assets/rolodexprofileimages/" + (s = a);
                                setTimeout(function () {
                                    null != s && (0 == $("#applicantImage").length ? $(".client-img").find("img.img-circle").attr("src", e) : ($("#applicantImage").remove(), $(".client-img").append('<center><img class="img-circle" height="67px" src="' + e + '" /></center>')))
                                }, 500)
                            }
                        },
                        error: function (a) {}
                    })
                }
                if ($.unblockUI(), "1" == t.status) {
                    if ($("#addRolodexForm")[0].reset(), $("#addcontact").modal("hide"), $("#addcontact .modal-title").html("Add New Contact"), 0 != t.caseid || "undefined" != t.caseid) {
                        $("#hdnCaseId").val();
                        if (0 != t.caseid && 1 == $("#new_case").val()) {
                            var r = baseUrl + "/cases/case_details/" + t.caseid;
                            setTimeout(function () {
                                window.location.href = r
                            }, 3e3)
                        }
                        "" == a && "parties" != $("#new_case").val() || $.ajax({
                            url: baseUrl + "/cases/case_details",
                            method: "POST",
                            data: {
                                caseId: $("#hdnCaseId").val()
                            },
                            success: function (a) {
                                var e = $.parseJSON(a);
                                setTimeout(function () {
                                    if ($(".applicant_name").html("<b>" + e.display_details.salutation + " " + e.display_details.first + " " + e.display_details.last + "</b>"), e.display_details.popular) {
                                        if (e.display_details.popular.length < 10)
                                            var a = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + e.display_details.popular + "</b></span><br/>";
                                        else
                                            a = '<br/><span class = "f-size12 marg-bot5 popular"><b>goes by ' + e.display_details.popular.substr(1, 10) + "..</b></span><br/>";
                                        $(".popular").length > 0 ? $(".popular").replaceWith(a) : $(".applicant_name").after(a)
                                    }
                                    $(".applicant_address").html(e.display_details.address1), $(".applicant_address2").html(e.display_details.city + " " + e.display_details.state + " " + e.display_details.zip), $("#address_map").attr("href", "https://maps.google.com/?q=" + e.display_details.address1 + e.display_details.address2 + " " + e.display_details.city + " " + e.display_details.state + " " + e.display_details.zip), $(".home").html(e.display_details.home), "" == e.display_details.car ? $(".business").html(e.display_details.business) : $(".business").html(e.display_details.car), $(".email").html(e.display_details.email);
                                    var t = new Date(e.display_details.birth_date),
                                            d = new Date,
                                            i = Math.floor((d - t) / 315576e5);
                                    $(".birth_date").html("Born on " + moment(e.display_details.birth_date).format("MM/DD/YYYY") + " ," + i + "years old"), $("#party_name").html(e.display_details.first + " " + e.display_details.last), $("#party_beeper").val(e.display_details.beeper);
                                    var s = $("table#party-table-data").find(".table-selected");
                                    s = s.attr("id"), $("#" + s).find(".parties_name").html(e.display_details.first + ", " + e.display_details.last)
                                }, 1e3)
                            }
                        })
                    }
                    rolodaxdataTable.draw(!1)
                } else
                    swal("Oops...", t.message, "error")
            }
        }), !1
    }), $("#getPriority").on("change", function () {
        var a = $(this).val();
        taskTable.columns(5).search(JSON.stringify({
            priority: a
        })).draw()
    }), $("#gettaskStatus").on("change", function () {
        var a = $(this).val();
        taskTable.columns(8).search(JSON.stringify({
            "task-status": a
        })).draw()
    }), $("#btn-taskPrint").on("click", function () {
        $.ajax({
            url: baseUrl + "/tasks/printTask",
            method: "POST",
            data: $("form#taskPrint").serialize() + "&searchBy=" + JSON.stringify(searchBy) + "&selectedData=" + selectedArray,
            success: function (a) {
                $(a).printThis({
                    debug: !1,
                    header: "<h2><center><?php echo APPLICATION_NAME; ?> | Tasks </center></h2>"
                })
            }
        })
    }), $("#billing_defaults").validate({
        submitHandler: function (a, e) {
            e.preventDefault(), $("#caseNo").val($("#hdnCaseId").val());
            var t = $("form#billing_defaults").serialize();
            $.ajax({
                url: baseUrl + "/cases/setBillingDefaults",
                method: "POST",
                data: t,
                beforeSend: function (a) {
                    $.blockUI()
                },
                complete: function (a, e) {
                    $.unblockUI()
                },
                success: function (a) {
                    $("#adddefault").modal("hide");
                    var e = $.parseJSON(a);
                    if (0 != e) {
                        if ("" == $("#activity_no").val()) {
                            "1970-01-01 05:30:00" != e.freezedt && "1899-12-30 00:00:00" != e.freezedt && "0000-00-00 00:00:00" != e.freezedt && ($(".freezeUntil").html(e.default_freeze_until), $(".freezeUntil").parent().removeClass("hidden")), $("#activity_entry_type").val(e.default_entry_type).trigger("change");
                            var t = $("#activity_entry_type option:selected").text();
                            $("#activity_short_desc").html(t), $("#activity_event_desc").html(t), $("#activity_teamno").val(e.default_teamno), $("#activity_staff").val(e.default_staff).trigger("change"), "1970-01-01 05:30:00" != e.default_payment_dt && "1899-12-30 00:00:00" != e.default_payment_dt && "0000-00-00 00:00:00" != e.default_payment_dt && ($(".dateoflastpayment").html(e.default_payment_dt), $(".dateoflastpayment").parent().removeClass("hidden")), $("#activity_latefee").html(e.default_late_fee), $("#activity_bi_rate").val(e.default_hr_rate)
                        }
                        swal("Success !", "Billing defaults set successfuly", "success")
                    } else
                        swal("Failure !", "Fail to set billing defaults", "error")
                }
            })
        }
    });
/*    $('.confirm').on('click',function(){
        $('#saveCompletedTask').removeAttr("disabled");
    });*/
    $("#addTaskForm1").validate({
        ignore: [],
        rules: {
            whofrom: {
                required: !0
            },
            whoto3: {
                required: !0
            },
            datereq: {
                required: !0
            },
            event: {
                required: !0,
                noBlankSpace: !0
            }
        }
    });
    $("#add_new_case_activity").validate({
        ignore: [],
        rules: {
            activity_event: {
                required: !0
            }
        },
        messages: {
            activity_event: {
                required: "Event Description is Required"
            }
        },
        invalidHandler: function (a, e) {
            var t = e.invalidElements().get(0),
                    d = $(t).closest(".tab-pane").attr("id");
            $('#addcaseact a[href="#' + d + '"]').tab("show")
        }
    }), $.validator.addMethod("phoneUSA", function (a, e) {
        return a = a.replace(/\s+/g, ""), this.optional(e) || a.length > 9 && a.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    }, "Please specify a valid phone number"), $("#prospectForm").validate({
        ignore: [],
        rules: {
            pro_first: {
                required: !0
            },
            pro_last: {
                required: !0
            },
            pro_type: {
                required: !0
            },
            pro_business: {
                phoneUSA: !0
            },
            pro_home: {
                phoneUSA: !0
            },
            pro_cell: {
                phoneUSA: !0
            },
            pro_social_sec: {
                ssId: !0
            },
            pro_state: {
                stateUS: !0
            },
            pro_zip: {
                zipcodeUS: !0
            },
            pro_caseno: {
                remote: {
                    url: baseUrl + "/Prospects/CheckCaseno",
                    type: "post",
                    data: {
                        pro_caseno: function () {
                            return $('#prospectForm :input[name="pro_caseno"]').val()
                        }
                    }
                },
                digits: !0
            }
        },
        messages: {
            pro_first: {
                required: "Please provide first name"
            },
            pro_last: {
                required: "Please provide last name"
            },
            pro_type: {
                required: "Please select Category"
            },
            pro_caseno: {
                remote: "This caseno is not exist"
            },
            pro_state: {
                stateUS: "Invalid USA state abbreviation"
            }
        },
        submitHandler: function (a) {
            return !1
        },
        invalidHandler: function (a, e) {
            var t = e.invalidElements().get(0),
                    d = $(t).closest(".tab-pane").attr("id");
            $('#proceedprospects a[href="#' + d + '"]').tab("show")
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "pro_type") {
                error.insertAfter(element.next());
            } else {
                error.insertAfter(element);
            }
        }
    }), $(".dtpickDisDayofWeek").datetimepicker({
        daysOfWeekDisabled: [0],
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }), $(".datepickerClass").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        endDate: new Date
    }), $("#casedate_closed").datetimepicker({
        format: "mm/dd/yyyy",
        minView: 2,
        autoclose: !0,
        startDate: new Date
    }), $("#add_case_time").mdtimepicker({
        autoclose: !0,
        twelvehour: !0
    }), $("#activity_entry_type").change(function () {
        var a = $("#activity_entry_type option:selected").data("key");
        $(".billingtab").addClass("hidden"), $("#" + a).removeClass("hidden")
    });
    var e = $("#activity_entry_type option:selected").data("key");
    $(".billingtab").addClass("hidden"), $("#" + e).removeClass("hidden"), Dropzone.options.addNewCaseActivity = {
        autoProcessQueue: !1,
        uploadMultiple: !0,
        parallelUploads: 11,
        maxFiles: 11,
        addRemoveLinks: !0,
        uploadprogress: !0,
        enqueueForUpload: !1,
        paramName: "document",
        acceptedFiles: "image/jpeg,application/docx,application/pdf,text/plain,application/msword",
        init: function () {
            myDropzone = this, this.element.querySelector("button[type=submit]").addEventListener("click", function (a) {
                $.blockUI(), $("#caseActNo").val($("#hdnCaseId").val()), a.preventDefault(), a.stopPropagation(), 1 == $(this).closest("#add_new_case_activity").valid() ? myDropzone.getQueuedFiles().length > 0 ? myDropzone.processQueue() : myDropzone.uploadFiles([]) : $.unblockUI()
            }), this.on("maxfilesreached", function () {
                swal("Failure !", "Max file allowed exceeded", "error"), null != this.files[1] && this.removeFile(this.files[0])
            }), this.on("addedfile", function (a) {
                var e, t;
                if (this.files.length)
                {
                    if($('#activity_event').val() == '') {
                        $('#activity_event').val(file.name.replace(".msg", ""));
                    }
                    for (e = 0, t = this.files.length; e < t - 1; e++)
                    {
                        this.files[e].name === a.name && this.removeFile(a)
                    }
                }
            }), this.on("success", function (a, e) {}), this.on("sendingmultiple", function () {}), this.on("successmultiple", function (a, e) {
                $.unblockUI();
                var t = "",
                        d = "";
                $("#addcaseact").modal("hide");
                var i = JSON.parse(e),
                        s = baseUrl + "/assets/clients/" + $("#caseNo").val() + "/" + i.file_name;
                d = 0 == a.length ? a.length + i.fileExist > 1 ? '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + i.actno + ' href="javascript:void(0);"><i class = "icon fa fa-eye"></i> View All</a>' : a.length + i.fileExist > 0 ? '<a class="btn btn-xs btn-primary marginClass view_attach"  download=' + i.file_name + "  data-actid=" + i.actno + ' href="' + s + '"><i class = "icon fa fa-eye"></i> View</a>' : "" : i.fileExist > 1 ? '<a class="btn btn-xs btn-primary marginClass view_all_attach" data-actid=' + i.actno + ' href="javascript:void(0);"><i class = "icon fa fa-eye"></i> View All</a>' : i.fileExist > 0 ? '<a class="btn btn-xs btn-primary marginClass view_attach" download=' + i.file_name + " data-actid=" + i.actno + ' href="' + s + '"><i class = "icon fa fa-eye"></i> View</a>' : "", t += "<tr id=caseAct" + i.actno + "><td>" + i.actData.add_case_cal + "</td>", t += "<td>" + i.actData.add_case_time + "</td>", t += "<td>" + i.actData.activity_event + "</td>", t += "<td>" + i.actData.last_ini + "</td>", t += "<td>" + i.actData.orignal_ini + "</td>", t += "<td>" + d + "</td>", t += "<td>", t += "<a class='btn btn-xs btn-info marginClass activity_edit' data-actid=" + i.actno + " href = 'javascript:;' title='Edit'><i class = 'icon fa fa-edit'></i> Edit</a>", t += "<a class='btn btn-xs btn-danger marginClass activity_delete' data-actid=" + i.actno + " href = 'javascript:;' title='Delete'><i class = 'icon fa fa-times'></i> Delete</a>", t += "</td>", t += "</tr>", swal({
                    title: "Success!",
                    text: "Activity List updated successfuly",
                    type: "success"
                }, function () {
                    location.reload()
                }), 2 == i.status ? (caseactTable.rows("[id=caseAct" + i.actno + "]").any() && caseactTable.rows($("tr#caseAct" + i.actno)).remove(), caseactTable.row.add($(t)).draw(), swal({
                    title: "Success!",
                    text: "Activity updated successfuly",
                    type: "success"
                }, function () {
                    location.reload()
                })) : 1 == i.status ? caseactTable.row.add($(t)).draw() : swal("Failure !", "Fail to add activity", "error"), document.getElementById("add_new_case_activity").reset()
            }), this.on("errormultiple", function (a, e) {}), this.on("completemultiple", function (a) {
                this.removeAllFiles(!0)
            })
        }
    }, rolodexSearchDatatable = $("#rolodexPartiesResult").DataTable({
        stateSave: true,
		stateSaveParams: function (settings, data) {
			data.search.search = "";
			data.start = 0;
			delete data.order;
		},
		lengthMenu: [5, 10, 25, 50, 100]
    }), rolodexSearch = $("#rolodexsearchResult_data").DataTable({
        scrollX: !0,
		stateSave: true,
		stateSaveParams: function (settings, data) {
			data.search.search = "";
			data.start = 0;
			delete data.order;
		},
        lengthMenu: [5, 10, 25, 50, 100]
    }), duplicateDatatable = $("#duplicateEntries").DataTable({ 
			stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
			}, 
		}), $("#rolodex_search_parties_form").validate({
        rules: {
            rolodex_search_text: {
                required: !0
            },
            rolodex_search_parties: {
                required: !0
            }
        },
        messages: {
            rolodex_search_text: {
                required: "Please provide search text"
            },
            rolodex_search_parties: {
                required: "Please provide search by"
            }
        },
        submitHandler: function (a, e) {
            e.preventDefault();
            var t = $("form#rolodex_search_parties_form").serialize();
            $.ajax({
                url: baseUrl + "/cases/searchForRolodex",
                method: "POST",
                data: t,
                beforeSend: function (a) {
                    $.blockUI()
                },
                complete: function (a, e) {
                    $.unblockUI()
                },
                success: function (a) {
                    var e = $("#rolodex_search_text").val().substr(0, 1);
                    $("input[name='rolodex_search_parties']:checked").val();
                    if ("<" == e) {
                        var t = $.parseJSON(a);
                        $("#searched_party_fullname").val(t[0].fullname), $("#searched_party_firmname").val(t[0].firm), $("#searched_party_type").html(t[0].type), $("#searched_party_cardnumber").html(t[0].cardcode), $(".addSearchedParties").trigger("click")
                    }
                    rolodexSearchDatatable.clear();
                    var d = $.parseJSON(a);
                    $.each(d, function (a, e) {
                        var t = "";
                        t += "<tr class='viewPartiesDetail' data-cardcode=" + e.cardcode + "><td>" + e.name + "</td>", t += "<td>" + e.firm + "</td>", t += "</tr>", rolodexSearchDatatable.row.add($(t))
                    }), rolodexSearchDatatable.draw()
                }
            })
        }
    }), $("#companylistForm").validate({
        rules: {
            rolodex_search_text_name: {
                required: !0,
                noBlankSpace: !0
            }
        },
        messages: {
            rolodex_search_text_name: {
                required: "Please enter rolodex name "
            }
        },
        submitHandler: function (a, e) {
            e.preventDefault()
        }
    }), $("#addNewCase").validate({
        rules: {
            first_name: {
                required: !0,
                noBlankSpace: !0
            },
            last_name: {
                required: !0,
                noBlankSpace: !0
            }
        },
        messages: {
            first_name: {
                required: "Please provide first name"
            },
            last_name: {
                required: "Please provide last name"
            }
        },
        submitHandler: function (a, e) {
            setValidation(), e.preventDefault(), $("#caseNo").val($("#hdnCaseId").val());
            var t = $("form#addNewCase").serialize();
            $.ajax({
                url: baseUrl + "/cases/checkDuplicateCase",
                method: "POST",
                data: t,
                beforeSend: function (a) {
                    $.blockUI()
                },
                complete: function (a, e) {
                    $.unblockUI()
                },
                success: function (a) {
                    var e = $.parseJSON(a);
                    duplicateDatatable.rows().remove().draw(), "1" == e.status ? ($("#addcase").modal("hide"), $.each(e.data, function (a, e) {
                        var t = "";
                        t += "<tr class='selectDuplicateData' data-cardcode=" + e.cardcode + ">", t += "<td>" + e.caseno + "</td>", t += "<td>" + e.type + "</td>", t += "<td>" + e.casestat + "</td>", t += "<td>" + e.dateenter + "</td>", t += "<td>" + e.social_sec + "</td>", t += "<td>" + e.casetype + "</td>", t += "<td>" + e.caption1 + "</td>", t += "</tr>", duplicateDatatable.row.add($(t))
                    }), duplicateDatatable.draw(), 1 == e.data.length && ($("#duplicateEntries tr.selectDuplicateData").addClass("highlight"), $("#edit_new_case").prop("disabled", !1), $("#attach_name").prop("disabled", !1)), $("input[name=first_name]").val($("#first_name").val()), $("input[name=last_name]").val($("#last_name").val()), $("#duplicateData").modal("show")) : "" != $("#prospect_id").val() ? $.ajax({
                        url: baseUrl + "/Prospects/getProspectsdata",
                        method: "POST",
                        data: {
                            prospect_id: $("#prospect_id").val()
                        },
                        beforeSend: function (a) {
                            $.blockUI()
                        },
                        complete: function (a, e) {
                            $.unblockUI()
                        },
                        success: function (a) {
                            var e = $.parseJSON(a);
                            $("#card_salutation").val(e.data.salutation), $("#card_suffix").val(e.data.suffix), $("#card_type").val(e.data.type), $("input[name=card_first]").val(e.data.first), $("input[name=card_last]").val(e.data.last), $("input[name=card2_address1]").val(e.data.address1), $("input[name=card2_city]").val(e.data.city), $("input[name=card2_state]").val(e.data.state), $("input[name=card2_zip]").val(e.data.zip), $("input[name=card_business]").val(e.data.business), $("input[name=card_home]").val(e.data.home), $("input[name=card_car]").val(e.data.cell), $("input[name=card_email]").val(e.data.email), $("input[name=card_social_sec]").val(e.data.social_sec), "NA" == e.data.birthdate ? $("input[name=card_birth_date]").val("") : $("input[name=card_birth_date]").val(moment(e.data.birthdate).format("MM/DD/YYYY")), $("#new_case").val(1), $("#prospect_id").val($("#prospect_id").val()), $("#addcase").modal("hide"), $("#addRolodexForm select").trigger("change"), $("select[name=card_language]").val("English"), $("select[name=card_language]").trigger("chosen:updated"), $("#addcontact").modal("show")
                        }
                    }) : ($("input[name=card_first]").val($("#first_name").val()), $("input[name=card_last]").val($("#last_name").val()), $("#new_case").val(1), $("#addcase").modal("hide"), $("#addRolodexForm select").trigger("change"), $("select[name=card_language]").val("English"), $("select[name=card_language]").trigger("chosen:updated"), $("#addcontact").modal("show"))
                }
            })
        }
    }), $("#savecase").on("click", function () {
        var a = $("form#edit_case_details").serialize(),
                e = $("#case_reffered_by option:selected").text();
        $("#editcase_caption").text();
        $.ajax({
            url: baseUrl + "/cases/editCase",
            method: "POST",
            data: a,
            beforeSend: function (a) {
                $.blockUI()
            },
            complete: function (a, e) {
                $.unblockUI()
            },
            success: function (a) {
                var t = $.parseJSON(a);
                $.unblockUI(), "1" == t.status ? (swal("Success !", t.message, "success"), $("#edit_case_details")[0].reset(), $("#editcasedetail").modal("hide"), $("#editcasedetail .modal-title").html("Edit Case")) : "0" == t.status && (swal("Warning !", t.message, "error"), $("#edit_case_details")[0].reset(), $("#editcasedetail").modal("hide"), $("#editcasedetail .modal-title").html("Edit Case")), $.ajax({
                    url: baseUrl + '/cases/case_details" ?>',
                    method: "POST",
                    data: {
                        caseId: $("#hdnCaseId").val()
                    },
                    success: function (a) {
                        var t = $.parseJSON(a);
                        $(".casetype").html(t.display_details.casetype), $(".location").html(t.display_details.location), $(".casestat").html(t.display_details.casestat), $(".yourfileno").html(t.display_details.yourfileno), $(".venue").html(t.display_details.first), $(".atty_resp").html(t.display_details.atty_resp), $(".para_hand").html(t.display_details.para_hand), $(".atty_hand").html(t.display_details.atty_hand), $(".sec_hand").html(t.display_details.sec_hand), t.display_details.caption1.length > 55 ? $(".caption #caption_details").html(t.display_details.caption1.substring(0, 55) + "...") : $(".caption #caption_details").html(t.display_details.caption1), $(".casesubtype").html(t.display_details.udfall), $("div.sticky.caption").hasClass("caption-closed") && $("div.sticky.caption").html(t.display_details.caption1 + " - CLOSED");
                        var d = "NA",
                                i = "NA",
                                s = "NA",
                                l = "NA",
                                n = "NA",
                                r = "NA";
                        d = "1899-12-31 00:00:00" == t.display_details.dateenter || "1899-12-30 00:00:00" == t.display_details.dateenter || "1970-01-01 00:00:00" == t.display_details.dateenter ? "NA" : moment(t.display_details.dateenter).format("MM/DD/YYYY"), i = "1899-12-31 00:00:00" == t.display_details.dateopen || "1899-12-30 00:00:00" == t.display_details.dateopen || "1970-01-01 00:00:00" == t.display_details.dateopen ? "NA" : moment(t.display_details.dateopen).format("MM/DD/YYYY"), s = "1899-12-31 00:00:00" == t.display_details.followup || "1899-12-30 00:00:00" == t.display_details.followup || "1970-01-01 00:00:00" == t.display_details.followup ? "NA" : moment(t.display_details.followup).format("MM/DD/YYYY"), l = "1899-12-31 00:00:00" == t.display_details.dateclosed || "1899-12-30 00:00:00" == t.display_details.dateclosed || "1970-01-01 00:00:00" == t.display_details.dateclosed ? "NA" : moment(t.display_details.dateclosed).format("MM/DD/YYYY"), n = "1899-12-31 00:00:00" == t.display_details.d_r || "1899-12-30 00:00:00" == t.display_details.d_r || "1970-01-01 00:00:00" == t.display_details.d_r ? "NA" : moment(t.display_details.d_r).format("MM/DD/YYYY"), r = "1899-12-31 00:00:00" == t.display_details.psdate || "1899-12-30 00:00:00" == t.display_details.psdate || "1970-01-01 00:00:00" == t.display_details.psdate || "" == t.display_details.psdate ? "NA" : moment(t.display_details.psdate).format("MM/DD/YYYY"), $(".dateenter").html(d), $(".dateopen").html(i), $(".followup").html(s), $(".d_r").html(n), $(".rb").html(e), $(".ps").html(r), $(".dateclosed").html(l), $(".notes").html(t.category_array.quickNote)
                    }
                })
            }
        })
    }), $("#parties_case_specific_data").validate({
        submitHandler: function (a, e) {
            e.preventDefault(), $("#caseNo").val($("#hdnCaseId").val());
            var t = $("form#parties_case_specific_data").serialize();
            $("#partiesCardCode").val(), $.ajax({
                url: baseUrl + "/cases/setPartiesCaseSpecific",
                method: "POST",
                data: t,
                beforeSend: function (a) {
                    $.blockUI()
                },
                complete: function (a, e) {
                    $.unblockUI()
                },
                success: function (a) {
                    var e = $.parseJSON(a);
                    "1" == e.status ? ($.ajax({
                        url: baseUrl + "/cases/getCaseSpecific",
                        method: "POST",
                        dataType: "json",
                        data: {
                            cardcode: $("#partiesCardCode").val(),
                            caseno: $("#caseNo").val()
                        },
                        beforeSend: function (a) {
                            $.blockUI()
                        },
                        complete: function (a, e) {
                            $.unblockUI()
                        },
                        success: function (a) {
                            $("#party_type").html(a[0].type), $("#party_side").html(a[0].side), $("#party_offc_no").html(a[0].officeno), $("#party_notes").html(a[0].notes)
                        }
                    }), swal("Success !", e.message, "success")) : swal("Alert !", e.message, "warning"), $("#caseSpecific").modal("hide")
                }
            })
        }
    }), $("#sfl").tooltip(), $("#addInjuryForm").validate({
        rules: {
            field_first: {
                required: !0
            },
            field_last: {
                required: !0
            },
            field_doi2: {
                required: function () {
                    return "" == $("#field_doi").val()
                }
            }
        },
        messages: {
            field_first: {
                required: "First name required"
            },
            field_last: {
                remote: "Last name required"
            },
            field_doi2: {
                required: "CT Dates is required."
            }
        }
    }), $("#send_mail_form1").validate({
        rules: {
            whoto1: {
                required: function () {
                    return "" == $("#emails1").val()
                }
            },
            mail_subject1: {
                required: !0
            }
        },
        messages: {
            whoto1: {
                required: "Please Select a User to send mail to"
            },
            mail_subject1: {
                required: "Please enter a Mail Subject"
            }
        },
        errorPlacement: function (a, e) {
            "whoto1" == $(e).attr("name") ? $(e).parent().append(a) : a.insertAfter(e)
        },
        submitHandler: function (a) {
            var e;
            e = $('select[name="whoto1"]').val();
            var t = $("#mail_subject1").val(),
                    d = $('input[name="mail_urgent1"]:checked').val(),
                    i = $('input[name="emails1"]').val(),
                    s = CKEDITOR.instances.mailbody1.getData(),
                    l = $("#filenameVal").val(),
                    n = $("#mailType1").val(),
                    r = $("#case_no1").val(),
                    c = new FormData;
            c.append("whoto", e), c.append("subject", t), c.append("urgent", d), c.append("ext_emails", i), c.append("mailbody", s), c.append("mailType", n), c.append("filenameVal", l), c.append("caseNo", r);
            var o = 0;
            return $.each($('input[name="afiles1[]"]'), function (a, e) {
                $.each(e.files, function (a, e) {
                    vaild_data = validateAttachFileExtension(e.name), c.append("files_" + o, e), o += 1
                })
            }), $.ajax({
                url: baseUrl + "/email/send_email",
                method: "POST",
                dataType: "json",
                cache: !1,
                contentType: !1,
                processData: !1,
                data: c,
                beforeSend: function (a) {
                    $.blockUI()
                },
                complete: function (a, e) {
                    $.unblockUI()
                },
                success: function (a) {
                    if ("true" == a.result)
                        swal({
                            title: "Success!",
                            text: "Mail is sent successfully.",
                            type: "success"
                        });
                    else if ("true" == a.result && "0" == a.ext_party)
                        swal({
                            title: "Alert",
                            text: "Oops! Some went wrong. Email wasn't send to External Party.",
                            type: "warning"
                        });
                    else {
                        if ("false" == a.result && "" != a.attachment_issue)
                            return swal({
                                title: "Alert",
                                text: "Oops! " + a.attachment_issue,
                                type: "warning"
                            }), !1;
                        swal({
                            title: "Alert",
                            text: "Oops! Some error has occured. Please try again later.",
                            type: "warning"
                        })
                    }
                    $("#CaseEmailModal").modal("hide"), caseactTable.draw()
                }
            }), !1
        }
    })
}), $(document.body).on("click", ".activity_edit", function () {
    var a = $(this).data("actid");
    caseActEdit(a)
}), $(document.body).on("change", "#default_freeze", function () {
    $("#default_freeze:checked").length ? $("#default_feeze_until").attr("disabled", !0) : $("#default_feeze_until").attr("disabled", !1)
}), $(document.body).on("click", ".complete_task", function () {
    var a = $(this).attr("data-id"),
            e = $(this).attr("data-event"),
            t = $(this).attr("data-caseno");
    t <= 0 ? completeTask(a) : ($("#case_event").val(e), $("#caseTaskId").val(a), $("#caseno").val(t), $("#TaskCompleted").modal("show"))
}), $(document.body).on("click", "#saveCompletedTask", function () {
/*$('#saveCompletedTask').attr('disabled','disabled');*/
    var a = $("#caseTaskId").val(),
            e = $("#case_event").val(),
            t = $("#case_extra_event").val(),
            d = $("#case_category").val(),
            i = $("#caseno").val();
    
    $.ajax({
        url: baseUrl + "/tasks/completedTaskWithCase",
        data: {
            taskId: a,
            case_event: e,
            case_extra_event: t,
            case_category: d,
            caseno: i
        },
        method: "POST",
        success: function (a) {
            $.unblockUI();
            var e = $.parseJSON(a);
            1 == e.status ? (swal("Success !", e.message, "success"), $("#TaskCompleted").modal("hide"), taskTable.ajax.reload(null, !1), window.reload()) : swal("Oops...", e.message, "error")
        }
    })
}), $(document.body).on("click", ".viewPartiesDetail", function () {
    $("#rolodexPartiesResult tr").removeClass("highlight"), $(this).addClass("highlight");
    var a = $("#caseNo").val();
    $.ajax({
        url: baseUrl + "/cases/viewSearchedPartieDetails",
        method: "POST",
        dataType: "json",
        data: {
            cardcode: $(this).data("cardcode"),
            caseno: a
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            0 == a ? swal({
                title: "Alert",
                text: "Oops! Some error has occured while fetching details. Please try again later.",
                type: "warning"
            }) : ($("#searched_party_fullname").val(a.fullname), $("#searched_party_firmname").val(a.firm), $("#searched_party_type").html(a.type), $("#searched_party_speciality").html(a.speciality), $("#searched_party_address").html(a.address1), $("#searched_party_city").html(a.city), $("#searched_party_cardnumber").html(a.cardcode), $("#searched_party_ssno").html(a.social_sec), $("#searched_party_email").html(a.email), $("#searched_party_licenseno").html(a.licenseno), $("#searched_party_homephone").html(a.home), $("#searched_party_business").html(a.business), $(".addSearchedParties").attr("disabled", !1), $(".editPartyCard").attr("disabled", !1))
        }
    })
}), $(document.body).on("click", ".addSearchedParties", function () {
    var a = $("#caseNo").val(),
            e = $("#searched_party_cardnumber").html(),
            t = $("#searched_party_type").html(),
            d = $("#searched_party_fullname").val(),
            i = $("#searched_party_firmname").val(),
            s = "";
    s = "" != i ? i : d, "rolodex-cardno" == $("input[name='rolodex_search_parties']:checked").val() && (s = d), $("#addparty").modal("hide"), $.ajax({
        url: baseUrl + "/cases/addSearchedPartyToCase",
        method: "POST",
        dataType: "json",
        data: {
            cardcode: e,
            caseno: a,
            type: t
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (d) {
            $("#parties table tr#tr-0").length < 1 && location.reload();
            var i = $("#party-table-data tr").length;
            "<h4>No Parties Found</h4>" == $(".party-in table td").html() && $(".party-in table td").remove();
            var l = "";
            l += '<tr class="" id="tr-' + i + '">', l += "<td>" + i + "</td>", l += "<td title=" + s + "><a class='view_party' href='javascript:void(0);' data-card=" + e + " data-case=" + a + ">" + s + "</a> </td>", l += "<td>" + t + "</td>", l += "<td>", l += "<a class='view_party' href='javascript:void(0);' data-card=" + e + " data-case=" + a + ">View</a> ", l += "<a class='remove_party' href='javascript:void(0);' data-card=" + e + " data-case=" + a + ">Delete</a>", l += "</td></tr>", table.row.add($(l)), table.draw(), $("table#party-table-data tr").hasClass("table-selected") && $("table#party-table-data tr").removeClass("table-selected"), $("tr.last").addClass("table-selected"), setPartyViewData(e, a)
        }
    })
}), $(document.body).on("click", "#change_name_warning", function () {
    $("#partiesNameChangeWarning").modal("hide"), editContact($("#party_name").data("cardcodeWarning"))
}), $(document.body).on("click", ".typeLink", function () {
    var a = $("#party_type").data("cardcodeWarning"),
            e = $("#caseNo").val();
    $.ajax({
        url: baseUrl + "/cases/getCaseSpecific",
        method: "POST",
        dataType: "json",
        data: {
            cardcode: a,
            caseno: e
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            $("#parties_case_specific").val(a[0].type), $("#parties_side").val(a[0].side), $("#parties_office_number").val(a[0].officeno), $("#parties_notes").val(a[0].notes), "1" != a[0].flags ? $("#parties_party_sheet").iCheck("uncheck") : $("#parties_party_sheet").iCheck("check")
        }
    }), $("#caseSpecific").modal("show")
}), $(document.body).on("click", ".editPartyCard", function () {
    var a = $("#searched_party_cardnumber").html();
    "" == a || "undefinded" == a ? swal({
        title: "Alert",
        text: "That card does not exist in the rolodex or cannot be edited at this time. Please try searching again.",
        type: "warning"
    }) : ($("#addcontact").attr("style", "z-index:9999 !important"), editContact(a))
}), $(document.body).on("click", "#attach_name", function () {
    var a = $("#duplicateEntries .highlight").data("cardcode"),
            e = $(".first_name").val(),
            t = $(".last_name").val();
    $.ajax({
        url: baseUrl + "/cases/attach_name",
        method: "POST",
        dataType: "json",
        data: {
            cardcode: a,
            first_name: e,
            last_name: t
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            var e = baseUrl + "/cases/case_details/" + a.caseno;
            $(location).attr("href", e)
        }
    })
}), $(document.body).on("click", ".selectDuplicateData", function () {
    $("#duplicateEntries tr").removeClass("highlight"), $(this).addClass("highlight"), "1" == $(".highlight").length && ($("#edit_new_case").prop("disabled", !1), $("#attach_name").prop("disabled", !1))
}), $(document.body).on("click", "#add_name_case", function () {
    return $("input[name=card_first]").val($(".first_name").val()), $("input[name=card_last]").val($(".last_name").val()), $("#new_case").val(1), $("#addcase").modal("hide"), $("#duplicateData").modal("hide"), $("#addcontact").modal("show"), !1
}), $(document.body).on("click", "#edit_new_case", function () {
    $("#duplicateData").modal("hide"), editContact($("#duplicateEntries .highlight").data("cardcode"))
}), $(document.body).on("click", ".addparty", function () {
    $(".addSearchedParties").attr("disabled", !0), $(".editPartyCard").attr("disabled", !0), $("#addparty").modal("show")
}), $(document.body).on("click", ".venuelist", function () {
    $("#venulist_model").modal("show")
}), $("#venue_table tbody").on("click", "tr", function () {
    $(this).hasClass("table-selected") ? $(this).removeClass("table-selected") : (venue_table_data.$("tr.table-selected").removeClass("table-selected"), $(this).addClass("table-selected"))
}), $(document.body).on("click", "#set_venue", function () {
    var a = [];
    $.map(venue_table_data.rows(".table-selected").data(), function (e) {
        return a = e
    }), a[1].lengh > 0 ? $("#editCaseVenue1").val(a[1]) : $("#editCaseVenue1").val(a[0]), $("#editCaseVenue").val(a.DT_RowId), $("#venulist_model").modal("hide")
}), $(document.body).on("click", ".edit-party", function () {
    $("#show_conform_model").modal("show")
}), $(document.body).on("click", "#relodex_yes", function () {
    var a = $("#edit-party").attr("data-casecard"),
            e = $("#edit-party").attr("data-case");
    $("#show_conform_model").modal("hide"), $.blockUI(), $.ajax({
        url: baseUrl + "/rolodex/getEditRolodex",
        data: {
            cardcode: a,
            caseNO: e,
            status: "Yes"
        },
        method: "POST",
        success: function (a) {
            var e = $.parseJSON(a);
            if (e.card_comments)
                var t = $.parseJSON(e.card_comments);
            else
                t = "";
            if (swal.close(), "0" == e.status) {
                var d = "";
                "APPLICANT" == e.data.type ? (d = e.data.last + ", " + e.data.first, $("input:radio[name=rolodex_search_parties][value=rolodex-first-last]").iCheck("check")) : "LIEN" == e.data.type || "BOARD" == e.data.type || "INSURANCE" == e.data.type || "ATTORNEY" == e.data.type || "EMPLOYER" == e.data.type ? (d = e.data.firm, $("input:radio[name=rolodex_search_parties][value=rolodex-company]").iCheck("check")) : "DR" == e.data.type ? 0 == e.data.firm.length ? (d = e.data.last + ", " + e.data.first, $("input:radio[name=rolodex_search_parties][value=rolodex-first-last]").iCheck("check")) : (d = e.data.firm, $("input:radio[name=rolodex_search_parties][value=rolodex-company]").iCheck("check")) : (d = e.data.last + ", " + e.data.first, $("input:radio[name=rolodex_search_parties][value=rolodex-first-last]").iCheck("check")), $("#rolodex_search_text").val(d), $("#rolodex_search_parties_form").submit(), $("#addparty").modal("show")
            } else
                $("#addcontact .modal-title").html("Edit Contact"), $("#card_salutation").val(e.salutation), $("#card_suffix").val(e.suffix), $("select[name=card_title]").val(e.title), $("#card_type").val(e.type), $("input[name=card_first]").val(e.first), $("input[name=card_middle]").val(e.middle), $("input[name=card_last]").val(e.last), $("input[name=card_popular]").val(e.popular), $("input[name=card_letsal]").val(e.letsal), $("input[name=card_occupation]").val(e.occupation), $("input[name=card_employer]").val(e.employer), $("input[name=hiddenimg]").val(e.picture), e.picture ? (document.getElementById("profilepicDisp").style.display = "block", document.getElementById("dispProfile").src = e.picturePath) : document.getElementById("dispProfile").src = "", $("input[name=card2_firm]").val(e.firm), $("input[name=card2_address1]").val(e.address1), $("input[name=card2_address2]").val(e.address2), $("input[name=card2_city]").val(e.city), $("input[name=card2_state]").val(e.state), $("input[name=card2_zip]").val(e.zip), $("input[name=card2_venue]").val(e.venue), $("input[name=card2_eamsref]").val(e.eamsref), $("input[name=card_business]").val(e.business), $("input[name=card_fax]").val(e.card_fax), $("input[name=card_home]").val(e.home), $("input[name=card_car]").val(e.car), $("input[name=card_email]").val(e.email), "" != t ? $("input[name=card_notes]").val(t[0].notes) : $("input[name=card_notes]").val(), $("input[name=card2_phone1]").val(e.phone1), $("input[name=card2_phone2]").val(e.phone2), $("input[name=card2_tax_id]").val(e.tax_id), $("input[name=card2_fax]").val(e.card_fax), $("input[name=card2_fax2]").val(e.card2_fax), $("input[name=card_social_sec]").val(e.social_sec), "NA" == e.birth_date ? $("input[name=card_birth_date]").val("") : $("input[name=card_birth_date]").val(e.birth_date), $("input[name=card_licenseno]").val(e.licenseno), $("select[name=card_specialty]").val(e.specialty), $("input[name=card_mothermaid]").val(e.mothermaid), $("input[name=card_cardcode]").val(e.cardcode), $("input[name=card_firmcode]").val(e.firmcode), $("select[name=card_interpret]").val(e.interpret), $("select[name=card_language]").val(e.language), $("#form_datetime1").val(e.card_origchg + " - " + e.card_origdt), $("#form_datetime2").val(e.card_lastchg + " - " + e.card_lastdt), $("#form_datetime3").val(e.card2_origchg + " - " + e.card2_origdt), $("#form_datetime4").val(e.card2_lastchg + " - " + e.card2_lastdt), "" != t ? $("textarea[name=card_comments]").val(t[1].comments) : $("textarea[name=card_comments]").val(), $("textarea[name=card2_comments]").val(e.card2_comments), $("textarea[name=card2_mailing1]").val(e.mailing1), $("textarea[name=card2_mailing2]").val(e.mailing2), $("textarea[name=card2_mailing3]").val(e.mailing3), $("textarea[name=card2_mailing4]").val(e.mailing4), $("#new_case").val("parties"), $('input[name="card_cardcode"]').val(e.cardcode), $("#addcontact").modal({
                    backdrop: "static",
                    keyboard: !1
                }), $("#addcontact").modal("show"), $("#addRolodexForm select").trigger("change"), $.unblockUI()
        }
    })
}), $(document.body).on("click", "#relodex_no", function () {
    $("#show_conform_model").modal("hide");
    var a = $("#edit-party").attr("data-casecard"),
            e = $("#edit-party").attr("data-case");
    $.blockUI(), $.ajax({
        url: baseUrl + "/rolodex/editRolodexdetail",
        data: {
            cardcode: a,
            caseNO: e,
            status: "NO"
        },
        method: "POST",
        success: function (a) {
            var e = $.parseJSON(a),
                    t = $.parseJSON(e.data.card_comments);
            "0" == e.status ? ($.unblockUI(), swal({
                title: "Are you sure you want to make changes to this rolodex card?",
                text: " " + e.message,
                showCancelButton: !0,
                confirmButtonColor: "#1ab394",
                confirmButtonText: "Yes",
                closeOnConfirm: !1,
                showLoaderOnConfirm: !0
            }, function (a) {
                a && (swal.close(), $("#addcontact .modal-title").html("Edit Contact"), $("#card_salutation").val(e.data.salutation), $("#card_suffix").val(e.data.suffix), $("select[name=card_title]").val(e.data.title), $("#card_type").val(e.data.type), $("input[name=card_first]").val(e.data.first), $("input[name=card_middle]").val(e.data.middle), $("input[name=card_last]").val(e.data.last), $("input[name=card_letsal]").val(e.data.letsal), $("input[name=card_occupation]").val(e.data.occupation), $("input[name=card_employer]").val(e.data.employer), $("input[name=hiddenimg]").val(e.data.picture), e.data.picture ? (document.getElementById("profilepicDisp").style.display = "block", document.getElementById("dispProfile").src = e.data.picturePath) : document.getElementById("dispProfile").src = "", $("input[name=card2_firm]").val(e.data.firm), $("input[name=card2_address1]").val(e.data.address1), $("input[name=card2_address2]").val(e.data.address2), $("input[name=card2_city]").val(e.data.city), $("input[name=card2_state]").val(e.data.state), $("input[name=card2_zip]").val(e.data.zip), $("input[name=card2_venue]").val(e.data.venue), $("input[name=card2_eamsref]").val(e.data.eamsref), $("input[name=card_business]").val(e.data.business), $("input[name=card_fax]").val(e.data.card_fax), $("input[name=card_home]").val(e.data.home), $("input[name=card_car]").val(e.data.car), $("input[name=card_email]").val(e.data.email), "" != t ? $("input[name=card_notes]").val(t[0].notes) : $("input[name=card_notes]").val(), $("input[name=card2_phone1]").val(e.data.phone1), $("input[name=card2_phone2]").val(e.data.phone2), $("input[name=card2_tax_id]").val(e.data.tax_id), $("input[name=card2_fax]").val(e.data.card_fax), $("input[name=card2_fax2]").val(e.data.card2_fax), $("input[name=card_social_sec]").val(e.data.social_sec), "NA" == e.data.birth_date ? $("input[name=card_birth_date]").val("") : $("input[name=card_birth_date]").val(e.data.birth_date), $("input[name=card_licenseno]").val(e.data.licenseno), $("select[name=card_specialty]").val(e.data.specialty), $("input[name=card_mothermaid]").val(e.data.mothermaid), $("input[name=card_cardcode]").val(e.data.cardcode), $("input[name=card_firmcode]").val(e.data.firmcode), $("select[name=card_interpret]").val(e.data.interpret), $("select[name=card_language]").val(e.data.language), $("#form_datetime1").val(e.data.card_origchg + " - " + e.data.card_origdt), $("#form_datetime2").val(e.data.card_lastchg + " - " + e.data.card_lastdt), $("#form_datetime3").val(e.data.card2_origchg + " - " + e.data.card2_origdt), $("#form_datetime4").val(e.data.card2_lastchg + " - " + e.data.card2_lastdt), "" != t ? $("textarea[name=card_comments]").val(t[1].comments) : $("textarea[name=card_comments]").val(), $("textarea[name=card2_comments]").val(e.data.card2_comments), $("textarea[name=card2_mailing1]").val(e.data.mailing1), $("textarea[name=card2_mailing2]").val(e.data.mailing2), $("textarea[name=card2_mailing3]").val(e.data.mailing3), $("textarea[name=card2_mailing4]").val(e.data.mailing4), $("#new_case").val("parties"), $('input[name="card_cardcode"]').val(e.data.cardcode), $("#addcontact").modal({
                    backdrop: "static",
                    keyboard: !1
                }), $("#addcontact").modal("show"), $("#addRolodexForm select").trigger("change"), $.unblockUI())
            })) : ($("#addcontact .modal-title").html("Edit Contact"), $("#card_salutation").val(e.data.salutation), $("#card_suffix").val(e.data.suffix), $("select[name=card_title]").val(e.data.title), $("#card_type").val(e.data.type), $("input[name=card_first]").val(e.data.first), $("input[name=card_middle]").val(e.data.middle), $("input[name=card_last]").val(e.data.last), $("input[name=card_letsal]").val(e.data.letsal), $("input[name=card_occupation]").val(e.data.occupation), $("input[name=card_employer]").val(e.data.employer), $("input[name=hiddenimg]").val(e.data.picture), e.data.picture ? (document.getElementById("profilepicDisp").style.display = "block", document.getElementById("dispProfile").src = e.data.picturePath) : document.getElementById("dispProfile").src = "", $("input[name=card2_firm]").val(e.data.firm), $("input[name=card2_address1]").val(e.data.address1), $("input[name=card2_address2]").val(e.data.address2), $("input[name=card2_city]").val(e.data.city), $("input[name=card2_state]").val(e.data.state), $("input[name=card2_zip]").val(e.data.zip), $("input[name=card2_venue]").val(e.data.venue), $("input[name=card2_eamsref]").val(e.data.eamsref), $("input[name=card_business]").val(e.data.business), $("input[name=card_fax]").val(e.data.card_fax), $("input[name=card_home]").val(e.data.home), $("input[name=card_car]").val(e.data.car), $("input[name=card_email]").val(e.data.email), $("input[name=card2_phone1]").val(e.data.phone1), $("input[name=card2_phone2]").val(e.data.phone2), $("input[name=card2_tax_id]").val(e.data.tax_id), $("input[name=card2_fax]").val(e.data.card_fax), $("input[name=card2_fax2]").val(e.data.card2_fax), $("input[name=card_social_sec]").val(e.data.social_sec), "NA" == e.data.birth_date ? $("input[name=card_birth_date]").val("") : $("input[name=card_birth_date]").val(e.data.birth_date), $("input[name=card_licenseno]").val(e.data.licenseno), $("select[name=card_specialty]").val(e.data.specialty), $("input[name=card_mothermaid]").val(e.data.mothermaid), $("input[name=card_cardcode]").val(e.data.cardcode), $("input[name=card_firmcode]").val(e.data.firmcode), $("select[name=card_interpret]").val(e.data.interpret), $("select[name=card_language]").val(e.data.language), $("#form_datetime1").val(e.data.card_origchg + " - " + e.data.card_origdt), $("#form_datetime2").val(e.data.card_lastchg + " - " + e.data.card_lastdt), $("#form_datetime3").val(e.data.card2_origchg + " - " + e.data.card2_origdt), $("#form_datetime4").val(e.data.card2_lastchg + " - " + e.data.card2_lastdt), $("textarea[name=card_comments]").val(e.data.card_comments), $("textarea[name=card2_comments]").val(e.data.card2_comments), $("textarea[name=card2_mailing1]").val(e.data.mailing1), $("textarea[name=card2_mailing2]").val(e.data.mailing2), $("textarea[name=card2_mailing3]").val(e.data.mailing3), $("textarea[name=card2_mailing4]").val(e.data.mailing4), $("#new_case").val("parties"), $('input[name="card_cardcode"]').val(e.data.cardcode), $("#addcontact").modal({
                backdrop: "static",
                keyboard: !1
            }), $("#addcontact").modal("show"), $("#addRolodexForm select").trigger("change"), $.unblockUI())
        }
    })
}), $(document.body).on("click", ".rolodex-company", function () {
    $("#firmcompanylist").attr("style", "z-index:9999 !important"), $("#firmcompanylist").modal("show")
}), $(document.body).on("blur keyup", "#rolodex_search_text_name", function (a) {
    "focusout" != a.type && "13" != a.keyCode || $("#companylistForm").valid() && ($("#rolodex_search_text_name").val().trim().length > 0 ? $.ajax({
        url: baseUrl + "/cases/searchForRolodex",
        method: "POST",
        data: {
            rolodex_search_parties: $("#rolodex_search_parties_name").val(),
            rolodex_search_text: $(this).val()
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            rolodexSearch.clear();
            var e = $.parseJSON(a);
            $.each(e, function (a, e) {
                var t = "";
                t += "<tr class='viewsearchPartiesDetail1' data-cardcode=" + e.cardcode + "><td>" + e.firm + "</td>", t += "<td>" + e.city + "</td>", t += "<td>" + e.address + "</td>", t += "<td>" + e.phone + "</td>", t += "</tr>", rolodexSearch.row.add($(t))
            }), rolodexSearch.draw()
        }
    }) : swal({
        title: "Alert",
        text: "Please enter Firm name.",
        type: "warning"
    }))
}), $("#rolodexsearchResult_data tbody").on("click", "tr", function () {
    $(this).hasClass("table-selected") ? $(this).removeClass("table-selected") : ($("#rolodexsearchResult_data tr").removeClass("table-selected"), $(this).addClass("table-selected"))
}), $(document.body).on("click", "#viewSearchedPartieDetails", function () {
    var a = rolodexSearch.row(".table-selected").node(),
            e = $("#caseNo").val();
    $("#firmcompanylist").modal("hide"), $.ajax({
        url: baseUrl + "/cases/viewSearchedPartieDetails",
        method: "POST",
        dataType: "json",
        data: {
            cardcode: $(a).attr("data-cardcode"),
            caseno: e
        },
        beforeSend: function (a) {
            $.blockUI()
        },
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            if (0 == a)
                swal({
                    title: "Alert",
                    text: "Oops! Some error has occured while fetching details. Please try again later.",
                    type: "warning"
                });
            else {
                var e = a.city;
                if (null != e) {
                    var t = e.split(",");
                    $('input[name="card2_city"]').val($.trim(t[0])), $('input[name="card2_state"]').val($.trim(t[1])), $('input[name="card2_zip"]').val($.trim(t[2]))
                }
                $('input[name="card2_firm"]').val(a.firm), $('input[name="card2_address1"]').val(a.address1), $('input[name="card2_address2"]').val(a.address2), $('input[name="card2_venue"]').val(a.venue), $(".addSearchedParties").attr("disabled", !1), $(".editPartyCard").attr("disabled", !1)
            }
        }
    })
}), $(document.body).on("click", ".event_note_list", function () {
    $("#eventnotelist").modal("show")
}), $("#eventnotelist").on("shown.bs.modal", function (a) {
    category_list($("#case_category_event").val(), $("#caseNo").val())
}), $(document.body).on("change", ".category_event_list", function () {
    category_list($("#case_category_event").val(), $("#caseNo").val())
}), $("#activity_list").on("ifClicked", function (a) {
    category_list($("#case_category_event").val(), $("#caseNo").val())
}), $("#caseEventlist tbody").on("click", "tr", function () {
    $(this).hasClass("table-selected") ? $(this).removeClass("table-selected") : ($("#caseEventlist tr").removeClass("table-selected"), $(this).addClass("table-selected"))
}), $(document.body).on("click", "#caseactivityevent", function () {
    var a = categoryeventlist.row(".table-selected").node();
    $("#eventnotelist").modal("hide"), $("#activity_event").val($(a).attr("data-event"))
}), $("#addcase.modal").on("hidden.bs.modal", function () {
    $(this).find('input[type="text"]').each(function () {
        this.value = ""
    })
}), $("#firmcompanylist").on("hidden.bs.modal", function () {

    $("#rolodex_search_text_name").val("");
    $('tr.table-selected').removeClass('table-selected');
    $(this).find('input[type="text"]').each(function () {
        this.value = ""
    }), $("#rolodexsearchResult_data").dataTable().fnClearTable()
}), $("#addparty").on("hidden.bs.modal", function () {
    $(this).find('input[type="text"]').each(function () {
        this.value = ""
    })
}), $("#addtask").on("hidden.bs.modal", function () {
    $(this).find("input,textarea").each(function () {
        this.value = "", $(this).iCheck("uncheck"), $(this).trigger("change")
    })
}), $(document.body).on("click", "#addtaskModal", function () {
    $(".select2Class#whofrom").val($.cookie("username")).trigger("change"), $(".select2Class#whoto").val("AEB").trigger("change"), $(".select2Class#priority").val("2").trigger("change"), $(".select2Class#color").val("1").trigger("change"), $("#addtask").modal({
        backdrop: "static",
        keyboard: !1
    }), $("#addtask").modal("show")
}), $(document.body).on("click", ".casepartydetails", function () {}), $("#casePartylist tbody").on("click", "tr", function () {
    $(this).hasClass("table-selected") ? $(this).removeClass("table-selected") : ($("#casePartylist tr").removeClass("table-selected"), $(this).addClass("table-selected"))
}), $(document.body).on("click", "#casepartysave", function () {
    var a = casepartylist_data.row(".table-selected").node();
    $("#casepartyDetaillist").modal("hide"), "general" == $(a).attr("data-active_data") ? ($("input[name=field_first]").val($(a).attr("data-first")), $("input[name=field_last]").val($(a).attr("data-last")), data - state ? $("input[name=field_state]").val($(a).attr("data-state")) : $("input[name=field_state]").val(""), $("input[name=field_zip_code]").val($(a).attr("data-zip")), $("input[name=field_social_sec]").val($(a).attr("data-social_sec")), $("input[name=field_aoe_coe_status]").val($(a).attr("data-aoe_coe_status")), $("input[name=field_address]").val($(a).find("td:eq(3)").text()), $("input[name=field_city]").val($(a).find("td:eq(2)").text())) : "employers1" == $(a).attr("data-active_data") ? ($("input[name=field_e_name]").val($(a).attr("data-first")), $("input[name=field_e_address]").val($(a).find("td:eq(3)").text()), $("input[name=field_e_city]").val($(a).find("td:eq(2)").text()), $("input[name=field_e_state]").val($(a).attr("data-state")), $("input[name=field_e_zip]").val($(a).attr("data-zip")), $("input[name=field_e_phone]").val($(a).attr("data-phone")), $("input[name=field_e_fax]").val($(a).attr("data-fax"))) : "employers2" == $(a).attr("data-active_data") ? ($("input[name=field_e2_name]").val($(a).attr("data-first")), $("input[name=field_e2_address]").val($(a).find("td:eq(3)").text()), $("input[name=field_e2_city]").val($(a).find("td:eq(2)").text()), $("input[name=field_e2_state]").val($(a).attr("data-state")), $("input[name=field_e2_zip]").val($(a).attr("data-zip")), $("input[name=field_e2_phone]").val($(a).attr("data-phone")), $("input[name=field_e2_fax]").val($(a).attr("data-fax"))) : "carriers1" == $(a).attr("data-active_data") ? ($("select[name=field_i_adjsal]").val($(a).attr("data-salutation")), $("input[name=field_i_adjfst]").val($(a).attr("data-first")), $("input[name=field_i_adjuster]").val($(a).attr("data-last")), $("input[name=field_i_name]").val($(a).find("td:eq(1)").text()), $("input[name=field_i_address]").val($(a).find("td:eq(3)").text()), $("input[name=field_i_city]").val($(a).find("td:eq(2)").text()), $("input[name=field_i_state]").val($(a).attr("data-state")), $("input[name=field_i_zip]").val($(a).attr("data-zip")), $("input[name=field_i_phone]").val($(a).attr("data-phone")), $("input[name=field_i_fax]").val($(a).attr("data-fax"))) : "carriers2" == $(a).attr("data-active_data") ? ($("select[name=field_i2_adjsal]").val($(a).attr("data-salutation")), $("input[name=field_i2_adjfst]").val($(a).attr("data-first")), $("input[name=field_i2_adjuste]").val($(a).attr("data-last")), $("input[name=field_i2_name]").val($(a).find("td:eq(1)").text()), $("input[name=field_i2_address]").val($(a).find("td:eq(3)").text()), $("input[name=field_i2_city]").val($(a).find("td:eq(2)").text()), $("input[name=field_i2_state]").val($(a).attr("data-state")), $("input[name=field_i2_zip]").val($(a).attr("data-zip")), $("input[name=field_i2_phone]").val($(a).attr("data-phone")), $("input[name=field_i2_fax]").val($(a).attr("data-fax"))) : "attorneys1" == $(a).attr("data-active_data") ? ($("input[name=field_d1_first]").val($(a).attr("data-first")), $("input[name=field_d1_last]").val($(a).attr("data-last")), $("input[name=field_d1_firm]").val($(a).find("td:eq(1)").text()), $("input[name=field_d1_address]").val($(a).find("td:eq(3)").text()), $("input[name=field_d1_city]").val($(a).find("td:eq(2)").text()), $("input[name=field_d1_state]").val($(a).attr("data-state")), $("input[name=field_d1_zip]").val($(a).attr("data-zip")), $("input[name=field_d1_phone]").val($(a).attr("data-phone")), $("input[name=field_d1_fax]").val($(a).attr("data-fax"))) : ($("input[name=field_d2_first]").val($(a).attr("data-first")), $("input[name=field_d2_last]").val($(a).attr("data-last")), $("input[name=field_d2_firm]").val($(a).find("td:eq(1)").text()), $("input[name=field_d2_address]").val($(a).find("td:eq(3)").text()), $("input[name=field_d2_city]").val($(a).find("td:eq(2)").text()), $("input[name=field_d2_state]").val($(a).attr("data-state")), $("input[name=field_d2_zip]").val($(a).attr("data-zip")), $("input[name=field_d2_phone]").val($(a).attr("data-phone")), $("input[name=field_d2_fax]").val($(a).attr("data-fax"))), $("#addInjuryForm select").trigger("change")
}), $(document.body).on("click", ".injury_print", function () {
    var a = $(this).attr("id");
    $("#injury_Print").modal("show"), $("#injuryId").val(a)
}), $(document.body).on("click", ".print_injury_data", function () {
    var a = $("input[name=form_data_type]:checked").val(),
            e = $("#injuryId").val(),
            t = $("#hdnCaseId").val();
    $.ajax({
        url: baseUrl + "/cases/printInjuryDetails",
        method: "POST",
        data: {
            form_data_type: a,
            injuryId: e,
            caseno: t
        },
        success: function (a) {
            $("#print_injures_form").modal("hide"), $(a).printThis({
                debug: !1
            })
        }
    })
}), $(document.body).on("click", ".calendar_clone", function () {
    var a = $("select[name=atty_hand]").val(),
            e = $("select[name=attyass]").val(),
            t = $("#first").val() + " " + $("#last").val(),
            d = $("#event").val();
    $("#atty_hand_clone").val(a), $("#attyass_clone").val(e), $("#event_clone").val(d), $("#firstname_clone").val(t), $("#first_clone").val($("#first").val()), $("#last_clone").val($("#last").val()), $("#calendarForm").valid() && $("#clonecalendar").modal("show")
}), $(document.body).on("change", "#entry_date", function () {
    var a = [],
            e = [],
            t = $("#entry_date").val();
    if (2 == t)
        for (var d = 0; d < 14; d++) {
            var i = moment().add(d, "days")._d;
            a.push(moment(i).format("M/D/YYYY ,dddd")), e.push(moment(i).format("YYYY-M-D"))
        }
    else if (3 == t)
        for (d = 0; d < 30; d++) {
            i = moment().add(d, "days")._d;
            a.push(moment(i).format("M/D/YYYY ,dddd")), e.push(moment(i).format("YYYY-M-D"))
        }
    else if (4 == t)
        for (d = 0; d < 4; d++) {
            i = moment().add(d, "weeks")._d;
            a.push(moment(i).format("M/D/YYYY ,dddd")), e.push(moment(i).format("YYYY-M-D"))
        }
    else if (5 == t)
        for (d = 0; d < 24; d++) {
            i = moment().add(d, "weeks")._d;
            a.push(moment(i).format("M/D/YYYY ,dddd")), e.push(moment(i).format("YYYY-M-D"))
        }
    else if (6 == t)
        for (d = 0; d < 52; d++) {
            i = moment().add(d, "weeks")._d;
            a.push(moment(i).format("M/D/YYYY ,dddd")), e.push(moment(i).format("YYYY-M-D"))
        }
    else if (7 == t)
        for (d = 0; d < 6; d++) {
            i = moment().add(d, "month")._d;
            a.push(moment(i).format("M/D/YYYY ,dddd")), e.push(moment(i).format("YYYY-M-D"))
        }
    else if (8 == t)
        for (d = 0; d < 12; d++) {
            i = moment().add(d, "month")._d;
            a.push(moment(i).format("M/D/YYYY ,dddd")), e.push(moment(i).format("YYYY-M-D"))
        }
    else
        a.push(moment().format("M/D/YYYY , dddd")), e.push(moment(i).format("YYYY-M-D"));
    $("#Date_copy").text(""), $("#startend1").val(e);
    for (d = 0; d < a.length; d++)
        $("#Date_copy").append(a[d] + "\n")
}), $("#clonecalender").on("click", function () {
    var a = baseUrl + "/calendar/addCalendarCloneData";
    $.blockUI(), $.ajax({
        url: a,
        dataType: "json",
        method: "post",
        data: $("#calendarForm").serialize() + "&startend1=" + $("#startend1").val() + "&attyass=" + $("#attyass_clone").val() + "&first=" + $("#firstname_clone").val(),
        complete: function (a, e) {
            $.unblockUI()
        },
        success: function (a) {
            $.unblockUI(), "1" == a[0].status ? (swal("Success !", a.message, "success"), $("#Addclonecase")[0].reset(), $("#calendarForm")[0].reset(), $("#addcalendar").modal("hide"), $("#clonecalendar").modal("hide"), $("#addcalendar .modal-title").html("Add Calendar"), createCalendarRow1(a), $("#calendar").fullCalendar("refetchEvents")) : swal("Oops...", a.message, "error")
        }
    })
}), $("#addparty.modal").on("show.bs.modal", function () {
    0 == $("table#rolodexPartiesResult tr").hasClass("highlight") && ($(".addSearchedParties").prop("disabled", !0), $(".editPartyCard").prop("disabled", !0))
}), $(document.body).on("click", ".clear_form", function () {
    $("#calendarForm").find("input,textarea,select").each(function () {
        this.value = "", $(this).iCheck("uncheck"), $(this).trigger("change")
    })
}), $("#firmcompanylist").on("show.bs.modal", function () {
    $(".dataTables-example").removeAttr("style"), $(".dataTables_scrollHeadInner").removeAttr("style"), $(".dataTables-example").css("width", "100% !important;"), $(".dataTables_scrollHeadInner").css("width", "100% !important;")
}), $("#addcaseact.modal").on("hidden.bs.modal", function () {
    $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
        this.value = ""
    }), $('#addcaseact a[href="#tab-26"]').tab("show")
}), $(document.body).on("click", ".CaseActEmail", function () {
    var a = $("#activity_event").val();
    CKEDITOR.instances.mailbody1 && CKEDITOR.instances.mailbody1.destroy(), CKEDITOR.replace("mailbody1"), $("#addcaseact").modal("hide"), $("#CaseEmailModal").modal("show"), $("#mailbody1").val(a)
}), $(document.body).on("click", ".CaseActPrint", function () {
    $.ajax({
        url: baseUrl + "/cases/printpde",
        method: "POST",
        data: {
            caseno: $("#hdnCaseId").val()
        },
        success: function (a) {
            var e = baseUrl + "/assets/printCaseAct/" + a;
            window.open(e, "_blank")
        }
    })
}), $(document).on("click", ".task_list", function () {
    var a = $(this).attr("data-proskey_id");
    searchBy1.prosno = a, taskTable1.ajax.reload(), $("#Task-list").modal("show")
}), $("#ProspectListDatatable tbody").on("click", "tr", function () {
    if ($(this).hasClass("table-selected"))
        $(this).removeClass("table-selected");
    else {
        ProspectListDatatable.$("tr.table-selected").removeClass("table-selected"), $(this).addClass("table-selected");
        var a = ProspectListDatatable.row(".table-selected").node();
        prospect_detail(a)
    }
}), $(document.body).on("click", "#addTask1", function () {
    if ($("#addTaskForm1").valid()) {
        $.blockUI();
        var a = "";
        a = "" != $("input[name=taskid]").val() && 0 != $("input[name=taskid]").val() ? baseUrl + "/tasks/editTask" : baseUrl + "/tasks/addTask", $.ajax({
            url: a,
            method: "POST",
            data: $("#addTaskForm1").serialize() + "&prosno=" + $(".task_list").attr("data-proskey_id"),
            success: function (a) {
                var e = $.parseJSON(a);
                $.unblockUI(), "1" == e.status ? swal({
                    title: "Success!",
                    text: e.message,
                    type: "success"
                }, function () {
                    $("#addTaskForm1")[0].reset(), $("#addtask1").modal("hide"), $("#addtask1 .modal-title").html("Add a Task"), taskTable.draw(!1), taskTable1.draw(!1)
                }) : swal("Oops...", e.message, "error")
            }
        })
    }
}), $("#addtask1.modal").on("hidden.bs.modal", function () {
    $("#addtask1 h4.modal-title").text("Add a Task"), $("label.error").remove(), $(this).find('input[type="text"],input[type="email"],textarea').each(function () {
        this.value = "", $(this).removeClass("error")
    }), $("input[name=taskid]").val(0), $('#addtask1 a[href="#name"]').tab("show")
}), $("#proceedprospects.modal").on("hidden.bs.modal", function () {
    $(this).find('input[type="text"],input[type="email"],textarea,select').each(function () {
        this.value = ""
    }), $("#prospectForm").validate().resetForm(), $("#prospectForm").find(".error").removeClass("error"), $("#proceedprospects .tab-content div#tab-6").addClass("active")
}), $("#CaseEmailModal.modal").on("hidden.bs.modal", function () {
    $(this).find('input[type="email"],textarea,select,file').each(function () {
        this.value = ""
    }), $("#case_category1").val("1"), $("#mail_subject1").val(""), $("#emails1").val(""), $("#send_mail_form1 select").trigger("change");
    var a = $("#afiles1");
    a.wrap("<form>").closest("form").get(0).reset(), a.unwrap()
}), $(document.body).on("change", "#activity_fee", function () {
    "OTHER" == $(this).val() ? $("#activity_fee1").show() : ($("#activity_fee1").hide(), $("#activity_fee1").val(""))
}), $(document.body).on("blur", "#case_act_time", function () {
    var a = eval($(this).val());
    $("#case_act_time").val(a);
    var hour = $("#case_act_hour_rate").val();
    if (hour > 0) {
        var cost = parseInt(a * hour) / parseInt(60);
        $("#case_act_cost_amt").val(cost)
    }
}), $(document.body).on("blur", "#case_act_hour_rate", function () {
    var a = eval($(this).val()),
            hour = $("#case_act_time").val();
    if (hour > 0) {
        var cost = parseInt(a * hour) / parseInt(60);
        $("#case_act_cost_amt").val(cost)
    }
});
var regExp = /[a-z]/i;

function validate(a) {
    var e = String.fromCharCode(a.which) || a.key;
    if (regExp.test(e))
        return a.preventDefault(), !1
}

function remove_error(a) {
    null === document.getElementById(a + "-error") || (document.getElementById(a + "-error").innerHTML = "")
}

function SubmitForm() {
    return !!$("#add_new_case_activity").valid()
}

function viewActDoc(a) {
    var e = $("#caseNo").val();
    $.ajax({
        url: baseUrl + "/cases/viewcaseactdoc",
        method: "POST",
        dataType: "json",
        data: {
            filename: a,
            caseno: e
        },
        success: function (a) {}
    })
}

function showhidedt1() {
    1 == $('input[type="checkbox"][name="reminder1"]').is(":checked") ? ($('input[type="checkbox"][name="reminder1"]').val("on"), $(".rmdt1").show()) : ($(".rmdt1").hide(), $(".rdate").val(""), $(".rtime").val(""))
}

function showhidedt() {
    1 == $('input[type="checkbox"][name="reminder"]').is(":checked") ? ($('input[type="checkbox"][name="reminder"]').val("on"), $(".rmdt").show()) : ($(".rmdt").hide(), $(".rdate").val(""), $(".rtime").val(""))
}
$("#addcaseact").on("hidden.bs.modal", function () {
    var a = $("#add_new_case_activity");
    a.validate().resetForm(), a.find(".error").removeClass("error")
}), $(document.body).on("click", "#printProspectdetails", function () {
    $("#printProspectmodal").modal("show")
}), $(document.body).on("click", "#save_prospect", function () {
    if ($("#prospectForm").valid()) {
        $.blockUI();
        "" != $("#prospectId").val() && 0 != $("#prospectId").val() ? baseUrl + "/Prospects/editProspects" : baseUrl + "/Prospects/addProspects"
    }
}), $(document.body).on("click", ".activity_view", function () {
    viewActDoc($(this).data("actid"))
}), $(".discard_message").click(function (a) {
    swal({
        title: "Are you sure?",
        text: "The changes you made will be lost",
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, discard it!",
        closeOnConfirm: !0
    }, function () {
        $("#CaseEmailModal").find(".att-add-file").not(":first").remove(), $("#CaseEmailModal").modal("hide")
    })
}), $(document).on("click", ".ca-addfile", function () {
    $(".att-add-file:last").after('<label class="col-sm-2 control-label"></label><div class="col-sm-9 att-add-file marg-top5 clearfix"> <input type="file" name="afiles1[]" id="afiles1" multiple ><i class="fa fa-plus-circle marg-top5 ca-addfile" aria-hidden="true"></i> <i class="fa fa-times marg-top5 removefile" aria-hidden="true"></i></div>')
}), $(document).on("click", ".removefile", function () {
    $(this).closest(".att-add-file").prev("label").remove(), $(this).closest(".att-add-file").remove()
});