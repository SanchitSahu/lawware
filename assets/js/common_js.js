$(document).ready(function () {
    /*$(document).on('focus', '.select2.select2-container', function (e) {
        if (e.originalEvent && $(this).find(".select2-selection--single").length > 0) {
            $(this).siblings('select').select2('open');
        } 
    });*/

    $('.select2Class').on('select2:select', function (e) {
        $(this).next().children().children().focus();
    });

    $('select').on('select2:close', function (e) {
        var selectedOption = $(e.target).data('select2').$dropdown.find('.select2-results__option--highlighted').text();
        var fieldName = $(this).attr('name');
        var fieldId = $(this).attr('id');
        if(selectedOption.includes('Select ') == false) {
            if(fieldName.includes('searchBy') == true || (fieldName.includes('casetype') == true) || fieldName.includes('atty_resp') == true || fieldName == 'color' || fieldName == 'typetask' || fieldName == 'priority' || fieldName == 'card_interpret' || fieldName == 'cal_typ' || fieldName == 'atty_hand' || fieldName == 'para_hand' || fieldName == 'sec_hand' || fieldName == 'categories' || fieldName == 'body_parts' || fieldName == 'case_reffered_by' || fieldName == 'case_ps' || fieldName.includes('case_category') == true || fieldName == 'color_codes' || fieldName == 'security' || fieldName == 'activity_typeact' || fieldName == 'activity_doc_type' || fieldName == 'activity_style' || fieldName == 'field_amended' || fieldName.includes('field_pob') == true || fieldName == 'Category') {
                if($(e.target).data('select2').$dropdown.find('.select2-results__option--highlighted').attr('id')) {
                    var tmpSelection = $(e.target).data('select2').$dropdown.find('.select2-results__option--highlighted').attr('id').split('-');   
                    if(fieldId != 'Ccasetype' && fieldId != 'Catty_resp' && fieldId != 'Catty_hand' && fieldId != 'Cpara_hand' && fieldId != 'Csec_hand') {
                        $(this).val(tmpSelection[tmpSelection.length - 1]);
                    } else {
                        $(this).val(tmpSelection[tmpSelection.length - 2] + '-' + tmpSelection[tmpSelection.length - 1]);
                    }
                    $(this).select2().trigger('change');
                }
            } else if(fieldName != 'whoto' && fieldName != 'whoto1') {
                $(this).val(selectedOption);
                $(this).select2().trigger('change');
            }
        } else {
            $(this).val("");
            $(this).select2().trigger('change');
        }
        
        $(this).next().children().children().focus();
    });

    $(document).on('keydown', '.cstmdrp', function(e) { 
        var keyCode = e.keyCode || e.which; 
        if (keyCode == 9) {
            if($(this).attr('id') == 'event' && $('#event-error').length != 0) {
                $(this).val($(this).next().next().find('li.es-visible.selected').text());
            } else {
                $(this).val($(this).next().find('li.es-visible.selected').text());
            }
        } 
    });

    "" != menu && $('#side-menu li[data-active="' + menu + '"]').addClass("active"), $(".cstmdrp ").editableSelect({
        filter: !1
    }), updateEmailCount(), toastr.options = {
        closeButton: !0,
        debug: !1,
        progressBar: !0,
        preventDuplicates: !1,
        positionClass: "toast-top-right",
        onclick: null,
        showDuration: "400",
        hideDuration: "1000",
        timeOut: "7000",
        extendedTimeOut: "1000",
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    }, /*$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green"}),$(".summernote").summernote(),*/ $(".select2Class").select2(), "" != message && toastr.success(message), $(".onlyNumbers").keyup(function (e) {
        if (e.keyCode > 47 && e.keyCode < 58 || e.keyCode < 106 && e.keyCode > 95) return !0;
        (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && (this.value = this.value.replace(/[^0-9\.]/g, ""))
    }), $(".datepicker").datetimepicker({
        autoclose: !0
    })
});
var setCookie = function (e, t) {
    var s = new Date;
    s.setTime(s.getTime() + 3e5), document.cookie = e + "=" + t + ";expires=" + s.toUTCString()
},
    getCookie = function (e) {
        var t = document.cookie.match("(^|;) ?" + e + "=([^;]*)(;|$)");
        return t ? t[2] : null
    },
    guid = function () {
        function e() {
            return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
        }
        return e() + e() + "-" + e() + "-" + e() + "-" + e() + "-" + e() + e() + e()
    },
    parseTimestamp = function (e) {
        var t = new Date(1e3 * e),
            s = t.getFullYear(),
            o = ("0" + (t.getMonth() + 1)).slice(-2),
            a = ("0" + t.getDate()).slice(-2),
            n = t.getHours(),
            i = n,
            r = ("0" + t.getMinutes()).slice(-2),
            l = "AM";
        return n > 12 ? (i = n - 12, l = "PM") : 12 === n ? (i = 12, l = "PM") : 0 == n && (i = 12), s + "-" + o + "-" + a + ", " + i + ":" + r + " " + l
    };

function setSpinner() {
    return '<div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>'
}

function updateEmailCount() {
    $.ajax({
        url: HTTP_PATH + "email/get_unread_count",
        dataType: "json",
        type: "POST",
        success: function (e) {
            e.count > 0 ? $(".email_count").html(e.count).removeClass("hidden") : $(".email_count").html(0).addClass("hidden");
            var t = "",
                s = "";
            $.each(e.emails, function (e, o) {
                s = null != o.whofrom_name && "" != o.whofrom_name ? o.whofrom_name : o.whofrom, t += "<li>", t += '<a href="' + HTTP_PATH + 'email" class="forMessageDrpdown">', t += '<div class="dropdown-messages-box">', t += '<div class="media-body">', t += '<small class="pull-right">' + /*moment(o.datesent).fromNow()+*/ "</small>", t += "New email from <strong>" + s + "</strong>. <br>", t += '<small class="text-muted">' + (o.caseno > 0 ? "<strong>Case No - " + o.caseno + "</strong>&nbsp;" : "") + /*moment(o.datesent).fromNow()+*/ " at " + moment(o.datesent).format("HH:mm a") + " - " + moment(o.datesent).format("DD.MM.YYYY") + "</small>", t += "</div>", t += "</div>", t += "</a>", t += "</li>", t += '<li class="divider"></li>'
            }), t += "<li>", t += '<div class="text-center link-block">', t += '<a href="' + HTTP_PATH + 'email">', t += '<i class="fa fa-envelope"></i> <strong>Read All Messages</strong>', t += "</a>", t += "</div>", t += "</li>", $("ul.dropdown-messages").html(t)
        }
    })
}