function grab_emails() {
    var e = $("#task-navigation li.active").attr("id");
    var current_list = $("#task-navigation li.active").attr('id');
    if(current_list == 'Self')
    {
        $(".mark_as_old").hide();
    }

    $("#searchFiltr").css("display", "block");
    $(".category-list a").hasClass("activeli") ? list_type = $(".activeli").data("listing") : list_type = "", current_list = e;
    var t = "",
            a = $("input[name=emailtypes]:checked");
    a.length > 0 && (t = a.val());
    var i = "";
    var l = $("input[name=casetypes]:checked");
    l.length > 0 && (i = l.val()),
            "delete" == e ? ($(".mark_button button").hide(), $("#selectall_div").hide()) : ($(".mark_button button").show(), $("#selectall_div").show()),
            "old" == e || "delete" == e ? $(".mark_as_old").hide() : $(".mark_as_old").show(),
            "sent" == e ? $(".category-list").hide() : $(".category-list").show(),
            "new" != e ? $(".mark_as_old,.move_to_old_one").hide():'';
    var n = $(".email-new-dataTable").DataTable({
        processing: !0,
        serverSide: !0,
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "Showing 0 to 0 of 0 entries",
        infoFiltered: "(filtered from _MAX_ total entries)",
        infoPostFix: "",
        thousands: ",",
        loadingRecords: "Loading...",
        processing: "Processing...",
        search: "Search:",
        zeroRecords: "No matching records found",
        ajax: {
            url: url + "email/refresh_listing",
            dataType: "json",
            type: "POST",
            data: {
                current_list: current_list,
                emailType: t,
                caseType: i,
                list_type: list_type
            }
        },
        columns: [{
                data: "e_id"
            }, {
                data: "e_from"
            }, {
                data: "e_subject"
            }, {
                data: "e_date"
            }],
        order: [
            [3, "desc"]
        ],
        destroy: !0,
        aoColumnDefs: [{
                bSortable: !1,
                aTargets: [0]
            }, {
                bSearchable: !1,
                aTargets: [0]
            }],
        initComplete: function () {
            this.api().columns().every(function () {
                var e = this,
                        t = document.createElement("input");
                $(t).appendTo($(e.footer()).empty()).on("change", function () {
                    e.search($(this).val(), !1, !1, !0).draw()
                })
            })
        }
    });
    if ("delete" == e ? n.columns([0]).visible(!1) : n.columns([0]).visible(!0), "sent" == e) {
        var o = n.columns(1).header();
        $(o).html("To")
    } else {
        o = n.columns(1).header();
        $(o).html("From")
    }
}

function show_hide_main_window(e) {

    "email_detail" == e ? ($(".changeContentDiv").hide(), $(".email_conversation").hide(), $(".emailDetailDiv").show()) : "email_listing" == e ? ($(".changeContentDiv").hide(), $(".email_conversation").hide(), $(".emailListing").show()) : "email_compose" == e ? ($(".changeContentDiv").hide(), $(".email_conversation").hide(), $(".composeEmailDiv").show()) : "email_conversation" == e && ($(".changeContentDiv").hide(), $(".email_conversation").show())
}

function validate_ext_email(e) {
    var t = "",
        a = 0; - 1 !== e.indexOf(",") && (a = (t = e.split(",")).length);
    var l = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (a > 0) {
        for (i = 0; i < t.length; i++)
            if (!l.test(t[i].trim())) return swal({
                    title: "Alert",
                    text: "Oops! Please enter valid email adress.\n" + t[i].trim(),
                    type: "warning"
                }), !1
    } else if (!l.test(e.trim())) return swal({
            title: "Alert",
            text: "Oops! Please enter valid email address.\n" + e.trim(),
            type: "warning"
        }), !1;
    return !0
}

function validateAttachFileExtension(e) {
    return !!/(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.gif|\.png|\.txt|\.xlsx|\.xls|\.rtf|\.html|\.htm|\.wpd|\.wps|\.csv|\.ppt|\.pptx|\.zip|\.tar|\.xml|\.XLR|\.mp3|\.rar)$/i.test(e) || (swal({
        title: "Alert",
        text: "Oops! Please provide valid file format.\n" + e + " is not valid file format",
        type: "warning"
    }), !1)
}

function removeDuplicateUsingSet(e) {
    return Array.from(new Set(e))
}
$(document).on("click", ".confirm", function () {
    $('#mailsenddisable').removeAttr( "disabled" , "disabled");
});

$(document).on("click", ".refresh-mail", function () {
    grab_emails()
}), $(document).on("click", "#email_filter", function () {
    grab_emails()
}), $(document).ready(function () {
    $(".close_email").click(function (e) {
        $(".composeEmailDiv").hide();
    });
    $('input[type="radio"]').removeAttr("checked").iCheck("update");
    var e = "";
    grab_emails(), CKEDITOR.replace("mailbody"), $(".category-list a").click(function (t) {
        $(this).data("listing"), $(".category-list a").removeClass("activeli"), $(this).addClass("activeli"), e = $("#task-navigation li.active").attr("id"), grab_emails()
    }), $("#task-navigation li").on("click", "a", function () {
        $(".category-list a").removeClass("activeli"), $("#task-navigation li").removeClass("active"), $(this).parent("#task-navigation li").addClass("active"), "", "", "", grab_emails()
    }), $("input[name=emailtypes]:radio,input[name=casetypes]:radio").on("change", function (e) {
        grab_emails()
    }), $(document).on("click", "#clear_email_filter", function () {
        $(".category-list a").removeClass("activeli"), $("input[name=emailtypes]:radio,input[name=casetypes]:radio").prop("checked", !1), $(".refresh-mail").trigger("click")
    }), $('select[name="whoto"]').select2({
        placeholder: "Select User to Send Email",
        multiple: !0,
        selectOnClose: !0
    }).on("change", function () {
        $('select[name="whoto"]').valid()
    }), $("#send_mail_form").submit(function () {
     $('#mailsenddisable').attr( "disabled" , "disabled");
     setTimeout(function(){
    $('#mailsenddisable').removeAttr( "disabled" , "disabled");
        }, 3000);

        $('select[name="whoto"]').parent().find("#wvalid").remove(), $('input[name="emails"]').parent().find("span").remove(), $("#mail_subject").parent().find("span").remove();
        var e;
        e = $('select[name="whoto"]').val();
        var t = $("#mail_subject").val(),
                a = $('input[name="mail_urgent"]:checked').val(),
                i = $('input[name="emails"]').val(),
                l = CKEDITOR.instances.mailbody.getData(),
                n = $("#filenameVal").val(),
                o = $("#mailType").val(),
                s = $("#case_no").val(),
                r = !0;
        if (null == e && "" == i && ($('select[name="whoto"]').parent().append('<span id="wvalid" style="color:red;">Please Select a User to send mail to</span>'), $('input[name="emails"]').parent().append('<span style="color:red;">Please add external party to send email </span>'), setTimeout(function () {
            $('select[name="whoto"]').parent().find("#wvalid").remove(), $('input[name="emails"]').parent().find("span").remove()
        }, 5e3), r = !1), "" != i && (r = validate_ext_email($("input[name=emails]").val())), "" == t) return r = !1, $("#mail_subject").parent().append('<span style="color:red;">Please enter a Mail Subject</span>'), setTimeout(function () {
                $("#mail_subject").parent().find("span").remove()
            }, 5e3), !1;
        if ("" == l) return r = !1, $(".mail-text").prepend('<span id="content-mail-text" style="color:red;">Please Enter Mail Content</span>'), setTimeout(function () {
                $(document).find("#content-mail-text").remove()
            }, 5e3), !1;
        var c = new FormData;
        c.append("whoto", e), c.append("subject", t), c.append("urgent", a), c.append("ext_emails", i), c.append("mailbody", l), c.append("mailType", o), c.append("filenameVal", n), c.append("caseNo", s);
        $("input#afiles")[0].files.length;
        var m = 0;
        return $.each($('input[name="afiles[]"]'), function (e, t) {
            $.each(t.files, function (e, t) {
                r = validateAttachFileExtension(t.name), c.append("files_" + m, t), m += 1
            })
        }), "" != s && 0 != s ? $.ajax({
            url: url + "Prospects/CheckCaseno",
            type: "post",
            data: {
                pro_caseno: s
            },
            success: function (e) {
                var t = $.parseJSON(e);
                0 == t ? (swal({
                    title: "Alert",
                    text: "Please enter valid case number.",
                    type: "warning"
                }), r = !1) : 1 == t && 1 == r && $.ajax({
                    url: url + "email/send_email",
                    method: "POST",
                    dataType: "json",
                    cache: !1,
                    contentType: !1,
                    processData: !1,
                    data: c,
                    beforeSend: function (e) {
                        $.blockUI();
                    },
                    complete: function (e, t) {
                        $.unblockUI()
                    },
                    success: function (e) {
                        if ("true" != e.result) return "false" == e.result && "0" == e.ext_party ? (swal({
                                title: "Alert",
                                text: "Oops! Something went wrong. Email wasn't send to External Party.",
                                type: "warning"
                            }), !1) : "false" == e.result && "" != e.attachment_issue ? (swal({
                                title: "Alert",
                                text: "Oops! " + e.attachment_issue,
                                type: "warning"
                            }), !1) : (swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again later.",
                                type: "warning"
                            }), !1);
                        swal({
                            title: "Success!",
                            text: "Mail sent Successfully",
                            type: "success"
                        }), $(".composeEmailDiv").hide();
                    }
                })
            }
        }) : 1 == r && $.ajax({
            url: url + "email/send_email",
            method: "POST",
            dataType: "json",
            cache: !1,
            contentType: !1,
            processData: !1,
            data: c,
            beforeSend: function (e) {
                $.blockUI()
            },
            complete: function (e, t) {
                $.unblockUI()
            },
            success: function (e) {
                if ("true" != e.result) return "true" == e.result && "0" == e.ext_party ? (swal({
                        title: "Alert",
                        text: "Oops! Something went wrong. Email wasn't send to External Party.",
                        type: "warning"
                    }), !1) : "false" == e.result && "" != e.attachment_issue ? (swal({
                        title: "Alert",
                        text: "Oops! " + e.attachment_issue,
                        type: "warning"
                    }), !1) : (swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), !1);
                swal({
                    title: "Success!",
                    text: "Mail sent Successfully",
                    type: "success"
                }), $(".composeEmailDiv").hide()
            }
        }), !1
    }),$(".discard_message").click(function (e) {
        swal({
            title: "Are you sure?",
            text: "The changes you made will be lost",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, discard it!",
            closeOnConfirm: !0
        }, function () {
            $(".changeContentDiv").hide(), $(".email_conversation").hide(), $(".emailListing").show(), $(".att-add-file").not(":first").remove()
        })
    }), $(".mark_as_read").click(function (e) {
        e.preventDefault();
        var t, a = [];
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val());
            var i = $(this).find("input[type=checkbox]");
            i.prop("checked", i.prop("checked"))
        }), t = a.length, "" != (a = a.join()) ? $.ajax({
            url: url + "email/mark_as_read",
            dataType: "json",
            type: "POST",
            data: {
                filenames: a
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? (total = e.count - t, e.unread_count - t > 0 ? ($(".total_mail_count").html(e.unread_count).show(), $(".nav .email_count").html(e.unread_count).show(), $(".mail-box-header .email_count").html("(" + (e.unread_count - t) + ")").show()) : $(".total_mail_count").hide(), $(".icheckbox_square-green.checked").parents("tr.unread").addClass("read").removeClass("unread"), $.each($("tbody tr .checked"), function (e, t) {
                    $(t).iCheck("uncheck")
                })) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                }), grab_emails()
            }
        }) : swal({
            title: "Alerts",
            text: "Please select atleast one unread Email to mark it as Read",
            type: "warning"
        })
    }), $(".mark_as_old").click(function (e) {
        e.preventDefault();
        var t, a = [];
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val())
        }), t = a.length, "" != (a = a.join()) ? swal({
            title: "Are you sure?",
            text: "You want to move this email to old?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, move it!",
            closeOnConfirm: !0
        }, function () {
            $.ajax({
                url: url + "email/mark_as_old",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: a
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? (total = e.count - t, e.unread_count - t > 0 ? ($(".total_mail_count").html(e.unread_count - t).show(), $(".nav .email_count").html(e.unread_count - t).show(), $(".mail-box-header .email_count").html("(" + (e.unread_count - t) + ")").show()) : $(".total_mail_count").hide(), $(".icheckbox_square-green.checked").parents("tr.unread").addClass("read").removeClass("unread"), $.each($("tbody tr .checked"), function (e, t) {
                        $(t).iCheck("uncheck")
                    })) : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), grab_emails()
                }
            })
        }) : swal({
            title: "Alerts",
            text: "Please select atleast one unread Email to mark it as Old",
            type: "warning"
        })
    }), $(".mark_as_imp").click(function (e) {
        e.preventDefault();
        var t, a = [];
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val())
        }), t = a.length, a = a.join(), t > 0 ? $.ajax({
            url: url + "email/mark_as_urgent",
            dataType: "json",
            type: "POST",
            data: {
                filenames: a
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? ($.each($(".icheckbox_square-green.checked").parents("tr").find("td.mail-ontact"), function (e, t) {
                    0 == $(t).find("i.fa.fa-circle.text-danger").length && $(t).append(' <i class="fa fa-circle text-danger"></i>')
                }), $.each($("tbody tr .checked"), function (e, t) {
                    $(t).iCheck("uncheck")
                })) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                }), grab_emails()
            }
        }) : swal({
            title: "Alerts",
            text: "Please select atleast one email to mark it as Important",
            type: "warning"
        })
    }), $(".refresh").click(function (t) {
        t.preventDefault(), $.ajax({
            url: url + "email/refresh_listing",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                start: 0,
                length: length
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                total = e.count, allEmail = e.count, e.unread_count > 0 ? ($(".total_mail_count").html(e.unread_count).show(), $(".mail-box-header .total_mail_count").html("(" + e.unread_count + ")").show()) : ($(".total_mail_count").hide(), $(".dropdown .email_count").text(e.unread_count).show());
                var t = "";
                $.each(e.result, function (e, a) {
                    t += generate_tr_html(a)
                }), $(".mail-box tbody").html(t), $(".i-checks").iCheck({
                    checkboxClass: "icheckbox_square-green",
                    radioClass: "iradio_square-green"
                }), $("input#selectall").iCheck("uncheck")
            }
        })
    }), $(".compose-mail").click(function (e) {
        $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"), CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"), show_hide_main_window("email_compose"), $(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html("Compose Email")
    }), $(document.body).on("click", ".back_email", function () {
        show_hide_main_window("email_listing"), grab_emails()
    }), $(document.body).on("click", "a.read-email", function () {
        var t = $(this).data("filename"),
                a = $(this).data("e2id"),
                i = $(this).attr("email_con_id"),
                l = $(this).attr("ethread"),
                n = $(this).attr("iore"),
                o = $(this),
                s = $("#task-navigation li.active").attr("id");
        l > 1 ? $.ajax({
            url: url + "email/emailThreads",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                e2id: a,
                email_con_id: i,
                vfrom: "email_details"
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                o.parent().parent().after(e), o.parent().parent().find("a").removeClass("read-email"), o.parent().parent().find("a").addClass("read-email-close")
            }
        }) : $.ajax({
            url: url + "email/view",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                iore: n,
                e2id: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (a) {
                if (show_hide_main_window("email_detail"), $("#filenameVal").val(t), $("#reply_iore").val(n), a.unread_count > 0 ? ($(".total_mail_count").html(a.unread_count).show(), $(".email_count").html(a.unread_count).show(), $(".mail-box-header .total_mail_count").html("(" + a.unread_count + ")").show()) : $(".total_mail_count").hide(), "sent" == e && $(".total_mail_count").hide(), $("#emailSubject").html(a.emails.subject), $("#emailDatetime").html(moment(a.emails.datesent).format("DD MMM YYYY - hh:mm A")), $("#emailfromName").html(a.emails.whofrom), a.emails.whofrom == a.emails.email_con_people && (a.emails.email_con_people = current_user_injs), "0" != a.emails.caseno) {
                    $("#gotocase").html("");
                    var i = url + "cases/case_details/" + a.emails.caseno;
                    $("#gotocase").html('Go To Case : <a href="' + i + '" target="_blank" >' + a.emails.caseno + "</a>")
                } else $("#gotocase").html("");
                if ("sent" == e) $("#inboxto").html(""), $("#sentto").html("<br>To: " + a.emails.email_con_people + "<br>");
                else {
                    $("#sentto").html("");
                    var l = null != a.emails.email_con_people ? a.emails.email_con_people : a.emails.whoto;
                    $("#inboxto").html("<br><br>To: " + l)
                }
                $(".mail-body #emailBody").html(a.emails.mail_body), $(".mail-body #file_with_attachment").html(a.file_with_attachment), $(".move_to_trash_one").attr("data-id", t), $(".print_email").attr("data-id", t), 1 == a.reply_all ? ($(".emailDetailDiv .reply_all_compose_email").show(), $(".emailDetailDiv .reply_all_compose_email").attr("email_con_id", a.emails.email_con_id)) : $(".emailDetailDiv .reply_all_compose_email").hide(), "delete" == s ? ($(".move_to_old_one").hide(), $(".move_to_trash_one").hide()) : ($(".move_to_old_one").show(), $(".move_to_trash_one").show())
                $(".mail-body #emailBody").html(a.emails.mail_body), $(".mail-body #file_with_attachment").html(a.file_with_attachment), $(".move_to_trash_one").attr("data-id", t), $(".print_email").attr("data-id", t), 1 == a.reply_all ? ($(".emailDetailDiv .reply_all_compose_email").show(), $(".emailDetailDiv .reply_all_compose_email").attr("email_con_id", a.emails.email_con_id)) : $(".emailDetailDiv .reply_all_compose_email").hide(), "delete" == s ? ($(".move_to_old_one").hide(), $(".move_to_trash_one").hide()) : ($(".move_to_old_one").show(), $(".move_to_trash_one").show()),
                        "new" != s ? $(".move_to_old_one").hide() : $(".move_to_old_one").show()
            }
        });
    }), $(document.body).on("click", "a.read-email-close", function () {
        $(this).parents("tr").next("tr.rclose").hide(), $(this).removeClass("read-email-close"), $(this).addClass("read-email")
    }), $(document.body).on("click", ".reply_compose_email", function () {
        $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
        var t = $(this).attr("email_con_id"),
                a = $(this).attr("ethread"),
                i = $("#reply_iore").val(),
                l = $("#filenameVal").val();
        $(".compose-mail").trigger("click"), $.ajax({
            url: url + "email/reply_email",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                filename: l,
                email_con_id: t,
                ethread: a,
                iore: i
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                $("#mailType").val("reply"), $("#whoto").val(e.emails[0].whofrom).trigger("change"), "E" == e.emails[0].iore && $("#emails").val(e.emails[0].whofrom), $("#mail_subject").val(e.emails[0].subject), $("#mail_urgent").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent").iCheck("check"), $("#case_no").val(e.emails[0].caseno)
            }
        })
    }), $(".move_to_old_one").click(function (e) {
        var t = $("#filenameVal").val();
        "" != t ? (swal({
            title: "Are you sure?",
            text: "You want to move this email to old?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, move it!",
            closeOnConfirm: !0
        }, function () {
            $.ajax({
                url: url + "email/mark_as_old",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: t
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? grab_emails() : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    })
                }
            })
        }), grab_emails()) : swal({
            title: "Alerts",
            text: "Please select atleast one unread Email to mark it as Read",
            type: "warning"
        })
    }), $(".forward_email").click(function (t) {
        $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
        var a = $("#reply_iore").val(),
                i = $("#filenameVal").val();
        $(".compose-mail").trigger("click"), $.ajax({
            url: url + "email/forward_email",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                filename: i,
                iore: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                if ($("#mailType").val("forward"), $("#mail_subject").val(e.emails[0].subject), "Y" == e.emails[0].urgent ? $("#mail_urgent").parent("div").addClass("checked") : $("#mail_urgent").parent("div").removeClass("checked"), e.emails[0].caseno > 0 && $("#case_no").val(e.emails[0].caseno), "" != e.file_with_attachment) {
                    new FormData("#send_mail_form");
                    $("#file_with_attachment_fwd").html(e.file_with_attachment), $("#fwd_with_attachment").prop("checked", !0)
                }
                CKEDITOR.instances.mailbody.setData(e.emails[0].mail_body + e.file_with_attachment)
            }
        })
    }), $(".move_to_trash").click(function (e) {
        e.preventDefault();
        var t, a = [],
                i = [],
                l = "";
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val())
        });
        var n = removeDuplicateUsingSet(a);
        t = a.length, a = a.join(), i = i.join(), l = t > 1 ? n.length + " emails" : n.length + " email", i.length, t > 0 ? swal({
            title: "Are you sure?",
            text: "You will not be able to recover " + l + " !",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: !1
        }, function () {
            $.ajax({
                url: url + "email/move_to_trash",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: a,
                    email_con_ids: i
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? setTimeout(function () {
                        swal({
                            title: "Success!",
                            text: "Your " + l + " has been deleted.",
                            type: "success"
                        })
                    }, 1e3) : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), e.unread_count > 0 ? ($(document).find("#side-menu .email_count").html(e.unread_count).show(), $(document).find(".dropdown .email_count").html(e.unread_count).show()) : $(" .total_mail_count").hide(), grab_emails()
                }
            })
        }) : swal({
            title: "Alerts",
            text: "Please select atleast one  Email to Delete",
            type: "warning"
        })
    }), $(".move_to_trash_one").click(function (e) {
        e.preventDefault();
        var t = $(".move_to_trash_one").attr("data-id");
        $.ajax({
            url: url + "email/move_to_trash",
            dataType: "json",
            type: "POST",
            data: {
                filenames: t
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? (swal({
                    title: "Alert",
                    text: "Email(s) deleted successfully",
                    type: "success"
                }), $(".changeContentDiv").hide(), $(".email_conversation").hide(), e.unread_count > 0 ? ($(" .total_mail_count").html(e.unread_count).show(), $(" .mail-box-header .total_mail_count").html("(" + e.unread_count + ")").show()) : $(" .total_mail_count").hide()) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                })
            }
        }), grab_emails()
    }), $(document.body).on("click", ".print_email", function () {
        var t = $(".print_email").attr("data-id"),
                a = $("#reply_iore").val();
        $.ajax({
            url: url + "email/printEmail",
            method: "POST",
            data: {
                current_list: e,
                filename: t,
                iore: a
            },
            success: function (e) {
                $(e).printThis({
                    debug: !1,
                    header: "<h2><center>View Message</center></h2>"
                })
            }
        })
    }), $(document.body).on("click", "a.open-read-email", function () {
        var e = $(this).data("filename"),
                t = $(document).find("#task-navigation li.active").attr("id"),
                a = ($(this).attr("email_con_id"), $(this).attr("ethread"), $(this).attr("iore")),
                i = ($(this), "#collapse" + e);
        $.ajax({
            url: url + "email/view",
            dataType: "json",
            type: "POST",
            data: {
                current_list: t,
                filename: e,
                iore: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (l) {
                $("#filenameVal").val(e), $("#reply_iore").val(a), l.unread_count > 0 ? ($(document).find("#side-menu .email_count").html(l.unread_count).show(), $(document).find(".dropdown .email_count").html(l.unread_count).show(), $(" .total_mail_count").html(l.unread_count).show(), $(" .mail-box-header .total_mail_count").html("(" + l.unread_count + ")").show()) : $(" .total_mail_count").hide(), "sent" == t && $(" .total_mail_count").hide(), $("body").find(i).find("#emailSubject").html(l.emails.subject), $("body").find(i).find("#emailDatetime").html(moment(l.emails.datesent).format("HH:mmA DD MMM YYYY")), $("body").find(i).find("#emailfromName").html(l.emails.whofrom_name + "(" + l.emails.whofrom + ")"), $("body").find(i).find(".mail-body #emailBody").html(l.emails.mail_body), $("body").find(i).find(".mail-body #file_with_attachment").html(l.file_with_attachment), $("body").find(i).find(".move_to_trash_one").attr("data-id", e), $("body").find(i).find(".print_email").attr("data-id", e)
            }
        })
    }), $(document.body).on("click", ".reply_all_compose_email", function () {
        $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
        var t = $(this).attr("email_con_id"),
                a = ($(this).attr("ethread"), $("#reply_iore").val()),
                i = $("#filenameVal").val();
        $(".compose-mail").trigger("click"), $.ajax({
            url: url + "email/reply_all_email",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                filename: i,
                email_con_id: t,
                iore: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                $("#mailType").val("reply"), e.internal_party.length > 0 && $("#whoto").val(e.internal_party).trigger("change"), "" != e.external_party && $("#emails").val(e.external_party), $("#mail_subject").val(e.emails[0].subject), $("#mail_urgent").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent").iCheck("check"), $("#case_no").val(e.emails[0].caseno)
            }
        })
    })
}), $(document.body).on("change", "input#selectall", function (e) {
    $(this).is(":checked");
    var t = $("input.email-check-id");
    t.prop("checked", !t.prop("checked")), $(this).is(":checked") ? $.each(t, function (e, t) {
        $(t).is(":checked") && $(t).data("ethread") > 0 && ($(t).closest("td").next("td").find("a.read-email").trigger("click"), setTimeout(function () {
            var e = $(t).closest("tr").next("tr").find("td").find("table").find("tbody tr").find("td input[type=checkbox]");
            $.each(e, function (e, t) {
                $(t).prop("checked", !0)
            })
        }, 700))
    }) : grab_emails()
}), $(document).on("change", "input#afiles", function () {
    for (var e = $("input#afiles")[0].files.length, t = 0; t < e; t++) validateAttachFileExtension($("#afiles")[0].files[t].name)
}), $(document).on("click", "i.fa-times-circle", function () {
    var e = $(this).attr("data"),
            t = $(this);
    swal({
        title: "Warning!",
        text: "Are you sure you want to remove this attachment?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: !1
    }, function () {
        $.blockUI(), $.ajax({
            url: url + "email/removeattachment",
            data: {
                remove_file: e
            },
            method: "POST",
            success: function (e) {
                $.unblockUI();
                var a = $.parseJSON(e);
                1 == a.status ? (swal("Success !", a.message, "success"), t.parent("li").remove()) : swal("Oops...", a.message, "error")
            }
        })
    })
}), $(window).on("beforeunload", function (e) {
    if ($(".composeEmailDiv").is(":visible")) {
        return e.returnValue = "The changes you made will be lost, Do you want to discard it?"
    }
    e = null
}), $(document).on("click", ".addfile", function () {
    $(".att-add-file:last").after('<label class="col-sm-2 control-label"></label><div class="col-sm-7 att-add-file marg-top5 clearfix"> <input type="file" name="afiles[]" id="afiles" multiple ><i class="fa fa-plus-circle marg-top5 addfile" aria-hidden="true"></i> <i class="fa fa-times marg-top5 removefile" aria-hidden="true"></i></div>')
}), $(document).on("click", ".removefile", function () {
    $(this).closest(".att-add-file").prev("label").remove(), $(this).closest(".att-add-file").remove()
}), $(document).on("change", '.email-new-dataTable tr input[type="checkbox"]', function () {
    var e = $(this);
    return e.data("ethread") > 0 && (e.closest("td").next("td").find("a.read-email").trigger("click"), setTimeout(function () {
        var t = e.closest("tr").next("tr").find("td").find("table").find("tbody tr").find("td input[type=checkbox]");
        $.each(t, function (e, t) {
            $(t).prop("checked", !0)
        })
    }, 700)), !1
}), $("#Self").click(function (e) {
    $(".category-list a").removeClass("activeli"), $("input[name=emailtypes]:radio").prop("checked", !1), $(".refresh-mail").trigger("click"), $(".externalemailtype").removeAttr("checked"), $(".internalemailtype").removeAttr("checked"), $(".externalemailtype").attr("disabled", "disabled"), $(".internalemailtype").attr("disabled", "disabled")
}), $("#sent").click(function (e) {
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled")
}), $("#new").click(function (e) {
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled")
}), $("#old").click(function (e) {
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled")
}), $("#delete").click(function (e) {
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled")
});
