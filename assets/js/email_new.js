
var n = "";
/*var url = "<?php echo base_url(); ?>";*/
function grab_emails() {
    var e = $("#task-navigation li.active").attr("id");
    var current_list = $("#task-navigation li.active").attr('id');
    if (current_list == 'Self') {
        $(".mark_as_old").hide();
    }
    $("#searchFiltr").css("display", "block");
    $(".category-list a").hasClass("activeli") ? list_type = $(".activeli").data("listing") : list_type = "", current_list = e;
    var t = "";
    var a = $("input[name=emailtypes]:checked");
    a.length > 0 && (t = a.val());
    var i = "";
    var l = $("input[name=casetypes]:checked");
    l.length > 0 && (i = l.val());
    if ("delete" == e) {
        $(".mark_button button").hide();
        /*$("#selectall_div").hide();*/
        $(".reply_compose_email").hide();
        $(".reply_all_compose_email").hide();
        $(".forward_email").hide();
        $(".envelope_email_open").hide();
        $(".move_to_new_one").show();
        $(".move_to_new").show();
    } else {
        $(".mark_button button").show();
        /*$("#selectall_div").show();*/
        $(".reply_compose_email").show();
        $(".reply_all_compose_email").show();
        $(".forward_email").show();
        $(".move_to_new").hide();
        $(".move_to_new_one").hide();
    }

    if ("old" == e || "delete" == e) {
        $(".mark_as_old").hide();
    } else {
        $(".mark_as_old").show();
    }

    if ("sent" == e) {
        $(".category-list").hide();
        $(".envelope_email_open").hide();
        $(".envelope_email_open_unread").hide();
    } else {
        $(".category-list").show();
    }

    if ("new" != e) {
        $(".mark_as_old,.move_to_old_one").hide();
    }

    n = $(".email-new-dataTable").DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        stateSaveParams: function (settings, data) {
            data.search.search = "";
            data.start = 0;
            delete data.order;
            data.order = [1, "ASC"];
        },
        scrollX: "100%",
        scrollY: "585px",
        scrollCollapse: true,
        ajax: {
            url: url + "email/refresh_listing_new",
            dataType: "json",
            type: "POST",
            data: {
                current_list: current_list,
                emailType: t,
                caseType: i,
                list_type: list_type
            }
        },
        columns: [
            {
                data: "parcelid",
                width: "1%"
            },
            {
                data: "whofrom",
                width: "99%"
            }
        ],
        order: [
            [1, "desc"]
        ],
        destroy: !0,
        aoColumnDefs: [
            {
                bSortable: !1,
                aTargets: [0]
            },
            {
                bSearchable: !1,
                aTargets: [0]
            }
        ],
        initComplete: function () {
            /*this.api().columns().every(function () {
                var e = this, t = document.createElement("input");
                $(t).appendTo($(e.footer()).empty()).on("change", function () {
                    e.search($(this).val(), !1, !1, !0).draw();
                });
            });

            if ($("#DataTables_Table_0 tbody tr td:eq(1) a").length > 0) {
            } else {
                $(".changeContentDiv").hide();
                $(".placeholderEmailDiv").show();
            }
            */
        },
        fnDrawCallback: function (oSettings) {
            $('#selectall').prop('checked', false);
            n.columns.adjust();
        }
    });

    if ("sent" == e) {
        var o = n.columns(1).header();
        $(o).html("Emails");
    } else {
        n.columns([0]).visible(!0);
        o = n.columns(1).header();
        $(o).html("Emails");
    }
}
/*$(".compose-mail").click(function (e) {
    $(".files_global_var").remove(), $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"), 
    CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"), show_hide_main_window("email_compose"), 
    $(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html("")
});*/

$(".compose-mail").click(function (e) {
    $("#whoto").val([]).trigger("change"),show_hide_main_window("email_compose");
    /* return false;*/
});

$(document.body).on("click", ".back_email", function () {
    show_hide_main_window("email_listing"), grab_emails()
});
    
function show_hide_main_window(e) {
    $('#includesignature').prop("disabled", false);
    $("#whoto").val([]).trigger("change");
    if ($(".changeContentDiv:visible").hasClass("composeEmailDiv")) {
        swal({
            title: "Are you sure?",
            text: "The changes you made will be lost",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, discard it!",
            closeOnConfirm: !0
        }, function () {
            $(".files_global_var").remove(), $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"),CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"), show_hide_main_window("email_compose"),$(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html(""),$('.fileremove').remove(),$('#totalFileSize').val('');
            if (e == "email_detail") {
                $(".changeContentDiv").hide();
                $(".email_conversation").hide();
                $(".emailDetailDiv").show();
            } else if (e == "email_listing") {
                $(".changeContentDiv").hide();
                $(".email_conversation").hide();
                $(".placeholderEmailDiv").show();
            } else if (e == "email_compose") {
                $(".changeContentDiv").hide();
                $(".email_conversation").hide();
                $(".composeEmailDiv").show();
            } else if (e == "email_conversation") {
                $(".changeContentDiv").hide();
                $(".email_conversation").show();
            }
        });
    } else {
        if (e == "email_detail") {
            $(".changeContentDiv").hide();
            $(".email_conversation").hide();
            $(".emailDetailDiv").show();
        } else if (e == "email_listing") {
            $(".changeContentDiv").hide();
            $(".email_conversation").hide();
            $(".placeholderEmailDiv").show();
        } else if (e == "email_compose") {
            $(".changeContentDiv").hide();
            $(".email_conversation").hide();
            $(".composeEmailDiv").show();
        } else if (e == "email_conversation") {
            $(".changeContentDiv").hide();
            $(".email_conversation").show();
        }
    }
   
    
}
$(document).on("click", ".confirm", function () {
    $('#mailsenddisable').removeAttr("disabled", "disabled");
    $('#mailsenddisable1').removeAttr("disabled", "disabled");
});
$(document).ready(function () {
    $(".close_email").click(function (e) {
         swal({
            title: "Are you sure?",
            text: "The changes you made will be lost",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, discard it!",
            closeOnConfirm: !0
        }, function () {
            $(".files_global_var").remove(), $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"),CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"), $(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html(""),$(".composeEmailDiv").hide(), $(".placeholderEmailDiv").show(),$('.fileremove').remove(),$('#totalFileSize').val('');
        });
        
    });
    $('input[type="radio"]').removeAttr("checked").iCheck("update");
    grab_emails();
    CKEDITOR.replace("mailbody");
    $(".category-list a").click(function (t) {
        $(this).data("listing");
        $(".category-list a").removeClass("activeli");
        $(this).addClass("activeli");
        e = $("#task-navigation li.active").attr("id");
        grab_emails();
    });
    $("#task-navigation li").on("click", "a", function () {
        $(".category-list a").removeClass("activeli");
        $("#task-navigation li").removeClass("active");
        $(this).parent("#task-navigation li").addClass("active");
        /* , "", "", "", */
        grab_emails();
    });
    $('select[name="whoto"]').select2({
        placeholder: "Select User to Send Email",
        multiple: !0,
        selectOnClose: 0
    }).on("change", function () {
        $('select[name="whoto"]').valid();
    });
    
     $("#send_mail_form").submit(function () {
        $('#mailsenddisable').attr("disabled", "disabled");
        $('#mailsenddisable1').attr("disabled", "disabled");
        setTimeout(function () {
            $('#mailsenddisable').removeAttr("disabled", "disabled");
            $('#mailsenddisable1').removeAttr("disabled", "disabled");
        }, 3000);

        $('select[name="whoto"]').parent().find("#wvalid").remove();
        $('input[name="emails"]').parent().find("span").remove();
        $("#mail_subject").parent().find("span").remove();

        var e;
        if($('select[name="whoto"]').val())
            e = $('select[name="whoto"]').val();
        else
            e = '';
        var t = $("#mail_subject").val();
        var a = $('input[name="mail_urgent"]:checked').val();
        var i = $('input[name="emails"]').val();
        var l = CKEDITOR.instances.mailbody.getData();
        var n = $("#filenameVal").val();
        var o = $("#mailType").val();
        var s = $("#case_no").val();
        var r = !0;
        
        if ((null == e || "" == e ) && "" == i && ($('select[name="whoto"]').parent().append('<span id="wvalid" style="color:red;">Please Select a User to send mail to</span>'), $('input[name="emails"]').parent().append('<span style="color:red;">Please add external party to send email </span>'), setTimeout(function () {
            $('select[name="whoto"]').parent().find("#wvalid").remove(), $('input[name="emails"]').parent().find("span").remove()
        }, 5e3), r = !1));
        if ("" == l)
            return r = !1, $(".mail-text").prepend('<span id="content-mail-text" style="color:red;">Please Enter Mail Content</span>'), setTimeout(function () {
                $(document).find("#content-mail-text").remove()
            }, 5e3), !1;

        var c = new FormData;
        c.append("whoto", e), c.append("subject", t), c.append("urgent", a), c.append("ext_emails", i), c.append("mailbody", l), c.append("mailType", o), c.append("filenameVal", n), c.append("caseNo", s);

        $("input#afiles")[0].files.length;
        var filelength = $("input[name='afiles1[]']");
        var fileGlobal = [];
        var tmp_fileGlobal = $("input[name='files_global_var[]']").map(function () {
            return $(this).val();
        }).get();

        $.each(tmp_fileGlobal, function (k, v) {
            var array = $.map(JSON.parse(v), function (value, index) {
                return [index + ':' + value];
            });
            fileGlobal[k] = array;
        });
        var files_arry = [];
        c.append("fileGlobalVariable", JSON.stringify(fileGlobal));
        return $.each(filelength, function (e, t) {
            files_arry.push(t.defaultValue);
            if (filelength.length == files_arry.length) {
                c.append("files", files_arry)
            }
        }), "" != s && 0 != s ? $.ajax({
            url: url + "Prospects/CheckCaseno",
            type: "post",
            data: {
                pro_caseno: s
            },
            success: function (e) {
                var t = $.parseJSON(e);
                0 == t ? (swal({
                    title: "Alert",
                    text: "Please enter valid case number.",
                    type: "warning"
                }), r = !1) : 1 == t && 1 == r && $.ajax({
                    url: url + "email/send_email",
                    method: "POST",
                    dataType: "json",
                    cache: !1,
                    contentType: !1,
                    processData: !1,
                    data: c,
                    beforeSend: function (e) {
                        $.blockUI();
                    },
                    complete: function (e, t) {
                        $.unblockUI()
                    },
                    success: function (e) {

                        if ("true" != e.result)
                            return "false" == e.result && "0" == e.ext_party ? (swal({
                                title: "Alert",
                                text: "Oops! Some went wrong. Email wasn't send to External Party.",
                                type: "warning"
                            }), !1) : "false" == e.result && "" != e.attachment_issue ? (swal({
                                title: "Alert",
                                text: "Oops! " + e.attachment_issue,
                                type: "warning"
                            }), !1) : (swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again later.",
                                type: "warning"
                            }), !1);
                        swal({
                            title: "Success!",
                            text: "Mail is sent successfully.",
                            type: "success"
                        }),$(".composeEmailDiv").hide(), $(".placeholderEmailDiv").show(),$(".files_global_var").remove(), $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"),CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"),$(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html(""),$('.fileremove').remove(),$('#totalFileSize').val(''),grab_emails();
                    }
                })
            }
        }) : 1 == r && $.ajax({
            url: url + "email/send_email",
            method: "POST",
            dataType: "json",
            cache: !1,
            contentType: !1,
            processData: !1,
            data: c,
            beforeSend: function (e) {
                $.blockUI()
            },
            complete: function (e, t) {
                $.unblockUI()
            },
            success: function (e) {
                if ("true" != e.result)
                    return "true" == e.result && "0" == e.ext_party ? (swal({
                        title: "Alert",
                        text: "Oops! Some went wrong. Email wasn't send to External Party.",
                        type: "warning"
                    }), !1) : "false" == e.result && "" != e.attachment_issue ? (swal({
                        title: "Alert",
                        text: "Oops! " + e.attachment_issue,
                        type: "warning"
                    }), !1) : (swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), !1);
                swal({
                    title: "Success!",
                    text: "Mail is sent successfully.",
                    type: "success"
                }),$(".composeEmailDiv").hide(), $(".placeholderEmailDiv").show(),$(".files_global_var").remove(), $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"),CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"),$(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html(""),$('.fileremove').remove(),$('#totalFileSize').val(''),grab_emails();
            }
        }), !1
    });
function validate_ext_email(e) {
var t = "", a = 0;
-1 !== e.indexOf(",") && (a = (t = e.split(",")).length);
var l = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
if (a > 0) {
    for (i = 0; i < t.length; i++)
        if (!l.test(t[i].trim()))
            return swal({
                title: "Alert",
                text: "Oops! Please enter valid email adress.\n" + t[i].trim(),
                type: "warning"
            }), !1
} else if (!l.test(e.trim()))
    return swal({
        title: "Alert",
        text: "Oops! Please enter valid email address.\n" + e.trim(),
        type: "warning"
    }), !1;
return !0
}

$(document.body).on("click", "a.read-email", function () {
    $('tr').removeClass('email-table-selected');
    $(this).parent().parent().addClass('email-table-selected');
    var t = $(this).data("parcelid"),
        o = $(this),
        s = $("#task-navigation li.active").attr("id");
        $.ajax({
        url: url + "email/view",
        dataType: "json",
        type: "POST",
        data: {
            current_list: s,
            parcelid: t,
        },
        beforeSend: function () {
            start = 0, $.blockUI()
        },
        complete: function () {
            $.unblockUI()
        },
        success: function (a) {
            $("#filenameVal").val(t);
            $('.openunread_filename').val(t);
            $('#emailSubject').html(a.emails[0].subject);
            $('#emailfromName').html(a.emails[0].whofrom);
            $('#inboxto').html('To: '+a.emails[0].whoto);
            $(".move_to_trash_one").attr("data-id", t);
            $(".move_to_new_one").attr("data-id", t);
            $(".print_email").attr("data-id", t);
            if(a.emails[0].whotoexternal){
                var str = a.emails[0].whoto+','+a.emails[0].whotoexternal;
                var trim = str.replace(/(^\s*,)|(,\s*$)/g, '');
                $('#inboxto').html('To: '+trim);
            }

            if (a.file_with_attachment) {
                $(".emailDetailDiv").addClass("file_with_attachment");
            } else {
                $(".emailDetailDiv").removeClass("file_with_attachment");
            }
            $('#emailBody').html(a.emails[0].body);
             $(".mail-body #file_with_attachment").html(a.file_with_attachment);
            $('#emailDatetime').html(moment(a.emails[0].datesent).format("DD MMM YYYY - hh:mm A"));
            if (a.emails[0].readyet == 'Y') {
                $('.envelope_email_open_unread').show();
                $('.envelope_email_open').hide();
                $('.reply_all_compose_email').show();
            } else {
                $('.envelope_email_open').show();
                $('.envelope_email_open_unread').hide();
                $('.reply_all_compose_email').show();
            }
            if ("new" != s) {
                $(".mark_as_old,.move_to_old_one").hide();
            }else{
                $(".mark_as_old,.move_to_old_one").show();
            }
            show_hide_main_window("email_detail");
            var caseInfo = '';
                if (a.emails[0].caseno && a.emails[0].caseno != 0) {
                    $(".gotocase").html('');
                    $.ajax({
                        url: url + "cases/get_party_info",
                        dataType: "json",
                        type: "POST",
                        async: false,
                        data: {
                            caseno: a.emails[0].caseno
                        },
                        success: function (result) {
                            if (typeof (result['firm']) != 'undefined' && result['firm'] != '') {
                                caseInfo = result['firm'];
                            } else if (typeof (result['first']) != 'undefined' && typeof (result['last']) != 'undefined') {
                                caseInfo = result['first'] + ' ' + result['last'];
                            } else {
                                caseInfo = '';
                            }
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                    var i = url + "cases/case_details/" + a.emails[0].caseno;
                    $(".gotocase").html('Go To Case : <a href="' + i + '" target="_blank" >' + a.emails[0].caseno + " " + caseInfo + "</a>")
                }else{
                    $(".gotocase").html('');
                }
            
        }
    });
});
});

$(document.body).on("click", "a.envelope_email_open", function () {
        var t = $('.openunread_filename').val(),
            o = $(this),
            s = $("#task-navigation li.active").attr("id");

        var emailcount = $('#task-navigation').find('.active').attr('id') != 'delete' ? $('.emailchk').text() - 1 : $('.emailchk').text();

        $.ajax({
            url: url + "email/markreadviewNew",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (a) {
                $("#check_click_unread").val("1");
                $("a[data-parcelid=" + t + "]").removeClass("notreadyet");
                $('.envelope_email_open').hide();
                $('.envelope_email_open_unread').show();
                $("td a[data-parcelid=" + t + "] i").removeClass("text-navy").addClass("text-warning");
                $(".move_to_trash_one").attr("data-id", t);
                $(".move_to_new_one").attr("data-id", t);
                $(".print_email").attr("data-id", t);
             }
        });
    });
    
$(document.body).on("click", "a.envelope_email_open_unread", function () {
        var t = $('.openunread_filename').val(),
            o = $(this),
            s = $("#task-navigation li.active").attr("id");

        var emailcount = $('#task-navigation').find('.active').attr('id') != 'delete' ? parseInt($('.emailchk').text()) + 1 : parseInt($('.emailchk').text());
        $.ajax({
            url: url + "email/markunreadviewnew",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (a) {
                $("#check_click_unread").val("1");
                $("a[data-parcelid=" + t + "]").addClass("notreadyet");
                $('.envelope_email_open').show();
                $('.envelope_email_open_unread').hide();
                if(s != 'old' && s!= 'sent') {
                    $(".email_count").html(emailcount);
                }
                $(".envelope_email_open").removeClass("text-warning");
                $("td a[data-parcelid=" + t + "] i").removeClass("text-warning").addClass("text-navy");
                $(".move_to_trash_one").attr("data-id", t);
                $(".move_to_new_one").attr("data-id", t);
                $(".print_email").attr("data-id", t);
            }
        });
    });

$(document).on("click", ".addfile", function () {
    $(".att-add-file:last").after('<label class="col-sm-2 control-label"></label><div class="col-sm-12 att-add-file marg-top5 clearfix no-padding"> <input type="file" name="afiles[]" id="afiles" multiple ><i class="fa fa-plus-circle addfile" aria-hidden="true"></i> <i class="fa fa-times removefile" aria-hidden="true"></i></div>')
});

$(document).on("click", ".removefile", function () {
    $(this).closest(".att-add-file").prev("label").remove(), $(this).closest(".att-add-file").remove()
});

$("#Self").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'hidden');
    $(".category-list a").removeClass("activeli");
    $("input[name=emailtypes]:radio").prop("checked", !1);
    // $(".refresh-mail").trigger("click");
    $(".externalemailtype").removeAttr("checked");
    $(".internalemailtype").removeAttr("checked");
    $(".externalemailtype").attr("disabled", "disabled");
    $(".internalemailtype").attr("disabled", "disabled");
    $(".changeContentDiv").hide();
    $(".placeholderEmailDiv").show();
});

$("#sent").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'hidden');
    $("input[name=emailtypes]:radio").prop("checked", !1);
    /* $(".refresh-mail").trigger("click"); */
    $(".externalemailtype").removeAttr("checked");
    $(".internalemailtype").removeAttr("checked");
    $(".externalemailtype").attr("disabled", "disabled");
    $(".internalemailtype").attr("disabled", "disabled");
    $(".changeContentDiv").hide();
    $(".placeholderEmailDiv").show();
    /*$(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"),*/
});

$("#new").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'visible');
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"), $(".changeContentDiv").hide(), $(".placeholderEmailDiv").show()
});

$("#old").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'visible');
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"), $(".changeContentDiv").hide(), $(".placeholderEmailDiv").show()
});

$("#delete").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'visible');
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"), $(".changeContentDiv").hide(), $(".placeholderEmailDiv").show()
});

$("input[name=emailtypes]:radio,input[name=casetypes]:radio").on("change", function (e) {
    grab_emails();
});

$(document).on("click", "#clear_email_filter", function () {
    $(".category-list a").removeClass("activeli");
    $("input[name=emailtypes]:radio,input[name=casetypes]:radio").prop("checked", !1);
    $(".refresh-mail").trigger("click");
});

$(".mark_as_old").click(function (e) {
    e.preventDefault();
    var t, a = [];
    $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
        a.push($(t).val())
    }), t = a.length, "" != (a = a.join()) ? swal({
        title: "Are you sure?",
        text: "You want to move this email to old?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, move it!",
        closeOnConfirm: !0
    }, function () {
        $.ajax({
            url: url + "email/mark_as_old",
            dataType: "json",
            type: "POST",
            data: {
                filenames: a
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? (total = e.count - t, e.unread_count - t > 0 ? ($(".total_mail_count").html(e.unread_count - t).show(), $(".nav .email_count").html(e.unread_count - t).show(), $(".mail-box-header .email_count").html("(" + (e.unread_count - t) + ")").show(), $("#check_click_unread").val("1")) : $(".total_mail_count").hide(), $(".icheckbox_square-green.checked").parents("tr.unread").addClass("read").removeClass("unread"), $.each($("tbody tr .checked"), function (e, t) {
                    $(t).iCheck("uncheck")
                })) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                }), grab_emails()
            }
        })
    }) : swal({
        title: "Alert",
        text: "Please select atleast one Email to mark it as Old",
        type: "warning"
    })
});
    
$(".move_to_old_one").click(function (e) {
    var t = $("#filenameVal").val();
    "" != t ? (swal({
        title: "Are you sure?",
        text: "You want to move this email to old?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, move it!",
        closeOnConfirm: !0
    }, function () {
        $.ajax({
            url: url + "email/mark_as_old",
            dataType: "json",
            type: "POST",
            data: {
                filenames: t
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? grab_emails() : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                });

                $(".nav .email_count").html(e.unread_count);
                $(".mail-box-header .email_count").html(e.unread_count);
            }
        })
    }), grab_emails()) : swal({
        title: "Alert",
        text: "Please select atleast one unread Email to mark it as Read",
        type: "warning"
    })
});

//$(window).on("beforeunload", function (e) {
//    if ($(".composeEmailDiv").is(":visible")) {
//        return e.returnValue = "The changes you made will be lost, Do you want to discard it?"
//    }
//    e = null
//});

$(document.body).on("click", ".reply_compose_email", function () {
   // $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
    var l = $("#filenameVal").val();
    t = $("#task-navigation li.active").attr("id");
    $(".compose-mail").trigger("click"), $.ajax({
        url: url + "email/reply_email",
        dataType: "json",
        type: "POST",
        data: {
            current_list: t,
            filename: l,
           
        },
        beforeSend: function () {
            start = 0, $.blockUI()
        },
        complete: function () {
            $.unblockUI()
        },
        success: function (e) {
            if(t=='sent'){
                $("#mailType").val("reply"),e.internal_party.length > 0 && $("#whoto").val(e.internal_party).trigger("change"), $("#mail_subject").val(e.emails[0].subject), "" != e.emails[0].whotoexternal && $("#emails").val(e.emails[0].whotoexternal), $("#mail_urgent").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent").iCheck("check"), $("#case_no").val(e.emails[0].caseno != '0' ? e.emails[0].caseno : "")
            }else{
                $("#mailType").val("reply"),e.emails[0].whofrom ? $("#whoto").val(e.emails[0].whofrom).trigger("change") : $("#emails").val(e.emails[0].whotoexternal), $("#mail_subject").val(e.emails[0].subject), $("#mail_urgent").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent").iCheck("check"), $("#case_no").val(e.emails[0].caseno != '0' ? e.emails[0].caseno : "")
            }
            CKEDITOR.instances.mailbody.setData(e.emails[0].body);
        }
    })
});

$(document.body).on("click", ".reply_all_compose_email", function () {
    $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
    var i = $("#filenameVal").val();
        e = $("#task-navigation li.active").attr("id");
    $(".compose-mail").trigger("click"), $.ajax({
        url: url + "email/reply_all_email",
        dataType: "json",
        type: "POST",
        data: {
            current_list: e,
            filename: i,
        },
        beforeSend: function () {
            start = 0, $.blockUI()
        },
        complete: function () {
            $.unblockUI()
        },
        success: function (e) {
            $("#mailType").val("reply"), e.internal_party.length > 0 && $("#whoto").val(e.internal_party).trigger("change"), "" != e.external_party && $("#emails").val(e.external_party), $("#mail_subject").val(e.emails[0].subject), $("#mail_urgent").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent").iCheck("check"), $("#case_no").val((e.emails[0].caseno) != '0' ? e.emails[0].caseno : '');
            CKEDITOR.instances.mailbody.setData(e.emails[0].body);
        }
    });
});

$(".discard_message").click(function (e) {
    swal({
        title: "Are you sure?",
        text: "The changes you made will be lost",
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, discard it!",
        closeOnConfirm: !0
    }, function () {
        $(".files_global_var").remove(), $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"),CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"), show_hide_main_window("email_compose"),$(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html(""),$('.fileremove').remove(),$('#totalFileSize').val('');
    });
});

$(".mark_as_read").click(function (e) {
    e.preventDefault();
    var t, a = [];
    $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
        a.push($(t).val());
        var i = $(this).find("input[type=checkbox]");
        i.prop("checked", i.prop("checked"))
    }), t = a.length, "" != (a = a.join()) ? $.ajax({
        url: url + "email/mark_as_read",
        dataType: "json",
        type: "POST",
        data: {
            filenames: a
        },
        beforeSend: function () {
            $.blockUI()
        },
        complete: function () {
            $.unblockUI()
        },
        success: function (e) {
            "true" == e.result ? (total = e.count - t, e.unread_count - t > 0 ? ($(".total_mail_count").html(e.unread_count).show(), $(".nav .email_count").html(e.unread_count).show(), $(".mail-box-header .email_count").html("(" + (e.unread_count - t) + ")").show()) : $(".total_mail_count").hide(), $(".icheckbox_square-green.checked").parents("tr.unread").addClass("read").removeClass("unread"), $.each($("tbody tr .checked"), function (e, t) {
                $(t).iCheck("uncheck")
            })) : "false" == e.result && swal({
                title: "Alert",
                text: "Oops! Some error has occured. Please try again later.",
                type: "warning"
            }),show_hide_main_window("email_listing"), grab_emails()
        }
    }) : swal({
        title: "Alert",
        text: "Please select atleast one unread Email to mark it as Read",
        type: "warning"
    })
});

$('#mailsenddisable1').on('click', function() {
    $('#mailsenddisable').trigger('click');
});

$(".move_to_trash").click(function (e) {
        e.preventDefault();
        var j = $("#task-navigation li.active").attr("id");
        var t, a = [],
            i = [],
            l = "";
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val())
        });
        var n = removeDuplicateUsingSet(a);
        t = a.length, a = a.join(), i = i.join(), l = t > 1 ? n.length + " emails" : n.length + " email", i.length, t > 0 ? swal({
            title: "Are you sure?",
            text: "You will not be able to recover " + l + " !",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function () {
            $.ajax({
                url: url + "email/move_to_trash",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: a,
                    email_con_ids: i,
                    current_list : j
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? setTimeout(function () {
                        swal({
                            title: "Success!",
                            text: "Your " + l + " has been deleted.",
                            type: "success"
                        })
                    }, 1e3) : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), e.unread_count > 0 ? ($(document).find("#side-menu .email_count").html(e.unread_count).show(), $(document).find(".dropdown .email_count").html(e.unread_count).show()) : $(" .total_mail_count").hide(), grab_emails()
                }
            })
        }) : swal({
            title: "Alert",
            text: "Please select atleast one  Email to Delete",
            type: "warning"
        })
    });

$(".move_to_trash_one").click(function (e) {
        e.preventDefault();
        var t = $(".move_to_trash_one").attr("data-id");
        var l = $("#task-navigation li.active").attr("id");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover selected email !",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }, function () {
            $.ajax({
                url: url + "email/move_to_trash",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: t,
                    current_list : l
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? (swal({
                        title: "Alert",
                        text: "Email(s) deleted successfully",
                        type: "success"
                    }), $(".changeContentDiv").hide(), $(".email_conversation").hide(), $(".placeholderEmailDiv").show()) : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    })
                }
            }), grab_emails()
        })
    });
    
function removeDuplicateUsingSet(e) {
    return Array.from(new Set(e));
}

$(".forward_email").click(function (t) {
        $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
        var i = $("#filenameVal").val();
        var j = $("#task-navigation li.active").attr("id");
        $(".compose-mail").trigger("click"), $.ajax({
            url: url + "email/forward_email",
            dataType: "json",
            type: "POST",
            data: {
                current_list: j,
                filename: i,
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                if ($("#mailType").val("forward"), $("#mail_subject").val(e.emails[0].subject), "Y" == e.emails[0].urgent ? $("#mail_urgent").parent("div").addClass("checked") : $("#mail_urgent").parent("div").removeClass("checked"), e.emails[0].caseno > 0 && $("#case_no").val(e.emails[0].caseno), "" != e.file_with_attachment) {
                    //new FormData("#send_mail_form");
                    $("#file_with_attachment_fwd").html(e.file_with_attachment), $("#fwd_with_attachment").prop("checked", !0);
                }
                CKEDITOR.instances.mailbody.setData(e.emails[0].body);
            }
        })
    });
$(document).on('click',".fwdremove",function (e) {
    var a = $(this).parent().remove();
    var imagename = $(this).data('image');
    var foldername = $(this).data('folder');
    $.ajax({
        url: "email/emailfwdremove",
        type: 'post',
        data: {'imagename': imagename , 'folder':foldername},
        success : function(data) {
            removedFileSize = data.split(':')[1];
            totalFileSize = $('#totalFileSize').val() - removedFileSize;
            console.log(totalFileSize);
            $('#totalFileSize').val(totalFileSize);
        },
        error : function(request) { }
    });
});
$(document).on("change", "#selectall", function (e) {
    var checkbocchk = $(this).is(":checked");
    var allMainCheckBox = $("input.email-check-id");
    if (checkbocchk == true) {
        $('#DataTables_Table_0 tbody tr').addClass('email-table-selected');
        allMainCheckBox.prop('checked', true);
        $('.childThread').prop('checked', true);
        $.each(allMainCheckBox, function (index, eachMainCheckBox) {
            var objEachMainCheckBox = $(eachMainCheckBox);
            if (objEachMainCheckBox.data('ethread') > 0) {
                getAllThreadEmail(objEachMainCheckBox.parent().parent().parent());
            }
        });
    } else {
        $('#DataTables_Table_0 tbody tr').removeClass('email-table-selected');
        allMainCheckBox.prop('checked', false);
        grab_emails();
    }


});


$(document).on("click", ".refresh-mail", function () {
    grab_emails();
});

$(".mark_as_imp").click(function (e) {
    e.preventDefault();
    var t, a = [];
    $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
        a.push($(t).val())
    }), t = a.length, a = a.join(), t > 0 ? $.ajax({
        url: url + "email/mark_as_urgent",
        dataType: "json",
        type: "POST",
        data: {
            filenames: a
        },
        beforeSend: function () {
            $.blockUI()
        },
        complete: function () {
            $.unblockUI()
        },
        success: function (e) {
            "true" == e.result ? ($.each($(".icheckbox_square-green.checked").parents("tr").find("td.mail-ontact"), function (e, t) {
                0 == $(t).find("i.fa.fa-circle.text-danger").length && $(t).append(' <i class="fa fa-circle text-danger"></i>')
            }), $.each($("tbody tr .checked"), function (e, t) {
                $(t).iCheck("uncheck")
            })) : "false" == e.result && swal({
                title: "Alert",
                text: "Oops! Some error has occured. Please try again later.",
                type: "warning"
            }), grab_emails()
        }
    }) : swal({
        title: "Alert",
        text: "Please select atleast one email to mark it as Important",
        type: "warning"
    })
});

$(document.body).on("click", ".print_email", function () {
    var t = $(".print_email").attr("data-id"),e = $("#task-navigation li.active").attr("id");
    
    $.ajax({
        url: url + "email/printEmail",
        method: "POST",
        data: {
            current_list: e,
            filename: t,
        },
        success: function (e) {
            $(e).printThis({
                debug: !1,
                header: "<h2 style=' padding-top:20px; '><center>View Message</center></h2>"
            })
        }
    })
});


$(".move_to_new_one").click(function (e) {
    e.preventDefault();
    var t = $(".move_to_new_one").attr("data-id");
    var l = $("#task-navigation li.active").attr("id");
    $.ajax({
            url: url + "email/move_to_new",
            dataType: "json",
            type: "POST",
            data: {
                filenames: t,
                current_list : l
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? (swal({
                    title: "Alert",
                    text: "Email(s) moved successfully",
                    type: "success"
                }), $(".changeContentDiv").hide(), $(".email_conversation").hide(), $(".placeholderEmailDiv").show()) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                })
            }
        }), grab_emails()
    
});

$(".move_to_new").click(function (e) {
    e.preventDefault();
    var j = $("#task-navigation li.active").attr("id");
    var t, a = [],
        i = [],
        l = "";
    $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
        a.push($(t).val())
    });
    var n = removeDuplicateUsingSet(a);
    t = a.length, a = a.join(), i = i.join(), l = t > 1 ? n.length + " emails" : n.length + " email", i.length, t > 0 ? 
        $.ajax({
            url: url + "email/move_to_new",
            dataType: "json",
            type: "POST",
            data: {
                filenames: a,
                email_con_ids: i,
                current_list : j
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? setTimeout(function () {
                    swal({
                        title: "Success!",
                        text: "Your " + l + " has been moved.",
                        type: "success"
                    })
                }, 1e3) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                }), e.unread_count > 0 ? ($(document).find("#side-menu .email_count").html(e.unread_count).show(), $(document).find(".dropdown .email_count").html(e.unread_count).show()) : $(" .total_mail_count").hide(), grab_emails()
            }
        })
    : swal({
        title: "Alert",
        text: "Please select atleast one  Email to move",
        type: "warning"
    })
});

