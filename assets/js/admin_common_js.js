function savetoadmin(t) {
    $.ajax({
        url: HTTP_PATH + "admin/updatefirm",
        method: "POST",
        data: $("#" + t).serialize(),
        beforeSend: function (t) {
            $.blockUI()
        },
        complete: function (t, e) {
            $.unblockUI()
        },
        success: function (t) {
            var e = $.parseJSON(t);
            "1" == e.status && swal("Success !", e.message, "success", function () {
                location.reload()
            })
        }
    })
}

function addstaff(t) {
    $.ajax({
        url: HTTP_PATH + "admin/addstaff",
        method: "POST",
        data: $("#" + t).serialize(),
        beforeSend: function (t) {
            $.blockUI()
        },
        complete: function (t, e) {
            $.unblockUI()
        },
        success: function (t) {
            var e = $.parseJSON(t);
            if(e.status == "2") {
                swal({
                    title: "Warning!",
                    text: e.message,
                    type: "warning"
                });
            } else if(e.status == "1") {
                swal({
                    title: "Success!",
                    text: e.message,
                    type: "success"
                }, function () {
                    $("#add").modal("hide"), location.reload();
                });
            } else if(e.status == "3") {
                swal({
                    title: "Warning!",
                    text: e.message,
                    type: "warning"
                });
            }
        }
    });
}

function editstaff(t) {
    $.ajax({
        url: HTTP_PATH + "admin/editstaff",
        method: "POST",
        data: $("#" + t).serialize(),
        beforeSend: function (t) {
            $.blockUI()
        },
        complete: function (t, e) {
            $.unblockUI()
        },
        success: function (t) {
            var e = $.parseJSON(t);
            if(e.status == "2") {
                swal({
                    title: "Warning!",
                    text: e.message,
                    type: "warning"
                });
            } else if(e.status == "1") {
                swal({
                    title: "Success!",
                    text: e.message,
                    type: "success"
                }, function () {
                    $("#edit").modal("hide"), location.reload();
                });
            } else if(e.status == "3") {
                swal({
                    title: "Warning!",
                    text: e.message,
                    type: "warning"
                });
            }
        }
    })
}
document.getElementById("bug_desciptions") && CKEDITOR.replace("bug_desciptions"), $(document).ready(function () {
    $("#bug_report_list").DataTable()
}), $(document.body).on("click", "#bug_view", function () {
    var t = $(this).attr("data-id");
    $.ajax({
        type: "POST",
        url: HTTP_PATH + "admin/view_bug_report",
        data: {
            bug_id: t
        },
        success: function (t) {
            return 1 == (t = $.parseJSON(t)).status ? ($("#bug_modal_body").html(""), $("#bug_firm_name").html(""), $("#bug_firm_name").html("Bug Reported By " + t.repoter), $("#bug_modal_body").html(t.data), $("#bug_modal").modal("show"), !1) : (swal({
                title: "Alert",
                text: "No Data Found.",
                type: "warning"
            }), !1)
        }
    })
});