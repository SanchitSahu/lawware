function notifyBrowser(e, o, n, i, t) {
    $("#baseUrl").val();
    if (o = e > 1 ? e + " New Reminders Added" : e + " New Reminder Added", Notification) {
        var r = new Notification(o, {
            body: t + "\r\n" + n,
            icon: 'https://compcaseonline.com/assets/img/favicon.png'
        });
        r.onclick = function() {
            r.close(), window.open(i)
        }, r.onclose = function() {
            console.log("Notification closed")
        }
    } else console.log("Desktop notifications not available in your browser..")
}
function notifyUrgentEmailBrowser(e, o, n, i, t) {
    $("#urgentEmailBaseUrl").val();
    if (e + " New Urgent Email Received", Notification) {
        var r = new Notification(o, {
            body: t + "\r\n" + n,
            icon: 'https://compcaseonline.com/assets/img/favicon.png'
        });
        r.onclick = function() {
            r.close(), window.open(i)
        }, r.onclose = function() {
            console.log("Notification closed")
        }
    } else console.log("Desktop notifications not available in your browser..")
}
setInterval(function() {
    var e = $("#baseUrl").val(),
        o = e,
        n = "";
    $.ajax({
        url: e + "/remindernotify",
        method: "GET",
        success: function(e) {
            var i, t = $.parseJSON(e),
                r = t.length;
            for (i = 0; i < r; i++) n += t[i].event + "\r\n";
            if (console.log("event =>" + n), !(r > 0)) return !1;
            notifyBrowser(r, "New Reminder Added", "Click to open", o, n)
        }
    })
}, 9e4), document.addEventListener("DOMContentLoaded", function() {
    "granted" !== Notification.permission && Notification.requestPermission()
});