$(document).ready(function () {

    /*Search the form on enter key event*/
    $(document).keypress(function (e) {
        if ($("#CASE_REPORT").hasClass('in') && (e.keycode == 13 || e.which == 13)) {
            $('#CaseRepotProceed').trigger('click');
        }
        if ($("#referral_report").hasClass('in') && (e.keycode == 13 || e.which == 13)) {
            $('#getReferralReport').trigger('click');
        }
        if ($("#case_activity_report").hasClass('in') && (e.keycode == 13 || e.which == 13)) {
            $('#getCaseActivityReport').trigger('click');
        }
        if ($("#case_injury_report").hasClass('in') && (e.keycode == 13 || e.which == 13)) {
            $('#getInjuryReport').trigger('click');
        }
        if ($("#task_report").hasClass('in') && (e.keycode == 13 || e.which == 13)) {
            $('#TaskRepotProceed').trigger('click');
        }
    });


    caseCountTable = $('#caseCountReport_dataTable').DataTable();
    caseReportTable = $('#caseReport_dataTable').DataTable();
    referralReportTable = $('#caseReferralReport_table').DataTable();
    taskTable = $('#tastReport_table').DataTable();
    caseInjuryTable = $('#caseInjurylReport_table').DataTable();
    markupTable = $('#markupReport_dataTable').DataTable();
    caseactivityTable = $('#caseActivityReport_table').DataTable();
    taskpoolTable = $('#taskpoolReport_table').DataTable();

    $('.CASE_REPORT').click(function () {
        $('#caseReportForm')[0].reset();
        $('.select2Class').trigger('change');
    });
    $('.referral_report').click(function () {
        $('#caseReferralForm')[0].reset();
        $('.select2Class').trigger('change');
    });
    $('.case_activity_report').click(function () {
        $('#caseActivityForm')[0].reset();
        $('.select2Class').trigger('change');
    });

    $('.case_injury_report').click(function () {
        $('#caseInjuryForm')[0].reset();
        $('.select2Class').trigger('change');
    });


    $('.contact').mask("(000) 000-0000");
    $('.securityno').mask("000-00-0000");
    $('.doi_my').mask("00/0000");
    /*change the value of search box according to search by*/
    $(document.body).on('change', '#searchBy', function () {
            $('#searchText').val('');
        var searchText = $('#searchBy').val().split('.');
        var message = '';
        var placeholder = '';

        if (searchText[1] === 'contact') {
            $("#searchText").addClass("contact");
        } else if (searchText[1] === "social_sec") {
            $("#searchText").addClass("securityno");
        } else {
            $("#searchText").val('');
            $("#searchText").unmask();
            $("#searchText").removeAttr("maxlength");
            $("#searchText").removeClass("contact");
        }
        $('#searchText').datetimepicker('remove');
        switch (searchText[1]) {
            case 'first':
                message = 'Enter all the Part of the First Name';
                placeholder = 'Search By ie. XYZ';
                break;
            case 'last':
                message = 'Enter all the Part of the Last Name';
                placeholder = 'Search By ie. XYZ';
                break;
            case 'firm':
                message = 'Enter all the Part of the Firm Name';
                placeholder = 'Search By ie. XYZ';
                break;
            case 'caseno':
                message = 'Enter One Case Number';
                placeholder = 'Search By ie. 123';
                break;
            case 'caseno_less':
                message = 'Enter One Case Number';
                placeholder = 'Search By ie. 123';
                break;
            case 'caseno_more':
                message = 'Enter One Case Number';
                placeholder = 'Search By ie. 123';
                break;
            case 'yourfileno':
                message = 'Enter all or part of your own internal office file Number';
                placeholder = 'Search By ie. 123';
                break;
            case 'social_sec':
                message = 'Enter all or part of the Social Security Number';
                placeholder = 'Search By ie. 234-56-5898';
                break;
            case 'followup':
                message = 'Display case as per Follow up Date';
                placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                $('#searchText').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                break;
            case 'contact':
                message = "Enter all or part of the phone number to appear anywhere within the card or firm's for telephone number";
                placeholder = 'Search By ie. (111)111-1111';
                break;
            case 'birth_date':
                message = 'Enter the date of birth';
                placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                $('#searchText').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                break;
            case 'birth_month':
                message = 'Enter the number for a month ie: 1=Jan, 2=Feb, 12=Dec, 0=All';
                placeholder = 'search by numbers in 0-12 Range';
                break;
            case 'dateenter':
                message = 'Display case as per Entered Date';
                placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                $('#searchText').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                break;
            case 'dateopen':
                message = 'Display case as per Opened Date';
                placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                $('#searchText').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                break;
            case 'dateclosed':
                message = 'Display case as per Closed Date';
                placeholder = 'Search By MM/DD/YYYY ie. 05/12/1947';
                $('#searchText').datetimepicker({
                    format: 'mm/dd/yyyy',
                    minView: 2,
                    autoclose: true,
                    endDate: new Date()
                });
                break;
        }

        $('#searchText').attr("title", message);
        $('#searchText').attr("placeholder", placeholder);
    });

    $('#searchText').on('blur', function () {
        dtValue = $(this).val();
        if (dtValue) {
            if ($('#searchBy').val() == 'cd.birth_date' || $('#searchBy').val() == 'c.dateenter' || $('#searchBy').val() == 'c.dateopen' || $('#searchBy').val() == 'c.dateclosed') {
                var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
                if (!dtRegex.test(dtValue)) {
                    $(this).parent().append('<span style="color:red;">Please enter a valid date</span>');
                    setTimeout(function () {
                        $('#searchText').parent().find('span').remove();
                    }, 5000);
                    return false;
                }
            } else if ($('#searchBy').val() == 'cd.birth_month') {
                var dtRegex = new RegExp(/^[0-9]{0,2}$/);
                if (!dtRegex.test(dtValue)) {
                    $(this).parent().append('<span style="color:red;">Please enter a valid month</span>');
                    setTimeout(function () {
                        $('#searchText').parent().find('span').remove();
                    }, 5000);
                    return false;
                }
            } else if ($('#searchBy').val() == 'cd.social_sec') {
                var dtRegex = new RegExp(/^\d{3}-\d{2}-\d{4}$/);
                if (!dtRegex.test(dtValue)) {
                    $('#searchText').parent().find('span').remove();
                    $(this).parent().append('<span style="color:red;">Please enter a valid input</span>');
                    setTimeout(function () {
                        $('#searchText').parent().find('span').remove();
                    }, 5000);
                    return false;
                }
            } else if ($('#searchBy').val() != 'cd.contact' && $('#searchBy').val() != 'c.yourfileno' && $('#searchBy').val() != 'cd2.firm') {
                var dtRegex = new RegExp(/^\s*[a-zA-Z0-9,\s]+\s*$/);
                if (!dtRegex.test(dtValue)) {
                    $('#searchText').parent().find('span').remove();
                    $(this).parent().append('<span style="color:red;">Please enter a valid input</span>');
                    setTimeout(function () {
                        $('#searchText').parent().find('span').remove();
                    }, 5000);
                    return false;
                }
            }
        }
    });

    $('#date_of_injury').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
        endDate: new Date()
    });


    $('#start_dr_date').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
        endDate: new Date()
    }).on('changeDate', function (ev) {
        $('#end_dr_date').datetimepicker('setStartDate', ev.date);
    });

    $('#end_dr_date').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
        endDate: new Date()
    }).on('changeDate', function (ev) {
        $('#start_dr_date').datetimepicker('setEndDate', ev.date);
    });


    $('#start_ca_date').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
        endDate: new Date()
    }).on('changeDate', function (ev) {
        $('#end_ca_date').datetimepicker('setStartDate', ev.date);
    });

    $('#end_ca_date').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
        endDate: new Date()
    }).on('changeDate', function (ev) {
        $('#start_ca_date').datetimepicker('setEndDate', ev.date);
    });
});

/* Script for Case Count Report */
$(document.body).on('change', '#isType', function () {
    var value = this.value;
    if (this.value == 'byMonth') {
        $('#casecount-isType').html('By Atty/All Months');
        $('#casecount-year').hide();
        $('#casecount-types').hide();

    } else {
        $('#casecount-isType').html('By Atty/All Months');
        $('#casecount-year').show();
        $('#casecount-types').show();
    }
});


$(document.body).on('click', '#caseCountProceed', function () {
    console.log($('#caseCountReportForm').serialize());
});

$(document.body).on('click', '#caseCountProceed', function () {

    var CaseCountReportUrl = $("#CaseCountReportUrl").val();
    var casecountFormarray = $('#caseCountReportForm').serializeArray();
    caseCountTable.destroy();
    caseCountTable = $('#caseCountReport_dataTable').DataTable({
        buttons: [{
            extend: 'print',
            title: 'Case Count Report Results',
            autoPrint: 'false',
            footer: 'true',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 7, 9]
            }
        }],
        lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
        processing: true,
        serverSide: true,
		stateSave: true,
		stateSaveParams: function (settings, data) {
			data.search.search = "";
			data.start = 0;
			delete data.order;
			data.order = [4, "ASC"];
		},
        bFilter: true,
        scrollX: true,
        sScrollY: 400,
        bScrollCollapse: true,
        destroy: true,
        order: [[4, "asc"]],
        autoWidth: true,
        ajax: {
            url: CaseCountReportUrl,
            type: "POST",
            "data": function (ae) {
                ae.searchData = casecountFormarray;
            }
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr("id", aData[0]);
            return nRow;
        },
        columnDefs: [{
            targets: 3,
            className: 'row-width',
            width: 220
        }, {
            targets: 13,
            className: 'row-width',
            width: 220
        }],
        select: {
            style: 'single'
        }
    });
    $('#case_count').modal('hide');

});


var caseformsearchdata = {};
var casereposelectedrow = [];
var CaseRepoSelectedrowId = "";

$(document).ready(function () {
    var APPLICATION_NAME = $('#APPLICATION_NAME').val();
            
    $.casereportTable = function () {
        $("#Caserow-name").html("");
        $("#Caserow-firnmame").html("");
        $("#Caserow-cardtype").html("");
        $("#Caserow-phone").html("");
        $("#Caserow-ssno").html("");
        $("#Caserow-casetype").html("");
        $("#Caserow-casestatus").html("");
        $("#Caserow-followup").html("");
        $("#Caserow-dateentered").html("");
        $("#Caserow-fileno").html("");
        $("#Caserow-staff").html("");
        $("#row-injurydates").html("");
        var CaseReportUrl = $("#CaseReportUrl").val();

        caseReportTable = $('#caseReport_dataTable').DataTable({
            dom: 'l<"html5buttons"B>tr<"col-sm-5"i><"col-sm-7"p>',
            buttons: [
                {
                    extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Excel', title: "'"+APPLICATION_NAME+" : Case Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
                    }
                },
                {
                    extend: 'pdfHtml5', text: '<i class="fa fa-file-pdf-o"></i> PDF', title: "'"+APPLICATION_NAME+" : Case Report'", header: true,
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
                    },
                    customize: function (doc) {
                        doc.styles.table = {
                            width: '920px'
                        };
                        doc.styles.tableHeader.alignment = 'left';
                        doc['header'] = (function (page, pages) {
                            return {
                                columns: [
                                    APPLICATION_NAME,
                                    {
                                        alignment: 'left',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: [5, 0]
                            };
                        });
                    }
                },
                {
                    extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print', title: "'"+APPLICATION_NAME+ ": Case Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                        columnDefs: [
                            { width: "10", targets: 15 }
                        ]
                    },
                    orientation: 'landscape',
                    customize: function (win) {
                        $(win.document.body).find('td').css('white-space', 'normal');
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],
            lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            processing: true,
            serverSide: true,
            bFilter: false,
            scrollX: true,
			stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            sScrollY: 400,
            bScrollCollapse: true,
            destroy: true,
            autoWidth: true,
            order: [[0, "DESC"]],
            ajax: {
                url: CaseReportUrl,
                type: "POST",
                data: function (ae) {
                    ae.searchData = caseformsearchdata;
                }
            },
            fnRowCallback: function (nRow, data) {
                $(nRow).attr("id", '' + data[0]);
                if (data[19] == "Deleted") {
                    $('td', nRow).css('background-color', '#ff9999');
                }
            },
            fnDrawCallback: function (oSettings) {
                var rows = caseReportTable.rows(0).data();

                if ("0" in rows) {
                    $('#case_report_list tbody tr:eq(0)').toggleClass("selected");
                    $("#Caserow-name").html(rows[0][1]);
                    $("#Caserow-firnmame").html(rows[0][15]);
                    $("#Caserow-cardtype").html(rows[0][13]);
                    $("#Caserow-phone").html(rows[0][6]);
                    $("#Caserow-ssno").html(rows[0][12]);
                    $("#Caserow-casetype").html(rows[0][4]);
                    $("#Caserow-casestatus").html(rows[0][5]);
                    $("#Caserow-followup").html(rows[0][8]);
                    $("#Caserow-dateentered").html(rows[0][9]);
                    $("#Caserow-fileno").html(rows[0][7]);
                    $("#Caserow-staff").html(rows[0][2] + " " + rows[0][3] + " " + rows[0][10] + " " + rows[0][11]);
                    $("#row-injurydates").html(rows[0][20]);
                }
            },
            initComplete: function (settings, json) {
                var selectedrows = caseReportTable.rows('.selected').data();
                if ("0" in selectedrows) {
                    CaseRepoSelectedrowId = selectedrows[0][0];
                }

                if ($("#Ctable_unique").val() === "SearchUniqueTrue") {
                    swal({
                        title: "Done Shorting by Name (unique)",
                        type: "success",
                        showCancelButton: false,
                        customClass: "sweet-alert-success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok"
                    });
                } else if ($("#Ctable_unique").val() === "SearchUniqueFalse") {
                    swal({
                        title: "Done Shorting by Name",
                        type: "success",
                        showCancelButton: false,
                        customClass: "sweet-alert-success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok"
                    });
                }
                $("#Ctable_unique").val('');
            },
            /*columnDefs: [{targets: 17, className: "textwrap-line"}],*/
            select: {
                style: 'single'
            }
        });
    }
});
/*SCRIPT FOR CASE REPORT*/
$(document.body).on('click', '#CaseRepotProceed', function () {
    caseformsearchdata = "";
    caseformsearchdata = $('#caseReportForm').serializeArray();
    var serialized = $('#caseReportForm').serializeArray();
    var s = '';
    var formjsonObject = {};
    for (s in serialized) {
        formjsonObject[serialized[s]['name']] = serialized[s]['value'];
    }
    if (formjsonObject.hasOwnProperty('close_window')) {
        $("#CASE_REPORT").modal('hide');
    }

    if (formjsonObject.casetype == "" && formjsonObject.case_status == "" && formjsonObject.card_type == "" && formjsonObject.atty_resp == "" && formjsonObject.atty_hand == "" && formjsonObject.para_hand == "" && formjsonObject.sec_hand == "" && formjsonObject.searchBy == "c.caseno") {
        if (formjsonObject.searchText == "") {
            swal({
                title: "Please Enter Case No",
                type: "warning",
                showCancelButton: false,
                customClass: "sweet-alert-warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok"
            });
        } else {
            window.open($(this).data('url') + formjsonObject.searchText, '_blank');
        }
    } else {
        $("#caseReport_dataTable  tbody").remove();
        caseReportTable.destroy();
        $('#case_report_list').modal('show');
        $.casereportTable();
    }

});

$('#resetCaseReportFilter').click(function () {
    caseReportTable.search('').draw();
    $('#caseReportForm')[0].reset();
    $('.select2Class').trigger('change');
    return false;
});

$('#Ctable_AutoPull').click(function () {
    var data = caseReportTable.rows().data();
    if (data.length === 1) {
        window.open($("#OpenCase").data('url') + CaseRepoSelectedrowId, '_blank');
    }
});

$(document.body).on('click', '#printCaseReport', function () {
    caseReportTable.buttons('.buttons-print').trigger();
});

$(document.body).on('click', '#Ctable_unique', function () {
    var uniquedata = { name: "Ctable_unique", value: "on" };
    if ($(this).prop("checked") == true) {
        $("#Ctable_unique").val('SearchUniqueTrue');
        caseformsearchdata.push(uniquedata);
    } else if ($(this).prop("checked") == false) {
        $("#Ctable_unique").val('SearchUniqueFalse');
        caseformsearchdata.pop(uniquedata);
    }
    $.casereportTable();
});

$(document.body).on('click', '.closetable', function () {
    CaseRepoSelectedrowId = "";
    $("#Ctable_unique").prop("checked", false);
    $("#OpenCase").prop("checked", false);
    $("#Ctable_AutoPull").prop("checked", false);
    $("#Ctable_altFormat").prop("checked", false);
});

$(document.body).on('click', '#case_report_list tbody tr', function () {
    var id = this.id;
    CaseRepoSelectedrowId = "";
    var index = $.inArray(id, casereposelectedrow);

    if (index === -1) {
        casereposelectedrow.push(id);
    } else {
        casereposelectedrow.splice(index, 1);
    }
    CaseRepoSelectedrowId = id;

    $("#case_report_list tbody tr").removeClass('selected');
    $(this).toggleClass('selected');

    /*get selected row data */

    var selectedrows = caseReportTable.rows('.selected').data();

    if ("0" in selectedrows) {
        $("#Caserow-name").html(selectedrows[0][1]);
        $("#Caserow-firnmame").html(selectedrows[0][15]);
        $("#Caserow-cardtype").html(selectedrows[0][13]);
        $("#Caserow-phone").html(selectedrows[0][6]);
        $("#Caserow-ssno").html(selectedrows[0][12]);
        $("#Caserow-casetype").html(selectedrows[0][4]);
        $("#Caserow-casestatus").html(selectedrows[0][5]);
        $("#Caserow-followup").html(selectedrows[0][8]);
        $("#Caserow-dateentered").html(selectedrows[0][9]);
        $("#Caserow-fileno").html(selectedrows[0][7]);
        $("#Caserow-staff").html(selectedrows[0][2] + " " + selectedrows[0][3] + " " + selectedrows[0][10] + " " + selectedrows[0][11]);
        $("#row-injurydates").html(selectedrows[0][20]);
    }
    if ($("#Ctable_AutoPull").prop("checked") == true) {
        window.open($("#OpenCase").data('url') + CaseRepoSelectedrowId, '_blank');
    }
    return false;

});

$(document.body).on('click', '#OpenCase', function () {
    if (CaseRepoSelectedrowId === "") {
        swal({
            title: "Please Select row",
            type: "warning",
            showCancelButton: false,
            customClass: "sweet-alert-warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok"
        });
    } else {
        window.open($(this).data('url') + CaseRepoSelectedrowId, '_blank');
    }
});

$(document.body).on('click', '#Ctable_altFormat', function () {
    if ($(this).prop("checked") == true) {
        $("#CaseDetails").hide();
        $("#CaseRepTable").removeClass('col-md-8');
        $("#CaseRepTable").addClass('col-md-12');
    } else if ($(this).prop("checked") == false) {
        $("#CaseDetails").show();
        $("#CaseRepTable").removeClass('col-md-12');
        $("#CaseRepTable").addClass('col-md-8');
    }
});
/* Case report [end]*/


/*SCRIPT REFERRAL REPORT*/
$(document.body).on('click', '#getReferralReport', function () {
    $("#caseReferralReport_table  tbody").remove();
    referralReportTable.destroy();
    var ReferralreportUrl = $("#ReferralreportUrl").val();
    var referralformarray = $('#caseReferralForm').serializeArray();
    referralReportTable = $('#caseReferralReport_table').DataTable({
        dom: 'l<"html5buttons"B>tr<"col-sm-5"i><"col-sm-7"p>',
        buttons: [
            { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Excel', title: "'"+APPLICATION_NAME+" : Referral Report'" },
            {
                extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> PDF', title: "'"+APPLICATION_NAME+" : Referral Report'", header: true,
                customize: function (doc) {
                    doc.styles.table = {
                        width: '100%'
                    };
                    doc.styles.tableHeader.alignment = 'left';
                    doc['header'] = (function (page, pages) {
                        return {
                            columns: [
                                APPLICATION_NAME,
                                {
                                    alignment: 'right',
                                    text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }
                            ],
                            margin: [10, 10]
                        };
                    });
                    doc.content[1].table.widths = '*';
                }
            },
            {
                extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print', title: "'"+APPLICATION_NAME+"  : Referral Report'",
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
        processing: true,
        serverSide: true,
		stateSave: true,
		stateSaveParams: function (settings, data) {
			data.search.search = "";
			data.start = 0;
			delete data.order;
			data.order = [0, "desc"];
		},
        bFilter: false,
        scrollX: true,
        sScrollY: 400,
        bScrollCollapse: true,
        destroy: true,
        autoWidth: true,
        order: [[0, "DESC"]],
        ajax: {
            url: ReferralreportUrl,
            type: "POST",
            data: function (ae) {
                ae.searchData = referralformarray;
            }
        },
        columnDefs: [{
            targets: 6,
            className: 'row-width',
            width: 200
        }],
        select: {
            style: 'single'
        }
    });
    $('#case_referral_list_modal').modal('show');
});
$('#resetReferralReportFilter').click(function () {
    referralReportTable.search('').draw();
    $('#caseReferralForm')[0].reset();
    $('.select2Class').trigger('change');
    return false;
});
$(document.body).on('click', '#printReferrall', function () {
    referralReportTable.buttons('.buttons-print').trigger();
});
$(document).ready(function () {
    /*Validation case injury report*/
    $('body').on('blur', '.doi_my', function () {
        dateString = $(this).val();
        var dtRegex = new RegExp("(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");
        if (!dtRegex.test(dateString)) {
            $(this).parent().append('<span style="color:red;">Please enter a valid date</span>');
            setTimeout(function () {
                $('.doi_my').parent().find('span').remove();
            }, 4000);
        }
    });
});
/* Case Injury report */
$(document.body).on('click', '#getInjuryReport', function () {
    $("#caseInjurylReport_table  tbody").remove();
    caseInjuryTable.destroy();
    var InjuryreportUrl = $("#InjuryreportUrl").val();
    var APPLICATION_NAME = ($('#APPLICATION_NAME').val());
    var caseInjuryFormarray = $('#caseInjuryForm').serializeArray();
    caseInjuryTable = $('#caseInjurylReport_table').DataTable({
        dom: 'l<"html5buttons"B>tr<"col-sm-5"i><"col-sm-7"p>',
        buttons: [
            { extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Excel', title: "'"+APPLICATION_NAME+" : Case Injury Report'" },
            {
                extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i> PDF', title: "'"+APPLICATION_NAME+" : Case Injury Report'", header: true,
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: ':visible'
                },
                customize: function (doc) {
                    doc.styles.table = {
                        width: '920px'
                    };
                    doc.styles.tableHeader.alignment = 'left';
                    doc['header'] = (function (page, pages) {
                        return {
                            columns: [
                                APPLICATION_NAME,
                                {
                                    alignment: 'left',
                                    text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }
                            ],
                            margin: [5, 0]
                        }
                    });
                }
            },
            {
                extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print', title: "'"+APPLICATION_NAME+" : Case Injury Report'",
                exportOptions: {
                    columns: ':visible',
                    columnDefs: [
                        { width: 5, targets: 9 },
                        { width: 5, targets: 10 }
                    ],
                    orientation: 'landscape'
                },
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10pt');
                    $(win.document.body).find('th.carrier').css('width', '5px !important');
                    $(win.document.body).find('td.carrier').css('width', '5px !important');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
        processing: true,
        serverSide: true,
        bFilter: false,
        scrollX: true,
		stateSave: true,
		stateSaveParams: function (settings, data) {
			data.search.search = "";
			data.start = 0;
			delete data.order;
			data.order = [0, "desc"];
		},
        sScrollY: 400,
        bScrollCollapse: true,
        destroy: true,
        autoWidth: true,
        order: [[0, "DESC"]],
        ajax: {
            url: InjuryreportUrl,
            type: "POST",
            data: function (ae) {
                ae.searchData = caseInjuryFormarray;
            }
        },
        columnDefs: [{ width: "50", targets: 9, className: 'textwrap-line' }
        ],
        select: {
            style: 'single'
        }
    });
    $('#case_injury_list_modal').modal('show');
});
$('#resetCaseInjuryReportFilter').click(function () {
    caseInjuryTable.search('').draw();
    $('#caseInjuryForm')[0].reset();
    $('.select2Class').trigger('change');
    return false;
});
$(document.body).on('click', '#printInury', function () {
    caseInjuryTable.buttons('.buttons-print').trigger();
});
var selected = [];
var CaseActSelectedrowId = "";
var CaseActSelectedrowdoclinks = "";
$(document.body).on('click', '.closetable', function () {
    CaseActSelectedrowId = "";
    CaseActSelectedrowdoclinks = "";
});

$(document).ready(function () {
var APPLICATION_NAME = $('#APPLICATION_NAME').val();
    $.caseactivitytable = function () {
        $("#caseActivityReport_table  tbody").remove();
        caseactivityTable.destroy();
        selected = [];
        var caseActivityUrl = $("#caseActivityUrl").val();
        var caseActivityFormarray = $('#caseActivityForm').serializeArray();
        caseactivityTable = $('#caseActivityReport_table').DataTable({
            dom: 'l<"html5buttons"B>tr<"col-sm-5"i><"col-sm-7"p>',
            buttons: [
                {
                    extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Excel', title: "'"+APPLICATION_NAME+" : Case Activity Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7]
                    }
                },
                {
                    extend: 'pdfHtml5', text: '<i class="fa fa-file-pdf-o"></i> PDF', title: "'"+APPLICATION_NAME+" : Case Activity Report'", header: true,
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7],
                    },
                    customize: function (doc) {
                        doc.styles.table = {
                            width: '920px'
                        };
                        doc.styles.tableHeader.alignment = 'left';
                        doc['header'] = (function (page, pages) {
                            return {
                                columns: [
                                    APPLICATION_NAME,
                                    {
                                        alignment: 'left',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: [5, 0]
                            };
                        });
                    }
                },
                {
                    extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print', title: "'"+APPLICATION_NAME+" : Case Activity Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7],
                        columnDefs: [
                            { width: "50", targets: 7, className: 'textwrap-line' }
                        ]
                    },
                    orientation: 'landscape',
                    customize: function (win) {
                        $(win.document.body).find('td').css('white-space', 'normal');
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],
            lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            processing: true,
            serverSide: true,
            bFilter: false,
			stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            scrollX: true,
            sScrollY: 400,
            bScrollCollapse: true,
            destroy: true,
            autoWidth: true,
            order: [[0, "DESC"]],
            ajax: {
                url: caseActivityUrl,
                type: "POST",
                data: function (ae) {
                    ae.searchData = caseActivityFormarray;
                }
            },
            fnRowCallback: function (nRow, aData) {
                $(nRow).attr("id", '' + aData[0]);
                $(nRow).attr("data-doclinks", '' + aData[8]);
                return nRow;
            },
            columnDefs: [{ width: "50", targets: 7, className: 'textwrap-line' }],
            select: {
                style: 'single'
            }
        });
        $('#caseActivityReportData').modal('show');
    }
});

$(document.body).on('click', '#getCaseActivityReport', function () {
    /*var documents = $("#documents").val();
    if (documents != "") {
        $("#Showdocument-modal").modal('show');
        $(document.body).on('click', '#ConfirmSearchdocument', function () {
            $("#Showdocument-modal").modal('hide');
            $.caseactivitytable();
        });
    } else {*/
    $.caseactivitytable();
    /*}*/


});
$(document.body).on('click', '#caseActivityReport_table tbody tr', function () {
    var id = this.id;
    CaseActSelectedrowId = "";
    CaseActSelectedrowdoclinks = "";
    CaseActSelectedrowdoclinks = $(this).data('doclinks');
    var index = $.inArray(id, selected);
    if (index === -1) {
        selected.push(id);
    } else {
        selected.splice(index, 1);
    }
    CaseActSelectedrowId = id;
    $("#caseActivityReport_table tbody tr").removeClass('selected');
    $(this).toggleClass('selected');
});
$('#resetcaseactivityForm').click(function () {
    caseactivityTable.search('').draw();
    $('#caseActivityForm')[0].reset();
    $('.select2Class').trigger('change');
    return false;
});

$(document.body).on('click', '#printCaseActivityReport', function () {
    caseactivityTable.buttons('.buttons-print').trigger();
});
$(document.body).on('click', '#CaseActPullCase', function () {
    if (CaseActSelectedrowId === "") {
        swal({
            title: "Please Select row",
            type: "warning",
            showCancelButton: false,
            customClass: "sweet-alert-warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok"
        });
    } else {
        window.open($(this).data('url') + CaseActSelectedrowId, '_blank');
    }
});


/*$(document.body).on('click', '#CaseActViewForm', function () {
 if (CaseActSelectedrowId === "") {
 swal({
 title: "Please Select row",
 type: "warning",
 showCancelButton: false,
 customClass: "sweet-alert-warning",
 confirmButtonColor: "#DD6B55",
 confirmButtonText: "Ok"
 });
 } else {
 if (CaseActSelectedrowdoclinks === "") {
 swal({
 title: "No document found for this Case Activity",
 type: "warning",
 showCancelButton: false,
 customClass: "sweet-alert-warning",
 confirmButtonColor: "#DD6B55",
 confirmButtonText: "Ok"
 });
 } else {
 var caseactdoclinksarray = CaseActSelectedrowdoclinks.split(',');
 for (let link of caseactdoclinksarray) {
 window.open(link, '_blank');
 }
 }
 }
 });*/
$(document.body).on('click', '.spandetail', function () {
    $("#Event-Containt-modal").modal('show');
    $("#Event-Containt").html($(this).data('description'));
});


/*SCRIPT FOR TASK REPORT START*/
$(document).ready(function () {
    var APPLICATION_NAME = ($('#APPLICATION_NAME').val());
    var TaskSelectedrowId = "";
    $.taskTableSearch = function () {
        taskTable.destroy();
        $("#tastReport_table  tbody").remove();
        var taskActFormArray = $('#taskActitvityForm').serializeArray();
        var TaskReportUrl = $("#TaskReportUrl").val();

        taskTable = $('#tastReport_table').DataTable({

            dom: 'l<"html5buttons"B>tr<"col-sm-5"i><"col-sm-7"p>',
            buttons: [
                {
                    extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Excel', title: "'"+APPLICATION_NAME+" : Task Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    }
                },
                {
                    extend: 'pdfHtml5', text: '<i class="fa fa-file-pdf-o"></i> PDF', title: "'"+APPLICATION_NAME+" : Task Report'", header: true,
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                    customize: function (doc) {
                        doc.content[1].table.widths = [40, 50, 50, 40, 70, 40, 50, 160, 110, 50, 50, 60, 60];
                        doc.styles.tableHeader.alignment = 'left';
                        doc['header'] = (function (page, pages) {
                            return {
                                columns: [
                                     APPLICATION_NAME,
                                    {
                                        alignment: 'left',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: [5, 0]
                            };
                        });
                    }
                },
                {
                    extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print', title: "'"+APPLICATION_NAME+" : Task Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                    },
                    orientation: 'landscape',
                    customize: function (win) {
                        $(win.document.body).find('td').css('white-space', 'normal');
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],
            lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            processing: true,
            serverSide: true,
			stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "desc"];
			},
            bFilter: false,
            scrollX: true,
            sScrollY: 400,
            bScrollCollapse: true,
            destroy: true,
            autoWidth: true,
            order: [[0, "DESC"], [4, "ASC"], [12, "ASC"], [10, "ASC"]],

            ajax: {
                url: TaskReportUrl,
                type: "POST",
                data: function (ae) {
                    ae.searchData = taskActFormArray;
                }
            },
            fnRowCallback: function (nRow, aData) {
                $(nRow).attr("id", '' + aData[0]);
                TaskSelectedrowId = "";
            },
            initComplete: function (settings, json) {
            },
            columnDefs: [{ targets: 7, className: "textwrap-line" }],
            select: {
                style: 'single'
            }
        });
    };

    $('#start_task_date').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true,
        endDate: new Date()
    }).on('changeDate', function (ev) {
        $('#end_task_date').datetimepicker('setStartDate', ev.date);
    });

    $('#end_task_date').datetimepicker({
        format: 'mm/dd/yyyy',
        minView: 2,
        autoclose: true
    }).on('changeDate', function (ev) {
        $('#start_task_date').datetimepicker('setEndDate', ev.date);
    });

    $('#TaskRepotProceed').click(function () {
        $("#taskReportListing").modal('show');
        $.taskTableSearch();
    });

    $('#printTaskList').click(function () {
        taskTable.buttons('.buttons-print').trigger();
    });

    $('#resetTaskReportFilter').click(function () {
        taskTable.search('').draw();
        $('#taskActitvityForm')[0].reset();
        $('.select2Class').trigger('change');
        return false;
    });

    $('body').on('click', '#tastReport_table tbody tr', function () {

        var id = this.id;

        var index = $.inArray(id, selected);
        if (index === -1) {
            selected.push(id);
        } else {
            selected.splice(index, 1);
        }
        TaskSelectedrowId = id;
        $("#tastReport_table tbody tr").removeClass('selected');
        $(this).toggleClass('selected');
    });

    $('body').on('click', '#OpenRelatedCase', function () {

        if (TaskSelectedrowId === "") {
            swal({
                title: "Please Select row",
                type: "warning",
                showCancelButton: false,
                customClass: "sweet-alert-warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok"
            });
        } else {
            if (TaskSelectedrowId === '0') {
                swal({
                    title: "Case activity files not found for case #0",
                    type: "warning",
                    showCancelButton: false,
                    customClass: "sweet-alert-warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok"
                });
            } else {
                window.open($(this).data('url') + TaskSelectedrowId, '_blank');
            }

        }
    });


    $.taskPoolreportsearch = function () {
        taskpoolTable.destroy();
        $("#taskpoolReport_table  tbody").remove();
        var taskpoolReportUrl = $("#taskpoolReportUrl").val();

        taskpoolTable = $('#taskpoolReport_table').DataTable({

            dom: 'l<"html5buttons"B>tr<"col-sm-5"i><"col-sm-7"p>',
            buttons: [
                {
                    extend: 'excel', text: '<i class="fa fa-file-excel-o"></i> Excel', title: "'"+APPLICATION_NAME+" : Pool Task Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
                {
                    extend: 'pdfHtml5', text: '<i class="fa fa-file-pdf-o"></i> PDF', title: "'"+APPLICATION_NAME+" : Pool Task Report'", header: true,
                    orientation: 'portrait',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    },
                    customize: function (doc) {
                        doc.content[1].table.widths = [90, 90, 90, 160, 90];
                        doc.styles.tableHeader.alignment = 'left';

                        doc['header'] = (function (page, pages) {
                            return {
                                columns: [
                                    APPLICATION_NAME,
                                    {
                                        alignment: 'left',
                                        text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                    }
                                ],
                                margin: [10, 0]
                            };
                        });
                    }
                },
                {
                    extend: 'print', text: '<i class="fa fa-print" aria-hidden="true"></i> Print', title: "'"+APPLICATION_NAME+" : Pool Task Report'",
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    },
                    orientation: 'portrait',
                    customize: function (win) {
                        $(win.document.body).find('td').css('white-space', 'normal');
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],
            lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            processing: true,
            serverSide: true,
            bFilter: false,
            scrollX: true,
			stateSave: true,
			stateSaveParams: function (settings, data) {
				data.search.search = "";
				data.start = 0;
				delete data.order;
				data.order = [0, "ASC"];
			},
            sScrollY: 400,
            bScrollCollapse: true,
            destroy: true,
            autoWidth: true,
            order: [[0, "ASC"]],
            columnDefs: [
                { className: "dt-right", targets: [1, 2, 3, 4] }, { className: "textwrap-line", targets: [1] }
            ],
            ajax: {
                url: taskpoolReportUrl,
                type: "POST"
            },
            fnRowCallback: function (nRow, aData) {

            },
            initComplete: function (settings, json) {
            },
            select: {
                style: 'single'
            }
        });
    };

    $('body').on('click', '#TaskPoolReport', function () {
        $("#taskpoolReportlist").modal('show');
        $.taskPoolreportsearch();
    });

    $('#printTaskPool').click(function () {
        taskpoolTable.buttons('.buttons-print').trigger();
    });
});


$('#task_report').on('hide.bs.modal', function () {
    $("#resetTaskReportFilter").trigger('click');
});




/* SCRIPT FOR MARKUP REPORT */
$(document.body).on('click', '#getMarkUpRecord', function () {
    $("#markupReport_dataTable tbody").remove();
    markupTable.destroy();
    var MarkupReportUrl = $("#MarkupReportUrl").val();
    $('#markupReport_Content').modal('show');
    var markupsarray = $('#markuReportForm').serializeArray();
    markupTable = $('#markupReport_dataTable').DataTable({
        buttons: [{
            extend: 'print',
            title: 'Markup Report Results',
            autoPrint: 'false',
            footer: 'true',
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            }
        }],
        lengthMenu: [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
        processing: true,
        serverSide: true,
        bFilter: true,
        scrollX: true,
		stateSave: true,
		stateSaveParams: function (settings, data) {
			data.search.search = "";
			data.start = 0;
			delete data.order;
			data.order = [0, "desc"];
		},
        sScrollY: 400,
        bScrollCollapse: true,
        destroy: true,
        order: [[4, "asc"]],
        autoWidth: true,
        ajax: {
            url: MarkupReportUrl,
            type: "POST",
            "data": function (ae) {
                ae.searchData = markupsarray;
            }
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).attr("id", aData[0]);
            return nRow;
        },
        columnDefs: [{
            targets: 7,
            className: 'row-width',
            width: 200
        }],
        select: {
            style: 'single'
        }
    });
    $('#markup_report').modal('hide');
});
$(document.body).on('click', '#printMarkupReport', function () {
    markupTable.buttons('.buttons-print').trigger();
});