var n = "";
/*var url = "<?php echo base_url(); ?>";*/

function grab_emails() {
    var e = $("#task-navigation li.active").attr("id");
    var current_list = $("#task-navigation li.active").attr('id');
    if (current_list == 'Self') {
        $(".mark_as_old").hide();
    }
    $("#searchFiltr").css("display", "block");
    $(".category-list a").hasClass("activeli") ? list_type = $(".activeli").data("listing") : list_type = "", current_list = e;
    var t = "";
    var a = $("input[name=emailtypes]:checked");
    a.length > 0 && (t = a.val());
    var i = "";
    var l = $("input[name=casetypes]:checked");
    l.length > 0 && (i = l.val());
    if ("delete" == e) {
        $(".mark_button button").hide();
        $("#selectall_div").hide();
        $(".reply_compose_email").hide();
        $(".reply_all_compose_email").hide();
        $(".forward_email").hide();
        $(".envelope_email_open").hide();
        $(".move_to_new").hide();
        $(".move_to_new_one").hide();
    } else {
        $(".mark_button button").show();
        $("#selectall_div").show();
        $(".reply_compose_email").show();
        $(".reply_all_compose_email").show();
        $(".forward_email").show();
        $(".envelope_email_open").show();
         $(".move_to_new").hide();
        $(".move_to_new_one").hide();
    }

    if ("old" == e || "delete" == e) {
        $(".mark_as_old").hide();
    } else {
        $(".mark_as_old").show();
    }

    if ("sent" == e) {
        $(".category-list").hide();
    } else {
        $(".category-list").show();
    }

    if ("new" != e) {
        $(".mark_as_old,.move_to_old_one").hide();
    }

    n = $(".email-new-dataTable").DataTable({
        processing: true,
        serverSide: true,
        stateSave: true,
        stateSaveParams: function (settings, data) {
            data.search.search = "";
            data.start = 0;
            delete data.order;
            data.order = [1, "desc"];
        },
        scrollX: "100%",
        scrollY: "585px",
        scrollCollapse: true,
        ajax: {
            url: url + "email_old/refresh_listing",
            dataType: "json",
            type: "POST",
            data: {
                current_list: current_list,
                emailType: t,
                caseType: i,
                list_type: list_type
            }
        },
        columns: [
            {
                data: "e_id",
                width: "1%"
            },
            {
                data: "e_from",
                width: "90%"
            }
        ],
        order: [
            [1, "desc"]
        ],
        destroy: !0,
        aoColumnDefs: [
            {
                bSortable: !1,
                aTargets: [0]
            },
            {
                bSearchable: !1,
                aTargets: [0]
            }
        ],
        initComplete: function () {
            /*this.api().columns().every(function () {
                var e = this, t = document.createElement("input");
                $(t).appendTo($(e.footer()).empty()).on("change", function () {
                    e.search($(this).val(), !1, !1, !0).draw();
                });
            });

            if ($("#DataTables_Table_0 tbody tr td:eq(1) a").length > 0) {
            } else {
                $(".changeContentDiv").hide();
                $(".placeholderEmailDiv").show();
            }
            */
        },
        fnDrawCallback: function (oSettings) {
            $('#selectall').prop('checked', false);
            n.columns.adjust();
        }
    });

    if ("delete" == e ? n.columns([0]).visible(!1) : n.columns([0]).visible(!0), "sent" == e) {
        var o = n.columns(1).header();
        $(o).html("Emails");
    } else {
        o = n.columns(1).header();
        $(o).html("Emails");
    }
}
function show_hide_main_window(e) {
    if ($(".changeContentDiv:visible").hasClass("composeEmailDiv")) {
        swal({
            title: "Are you sure?",
            text: "The changes you made will be lost",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, discard it!",
            closeOnConfirm: !0
        }, function () {
            if (e == "email_detail") {
                $(".changeContentDiv").hide();
                $(".email_conversation").hide();
                $(".emailDetailDiv").show();
            } else if (e == "email_listing") {
                $(".changeContentDiv").hide();
                $(".email_conversation").hide();
                $(".placeholderEmailDiv").show();
            } else if (e == "email_compose") {
                $(".changeContentDiv").hide();
                $(".email_conversation").hide();
                $(".composeEmailDiv").show();
            } else if (e == "email_conversation") {
                $(".changeContentDiv").hide();
                $(".email_conversation").show();
            }
        });
    } else {
        if (e == "email_detail") {
            $(".changeContentDiv").hide();
            $(".email_conversation").hide();
            $(".emailDetailDiv").show();
        } else if (e == "email_listing") {
            $(".changeContentDiv").hide();
            $(".email_conversation").hide();
            $(".placeholderEmailDiv").show();
        } else if (e == "email_compose") {
            $(".changeContentDiv").hide();
            $(".email_conversation").hide();
            $(".composeEmailDiv").show();
        } else if (e == "email_conversation") {
            $(".changeContentDiv").hide();
            $(".email_conversation").show();
        }
    }
}

function validate_ext_email(e) {
    var t = "", a = 0;
    -1 !== e.indexOf(",") && (a = (t = e.split(",")).length);
    var l = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (a > 0) {
        for (i = 0; i < t.length; i++)
            if (!l.test(t[i].trim()))
                return swal({
                    title: "Alert",
                    text: "Oops! Please enter valid email adress.\n" + t[i].trim(),
                    type: "warning"
                }), !1
    } else if (!l.test(e.trim()))
        return swal({
            title: "Alert",
            text: "Oops! Please enter valid email address.\n" + e.trim(),
            type: "warning"
        }), !1;
    return !0
}

/*
 function validateAttachFileExtension(e) {
 return !!/(\.doc|\.docx|\.pdf|\.jpg|\.jpeg|\.gif|\.png|\.txt|\.xlsx|\.xls|\.rtf|\.html|\.htm|\.wpd|\.wps|\.csv|\.ppt|\.pptx|\.zip|\.tar|\.xml|\.XLR|\.mp3|\.rar)$/i.test(e) || (swal({
 title: "Alert",
 text: "Oops! Please provide valid file format.\n" + e + " is not valid file format",
 type: "warning"
 }), !1)
 }
 */

function removeDuplicateUsingSet(e) {
    return Array.from(new Set(e));
}

$(document).on("click", ".confirm", function () {
    $('#mailsenddisable').removeAttr("disabled", "disabled");
    $('#mailsenddisable1').removeAttr("disabled", "disabled");
});

$(document).on("click", ".refresh-mail", function () {
    grab_emails();
});

$(document).on("click", "#email_filter", function () {
    grab_emails();
});

$(document).ready(function () {
    $(".close_email").click(function (e) {
        $(".composeEmailDiv").hide();
        $(".placeholderEmailDiv").show();
    });

    $('input[type="radio"]').removeAttr("checked").iCheck("update");

    var e = "";
    grab_emails();

    CKEDITOR.replace("mailbody");

    $(".category-list a").click(function (t) {
        $(this).data("listing");
        $(".category-list a").removeClass("activeli");
        $(this).addClass("activeli");
        e = $("#task-navigation li.active").attr("id");
        grab_emails();
    });

    $("#task-navigation li").on("click", "a", function () {
        $(".category-list a").removeClass("activeli");
        $("#task-navigation li").removeClass("active");
        $(this).parent("#task-navigation li").addClass("active");
        /* , "", "", "", */
        grab_emails();
    });

    $("input[name=emailtypes]:radio,input[name=casetypes]:radio").on("change", function (e) {
        grab_emails();
    });

    $(document).on("click", "#clear_email_filter", function () {
        $(".category-list a").removeClass("activeli");
        $("input[name=emailtypes]:radio,input[name=casetypes]:radio").prop("checked", !1);
        $(".refresh-mail").trigger("click");
    });

    $('select[name="whoto"]').select2({
        placeholder: "Select User to Send Email",
        multiple: !0,
        selectOnClose: 0
    }).on("change", function () {
        $('select[name="whoto"]').valid();
    });

    $('#mailsenddisable1').on('click', function() {
        $('#mailsenddisable').trigger('click');
    });
    $("#send_mail_form").submit(function () {
        $('#mailsenddisable').attr("disabled", "disabled");
        $('#mailsenddisable1').attr("disabled", "disabled");
        setTimeout(function () {
            $('#mailsenddisable').removeAttr("disabled", "disabled");
            $('#mailsenddisable1').removeAttr("disabled", "disabled");
        }, 3000);

        $('select[name="whoto"]').parent().find("#wvalid").remove();
        $('input[name="emails"]').parent().find("span").remove();
        $("#mail_subject").parent().find("span").remove();

        var e;
        e = $('select[name="whoto"]').val();
        var t = $("#mail_subject").val();
        var a = $('input[name="mail_urgent"]:checked').val();
        var i = $('input[name="emails"]').val();
        var l = CKEDITOR.instances.mailbody.getData();
        var n = $("#filenameVal").val();
        var o = $("#mailType").val();
        var s = $("#case_no").val();
        var r = !0;


        if (null == e && "" == i && ($('select[name="whoto"]').parent().append('<span id="wvalid" style="color:red;">Please Select a User to send mail to</span>'), $('input[name="emails"]').parent().append('<span style="color:red;">Please add external party to send email </span>'), setTimeout(function () {
            $('select[name="whoto"]').parent().find("#wvalid").remove(), $('input[name="emails"]').parent().find("span").remove()
        }, 5e3), r = !1));
        if ("" == l)
            return r = !1, $(".mail-text").prepend('<span id="content-mail-text" style="color:red;">Please Enter Mail Content</span>'), setTimeout(function () {
                $(document).find("#content-mail-text").remove()
            }, 5e3), !1;

        var c = new FormData;
        c.append("whoto", e), c.append("subject", t), c.append("urgent", a), c.append("ext_emails", i), c.append("mailbody", l), c.append("mailType", o), c.append("filenameVal", n), c.append("caseNo", s);

        $("input#afiles")[0].files.length;
        var filelength = $("input[name='afiles1[]']");
        var fileGlobal = [];
        var tmp_fileGlobal = $("input[name='files_global_var[]']").map(function () {
            return $(this).val();
        }).get();

        $.each(tmp_fileGlobal, function (k, v) {
            var array = $.map(JSON.parse(v), function (value, index) {
                return [index + ':' + value];
            });
            fileGlobal[k] = array;
        });
        var files_arry = [];
        c.append("fileGlobalVariable", JSON.stringify(fileGlobal));
        return $.each(filelength, function (e, t) {
            files_arry.push(t.defaultValue);
            if (filelength.length == files_arry.length) {
                c.append("files", files_arry)
            }
        }), "" != s && 0 != s ? $.ajax({
            url: url + "Prospects/CheckCaseno",
            type: "post",
            data: {
                pro_caseno: s
            },
            success: function (e) {
                var t = $.parseJSON(e);
                0 == t ? (swal({
                    title: "Alert",
                    text: "Please enter valid case number.",
                    type: "warning"
                }), r = !1) : 1 == t && 1 == r && $.ajax({
                    url: url + "email_old/send_email",
                    method: "POST",
                    dataType: "json",
                    cache: !1,
                    contentType: !1,
                    processData: !1,
                    data: c,
                    beforeSend: function (e) {
                        $.blockUI();
                    },
                    complete: function (e, t) {
                        $.unblockUI()
                    },
                    success: function (e) {

                        if ("true" != e.result)
                            return "false" == e.result && "0" == e.ext_party ? (swal({
                                title: "Alert",
                                text: "Oops! Some went wrong. Email wasn't send to External Party.",
                                type: "warning"
                            }), !1) : "false" == e.result && "" != e.attachment_issue ? (swal({
                                title: "Alert",
                                text: "Oops! " + e.attachment_issue,
                                type: "warning"
                            }), !1) : (swal({
                                title: "Alert",
                                text: "Oops! Some error has occured. Please try again later.",
                                type: "warning"
                            }), !1);
                        swal({
                            title: "Success!",
                            text: "Mail is sent successfully.",
                            type: "success"
                        }), $(".composeEmailDiv").hide(), $(".placeholderEmailDiv").show(), $('.fileremove').remove()
                    }
                })
            }
        }) : 1 == r && $.ajax({
            url: url + "email_old/send_email",
            method: "POST",
            dataType: "json",
            cache: !1,
            contentType: !1,
            processData: !1,
            data: c,
            beforeSend: function (e) {
                $.blockUI()
            },
            complete: function (e, t) {
                $.unblockUI()
            },
            success: function (e) {
                if ("true" != e.result)
                    return "true" == e.result && "0" == e.ext_party ? (swal({
                        title: "Alert",
                        text: "Oops! Some went wrong. Email wasn't send to External Party.",
                        type: "warning"
                    }), !1) : "false" == e.result && "" != e.attachment_issue ? (swal({
                        title: "Alert",
                        text: "Oops! " + e.attachment_issue,
                        type: "warning"
                    }), !1) : (swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), !1);
                swal({
                    title: "Success!",
                    text: "Mail is sent successfully.",
                    type: "success"
                }), $(".composeEmailDiv").hide(), $(".placeholderEmailDiv").show(), $('.fileremove').remove()
            }
        }), !1
    });

    $(".discard_message").click(function (e) {
        swal({
            title: "Are you sure?",
            text: "The changes you made will be lost",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, discard it!",
            closeOnConfirm: !0
        }, function () {
            $(".changeContentDiv").hide(), $(".email_conversation").hide(), $(".emailListing").show(), $(".placeholderEmailDiv").show(), $(".att-add-file").not(":first").remove()
        });
    });

    $(".mark_as_read").click(function (e) {
        e.preventDefault();
        var t, a = [];
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val());
            var i = $(this).find("input[type=checkbox]");
            i.prop("checked", i.prop("checked"))
        }), t = a.length, "" != (a = a.join()) ? $.ajax({
            url: url + "email_old/mark_as_read",
            dataType: "json",
            type: "POST",
            data: {
                filenames: a
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? (total = e.count - t, e.unread_count - t > 0 ? ($(".total_mail_count").html(e.unread_count).show(), $(".nav .email_count").html(e.unread_count).show(), $(".mail-box-header .email_count").html("(" + (e.unread_count - t) + ")").show()) : $(".total_mail_count").hide(), $(".icheckbox_square-green.checked").parents("tr.unread").addClass("read").removeClass("unread"), $.each($("tbody tr .checked"), function (e, t) {
                    $(t).iCheck("uncheck")
                })) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                }), grab_emails()
            }
        }) : swal({
            title: "Alert",
            text: "Please select atleast one unread Email to mark it as Read",
            type: "warning"
        })
    });

    $(".mark_as_old").click(function (e) {
        e.preventDefault();
        var t, a = [];
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val())
        }), t = a.length, "" != (a = a.join()) ? swal({
            title: "Are you sure?",
            text: "You want to move this email to old?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, move it!",
            closeOnConfirm: !0
        }, function () {
            $.ajax({
                url: url + "email_old/mark_as_old",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: a
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? (total = e.count - t, e.unread_count - t > 0 ? ($(".total_mail_count").html(e.unread_count - t).show(), $(".nav .email_count").html(e.unread_count - t).show(), $(".mail-box-header .email_count").html("(" + (e.unread_count - t) + ")").show(), $("#check_click_unread").val("1")) : $(".total_mail_count").hide(), $(".icheckbox_square-green.checked").parents("tr.unread").addClass("read").removeClass("unread"), $.each($("tbody tr .checked"), function (e, t) {
                        $(t).iCheck("uncheck")
                    })) : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), grab_emails()
                }
            })
        }) : swal({
            title: "Alert",
            text: "Please select atleast one Email to mark it as Old",
            type: "warning"
        })
    });

    $(".mark_as_imp").click(function (e) {
        e.preventDefault();
        var t, a = [];
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val())
        }), t = a.length, a = a.join(), t > 0 ? $.ajax({
            url: url + "email_old/mark_as_urgent",
            dataType: "json",
            type: "POST",
            data: {
                filenames: a
            },
            beforeSend: function () {
                $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                "true" == e.result ? ($.each($(".icheckbox_square-green.checked").parents("tr").find("td.mail-ontact"), function (e, t) {
                    0 == $(t).find("i.fa.fa-circle.text-danger").length && $(t).append(' <i class="fa fa-circle text-danger"></i>')
                }), $.each($("tbody tr .checked"), function (e, t) {
                    $(t).iCheck("uncheck")
                })) : "false" == e.result && swal({
                    title: "Alert",
                    text: "Oops! Some error has occured. Please try again later.",
                    type: "warning"
                }), grab_emails()
            }
        }) : swal({
            title: "Alert",
            text: "Please select atleast one email to mark it as Important",
            type: "warning"
        })
    });

    $(".refresh").click(function (t) {

        t.preventDefault(), $.ajax({
            url: url + "email_old/refresh_listing",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                start: 0,
                length: length
            },
            beforeSend: function () {
                start = 0, $.blockUI();
            },
            complete: function () {
                $.unblockUI();

            },
            success: function (e) {

                total = e.count, allEmail = e.count, e.unread_count > 0 ? ($(".total_mail_count").html(e.unread_count).show(), $(".mail-box-header .total_mail_count").html("(" + e.unread_count + ")").show()) : ($(".total_mail_count").hide(), $(".dropdown .email_count").text(e.unread_count).show());
                var t = "";
                $.each(e.result, function (e, a) {
                    t += generate_tr_html(a)
                }), $(".mail-box tbody").html(t), $(".i-checks").iCheck({
                    checkboxClass: "icheckbox_square-green",
                    radioClass: "iradio_square-green"
                }), $("input#selectall").iCheck("uncheck")
            }
        })
    });

    $(".compose-mail").click(function (e) {
        $(".files_global_var").remove(), $("#mailType").val(""), $("#send_mail_form")[0].reset(), $("#cke_1_contents").css("height", "130px"), CKEDITOR.instances.mailbody.setData(""), $("#whoto").val([]).trigger("change"), show_hide_main_window("email_compose"), $(document).find(".att-add-file").not(":first").remove(), $(".compose_title").html("")
    });

    $(document.body).on("click", ".back_email", function () {
        show_hide_main_window("email_listing"), grab_emails()
    });

    $(document.body).on("click", "a.read-email", function () {
        var checkchildmail = $(this);
        $('tr').removeClass('email-table-selected');
        $(this).parent().parent().addClass('email-table-selected');
        var t = $(this).data("filename"),
            a = $(this).data("e2id"),
            i = $(this).attr("email_con_id"),
            l = $(this).attr("ethread"),
            n = $(this).attr("iore"),
            d = $(this).attr("data-ids"),
            ttt = $(this).attr("data-needtocheckchildthread"),
            o = $(this),
            s = $("#task-navigation li.active").attr("id");

        l > 1 ? $.ajax({
            url: url + "email_old/emailThreads",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                ids: d,
                e2id: a,
                thread: l,
                email_con_id: i,
                vfrom: "email_details"
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                o.parent().parent().after(e),
                    o.parent().parent().find("a").removeClass("read-email"),
                    o.parent().parent().find("a").addClass("read-email-close");

                if ($('.email-check-id').is(":checked")) {
                    $('.childThread[data-ids=' + d + ']').prop('checked', true);
                }
            }
        }) : $.ajax({
            url: url + "email_old/view",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                iore: n,
                e2id: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (a) {

                $('.openunread_filename').val(t), $('.envelope_email_open').attr('iore', n), $('.envelope_email_open').attr('email_con_id', a.emails.email_con_id);
                $('.openunread_filename').val(t), $('.envelope_email_open_unread').attr('iore', n), $('.envelope_email_open_unread').attr('email_con_id', a.emails.email_con_id);

                if (a.emails.readyet == 'Y') {
                    $('.envelope_email_open_unread').show();
                    $('.envelope_email_open').hide();
                    $('.reply_all_compose_email').show();
                } else {
                    $('.envelope_email_open').show();
                    $('.envelope_email_open_unread').hide();
                    $('.reply_all_compose_email').show();
                }
                if (show_hide_main_window("email_detail"), $("#filenameVal").val(t), $("#reply_iore").val(n), a.unread_count > 0 ? ($(".total_mail_count").html(a.unread_count).show(), $(".email_count").html(a.unread_count).show(), $(".mail-box-header .total_mail_count").html("(" + a.unread_count + ")").show()) : $(".total_mail_count").hide(), "sent" == e && $(".total_mail_count").hide(), $("#emailSubject").html(a.emails.subject), $("#emailDatetime").html(moment(a.emails.datesent).format("DD MMM YYYY - hh:mm A")), $("#emailfromName").html(a.emails.whofrom), a.emails.whofrom == a.emails.email_con_people && (a.emails.email_con_people = current_user_injs), "0" != a.emails.caseno) {
                    $("#gotocase").html("");
                    var caseInfo = '';
                    if (a.emails.caseno != '0' && a.emails.caseno != '') {
                        $.ajax({
                            url: url + "cases/get_party_info",
                            dataType: "json",
                            type: "POST",
                            async: false,
                            data: {
                                caseno: a.emails.caseno
                            },
                            success: function (result) {
                                if (typeof (result['firm']) != 'undefined' && result['firm'] != '') {
                                    caseInfo = result['firm'];
                                } else if (typeof (result['first']) != 'undefined' && typeof (result['last']) != 'undefined') {
                                    caseInfo = result['first'] + ' ' + result['last'];
                                } else {
                                    caseInfo = '';
                                }
                            },
                            complete: function () {
                                $.unblockUI();
                            }
                        });
                    }
                    var i = url + "cases/case_details/" + a.emails.caseno;
                    $("#gotocase").html('Go To Case : <a href="' + i + '" target="_blank" >' + a.emails.caseno + " " + caseInfo + "</a>")
                } else {
                    $("#gotocase").html("");
                }
                if ("sent" == e) {
                    var email_con_peoples = a.emails.email_con_people;
                    var email_con_peoples_res = email_con_peoples.replace(a.emails.whofrom.concat(','), "");
                    $("#inboxto").html(""), $("#sentto").html("<br>To: " + email_con_peoples_res + "");
                } else {
                    $("#sentto").html("");
                    if ($('#task-navigation').find('.whofrom.active').children().parent().attr('id') == 'Self') {
                        var l = a.emails.whoto;
                    } else if ($('#task-navigation').find('.whofrom.active').children().parent().attr('id') == 'sent') {
                        var l = a.emails.whoto;
                    } else if (a.emails.subject.match(/RE/g) != null && $('#task-navigation').find('.whoto.active').children().parent().attr('id') != 'new') {
                        var l = a.emails.whoto;
                    } else {
                        // var email_con_peoples = a.emails.email_con_people;
                        var email_con_peoples = a.emails.whoto;
                        var l = null != email_con_peoples ? email_con_peoples : a.emails.whoto;
                    }
                    if (l == null) {
                        l = a.emails.whofrom;
                    }
                    if (l.split(",").length > 1) {
                        $('.reply_compose_email').hide();
                    }else{
                        $('.reply_compose_email').show();
                    }
                    $("#inboxto").html("To: " + l)
                }

                if (a.file_with_attachment) {
                    $(".emailDetailDiv").addClass("file_with_attachment");
                } else {
                    $(".emailDetailDiv").removeClass("file_with_attachment");
                }

                $(".mail-body #emailBody").html(a.emails.mail_body);
                $(".mail-body #file_with_attachment").html(a.file_with_attachment);
                $(".move_to_trash_one").attr("data-id", t);
                $(".print_email").attr("data-id", t);

                if (1 == a.reply_all) {
                    $(".emailDetailDiv .reply_all_compose_email").show();
                    $(".emailDetailDiv .reply_all_compose_email").attr("email_con_id", a.emails.email_con_id);
                } else {
                    $(".emailDetailDiv .reply_all_compose_email").show();
                }
                if ("delete" == s) {
                    $(".move_to_old_one").hide();
                    $(".move_to_trash_one").hide();
                    $(".reply_compose_email").hide();
                    $(".reply_all_compose_email").hide();
                    $(".forward_email").hide();
                    $(".envelope_email_open").hide();
                }
                if ("new" != s) {
                    $(".move_to_old_one").hide();
                } else {
                    $(".move_to_old_one").show();
                }
            }
        });
    });

    $(document.body).on("click", "a.emailaddresswidth", function () {
        $("#check_click_unread").val("0");
    });

    $(document.body).on("click", "a.envelope_email_open", function () {
        var t = $('.openunread_filename').val(),
            a = $(this).data("e2id"),
            i = $(this).attr("email_con_id"),
            l = $(this).attr("ethread"),
            n = $(this).attr("iore"),
            d = $(this).data("ids"),
            d = $(this).data("ids"),
            o = $(this),
            s = $("#task-navigation li.active").attr("id");

        var emailcount = $('#task-navigation').find('.active').attr('id') != 'delete' ? $('.emailchk').text() - 1 : $('.emailchk').text();

        l > 1 ? $.ajax({
            url: url + "email_old/emailThreads",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                e2id: a,
                ids: d,
                thread: l,
                email_con_id: i,
                vfrom: "email_details"
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                o.parent().parent().after(e), o.parent().parent().find("a").removeClass("read-email"), o.parent().parent().find("a").addClass("read-email-close")
            }
        }) : $.ajax({
            url: url + "email_old/markreadview",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                iore: n,
                e2id: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (a) {
                $("#check_click_unread").val("1");
                $("a[data-filename=" + t + "]").removeClass("notreadyet");
                $('.envelope_email_open').hide();
                $('.envelope_email_open_unread').show();

                if(s != 'old' && s!= 'sent') {
                    $(".email_count").html(emailcount);
                }
                $("td a[data-filename=" + t + "] i").removeClass("text-navy").addClass("text-warning");

                $(".mail-body #emailBody").html(a.emails.mail_body);
                $(".mail-body #file_with_attachment").html(a.file_with_attachment);
                $(".move_to_trash_one").attr("data-id", t);
                $(".print_email").attr("data-id", t);
                if (1 == a.reply_all) {
                    $(".emailDetailDiv .reply_all_compose_email").show();
                    $(".emailDetailDiv .reply_all_compose_email").attr("email_con_id", a.emails.email_con_id);
                } else {
                    $(".emailDetailDiv .reply_all_compose_email").show();
                }

                if ("delete" == s) {
                    $(".move_to_old_one").hide();
                    $(".move_to_trash_one").hide();
                    $(".reply_compose_email").hide();
                    $(".reply_all_compose_email").hide();
                    $(".forward_email").hide();
                    $(".envelope_email_open").hide();
                }

                if ("new" != s) {
                    $(".move_to_old_one").hide();
                } else {
                    $(".move_to_old_one").show();
                }
            }
        });
    });

    $(document.body).on("click", "a.envelope_email_open_unread", function () {
        var t = $('.openunread_filename').val(),
            a = $(this).data("e2id"),
            i = $(this).attr("email_con_id"),
            l = $(this).attr("ethread"),
            n = $(this).attr("iore"),
            d = $(this).data("ids"),
            o = $(this),
            s = $("#task-navigation li.active").attr("id");

        var emailcount = $('#task-navigation').find('.active').attr('id') != 'delete' ? parseInt($('.emailchk').text()) + 1 : parseInt($('.emailchk').text());

        l > 1 ? $.ajax({
            url: url + "email_old/emailThreads",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                e2id: a,
                ids: d,
                thread: l,
                email_con_id: i,
                vfrom: "email_details"
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                o.parent().parent().after(e), o.parent().parent().find("a").removeClass("read-email"), o.parent().parent().find("a").addClass("read-email-close")
            }
        }) : $.ajax({
            url: url + "email_old/markunreadview",
            dataType: "json",
            type: "POST",
            data: {
                current_list: s,
                filename: t,
                iore: n,
                e2id: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (a) {
                $("#check_click_unread").val("1");
                $("a[data-filename=" + t + "]").addClass("notreadyet");
                $('.envelope_email_open').show();
                $('.envelope_email_open_unread').hide();
                if(s != 'old' && s!= 'sent') {
                    $(".email_count").html(emailcount);
                }
                $(".envelope_email_open").removeClass("text-warning");
                $("td a[data-filename=" + t + "] i").removeClass("text-warning").addClass("text-navy");

                $(".mail-body #emailBody").html(a.emails.mail_body);
                $(".mail-body #file_with_attachment").html(a.file_with_attachment);
                $(".move_to_trash_one").attr("data-id", t);
                $(".print_email").attr("data-id", t);
                if (1 == a.reply_all) {
                    $(".emailDetailDiv .reply_all_compose_email").show();
                    $(".emailDetailDiv .reply_all_compose_email").attr("email_con_id", a.emails.email_con_id);
                } else {
                    $(".emailDetailDiv .reply_all_compose_email").show();
                }

                if ("delete" == s) {
                    $(".move_to_old_one").hide();
                    $(".move_to_trash_one").hide();
                    $(".reply_compose_email").hide();
                    $(".reply_all_compose_email").hide();
                    $(".forward_email").hide();
                    $(".envelope_email_open").hide();
                }

                if ("new" != s) {
                    $(".move_to_old_one").hide();
                } else {
                    $(".move_to_old_one").show();
                }
            }
        });
    });

    $(document.body).on("click", "a.read-email-close", function () {
        $(this).parents("tr").next("tr.rclose").remove(), $(this).removeClass("read-email-close"), $(this).addClass("read-email")
    });

    $(document.body).on("click", ".reply_compose_email", function () {
        $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
        var t = $(this).attr("email_con_id"),
            a = $(this).attr("ethread"),
            i = $("#reply_iore").val(),
            l = $("#filenameVal").val();
        $(".compose-mail").trigger("click"), $.ajax({
            url: url + "email_old/reply_email",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                filename: l,
                email_con_id: t,
                ethread: a,
                iore: i
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                $("#mailType").val("reply"), $("#whoto").val(e.emails[0].whofrom).trigger("change"), "E" == e.emails[0].iore && $("#emails").val(e.emails[0].whofrom), $("#mail_subject").val(e.emails[0].subject), $("#mail_urgent").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent").iCheck("check"), $("#case_no").val(e.emails[0].caseno != '0' ? e.emails[0].caseno : "");
                CKEDITOR.instances.mailbody.setData(e.emails[0].mail_body);
            }
        })
    });

    $(".move_to_old_one").click(function (e) {
        var t = $("#filenameVal").val();
        "" != t ? (swal({
            title: "Are you sure?",
            text: "You want to move this email to old?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, move it!",
            closeOnConfirm: !0
        }, function () {
            $.ajax({
                url: url + "email_old/mark_as_old",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: t
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? grab_emails() : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    });

                    $(".nav .email_count").html(e.unread_count);
                    $(".mail-box-header .email_count").html(e.unread_count);
                }
            })
        }), grab_emails()) : swal({
            title: "Alert",
            text: "Please select atleast one unread Email to mark it as Read",
            type: "warning"
        })
    });

    $(".forward_email").click(function (t) {
        $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
        var a = $("#reply_iore").val(),
            i = $("#filenameVal").val();
        $(".compose-mail").trigger("click"), $.ajax({
            url: url + "email_old/forward_email",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                filename: i,
                iore: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                if ($("#mailType").val("forward"), $("#mail_subject").val(e.emails[0].subject), "Y" == e.emails[0].urgent ? $("#mail_urgent").parent("div").addClass("checked") : $("#mail_urgent").parent("div").removeClass("checked"), e.emails[0].caseno > 0 && $("#case_no").val(e.emails[0].caseno), "" != e.file_with_attachment) {
                    new FormData("#send_mail_form");
                    /*$("#file_with_attachment_fwd").html(e.file_with_attachment), */$("#fwd_with_attachment").prop("checked", !0)
                }
                CKEDITOR.instances.mailbody.setData(e.emails[0].mail_body + e.file_with_attachment)
            }
        })
    });

    $(".move_to_trash").click(function (e) {
        e.preventDefault();
        var t, a = [],
            i = [],
            l = "";
        $.each($(".email-new-dataTable").find("input[type=checkbox]:checked"), function (e, t) {
            a.push($(t).val())
        });
        var n = removeDuplicateUsingSet(a);
        t = a.length, a = a.join(), i = i.join(), l = t > 1 ? n.length + " emails" : n.length + " email", i.length, t > 0 ? swal({
            title: "Are you sure?",
            text: "You will not be able to recover " + l + " !",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: !1
        }, function () {
            $.ajax({
                url: url + "email_old/move_to_trash",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: a,
                    email_con_ids: i
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? setTimeout(function () {
                        swal({
                            title: "Success!",
                            text: "Your " + l + " has been deleted.",
                            type: "success"
                        })
                    }, 1e3) : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    }), e.unread_count > 0 ? ($(document).find("#side-menu .email_count").html(e.unread_count).show(), $(document).find(".dropdown .email_count").html(e.unread_count).show()) : $(" .total_mail_count").hide(), grab_emails()
                }
            })
        }) : swal({
            title: "Alert",
            text: "Please select atleast one  Email to Delete",
            type: "warning"
        })
    });

    $(".move_to_trash_one").click(function (e) {
        e.preventDefault();
        var t = $(".move_to_trash_one").attr("data-id");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover selected email !",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: !1
        }, function () {
            $.ajax({
                url: url + "email_old/move_to_trash",
                dataType: "json",
                type: "POST",
                data: {
                    filenames: t
                },
                beforeSend: function () {
                    $.blockUI()
                },
                complete: function () {
                    $.unblockUI()
                },
                success: function (e) {
                    "true" == e.result ? (swal({
                        title: "Alert",
                        text: "Email(s) deleted successfully",
                        type: "success"
                    }), $(".changeContentDiv").hide(), $(".email_conversation").hide(), e.unread_count > 0 ? ($(" .total_mail_count").html(e.unread_count).show(), $(" .mail-box-header .total_mail_count").html("(" + e.unread_count + ")").show()) : $(" .total_mail_count").hide(), $(".placeholderEmailDiv").show()) : "false" == e.result && swal({
                        title: "Alert",
                        text: "Oops! Some error has occured. Please try again later.",
                        type: "warning"
                    })
                }
            }), grab_emails()
        })
    });

    $(document.body).on("click", ".print_email", function () {
        var t = $(".print_email").attr("data-id"),
            a = $("#reply_iore").val();
        $.ajax({
            url: url + "email_old/printEmail",
            method: "POST",
            data: {
                current_list: e,
                filename: t,
                iore: a
            },
            success: function (e) {
                $(e).printThis({
                    debug: !1,
                    header: "<h2 style=' padding-top:20px; '><center>View Message</center></h2>"
                })
            }
        })
    });

    $(document.body).on("click", "a.open-read-email", function () {
        var e = $(this).data("filename"),
            t = $(document).find("#task-navigation li.active").attr("id"),
            a = ($(this).attr("email_con_id"), $(this).attr("ethread"), $(this).attr("iore")),
            i = ($(this), "#collapse" + e);
        $.ajax({
            url: url + "email_old/view",
            dataType: "json",
            type: "POST",
            data: {
                current_list: t,
                filename: e,
                iore: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (l) {
                $("#filenameVal").val(e);
                $("#reply_iore").val(a);
                if (l.unread_count > 0) {
                    $(document).find("#side-menu .email_count").html(l.unread_count).show();
                    $(document).find(".dropdown .email_count").html(l.unread_count).show();
                    $(" .total_mail_count").html(l.unread_count).show();
                    $(" .mail-box-header .total_mail_count").html("(" + l.unread_count + ")").show();
                } else {
                    $(" .total_mail_count").hide();
                }

                "sent" == t && $(" .total_mail_count").hide(), $("body").find(i).find("#emailSubject").html(l.emails.subject), $("body").find(i).find("#emailDatetime").html(moment(l.emails.datesent).format("HH:mmA DD MMM YYYY")), data - filename$("body").find(i).find("#emailfromName").html(l.emails.whofrom_name + "(" + l.emails.whofrom + ")"), $("body").find(i).find(".mail-body #emailBody").html(l.emails.mail_body), $("body").find(i).find(".mail-body #file_with_attachment").html(l.file_with_attachment), $("body").find(i).find(".move_to_trash_one").attr("data-id", e), $("body").find(i).find(".print_email").attr("data-id", e)
            }
        })
    });

    $(document.body).on("click", ".reply_all_compose_email", function () {
        $("#whoto").val([]).trigger("change"), CKEDITOR.instances.mailbody.setData("");
        var t = $(this).attr("email_con_id"),
            a = ($(this).attr("ethread"), $("#reply_iore").val()),
            i = $("#filenameVal").val();
        $(".compose-mail").trigger("click"), $.ajax({
            url: url + "email_old/reply_all_email",
            dataType: "json",
            type: "POST",
            data: {
                current_list: e,
                filename: i,
                email_con_id: t,
                iore: a
            },
            beforeSend: function () {
                start = 0, $.blockUI()
            },
            complete: function () {
                $.unblockUI()
            },
            success: function (e) {
                $("#mailType").val("reply"), e.internal_party.length > 0 && $("#whoto").val(e.internal_party).trigger("change"), "" != e.external_party && $("#emails").val(e.external_party), $("#mail_subject").val(e.emails[0].subject), $("#mail_urgent").iCheck("uncheck"), "Y" == e.emails[0].urgent && $("#mail_urgent").iCheck("check"), $("#case_no").val((e.emails[0].caseno) != '0' ? e.emails[0].caseno : '');
                CKEDITOR.instances.mailbody.setData(e.emails[0].mail_body);
            }
        });
    });
});

$(document).on("change", "#selectall", function (e) {
    var checkbocchk = $(this).is(":checked");
    var allMainCheckBox = $("input.email-check-id");
    if (checkbocchk == true) {
        $('#DataTables_Table_0 tbody tr').addClass('email-table-selected');
        allMainCheckBox.prop('checked', true);
        $('.childThread').prop('checked', true);
        $.each(allMainCheckBox, function (index, eachMainCheckBox) {
            var objEachMainCheckBox = $(eachMainCheckBox);
            if (objEachMainCheckBox.data('ethread') > 0) {
                getAllThreadEmail(objEachMainCheckBox.parent().parent().parent());
            }
        });
    } else {
        $('#DataTables_Table_0 tbody tr').removeClass('email-table-selected');
        allMainCheckBox.prop('checked', false);
        grab_emails();
    }

});

$(document).on("change", ".email-check-id", function (e) {
    var checkmainbox = $(this).is(":checked");
    if (checkmainbox == false) {
        $('#selectall').prop('checked', false);
    }
    if ($('.email-check-id:checked').length == $('.email-check-id').length) {
        if ($('.childThread:checked').length == $('.childThread').length) {
            $('#selectall').prop('checked', true);
        }
    }
    if (checkmainbox == true) {
        var childthreadchk = $('.childThread[data-ids=' + $(this).attr('data-ids') + ']').attr('data-ids');
        var mainthreadchk = $('.email-check-id[data-ids=' + $(this).attr('data-ids') + ']').attr('data-ids');

        if (childthreadchk == mainthreadchk) {
            $('.childThread[data-ids=' + $(this).attr('data-ids') + ']').prop('checked', true);
            if ($('.email-check-id:checked').length == $('.email-check-id').length) {
                if ($('.childThread:checked').length == $('.childThread').length) {
                    $('#selectall').prop('checked', true);
                }
            }
        }
    }

});

$(document).on("change", ".childThread", function (e) {
    var checkchildboxs = $(this).is(":checked");

    if (checkchildboxs == false) {
        $('#selectall').prop('checked', false);
        $('.email-check-id[data-ids=' + $(this).attr('data-ids') + ']').prop('checked', false);
    }
    if (checkchildboxs == true) {
        var checkedchild = $('.childThread[data-ids=' + $(this).attr('data-ids') + ']:checked').length;
        var checkedmainparent = $(this).attr('eethread');

        if (checkedchild == checkedmainparent) {
            $('.email-check-id[data-ids=' + $(this).attr('data-ids') + ']').prop('checked', true);

            if ($('.email-check-id:checked').length == $('.email-check-id').length) {
                if ($('.childThread:checked').length == $('.childThread').length) {
                    $('#selectall').prop('checked', true);
                }
            }
        }
    }
    if ($('.email-check-id:checked').length == $('.email-check-id').length) {
        if ($('.childThread:checked').length == $('.childThread').length) {
            $('#selectall').prop('checked', true);
        }
    }
});

$(document).on("change", '.email-new-dataTable tr input[type="checkbox"]', function () {
    var a = $(this).is(":checked");
    var e = $(this);

    if (a == true) {
        if (e.data('ethread') > 0) {
            $(e.closest('td').next('td')).find('a.read-email').attr('data-needToCheckChildThread', '1');
            e.closest('td').next('td').find('a.read-email').trigger('click');
        }
    } else {
        $(e.closest('td').next('td')).find('a.read-email').attr('data-needToCheckChildThread', '1');
        e.closest('td').next('td').find('a.read-email-close').trigger('click');
    }
});

$(document).on("click", "i.fa-times-circle", function () {
    var e = $(this).attr("data"),
        t = $(this);
    swal({
        title: "Warning!",
        text: "Are you sure? Want to Remove This attachment ",
        type: "warning",
        showCancelButton: !0,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: !1
    }, function () {
        $.blockUI(), $.ajax({
            url: url + "email_old/removeattachment",
            data: {
                remove_file: e
            },
            method: "POST",
            success: function (e) {
                $.unblockUI();
                var a = $.parseJSON(e);
                1 == a.status ? (swal("Success !", a.message, "success"), t.parent("li").remove()) : swal("Oops...", a.message, "error")
            }
        })
    })
});

$(window).on("beforeunload", function (e) {
    if ($(".composeEmailDiv").is(":visible")) {
        return e.returnValue = "The changes you made will be lost, Do you want to discard it?"
    }
    e = null
});

$(document).on("click", ".addfile", function () {
    $(".att-add-file:last").after('<label class="col-sm-2 control-label"></label><div class="col-sm-12 att-add-file marg-top5 clearfix no-padding"> <input type="file" name="afiles[]" id="afiles" multiple ><i class="fa fa-plus-circle addfile" aria-hidden="true"></i> <i class="fa fa-times removefile" aria-hidden="true"></i></div>')
});

$(document).on("click", ".removefile", function () {
    $(this).closest(".att-add-file").prev("label").remove(), $(this).closest(".att-add-file").remove()
});

$("#Self").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'hidden');
    $(".category-list a").removeClass("activeli");
    $("input[name=emailtypes]:radio").prop("checked", !1);
    // $(".refresh-mail").trigger("click");
    $(".externalemailtype").removeAttr("checked");
    $(".internalemailtype").removeAttr("checked");
    $(".externalemailtype").attr("disabled", "disabled");
    $(".internalemailtype").attr("disabled", "disabled");
    $(".changeContentDiv").hide();
    $(".placeholderEmailDiv").show();
});

$("#sent").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'hidden');
    $("input[name=emailtypes]:radio").prop("checked", !1);
    /* $(".refresh-mail").trigger("click"); */
    $(".externalemailtype").removeAttr("checked");
    $(".internalemailtype").removeAttr("checked");
    $(".externalemailtype").attr("disabled", "disabled");
    $(".internalemailtype").attr("disabled", "disabled");
    $(".changeContentDiv").hide();
    $(".placeholderEmailDiv").show();
    /*$(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"),*/
});

$("#new").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'visible');
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"), $(".changeContentDiv").hide(), $(".placeholderEmailDiv").show()
});

$("#old").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'visible');
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"), $(".changeContentDiv").hide(), $(".placeholderEmailDiv").show()
});

$("#delete").click(function (e) {
    $('#searchFiltr').find('.col-lg-6:first-child').css('visibility', 'visible');
    $(".externalemailtype").removeAttr("disabled", "disabled"), $(".internalemailtype").removeAttr("disabled", "disabled"), $(".changeContentDiv").hide(), $(".placeholderEmailDiv").show()
});

$(document).on('click', ".email-check-id", function (e) {
    var checkbocchk = $(this).is(":checked");
    if (checkbocchk == true) {
        $(this).parent().parent().parent().addClass('email-table-selected');
    } else {
        $(this).parent().parent().parent().removeClass('email-table-selected');
    }
});

function getAllThreadEmail(trObject) {
    $(trObject).find('a.read-email').attr('data-needToCheckChildThread', '1');
    $(trObject).find('a.read-email').trigger('click');
}